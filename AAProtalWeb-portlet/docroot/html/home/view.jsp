<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
 <%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
 <%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet"%>
 <%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
 <%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
 <%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
 <portlet:defineObjects />
 <%@page import="java.io.Serializable"%>
 <%@page import="javax.portlet.PortletSession"%>
 <%@page import="com.liferay.portal.model.User"%>
 <%@page import="com.liferay.portal.model.Organization"%>
 <%@page import="com.liferay.portal.theme.ThemeDisplay"%>
 <%@ page language="java" contentType="text/html; charset=UTF-8"
     pageEncoding="UTF-8"%>
 <%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
 <%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
 <%@ page import="java.util.*"%>
 <%@ page import="javax.portlet.PortletPreferences"%>
 <%@ page import="java.text.DateFormat"%>
 <%@ page import="java.text.SimpleDateFormat"%>
 <%@ page
     import="com.liferay.portlet.expando.service.ExpandoValueServiceUtil"%>
 <%@ page
     import="com.liferay.portal.service.OrganizationLocalServiceUtil"%>
 <%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
 <%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
 <%@ page import="com.liferay.portal.kernel.language.LanguageUtil"%>
 <%@ page import="com.liferay.portal.kernel.util.ParamUtil"%>
 <%@ page import="com.liferay.portal.util.PortalUtil"%>
 <%@ page import="com.liferay.portal.kernel.language.LanguageWrapper"%>
 <%@ page import="com.liferay.portal.kernel.language.UnicodeLanguageUtil"%>
 <%@page import="com.liferay.portal.util.PortalUtil"%>
 <%@page import="javax.portlet.PortletURL"%>
 <%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
 <portlet:defineObjects />
 <%
 String namespace="_formPortlet_";
 ThemeDisplay themeDisplay = (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
 User user = themeDisplay.getUser();
 long userId=user.getUserId();

 HttpServletRequest r = PortalUtil.getHttpServletRequest(renderRequest);
 /* String langId=LanguageUtil.getLanguageId(request);
 String val =  PortalUtil.getOriginalServletRequest(r).getParameter("language");
 if(val !=null){
 	language =val;
 }else{
 boolean is_en_US=langId.equals("en_US");
 boolean is_zh_CN=langId.equals("zh_CN");

 if(is_zh_CN)
 	language="zh";
 } */
 String langId=LanguageUtil.getLanguageId(request);
 boolean is_en_US=langId.equals("en_US");
 boolean is_zh_CN=langId.equals("zh_CN");
 boolean is_ja_JP=langId.equals("ja_JP");

 %>

    <style>
    .aui .portlet-content{
       padding:0px;
    }
   .outlook{
               width:70%;
               color:white;
               border-bottom:solid 1px white;
               padding-bottom: 50px;
               margin:auto;
               text-align:left;
   }
   .homepage h1{
     color:#ECD3B2;
     text-aligh:left;
     font-size:4em;
     font-family:unset;
     line-height:80px;
   }
   .part2{
             display:block;
             width:100%;
    }
    .homepage img{
      width:70px;
      height:70px;
    }
 .homepage{
     background-color:#1d2752;
      width:100%;
      padding-top:30px;
      padding-bottom:30px;
   }
   .part3whole{
     display:block;
     flex:1;
     padding:10px;
   }
   .part3whole h1{
      font-size:150px;
      padding:50px;
      color:white;
   }
   .homepage p{
      font-size:18px;
      letter-spacing: 3px;
      line-height: 35px;
   }
   .part2{
       display:flex;
   }
   #part2{
     flex:1;
   }
   #part3{
     display:flex;
   }
   .part3whole h2{
      border-top:solid 1px;
      padding-top:30px;
   }
   #serivice{
      display:inline-block;
      width:30%;
      margin:auto
   }
   .part4whole{
     display:flex;
     margin-top:30px;
     margin-bottom: 30px;
   }
   /* .part4whole img{
     width:25%;
   } */
   .part4content{
     flex:1;
     text-align: left;
     margin-left:20px;
     padding:10px;
   }
   .part4content h1{
     font-size:40px;
     color:white;
   }
   .part5whole{
     display:flex;
     
   }
   .part5content{
     flex:1;
     padding:10px;
     text-align:center;
   }
   .homepage a{
        text-decoration: none;
        color:white;
   }
  .part2content{
        flex:1;
        padding:10px;
   }
   
   @media only screen and (max-width: 1375px) {
        #part3{
          display:block;
        }
        .part3whole h1{
          text-align:center;
        }
   }
   @media only screen and (max-width: 975px) {
       .part5whole{
     display:block;
     
   }
   .homepage h1{
       font-size:35px;
       line-height:50px;
   }
   .outlook{
      width:90%;
      text-align:center;
   }
   .part3whole h1{
      padding:50px 0 50px 0;
      font-size:150px;
   }
    .part2{
       display:block;
   }
    #part3{
       display:block;
   }
   .part4whole{
     display:block;
 
   }
   .part4content h1{
      text-align:center;
      font-size:30px;
   }
   #serivice{
     width:100%;
   }
       
   }
  </style>

<div class="homepage">
<div id="partone" class='outlook'>
    <h1><liferay-ui:message key='11'/></h1> 
    <p><liferay-ui:message key='12'/></p>
   <p><liferay-ui:message key='13'/></p>
</div>
<div id='parttwo' class='outlook'>
    <h1><liferay-ui:message key='21'/></h1>
    <div id='part2'>
   <div id='part21' class='part2'>
                    <div class='part2content'><img src='/html/themes/classic/images/AntiqueWorldSocialMedia.png'>
                           <h2><liferay-ui:message key='22'/></h2>
                           <p><liferay-ui:message key='23'/></p>
                     </div>
                    <div class='part2content'><img src='/html/themes/classic/images/AACoinExchangeServiceCenter.png'>
                            <h2><liferay-ui:message key='24'/></h2>
                           <p><liferay-ui:message key='25'/></p>
</div>
                    <div class='part2content'><img src='/html/themes/classic/images/AntiqueArtMortgageService.png'>
                           <h2><liferay-ui:message key='26'/></h2>
                           <p><liferay-ui:message key='27'/></p>
</div>
   </div>
   <div id='part22' class='part2'>
     <div class='part2content'><img src='/html/themes/classic/images/InsuranceService.png'>
                           <h2><liferay-ui:message key='28'/></h2>
                           <p><liferay-ui:message key='29'/></p>
                     </div>
                    <div class='part2content'><img src='/html/themes/classic/images/AACoinWallet.png'>
                            <h2><liferay-ui:message key='295'/></h2>
                           <p><liferay-ui:message key='296'/></p>
</div>
                    <div class='part2content'><img src='/html/themes/classic/images/AuctionService.png'>
                           <h2><liferay-ui:message key='293'/></h2>
                           <p><liferay-ui:message key='294'/></p>
</div>
</div>
<%--    <div id='part23' class='part2'>
         <div id='serivice'><img src='https://www.antiqueworld.com/html/themes/classic/images/Antique-Art-Valuation-Service.png'>
                           <h2><liferay-ui:message key='295'/></h2>
                           <p><liferay-ui:message key='296'/> </p>
</div>
</div> --%>
</div>
</div>
<div id='partthree' class='outlook'>
    <h1 style='display:block'><liferay-ui:message key='31'/></h1>
    <div id='part3'>
    <div id='part31' class='part3whole'>
          <h1>01</h1>
          <h2><liferay-ui:message key='32'/></h2>
          <p><liferay-ui:message key='33'/></p>
      
    </div>
    <div id='part32' class='part3whole'>
         <h1>02</h1>
         <h2><liferay-ui:message key='34'/></h2>
         <p><liferay-ui:message key='35'/></p>
    </div>
    <div id='part32' class='part3whole'>
         <h1>03</h1>
         <h2><liferay-ui:message key='36'/></h2>
         <p><liferay-ui:message key='37'/></p>
      
    </div>
  </div>
</div>
<div id='partfour' class='outlook'>
    <h1><liferay-ui:message key='41'/></h1>
    <div class='part4whole' id='part41'>
      <img src='/html/themes/classic/images/our-story.png'>
      <div class='part4content'>
           <h1><liferay-ui:message key='42'/></h1>
           <p><liferay-ui:message key='43'/></p>
      </div>
    </div>
    <div class='part4whole' id='part42'>
      <img src='/html/themes/classic/images/asset-backed.png'>
      <div class='part4content'>
           <h1><liferay-ui:message key='44'/></h1>
           <p><liferay-ui:message key='45'/></p>
      </div>
    </div>
    <div class='part4whole' id='part43'>
      <img src='/html/themes/classic/images/technology.png'>
      <div class='part4content'>
           <h1><liferay-ui:message key='46'/></h1>
           <p><liferay-ui:message key='47'/></p>
      </div>
    </div>
</div>
<div id='partfive' class='outlook'>
     <h1><liferay-ui:message key='51'/></h1>
     <div class='part5whole'>
         <div id='part51' class='part5content'>
           <img src='/html/themes/classic/images/Info@antiqueworld.com.png'>
           <p>info@antiqueworld.com</p>
         </div>
         <div id='part52' class='part5content'>
           <img src='/html/themes/classic/images/Tel-800-988-8230.png'>
           <p>Tel: 800-988-8230</p>
         </div>
         <div id='part53' class='part5content'>
           <img src='/html/themes/classic/images/200.png'>
           <p>200 Vesey St, New York,NY 10281</p>
         </div>
     </div>
     <center><a style="border:1px solid;padding:10px" href="https://www.antiqueworld.com/contact-us"><liferay-ui:message key='Contact'/></a></center>
     
</div>

<table style='margin:auto;margin-top:30px'>
        <tr>
            <td colspan='2'><a href='https://www.antiqueworld.com/termofuse' target="_blank"><liferay-ui:message key='termofuse'/></a></td>
            <td colspan='2'><a href='https://www.antiqueworld.com/-3' target="_blank" style='border-left:2px solid;padding-left:10px;margin-left:5px'><liferay-ui:message key='privacypolicy'/></a></td>
          </tr>
      </table>
<footer style='text-align:center;margin-top:50px;color:white'>
    <strong>© Copyright 2018, Antique World</strong>
  </footer>
</div>

<aui:script async src="https://www.googletagmanager.com/gtag/js?id=UA-122566395-1"></aui:script>
<aui:script use="array-extras, node, event">
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-122566395-1');

</aui:script>