 <%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
 <%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
 <%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet"%>
 <%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
 <%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
 <%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
 <portlet:defineObjects />
 <%@page import="java.io.Serializable"%>
 <%@page import="javax.portlet.PortletSession"%>
 <%@page import="com.liferay.portal.model.User"%>
 <%@page import="com.liferay.portal.model.Organization"%>
 <%@page import="com.liferay.portal.theme.ThemeDisplay"%>
 <%@ page language="java" contentType="text/html; charset=UTF-8"
     pageEncoding="UTF-8"%>
 <%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
 <%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
 <%@ page import="java.util.*"%>
 <%@ page import="javax.portlet.PortletPreferences"%>
 <%@ page import="java.text.DateFormat"%>
 <%@ page import="java.text.SimpleDateFormat"%>
 <%@ page
     import="com.liferay.portlet.expando.service.ExpandoValueServiceUtil"%>
 <%@ page
     import="com.liferay.portal.service.OrganizationLocalServiceUtil"%>
 <%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
 <%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
 <%@ page import="com.liferay.portal.kernel.language.LanguageUtil"%>
 <%@ page import="com.liferay.portal.kernel.util.ParamUtil"%>
 <%@ page import="com.liferay.portal.util.PortalUtil"%>
 <%@ page import="com.liferay.portal.kernel.language.LanguageWrapper"%>
 <%@ page import="com.liferay.portal.kernel.language.UnicodeLanguageUtil"%>
 <%@page import="com.liferay.portal.util.PortalUtil"%>
 <%@page import="javax.portlet.PortletURL"%>
 <%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
 <portlet:defineObjects />
 <%
 String namespace="OrderPortlet";
 ThemeDisplay themeDisplay = (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
 User user = themeDisplay.getUser();
 long userId=user.getUserId();

 String language = "en";
 HttpServletRequest r = PortalUtil.getHttpServletRequest(renderRequest);
 String langId=LanguageUtil.getLanguageId(request);
 boolean is_en_US=langId.equals("en_US");
 boolean is_zh_CN=langId.equals("zh_CN");
 boolean is_zh_TW=langId.equals("zh_TW");
 boolean is_ja_JP=langId.equals("ja_JP");

 %>
 <div>
      <table class='table table-striped table-bordered dataTable no-footer'>
         <thead>
            <tr>
               <th>名字</th>
               <th>姓氏</th>
               <th>电话号码</th>
               <th>邮箱</th>
               <th>用户id</th>
               <th>生日</th>
            </tr>
         </thead>
         <tbody id='userinfo'>
         </tbody>
      </table>
      
      <form>
          <input id='ori' type='password' placeholder='原密码'/>
          <input id='new1' type='password' placeholder='新密码'/>
          <input id='new2' type='password' placeholder='新密码'/>
      </form>
      <button id='submit'>提交</button>
 </div>
<aui:script use="array-extras, node, event">
   Liferay.Service(
		 '/AaPortal-portlet.commonfunction/get-user-info',
		 function(obj) {
			 var info = '';
			 info = '<tr><td>'+obj.data.firstName+'</td><td>'+obj.data.lastName+'</td><td>'+obj.data.phoneNumber+'</td><td>'+obj.data.emailAddress+'</td><td>'+obj.data.userId+'</td><td>'+obj.data.birthday+'</td></tr>';
			$('#userinfo').html(info);
		 }
		);
   
   $(document).ready(function(){
	   $('#submit').click(function(){
		   Liferay.Service(
				   '/AaPortal-portlet.commonfunction/update-password',
				   {
				     origin: document.getElementById('ori').value,
				     new1: document.getElementById('new1').value,
				     new2: document.getElementById('new2').value
				   },
				   function(obj) {
				     if(obj){
				    	 alert('成功');
				    	 $('#ori').val('');
				    	 $('#new1').val('');
				    	 $('#new2').val('');
				     }else{
				    	 alert('失败,请确认密码是否正确');
				     }
				   }
				  );
	   })
   })
</aui:script>
