<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
 <%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
 <%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet"%>
 <%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
 <%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
 <%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
 <portlet:defineObjects />
 <%@page import="java.io.Serializable"%>
 <%@page import="javax.portlet.PortletSession"%>
 <%@page import="com.liferay.portal.model.User"%>
 <%@page import="com.liferay.portal.model.Organization"%>
 <%@page import="com.liferay.portal.theme.ThemeDisplay"%>
 <%@ page language="java" contentType="text/html; charset=UTF-8"
     pageEncoding="UTF-8"%>
 <%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
 <%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
 <%@ page import="java.util.*"%>
 <%@ page import="javax.portlet.PortletPreferences"%>
 <%@ page import="java.text.DateFormat"%>
 <%@ page import="java.text.SimpleDateFormat"%>
 <%@ page
     import="com.liferay.portlet.expando.service.ExpandoValueServiceUtil"%>
 <%@ page
     import="com.liferay.portal.service.OrganizationLocalServiceUtil"%>
 <%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
 <%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
 <%@ page import="com.liferay.portal.kernel.language.LanguageUtil"%>
 <%@ page import="com.liferay.portal.kernel.util.ParamUtil"%>
 <%@ page import="com.liferay.portal.util.PortalUtil"%>
 <%@ page import="com.liferay.portal.kernel.language.LanguageWrapper"%>
 <%@ page import="com.liferay.portal.kernel.language.UnicodeLanguageUtil"%>
 <%@page import="com.liferay.portal.util.PortalUtil"%>
 <%@page import="javax.portlet.PortletURL"%>
 <%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
 <portlet:defineObjects />
 <%
 String namespace="_formPortlet_";
 ThemeDisplay themeDisplay = (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
 User user = themeDisplay.getUser();
 long userId=user.getUserId();

 HttpServletRequest r = PortalUtil.getHttpServletRequest(renderRequest);
 /* String langId=LanguageUtil.getLanguageId(request);
 String val =  PortalUtil.getOriginalServletRequest(r).getParameter("language");
 if(val !=null){
 	language =val;
 }else{
 boolean is_en_US=langId.equals("en_US");
 boolean is_zh_CN=langId.equals("zh_CN");

 if(is_zh_CN)
 	language="zh";
 } */
 String langId=LanguageUtil.getLanguageId(request);
 boolean is_en_US=langId.equals("en_US");
 boolean is_zh_CN=langId.equals("zh_CN");
 boolean is_ja_JP=langId.equals("ja_JP");

 %>
 <style>
      .content{
          text-align:left;
          
      }
      .body{
         width:80%;
         margin:auto;
         padding:20px;
         color:white;
      }
      .body p{
         font-size:18px;
         line-height:30px;
      }
 </style>
 <div style='background-color:#1d2752'>
 <div class='body'>
     <h1><liferay-ui:message key='termofuse'/></h1>
     <p style='text-align:left'><liferay-ui:message key='termofuseintro'/></p>
    <div id='part1' class='content'>
       <h2><liferay-ui:message key='termofusep1head'/></h2>
       <p><liferay-ui:message key='termofusep1content'/></p>
    </div>
    <div id='part2' class='content'>
       <h2><liferay-ui:message key='termofusep2head'/></h2>
       <p><liferay-ui:message key='termofusep2content'/></p>
    </div>
    <div id='part3' class='content'>
       <h2><liferay-ui:message key='termofusep3head'/></h2>
       <p><liferay-ui:message key='termofusep3content'/></p>
    </div>
    <div id='part4' class='content'>
       <h2><liferay-ui:message key='termofusep4head'/></h2>
       <p><liferay-ui:message key='termofusep4content'/></p>
    </div>
    <div id='part5' class='content'>
       <h2><liferay-ui:message key='termofusep5head'/></h2>
       <p><liferay-ui:message key='termofusep5content'/></p>
    </div>
    <div id='part6' class='content'>
       <h2><liferay-ui:message key='termofusep6head'/></h2>
       <p><liferay-ui:message key='termofusep6content'/></p>
    </div>
    <div id='part7' class='content'>
       <h2><liferay-ui:message key='termofusep7head'/></h2>
       <p><liferay-ui:message key='termofusep7content'/></p>
    </div>
    <div id='part8' class='content'>
       <h2><liferay-ui:message key='termofusep8head'/></h2>
       <p><liferay-ui:message key='termofusep8content'/></p>
    </div>
    <div id='part9' class='content'>
       <h2><liferay-ui:message key='termofusep9head'/></h2>
       <p><liferay-ui:message key='termofusep9content'/></p>
    </div>
    <div id='part10' class='content'>
       <h2><liferay-ui:message key='termofusep10head'/></h2>
       <p><liferay-ui:message key='termofusep10content'/></p>
    </div>
 </div>
 </div>
