 <%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
 <%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
 <%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet"%>
 <%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
 <%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
 <%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
 <portlet:defineObjects />
 <%@page import="java.io.Serializable"%>
 <%@page import="javax.portlet.PortletSession"%>
 <%@page import="com.liferay.portal.model.User"%>
 <%@page import="com.liferay.portal.model.Organization"%>
 <%@page import="com.liferay.portal.theme.ThemeDisplay"%>
 <%@ page language="java" contentType="text/html; charset=UTF-8"
     pageEncoding="UTF-8"%>
 <%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
 <%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
 <%@ page import="java.util.*"%>
 <%@ page import="javax.portlet.PortletPreferences"%>
 <%@ page import="java.text.DateFormat"%>
 <%@ page import="java.text.SimpleDateFormat"%>
 <%@ page
     import="com.liferay.portlet.expando.service.ExpandoValueServiceUtil"%>
 <%@ page
     import="com.liferay.portal.service.OrganizationLocalServiceUtil"%>
 <%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
 <%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
 <%@ page import="com.liferay.portal.kernel.language.LanguageUtil"%>
 <%@ page import="com.liferay.portal.kernel.util.ParamUtil"%>
 <%@ page import="com.liferay.portal.util.PortalUtil"%>
 <%@ page import="com.liferay.portal.kernel.language.LanguageWrapper"%>
 <%@ page import="com.liferay.portal.kernel.language.UnicodeLanguageUtil"%>
 <%@page import="com.liferay.portal.util.PortalUtil"%>
 <%@page import="javax.portlet.PortletURL"%>
 <%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
 <portlet:defineObjects />
 <%
 String namespace="OrderPortlet";
 ThemeDisplay themeDisplay = (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
 User user = themeDisplay.getUser();
 long userId=user.getUserId();

 String language = "en";
 HttpServletRequest r = PortalUtil.getHttpServletRequest(renderRequest);

 String langId=LanguageUtil.getLanguageId(request);
 boolean is_en_US=langId.equals("en_US");
 boolean is_zh_CN=langId.equals("zh_CN");
 boolean is_zh_TW=langId.equals("zh_TW");
 boolean is_ja_JP=langId.equals("ja_JP");

 %>
 <style>
 </style>
 <div style='width:100%'>
 <label><liferay-ui:message key='startdate'/></label>
 <input id='from' type='date'>
 <label><liferay-ui:message key='enddate'/></label>
 <input id='to' type='date'>
 <label><liferay-ui:message key='phase'/></label>
 <select id='phase'>
    <option value=1><liferay-ui:message key='firstphase'/></option>
    <option value=2><liferay-ui:message key='secondphase'/></option>
    <option value=3><liferay-ui:message key='thirdphase'/></option>
 </select>
 <label>状态<liferay-ui:message key='status'/></label>
 <select id='status'>
    <option data-value='{"pending":true,"paid":false}'>处理中(未付款)</option>
    <option data-value='{"pending":true,"paid":true}'>处理中(已付款)</option>
    <option data-value='{"pending":false,"paid":true}'>已完成</option>
 </select>
 <button class='btn' id='submit'>查找</button>
 <table class='table table-striped table-bordered dataTable no-footer'>
      <thead>
         <tr>
             <th>币种</th>
             <th>地区</th>
             <th>送币数量</th>
             <th>卖出数量</th>
             <th>卖出金额</th>
         </tr>
         </thead>
         <tbody id='showdata'>
         </tbody>
</table>
</div>
<aui:script use="array-extras, node, event">
     $(document).ready(function(){
    	 if ( $('[type="date"]').prop('type') != 'date' ) {
    		    $('[type="date"]').datepicker({ dateFormat: 'yy-mm-dd',changeYear: true,changeMonth:true,yearRange: '1900:2099' });
    		}
    	     var show;
    	     $('#submit').click(function(){
    	    	 $('#showdata').html('');
    		     Liferay.Service(
       	    		  '/AaPortal-portlet.coinorder/get-sale-summary',
       	    		  {
       	    		    isPending: $('#status option:selected').data('value').pending,
       	    		    phase: parseInt($('#phase option:selected').val()),
       	    		    startDate: $('#from').val().replace(/-/g,''),
       	    		    isPaid:$('#status option:selected').data('value').paid,
       	    		    endDate: $('#to').val().replace(/-/g,'')
       	    		  },
       	    		  function(obj) {
       	    			show = '';
       	    		    var data = obj.data;
       	    		    var currency = Object.keys(data);
       	    		    //console.log(currency);
       	    		    currency.map(function(info,index1){
       	    		    	  var region = Object.keys(data[info]);
       	    		    	   region.map(function(allinfo,index2){
       	    		    		     console.log(data[info][allinfo]);
       	    		    	         show += '<tr><td>'+info+'</td><td>'+region[index2]+'</td><td>'+data[info][allinfo].promocoin+'</td><td>'+data[info][allinfo].salecoin+'</td><td>'
       	    		    	         +data[info][allinfo].money+'</td></tr>';
       	    		    	       
       	    		    	  }) 
       	    		    })
       	    		    console.log(show.length);
       	    		    if(show.length==0){
       	    		    	show = '<tr><td colspan="5" style="text-align:center">未找到信息</td></tr>'
       	    		    }
       	    		    //console.log(show);
       	    		    $('#showdata').html(show);
       	    		  }
       	    		);
    	     })
    
     })
</aui:script>
 
 
