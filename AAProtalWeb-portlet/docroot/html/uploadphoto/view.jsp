 <%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
 <%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
 <%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet"%>
 <%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
 <%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
 <%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
 <portlet:defineObjects />
 <%@page import="java.io.Serializable"%>
 <%@page import="javax.portlet.PortletSession"%>
 <%@page import="com.liferay.portal.model.User"%>
 <%@page import="com.liferay.portal.model.Organization"%>
 <%@page import="com.liferay.portal.theme.ThemeDisplay"%>
 <%@ page language="java" contentType="text/html; charset=UTF-8"
     pageEncoding="UTF-8"%>
 <%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
 <%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
 <%@ page import="java.util.*"%>
 <%@ page import="javax.portlet.PortletPreferences"%>
 <%@ page import="java.text.DateFormat"%>
 <%@ page import="java.text.SimpleDateFormat"%>
 <%@ page
     import="com.liferay.portlet.expando.service.ExpandoValueServiceUtil"%>
 <%@ page
     import="com.liferay.portal.service.OrganizationLocalServiceUtil"%>
 <%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
 <%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
 <%@ page import="com.liferay.portal.kernel.language.LanguageUtil"%>
 <%@ page import="com.liferay.portal.kernel.util.ParamUtil"%>
 <%@ page import="com.liferay.portal.util.PortalUtil"%>
 <%@ page import="com.liferay.portal.kernel.language.LanguageWrapper"%>
 <%@ page import="com.liferay.portal.kernel.language.UnicodeLanguageUtil"%>
 <%@page import="com.liferay.portal.util.PortalUtil"%>
 <%@page import="javax.portlet.PortletURL"%>
 <%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
 <portlet:defineObjects />
 <%
 String namespace="OrderPortlet";
 ThemeDisplay themeDisplay = (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
 User user = themeDisplay.getUser();
 long userId=user.getUserId();

 String language = "en";
 HttpServletRequest r = PortalUtil.getHttpServletRequest(renderRequest);
 /* String langId=LanguageUtil.getLanguageId(request);
 String val =  PortalUtil.getOriginalServletRequest(r).getParameter("language");
 if(val !=null){
 	language =val;
 }else{
 boolean is_en_US=langId.equals("en_US");
 boolean is_zh_CN=langId.equals("zh_CN");

 if(is_zh_CN)
 	language="zh";
 } */
 String langId=LanguageUtil.getLanguageId(request);
 boolean is_en_US=langId.equals("en_US");
 boolean is_zh_CN=langId.equals("zh_CN");
 boolean is_zh_TW=langId.equals("zh_TW");
 boolean is_ja_JP=langId.equals("ja_JP");

 %>
 <div>
      <form class='form well form-search'>
            <input type='text' placeholder='UserId'/>
            <input  type='file' onchange="previewFile()" class='input input-medium search-query' id='photo' />
      </form>
      <button class='btn'><liferay-ui:message key='update'/></button><br/>
      <img id='preview' src='' style='width:500px;height:500px'/>
 </div>
 <aui:script use="array-extras, node, event">
 
 
 </aui:script>
 <script>
 function previewFile(){
	    var preview = document.getElementById('preview'); //selects the query named img
	    var file    = document.getElementById('photo').files[0]; //sames as here
	    var reader  = new FileReader();

	    reader.onloadend = function () {
	   	 $("#preview").attr("src",reader.result);
	   	 $('#previewimg').css('display','block');
	        //preview.src = reader.result;
	    }

	    if (file) {
	        //reader.readAsDataURL(file); //reads the data as a URL
	       reader.readAsDataURL(file);
	    } else {
	        preview.src = "";
	    }

	}
 </script>
