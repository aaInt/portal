<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
 <%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
 <%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet"%>
 <%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
 <%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
 <%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
 <portlet:defineObjects />
 <%@page import="java.io.Serializable"%>
 <%@page import="javax.portlet.PortletSession"%>
 <%@page import="com.liferay.portal.model.User"%>
 <%@page import="com.liferay.portal.model.Organization"%>
 <%@page import="com.liferay.portal.theme.ThemeDisplay"%>
 <%@ page language="java" contentType="text/html; charset=UTF-8"
     pageEncoding="UTF-8"%>
 <%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
 <%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
 <%@ page import="java.util.*"%>
 <%@ page import="javax.portlet.PortletPreferences"%>
 <%@ page import="java.text.DateFormat"%>
 <%@ page import="java.text.SimpleDateFormat"%>
 <%@ page
     import="com.liferay.portlet.expando.service.ExpandoValueServiceUtil"%>
 <%@ page
     import="com.liferay.portal.service.OrganizationLocalServiceUtil"%>
 <%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
 <%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
 <%@ page import="com.liferay.portal.kernel.language.LanguageUtil"%>
 <%@ page import="com.liferay.portal.kernel.util.ParamUtil"%>
 <%@ page import="com.liferay.portal.util.PortalUtil"%>
 <%@ page import="com.liferay.portal.kernel.language.LanguageWrapper"%>
 <%@ page import="com.liferay.portal.kernel.language.UnicodeLanguageUtil"%>
 <%@page import="com.liferay.portal.util.PortalUtil"%>
 <%@page import="javax.portlet.PortletURL"%>
 <%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
 <portlet:defineObjects />
 <%
 String namespace="_coinUserPortlet_";
 ThemeDisplay themeDisplay = (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
 User user = themeDisplay.getUser();
 long userId=user.getUserId();

 HttpServletRequest r = PortalUtil.getHttpServletRequest(renderRequest);
 /* String langId=LanguageUtil.getLanguageId(request);
 String val =  PortalUtil.getOriginalServletRequest(r).getParameter("language");
 if(val !=null){
 	language =val;
 }else{
 boolean is_en_US=langId.equals("en_US");
 boolean is_zh_CN=langId.equals("zh_CN");

 if(is_zh_CN)
 	language="zh";
 } */
 String langId=LanguageUtil.getLanguageId(request);
 boolean is_en_US=langId.equals("en_US");
 boolean is_zh_CN=langId.equals("zh_CN");
 boolean is_ja_JP=langId.equals("ja_JP");

 %>
 <style>
     #form{
        width:100%;
        padding:20px; 
        margin:auto;
        text-align:center;
    }
    #table{
        margin-top:20px;
        
        padding:20px; 
        text-align:center;
    }
    #ordercheck label{
       width:200px;
    }
    #contentform{
        width:40%;
        padding-top:100px;
        margin:auto;
        padding-bottom:100px;
    }
    #table{
       display:none;
    }
        @media only screen and (max-width: 800px) {
    
        #contentform{
        width:90%;
        padding-top:10px;
        margin:auto;
        padding-bottom:10px;
    
    }
    #form{
       width:auto;
    }
    #submit{
      margin:auto;
      margin-top:20px;
      width:50%;
    }
    }
}
 </style>
 <div id='ordercheck' style='min-height:800px;background-color:#1d2752;display:flex'>
   <div id='contentform'>
     <form id='form' class='form well form-search'>
        <label class="control-label" style='color:rgba(0,0,0,1)'><liferay-ui:message key='orderId1'/></label>
        <input type='text' id='orderid' class='check input input-medium search-query'/><br/>
        <label class="control-label" style='color:rgba(0,0,0,1)'><liferay-ui:message key='userId'/></label>
        <input type='text' id='userid' class='check input input-medium search-query'/><br/>
        <label class="control-label" style='color:rgba(0,0,0,1)'><liferay-ui:message key='lastname'/></label>
        <input type='text' id='lastname' class='check input input-medium search-query'/><br/>
        <button id='submit' class='btn btn-large btn-primary'><liferay-ui:message key='submitform'/></button>
     </form>

            <form id='table' class='form well form-search'>
                <label class="control-label" style='color:rgba(0,0,0,1)'><liferay-ui:message key='lastname'/></label>
                <input readonly='readonly' type='text' id='last' class='check input input-medium search-query'/><br/>
                <label class="control-label" style='color:rgba(0,0,0,1)'><liferay-ui:message key='birthday'/></label>
                <input readonly='readonly' type='text' id='birthday' class='check input input-medium search-query'/><br/>
                <label class="control-label" style='color:rgba(0,0,0,1)'><liferay-ui:message key='saleamt'/></label>
                <input readonly='readonly' type='text' id='buyamount' class='check input input-medium search-query'/><br/>
                <label class="control-label" style='color:rgba(0,0,0,1)'><liferay-ui:message key='promoAmount'/></label>
                <input readonly='readonly' type='text' id='freeamount' class='check input input-medium search-query'/><br/>
                <label class="control-label" style='color:rgba(0,0,0,1)'><liferay-ui:message key='saleregion'/></label>
                <input readonly='readonly' type='text' id='region' class='check input input-medium search-query'/><br/>
                <label class="control-label" style='color:rgba(0,0,0,1)'><liferay-ui:message key='salepeople'/></label>
                <input readonly='readonly' type='text' id='saleid' class='check input input-medium search-query'/>
            </form>
   </div>
 </div>
 <aui:script use="array-extras, node, event">
      function cleardata(){
    	  $('#orderid').val('');
		  $('#userid').val('');
		  $('#lastname').val(''); 
      }
      $('#submit').click(function(e){
    	  e.preventDefault();
    	   $('#last').val('');
	   	   $('#birthday').val('');
	   	   $('#buyamount').val('');
	   	   $('#freeamount').val('');
	   	   $('#region').val('');
	   	   $('#saleid').val('');
    	  if($('#orderid').val().trim()!=''&&$('#userid').val().trim()!=''&&$('#lastname').val().trim()!=''){
    		  Liferay.Service(
        			  '/AaPortal-portlet.coinorder/verify-order-info',
        			  {
        			    orderId: $('#orderid').val(),
        			    userId: $('#userid').val(),
        			    lastName: $('#lastname').val()
        			  },
        			  function(obj) {
        				  cleardata();
        			    if(obj.status.code=='00'){
        			    	   $('#last').val(obj.data.lastName);
        			    	   $('#birthday').val(obj.data.birthday);
        			    	   $('#buyamount').val(obj.data.purchaseAmount);
        			    	   $('#freeamount').val(obj.data.freeAmount);
        			    	   $('#region').val(obj.data.saleRegion);
        			    	   $('#saleid').val(obj.data.salesId);
        			    	   $('#table').show();
        			    }else{
        			    	$('#table').hide();
 
        			    	swal({
        			    		  title: "<liferay-ui:message key='infoerror'/>",
        			    		  text: "<liferay-ui:message key='checkinputinfo'/>",
        			    		  icon: "error",
        			    		});
        			    }
        			  }
        			 );
    	  }else{
    		  $('#table').hide();
    		  swal({
	    		  title: "<liferay-ui:message key='infoerror'/>",
	    		  text: "<liferay-ui:message key='resubmit'/>",
	    		  icon: "error",
	    		});
    	  }
    	
      })
 </aui:script>
