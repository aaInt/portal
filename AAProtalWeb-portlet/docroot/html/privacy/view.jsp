<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
 <%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
 <%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet"%>
 <%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
 <%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
 <%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
 <portlet:defineObjects />
 <%@page import="java.io.Serializable"%>
 <%@page import="javax.portlet.PortletSession"%>
 <%@page import="com.liferay.portal.model.User"%>
 <%@page import="com.liferay.portal.model.Organization"%>
 <%@page import="com.liferay.portal.theme.ThemeDisplay"%>
 <%@ page language="java" contentType="text/html; charset=UTF-8"
     pageEncoding="UTF-8"%>
 <%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
 <%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
 <%@ page import="java.util.*"%>
 <%@ page import="javax.portlet.PortletPreferences"%>
 <%@ page import="java.text.DateFormat"%>
 <%@ page import="java.text.SimpleDateFormat"%>
 <%@ page
     import="com.liferay.portlet.expando.service.ExpandoValueServiceUtil"%>
 <%@ page
     import="com.liferay.portal.service.OrganizationLocalServiceUtil"%>
 <%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
 <%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
 <%@ page import="com.liferay.portal.kernel.language.LanguageUtil"%>
 <%@ page import="com.liferay.portal.kernel.util.ParamUtil"%>
 <%@ page import="com.liferay.portal.util.PortalUtil"%>
 <%@ page import="com.liferay.portal.kernel.language.LanguageWrapper"%>
 <%@ page import="com.liferay.portal.kernel.language.UnicodeLanguageUtil"%>
 <%@page import="com.liferay.portal.util.PortalUtil"%>
 <%@page import="javax.portlet.PortletURL"%>
 <%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
 <portlet:defineObjects />
 <%
 String namespace="_formPortlet_";
 ThemeDisplay themeDisplay = (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
 User user = themeDisplay.getUser();
 long userId=user.getUserId();

 HttpServletRequest r = PortalUtil.getHttpServletRequest(renderRequest);
 /* String langId=LanguageUtil.getLanguageId(request);
 String val =  PortalUtil.getOriginalServletRequest(r).getParameter("language");
 if(val !=null){
 	language =val;
 }else{
 boolean is_en_US=langId.equals("en_US");
 boolean is_zh_CN=langId.equals("zh_CN");

 if(is_zh_CN)
 	language="zh";
 } */
 String langId=LanguageUtil.getLanguageId(request);
 boolean is_en_US=langId.equals("en_US");
 boolean is_zh_CN=langId.equals("zh_CN");
 boolean is_ja_JP=langId.equals("ja_JP");

 %>
 <style>
      .content{
          text-align:left;
          
      }
      .body{
         width:80%;
         margin:auto;
         padding:20px;
         color:white;
      }
      .body p{
         font-size:20px;
         line-height:30px;
      }
      .body li{
         font-size:18px;
      }
 </style>
 <div style='background-color:#1d2752'>
 <div class='body'>
     <h1><liferay-ui:message key='privacy'/></h1>
     <p style='text-align:left'><liferay-ui:message key='privacyintro'/></p>
     <p><liferay-ui:message key='maycontact'/></p>
     <ul>
        <li><liferay-ui:message key='maycontactinfo1'/></li>
        <li><liferay-ui:message key='maycontactinfo2'/></li>
     </ul>
     <div class='part'>
          <h1><liferay-ui:message key='privacycollect'/></h1>
          <h2><liferay-ui:message key='collect'/></h2>
          <ul>
            <li><liferay-ui:message key='collect1'/></li>
            <li><liferay-ui:message key='collect2'/></li>
            <li><liferay-ui:message key='collect3'/></li>
            <li><liferay-ui:message key='collect4'/></li>
            <li><liferay-ui:message key='collect5'/></li>
            <li><liferay-ui:message key='collect6'/></li>
            <li><liferay-ui:message key='collect7'/></li>
            <li><liferay-ui:message key='collect8'/></li>
          </ul>
     </div>
      <div class='part'>
          <h1><liferay-ui:message key='privacyshare'/></h1>
          <p><liferay-ui:message key='share1'/></p>
          <p><liferay-ui:message key='share2'/></p>
          <ul>
            <li><liferay-ui:message key='sharedetail1'/></li>
            <li><liferay-ui:message key='sharedetail2'/>
            </li>
            <li><liferay-ui:message key='sharedetail3'/>
               <ul>
                   <li><liferay-ui:message key='sharedetail31'/></li>
                   <li><liferay-ui:message key='sharedetail32'/></li>
               </ul>
            </li>
          </ul>
     </div>
     <div class='part'>
        <h1><liferay-ui:message key='Restricting'/></h1>
        <p><liferay-ui:message key='Restrictingcontent'/></p>
     </div>
     <div class='part'>
        <h1><liferay-ui:message key='Contact'/></h1>
        <p><liferay-ui:message key='contactcontent'/></p>
     </div>
 </div>
 </div>
