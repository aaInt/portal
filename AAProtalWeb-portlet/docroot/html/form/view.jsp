<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
 <%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
 <%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet"%>
 <%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
 <%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
 <%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
 <portlet:defineObjects />
 <%@page import="java.io.Serializable"%>
 <%@page import="javax.portlet.PortletSession"%>
 <%@page import="com.liferay.portal.model.User"%>
 <%@page import="com.liferay.portal.model.Organization"%>
 <%@page import="com.liferay.portal.theme.ThemeDisplay"%>
 <%@ page language="java" contentType="text/html; charset=UTF-8"
     pageEncoding="UTF-8"%>
 <%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
 <%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
 <%@ page import="java.util.*"%>
 <%@ page import="javax.portlet.PortletPreferences"%>
 <%@ page import="java.text.DateFormat"%>
 <%@ page import="java.text.SimpleDateFormat"%>
 <%@ page
     import="com.liferay.portlet.expando.service.ExpandoValueServiceUtil"%>
 <%@ page
     import="com.liferay.portal.service.OrganizationLocalServiceUtil"%>
 <%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
 <%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
 <%@ page import="com.liferay.portal.kernel.language.LanguageUtil"%>
 <%@ page import="com.liferay.portal.kernel.util.ParamUtil"%>
 <%@ page import="com.liferay.portal.util.PortalUtil"%>
 <%@ page import="com.liferay.portal.kernel.language.LanguageWrapper"%>
 <%@ page import="com.liferay.portal.kernel.language.UnicodeLanguageUtil"%>
 <%@page import="com.liferay.portal.util.PortalUtil"%>
 <%@page import="javax.portlet.PortletURL"%>
 <%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
 <portlet:defineObjects />
 <%
 String namespace="_formPortlet_";
 ThemeDisplay themeDisplay = (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
 User user = themeDisplay.getUser();
 long userId=user.getUserId();

 HttpServletRequest r = PortalUtil.getHttpServletRequest(renderRequest);
 /* String langId=LanguageUtil.getLanguageId(request);
 String val =  PortalUtil.getOriginalServletRequest(r).getParameter("language");
 if(val !=null){
 	language =val;
 }else{
 boolean is_en_US=langId.equals("en_US");
 boolean is_zh_CN=langId.equals("zh_CN");

 if(is_zh_CN)
 	language="zh";
 } */
 String langId=LanguageUtil.getLanguageId(request);
 boolean is_en_US=langId.equals("en_US");
 boolean is_zh_CN=langId.equals("zh_CN");
 boolean is_ja_JP=langId.equals("ja_JP");

 %>
 <style>
     #userform{
        width:100%;
        padding:10px; 
        margin:auto;
        text-align:center;
        
    } 
    #contentform input{
        font-size:1em;
    }
    .input{
       margin:10px !important;
    }
    .span{
       color:red;
    }
    .aui label{
       width:120px;
    }
    #submit{
        margin-top:10px;
    }
    #contentform{
        width:40%;
        padding-top:100px;
        margin:auto;
        padding-bottom:100px;
    }

    #country{
       font-size:1em;
       width:180px;
      border-radius:15px;
    }
    #code{
       font-size:1em;
       width:180px;
      border-radius:15px;
    }
    @media only screen and (max-width: 980px) {
    
        #contentform{
        width:90%;
        padding-top:10px;
        margin:auto;
        padding-bottom:10px;
    
    }
    #submit{
      margin:auto;
      margin-top:20px;
      width:50%;
    }
     #userform{
       width:auto;
    } 
}
 </style>
  <script data-require="jquery@3.1.1" data-semver="3.1.1" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
 <div id='contentform'>
     <form class='form well form-search' id='userform'>
     
          <label class="control-label" style='color:rgba(0,0,0,1)'><liferay-ui:message key='firstname'/></label>
          
          <input type='text' class='check input input-medium search-query' id='first' /><br/>
          <label class="control-label" style='color:rgba(0,0,0,1)'><liferay-ui:message key='lastname'/></label>
          <input  type='text' class='check input input-medium search-query' id='last' /><br/>
         
          <label class="control-label" style='color:rgba(0,0,0,1)'><liferay-ui:message key='email'/></label>
         
          <input  type='text' class='check input input-medium search-query' id='email'/><br/>
          
            <label class="control-label" style='color:rgba(0,0,0,1)'><liferay-ui:message key='areacode'/></label>
          
          <select id='code' class="selectpicker">
             <option value='1'>1</option>
             <option value='86'>86</option>
             <option value='81'>81</option>
             <option value='852'>852</option>
          </select><br/>
          
          <label class="control-label" style='color:rgba(0,0,0,1)'><liferay-ui:message key='phone'/></label>
         
          <input type='text' onkeydown='return (event.which >= 48 && event.which <= 57) 
   || event.which == 8 || (event.which >= 96 && event.which <= 105)' class='check input input-medium search-query' id='number'/><br/>
          
          
          <label class="control-label" style='color:rgba(0,0,0,1)'><liferay-ui:message key='country'/></label>
          <select id='country' class="selectpicker">
             <option value='US'><liferay-ui:message key='US'/></option>
             <option value='CN'><liferay-ui:message key='CN'/></option>
             <option value='JP'><liferay-ui:message key='JP'/></option>
             <option value='HK'><liferay-ui:message key='HK'/></option>
          </select><br/>
          
          <button id='submit' class='btn btn-large btn-primary'><liferay-ui:message key='submitform'/></button>
     </form>
 </div>
 <script>
        $(document).ready(function(){
        	$('.check').keyup(function(e){
        		if(e.target.value!=''){
        			$(this).css('border-color','#DDD');
        		}else{
        			$(this).css('border-color','red');
        		}
        	})
        	$('#submit').click(function(e){
        		e.preventDefault();
        		var first = $('#first').val().trim();
        		var last = $('#last').val().trim();
        		var email = $('#email').val().trim();
        		var number = $('#number').val().trim();
        		var code = $('#code option:selected').val();
        		var country = $('#country option:selected').val();
        		if(first!=''&&last!=''&&email!=''&&number!=''){
        			Liferay.Service(
        					  '/AaPortal-portlet.futureuser/record-user',
        					  {
        					    email: email,
        					    firstN: first,
        					    lastN: last,
        					    residency: country,
        					    areaCode: code,
        					    phone: number,
        					  },
        					  function(obj) {
        						  swal({
									  title:$('<p><liferay-ui:message key="success"/></p>').html(),
									  text:'<liferay-ui:message key="success"/>',
									  type:"success",
									  showConfirmButton:true
									}); 
								//alert(JSON.stringify(obj));
        		        		$('#first').val('');
        		        		$('#last').val('');
        		        		$('#email').val('');
        		        		$('#number').val('');
        		        		$('#first').css('border-color','#DDD');
        		        		$('#last').css('border-color','#DDD');
        		        		$('#email').css('border-color','#DDD');
        		        		$('#number').css('border-color','#DDD');
        					    
        					  }
        					);
        		}else{
        			if(first==''){
        				$('#first').css('border-color','red');
        			}else{
        				$('#first').css('border-color','#DDD');
        			}
        			if(last==''){
        				$('#last').css('border-color','red');
        			}else{
        				$('#last').css('border-color','#DDD');
        			}
        			if(email==''){
        				$('#email').css('border-color','red');
        			}else{
        				$('#email').css('border-color','#DDD');
        			}
        			if(number==''){
        				$('#number').css('border-color','red');
        			}else{
        				$('#number').css('border-color','#DDD');
        			}
        		
        			
        		}
        	})
        })
 </script>