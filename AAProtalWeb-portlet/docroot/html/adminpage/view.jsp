 <%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
 <%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
 <%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet"%>
 <%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
 <%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
 <%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
 <portlet:defineObjects />
 <%@page import="java.io.Serializable"%>
 <%@page import="javax.portlet.PortletSession"%>
 <%@page import="com.liferay.portal.model.User"%>
 <%@page import="com.liferay.portal.model.Organization"%>
 <%@page import="com.liferay.portal.theme.ThemeDisplay"%>
 <%@ page language="java" contentType="text/html; charset=UTF-8"
     pageEncoding="UTF-8"%>
 <%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
 <%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
 <%@ page import="java.util.*"%>
 <%@ page import="javax.portlet.PortletPreferences"%>
 <%@ page import="java.text.DateFormat"%>
 <%@ page import="java.text.SimpleDateFormat"%>
 <%@ page
     import="com.liferay.portlet.expando.service.ExpandoValueServiceUtil"%>
 <%@ page
     import="com.liferay.portal.service.OrganizationLocalServiceUtil"%>
 <%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
 <%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
 <%@ page import="com.liferay.portal.kernel.language.LanguageUtil"%>
 <%@ page import="com.liferay.portal.kernel.util.ParamUtil"%>
 <%@ page import="com.liferay.portal.util.PortalUtil"%>
 <%@ page import="com.liferay.portal.kernel.language.LanguageWrapper"%>
 <%@ page import="com.liferay.portal.kernel.language.UnicodeLanguageUtil"%>
 <%@page import="com.liferay.portal.util.PortalUtil"%>
 <%@page import="javax.portlet.PortletURL"%>
 <%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
 <portlet:defineObjects />
 <%
 String namespace="OrderPortlet";
 ThemeDisplay themeDisplay = (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
 User user = themeDisplay.getUser();
 long userId=user.getUserId();

 String language = "en";
 HttpServletRequest r = PortalUtil.getHttpServletRequest(renderRequest);
 /* String langId=LanguageUtil.getLanguageId(request);
 String val =  PortalUtil.getOriginalServletRequest(r).getParameter("language");
 if(val !=null){
 	language =val;
 }else{
 boolean is_en_US=langId.equals("en_US");
 boolean is_zh_CN=langId.equals("zh_CN");

 if(is_zh_CN)
 	language="zh";
 } */
 String langId=LanguageUtil.getLanguageId(request);
 boolean is_en_US=langId.equals("en_US");
 boolean is_zh_CN=langId.equals("zh_CN");
 boolean is_zh_TW=langId.equals("zh_TW");
 boolean is_ja_JP=langId.equals("ja_JP");

 %>
 <style>
    .control-label{
       width:150px;
    }
 </style>
 <div style='text-align:center'>
      <h2><liferay-ui:message key='deactiveuser'/></h2>
      <form class='form well form-search'>
          <input placeholder='userId' id='userid'/>
      </form>
      <button id='deleteuser' class='btn btn-primary'><liferay-ui:message key='submit'/></button><br>
      <div style='border-top:solid 1px'>
        <h2><liferay-ui:message key='splitorder'/></h2>
      <form class='form well form-search'>
          <input placeholder='orderId' id='splitorderid'/>
          <input placeholder='salesId' id='splitsalesid'/>
          <input placeholder='userId' id='splituserid'/>
          <input placeholder='amount' id='splitmoney'/>
      </form>
      <button id='splitorder' class='btn btn-primary'><liferay-ui:message key='submit'/></button><br>
      </div>
      <div style='border-top:solid 1px'>
         <h2><liferay-ui:message key='stafforder'/></h2>
      <table id='staff' style="width:100%;" class='table table-striped table-bordered dataTable no-footer'>
         <thead>
            <tr>
               <th><liferay-ui:message key='name'/></th>
               <th><liferay-ui:message key='email'/></th>
               <th><liferay-ui:message key='userId'/></th>
               <th><liferay-ui:message key='orderId'/></th>
               <th><liferay-ui:message key='verifyFlag'/></th>
               <th><liferay-ui:message key='docSentFlag'/></th>
               <th><liferay-ui:message key='docSignFlag'/></th>
               <th><liferay-ui:message key='paidFlag'/></th>
               <th><liferay-ui:message key='approvalFlag'/></th>
               <th><liferay-ui:message key='certSentFlag'/></th>
               <th><liferay-ui:message key='completeFlag'/></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th><liferay-ui:message key='promoamt'/></th>
               <th><liferay-ui:message key='saleamt'/></th>
               <th><liferay-ui:message key='salemoney'/></th>
               <th></th>
               <th></th>
               <th></th>
            </tr>
         </thead>
         <tbody>
         </tbody>
     </table>
      <div id='detail'>
         <div id='info'>
         </div>
     </div>
      </div>
 </div>
  <aui:script use="array-extras, node, event">
  var codeerror = {
			'00':'成功',
			'010':'无权访问',
			'011':'无权操作该用户/订单',
			'020':'邮箱已存在',
			'040':'系统错误，请联系IT部门',
			'050':'代理人信息错误，请确认后重新提交',
			'051':'用户类型错误，请输入正确类型 ',
			'052':'用户信息有误，请确认后重新提交',
			'053':'联系方式或地址有误，请确认后重新提交',
			'054':'基金信息有误，请确认后重新提交',
			'055':'错误的图片格式，请确认后重新提交',
			'056':'错误的失败原因代码，请确认后重新提交',
			'057':'错误的销售期或价格， 请确认后重新提交',
			'060':'最多只能处理3名潜在用户，请完成已有任务再来接新任务，谢谢',
			'061':'剩余的发行量不足， 请减少购买数量',
			'080':'订单还未确认， 请确认后再操作',
			'081':'订单合同还未发出， 请确认发出后再操作',
			'082':'订单还未付款，请确认付款后再做操作',
			'083':'订单合同还未签字，请确认用户签字后再做操作',
			'084':'订单未批准，请联系管理人员进行审核和批准再做操作',
			'085':'订单证书还未寄出，请确认寄出后再操作',
			'086':'订单处于未激活状态，无法进行操作',
			'090':'用户还未验证，请验证后再进行操作',
			'091':'代理人未验证，请验证后再操作',
			'092':'基金未验证， 请验证后再操作'
		};
  var translation = {
			firstname:"<liferay-ui:message key='firstname'/>",
			lastname:"<liferay-ui:message key='lastname'/>",
			firstName:"<liferay-ui:message key='firstname'/>",
			lastName:"<liferay-ui:message key='lastname'/>",
			birthday:"<liferay-ui:message key='birthday'/>",
			idType:"<liferay-ui:message key='idType'/>",
			asset:"<liferay-ui:message key='asset'/>",
			idIssueCountry:"<liferay-ui:message key='idIssueCountry'/>",
			addUser:"<liferay-ui:message key='addUser'/>",
			brokerId:"<liferay-ui:message key='brokerId'/>",
			userTypee:"<liferay-ui:message key='userType'/>",
			nationality:"<liferay-ui:message key='nationality'/>",
			fundId:"<liferay-ui:message key='fundId'/>",
			activeFlag:"<liferay-ui:message key='activeFlag'/>",
			income:"<liferay-ui:message key='income'/>",
			userId:"<liferay-ui:message key='userId'/>",
			isValidated:"<liferay-ui:message key='isValidated'/>",
			photoImage:"<liferay-ui:message key='photoImage'/>",
			wechat:"<liferay-ui:message key='wechat'/>",
			areacode:"<liferay-ui:message key='areacode'/>",
			phone:"<liferay-ui:message key='phone'/>",
			email:"<liferay-ui:message key='email'/>",
			idNumber:"<liferay-ui:message key='idNumber'/>",
			facebook:"<liferay-ui:message key='facebook'/>",
			weibo:"<liferay-ui:message key='weibo'/>",
			twitter:"<liferay-ui:message key='twitter'/>",
			str1:"<liferay-ui:message key='str1'/>",
			str2:"<liferay-ui:message key='str2'/>",
			city:"<liferay-ui:message key='city'/>",
			state:"<liferay-ui:message key='state'/>",
			country:"<liferay-ui:message key='country'/>",
			zipcode:"<liferay-ui:message key='zipcode'/>",
			licenseId:"<liferay-ui:message key='licenseId'/>",
			licenseCountry:"<liferay-ui:message key='licenseCountry'/>",
			addTime:"<liferay-ui:message key='addTime'/>",
			docCarrier:"<liferay-ui:message key='docCarrier'/>",
			docTrackingNo:"<liferay-ui:message key='docTrackingNo'/>",
			promoAmount:"<liferay-ui:message key='promoamt'/>",
			saleAmount:"<liferay-ui:message key='saleamt'/>",
			salePhase:"<liferay-ui:message key='phase'/>",
			salesId:"<liferay-ui:message key='salesId'/>",
			totalAmount:"<liferay-ui:message key='totalAmount'/>",
			totalPrice:"<liferay-ui:message key='totalPrice'/>",
			unitPrice:"<liferay-ui:message key='unitPrice'/>",
			currency:"<liferay-ui:message key='currency'/>",
			activeFlag:"<liferay-ui:message key='activeFlag'/>",
			approvalFlag:"<liferay-ui:message key='approvalFlag'/>",
			certCarrier:"<liferay-ui:message key='certCarrier'/>",
			certTrackingNo:"<liferay-ui:message key='certTrackingNo'/>",
			certSentFlag:"<liferay-ui:message key='certSentFlag'/>",
			completeFlag:"<liferay-ui:message key='completeFlag'/>",
			orderId:"<liferay-ui:message key='orderId'/>",
			rsnCd:"<liferay-ui:message key='rsnCd'/>",
			docSentFlag:"<liferay-ui:message key='docSentFlag'/>",
			docSignFlag:"<liferay-ui:message key='docSignFlag'/>",
			userType:"<liferay-ui:message key='userType'/>",
			licenseId:"<liferay-ui:message key='licenseId'/>",
			licenseCountry:"<liferay-ui:message key='licenseCountry'/>",
			addUser:"<liferay-ui:message key='addUser'/>",
			paidFlag:"<liferay-ui:message key='paidFlag'/>",
			caseCloser:"<liferay-ui:message key='caseCloser'/>",
			verifyFlag:"<liferay-ui:message key='verifyFlag'/>",
			failFlag:"<liferay-ui:message key='failFlag'/>",
			ssn:"<liferay-ui:message key='ssn'/>",
			ein:"<liferay-ui:message key='ein'/>",
			isCreddited:"<liferay-ui:message key='isCreddited'/>"
			
	}
  $(document).ready(function(){
	  $('#deleteuser').click(function(){
		  Liferay.Service(
	    		  '/AaPortal-portlet.coinuser/deactivate-coin-user',
	    		  {
	    		    coinUserId: $('#userid').val().trim()
	    		  },
	    		  function(obj) {
	    			  if(obj.status.code==='00'){
	    				  $('#userid').val('');
	    				  alert('<liferay-ui:message key="success"/>');
					    }else{
					    	var code = obj.status.code.toString();
							   console.log(codeerror)
							   alert(codeerror[code]);
					    }
	    		  }
	    		);
	  })
	 $('#splitorder').click(function(){
		 Liferay.Service(
				  '/AaPortal-portlet.coinorder/split-order',
				  {
				    originOrderId: $('#splitorderid').val().trim(),
				    salesId: $('#splitsalesid').val().trim(),
				    userId: $('#splituserid').val().trim(),
				    amount: $('#splitmoney').val().trim()
				  },
				  function(obj) {
					  if(obj.status.code==='00'){
	    				  $('#splitorderid').val('');
	    				  $('#splitsalesid').val('');
	    				  $('#splituserid').val('');
	    				  $('#splitmoney').val('');
	    				  alert('<liferay-ui:message key="success"/>');
					    }else{
					    	var code = obj.status.code.toString();
							   console.log(codeerror)
							   alert(codeerror[code]);
					    }
				  }
				);
	 })    
		  
	  
	  function getneedinfo(data){
 	     var obj = {'name':data.firstName+' '+data.lastName,'email':data.email,'userId':data.userId,'orderId':data.orderId,'verifyFlag':data.verifyFlag,
 	    		 'docSentFlag':data.docSentFlag,'docSignFlag':data.docSignFlag,'paidFlag':data.paidFlag,
 	    		 'approvalFlag':data.approvalFlag,'certSentFlag':data.certSentFlag,'completeFlag':data.completeFlag,
 	    		 'doccarrier':data.docCarrier,'doctracking':data.docTrackingNo,'certcarrier':data.certCarrier,
 	    		 'certtracking':data.certTrackingNo,'promoAmount':data.promoAmount.toString(),'saleAmount':data.saleAmount.toString(),'saleMoney':data.totalPrice.toString()+' '+data.currency};
 	     return obj
    }
      (function getcompleted(){
   	   var datainfo = [];
			 var createtableinfo = [];
			 
			 Liferay.Service(
					  '/AaPortal-portlet.coinorder/get-staff-orders',
					  function(obj) {
					    if(obj.status.code==='00'){
					    	var allinfo = obj.data;
					    	obj.data.map(function(info){
					    		datainfo.push(getneedinfo(info));
					    		createtableinfo.push(Object.values(getneedinfo(info)));
					    	})
					    }else{
					    	var code = obj.status.code.toString();
							   console.log(codeerror)
							   alert(codeerror[code]);
					    }
					    createtablestaff(createtableinfo,allinfo);
					  }
					);
      })();
	  function createtablestaff(data,allinfo){
		   if ( $.fn.DataTable.isDataTable('#staff') ) {
				
				  $('#staff').DataTable().destroy();
				  
				  
				}
			var table1 = $('#staff').DataTable({
				
		    	"iDisplayLength": 25,
		         "responsive": true,
		         "destroy": true,
		        "autoFill": false,
		        "data":data,
		        "columnDefs":[
		            {
	 	              "targets": -3,
	 	              "data": null,
	 	              "defaultContent": "<div class='check'><button>查看</button></div>",
	 	            },
	 	          {
	   	              "targets": -2,
	   	              "data": null,
	   	              "defaultContent": "<div class='userinfo'><button>用户信息</button></div>",
	   	            },
	   	            {
	     	              "targets": -1,
	     	              "data": null,
	     	              "defaultContent": "<div class='userphoto'><button>查看照片</button></div>",
	     	            },

		            {
		                "targets": [-9,-8,-10,-7],
		                "visible": false,
		                "searchable": false,
		              }
		            ],
		            "oLanguage" : {
			                "sLengthMenu": "<liferay-ui:message key='LengthMenu'/>",
			                "sZeroRecords": "<liferay-ui:message key='ZeroRecords'/>",
			                "sInfo": "<liferay-ui:message key='Info'/>",
			                "sInfoEmpty": "<liferay-ui:message key='InfoEmpty'/>",
			                "sInfoFiltered": "<liferay-ui:message key='InfoFiltered'/>",
			                 "sSearch": "<liferay-ui:message key='Search'/>",
			                "oPaginate": {
			                "sFirst": "<liferay-ui:message key='First'/>",
			                "sPrevious": "<liferay-ui:message key='Previous'/>",
			                "sNext": "<liferay-ui:message key='Next'/>",
			                "sLast": "<liferay-ui:message key='Last'/>"
			                 }
			                 }
		            /* "createdRow": function (row, data, dataIndex) {
		         	   if(data[14] == "Y"){
		             	   $(row).find('button:first').text('Create Order');
		                }else{
		                	$(row).find('button:first').text('Verify');
		                }
		            }  */
				
			})

			  $('#staff tbody').on( 'click', '.check', function (event) {
					var newindex=table1.row($(this).closest('tr')).index();
			        if(newindex==undefined){
			       	 newindex=table1.row($(this).closest('tr').prev()).index();
			        }
			    
			        window.index=newindex;
			        $('#detail').dialog({
			        	title: '订单信息',
			            modal: true,
			            width: '500px',
			            resizable:true,
			            draggable:true,
			            height:500,
			            maxHeight: 800,
			            position: { my: "center", at: "center", of: window },
			            open:function(){
			                for(var i=0;i<allinfo.length;i++){
		        	             if(allinfo[i].orderId==data[newindex][3]){
		        	            	    var keys = Object.keys(allinfo[i]).sort();
		                    			var values = Object.values(allinfo[i]);
		                    			var info='';
		                    			keys.map(function(data,index){
		                    				
		                    					info +="<form class='form well form-search'><label class='control-label formlabel'>"+translation[data]+"</label>"
	 	                    				  +"<input type='text' class='input input-medium search-query' readonly='readonly' value='"+allinfo[i][data]+"'></br>"
		                    				
		                    				  
		        		            })
		        		            $('#info').html(info);
		        	
		        			}
		        		      }
			            }
			        })
			  })
			    $('#staff tbody').on( 'click', '.userphoto', function (event) {
					var newindex=table1.row($(this).closest('tr')).index();
			        if(newindex==undefined){
			       	 newindex=table1.row($(this).closest('tr').prev()).index();
			        }
			
			        window.index=newindex;
			        $('#detail').dialog({
			        	title: '用户照片',
			            modal: true,
			            width: '500px',
			            resizable:true,
			            draggable:true,
			            height:500,
			            maxHeight: 800,
			            position: { my: "center", at: "center", of: window },
			            open:function(){
			            	var info = '加载中。。。'
			            	$('#info').html(info);
			            	Liferay.Service(
			            			  '/AaPortal-portlet.coinuser/get-image',
			            			  {
			            			    userId: data[newindex][2]
			            			  },
			            			  function(obj) {
			            				  if(Object.values(obj).length==0){
			            					  info = '未上传照片'
			            				  }else{
			            					  info = '<img id="newpreview" src="data:image/png;base64,'+obj+'"/></br>'
			            				  }
			            	
			            			    if(data[newindex][16]==="N"){
			            			    	info += '<input id="uploadimg" type="file" onchange="previewFile1()" class="input input-medium search-query" id="uploadphoto" />'
			            			    }
			            			    $('#info').html(info);
			            			  }
			            			);
			                
			            }
			        })
			  })
			  	  $('#staff tbody').on( 'click', '.userinfo', function (event) {
					var newindex=table1.row($(this).closest('tr')).index();
			        if(newindex==undefined){
			       	 newindex=table1.row($(this).closest('tr').prev()).index();
			        }
			
			        window.index=newindex;
			        $('#detail').dialog({
			        	title: '用户信息',
			            modal: true,
			            width: '500px',
			            resizable:true,
			            draggable:true,
			            height:500,
			            maxHeight: 800,
			            position: { my: "center", at: "center", of: window },
			            open:function(){
			            	$('#info').html('');
			            	Liferay.Service(
			            			  '/AaPortal-portlet.coinuser/get-coin-user',
			            			  {
			            			    coinUserId: data[newindex][2]
			            			  },
			            			  function(obj) {
			            				 var info = obj.data;
			            				 var show = '';
			            				  newinfo = {'firstname':info.firstName,'lastname':info.lastName,'birthday':info.birthday,'idType':info.idType,
			    				    			  'asset':info.asset,'idIssueCountry':info.idIssueCountry,'addUser':info.addUser,'brokerId':info.brokerId,
			    				    			  'userType':info.userType,'licenseId':info.licenseId,'licenseCountry':info.licenseCountry,'nationality':info.nationality,
			    				    			  'ssn':info.ssn,'ein':info.ein,'fundId':info.fundId,'income':info.income,'userId':info.userId,
			    				    			  'isValidated':info.isValidated,'wechat':info.contact.wechat,'areacode':info.contact.areaCode,
			    				    			  'phone':info.contact.phone,'email':info.contact.email,'idNumber':info.idNumber,
			    				    			  'str1':info.contact.street1,'str2':info.contact.street2,'city':info.contact.city,'state':info.contact.state,'country':info.contact.country,'zipcode':info.contact.zipCode,
			    				    			  };
			            				  var keys = Object.keys(newinfo);
			            				  var value = Object.values(newinfo);
			            				  keys.map(function(data,index){
			            					  show +="<form class='form well form-search'><label class='control-label'>"+translation[data]+"</label>"
	                       				  +"<input type='text' class='input input-medium search-query' readonly='readonly' value='"+value[index]+"'></br>"
			            				  })
			            				  $('#info').html(show);
			            			  }
			            			);
			            }
			        })
			  })
		
	  }
  })

  </aui:script>
