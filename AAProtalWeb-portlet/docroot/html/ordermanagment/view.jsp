
 <%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
 <%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
 <%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet"%>
 <%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
 <%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
 <%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
 <portlet:defineObjects />
 <%@page import="java.io.Serializable"%>
 <%@page import="javax.portlet.PortletSession"%>
 <%@page import="com.liferay.portal.model.User"%>
 <%@page import="com.liferay.portal.model.Organization"%>
 <%@page import="com.liferay.portal.theme.ThemeDisplay"%>
 <%@ page language="java" contentType="text/html; charset=UTF-8"
     pageEncoding="UTF-8"%>
 <%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
 <%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
 <%@ page import="java.util.*"%>
 <%@ page import="javax.portlet.PortletPreferences"%>
 <%@ page import="java.text.DateFormat"%>
 <%@ page import="java.text.SimpleDateFormat"%>
 <%@ page
     import="com.liferay.portlet.expando.service.ExpandoValueServiceUtil"%>
 <%@ page
     import="com.liferay.portal.service.OrganizationLocalServiceUtil"%>
 <%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
 <%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
 <%@ page import="com.liferay.portal.kernel.language.LanguageUtil"%>
 <%@ page import="com.liferay.portal.kernel.util.ParamUtil"%>
 <%@ page import="com.liferay.portal.util.PortalUtil"%>
 <%@ page import="com.liferay.portal.kernel.language.LanguageWrapper"%>
 <%@ page import="com.liferay.portal.kernel.language.UnicodeLanguageUtil"%>
 <%@page import="com.liferay.portal.util.PortalUtil"%>
 <%@page import="javax.portlet.PortletURL"%>
 <%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
 <portlet:defineObjects />
 <%
 String namespace="OrderPortlet";
 ThemeDisplay themeDisplay = (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
 User user = themeDisplay.getUser();
 long userId=user.getUserId();

 String language = "en";
 HttpServletRequest r = PortalUtil.getHttpServletRequest(renderRequest);
 /* String langId=LanguageUtil.getLanguageId(request);
 String val =  PortalUtil.getOriginalServletRequest(r).getParameter("language");
 if(val !=null){
 	language =val;
 }else{
 boolean is_en_US=langId.equals("en_US");
 boolean is_zh_CN=langId.equals("zh_CN");

 if(is_zh_CN)
 	language="zh";
 } */
 String langId=LanguageUtil.getLanguageId(request);
 boolean is_en_US=langId.equals("en_US");
 boolean is_zh_CN=langId.equals("zh_CN");
 boolean is_zh_TW=langId.equals("zh_TW");
 boolean is_ja_JP=langId.equals("ja_JP");

 %>
 <style>
     .formlabel{
        width:100px;
     }
   
     
     #docpreview{
        zoom:2;
     }
 </style>
 <button id="refresh" style='color:red'>自动刷新</button>
 <select id="selectphase">
    <option value='all'>所有期</option>
    <option value='1'>第一期</option>
    <option value='2'>第二期</option>
    <option value='3'>第三期</option>
    <option value='99'>ICO</option>
 </select>
 <div>
     <table id='orderinfo' style="width:100%" class='table table-striped table-bordered dataTable no-footer'>
         <thead>
            <tr>
               <th><liferay-ui:message key='name'/></th>
               <th><liferay-ui:message key='email'/></th>
               <th><liferay-ui:message key='userId'/></th>
               <th><liferay-ui:message key='orderId'/></th>
               <th><liferay-ui:message key='verifyFlag'/></th>
               <th><liferay-ui:message key='docSentFlag'/></th>
               <th><liferay-ui:message key='docSignFlag'/></th>
               <th><liferay-ui:message key='paidFlag'/></th>
               <th><liferay-ui:message key='approvalFlag'/></th>
               <th><liferay-ui:message key='certSentFlag'/></th>
               <th><liferay-ui:message key='completeFlag'/></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th><liferay-ui:message key='saleamt'/></th>
               <th><liferay-ui:message key='salemoney'/></th>
               <th><liferay-ui:message key='phase'/></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <!-- <th></th> -->
            </tr>
         </thead>
         <tbody id='orderdetail'>
         </tbody>
     </table>
     <div id='detail'>
         <div id='info'>
         </div>
     </div>
     <div id='docdetail'>
         <div id='docinfo'>
         </div>
     </div>
 </div>
 <aui:script use="array-extras, node, event">
 var codeerror = {
			'00':'成功',
			'010':'无权访问',
			'011':'无权操作该用户/订单',
			'020':'邮箱已存在',
			'040':'系统错误，请联系IT部门',
			'050':'代理人信息错误，请确认后重新提交',
			'051':'用户类型错误，请输入正确类型 ',
			'052':'用户信息有误，请确认后重新提交',
			'053':'联系方式或地址有误，请确认后重新提交',
			'054':'基金信息有误，请确认后重新提交',
			'055':'错误的图片格式，请确认后重新提交',
			'056':'错误的失败原因代码，请确认后重新提交',
			'057':'错误的销售期或价格， 请确认后重新提交',
			'060':'最多只能处理3名潜在用户，请完成已有任务再来接新任务，谢谢',
			'061':'剩余的发行量不足， 请减少购买数量',
			'080':'订单还未确认， 请确认后再操作',
			'081':'订单合同还未发出， 请确认发出后再操作',
			'082':'订单还未付款，请确认付款后再做操作',
			'083':'订单合同还未签字，请确认用户签字后再做操作',
			'084':'订单未批准，请联系管理人员进行审核和批准再做操作',
			'085':'订单证书还未寄出，请确认寄出后再操作',
			'086':'订单处于未激活状态，无法进行操作',
			'090':'用户还未验证，请验证后再进行操作',
			'091':'代理人未验证，请验证后再操作',
			'092':'基金未验证， 请验证后再操作'
		};
 var refresh = true;
 settimeout = setTimeout("location.reload(true);", 31000);
$(document).ready(function(){
	 $('#refresh').click(function(){
		 	refresh = !refresh;
		 	console.log(refresh);
		 	clearTimeout(settimeout);
		 	if(refresh){
		 		console.log('restart');
		 		$('#refresh').css('color','red');
		 		settimeout = setTimeout("location.reload(true);", 31000);
		 	}else{
		 		$('#refresh').css('color','black');
		 		console.log('stop');
		 		
		 	}
		 	
		 	
		 });

})
 var translation = {
			firstname:"<liferay-ui:message key='firstname'/>",
			lastname:"<liferay-ui:message key='lastname'/>",
			firstName:"<liferay-ui:message key='firstname'/>",
			lastName:"<liferay-ui:message key='lastname'/>",
			birthday:"<liferay-ui:message key='birthday'/>",
			idType:"<liferay-ui:message key='idType'/>",
			asset:"<liferay-ui:message key='asset'/>",
			idIssueCountry:"<liferay-ui:message key='idIssueCountry'/>",
			addUser:"<liferay-ui:message key='addUser'/>",
			brokerId:"<liferay-ui:message key='brokerId'/>",
			userTypee:"<liferay-ui:message key='userType'/>",
			nationality:"<liferay-ui:message key='nationality'/>",
			fundId:"<liferay-ui:message key='fundId'/>",
			activeFlag:"<liferay-ui:message key='activeFlag'/>",
			income:"<liferay-ui:message key='income'/>",
			userId:"<liferay-ui:message key='userId'/>",
			isValidated:"<liferay-ui:message key='isValidated'/>",
			photoImage:"<liferay-ui:message key='photoImage'/>",
			wechat:"<liferay-ui:message key='wechat'/>",
			areacode:"<liferay-ui:message key='areacode'/>",
			phone:"<liferay-ui:message key='phone'/>",
			email:"<liferay-ui:message key='email'/>",
			idNumber:"<liferay-ui:message key='idNumber'/>",
			facebook:"<liferay-ui:message key='facebook'/>",
			weibo:"<liferay-ui:message key='weibo'/>",
			twitter:"<liferay-ui:message key='twitter'/>",
			str1:"<liferay-ui:message key='str1'/>",
			str2:"<liferay-ui:message key='str2'/>",
			city:"<liferay-ui:message key='city'/>",
			state:"<liferay-ui:message key='state'/>",
			country:"<liferay-ui:message key='country'/>",
			zipcode:"<liferay-ui:message key='zipcode'/>",
			licenseId:"<liferay-ui:message key='licenseId'/>",
			licenseCountry:"<liferay-ui:message key='licenseCountry'/>",
			addTime:"<liferay-ui:message key='addTime'/>",
			docCarrier:"<liferay-ui:message key='docCarrier'/>",
			docTrackingNo:"<liferay-ui:message key='docTrackingNo'/>",
			promoAmount:"<liferay-ui:message key='promoamt'/>",
			saleAmount:"<liferay-ui:message key='saleamt'/>",
			salePhase:"<liferay-ui:message key='phase'/>",
			salesId:"<liferay-ui:message key='salesId'/>",
			totalAmount:"<liferay-ui:message key='totalAmount'/>",
			totalPrice:"<liferay-ui:message key='totalPrice'/>",
			unitPrice:"<liferay-ui:message key='unitPrice'/>",
			currency:"<liferay-ui:message key='currency'/>",
			activeFlag:"<liferay-ui:message key='activeFlag'/>",
			approvalFlag:"<liferay-ui:message key='approvalFlag'/>",
			certCarrier:"<liferay-ui:message key='certCarrier'/>",
			certTrackingNo:"<liferay-ui:message key='certTrackingNo'/>",
			certSentFlag:"<liferay-ui:message key='certSentFlag'/>",
			completeFlag:"<liferay-ui:message key='completeFlag'/>",
			orderId:"<liferay-ui:message key='orderId'/>",
			rsnCd:"<liferay-ui:message key='rsnCd'/>",
			docSentFlag:"<liferay-ui:message key='docSentFlag'/>",
			docSignFlag:"<liferay-ui:message key='docSignFlag'/>",
			userType:"<liferay-ui:message key='userType'/>",
			licenseId:"<liferay-ui:message key='licenseId'/>",
			licenseCountry:"<liferay-ui:message key='licenseCountry'/>",
			addUser:"<liferay-ui:message key='addUser'/>",
			paidFlag:"<liferay-ui:message key='paidFlag'/>",
			caseCloser:"<liferay-ui:message key='caseCloser'/>",
			verifyFlag:"<liferay-ui:message key='verifyFlag'/>",
			failFlag:"<liferay-ui:message key='failFlag'/>",
			ssn:"<liferay-ui:message key='ssn'/>",
			ein:"<liferay-ui:message key='ein'/>",
			shippingstr1:"<liferay-ui:message key='shippingstr1'/>",
			shippingstr2:"<liferay-ui:message key='shippingstr2'/>",
			shippingcity:"<liferay-ui:message key='shippingcity'/>",
			shippingstate:"<liferay-ui:message key='shippingstate'/>",
			shippingcountry:"<liferay-ui:message key='shippingcountry'/>",
			shippingzipcode:"<liferay-ui:message key='shippingzipcode'/>",
			
	}
       function getneedinfo(data){
    	     var obj = {'name':data.firstName+' '+data.lastName,'email':data.email,'userId':data.userId,'orderId':data.orderId,'verifyFlag':data.verifyFlag,
    	    		 'docSentFlag':data.docSentFlag,'docSignFlag':data.docSignFlag,'paidFlag':data.paidFlag,
    	    		 'approvalFlag':data.approvalFlag,'certSentFlag':data.certSentFlag,'completeFlag':data.completeFlag,
    	    		 'doccarrier':data.docCarrier,'doctracking':data.docTrackingNo,'certcarrier':data.certCarrier,
    	    		 'certtracking':data.certTrackingNo,'saleAmount':data.saleAmount.toString(),'saleMoney':data.totalPrice.toString()+' '+data.currency,'pahse':data.salePhase};
    	     return obj
       }

       function createtable(data,allinfo){
    	   if ( $.fn.DataTable.isDataTable('#orderinfo') ) {
    		      $('#orderinfo').html('');
    			  $('#orderinfo').DataTable().destroy();
    			  
    			  
    			}
    		var table1 = $('#orderinfo').DataTable({
    			
    	    	"iDisplayLength": 25,
    	         "responsive": true,
    	         "destroy": true,
    	        "autoFill": false,
    	        "data":data,
    	        "columnDefs":[ {
    	              "targets": -4,
    	              "data": null,
    	              "defaultContent": "<div class='verify'><button>操作</button></div>",
    	            },
    	            {
      	              "targets": -3,
      	              "data": null,
      	              "defaultContent": "<div class='check'><button>查看</button></div>",
      	            },
      	          {
        	              "targets": -2,
        	              "data": null,
        	              "defaultContent": "<div class='userinfo'><button>用户信息</button></div>",
        	            },
        	            {
          	              "targets": -1,
          	              "data": null,
          	              "defaultContent": "<div class='userphoto'><button>查看照片</button></div>",
          	            },
          	       /*    {
            	              "targets": -1,
            	              "data": null,
            	              "defaultContent": "<div class='checkpdf'><button>查看文件</button></div>",
            	            }, */

    	            {
    	                "targets": [-11,-10,-8,-9],
    	                "visible": false,
    	                "searchable": false,
    	              }
    	            ],
    	            "oLanguage" : {
	 		                "sLengthMenu": "<liferay-ui:message key='LengthMenu'/>",
	 		                "sZeroRecords": "<liferay-ui:message key='ZeroRecords'/>",
	 		                "sInfo": "<liferay-ui:message key='Info'/>",
	 		                "sInfoEmpty": "<liferay-ui:message key='InfoEmpty'/>",
	 		                "sInfoFiltered": "<liferay-ui:message key='InfoFiltered'/>",
	 		                 "sSearch": "<liferay-ui:message key='Search'/>",
	 		                "oPaginate": {
	 		                "sFirst": "<liferay-ui:message key='First'/>",
	 		                "sPrevious": "<liferay-ui:message key='Previous'/>",
	 		                "sNext": "<liferay-ui:message key='Next'/>",
	 		                "sLast": "<liferay-ui:message key='Last'/>"
	 		                 }
	 		                 }
    	            /* "createdRow": function (row, data, dataIndex) {
    	         	   if(data[14] == "Y"){
    	             	   $(row).find('button:first').text('Create Order');
    	                }else{
    	                	$(row).find('button:first').text('Verify');
    	                }
    	            }  */
    			
    		})
  $('#orderinfo tbody').off( 'click.rowClick' ).on( 'click.rowClick', '.verify', function (event) {
		var newindex=table1.row($(this).closest('tr')).index();
        if(newindex==undefined){
       	 newindex=table1.row($(this).closest('tr').prev()).index();
        }
        if(refresh){
        	refresh = !refresh;
    	 	console.log(refresh);
    	 	clearTimeout(settimeout);
    	 	if(refresh){
    	 		console.log('restart');
    	 		$('#refresh').css('color','red');
    	 		settimeout = setTimeout("location.reload(true);", 31000);
    	 	}else{
    	 		$('#refresh').css('color','black');
    	 		console.log('stop');
    	 		
    	 	}
        }
    
        window.index=newindex;
        if($(window).width()>600){
          	  var screenWidth = '500px';
            }else{
          	  var screenWidth = '350px';
            }
        $('#detail').dialog({
        	title: '操作',
            modal: true,
            width: screenWidth,
            resizable:true,
            draggable:true,
            height:500,
            maxHeight: 800,
            position: { my: "center", at: "center", of: window },
            open:function(){
            	console.log(data[newindex]);
                    var orderid = data[newindex][3];
            		$('#info').html('');
            		$('#info').html("<input style='display:none' value='"+orderid+"' id='orderid'><p><strong>请先输入登陆密码</strong></p><input id='password' type='password' placeholder='password'></br></br></br><label class='control-label'><liferay-ui:message key='verifyFlag'/></label><p>"+data[newindex][4]+"</p><button onclick='verify()'><liferay-ui:message key='verify'/></button></br></br></br>\
            				<label class='control-label'><liferay-ui:message key='docSentFlag'/></label><p>"+data[newindex][5]+"</p><input placeholder='carrier' id='doccarrier'><input placeholder='tracking NO' id='doctracking'><button onclick='sent()'><liferay-ui:message key='sent'/></button></br></br></br>\
            				<label class='control-label'><liferay-ui:message key='docSignFlag'/></label><p>"+data[newindex][6]+"</p><button onclick='signed()'><liferay-ui:message key='signed'/></button></br></br></br>\
            				<label class='control-label'><liferay-ui:message key='paidFlag'/></label><p>"+data[newindex][7]+"</p><button onclick='paid()'><liferay-ui:message key='paid'/></button></br></br></br>\
            				<label class='control-label'><liferay-ui:message key='approvalFlag'/></label><p>"+data[newindex][8]+"</p><button onclick='approve()'><liferay-ui:message key='approve'/></button></br></br></br>\
            				<label class='control-label'><liferay-ui:message key='certSentFlag'/></label><p>"+data[newindex][9]+"</p><input placeholder='carrier' id='certcarrier'><input placeholder='tracking NO' id='certtracking'><button onclick='cert()'><liferay-ui:message key='sent'/></button></br></br></br>\
            				<label class='control-label'><liferay-ui:message key='completeFlag'/></label><p>"+data[newindex][10]+"</p><button onclick='complete()'><liferay-ui:message key='complete'/></button></br></br></br>\
            				<label class='control-label'><liferay-ui:message key='failFlag'/></label><select id='failreason'></select><button onclick='fail()'><liferay-ui:message key='fail'/></button></br>");
            		$('#doccarrier').val(data[newindex][11]);
            		$('#doctracking').val(data[newindex][12]);
            		$('#certcarrier').val(data[newindex][13]);
            		$('#certtracking').val(data[newindex][14]);
            		var fail = document.getElementById('failreason');
            		Liferay.Service(
            				  '/AaPortal-portlet.coinorder/get-reasons',
            				  function(obj) {
            				    var keys = Object.keys(obj.data);
            				    var value = Object.values(obj.data);
            				    keys.map(function(info,index){
            				    	var opt = document.createElement('option');
            				    	var opt_txt = document.createTextNode (value[index]);
            				          opt.appendChild (opt_txt);
            				    	   opt.setAttribute ("value", info);
            				           fail.appendChild(opt);
            				    })
            				  }
            				);
            		
            	
            },
            buttons:[]
        	
        	
        })
		
	});
    		  $('#orderinfo tbody').on( 'click', '.check', function (event) {
    				var newindex=table1.row($(this).closest('tr')).index();
    		        if(newindex==undefined){
    		       	 newindex=table1.row($(this).closest('tr').prev()).index();
    		        }
    		        if(refresh){
    		        	refresh = !refresh;
    		    	 	console.log(refresh);
    		    	 	clearTimeout(settimeout);
    		    	 	if(refresh){
    		    	 		console.log('restart');
    		    	 		$('#refresh').css('color','red');
    		    	 		settimeout = setTimeout("location.reload(true);", 31000);
    		    	 	}else{
    		    	 		$('#refresh').css('color','black');
    		    	 		console.log('stop');
    		    	 		
    		    	 	}
    		        }
    		        window.index=newindex;
    		        if($(window).width()>600){
    		          	  var screenWidth = '500px';
    		            }else{
    		          	  var screenWidth = '350px';
    		            }
    		        $('#detail').dialog({
    		        	title: '订单信息',
    		            modal: true,
    		            width: screenWidth,
    		            resizable:true,
    		            draggable:true,
    		            height:500,
    		            maxHeight: 800,
    		            position: { my: "center", at: "center", of: window },
    		            open:function(){
    		                for(var i=0;i<allinfo.length;i++){
    	        	             if(allinfo[i].orderId==data[newindex][3]){
    	        	            	    
    	        	            	    var keys = Object.keys(allinfo[i]).sort();
    	                    			//var keys = Object.keys(allinfo[i]);
    	                    			
    	                    			var values = Object.values(allinfo[i]);
    	                    			var info='';
    	                    			keys.map(function(data,index){
    	                    				if(data=='activeFlag'){
    	                    					info +="<form class='form well form-search'><label style='display:none' class='control-label formlabel'>"+translation[data]+"</label>"
      	                    				  +"<input style='display:none' type='text' class='input input-medium search-query' readonly='readonly' value='"+allinfo[i][data]+"'></br>"
    	                    				}else{
    	                    					info +="<form class='form well form-search'><label class='control-label formlabel' style='width:100px'>"+translation[data]+"</label>"
      	                    				  +"<input type='text' class='input input-medium search-query' readonly='readonly' value='"+allinfo[i][data]+"'></br>"
    	                    				}
    	                    				  
    	        		            })
    	        		            $('#info').html(info);
    	        	
    	        			}
    	        		      }
    		            }
    		        })
    		  })
    		    $('#orderinfo tbody').on( 'click', '.userphoto', function (event) {
    				var newindex=table1.row($(this).closest('tr')).index();
    		        if(newindex==undefined){
    		       	 newindex=table1.row($(this).closest('tr').prev()).index();
    		        }
    		        if(refresh){
    		        	refresh = !refresh;
    		    	 	console.log(refresh);
    		    	 	clearTimeout(settimeout);
    		    	 	if(refresh){
    		    	 		console.log('restart');
    		    	 		$('#refresh').css('color','red');
    		    	 		settimeout = setTimeout("location.reload(true);", 31000);
    		    	 	}else{
    		    	 		$('#refresh').css('color','black');
    		    	 		console.log('stop');
    		    	 		
    		    	 	}
    		        }
    		        window.index=newindex;
    		        if($(window).width()>600){
  		          	  var screenWidth = '500px';
  		            }else{
  		          	  var screenWidth = '350px';
  		            }
    		        $('#detail').dialog({
    		        	title: '用户照片',
    		            modal: true,
    		            width: screenWidth,
    		            resizable:true,
    		            draggable:true,
    		            height:500,
    		            maxHeight: 800,
    		            position: { my: "center", at: "center", of: window },
    		            open:function(){
    		            	var info = '加载中。。。'
    		            	$('#info').html(info);
    		            	Liferay.Service(
    		            			  '/AaPortal-portlet.coinuser/get-image',
    		            			  {
    		            			    userId: data[newindex][2]
    		            			  },
    		            			  function(obj) {
    		            				  
    		            				  if(Object.values(obj).length==0){
    		            					  info = '未上传照片';
    		            					  info += '<img id="newpreview" src="data:image/png;base64,'+obj+'"/></br>';
    		            				  }else{
    		            					  info = '<img id="newpreview" src="data:image/png;base64,'+obj+'"/></br>'
    		            				  }
    		            			    if(data[newindex][14]==="N"){
    		            			    	info += '<input id="uploadimg" type="file" onchange="previewFile1()" class="input input-medium search-query" id="uploadphoto" />'
    		            			    }
    		            			    $('#info').html(info);
    		            			  }
    		            			);
    		                
    		            }
    		        })
    		  })
    		      $('#orderinfo tbody').on( 'click', '.checkpdf', function (event) {
    				var newindex=table1.row($(this).closest('tr')).index();
    		        if(newindex==undefined){
    		       	 newindex=table1.row($(this).closest('tr').prev()).index();
    		        }
    		        if(refresh){
    		        	refresh = !refresh;
    		    	 	console.log(refresh);
    		    	 	clearTimeout(settimeout);
    		    	 	if(refresh){
    		    	 		console.log('restart');
    		    	 		$('#refresh').css('color','red');
    		    	 		settimeout = setTimeout("location.reload(true);", 31000);
    		    	 	}else{
    		    	 		$('#refresh').css('color','black');
    		    	 		console.log('stop');
    		    	 		
    		    	 	}
    		        }
    		        window.index=newindex;
    		        if($(window).width()>600){
    		          	  var screenWidth = '500px';
    		            }else{
    		          	  var screenWidth = '350px';
    		            }
    		        $('#docdetail').dialog({
    		        	title: '认证信息',
    		            modal: true,
    		            width: screenWidth,
    		            resizable:true,
    		            draggable:true,
    		            height:500,
    		            maxHeight: 800,
    		            position: { my: "center", at: "center", of: window },
    		            open:function(){
    		            	var info = '加载中。。。'
    		            	$('#docinfo').html(info);
    		            	Liferay.Service(
    		            			  '/AaPortal-portlet.coinuser/get-cert-doc',
    		            			  {
    		            			    userId: data[newindex][2]
    		            			  },
    		            			  function(obj) {
    		            				  
    		            				  if(Object.values(obj).length==0){
    		            					  info = '未上传信息';
    		            					  info += '<iframe id="docpreview" src="data:image/png;base64,'+obj+'"/></br>';
    		            				  }else{
    		            					  info = '<iframe id="docpreview" src="data:application/pdf;base64,'+obj+'"/></br>'
    		            				  }
    		            			    $('#docinfo').html(info);
    		            			  }
    		            			);
    		                
    		            }
    		        })
    		  })
    		  	  $('#orderinfo tbody').on( 'click', '.userinfo', function (event) {
    				var newindex=table1.row($(this).closest('tr')).index();
    		        if(newindex==undefined){
    		       	 newindex=table1.row($(this).closest('tr').prev()).index();
    		        }
    		        if(refresh){
    		        	refresh = !refresh;
    		    	 	console.log(refresh);
    		    	 	clearTimeout(settimeout);
    		    	 	if(refresh){
    		    	 		console.log('restart');
    		    	 		$('#refresh').css('color','red');
    		    	 		settimeout = setTimeout("location.reload(true);", 31000);
    		    	 	}else{
    		    	 		$('#refresh').css('color','black');
    		    	 		console.log('stop');
    		    	 		
    		    	 	}
    		        }
    		        window.index=newindex;
    		        if($(window).width()>600){
  		          	  var screenWidth = '500px';
  		            }else{
  		          	  var screenWidth = '350px';
  		            }
    		        $('#detail').dialog({
    		        	title: '用户信息',
    		            modal: true,
    		            width: screenWidth,
    		            resizable:true,
    		            draggable:true,
    		            height:500,
    		            maxHeight: 800,
    		            position: { my: "center", at: "center", of: window },
    		            open:function(){
    		            	$('#info').html('');
    		            	Liferay.Service(
    		            			  '/AaPortal-portlet.coinuser/get-coin-user',
    		            			  {
    		            			    coinUserId: data[newindex][2]
    		            			  },
    		            			  function(obj) {
    		            				 var info = obj.data;
    		            				 var show = '';
    		            				  newinfo = {'firstname':info.firstName,'lastname':info.lastName,'birthday':info.birthday,'idType':info.idType,
    		    				    			  'asset':info.asset,'idIssueCountry':info.idIssueCountry,'addUser':info.addUser,'brokerId':info.brokerId,
    		    				    			  'userType':info.userType,'licenseId':info.licenseId,'licenseCountry':info.licenseCountry,'nationality':info.nationality,
    		    				    			  'ssn':info.ssn,'ein':info.ein,'fundId':info.fundId,'income':info.income,'userId':info.userId,
    		    				    			  'isValidated':info.isValidated,'wechat':info.contact.wechat,'areacode':info.contact.areaCode,
    		    				    			  'phone':info.contact.phone,'email':info.contact.email,'idNumber':info.idNumber,
    		    				    			  'str1':info.contact.street1,'str2':info.contact.street2,'city':info.contact.city,'state':info.contact.state,'country':info.contact.country,'zipcode':info.contact.zipCode,
    		    				    			  'shippingstr1':info.contact.mailstreet1,'shippingstr2':info.contact.mailstreet2,'shippingcity':info.contact.mailcity,'shippingstate':info.contact.mailstate,'shippingcountry':info.contact.mailcountry,'shippingzipcode':info.contact.mailzipCode,
    		    				    			  
    		    				    			  };
    		            				  var keys = Object.keys(newinfo);
    		            				  var value = Object.values(newinfo);
    		            				  keys.map(function(data,index){
    		            					  show +="<form class='form well form-search'><label class='control-label' style='width:100px'>"+translation[data]+"</label>"
                            				  +"<input type='text' class='input input-medium search-query' readonly='readonly' value='"+value[index]+"'></br>"
    		            				  })
    		            				  $('#info').html(show);
    		            			  }
    		            			);
    		            }
    		        })
    		  })
    	
       }
		 if (Liferay.ThemeDisplay.isSignedIn()) {
			 var datainfo = [];
			 var createtableinfo = [];
			 Liferay.Service(
					  '/AaPortal-portlet.coinorder/get-peding-order',
					  function(obj) {
					    if(obj.status.code==='00'){
					    	var allinfo = obj.data;
					    	obj.data.map(function(info){
					    		datainfo.push(getneedinfo(info));
					    		createtableinfo.push(Object.values(getneedinfo(info)));
					    	})
					    }
					    createtable(createtableinfo,allinfo);
						$('#selectphase').change(function(){
							
							 var phase = $('#selectphase option:selected').val();
							 console.log('changed'+phase);
							 var datainfo = [];
							 var createtableinfo = [];
									    if(phase=='all'){
									   
									    		obj.data.map(function(info){
										    		
										    			datainfo.push(getneedinfo(info));
											    		createtableinfo.push(Object.values(getneedinfo(info)));
										    		
										    	})
										    	createtable(createtableinfo,allinfo);
									    	}else{
									    
									    		obj.data.map(function(info){
										    		if(info.salePhase==phase){
										    			datainfo.push(getneedinfo(info));
											    		createtableinfo.push(Object.values(getneedinfo(info)));
										    		}
										    		
										    	})
										    	createtable(createtableinfo,allinfo);
									    	}
									    	
									
						 })
					  }
					);
			 
			 
		 }

	
 </aui:script>
 <script>
 var codeerror = {
			'00':'成功',
			'010':'无权访问',
			'011':'无权操作该用户/订单',
			'020':'邮箱已存在',
			'040':'系统错误，请联系IT部门',
			'050':'代理人信息错误，请确认后重新提交',
			'051':'用户类型错误，请输入正确类型 ',
			'052':'用户信息有误，请确认后重新提交',
			'053':'联系方式或地址有误，请确认后重新提交',
			'054':'基金信息有误，请确认后重新提交',
			'055':'错误的图片格式，请确认后重新提交',
			'056':'错误的失败原因代码，请确认后重新提交',
			'057':'错误的销售期或价格， 请确认后重新提交',
			'060':'最多只能处理3名潜在用户，请完成已有任务再来接新任务，谢谢',
			'061':'剩余的发行量不足， 请减少购买数量',
			'080':'订单还未确认， 请确认后再操作',
			'081':'订单合同还未发出， 请确认发出后再操作',
			'082':'订单还未付款，请确认付款后再做操作',
			'083':'订单合同还未签字，请确认用户签字后再做操作',
			'084':'订单未批准，请联系管理人员进行审核和批准再做操作',
			'085':'订单证书还未寄出，请确认寄出后再操作',
			'086':'订单处于未激活状态，无法进行操作',
			'090':'用户还未验证，请验证后再进行操作',
			'091':'代理人未验证，请验证后再操作',
			'092':'基金未验证， 请验证后再操作'
		};

 function verify(data){
	 console.log($('#orderid').val());
	 console.log(typeof(data));
	 console.log(data);
	   var password = $('#password').val();
	   Liferay.Service(
			   '/AaPortal-portlet.commonfunction/check-password',
			   {
			     password: password
			   },
			   function(obj) {
			     if(obj){
			    	   Liferay.Service(
			    			   '/AaPortal-portlet.coinorder/verified',
			    			   {
			    			     orderId: $('#orderid').val()
			    			   },
			    			   function(obj) {
			    			     if(obj.status.code==='00'){
			    			    	 alert('成功');
			    			    	 location.reload(true);
			    			     }else{
			    			    	 var code = obj.status.code.toString();
									   console.log(codeerror)
									   alert(codeerror[code]);
			    			     }
			    			   }
			    			 );
			     }else{
			    	 alert('密码错误');
			     }
			   }
			 );

 }
 function signed(data){
	   var password = $('#password').val();
	   Liferay.Service(
			   '/AaPortal-portlet.commonfunction/check-password',
			   {
			     password: password
			   },
			   function(obj) {
			     if(obj){
			    	 Liferay.Service(
			    			   '/AaPortal-portlet.coinorder/contract-signed',
			    			   {
			    			     orderId: $('#orderid').val()
			    			   },
			    			   function(obj) {
			    				   if(obj.status.code==='00'){
			    			    	 alert('成功');
			    			    	 location.reload(true);
			    			     }else{
			    			    	 var code = obj.status.code.toString();
									   console.log(codeerror)
									   alert(codeerror[code]);
			    			     }
			    			   }
			    			 );
			     }else{
			    	 alert('密码错误');
			     }
			   }
			 );
 }
 function sent(orderid){

	   var password = $('#password').val();
	   var doccarrier = $('#doccarrier').val();
	   var doctracking = $('#doctracking').val();
	   Liferay.Service(
			   '/AaPortal-portlet.commonfunction/check-password',
			   {
			     password: password
			   },
			   function(obj) {
				   if(obj){
			    	   Liferay.Service(
			    			   '/AaPortal-portlet.coinorder/contract-sent',
			    			   {
			    			     orderId: $('#orderid').val(),
			    			     carrier: doccarrier,
			    			     trackingNo: doctracking
			    			   },
			    			   function(obj) {
			    				   if(obj.status.code==='00'){
			    			    	 alert('成功');
			    			    	 location.reload(true);
			    			     }else{
			    			    	 var code = obj.status.code.toString();
									   console.log(codeerror)
									   alert(codeerror[code]);
			    			     }
			    			   }
			    			 );
				   }else{
				    	 alert('密码错误');
				     }
			   }
			 );

 }
 function paid(data){

	   var password = $('#password').val();
	   Liferay.Service(
			   '/AaPortal-portlet.commonfunction/check-password',
			   {
			     password: password
			   },
			   function(obj) {
				   if(obj){
				   	   Liferay.Service(
			    			   '/AaPortal-portlet.coinorder/paid',
			    			   {
			    			     orderId: $('#orderid').val()
			    			   },
			    			   function(obj) {
			    				   if(obj.status.code==='00'){
			    			    	 alert('成功');
			    			    	 location.reload(true);
			    			     }else{
			    			    	 var code = obj.status.code.toString();
									   console.log(codeerror)
									   alert(codeerror[code]);
			    			     }
			    			   }
			    			 );
				   }else{
					   alert('密码错误');
				     }
			   }
			 );

 }
 function approve(data){

	   var password = $('#password').val();
	   Liferay.Service(
			   '/AaPortal-portlet.commonfunction/check-password',
			   {
			     password: password
			   },
			   function(obj) {
				   if(obj){
			    	   Liferay.Service(
			    			   '/AaPortal-portlet.coinorder/approve',
			    			   {
			    			     orderId: $('#orderid').val()
			    			   },
			    			   function(obj) {
			    				   if(obj.status.code==='00'){
			    			    	 alert('成功');
			    			    	 location.reload(true);
			    			     }else{
			    			    	 var code = obj.status.code.toString();
									   console.log(codeerror)
									   alert(codeerror[code]);
			    			     }
			    			   }
			    			 );
				   }else{
					   alert('密码错误');
				     }
			   }
			 );

 }
 function cert(data){

	   var password = $('#password').val();
	   var certcarrier = $('#certcarrier').val();
	   var certtracking = $('#certtracking').val();
	   Liferay.Service(
			   '/AaPortal-portlet.commonfunction/check-password',
			   {
			     password: password
			   },
			   function(obj) {
				   if(obj){
			    	   Liferay.Service(
			    			   '/AaPortal-portlet.coinorder/cert-sent',
			    			   {
			    			     orderId: $('#orderid').val(),
			    			     carrier: certcarrier,
			    			     trackingNo: certtracking
			    			   },
			    			   function(obj) {
			    				   if(obj.status.code==='00'){
			    			    	 alert('成功');
			    			    	 location.reload(true);
			    			     }else{
			    			    	 var code = obj.status.code.toString();
									   console.log(codeerror)
									   alert(codeerror[code]);
			    			     }
			    			   }
			    			 );
				   }else{
					   alert('密码错误');
				     }
			   }
			 );

 }
 function complete(data){
	   var password = $('#password').val();
	   Liferay.Service(
			   '/AaPortal-portlet.commonfunction/check-password',
			   {
			     password: password
			   },
			   function(obj) {
				   if(obj){
			    	   Liferay.Service(
			    			   '/AaPortal-portlet.coinorder/complete',
			    			   {
			    			     orderId: $('#orderid').val()
			    			   },
			    			   function(obj) {
			    				   if(obj.status.code==='00'){
			    			    	 alert('成功');
			    			    	 location.reload(true);
			    			     }else{
			    			    	 var code = obj.status.code.toString();
									  
									   //console.log(codeerror['085']);
									   console.log(codeerror)
									   alert(codeerror[code]);
			    			     }
			    			   }
			    			 );
				   }else{
					   alert('密码错误');
				     }
			   }
			 );

 }
 function fail(data){
	   var password = $('#password').val();
	   var failreason = $('#failreason option:selected').val();
	   Liferay.Service(
			   '/AaPortal-portlet.commonfunction/check-password',
			   {
			     password: password
			   },
			   function(obj) {
				   if(obj){
					   Liferay.Service(
							   '/AaPortal-portlet.coinorder/fail',
							   {
							     orderId: $('#orderid').val(),
							     rsnCd: failreason
							   },
							   function(obj) {
								   if(obj.status.code==='00'){
									   alert('成功');
				    			    	 location.reload(true);
								   }else{
									   var code = obj.status.code.toString();
									   console.log(codeerror)
									   alert(codeerror[code]);
								   }
							    
							   }
							 );
				   }else{
					   alert('密码错误');
				     }
			   }
			 );

}
 </script>