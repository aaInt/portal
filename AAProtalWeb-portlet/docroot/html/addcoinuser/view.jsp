<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
 <%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
 <%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet"%>
 <%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
 <%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
 <%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
 <portlet:defineObjects />
 <%@page import="java.io.Serializable"%>
 <%@page import="javax.portlet.PortletSession"%>
 <%@page import="com.liferay.portal.model.User"%>
 <%@page import="com.liferay.portal.model.Organization"%>
 <%@page import="com.liferay.portal.theme.ThemeDisplay"%>
 <%@ page language="java" contentType="text/html; charset=UTF-8"
     pageEncoding="UTF-8"%>
 <%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
 <%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
 <%@ page import="java.util.*"%>
 <%@ page import="javax.portlet.PortletPreferences"%>
 <%@ page import="java.text.DateFormat"%>
 <%@ page import="java.text.SimpleDateFormat"%>
 <%@ page
     import="com.liferay.portlet.expando.service.ExpandoValueServiceUtil"%>
 <%@ page
     import="com.liferay.portal.service.OrganizationLocalServiceUtil"%>
 <%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
 <%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
 <%@ page import="com.liferay.portal.kernel.language.LanguageUtil"%>
 <%@ page import="com.liferay.portal.kernel.util.ParamUtil"%>
 <%@ page import="com.liferay.portal.util.PortalUtil"%>
 <%@ page import="com.liferay.portal.kernel.language.LanguageWrapper"%>
 <%@ page import="com.liferay.portal.kernel.language.UnicodeLanguageUtil"%>
 <%@page import="com.liferay.portal.util.PortalUtil"%>
 <%@page import="javax.portlet.PortletURL"%>
 <%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
 <portlet:defineObjects />
 <%
 String namespace="_coinUserPortlet_";
 ThemeDisplay themeDisplay = (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
 User user = themeDisplay.getUser();
 long userId=user.getUserId();

 HttpServletRequest r = PortalUtil.getHttpServletRequest(renderRequest);
 /* String langId=LanguageUtil.getLanguageId(request);
 String val =  PortalUtil.getOriginalServletRequest(r).getParameter("language");
 if(val !=null){
 	language =val;
 }else{
 boolean is_en_US=langId.equals("en_US");
 boolean is_zh_CN=langId.equals("zh_CN");

 if(is_zh_CN)
 	language="zh";
 } */
 String langId=LanguageUtil.getLanguageId(request);
 boolean is_en_US=langId.equals("en_US");
 boolean is_zh_CN=langId.equals("zh_CN");
 boolean is_ja_JP=langId.equals("ja_JP");

 %>
 <style>
    #popup label{
        width:100px;
    }
    #docdetail label{
       width:100px;
    }
    #detail label{
       width:100px;
    }
  /*   #usertable_filter{
       display:none;
       
    } */
   /*  .dataTables_length{
       display:none;
    } */
    .myclients{
       display:none;
    }
   /*  #clienttable_filter{
       display:none;
    } */
    .handelclients{
      display:none;
    }
   /*  #handeltable_filter{
       display:none;
    } */
      .verifiedclients{
      display:none;
    }
   /*  #verifiedtable_filter{
       display:none;
    }  */
    .ui-dialog{
        height:900px;
    }
    #upload-button {
	width: 150px;
	display: block;
	margin: 20px auto;
}


#testpdf{
  zoom:2;
  display:none;
}
#docpreview{
    zoom:2;
}
 </style>
 <div>
 <div class="btn-group select">
  <button class="btn" id='all' value='All'><liferay-ui:message key='allclients'/></button>
  <button class="btn" id='my' value='My'><liferay-ui:message key='myclients'/></button>
  <button class="btn" id='process' value='Process'><liferay-ui:message key='processingclients'/></button>
  <button class="btn" id='verified' value='verified'><liferay-ui:message key='verifiedclients'/></button>
</div>
 <div class='allclients'>
       <table id='clienttable' style="width:100%;" class='table table-striped table-bordered dataTable no-footer'>
         <thead>
           <tr>
              <th><liferay-ui:message key='firstname'/></th>
              <th><liferay-ui:message key='lastname'/></th>
              <th><liferay-ui:message key='email'/></th>
              <th><liferay-ui:message key='areacode'/></th>
              <th><liferay-ui:message key='phone'/></th>
              <th><liferay-ui:message key='country'/></th>
              <th></th>
           </tr>
           </thead>
           <tbody style='text-align:center'>
             
           </tbody>
       </table>
 </div>
 <div class='myclients'>
       <table id='usertable' style="width:100%;" class='table table-striped table-bordered dataTable no-footer'>
         <thead>
           <tr>
              <th><liferay-ui:message key='firstname'/></th>
              <th><liferay-ui:message key='lastname'/></th>
              <th><liferay-ui:message key='email'/></th>
              <th><liferay-ui:message key='areacode'/></th>
              <th><liferay-ui:message key='phone'/></th>
              <th><liferay-ui:message key='country'/></th>
              <th></th>
              <th></th>
              <th></th>
           </tr>
           </thead>
           <tfoot>
               <tr>
                <button class="btn" id='newclient' ><liferay-ui:message key='addnewclient'/></button>
                </tr>
           </tfoot>
           <tbody style='text-align:center'>
             
           </tbody>
       </table>
 </div>
 <div class='handelclients'>
      <table id='handeltable' style="width:100%;" class='table table-striped table-bordered dataTable no-footer'>
         <thead>
           <tr>
              <th><liferay-ui:message key='firstname'/></th>
              <th><liferay-ui:message key='lastname'/></th>
              <th><liferay-ui:message key='birthday'/></th>
              <th><liferay-ui:message key='idtype'/></th>
              <th><liferay-ui:message key='asset'/></th>
              <th><liferay-ui:message key='idissuecountry'/></th>
              <th><liferay-ui:message key='adduser'/></th>
              <th><liferay-ui:message key='brokerid'/></th>
              <th><liferay-ui:message key='usertype'/></th>
              <th><liferay-ui:message key='nationality'/></th>
              <th></th>
              <th></th>
              <th><liferay-ui:message key='fundid'/></th>
              <th><liferay-ui:message key='activeflag'/></th>
              <th><liferay-ui:message key='income'/></th>
              <th><liferay-ui:message key='userid'/></th>
              <th><liferay-ui:message key='isvalidated'/></th>
              <th><liferay-ui:message key='photo'/></th>
              <th><liferay-ui:message key='wechat'/></th>
              <th><liferay-ui:message key='areacode'/></th>
              <th><liferay-ui:message key='phone'/></th>
              <th><liferay-ui:message key='email'/></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
           </tr>
           </thead>
           <tbody style='text-align:center'>
             
           </tbody>
       </table>
 
 </div>
  <div class='verifiedclients'>
      <table id='verifiedtable' style="width:100%;" class='table table-striped table-bordered dataTable no-footer'>
         <thead>
           <tr>
              <th><liferay-ui:message key='firstname'/></th>
              <th><liferay-ui:message key='lastname'/></th>
              <th><liferay-ui:message key='birthday'/></th>
              <th><liferay-ui:message key='idtype'/></th>
              <th><liferay-ui:message key='asset'/></th>
              <th><liferay-ui:message key='idissuecountry'/></th>
              <th><liferay-ui:message key='adduser'/></th>
              <th><liferay-ui:message key='brokerid'/></th>
              <th><liferay-ui:message key='usertype'/></th>
              <th><liferay-ui:message key='nationality'/></th>
              <th></th>
              <th></th>
              <th><liferay-ui:message key='fundid'/></th>
              <th><liferay-ui:message key='activeflag'/></th>
              <th><liferay-ui:message key='income'/></th>
              <th><liferay-ui:message key='userid'/></th>
              <th><liferay-ui:message key='isvalidated'/></th>
              <th><liferay-ui:message key='photo'/></th>
              <th><liferay-ui:message key='wechat'/></th>
              <th><liferay-ui:message key='areacode'/></th>
              <th><liferay-ui:message key='phone'/></th>
              <th><liferay-ui:message key='email'/></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th><liferay-ui:message key='isCreddited'/></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
           </tr>
           </thead>
           <tbody style='text-align:center'>
             
           </tbody>
       </table>
 
 </div>
 <div id='verifyinfo'>
    <div id='detail'></div>
 </div>
 <div id='submitdetail' style='display:none'>
     <label class='control-label'>ClientId</label><input readonly='readonly' type='text' id='clientid'></br>
            				<label class='control-label'><liferay-ui:message key='brokerid'/></label><input id='brokerId' type='text' value='-1'></br>
            				<label class='control-label'><liferay-ui:message key='phase'/></label><input id='phase' type='text' readonly='readonly'></br>
            				<label class='control-label'><liferay-ui:message key='unitprice'/></label><input readonly='readonly' id='unitPrice' type='text' ></br>
            				<label class='control-label'><liferay-ui:message key='saleamt'/></label><input id='saleAmt' type='text' ></br>
            				<label class='control-label'><liferay-ui:message key='currency'/></label><select id='currency'><option value=''></option><option value='CNY'>CNY</option>
            				<option value='USD'>USD</option><option value='JPY'>JPY</option><option value='HKD'>HKD</option></select></br>
            				<label class='control-label'><liferay-ui:message key='totalprice'/></label><input readonly='readonly' id='totalPrice' type='text' ></br>
            				<label class='control-label'><liferay-ui:message key='promoamt'/></label><input id='promoAmt' type='text' value='0'></br>
 </div>
  <div id='creditdetail' style='display:none'>
                            <label class='control-label'>请先输入登陆密码</label><input type='password' id='password'></br>         				
 </div>
  <div id='verifdoc'>
         <div id='docdetail'></div>
 </div>
<div id='popup' style='display:none'>
    <form class='form well form-search'>
        <label class="control-label"><liferay-ui:message key='firstname'/><span style='color:red'>*</span></label>
        <input  type='text' class='input input-medium search-query' id='first' /><br/>
        <label class="control-label"><liferay-ui:message key='lastname'/><span style='color:red'>*</span></label>
        <input  type='text' class='input input-medium search-query' id='last' /><br/>
        <label class="control-label"><liferay-ui:message key='birthday'/><span style='color:red'>*</span></label>
        <input  type='date' class='input input-medium search-query' id='birthday' /><br/>
        <label class="control-label"><liferay-ui:message key='usertype'/><span style='color:red'>*</span></label>
        <select id='usertype'>
           <option value=''></option>
           <option value='BRK'><liferay-ui:message key='BRK'/></option>
           <option value='FND'><liferay-ui:message key='FND'/></option>
           <option value='IDV'><liferay-ui:message key='IDV'/></option>
        </select><br/>
        <label class="control-label"><liferay-ui:message key='brokerlicense'/><span style='color:red;display:none' id='brokeralert'>*</span></label>
        <input  type='text' class='input input-medium search-query' id='blicense' /><br/>
        <label class="control-label"><liferay-ui:message key='brokercountry'/><span style='color:red;display:none' id='bcountryalert'>*</span></label>
        <select id='bcountry'>
            <option value=''></option>
           <option value='US'><liferay-ui:message key='US'/></option>
           <option value='CN'><liferay-ui:message key='CN'/></option>
           <option value='JP'><liferay-ui:message key='JP'/></option>
           <option value='HK'><liferay-ui:message key='HK'/></option>
        </select><br/>
        <label class="control-label"><liferay-ui:message key='fundId'/></label>
        <input  type='text' class='input input-medium search-query' id='fundid' value='-1' /><br/>
        <label class="control-label"><liferay-ui:message key='nationality'/><span style='color:red'>*</span></label>
        <select id='nationality'>
           <option value=''></option>
           <option value='US'><liferay-ui:message key='US'/></option>
           <option value='CN'><liferay-ui:message key='CN'/></option>
           <option value='JP'><liferay-ui:message key='JP'/></option>
           <option value='HK'><liferay-ui:message key='HK'/></option>
           <option value='other'><liferay-ui:message key='other'/></option>
        </select><br/>
        <input  placeholder='<liferay-ui:message key="otherinfo"/>' type='text' class='input input-medium search-query' id='othernationality' /><br/>
        <label class="control-label"><liferay-ui:message key='ssn'/><span style='color:red;display:none' id='ssnalert'>*</span></label>
        <input  type='text' class='input input-medium search-query' id='ssn' value='' /><br/>
        <label class="control-label"><liferay-ui:message key='ein'/><span style='color:red;display:none' id='einalert'>*</span></label>
        <input  type='text' class='input input-medium search-query' id='ein' value='' /><br/>
        <label class="control-label"><liferay-ui:message key='idIssueCountry'/><span style='color:red'>*</span></label>
        <select id='issuecountry'>
           <option value=''></option>
           <option value='US'><liferay-ui:message key='US'/></option>
           <option value='CN'><liferay-ui:message key='CN'/></option>
           <option value='JP'><liferay-ui:message key='JP'/></option>
           <option value='HK'><liferay-ui:message key='HK'/></option>
           <option value='other'><liferay-ui:message key='other'/></option>
        </select><br/>
        <input  placeholder='<liferay-ui:message key="otherinfo"/>' type='text' class='input input-medium search-query' id='otherissuecountry' /><br/>
        <label class="control-label"><liferay-ui:message key='idType'/><span style='color:red'>*</span></label>
        <select id='issuetype'>
           <option value='passport'><liferay-ui:message key='passport'/></option>
           <option value='idcard'><liferay-ui:message key='idcard'/></option>
           <option value='greencard'><liferay-ui:message key='greencard'/></option>
           <option value='other'><liferay-ui:message key='other'/></option>
        </select><br/>
        <input  placeholder='<liferay-ui:message key="otherinfo"/>' type='text' class='input input-medium search-query' id='otherissuetype' /><br/>
        <label class="control-label"><liferay-ui:message key='idnumber'/><span style='color:red'>*</span></label>
        <input  type='text' class='input input-medium search-query' id='id' /><br/>
        <label class="control-label"><liferay-ui:message key='photo'/></label>
        <img id='preview' src=''/><input  type='file' onchange="previewFile()" class='input input-medium search-query' id='photo' /><br/>
        <label class="control-label"><liferay-ui:message key='pdf'/></label>
          <!-- <button id="upload-button">Select PDF</button>  -->
			<input type="file" id="file-to-upload"  />
			<iframe src='' id='testpdf'></iframe>
		<br/>
        <label class="control-label"><liferay-ui:message key='income'/></label>
        <input  type='text' class='input input-medium search-query' id='income' /><br/>
        <label class="control-label"><liferay-ui:message key='asset'/></label>
        <input  type='text' class='input input-medium search-query' id='asset' /><br/>
        <label class="control-label"><liferay-ui:message key='phone'/><span style='color:red'>*</span></label>
        <input  type='text' class='input input-medium search-query' id='phone' /><br/>
        <label class="control-label"><liferay-ui:message key='areacode'/><span style='color:red'>*</span></label>
        <select id='areacode'>
           <option value='86'>86</option>
           <option value='81'>81</option>
           <option value='1'>1</option>
           <option value='852'>852</option>
        </select><br/>
        <label class="control-label"><liferay-ui:message key='email'/><span style='color:red'>*</span></label>
        <input  type='text' class='input input-medium search-query' id='email' /><br/>
        <label class="control-label"><liferay-ui:message key='wechat'/></label>
        <input  type='text' class='input input-medium search-query' id='wechat' /><br/>
        <label style='display:none' class="control-label">FaceBook</label>
        <input  style='display:none' type='text' class='input input-medium search-query' id='facebook' /><br/>
        <label style='display:none' class="control-label"><liferay-ui:message key='twitter'/></label>
        <input  style='display:none' type='text' class='input input-medium search-query' id='twitter' /><br/>
        <label style='display:none' class="control-label">Weibo</label>
        <input  style='display:none' type='text' class='input input-medium search-query' id='weibo' /><br/>
        <label class="control-label"><liferay-ui:message key='str1'/><span style='color:red'>*</span></label>
        <input  type='text' class='input input-medium search-query' id='str1' /><br/>
        <label class="control-label"><liferay-ui:message key='str2'/></label>
        <input  type='text' class='input input-medium search-query' id='str2' /><br/>
        <label class="control-label"><liferay-ui:message key='city'/><span style='color:red'>*</span></label>
        <input  type='text' class='input input-medium search-query' id='city' /><br/>
        <label class="control-label"><liferay-ui:message key='state'/><span style='color:red'>*</span></label>
        <input  type='text' class='input input-medium search-query' id='state' /><br/>
        <label class="control-label"><liferay-ui:message key='country'/><span style='color:red'>*</span></label>
        <select id='country'>
           <option value='US'><liferay-ui:message key='US'/></option>
           <option value='CN'><liferay-ui:message key='CN'/></option>
           <option value='JP'><liferay-ui:message key='JP'/></option>
           <option value='HK'><liferay-ui:message key='HK'/></option>
           <option value='other'><liferay-ui:message key='other'/></option>
        </select><br/>
        <input  placeholder='<liferay-ui:message key="otherinfo"/>' type='text' class='input input-medium search-query' id='othercountry' /><br/>
        <label class="control-label"><liferay-ui:message key='zipcode'/><span style='color:red'>*</span></label>
        <input  type='text' class='input input-medium search-query' id='zipcode' /><br/>
        <label class="control-label"><liferay-ui:message key='notsame'/></label>
        <input id='notsame' type="checkbox"/>
        <div id='shippingaddress' style='display:none'>
            <label class="control-label"><liferay-ui:message key='str1'/><span style='color:red'>*</span></label>
	        <input  type='text' class='input input-medium search-query' id='shippingstr1' /><br/>
	        <label class="control-label"><liferay-ui:message key='str2'/></label>
	        <input  type='text' class='input input-medium search-query' id='shippingstr2' /><br/>
	        <label class="control-label"><liferay-ui:message key='city'/><span style='color:red'>*</span></label>
	        <input  type='text' class='input input-medium search-query' id='shippingcity' /><br/>
	        <label class="control-label"><liferay-ui:message key='state'/><span style='color:red'>*</span></label>
	        <input  type='text' class='input input-medium search-query' id='shippingstate' /><br/>
	        <label class="control-label"><liferay-ui:message key='country'/><span style='color:red'>*</span></label>
	        <select id='shippingcountry'>
	           <option value='US'><liferay-ui:message key='US'/></option>
	           <option value='CN'><liferay-ui:message key='CN'/></option>
	           <option value='JP'><liferay-ui:message key='JP'/></option>
	           <option value='HK'><liferay-ui:message key='HK'/></option>
	           <option value='other'><liferay-ui:message key='other'/></option>
	        </select><br/>
	        <input  placeholder='<liferay-ui:message key="otherinfo"/>' type='text' class='input input-medium search-query' id='shippingothercountry' /><br/>
	        <label class="control-label"><liferay-ui:message key='zipcode'/><span style='color:red'>*</span></label>
	        <input  type='text' class='input input-medium search-query' id='shippingzipcode' /><br/>
        </div>
        
    </form>
</div>
</div>
<aui:script use="array-extras, node, event">
// for pdf
var userIdforcurrency;
var uploadpdf='';
$('#file-to-upload').on('change',function(){
    var file = document.getElementById('file-to-upload').files[0];
    var reader  = new FileReader();
      reader.addEventListener("load", function () {
    	  uploadpdf = reader.result;
     $('#testpdf').attr('src',reader.result);
     if(uploadpdf.length>0){
    	 $('#testpdf').css('display','block')
     }else{
    	 $('#testpdf').css('display','none')
     }
     }, false);

		if (file) {
		reader.readAsDataURL(file);
		}
})

// for pdf
if ( $('[type="date"]').prop('type') != 'date' ) {
    $('[type="date"]').datepicker({changeYear: true,changeMonth:true,yearRange: '1900:2099'});
}
document.getElementById('usertype').addEventListener('change',function(){
	  if($('#usertype option:selected').val()=='FND'){
		  $('#einalert').css('display','inline-block');
	  }else{
		  $('#einalert').css('display','none');
	  }
	  if($('#usertype option:selected').val()=='BRK'){
		  $('#brokeralert').css('display','inline-block');
		  $('#bcountryalert').css('display','inline-block');
	  }else{
		  $('#brokeralert').css('display','none');
		  $('#bcountryalert').css('display','none');
	  }
	  if($('#nationality option:selected').val()=='US'&&$('#usertype option:selected').val()=='IDV'){
		  $('#ssnalert').css('display','inline-block');
	  }else{
		  $('#ssnalert').css('display','none');
	  }
})
document.getElementById('nationality').addEventListener('change',function(){
	  if($('#nationality option:selected').val()=='US'&&$('#usertype option:selected').val()=='IDV'){
		  $('#ssnalert').css('display','inline-block');
	  }else{
		  $('#ssnalert').css('display','none');
	  }
})
var codeerror = {
		'00':'成功',
		'010':'无权访问',
		'011':'无权操作该用户/订单',
		'020':'邮箱或用户已存在',
		'040':'系统错误，请联系IT部门',
		'050':'代理人信息错误，请确认后重新提交',
		'051':'用户类型错误，请输入正确类型 ',
		'052':'用户信息有误，请确认后重新提交',
		'053':'联系方式或地址有误，请确认后重新提交',
		'054':'基金信息有误，请确认后重新提交',
		'055':'错误的图片格式，请确认后重新提交',
		'056':'错误的失败原因代码，请确认后重新提交',
		'057':'错误的销售期或价格， 请确认后重新提交',
		'060':'最多只能处理3名潜在用户，请完成已有任务再来接新任务，谢谢',
		'061':'剩余的发行量不足， 请减少购买数量',
		'080':'订单还未确认， 请确认后再操作',
		'081':'订单合同还未发出， 请确认发出后再操作',
		'082':'订单还未付款，请确认付款后再做操作',
		'083':'订单合同还未签字，请确认用户签字后再做操作',
		'084':'订单未批准，请联系管理人员进行审核和批准再做操作',
		'085':'订单证书还未寄出，请确认寄出后再操作',
		'086':'订单处于未激活状态，无法进行操作',
		'090':'用户还未验证，请验证后再进行操作',
		'091':'代理人未验证，请验证后再操作',
		'092':'基金未验证， 请验证后再操作'
	};
$('#notsame').change(function(){
	   if($('#notsame').is(':checked')){
		     console.log('checked');
		     $('#shippingaddress').css('display','block');
	   }else{
		   console.log('unchecked');
		     $('#shippingaddress').css('display','none');
	   }
})
document.getElementById('newclient').addEventListener('click',function(){
	if($(window).width()>600){
      	  var screenWidth = '500px';
        }else{
      	  var screenWidth = '350px';
        }
    $('#popup').dialog({
    	title: '详细信息',
        modal: true,
        width: screenWidth,
        resizable:true,
        draggable:true,
        height:500,
        maxHeight: 800,
        position: { my: "center", at: "center", of: window },
        buttons:[{
        	id:'save',
    	        text: '保存',
    	        click: function(e) {
    	        	e.preventDefault();
    	        	var firstname = document.getElementById('first').value;
    	        	var lastname = $('#last').val();
    	        	var birthday = $('#birthday').val();
    	        	var usertype = $('#usertype option:selected').val();
    	        	var blicense = $('#blicense').val();
    	        	var bcountry = $('#bcountry option:selected').val();
    	        	var fundid = parseInt($('#fundid').val());
    	        	if($('#nationality option:selected').val()=='other'){
    	        		var nationality = $('#othernationality').val();
    	        	}else{
    	        		var nationality = $('#nationality option:selected').val();
    	        	}
    	        	if($('#issuecountry option:selected').val()=='other'){
    	        		var issuecountry = $('#otherissuecountry').val();
    	        	}else{
    	        		var issuecountry = $('#issuecountry option:selected').val();
    	        	}
    	        	if($('#issuetype option:selected').val()=='other'){
    	        		var issuetype = $('#otherissuetype').val();
    	        	}else{
    	        		var issuetype = $('#issuetype option:selected').val();
    	        	}
    	        	var id = $('#id').val();
    	        	var newphotoori = $('#preview').attr('src');
    	        	var photo = downscaleImage(newphotoori).replace(/^data:(image\/(png|jpg|jpeg|PNG|JPG|JEPG);base64)*,/, "");
    	        	var pdf = uploadpdf.replace(/^data:application\/pdf;base64*,/, "");
    	        	var income = parseInt($('#income').val());
    	        	var asset = parseInt($('#asset').val());
    	        	var phone = parseInt($('#phone').val());
    	        	var areacode = parseInt($('#areacode option:selected').val());
    	        	var email = $('#email').val();
    	        	var wechat = $('#wechat').val();
    	        	var facebook = $('#facebook').val();
    	        	var twitter = $('#twitter').val();
    	        	var weibo = $('#weibo').val();
    	        	var str1 = document.getElementById('str1').value;
    	        	var str2 = document.getElementById('str2').value;
    	        	var city = document.getElementById('city').value;
    	        	var state = document.getElementById('state').value;
    	        	if($('#country option:selected').val()=='other'){
    	        		var country = $('#othercountry').val();
    	        	}else{
    	        		var country = $('#country option:selected').val();
    	        	}
    	        	
    	        	var zipcode = $('#zipcode').val();
    	           	if($('#notsame').is(':checked')){
    	        		var notsame = 'Y';
    	        		var shippingstr1 = document.getElementById('shippingstr1').value;
        	        	var shippingstr2 = document.getElementById('shippingstr2').value;
        	        	var shippingcity = document.getElementById('shippingcity').value;
        	        	var shippingstate = document.getElementById('shippingstate').value;
        	        	if($('#shippingcountry option:selected').val()=='other'){
        	        		var shippingcountry = $('#shippingothercountry').val();
        	        	}else{
        	        		var shippingcountry = $('#shippingcountry option:selected').val();
        	        	}
        	        	var shippingzipcode = $('#shippingzipcode').val();
    	        	}else{
    	        		var notsame = 'N';
    	        		var shippingstr1 = '';
        	        	var shippingstr2 = '';
        	        	var shippingcity = '';
        	        	var shippingstate = '';
        	            var shippingcountry = '';
                        var shippingzipcode = '';
    	        	}
    	        	var ssn=$('#ssn').val();
    	        	var ein=$('#ein').val();
    	        	Liferay.Service(
    	        			  '/AaPortal-portlet.coinuser/add-coin-user',
    	        			  {
    	        			    firstName: firstname,
    	        			    lastName: lastname,
    	        			    birthday: birthday,
    	        			    userType: usertype,
    	        			    brkLics: blicense,
    	        			    brkCountry: bcountry,
    	        			    fundId: parseInt(fundid),
    	        			    nationality: nationality,
    	        			    idIssueCountry: issuecountry,
    	        			    idType: issuetype,
    	        			    idNumber: id,
    	        			    photoImage: photo,
    	        			    certDocStr:pdf,
    	        			    ssn:ssn,
    	        			    ein:ein,
    	        			    income: income,
    	        			    asset: asset,
    	        			    phone: parseInt(phone),
    	        			    areaCode: parseInt(areacode),
    	        			    email: email,
    	        			    wechat: wechat,
    	        			    facebook: facebook,
    	        			    twitter: twitter,
    	        			    weibo: weibo,
    	        			    street1: str1,
    	        			    street2: str2,
    	        			    city: city,
    	        			    state: state,
    	        			    country: country,
    	        			    zipCode: zipcode,
    	        			    isMailDiff: notsame,
    	        			    mailStreet1: shippingstr1,
    	        			    mailStreet2: shippingstr2,
    	        			    mailCity: shippingcity,
    	        			    mailState: shippingstate,
    	        			    mailCountry: shippingcountry,
    	        			    mailZipCode: shippingzipcode
    	        			  },
    	        			  function(obj) {
    	        			    console.log(obj);
    	        			    if(obj.status.code=='00'){
    	        			    	var code = obj.status.code.toString();
									   console.log(codeerror)
									   alert(codeerror[code]);
    	        				    $('#popup').dialog('close');
        	        			 	$('#first').val('');
        	        	        	$('#last').val('');
        	        	        	$('#birthday').val('');
        	        	        	$('#usertype').val('');
        	        	        	$('#blicense').val('');
        	        	        	$('#bcountry').val('');
        	        	        	$('#fundid').val('-1');
        	        	        	$('#nationality').val('');
        	        	        	$('#issuecountry').val('');
        	        	        	$('#issuetype').val('');
        	        	        	$('#id').val('');
        	        	        	$('#preview').attr('src','');
        	        	        	$('#income').val('');
        	        	        	$('#asset').val('');
        	        	        	$('#phone').val('');
        	        	        	$('#areacode').val('');
        	        	        	$('#email').val('');
        	        	        	$('#wechat').val('');
        	        	        	$('#facebook').val('');
        	        	        	$('#twitter').val('');
        	        	        	$('#weibo').val('');
        	        	        	$('#str1').val('');
        	        	        	$('#str2').val('');
        	        	        	$('#city').val('');
        	        	        	$('#state').val('');
        	        	        	$('#country').val('');
        	        	        	$('#zipcode').val('');
        	        	        	$('#ssn').val('');
        	        	        	$('#ein').val('');
        	        	        	$('#shippingstr1').val('');
        	        	        	$('#shippingstr2').val('');
        	        	        	$('#shippingcity').val('');
        	        	        	$('#shippingstate').val('');
        	        	        	$('#shippingothercountry').val('');
        	        	        	$('#shippingcountry option:selected').val('');	
        	        	        	$('#shippingzipcode').val('');
    	        			    }else{
    	        			    	var code = obj.status.code.toString();
									   console.log(codeerror)
									   alert(codeerror[code]);
    	        			    }
    	        		
    	        			  }
    	        			);
    	        	
    	        }
        }]
    	
    	
    })
})
var translation = {
		firstname:"<liferay-ui:message key='firstname'/>",
		lastname:"<liferay-ui:message key='lastname'/>",
		birthday:"<liferay-ui:message key='birthday'/>",
		idType:"<liferay-ui:message key='idType'/>",
		asset:"<liferay-ui:message key='asset'/>",
		idIssueCountry:"<liferay-ui:message key='idIssueCountry'/>",
		addUser:"<liferay-ui:message key='addUser'/>",
		brokerId:"<liferay-ui:message key='brokerId'/>",
		userTypee:"<liferay-ui:message key='userType'/>",
		nationality:"<liferay-ui:message key='nationality'/>",
		fundId:"<liferay-ui:message key='fundId'/>",
		activeFlag:"<liferay-ui:message key='activeFlag'/>",
		income:"<liferay-ui:message key='income'/>",
		userId:"<liferay-ui:message key='userId'/>",
		isValidated:"<liferay-ui:message key='isValidated'/>",
		photoImage:"<liferay-ui:message key='photoImage'/>",
		wechat:"<liferay-ui:message key='wechat'/>",
		areacode:"<liferay-ui:message key='areacode'/>",
		phone:"<liferay-ui:message key='phone'/>",
		email:"<liferay-ui:message key='email'/>",
		idNumber:"<liferay-ui:message key='idNumber'/>",
		facebook:"<liferay-ui:message key='facebook'/>",
		weibo:"<liferay-ui:message key='weibo'/>",
		twitter:"<liferay-ui:message key='twitter'/>",
		str1:"<liferay-ui:message key='str1'/>",
		str2:"<liferay-ui:message key='str2'/>",
		city:"<liferay-ui:message key='city'/>",
		state:"<liferay-ui:message key='state'/>",
		country:"<liferay-ui:message key='country'/>",
		zipcode:"<liferay-ui:message key='zipcode'/>",
		shippingstr1:"<liferay-ui:message key='shippingstr1'/>",
		shippingstr2:"<liferay-ui:message key='shippingstr2'/>",
		shippingcity:"<liferay-ui:message key='shippingcity'/>",
		shippingstate:"<liferay-ui:message key='shippingstate'/>",
		shippingcountry:"<liferay-ui:message key='shippingcountry'/>",
		shippingzipcode:"<liferay-ui:message key='shippingzipcode'/>",
		licenseId:"<liferay-ui:message key='licenseId'/>",
		licenseCountry:"<liferay-ui:message key='licenseCountry'/>",
		userType:"<liferay-ui:message key='userType'/>",
		addUser:"<liferay-ui:message key='addUser'/>",
		licenseId:"<liferay-ui:message key='licenseId'/>",
		licenseCountry:"<liferay-ui:message key='licenseCountry'/>",
		ssn:"<liferay-ui:message key='ssn'/>",
		ein:"<liferay-ui:message key='ein'/>",
		isCreddited:"<liferay-ui:message key='isCreddited'/>",
}
if (Liferay.ThemeDisplay.isSignedIn()) {
	
	$('#my').click(function(){
		$('.myclients').show();
		$('.allclients').css('display','none');
		$('.handelclients').css('display','none');
		$('.verifiedclients').css('display','none');
		myclientinfo();
	})
	$('#all').click(function(){
		$('.allclients').show();
		$('.myclients').css('display','none');
		$('.handelclients').css('display','none');
		$('.verifiedclients').css('display','none');
		allclientinfo();
	})
	$('#process').click(function(){
		$('.handelclients').show();
		$('.allclients').css('display','none');
		$('.myclients').css('display','none');
		$('.verifiedclients').css('display','none');
		saleclients();
	})
	$('#verified').click(function(){
		$('.handelclients').css('display','none');
		$('.allclients').css('display','none');
		$('.myclients').css('display','none');
		$('.verifiedclients').show();
		verifiedclients();
	})
	allclientinfo();
	
function allclientinfo(){
	Liferay.Service(
			  '/AaPortal-portlet.futureuser/get-all-active-future-users',
			  function(obj) {
				  
					  var alldata = [];
				       obj.data.map(function(info){
				    	  var newinfo = {'firstname':info.firstName,'lastname':info.lastName,'email':info.email,'area':info.areaCd,
				    			  'phone':info.phone,'residency':info.residency};
				    	  alldata.push(Object.values(newinfo));
				      }) 
				       createall(alldata);
				  
			  }
			);
}
function myclientinfo(){
	Liferay.Service(
			  '/AaPortal-portlet.futureuser/get-my-active-future-users',
			  function(obj) {
				  console.log(obj);
				  var infodata = [];
			       obj.data.map(function(info){
			    	  var newinfo = {'firstname':info.firstName,'lastname':info.lastName,'email':info.email,'area':info.areaCd,
			    			  'phone':info.phone,'residency':info.residency};
			    	  infodata.push(Object.values(newinfo));
			      }) 
			      console.log(infodata);
			      createtable(infodata);
			  }
			);
}
function saleclients(){
	Liferay.Service(
			  '/AaPortal-portlet.coinuser/get-sale-coin-users',
			  function(obj) {
				var saleclients = [];
				var myallclients = [];
			    if(obj.status.code=='00'){
			    	 obj.data.map(function(info){
			    		 if(info.isValidated=='N'){
				    	  newinfo = {'firstname':info.firstName,'lastname':info.lastName,'birthday':info.birthday,'idType':info.idType,
				    			  'asset':info.asset,'idIssueCountry':info.idIssueCountry,'addUser':info.addUser,'brokerId':info.brokerId,
				    			  'userType':info.userType,'nationality':info.nationality,'ssn':info.ssn,'ein':info.ein,
				    			  'fundId':info.fundId,'activeFlag':info.activeFlag,'income':info.income,'userId':info.userId,
				    			  'isValidated':info.isValidated,'photoImage':info.photoImage,'wechat':info.contact.wechat,'areacode':info.contact.areaCode,
				    			  'phone':info.contact.phone,'email':info.contact.email,'idNumber':info.idNumber,'facebook':info.contact.facebook,'weibo':info.contact.weibo,'twitter':info.contact.twitter,
				    			  'str1':info.contact.street1,'str2':info.contact.street2,'city':info.contact.city,'state':info.contact.state,'country':info.contact.country,'zipcode':info.contact.zipCode,
				    			  'shippingstr1':info.contact.mailstreet1,'shippingstr2':info.contact.mailstreet2,'shippingcity':info.contact.mailcity,'shippingstate':info.contact.mailstate,'shippingcountry':info.contact.mailcountry,'shippingzipcode':info.contact.mailzipCode,
				    			  'licenseId':info.licenseId,'licenseCountry':info.licenseCountry};
				    	  myallclients.push(newinfo);
				    	  saleclients.push(Object.values(newinfo));
			    		 }
			    		 }) 
				      console.log(saleclients);
			    	 
				      createsale(saleclients,myallclients);
			    }else{
					   var code = obj.status.code.toString();
					   console.log(codeerror)
					   alert(codeerror[code]);
				   }
			    
			  }
			);
}
function verifiedclients(){
	Liferay.Service(
			  '/AaPortal-portlet.coinuser/get-sale-coin-users',
			  function(obj) {
				var saleclients = [];
				var myallclients = [];
			    if(obj.status.code=='00'){
			    	 obj.data.map(function(info){
			    		 if(info.isValidated=='Y'){
			    			  newinfo = {'firstname':info.firstName,'lastname':info.lastName,'birthday':info.birthday,'idType':info.idType,
					    			  'asset':info.asset,'idIssueCountry':info.idIssueCountry,'addUser':info.addUser,'brokerId':info.brokerId,
					    			  'userType':info.userType,'nationality':info.nationality,'ssn':info.ssn,'ein':info.ein,
					    			  'fundId':info.fundId,'activeFlag':info.activeFlag,'income':info.income,'userId':info.userId,
					    			  'isValidated':info.isValidated,'photoImage':info.photoImage,'wechat':info.contact.wechat,'areacode':info.contact.areaCode,
					    			  'phone':info.contact.phone,'email':info.contact.email,'idNumber':info.idNumber,'facebook':info.contact.facebook,'weibo':info.contact.weibo,'twitter':info.contact.twitter,
					    			  'str1':info.contact.street1,'str2':info.contact.street2,'city':info.contact.city,'state':info.contact.state,'country':info.contact.country,'zipcode':info.contact.zipCode,
					    			  'shippingstr1':info.contact.mailstreet1,'shippingstr2':info.contact.mailstreet2,'shippingcity':info.contact.mailcity,'shippingstate':info.contact.mailstate,'shippingcountry':info.contact.mailcountry,'shippingzipcode':info.contact.mailzipCode,
					    			  'licenseId':info.licenseId,'licenseCountry':info.licenseCountry,'isCreddited':info.isCreddited};
					    	  myallclients.push(newinfo);
					    	  saleclients.push(Object.values(newinfo));
			    		 }
				      }) 
				      console.log(saleclients);
			    	 createvarified(saleclients,myallclients);
			    }else{
					   var code = obj.status.code.toString();
					   console.log(codeerror)
					   alert(codeerror[code]);
				   }
			    
			  }
			);
}
function createsale(data,myallclients){
	if ( $.fn.DataTable.isDataTable('#handeltable') ) {
		$('#handeltable').html('');
		$('#handeltable').DataTable().destroy();
		  
		  
		  
		}
	var table1 = $('#handeltable').DataTable({
		
    	"iDisplayLength": 25,
         "responsive": true,
         "bDestroy": true,
        "autoFill": false,
        "data":data,
        "columnDefs":[ {
              "targets": -3,
              "data": null,
              "defaultContent": "<div class='verify'><button><liferay-ui:message key='verify1'/></button></div>",
            },
            {
                "targets": -2,
                "data": null,
                "defaultContent": "<div class='checkphoto'><button><liferay-ui:message key='checkphoto'/></button></div>",
              },
              {
                  "targets": -1,
                  "data": null,
                  "defaultContent": "<div class='checkdoc'><button><liferay-ui:message key='checkdoc'/></button></div>",
                },
            {
                "targets": [2,3,4,5,6,7,8,9,10,11,12,13,14,17,18,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39],
                "visible": false,
                "searchable": false,
              }
            ],
            "oLanguage" : {
	                "sLengthMenu": "<liferay-ui:message key='LengthMenu'/>",
	                "sZeroRecords": "<liferay-ui:message key='ZeroRecords'/>",
	                "sInfo": "<liferay-ui:message key='Info'/>",
	                "sInfoEmpty": "<liferay-ui:message key='InfoEmpty'/>",
	                "sInfoFiltered": "<liferay-ui:message key='InfoFiltered'/>",
	                 "sSearch": "<liferay-ui:message key='Search'/>",
	                "oPaginate": {
	                "sFirst": "<liferay-ui:message key='First'/>",
	                "sPrevious": "<liferay-ui:message key='Previous'/>",
	                "sNext": "<liferay-ui:message key='Next'/>",
	                "sLast": "<liferay-ui:message key='Last'/>"
	                 }
	                 },
         /*    "createdRow": function (row, data, dataIndex) {
         	   
                	$(row).find('button:first').text('<liferay-ui:message key="verify1"/>');
        
            }  */
		
	})
	$('#handeltable tbody').on( 'click', '.checkphoto', function (event) {
		var newindex=table1.row($(this).closest('tr')).index();
        if(newindex==undefined){
       	 newindex=table1.row($(this).closest('tr').prev()).index();
        }
        
        window.index=newindex;
    	if($(window).width()>600){
        	  var screenWidth = '500px';
          }else{
        	  var screenWidth = '350px';
          }
        $('#verifyinfo').dialog({
        	id:'photo',
        	title: '照片',
            modal: true,
            width: screenWidth,
            height:500,
            maxHeight: 800,
            resizable:true,
            draggable:true,
            position: { my: "center", at: "center", of: window },
            open:function(){
            	var info = '加载中。。。';
            	$('#detail').html(info);
            	Liferay.Service(
            			  '/AaPortal-portlet.coinuser/get-image',
            			  {
            			    userId: data[newindex][15]
            			  },
            			  function(obj) {
            				  if(Object.values(obj).length==0){
            					  info = '未上传照片';
            					  info += '<img id="newpreview" src="data:image/png;base64,'+obj+'"/></br>';
            				  }else{
            					  info = '<img id="newpreview" src="data:image/png;base64,'+obj+'"/></br>'
            				  }
            			    
            			    if(data[newindex][16]==="N"){
            			    	info += '<input type="file" id="uploadimg" type="file" onchange="previewFile1()" class="input input-medium search-query" id="uploadphoto"  />'
            			    }
            			    $('#detail').html(info);
            			  }
            			);
         
            },
            buttons:[{
        	        text: '提交',
        	        click: function(e) {
        	        	if(data[newindex][16]=="N"){
        	        		var newphotoori = $('#newpreview').attr('src');
            	        	var photo = downscaleImage(newphotoori).replace(/^data:(image\/(png|jpg|jpeg|PNG|JPG|JEPG);base64)*,/, "");
            	        	Liferay.Service(
            	        			  '/AaPortal-portlet.coinuser/update-photo',
            	        			  {
            	        			    coinuserId: data[newindex][15],
            	        			    photoStr: photo
            	        			  },
            	        			  function(obj) {
            	        				  var code = obj.status.code.toString();
   									   console.log(codeerror)
   									   alert(codeerror[code]);
            	        			    $('#verifyinfo').dialog('close');
            	        			  }
            	        			);
        	        	}else{
        	        		$('#verifyinfo').dialog('close');
        	        	}
        	        	
        	        	
        	        }
            }]
        	
        	
        })
		
	})
		$('#handeltable tbody').on( 'click', '.checkdoc', function (event) {
		var newindex=table1.row($(this).closest('tr')).index();
        if(newindex==undefined){
       	 newindex=table1.row($(this).closest('tr').prev()).index();
        }
        window.index=newindex;
        if($(window).width()>600){
      	  var screenWidth = '500px';
        }else{
      	  var screenWidth = '350px';
        }
        $('#verifdoc').dialog({
        	id:'doc',
        	title: '认证信息',
            modal: true,
            width: screenWidth,
            height:500,
            maxHeight: 800,
            resizable:true,
            draggable:true,
            position: { my: "center", at: "center", of: window },
            open:function(){
            	var info = '加载中。。。';
            	$('#docdetail').html(info);
            	Liferay.Service(
            			  '/AaPortal-portlet.coinuser/get-cert-doc',
            			  {
            			    userId: data[newindex][15]
            			  },
            			  function(obj) {
            				  if(Object.values(obj).length==0){
            					  info = '未上传信息';
            					  info += '<iframe id="docpreview" src="data:image/png;base64,'+obj+'"/></br>';
            				  }else{
            					  info = '<iframe id="docpreview" src="data:application/pdf;base64,'+obj+'"/></br>'
            				  }
            			    
            			    if(data[newindex][16]==="N"){
            			    	info += '<input type="file" onchange="uploaddoc()" class="input input-medium search-query" id="docinfo" />'
            			    }
            			    $('#docdetail').html(info);
            			  }
            			);
         
            },
            buttons:[{
        	        text: '提交',
        	        click: function(e) {
        	        	if(data[newindex][16]=="N"){
        	        		
        	        		var pdf = $('#docpreview').prop('src').replace(/^data:application\/pdf;base64*,/, "");
            	        	Liferay.Service(
            	        			  '/AaPortal-portlet.coinuser/update-doc',
            	        			  {
            	        			    coinuserId: data[newindex][15],
            	        			    docStr: pdf
            	        			  },
            	        			  function(obj) {
            	        				  var code = obj.status.code.toString();
   									   console.log(codeerror)
   									   alert(codeerror[code]);
            	        			    $('#verifdoc').dialog('close');
            	        			  }
            	        			);
        	        	}else{
        	        		$('#verifdoc').dialog('close');
        	        	}
        	        	
        	        	
        	        }
            }]
        	
        	
        })
		
	})
		$('#handeltable tbody').on( 'click', '.verify', function (event) {
		var newindex=table1.row($(this).closest('tr')).index();
        if(newindex==undefined){
       	 newindex=table1.row($(this).closest('tr').prev()).index();
        }
        
        window.index=newindex;
        if($(window).width()>600){
        	  var screenWidth = '500px';
          }else{
        	  var screenWidth = '350px';
          }
        $('#verifyinfo').dialog({
        	id:'needverify',
        	title: '',
            modal: true,
            width: screenWidth,
            resizable:true,
            draggable:true,
            height:500,
            maxHeight: 800,
            position: { my: "center", at: "center", of: window },
            open:function(){
            	console.log(data[newindex]);
            	if(data[newindex][16]=='Y'){
            		$('#tijiao').text('提交订单');
            		$('#changeinfo').css('display','none');
            		$('#detail').html('');
            		$('#detail').html("<form><label class='control-label'>ClientId</label><input readonly='readonly' type='text' value="+data[newindex][15]+"></br></form>\
            				<label class='control-label'><liferay-ui:message key='brokerid'/></label><input id='brokerId' type='text' value='-1'></br>\
            				<label class='control-label'><liferay-ui:message key='phase'/></label><input id='phase' type='text' readonly='readonly' value='1'></br>\
            				<label class='control-label'><liferay-ui:message key='unitprice'/></label><input readonly='readonly' id='unitPrice' type='text' ></br>\
            				<label class='control-label'><liferay-ui:message key='saleamt'/></label><input id='saleAmt' type='text' ></br>\
            				<label class='control-label'><liferay-ui:message key='currency'/></label><select id='currency'><option value=''></option><option value='CNY'>CNY</option>\
            				<option value='USD'>USD</option><option value='JPY'>JPY</option><option value='HKD'>HKD</option></select></br>\
            				<label class='control-label'><liferay-ui:message key='totalprice'/></label><input readonly='readonly' id='totalPrice' type='text' ></br>\
            				<label class='control-label'><liferay-ui:message key='promoamt'/></label><input id='promoAmt' type='text' value='0'></br>")
            	}else{
            		$('#tijiao').text('验证');
                	$('#detail').html('');
                	var usertype='';
                	var idtype='';
                	for(var i=0;i<myallclients.length;i++){
                		if(myallclients[i].userId==data[newindex][15]){
                			var keys = Object.keys(myallclients[i]);
                			var values = Object.values(myallclients[i]);
                			var info='';
                			keys.map(function(data,index){
                				console.log(values[index]);
                				var iddata = data+'update';
                				if(data=='addUser'||data=='userId'||data=='isValidated'){
                					info +="<form class='form well form-search'><label class='control-label'>"+translation[data]+"</label>"
                  				  +"<input id="+iddata+" type='text' class='input input-medium search-query' readonly='readonly' value=" + values[index] + "></br>"
                				}else if(data=='userType'){
                					info +="<form class='form well form-search'><label class='control-label'>"+translation[data]+"</label>"+
                					"<select id="+iddata+"><option value='BRK'><liferay-ui:message key='BRK'/></option><option value='FND'><liferay-ui:message key='FND'/></option>\
                			           <option value='IDV'><liferay-ui:message key='IDV'/></option></select></br>";
                			           usertype = values[index];
                					
                				}else if(data=='idType'){
                					if(values[index]!='passport'&&values[index]!='idcard'&&values[index]!='greencard'){
                						info +="<form class='form well form-search'><label class='control-label'>"+translation[data]+"</label>"+
                    					"<select id="+iddata+"><option value='passport'><liferay-ui:message key='passport'/></option><option value='idcard'><liferay-ui:message key='idcard'/></option>\
                    			           <option value='greencard'><liferay-ui:message key='greencard'/></option><option value='"+values[index]+"'>"+values[index]+"</option></select></br>";
                					}else{
                						info +="<form class='form well form-search'><label class='control-label'>"+translation[data]+"</label>"+
                    					"<select id="+iddata+"><option value='passport'><liferay-ui:message key='passport'/></option><option value='idcard'><liferay-ui:message key='idcard'/></option>\
                    			           <option value='greencard'><liferay-ui:message key='greencard'/></option></select></br>";
                					}
                					
                			           idtype = values[index];
                			         
                				}else if(data=='photoImage'||data=='facebook'||data=='twitter'||data=='weibo'||data=='activeFlag'||data=='brokerId'){
                					info +="<form  class='form well form-search'><label style='display:none' class='control-label'></label>"
                  				  +"<input id="+iddata+" type='text' style='display:none' readonly='readonly' class='input input-medium search-query' value="+values[index]+"></br>"
                				}else{
                					info +="<form class='form well form-search'><label class='control-label'>"+translation[data]+"</label>"
                    				  +"<input id="+iddata+" type='text' class='input input-medium search-query' value='" + values[index] + "'></br>"
                				}
                				  
                				  
                			})
                			$('#detail').html(info);
                			//console.log(usertype);
                			$('#userTypeupdate option[value='+usertype+']').prop('selected', true);
                			$('#idTypeupdate option[value='+idtype+']').prop('selected', true);
                			break;
                		}
                	}
            	}
         
            },
            buttons:[{
        	        text:'验证',
        	        id:'tijiao',
        	        click: function(e) {
        	        	if(data[newindex][16]=='Y'){
        	        		console.log($('#currency option:selected').val());
        	        		console.log(parseFloat($('#totalPrice').val()));
        	        		if(parseInt($('#saleAmt').val())>=2000000){
        	        			Liferay.Service(
          	        				  '/AaPortal-portlet.coinorder/create-coin-order',
          	        				  {
          	        				    userId: data[newindex][15],
          	        				    brokerId: parseInt($('#brokerId').val()),
          	        				    phase: parseInt($('#phase').val()),
          	        				    unitPrice: parseFloat($('#unitPrice').val()),
          	        				    totalPrice: parseFloat($('#totalPrice').val()),
          	        				    currency: $('#currency option:selected').val(),
          	        				    saleAmt: parseInt($('#saleAmt').val()),
          	        				    promoAmt: parseInt($('#promoAmt').val())
          	        				  },
          	        				  function(obj) {
          	        					  if(obj.status.code==='00'){
          	        						  alert('订单号:'+obj.data.orderId);
          	        						  $('#verifyinfo').dialog('close');
          	        					  }else{
          	        						  var code = obj.status.code.toString();
         									   console.log(codeerror)
         									   alert(codeerror[code]);
          	        					  }
          	        					
          	        				    console.log(obj);
          	        				  }
          	        				);
        	        		}else{
        	        			alert('购买数量过小');
        	        		}
        	        		
        	        	}else{
        	        		var cf = confirm('确认验证用户？');
        	        		if(cf){
        	        			Liferay.Service(
                	        			  '/AaPortal-portlet.coinuser/verify-coin-user',
                	        			  {
                	        			    coinUserId: data[newindex][15],
                	        			  },
                	        			  function(obj) {
                	        			    console.log(obj);
                	        			    if(obj.status.code=='00'){
                	        			    	$('#verifyinfo').dialog('close');
                    	        			    saleclients();
                	        			    }else{
                	        			    	var code = obj.status.code.toString();
       									   console.log(codeerror)
       									   alert(codeerror[code]);
                	        			    }
                	        			    
                	        			  }
                	        			);
        	        		}
        	        	
        	        	}
        	        	
        	        }
            },{
            	text: '更改',
            	id:'changeinfo',
    	        click: function(e) {
    	        	if(data[newindex][16]=='N'){
    	        		var cf = confirm('确认更改用户信息？');
    	        		if($('#shippingstr1update').val().length!=0&&$('#shippingcityupdate').val().length!=0&&$('#shippingstateupdate').val().length!=0&&$('#shippingcountryupdate').val().length!=0&&$('#shippingzipcodeupdate').val().length!=0){
    	        			var MailDiff = 'Y';
    	        		}else{
    	        			var MailDiff = 'N'
    	        		}
    	        		if(cf){
    	        			Liferay.Service(
    	    	        			  '/AaPortal-portlet.coinuser/update-coin-user',
    	    	        			  {
    	    	        			    coinUserId: data[newindex][15],
    	    	        			    firstName: document.getElementById('firstnameupdate').value,
    	    	        			    lastName: document.getElementById('lastnameupdate').value,
    	    	        			    birthday: $('#birthdayupdate').val(),
    	    	        			    userType: $('#userTypeupdate option:selected').val(),
    	    	        			    brkLics: $('#licenseIdupdate').val(),
    	    	        			    brkCountry: $('#licenseCountryupdate').val(),
    	    	        			    fundId:parseInt($('#fundIdupdate').val()),
    	    	        			    nationality: $('#nationalityupdate').val(),
    	    	        			    ssn:$('#ssnupdate').val(),
    	    	        			    ein:$('#einupdate').val(),
    	    	        			    idIssueCountry: $('#idIssueCountryupdate').val(),
    	    	        			    idType: $('#idTypeupdate option:selected').val(),
    	    	        			    idNumber: $('#idNumberupdate').val(),
    	    	        			    income: $('#incomeupdate').val(),
    	    	        			    asset: $('#assetupdate').val(),
    	    	        			    phone: parseInt($('#phoneupdate').val()),
    	    	        			    areaCode: parseInt($('#areacodeupdate').val()),
    	    	        			    email: $('#emailupdate').val(),
    	    	        			    wechat: $('#wechatupdate').val(),
    	    	        			    facebook: $('#facebookupdate').val(),
    	    	        			    twitter: $('#twitterupdate').val(),
    	    	        			    weibo: $('#weiboupdate').val(),
    	    	        			    street1: document.getElementById('str1update').value,
    	    	        			    street2: document.getElementById('str2update').value,
    	    	        			    city: document.getElementById('cityupdate').value,
    	    	        			    state: document.getElementById('stateupdate').value,
    	    	        			    country: $('#countryupdate').val(),
    	    	        			    zipCode: $('#zipcodeupdate').val(),
    	    	        			    isMailDiff: MailDiff,
    	    	        			    mailStreet1: $('#shippingstr1update').val(),
    	    	        			    mailStreet2: $('#shippingstr2update').val(),
    	    	        			    mailCity: $('#shippingcityupdate').val(),
    	    	        			    mailState: $('#shippingstateupdate').val(),
    	    	        			    mailCountry: $('#shippingcountryupdate').val(),
    	    	        			    mailZipCode: $('#shippingzipcodeupdate').val()
    	    	        			  },
    	    	        			  function(obj) {
    	    	        			    if(obj.status.code==='00'){
    	    	        			    	$('#verifyinfo').dialog('close');
    	          	        			    saleclients();
    	    	        			    }else{
    	    	        			    	var code = obj.status.code.toString();
    										   console.log(codeerror)
    										   alert(codeerror[code]);
    	    	        			    }
    	    	        			  }
    	    	        			)
    	        		}
    	        	}else{
    	        				alert('用户已验证，不能更改信息')
    	        			}
    	        }
            }]
        	
        	
        })
		
	})
}
function createvarified(data,myallclients){
	if ( $.fn.DataTable.isDataTable('#verifiedtable') ) {
		$('#verifiedtable').html('');
		$('#verifiedtable').DataTable().destroy();
		  
		  
		  
		}
	var table1 = $('#verifiedtable').DataTable({
		
    	"iDisplayLength": 25,
         "responsive": true,
         "bDestroy": true,
        "autoFill": false,
        "data":data,
        "columnDefs":[ {
              "targets": -4,
              "data": null,
              "defaultContent": "<div class='credit'><button><liferay-ui:message key='credit1'/></button></div>",
            },
            {
                "targets": -3,
                "data": null,
                "defaultContent": "<div class='verify'><button><liferay-ui:message key='createorder'/></button></div>",
              },
            {
                "targets": -2,
                "data": null,
                "defaultContent": "<div class='checkphoto'><button><liferay-ui:message key='checkphoto'/></button></div>",
              },
              {
                  "targets": -1,
                  "data": null,
                  "defaultContent": "<div class='checkdoc'><button><liferay-ui:message key='checkinfo'/></button></div>",
                },
            {
                "targets": [2,3,4,5,6,7,8,9,10,11,12,13,14,17,18,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39],
                "visible": false,
                "searchable": false,
              }
            ],
            "createdRow": function (row, data, dataIndex) {
            	if(data[40] != "Y"){
            		$(row).find('button:eq(1)').attr('disabled','disabled');
            	}
            },
            "oLanguage" : {
	                "sLengthMenu": "<liferay-ui:message key='LengthMenu'/>",
	                "sZeroRecords": "<liferay-ui:message key='ZeroRecords'/>",
	                "sInfo": "<liferay-ui:message key='Info'/>",
	                "sInfoEmpty": "<liferay-ui:message key='InfoEmpty'/>",
	                "sInfoFiltered": "<liferay-ui:message key='InfoFiltered'/>",
	                 "sSearch": "<liferay-ui:message key='Search'/>",
	                "oPaginate": {
	                "sFirst": "<liferay-ui:message key='First'/>",
	                "sPrevious": "<liferay-ui:message key='Previous'/>",
	                "sNext": "<liferay-ui:message key='Next'/>",
	                "sLast": "<liferay-ui:message key='Last'/>"
	                 }
	                 },
       /*      "createdRow": function (row, data, dataIndex) {
         	   if(data[16] == "Y"){
             	   $(row).find('button:first').text('<liferay-ui:message key="createorder"/>');
                }else{
                	$(row).find('button:first').text('<liferay-ui:message key="verify1"/>');
                }
            }  */
		
	})
		$('#verifiedtable tbody').on( 'click', '.credit', function (event) {
		var newindex=table1.row($(this).closest('tr')).index();
        if(newindex==undefined){
       	 newindex=table1.row($(this).closest('tr').prev()).index();
        }
        
        window.index=newindex;
        if($(window).width()>600){
      	  var screenWidth = '500px';
        }else{
      	  var screenWidth = '350px';
        }
        $('#creditdetail').dialog({
        	title: '',
            modal: true,
            width: screenWidth,
            height:500,
            maxHeight: 800,
            resizable:true,
            draggable:true,
            position: { my: "center", at: "center", of: window },
            buttons:[{
        	        text: '<liferay-ui:message key="pending1"/>',
        	        click: function(e) {
        	        	Liferay.Service(
        	        			  '/AaPortal-portlet.commonfunction/check-password',
        	        			  {
        	        			    password: $('#password').val()
        	        			  },
        	        			  function(obj) {
        	        			    if(obj){
        	        			    	Liferay.Service(
        	        			    			  '/AaPortal-portlet.coinuser/pend-acreditted-coin-user',
        	        			    			  {
        	        			    			    coinUserId: data[newindex][15]
        	        			    			  },
        	        			    			  function(obj) {
        	        			    				  if(obj.status.code==='00'){
        	          	        						  
        	          	        						  $('#creditdetail').dialog('close');
        	          	        						  verifiedclients();
        	          	        						$('#password').val('');
        	          	        					  }else{
        	          	        						  var code = obj.status.code.toString();
        	         									   console.log(codeerror)
        	         									   alert(codeerror[code]);
        	          	        					  }
        	        			    			    
        	        			    			  }
        	        			    			);
        	        			    }else{
        	        			    	alert('密码错误')
        	        			    }
        	        			  }
        	        			);
        	        	
        	        	}
  
            },
            {
    	        text: '<liferay-ui:message key="alreadyverified"/>',
    	        click: function(e) {
    	        	Liferay.Service(
  	        			  '/AaPortal-portlet.commonfunction/check-password',
  	        			  {
  	        			    password: $('#password').val()
  	        			  },
  	        			  function(obj) {
  	        			    if(obj){
  	        			    	Liferay.Service(
	        			    			  '/AaPortal-portlet.coinuser/verify-acreditted-coin-user',
	        			    			  {
	        			    			    coinUserId: data[newindex][15]
	        			    			  },
	        			    			  function(obj) {
	        			    				  if(obj.status.code==='00'){
	          	        						  
	          	        						  $('#creditdetail').dialog('close');
	          	        						  verifiedclients();
	          	        						$('#password').val('');
	          	        					  }else{
	          	        						  var code = obj.status.code.toString();
	         									   console.log(codeerror)
	         									   alert(codeerror[code]);
	          	        					  }
	        			    			    
	        			    			  }
	        			    			);
  	        			    }else{
  	        			    	alert('密码错误')
  	        			    }
  	        			  }
  	        			);
    	        	
    	        	}

        }
            ]
        	
        	
        })
		
	})
	$('#verifiedtable tbody').on( 'click', '.checkphoto', function (event) {
		var newindex=table1.row($(this).closest('tr')).index();
        if(newindex==undefined){
       	 newindex=table1.row($(this).closest('tr').prev()).index();
        }
        
        window.index=newindex;
        if($(window).width()>600){
      	  var screenWidth = '500px';
        }else{
      	  var screenWidth = '350px';
        }
        $('#verifyinfo').dialog({
        	id:'photo',
        	title: '照片',
            modal: true,
            width: screenWidth,
            height:500,
            maxHeight: 800,
            resizable:true,
            draggable:true,
            position: { my: "center", at: "center", of: window },
            open:function(){
            	var info = '加载中。。。';
            	$('#detail').html(info);
            	Liferay.Service(
            			  '/AaPortal-portlet.coinuser/get-image',
            			  {
            			    userId: data[newindex][15]
            			  },
            			  function(obj) {
            				  if(Object.values(obj).length==0){
            					  info = '未上传照片';
            					  info += '<img id="newpreview" src="data:image/png;base64,'+obj+'"/></br>';
            				  }else{
            					  info = '<img id="newpreview" src="data:image/png;base64,'+obj+'"/></br>'
            				  }
            			
            			    $('#detail').html(info);
            			  }
            			);
         
            },
            buttons:[{
        	        text: '关闭',
        	        click: function(e) {
        	        
        	        		$('#verifyinfo').dialog('close');
        	        	}
  
            }]
        	
        	
        })
		
	})
		$('#verifiedtable tbody').on( 'click', '.checkdoc', function (event) {
		var newindex=table1.row($(this).closest('tr')).index();
        if(newindex==undefined){
       	 newindex=table1.row($(this).closest('tr').prev()).index();
        }
        window.index=newindex;
        if($(window).width()>600){
        	  var screenWidth = '500px';
          }else{
        	  var screenWidth = '350px';
          }
        $('#verifdoc').dialog({
        	id:'doc',
        	title: '用户信息',
            modal: true,
            width: screenWidth,
            height:500,
            maxHeight: 800,
            resizable:true,
            draggable:true,
            position: { my: "center", at: "center", of: window },
            open:function(){
            	/* var info = '加载中。。。';
            	$('#docdetail').html(info);
            	Liferay.Service(
            			  '/AaPortal-portlet.coinuser/get-cert-doc',
            			  {
            			    userId: data[newindex][15]
            			  },
            			  function(obj) {
            				  if(Object.values(obj).length==0){
            					  info = '未上传信息';
            					  info += '<iframe id="docpreview" src="data:image/png;base64,'+obj+'"/></br>';
            				  }else{
            					  info = '<iframe id="docpreview" src="data:application/pdf;base64,'+obj+'"/></br>'
            				  }
            			
            			    $('#docdetail').html(info);
            			  }
            			); */
            	$('#docdetail').html('');
            	var usertype='';
            	var idtype='';
            	for(var i=0;i<myallclients.length;i++){
            		if(myallclients[i].userId==data[newindex][15]){
            			var keys = Object.keys(myallclients[i]);
            			var values = Object.values(myallclients[i]);
            			var info='';
            			keys.map(function(data,index){
            				console.log(values[index]);
            			
            			 if(data=='photoImage'||data=='facebook'||data=='twitter'||data=='weibo'||data=='activeFlag'||data=='brokerId'){
            					info +="<form  class='form well form-search'><label style='display:none' class='control-label'></label>"
              				  +"<input  type='text' style='display:none' readonly='readonly' class='input input-medium search-query' value="+values[index]+"></br>"
            				}else{
            					info +="<form class='form well form-search'><label class='control-label'>"+translation[data]+"</label>"
                				  +"<input readonly='readonly' type='text' class='input input-medium search-query' value='" + values[index] + "'></br>"
            				}
            				  
            				  
            			})
            			$('#docdetail').html(info);
            			break;
            		}
            	}
            },
            buttons:[{
        	        text: '关闭',
        	        click: function(e) {
        	        	
        	        		$('#verifdoc').dialog('close');

        	        	        	        	
        	        }
            }]
        	
        	
        })
		
	})
		$('#verifiedtable tbody').on( 'click', '.verify', function (event) {
		//$('#detail').html('');
		var newindex=table1.row($(this).closest('tr')).index();
        if(newindex==undefined){
       	 newindex=table1.row($(this).closest('tr').prev()).index();
        }
        
        window.index=newindex;
        if($(window).width()>600){
      	  var screenWidth = '500px';
        }else{
      	  var screenWidth = '350px';
        }

        $('#submitdetail').dialog({
        	id:'needverify',
        	title: '',
            modal: true,
            width: screenWidth,
            resizable:true,
            draggable:true,
            height:500,
            maxHeight: 800,
            position: { my: "center", at: "center", of: window },
            open:function(){
            	console.log(data[newindex]);
            	userIdforcurrency = data[newindex][15];
        		$('#changeinfo').css('display','none');
        		$('#clientid').val(data[newindex][15]);
        		$('#unitPrice').val('');
        		$('#saleAmt').val('');
        		$('#promoAmt').val('0');
        		$('#totalPrice').val('');
        		$('#currency').val('');           

            },
            buttons:[{
        	        text:'提交',
        	        id:'tijiao',
        	        click: function(e) {
        	        		
        	        		console.log(parseFloat($('#totalPrice').val()));
        	        		if((parseInt($('#saleAmt').val())>=2000000)||(parseInt($('#promoAmt').val())>0&&parseInt($('#saleAmt').val())==0)){
        	        			Liferay.Service(
          	        				  '/AaPortal-portlet.coinorder/create-coin-order',
          	        				  {
          	        				    userId: data[newindex][15],
          	        				    brokerId: parseInt($('#brokerId').val()),
          	        				    phase: parseInt($('#phase').val()),
          	        				    unitPrice: parseFloat($('#unitPrice').val()),
          	        				    totalPrice: parseFloat($('#totalPrice').val()),
          	        				    currency: $('#currency option:selected').val(),
          	        				    saleAmt: parseInt($('#saleAmt').val()),
          	        				    promoAmt: parseInt($('#promoAmt').val())
          	        				  },
          	        				  function(obj) {
          	        					  if(obj.status.code==='00'){
          	        						  alert('订单号:'+obj.data.orderId);
          	        						  $('#submitdetail').dialog('close');
          	        					  }else{
          	        						  var code = obj.status.code.toString();
         									   console.log(codeerror)
         									   alert(codeerror[code]);
          	        					  }
          	        					
          	        				    console.log(obj);
          	        				  }
          	        				);
        	        		}else{
        	        			alert('购买数量过小或者请确认是合格投资人');
        	        		}

        	        	
        	        }
            }]
        	
        	
        })
		
	})
}
var unitprice;
$('#currency').change(function(){
	var currency = $('#currency option:selected').val();
		   Liferay.Service(
				   '/AaPortal-portlet.commonfunction/get-phase-price',
				   {
				     currency: currency,
				     userId: userIdforcurrency
				   },
				   function(obj) {
				     unitprice = obj.data.unitPrice;
				     $('#phase').val(obj.data.phase);
				     $('#unitPrice').val(unitprice);
				     $('#totalPrice').val(($('#saleAmt').val()*unitprice).toFixed(2));	
				   }
				 );
	})

$(document).on('keyup','#saleAmt',function(e){
	   console.log($('#saleAmt').val());
	   var amount = parseInt(e.target.value);
	   $('#totalPrice').val((amount*unitprice).toFixed(2)); 
})
function createall(data){
	if ( $.fn.DataTable.isDataTable('#clienttable') ) {
		 
		  $('#clienttable').DataTable().destroy();
		  
		}
	var table = $('#clienttable').DataTable({
    	"iDisplayLength": 25,
      "responsive": true,
      'destroy':true,
        "autoFill": false,
        "data":data,
        "columnDefs":[ {
              "targets": -1,
              "data": null,
              "defaultContent": "<div class='take'><button><liferay-ui:message key='take'/></button></div>",
            }],
            "oLanguage" : {
	                "sLengthMenu": "<liferay-ui:message key='LengthMenu'/>",
	                "sZeroRecords": "<liferay-ui:message key='ZeroRecords'/>",
	                "sInfo": "<liferay-ui:message key='Info'/>",
	                "sInfoEmpty": "<liferay-ui:message key='InfoEmpty'/>",
	                "sInfoFiltered": "<liferay-ui:message key='InfoFiltered'/>",
	                 "sSearch": "<liferay-ui:message key='Search'/>",
	                "oPaginate": {
	                "sFirst": "<liferay-ui:message key='First'/>",
	                "sPrevious": "<liferay-ui:message key='Previous'/>",
	                "sNext": "<liferay-ui:message key='Next'/>",
	                "sLast": "<liferay-ui:message key='Last'/>"
	                 }
	                 }
		
	})
	$('#clienttable tbody').off( 'click.rowClick' ).on( 'click.rowClick', '.take', function (event) {
		var newindex=table.row($(this).closest('tr')).index();
        if(newindex==undefined){
       	 newindex=table.row($(this).closest('tr').prev()).index();
        }
        
        window.index=newindex;
        Liferay.Service(
        		  '/AaPortal-portlet.futureuser/take-user',
        		  {
        		    email: data[window.index][2]
        		  },
        		  function(obj) {
        			  var code = obj.status.code.toString();
					   console.log(codeerror)
					   alert(codeerror[code]);
        		    allclientinfo();
        		  }
        		);
		
	})
	
	
}
function createtable(data){
	if ( $.fn.DataTable.isDataTable('#usertable') ) {
		 $('#usertable').html('');
		  //$('#usertable').DataTable().clear();
		  //table.clear();
		  $('#usertable').DataTable().destroy()
		    	//console.log(dataSet);
		}
	var table = $('#usertable').DataTable({
    	"iDisplayLength": 25,
    	'responsive': false,
      'destroy':true,
        "autoFill": false,
        "data":data,
        "columnDefs":[ {
            "targets": -3,
            "data": null,
            "defaultContent": "<div class='adddetail'><button><liferay-ui:message key='adddetail'/></button></div>",
          } ,{
              "targets": -2,
              "data": null,
              "defaultContent": "<div class='deactive'><button><liferay-ui:message key='deactive'/></button></div>",
            },{
                "targets": -1,
                "data": null,
                "defaultContent": "<div class='reback'><button><liferay-ui:message key='releaseback'/></button></div>",
              }],
              "oLanguage" : {
	                "sLengthMenu": "<liferay-ui:message key='LengthMenu'/>",
	                "sZeroRecords": "<liferay-ui:message key='ZeroRecords'/>",
	                "sInfo": "<liferay-ui:message key='Info'/>",
	                "sInfoEmpty": "<liferay-ui:message key='InfoEmpty'/>",
	                "sInfoFiltered": "<liferay-ui:message key='InfoFiltered'/>",
	                 "sSearch": "<liferay-ui:message key='Search'/>",
	                "oPaginate": {
	                "sFirst": "<liferay-ui:message key='First'/>",
	                "sPrevious": "<liferay-ui:message key='Previous'/>",
	                "sNext": "<liferay-ui:message key='Next'/>",
	                "sLast": "<liferay-ui:message key='Last'/>"
	                 }
	                 }
		
	})
	$('#usertable tbody').on( 'click','.adddetail', function (event) {
		var newindex=table.row($(this).closest('tr')).index();
        if(newindex==undefined){
       	 newindex=table.row($(this).closest('tr').prev()).index();
        }
        
        window.index=newindex;
        console.log(window.index);
        console.log(data[window.index]);
        $('#first').val(data[window.index][0]);
    	$('#last').val(data[window.index][1]);
    	$('#country').val(data[window.index][5]);
    	if(data[window.index][5]=='US'){
    		$('#ssnalert').css('display','inline-block');
    	}else{
    		$('#ssnalert').css('display','none');
    	}
    	$('#phone').val(data[window.index][4]);
    	$('#areacode option[value='+data[window.index][3]+']').prop('selected', true);
    	$('#email').val(data[window.index][2]);
    	 if($(window).width()>600){
         	  var screenWidth = '500px';
           }else{
         	  var screenWidth = '350px';
           }
        $('#popup').dialog({
        	title: '详细信息',
            modal: true,
            width: screenWidth,
            resizable:true,
            draggable:true,
            height:500,
            maxHeight: 800,
            position: { my: "center", at: "center", of: window },
            buttons:[{
            	id:'save',
        	        text: '保存',
        	        click: function(e) {
        	        	console.log(uploadpdf);
        	        	e.preventDefault();
        	        	var firstname = document.getElementById('first').value;
        	        	var lastname = document.getElementById('last').value;
        	        	var birthday = $('#birthday').val();
        	        	var usertype = $('#usertype option:selected').val();
        	        	var blicense = $('#blicense').val();
        	        	var bcountry = $('#bcountry option:selected').val();
        	        	var fundid = parseInt($('#fundid').val());
        	        	if($('#nationality option:selected').val()=='other'){
        	        		var nationality = $('#othernationality').val();
        	        	}else{
        	        		var nationality = $('#nationality option:selected').val();
        	        	}
        	        	if($('#issuecountry option:selected').val()=='other'){
        	        		var issuecountry = $('#otherissuecountry').val();
        	        	}else{
        	        		var issuecountry = $('#issuecountry option:selected').val();
        	        	}
        	        	if($('#issuetype option:selected').val()=='other'){
        	        		var issuetype = $('#otherissuetype').val();
        	        	}else{
        	        		var issuetype = $('#issuetype option:selected').val();
        	        	}
        	        	var id = $('#id').val();
        	        	var newphotoori = $('#preview').attr('src');
        	        	var photo = downscaleImage(newphotoori).replace(/^data:(image\/(png|jpg|jpeg|PNG|JPG|JEPG);base64)*,/, "");
        	        	var pdf = uploadpdf.replace(/^data:application\/pdf;base64*,/, "");
        	        	var income = $('#income').val();
        	        	var asset = $('#asset').val();
        	        	var phone = parseInt($('#phone').val());
        	        	var areacode = parseInt($('#areacode option:selected').val());
        	        	var email = $('#email').val();
        	        	var wechat = $('#wechat').val();
        	        	var facebook = $('#facebook').val();
        	        	var twitter = $('#twitter').val();
        	        	var weibo = $('#weibo').val();
        	        	var str1 = document.getElementById('str1').value;
        	        	var str2 = document.getElementById('str2').value;
        	        	var city = document.getElementById('city').value;
        	        	var state = document.getElementById('state').value;
        	        	if($('#country option:selected').val()=='other'){
        	        		var country = $('#othercountry').val();
        	        	}else{
        	        		var country = $('#country option:selected').val();
        	        	}
        	        	var zipcode = $('#zipcode').val();
        	         	if($('#notsame').is(':checked')){
        	        		var notsame = 'Y';
        	        		var shippingstr1 = document.getElementById('shippingstr1').value;
            	        	var shippingstr2 = document.getElementById('shippingstr2').value;
            	        	var shippingcity = document.getElementById('shippingcity').value;
            	        	var shippingstate = document.getElementById('shippingstate').value;
            	        	if($('#shippingcountry option:selected').val()=='other'){
            	        		var shippingcountry = $('#shippingothercountry').val();
            	        	}else{
            	        		var shippingcountry = $('#shippingcountry option:selected').val();
            	        	}
            	        	var shippingzipcode = $('#shippingzipcode').val();
        	        	}else{
        	        		var notsame = 'N';
        	        		var shippingstr1 = '';
            	        	var shippingstr2 = '';
            	        	var shippingcity = '';
            	        	var shippingstate = '';
            	            var shippingcountry = '';
                            var shippingzipcode = '';
        	        	}
        	        	var ssn=$('#ssn').val();
        	        	var ein=$('#ein').val();
        	        	Liferay.Service(
        	        			  '/AaPortal-portlet.coinuser/add-coin-user',
        	        			  {
        	        			    firstName: firstname,
        	        			    lastName: lastname,
        	        			    birthday: birthday,
        	        			    userType: usertype,
        	        			    brkLics: blicense,
        	        			    brkCountry: bcountry,
        	        			    fundId: parseInt(fundid),
        	        			    nationality: nationality,
        	        			    idIssueCountry: issuecountry,
        	        			    idType: issuetype,
        	        			    idNumber: id,
        	        			    photoImage: photo,
        	        			    certDocStr:pdf,
        	        			    ssn:ssn,
        	        			    ein:ein,
        	        			    income: income,
        	        			    asset: asset,
        	        			    phone: parseInt(phone),
        	        			    areaCode: parseInt(areacode),
        	        			    email: email,
        	        			    wechat: wechat,
        	        			    facebook: facebook,
        	        			    twitter: twitter,
        	        			    weibo: weibo,
        	        			    street1: str1,
        	        			    street2: str2,
        	        			    city: city,
        	        			    state: state,
        	        			    country: country,
        	        			    zipCode: zipcode,
        	        			    isMailDiff: notsame,
        	        			    mailStreet1: shippingstr1,
        	        			    mailStreet2: shippingstr2,
        	        			    mailCity: shippingcity,
        	        			    mailState: shippingstate,
        	        			    mailCountry: shippingcountry,
        	        			    mailZipCode: shippingzipcode
        	        			    
        	        			  },
        	        			  function(obj) {
        	        			    console.log(obj);
        	        			    if(obj.status.code=='00'){
        	        			    	Liferay.Service(
        	        			    			  '/AaPortal-portlet.futureuser/deactivate-user',
        	        			    			  {
        	        			    			    email: data[window.index][2]
        	        			    			  },
        	        			    			  function(obj) {
        	        			    				  myclientinfo();
        	        			    			  }
        	        			    			);
        	        				    $('#popup').dialog('close');
            	        			 	$('#first').val('');
            	        	        	$('#last').val('');
            	        	        	$('#birthday').val('');
            	        	        	$('#usertype').val('');
            	        	        	$('#blicense').val('');
            	        	        	$('#bcountry').val('');
            	        	        	$('#fundid').val('-1');
            	        	        	$('#nationality').val('');
            	        	        	$('#issuecountry').val('');
            	        	        	$('#issuetype').val('');
            	        	        	$('#id').val('');
            	        	        	$('#preview').attr('src','');
            	        	        	$('#income').val('');
            	        	        	$('#asset').val('');
            	        	        	$('#phone').val('');
            	        	        	$('#areacode').val('');
            	        	        	$('#email').val('');
            	        	        	$('#wechat').val('');
            	        	        	$('#facebook').val('');
            	        	        	$('#twitter').val('');
            	        	        	$('#weibo').val('');
            	        	        	$('#str1').val('');
            	        	        	$('#str2').val('');
            	        	        	$('#city').val('');
            	        	        	$('#state').val('');
            	        	        	$('#country').val('');
            	        	        	$('#zipcode').val('');
            	        	        	$('#ssn').val('');
            	        	        	$('#ein').val('');
            	        	        	$('#shippingstr1').val('');
            	        	        	$('#shippingstr2').val('');
            	        	        	$('#shippingcity').val('');
            	        	        	$('#shippingstate').val('');
            	        	        	$('#shippingothercountry').val('');
            	        	        	$('#shippingcountry option:selected').val('');	
            	        	        	$('#shippingzipcode').val('');
        	        			    }else{
        	        			    	var code = obj.status.code.toString();
 									   console.log(codeerror)
 									   alert(codeerror[code]);
        	        			    }
        	        		
        	        			  }
        	        			);
        	        	
        	        }
            }]
        	
        	
        })
		
	})
	$('#usertable tbody').on( 'click', '.deactive', function (event) {
		var newindex=table.row($(this).closest('tr')).index();
        if(newindex==undefined){
       	 newindex=table.row($(this).closest('tr').prev()).index();
        }
        
        window.index=newindex;
    	Liferay.Service(
    			  '/AaPortal-portlet.futureuser/deactivate-user',
    			  {
    			    email: data[window.index][2]
    			  },
    			  function(obj) {
    				  if(obj.status.code=='00'){
    					  var code = obj.status.code.toString();
						   console.log(codeerror)
						   alert(codeerror[code]);
    					  myclientinfo();
    				  }else{
    					  var code = obj.status.code.toString();
						   console.log(codeerror)
						   alert(codeerror[code]);
    				  }
    				  
    			  }
    			);
		
	})
	$('#usertable tbody').on( 'click', '.reback', function (event) {
		var newindex=table.row($(this).closest('tr')).index();
        if(newindex==undefined){
       	 newindex=table.row($(this).closest('tr').prev()).index();
        }
        
        window.index=newindex;
    	Liferay.Service(
    			  '/AaPortal-portlet.futureuser/release-taken',
    			  {
    			    email: data[window.index][2]
    			  },
    			  function(obj) {
    				  if(obj.status.code=='00'){
    					  var code = obj.status.code.toString();
						   console.log(codeerror)
						   alert(codeerror[code]);
    					  myclientinfo();
    				  }else{
    					  var code = obj.status.code.toString();
						   console.log(codeerror)
						   alert(codeerror[code]);
    				  }
    				  
    			  }
    			);
		
	})
} 
}

</aui:script>
<script>
function uploaddoc(){

	    var file = document.getElementById('docinfo').files[0];
	    var reader  = new FileReader();
	      reader.addEventListener("load", function () {
	    var uploadpdf = reader.result;
	     $('#docpreview').attr('src',reader.result);
	     if(uploadpdf.length>0){
	    	 $('#docpreview').css('display','block')
	     }else{
	    	 $('#docpreview').css('display','none')
	     }
	     }, false);

			if (file) {
			reader.readAsDataURL(file);
			}
	  return uploadpdf;

}
function previewFile(){
    var preview = document.getElementById('preview'); //selects the query named img
    var file    = document.getElementById('photo').files[0]; //sames as here
    var reader  = new FileReader();

    reader.onloadend = function () {
   	 $("#preview").attr("src",reader.result);
   	 $('#previewimg').css('display','block');
        //preview.src = reader.result;
    }

    if (file) {
        //reader.readAsDataURL(file); //reads the data as a URL
       reader.readAsDataURL(file);
    } else {
        preview.src = "";
    }

}
function previewFile1(){
    var preview = document.getElementById('newpreview'); //selects the query named img
    var file    = document.getElementById('uploadimg').files[0]; //sames as here
    var reader  = new FileReader();

    reader.onloadend = function () {
   	 $("#newpreview").attr("src",reader.result);
   	 $('#previewimg').css('display','block');
        //preview.src = reader.result;
    }

    if (file) {
        //reader.readAsDataURL(file); //reads the data as a URL
       reader.readAsDataURL(file);
    } else {
        preview.src = "";
    }

}
function downscaleImage(dataUrl) {
	    "use strict";
	    var image, oldWidth, oldHeight, newHeight, canvas, ctx, newDataUrl;

	    // Provide default values
	    imageType = 'jpeg';
	    imageArguments = 1;

	    // Create a temporary image so that we can compute the height of the downscaled image.
	    image = new Image();
	    image.src = dataUrl;
	    oldWidth = image.width;
	    oldHeight = image.height;
	    newHeight = Math.floor(oldHeight / oldWidth * 900);
       if(newHeight>700){
       	newHeight = 699;
	    }
	    // Create a temporary canvas to draw the downscaled image on.
	    canvas = document.createElement("canvas");
	    canvas.width = 950;
	    canvas.height = newHeight;
	    // Draw the downscaled image on the canvas and return the new data URL.
	    ctx = canvas.getContext("2d");
	    ctx.drawImage(image, 0, 0, 950, newHeight);
	    newDataUrl = canvas.toDataURL(imageType, imageArguments);
	    return newDataUrl;
	}
</script>
