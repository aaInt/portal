/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CoinUser}.
 * </p>
 *
 * @author zl
 * @see CoinUser
 * @generated
 */
public class CoinUserWrapper implements CoinUser, ModelWrapper<CoinUser> {
	public CoinUserWrapper(CoinUser coinUser) {
		_coinUser = coinUser;
	}

	@Override
	public Class<?> getModelClass() {
		return CoinUser.class;
	}

	@Override
	public String getModelClassName() {
		return CoinUser.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("userId", getUserId());
		attributes.put("firstName", getFirstName());
		attributes.put("lastName", getLastName());
		attributes.put("activeFlag", getActiveFlag());
		attributes.put("birthday", getBirthday());
		attributes.put("contactId", getContactId());
		attributes.put("userType", getUserType());
		attributes.put("brokerId", getBrokerId());
		attributes.put("fundId", getFundId());
		attributes.put("nationality", getNationality());
		attributes.put("idIssueCountry", getIdIssueCountry());
		attributes.put("idType", getIdType());
		attributes.put("idNumber", getIdNumber());
		attributes.put("photoImage", getPhotoImage());
		attributes.put("docId", getDocId());
		attributes.put("ssn", getSsn());
		attributes.put("ein", getEin());
		attributes.put("income", getIncome());
		attributes.put("asset", getAsset());
		attributes.put("isValidated", getIsValidated());
		attributes.put("isCreddited", getIsCreddited());
		attributes.put("addTime", getAddTime());
		attributes.put("addUser", getAddUser());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String userId = (String)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String firstName = (String)attributes.get("firstName");

		if (firstName != null) {
			setFirstName(firstName);
		}

		String lastName = (String)attributes.get("lastName");

		if (lastName != null) {
			setLastName(lastName);
		}

		String activeFlag = (String)attributes.get("activeFlag");

		if (activeFlag != null) {
			setActiveFlag(activeFlag);
		}

		String birthday = (String)attributes.get("birthday");

		if (birthday != null) {
			setBirthday(birthday);
		}

		Long contactId = (Long)attributes.get("contactId");

		if (contactId != null) {
			setContactId(contactId);
		}

		String userType = (String)attributes.get("userType");

		if (userType != null) {
			setUserType(userType);
		}

		Long brokerId = (Long)attributes.get("brokerId");

		if (brokerId != null) {
			setBrokerId(brokerId);
		}

		Long fundId = (Long)attributes.get("fundId");

		if (fundId != null) {
			setFundId(fundId);
		}

		String nationality = (String)attributes.get("nationality");

		if (nationality != null) {
			setNationality(nationality);
		}

		String idIssueCountry = (String)attributes.get("idIssueCountry");

		if (idIssueCountry != null) {
			setIdIssueCountry(idIssueCountry);
		}

		String idType = (String)attributes.get("idType");

		if (idType != null) {
			setIdType(idType);
		}

		String idNumber = (String)attributes.get("idNumber");

		if (idNumber != null) {
			setIdNumber(idNumber);
		}

		Long photoImage = (Long)attributes.get("photoImage");

		if (photoImage != null) {
			setPhotoImage(photoImage);
		}

		Long docId = (Long)attributes.get("docId");

		if (docId != null) {
			setDocId(docId);
		}

		String ssn = (String)attributes.get("ssn");

		if (ssn != null) {
			setSsn(ssn);
		}

		String ein = (String)attributes.get("ein");

		if (ein != null) {
			setEin(ein);
		}

		String income = (String)attributes.get("income");

		if (income != null) {
			setIncome(income);
		}

		String asset = (String)attributes.get("asset");

		if (asset != null) {
			setAsset(asset);
		}

		String isValidated = (String)attributes.get("isValidated");

		if (isValidated != null) {
			setIsValidated(isValidated);
		}

		String isCreddited = (String)attributes.get("isCreddited");

		if (isCreddited != null) {
			setIsCreddited(isCreddited);
		}

		Date addTime = (Date)attributes.get("addTime");

		if (addTime != null) {
			setAddTime(addTime);
		}

		Long addUser = (Long)attributes.get("addUser");

		if (addUser != null) {
			setAddUser(addUser);
		}
	}

	/**
	* Returns the primary key of this coin user.
	*
	* @return the primary key of this coin user
	*/
	@Override
	public java.lang.String getPrimaryKey() {
		return _coinUser.getPrimaryKey();
	}

	/**
	* Sets the primary key of this coin user.
	*
	* @param primaryKey the primary key of this coin user
	*/
	@Override
	public void setPrimaryKey(java.lang.String primaryKey) {
		_coinUser.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the user ID of this coin user.
	*
	* @return the user ID of this coin user
	*/
	@Override
	public java.lang.String getUserId() {
		return _coinUser.getUserId();
	}

	/**
	* Sets the user ID of this coin user.
	*
	* @param userId the user ID of this coin user
	*/
	@Override
	public void setUserId(java.lang.String userId) {
		_coinUser.setUserId(userId);
	}

	/**
	* Returns the first name of this coin user.
	*
	* @return the first name of this coin user
	*/
	@Override
	public java.lang.String getFirstName() {
		return _coinUser.getFirstName();
	}

	/**
	* Sets the first name of this coin user.
	*
	* @param firstName the first name of this coin user
	*/
	@Override
	public void setFirstName(java.lang.String firstName) {
		_coinUser.setFirstName(firstName);
	}

	/**
	* Returns the last name of this coin user.
	*
	* @return the last name of this coin user
	*/
	@Override
	public java.lang.String getLastName() {
		return _coinUser.getLastName();
	}

	/**
	* Sets the last name of this coin user.
	*
	* @param lastName the last name of this coin user
	*/
	@Override
	public void setLastName(java.lang.String lastName) {
		_coinUser.setLastName(lastName);
	}

	/**
	* Returns the active flag of this coin user.
	*
	* @return the active flag of this coin user
	*/
	@Override
	public java.lang.String getActiveFlag() {
		return _coinUser.getActiveFlag();
	}

	/**
	* Sets the active flag of this coin user.
	*
	* @param activeFlag the active flag of this coin user
	*/
	@Override
	public void setActiveFlag(java.lang.String activeFlag) {
		_coinUser.setActiveFlag(activeFlag);
	}

	/**
	* Returns the birthday of this coin user.
	*
	* @return the birthday of this coin user
	*/
	@Override
	public java.lang.String getBirthday() {
		return _coinUser.getBirthday();
	}

	/**
	* Sets the birthday of this coin user.
	*
	* @param birthday the birthday of this coin user
	*/
	@Override
	public void setBirthday(java.lang.String birthday) {
		_coinUser.setBirthday(birthday);
	}

	/**
	* Returns the contact ID of this coin user.
	*
	* @return the contact ID of this coin user
	*/
	@Override
	public long getContactId() {
		return _coinUser.getContactId();
	}

	/**
	* Sets the contact ID of this coin user.
	*
	* @param contactId the contact ID of this coin user
	*/
	@Override
	public void setContactId(long contactId) {
		_coinUser.setContactId(contactId);
	}

	/**
	* Returns the user type of this coin user.
	*
	* @return the user type of this coin user
	*/
	@Override
	public java.lang.String getUserType() {
		return _coinUser.getUserType();
	}

	/**
	* Sets the user type of this coin user.
	*
	* @param userType the user type of this coin user
	*/
	@Override
	public void setUserType(java.lang.String userType) {
		_coinUser.setUserType(userType);
	}

	/**
	* Returns the broker ID of this coin user.
	*
	* @return the broker ID of this coin user
	*/
	@Override
	public long getBrokerId() {
		return _coinUser.getBrokerId();
	}

	/**
	* Sets the broker ID of this coin user.
	*
	* @param brokerId the broker ID of this coin user
	*/
	@Override
	public void setBrokerId(long brokerId) {
		_coinUser.setBrokerId(brokerId);
	}

	/**
	* Returns the fund ID of this coin user.
	*
	* @return the fund ID of this coin user
	*/
	@Override
	public long getFundId() {
		return _coinUser.getFundId();
	}

	/**
	* Sets the fund ID of this coin user.
	*
	* @param fundId the fund ID of this coin user
	*/
	@Override
	public void setFundId(long fundId) {
		_coinUser.setFundId(fundId);
	}

	/**
	* Returns the nationality of this coin user.
	*
	* @return the nationality of this coin user
	*/
	@Override
	public java.lang.String getNationality() {
		return _coinUser.getNationality();
	}

	/**
	* Sets the nationality of this coin user.
	*
	* @param nationality the nationality of this coin user
	*/
	@Override
	public void setNationality(java.lang.String nationality) {
		_coinUser.setNationality(nationality);
	}

	/**
	* Returns the id issue country of this coin user.
	*
	* @return the id issue country of this coin user
	*/
	@Override
	public java.lang.String getIdIssueCountry() {
		return _coinUser.getIdIssueCountry();
	}

	/**
	* Sets the id issue country of this coin user.
	*
	* @param idIssueCountry the id issue country of this coin user
	*/
	@Override
	public void setIdIssueCountry(java.lang.String idIssueCountry) {
		_coinUser.setIdIssueCountry(idIssueCountry);
	}

	/**
	* Returns the id type of this coin user.
	*
	* @return the id type of this coin user
	*/
	@Override
	public java.lang.String getIdType() {
		return _coinUser.getIdType();
	}

	/**
	* Sets the id type of this coin user.
	*
	* @param idType the id type of this coin user
	*/
	@Override
	public void setIdType(java.lang.String idType) {
		_coinUser.setIdType(idType);
	}

	/**
	* Returns the id number of this coin user.
	*
	* @return the id number of this coin user
	*/
	@Override
	public java.lang.String getIdNumber() {
		return _coinUser.getIdNumber();
	}

	/**
	* Sets the id number of this coin user.
	*
	* @param idNumber the id number of this coin user
	*/
	@Override
	public void setIdNumber(java.lang.String idNumber) {
		_coinUser.setIdNumber(idNumber);
	}

	/**
	* Returns the photo image of this coin user.
	*
	* @return the photo image of this coin user
	*/
	@Override
	public long getPhotoImage() {
		return _coinUser.getPhotoImage();
	}

	/**
	* Sets the photo image of this coin user.
	*
	* @param photoImage the photo image of this coin user
	*/
	@Override
	public void setPhotoImage(long photoImage) {
		_coinUser.setPhotoImage(photoImage);
	}

	/**
	* Returns the doc ID of this coin user.
	*
	* @return the doc ID of this coin user
	*/
	@Override
	public long getDocId() {
		return _coinUser.getDocId();
	}

	/**
	* Sets the doc ID of this coin user.
	*
	* @param docId the doc ID of this coin user
	*/
	@Override
	public void setDocId(long docId) {
		_coinUser.setDocId(docId);
	}

	/**
	* Returns the ssn of this coin user.
	*
	* @return the ssn of this coin user
	*/
	@Override
	public java.lang.String getSsn() {
		return _coinUser.getSsn();
	}

	/**
	* Sets the ssn of this coin user.
	*
	* @param ssn the ssn of this coin user
	*/
	@Override
	public void setSsn(java.lang.String ssn) {
		_coinUser.setSsn(ssn);
	}

	/**
	* Returns the ein of this coin user.
	*
	* @return the ein of this coin user
	*/
	@Override
	public java.lang.String getEin() {
		return _coinUser.getEin();
	}

	/**
	* Sets the ein of this coin user.
	*
	* @param ein the ein of this coin user
	*/
	@Override
	public void setEin(java.lang.String ein) {
		_coinUser.setEin(ein);
	}

	/**
	* Returns the income of this coin user.
	*
	* @return the income of this coin user
	*/
	@Override
	public java.lang.String getIncome() {
		return _coinUser.getIncome();
	}

	/**
	* Sets the income of this coin user.
	*
	* @param income the income of this coin user
	*/
	@Override
	public void setIncome(java.lang.String income) {
		_coinUser.setIncome(income);
	}

	/**
	* Returns the asset of this coin user.
	*
	* @return the asset of this coin user
	*/
	@Override
	public java.lang.String getAsset() {
		return _coinUser.getAsset();
	}

	/**
	* Sets the asset of this coin user.
	*
	* @param asset the asset of this coin user
	*/
	@Override
	public void setAsset(java.lang.String asset) {
		_coinUser.setAsset(asset);
	}

	/**
	* Returns the is validated of this coin user.
	*
	* @return the is validated of this coin user
	*/
	@Override
	public java.lang.String getIsValidated() {
		return _coinUser.getIsValidated();
	}

	/**
	* Sets the is validated of this coin user.
	*
	* @param isValidated the is validated of this coin user
	*/
	@Override
	public void setIsValidated(java.lang.String isValidated) {
		_coinUser.setIsValidated(isValidated);
	}

	/**
	* Returns the is creddited of this coin user.
	*
	* @return the is creddited of this coin user
	*/
	@Override
	public java.lang.String getIsCreddited() {
		return _coinUser.getIsCreddited();
	}

	/**
	* Sets the is creddited of this coin user.
	*
	* @param isCreddited the is creddited of this coin user
	*/
	@Override
	public void setIsCreddited(java.lang.String isCreddited) {
		_coinUser.setIsCreddited(isCreddited);
	}

	/**
	* Returns the add time of this coin user.
	*
	* @return the add time of this coin user
	*/
	@Override
	public java.util.Date getAddTime() {
		return _coinUser.getAddTime();
	}

	/**
	* Sets the add time of this coin user.
	*
	* @param addTime the add time of this coin user
	*/
	@Override
	public void setAddTime(java.util.Date addTime) {
		_coinUser.setAddTime(addTime);
	}

	/**
	* Returns the add user of this coin user.
	*
	* @return the add user of this coin user
	*/
	@Override
	public long getAddUser() {
		return _coinUser.getAddUser();
	}

	/**
	* Sets the add user of this coin user.
	*
	* @param addUser the add user of this coin user
	*/
	@Override
	public void setAddUser(long addUser) {
		_coinUser.setAddUser(addUser);
	}

	@Override
	public boolean isNew() {
		return _coinUser.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_coinUser.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _coinUser.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_coinUser.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _coinUser.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _coinUser.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_coinUser.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _coinUser.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_coinUser.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_coinUser.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_coinUser.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new CoinUserWrapper((CoinUser)_coinUser.clone());
	}

	@Override
	public int compareTo(com.aw.internal.model.CoinUser coinUser) {
		return _coinUser.compareTo(coinUser);
	}

	@Override
	public int hashCode() {
		return _coinUser.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.aw.internal.model.CoinUser> toCacheModel() {
		return _coinUser.toCacheModel();
	}

	@Override
	public com.aw.internal.model.CoinUser toEscapedModel() {
		return new CoinUserWrapper(_coinUser.toEscapedModel());
	}

	@Override
	public com.aw.internal.model.CoinUser toUnescapedModel() {
		return new CoinUserWrapper(_coinUser.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _coinUser.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _coinUser.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_coinUser.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CoinUserWrapper)) {
			return false;
		}

		CoinUserWrapper coinUserWrapper = (CoinUserWrapper)obj;

		if (Validator.equals(_coinUser, coinUserWrapper._coinUser)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public CoinUser getWrappedCoinUser() {
		return _coinUser;
	}

	@Override
	public CoinUser getWrappedModel() {
		return _coinUser;
	}

	@Override
	public void resetOriginalValues() {
		_coinUser.resetOriginalValues();
	}

	private CoinUser _coinUser;
}