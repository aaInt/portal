/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model;

import com.aw.internal.service.ClpSerializer;
import com.aw.internal.service.FundInfoLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zl
 */
public class FundInfoClp extends BaseModelImpl<FundInfo> implements FundInfo {
	public FundInfoClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return FundInfo.class;
	}

	@Override
	public String getModelClassName() {
		return FundInfo.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _fundId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setFundId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _fundId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("fundId", getFundId());
		attributes.put("name", getName());
		attributes.put("contactId", getContactId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long fundId = (Long)attributes.get("fundId");

		if (fundId != null) {
			setFundId(fundId);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		Long contactId = (Long)attributes.get("contactId");

		if (contactId != null) {
			setContactId(contactId);
		}
	}

	@Override
	public long getFundId() {
		return _fundId;
	}

	@Override
	public void setFundId(long fundId) {
		_fundId = fundId;

		if (_fundInfoRemoteModel != null) {
			try {
				Class<?> clazz = _fundInfoRemoteModel.getClass();

				Method method = clazz.getMethod("setFundId", long.class);

				method.invoke(_fundInfoRemoteModel, fundId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getName() {
		return _name;
	}

	@Override
	public void setName(String name) {
		_name = name;

		if (_fundInfoRemoteModel != null) {
			try {
				Class<?> clazz = _fundInfoRemoteModel.getClass();

				Method method = clazz.getMethod("setName", String.class);

				method.invoke(_fundInfoRemoteModel, name);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getContactId() {
		return _contactId;
	}

	@Override
	public void setContactId(long contactId) {
		_contactId = contactId;

		if (_fundInfoRemoteModel != null) {
			try {
				Class<?> clazz = _fundInfoRemoteModel.getClass();

				Method method = clazz.getMethod("setContactId", long.class);

				method.invoke(_fundInfoRemoteModel, contactId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getFundInfoRemoteModel() {
		return _fundInfoRemoteModel;
	}

	public void setFundInfoRemoteModel(BaseModel<?> fundInfoRemoteModel) {
		_fundInfoRemoteModel = fundInfoRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _fundInfoRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_fundInfoRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			FundInfoLocalServiceUtil.addFundInfo(this);
		}
		else {
			FundInfoLocalServiceUtil.updateFundInfo(this);
		}
	}

	@Override
	public FundInfo toEscapedModel() {
		return (FundInfo)ProxyUtil.newProxyInstance(FundInfo.class.getClassLoader(),
			new Class[] { FundInfo.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		FundInfoClp clone = new FundInfoClp();

		clone.setFundId(getFundId());
		clone.setName(getName());
		clone.setContactId(getContactId());

		return clone;
	}

	@Override
	public int compareTo(FundInfo fundInfo) {
		long primaryKey = fundInfo.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof FundInfoClp)) {
			return false;
		}

		FundInfoClp fundInfo = (FundInfoClp)obj;

		long primaryKey = fundInfo.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{fundId=");
		sb.append(getFundId());
		sb.append(", name=");
		sb.append(getName());
		sb.append(", contactId=");
		sb.append(getContactId());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(13);

		sb.append("<model><model-name>");
		sb.append("com.aw.internal.model.FundInfo");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>fundId</column-name><column-value><![CDATA[");
		sb.append(getFundId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>name</column-name><column-value><![CDATA[");
		sb.append(getName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>contactId</column-name><column-value><![CDATA[");
		sb.append(getContactId());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _fundId;
	private String _name;
	private long _contactId;
	private BaseModel<?> _fundInfoRemoteModel;
	private Class<?> _clpSerializerClass = com.aw.internal.service.ClpSerializer.class;
}