/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model;

import com.aw.internal.service.ClpSerializer;
import com.aw.internal.service.CoinCountLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zl
 */
public class CoinCountClp extends BaseModelImpl<CoinCount> implements CoinCount {
	public CoinCountClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return CoinCount.class;
	}

	@Override
	public String getModelClassName() {
		return CoinCount.class.getName();
	}

	@Override
	public int getPrimaryKey() {
		return _phase;
	}

	@Override
	public void setPrimaryKey(int primaryKey) {
		setPhase(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _phase;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Integer)primaryKeyObj).intValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("phase", getPhase());
		attributes.put("totalAmount", getTotalAmount());
		attributes.put("endFlag", getEndFlag());
		attributes.put("rollToPhase", getRollToPhase());
		attributes.put("rollToAmt", getRollToAmt());
		attributes.put("limit", getLimit());
		attributes.put("rollOverAmt", getRollOverAmt());
		attributes.put("lastOrderId", getLastOrderId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer phase = (Integer)attributes.get("phase");

		if (phase != null) {
			setPhase(phase);
		}

		Long totalAmount = (Long)attributes.get("totalAmount");

		if (totalAmount != null) {
			setTotalAmount(totalAmount);
		}

		String endFlag = (String)attributes.get("endFlag");

		if (endFlag != null) {
			setEndFlag(endFlag);
		}

		Integer rollToPhase = (Integer)attributes.get("rollToPhase");

		if (rollToPhase != null) {
			setRollToPhase(rollToPhase);
		}

		Long rollToAmt = (Long)attributes.get("rollToAmt");

		if (rollToAmt != null) {
			setRollToAmt(rollToAmt);
		}

		Long limit = (Long)attributes.get("limit");

		if (limit != null) {
			setLimit(limit);
		}

		Long rollOverAmt = (Long)attributes.get("rollOverAmt");

		if (rollOverAmt != null) {
			setRollOverAmt(rollOverAmt);
		}

		String lastOrderId = (String)attributes.get("lastOrderId");

		if (lastOrderId != null) {
			setLastOrderId(lastOrderId);
		}
	}

	@Override
	public int getPhase() {
		return _phase;
	}

	@Override
	public void setPhase(int phase) {
		_phase = phase;

		if (_coinCountRemoteModel != null) {
			try {
				Class<?> clazz = _coinCountRemoteModel.getClass();

				Method method = clazz.getMethod("setPhase", int.class);

				method.invoke(_coinCountRemoteModel, phase);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getTotalAmount() {
		return _totalAmount;
	}

	@Override
	public void setTotalAmount(long totalAmount) {
		_totalAmount = totalAmount;

		if (_coinCountRemoteModel != null) {
			try {
				Class<?> clazz = _coinCountRemoteModel.getClass();

				Method method = clazz.getMethod("setTotalAmount", long.class);

				method.invoke(_coinCountRemoteModel, totalAmount);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getEndFlag() {
		return _endFlag;
	}

	@Override
	public void setEndFlag(String endFlag) {
		_endFlag = endFlag;

		if (_coinCountRemoteModel != null) {
			try {
				Class<?> clazz = _coinCountRemoteModel.getClass();

				Method method = clazz.getMethod("setEndFlag", String.class);

				method.invoke(_coinCountRemoteModel, endFlag);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getRollToPhase() {
		return _rollToPhase;
	}

	@Override
	public void setRollToPhase(int rollToPhase) {
		_rollToPhase = rollToPhase;

		if (_coinCountRemoteModel != null) {
			try {
				Class<?> clazz = _coinCountRemoteModel.getClass();

				Method method = clazz.getMethod("setRollToPhase", int.class);

				method.invoke(_coinCountRemoteModel, rollToPhase);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getRollToAmt() {
		return _rollToAmt;
	}

	@Override
	public void setRollToAmt(long rollToAmt) {
		_rollToAmt = rollToAmt;

		if (_coinCountRemoteModel != null) {
			try {
				Class<?> clazz = _coinCountRemoteModel.getClass();

				Method method = clazz.getMethod("setRollToAmt", long.class);

				method.invoke(_coinCountRemoteModel, rollToAmt);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getLimit() {
		return _limit;
	}

	@Override
	public void setLimit(long limit) {
		_limit = limit;

		if (_coinCountRemoteModel != null) {
			try {
				Class<?> clazz = _coinCountRemoteModel.getClass();

				Method method = clazz.getMethod("setLimit", long.class);

				method.invoke(_coinCountRemoteModel, limit);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getRollOverAmt() {
		return _rollOverAmt;
	}

	@Override
	public void setRollOverAmt(long rollOverAmt) {
		_rollOverAmt = rollOverAmt;

		if (_coinCountRemoteModel != null) {
			try {
				Class<?> clazz = _coinCountRemoteModel.getClass();

				Method method = clazz.getMethod("setRollOverAmt", long.class);

				method.invoke(_coinCountRemoteModel, rollOverAmt);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLastOrderId() {
		return _lastOrderId;
	}

	@Override
	public void setLastOrderId(String lastOrderId) {
		_lastOrderId = lastOrderId;

		if (_coinCountRemoteModel != null) {
			try {
				Class<?> clazz = _coinCountRemoteModel.getClass();

				Method method = clazz.getMethod("setLastOrderId", String.class);

				method.invoke(_coinCountRemoteModel, lastOrderId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getCoinCountRemoteModel() {
		return _coinCountRemoteModel;
	}

	public void setCoinCountRemoteModel(BaseModel<?> coinCountRemoteModel) {
		_coinCountRemoteModel = coinCountRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _coinCountRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_coinCountRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			CoinCountLocalServiceUtil.addCoinCount(this);
		}
		else {
			CoinCountLocalServiceUtil.updateCoinCount(this);
		}
	}

	@Override
	public CoinCount toEscapedModel() {
		return (CoinCount)ProxyUtil.newProxyInstance(CoinCount.class.getClassLoader(),
			new Class[] { CoinCount.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		CoinCountClp clone = new CoinCountClp();

		clone.setPhase(getPhase());
		clone.setTotalAmount(getTotalAmount());
		clone.setEndFlag(getEndFlag());
		clone.setRollToPhase(getRollToPhase());
		clone.setRollToAmt(getRollToAmt());
		clone.setLimit(getLimit());
		clone.setRollOverAmt(getRollOverAmt());
		clone.setLastOrderId(getLastOrderId());

		return clone;
	}

	@Override
	public int compareTo(CoinCount coinCount) {
		int primaryKey = coinCount.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CoinCountClp)) {
			return false;
		}

		CoinCountClp coinCount = (CoinCountClp)obj;

		int primaryKey = coinCount.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(17);

		sb.append("{phase=");
		sb.append(getPhase());
		sb.append(", totalAmount=");
		sb.append(getTotalAmount());
		sb.append(", endFlag=");
		sb.append(getEndFlag());
		sb.append(", rollToPhase=");
		sb.append(getRollToPhase());
		sb.append(", rollToAmt=");
		sb.append(getRollToAmt());
		sb.append(", limit=");
		sb.append(getLimit());
		sb.append(", rollOverAmt=");
		sb.append(getRollOverAmt());
		sb.append(", lastOrderId=");
		sb.append(getLastOrderId());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(28);

		sb.append("<model><model-name>");
		sb.append("com.aw.internal.model.CoinCount");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>phase</column-name><column-value><![CDATA[");
		sb.append(getPhase());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>totalAmount</column-name><column-value><![CDATA[");
		sb.append(getTotalAmount());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>endFlag</column-name><column-value><![CDATA[");
		sb.append(getEndFlag());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>rollToPhase</column-name><column-value><![CDATA[");
		sb.append(getRollToPhase());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>rollToAmt</column-name><column-value><![CDATA[");
		sb.append(getRollToAmt());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>limit</column-name><column-value><![CDATA[");
		sb.append(getLimit());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>rollOverAmt</column-name><column-value><![CDATA[");
		sb.append(getRollOverAmt());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>lastOrderId</column-name><column-value><![CDATA[");
		sb.append(getLastOrderId());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private int _phase;
	private long _totalAmount;
	private String _endFlag;
	private int _rollToPhase;
	private long _rollToAmt;
	private long _limit;
	private long _rollOverAmt;
	private String _lastOrderId;
	private BaseModel<?> _coinCountRemoteModel;
	private Class<?> _clpSerializerClass = com.aw.internal.service.ClpSerializer.class;
}