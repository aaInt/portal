/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author zl
 * @generated
 */
public class BrokerInfoSoap implements Serializable {
	public static BrokerInfoSoap toSoapModel(BrokerInfo model) {
		BrokerInfoSoap soapModel = new BrokerInfoSoap();

		soapModel.setBrokerId(model.getBrokerId());
		soapModel.setLicenseId(model.getLicenseId());
		soapModel.setLicenseCountry(model.getLicenseCountry());

		return soapModel;
	}

	public static BrokerInfoSoap[] toSoapModels(BrokerInfo[] models) {
		BrokerInfoSoap[] soapModels = new BrokerInfoSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static BrokerInfoSoap[][] toSoapModels(BrokerInfo[][] models) {
		BrokerInfoSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new BrokerInfoSoap[models.length][models[0].length];
		}
		else {
			soapModels = new BrokerInfoSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static BrokerInfoSoap[] toSoapModels(List<BrokerInfo> models) {
		List<BrokerInfoSoap> soapModels = new ArrayList<BrokerInfoSoap>(models.size());

		for (BrokerInfo model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new BrokerInfoSoap[soapModels.size()]);
	}

	public BrokerInfoSoap() {
	}

	public long getPrimaryKey() {
		return _brokerId;
	}

	public void setPrimaryKey(long pk) {
		setBrokerId(pk);
	}

	public long getBrokerId() {
		return _brokerId;
	}

	public void setBrokerId(long brokerId) {
		_brokerId = brokerId;
	}

	public String getLicenseId() {
		return _licenseId;
	}

	public void setLicenseId(String licenseId) {
		_licenseId = licenseId;
	}

	public String getLicenseCountry() {
		return _licenseCountry;
	}

	public void setLicenseCountry(String licenseCountry) {
		_licenseCountry = licenseCountry;
	}

	private long _brokerId;
	private String _licenseId;
	private String _licenseCountry;
}