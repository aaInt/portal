/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link FailReason}.
 * </p>
 *
 * @author zl
 * @see FailReason
 * @generated
 */
public class FailReasonWrapper implements FailReason, ModelWrapper<FailReason> {
	public FailReasonWrapper(FailReason failReason) {
		_failReason = failReason;
	}

	@Override
	public Class<?> getModelClass() {
		return FailReason.class;
	}

	@Override
	public String getModelClassName() {
		return FailReason.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("rsnCd", getRsnCd());
		attributes.put("desc", getDesc());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String rsnCd = (String)attributes.get("rsnCd");

		if (rsnCd != null) {
			setRsnCd(rsnCd);
		}

		String desc = (String)attributes.get("desc");

		if (desc != null) {
			setDesc(desc);
		}
	}

	/**
	* Returns the primary key of this fail reason.
	*
	* @return the primary key of this fail reason
	*/
	@Override
	public java.lang.String getPrimaryKey() {
		return _failReason.getPrimaryKey();
	}

	/**
	* Sets the primary key of this fail reason.
	*
	* @param primaryKey the primary key of this fail reason
	*/
	@Override
	public void setPrimaryKey(java.lang.String primaryKey) {
		_failReason.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the rsn cd of this fail reason.
	*
	* @return the rsn cd of this fail reason
	*/
	@Override
	public java.lang.String getRsnCd() {
		return _failReason.getRsnCd();
	}

	/**
	* Sets the rsn cd of this fail reason.
	*
	* @param rsnCd the rsn cd of this fail reason
	*/
	@Override
	public void setRsnCd(java.lang.String rsnCd) {
		_failReason.setRsnCd(rsnCd);
	}

	/**
	* Returns the desc of this fail reason.
	*
	* @return the desc of this fail reason
	*/
	@Override
	public java.lang.String getDesc() {
		return _failReason.getDesc();
	}

	/**
	* Sets the desc of this fail reason.
	*
	* @param desc the desc of this fail reason
	*/
	@Override
	public void setDesc(java.lang.String desc) {
		_failReason.setDesc(desc);
	}

	@Override
	public boolean isNew() {
		return _failReason.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_failReason.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _failReason.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_failReason.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _failReason.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _failReason.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_failReason.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _failReason.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_failReason.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_failReason.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_failReason.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new FailReasonWrapper((FailReason)_failReason.clone());
	}

	@Override
	public int compareTo(com.aw.internal.model.FailReason failReason) {
		return _failReason.compareTo(failReason);
	}

	@Override
	public int hashCode() {
		return _failReason.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.aw.internal.model.FailReason> toCacheModel() {
		return _failReason.toCacheModel();
	}

	@Override
	public com.aw.internal.model.FailReason toEscapedModel() {
		return new FailReasonWrapper(_failReason.toEscapedModel());
	}

	@Override
	public com.aw.internal.model.FailReason toUnescapedModel() {
		return new FailReasonWrapper(_failReason.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _failReason.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _failReason.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_failReason.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof FailReasonWrapper)) {
			return false;
		}

		FailReasonWrapper failReasonWrapper = (FailReasonWrapper)obj;

		if (Validator.equals(_failReason, failReasonWrapper._failReason)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public FailReason getWrappedFailReason() {
		return _failReason;
	}

	@Override
	public FailReason getWrappedModel() {
		return _failReason;
	}

	@Override
	public void resetOriginalValues() {
		_failReason.resetOriginalValues();
	}

	private FailReason _failReason;
}