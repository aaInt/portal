/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author zl
 * @generated
 */
public class ActivePhaseSoap implements Serializable {
	public static ActivePhaseSoap toSoapModel(ActivePhase model) {
		ActivePhaseSoap soapModel = new ActivePhaseSoap();

		soapModel.setRegion(model.getRegion());
		soapModel.setSalePhase(model.getSalePhase());

		return soapModel;
	}

	public static ActivePhaseSoap[] toSoapModels(ActivePhase[] models) {
		ActivePhaseSoap[] soapModels = new ActivePhaseSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ActivePhaseSoap[][] toSoapModels(ActivePhase[][] models) {
		ActivePhaseSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ActivePhaseSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ActivePhaseSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ActivePhaseSoap[] toSoapModels(List<ActivePhase> models) {
		List<ActivePhaseSoap> soapModels = new ArrayList<ActivePhaseSoap>(models.size());

		for (ActivePhase model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ActivePhaseSoap[soapModels.size()]);
	}

	public ActivePhaseSoap() {
	}

	public String getPrimaryKey() {
		return _region;
	}

	public void setPrimaryKey(String pk) {
		setRegion(pk);
	}

	public String getRegion() {
		return _region;
	}

	public void setRegion(String region) {
		_region = region;
	}

	public int getSalePhase() {
		return _salePhase;
	}

	public void setSalePhase(int salePhase) {
		_salePhase = salePhase;
	}

	private String _region;
	private int _salePhase;
}