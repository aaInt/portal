/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author zl
 * @generated
 */
public class AaContactSoap implements Serializable {
	public static AaContactSoap toSoapModel(AaContact model) {
		AaContactSoap soapModel = new AaContactSoap();

		soapModel.setContactId(model.getContactId());
		soapModel.setPhone(model.getPhone());
		soapModel.setAreaCode(model.getAreaCode());
		soapModel.setEmail(model.getEmail());
		soapModel.setWechat(model.getWechat());
		soapModel.setFacebook(model.getFacebook());
		soapModel.setTwitter(model.getTwitter());
		soapModel.setWeibo(model.getWeibo());
		soapModel.setStreet1(model.getStreet1());
		soapModel.setStreet2(model.getStreet2());
		soapModel.setCity(model.getCity());
		soapModel.setState(model.getState());
		soapModel.setCountry(model.getCountry());
		soapModel.setZipCode(model.getZipCode());
		soapModel.setIsMailDiff(model.getIsMailDiff());
		soapModel.setMailstreet1(model.getMailstreet1());
		soapModel.setMailstreet2(model.getMailstreet2());
		soapModel.setMailcity(model.getMailcity());
		soapModel.setMailstate(model.getMailstate());
		soapModel.setMailcountry(model.getMailcountry());
		soapModel.setMailzipCode(model.getMailzipCode());

		return soapModel;
	}

	public static AaContactSoap[] toSoapModels(AaContact[] models) {
		AaContactSoap[] soapModels = new AaContactSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static AaContactSoap[][] toSoapModels(AaContact[][] models) {
		AaContactSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new AaContactSoap[models.length][models[0].length];
		}
		else {
			soapModels = new AaContactSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static AaContactSoap[] toSoapModels(List<AaContact> models) {
		List<AaContactSoap> soapModels = new ArrayList<AaContactSoap>(models.size());

		for (AaContact model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new AaContactSoap[soapModels.size()]);
	}

	public AaContactSoap() {
	}

	public long getPrimaryKey() {
		return _contactId;
	}

	public void setPrimaryKey(long pk) {
		setContactId(pk);
	}

	public long getContactId() {
		return _contactId;
	}

	public void setContactId(long contactId) {
		_contactId = contactId;
	}

	public long getPhone() {
		return _phone;
	}

	public void setPhone(long phone) {
		_phone = phone;
	}

	public int getAreaCode() {
		return _areaCode;
	}

	public void setAreaCode(int areaCode) {
		_areaCode = areaCode;
	}

	public String getEmail() {
		return _email;
	}

	public void setEmail(String email) {
		_email = email;
	}

	public String getWechat() {
		return _wechat;
	}

	public void setWechat(String wechat) {
		_wechat = wechat;
	}

	public String getFacebook() {
		return _facebook;
	}

	public void setFacebook(String facebook) {
		_facebook = facebook;
	}

	public String getTwitter() {
		return _twitter;
	}

	public void setTwitter(String twitter) {
		_twitter = twitter;
	}

	public String getWeibo() {
		return _weibo;
	}

	public void setWeibo(String weibo) {
		_weibo = weibo;
	}

	public String getStreet1() {
		return _street1;
	}

	public void setStreet1(String street1) {
		_street1 = street1;
	}

	public String getStreet2() {
		return _street2;
	}

	public void setStreet2(String street2) {
		_street2 = street2;
	}

	public String getCity() {
		return _city;
	}

	public void setCity(String city) {
		_city = city;
	}

	public String getState() {
		return _state;
	}

	public void setState(String state) {
		_state = state;
	}

	public String getCountry() {
		return _country;
	}

	public void setCountry(String country) {
		_country = country;
	}

	public String getZipCode() {
		return _zipCode;
	}

	public void setZipCode(String zipCode) {
		_zipCode = zipCode;
	}

	public String getIsMailDiff() {
		return _isMailDiff;
	}

	public void setIsMailDiff(String isMailDiff) {
		_isMailDiff = isMailDiff;
	}

	public String getMailstreet1() {
		return _mailstreet1;
	}

	public void setMailstreet1(String mailstreet1) {
		_mailstreet1 = mailstreet1;
	}

	public String getMailstreet2() {
		return _mailstreet2;
	}

	public void setMailstreet2(String mailstreet2) {
		_mailstreet2 = mailstreet2;
	}

	public String getMailcity() {
		return _mailcity;
	}

	public void setMailcity(String mailcity) {
		_mailcity = mailcity;
	}

	public String getMailstate() {
		return _mailstate;
	}

	public void setMailstate(String mailstate) {
		_mailstate = mailstate;
	}

	public String getMailcountry() {
		return _mailcountry;
	}

	public void setMailcountry(String mailcountry) {
		_mailcountry = mailcountry;
	}

	public String getMailzipCode() {
		return _mailzipCode;
	}

	public void setMailzipCode(String mailzipCode) {
		_mailzipCode = mailzipCode;
	}

	private long _contactId;
	private long _phone;
	private int _areaCode;
	private String _email;
	private String _wechat;
	private String _facebook;
	private String _twitter;
	private String _weibo;
	private String _street1;
	private String _street2;
	private String _city;
	private String _state;
	private String _country;
	private String _zipCode;
	private String _isMailDiff;
	private String _mailstreet1;
	private String _mailstreet2;
	private String _mailcity;
	private String _mailstate;
	private String _mailcountry;
	private String _mailzipCode;
}