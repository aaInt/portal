/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model;

import com.aw.internal.service.ClpSerializer;
import com.aw.internal.service.FailReasonLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zl
 */
public class FailReasonClp extends BaseModelImpl<FailReason>
	implements FailReason {
	public FailReasonClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return FailReason.class;
	}

	@Override
	public String getModelClassName() {
		return FailReason.class.getName();
	}

	@Override
	public String getPrimaryKey() {
		return _rsnCd;
	}

	@Override
	public void setPrimaryKey(String primaryKey) {
		setRsnCd(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _rsnCd;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((String)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("rsnCd", getRsnCd());
		attributes.put("desc", getDesc());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String rsnCd = (String)attributes.get("rsnCd");

		if (rsnCd != null) {
			setRsnCd(rsnCd);
		}

		String desc = (String)attributes.get("desc");

		if (desc != null) {
			setDesc(desc);
		}
	}

	@Override
	public String getRsnCd() {
		return _rsnCd;
	}

	@Override
	public void setRsnCd(String rsnCd) {
		_rsnCd = rsnCd;

		if (_failReasonRemoteModel != null) {
			try {
				Class<?> clazz = _failReasonRemoteModel.getClass();

				Method method = clazz.getMethod("setRsnCd", String.class);

				method.invoke(_failReasonRemoteModel, rsnCd);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDesc() {
		return _desc;
	}

	@Override
	public void setDesc(String desc) {
		_desc = desc;

		if (_failReasonRemoteModel != null) {
			try {
				Class<?> clazz = _failReasonRemoteModel.getClass();

				Method method = clazz.getMethod("setDesc", String.class);

				method.invoke(_failReasonRemoteModel, desc);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getFailReasonRemoteModel() {
		return _failReasonRemoteModel;
	}

	public void setFailReasonRemoteModel(BaseModel<?> failReasonRemoteModel) {
		_failReasonRemoteModel = failReasonRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _failReasonRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_failReasonRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			FailReasonLocalServiceUtil.addFailReason(this);
		}
		else {
			FailReasonLocalServiceUtil.updateFailReason(this);
		}
	}

	@Override
	public FailReason toEscapedModel() {
		return (FailReason)ProxyUtil.newProxyInstance(FailReason.class.getClassLoader(),
			new Class[] { FailReason.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		FailReasonClp clone = new FailReasonClp();

		clone.setRsnCd(getRsnCd());
		clone.setDesc(getDesc());

		return clone;
	}

	@Override
	public int compareTo(FailReason failReason) {
		String primaryKey = failReason.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof FailReasonClp)) {
			return false;
		}

		FailReasonClp failReason = (FailReasonClp)obj;

		String primaryKey = failReason.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{rsnCd=");
		sb.append(getRsnCd());
		sb.append(", desc=");
		sb.append(getDesc());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(10);

		sb.append("<model><model-name>");
		sb.append("com.aw.internal.model.FailReason");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>rsnCd</column-name><column-value><![CDATA[");
		sb.append(getRsnCd());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>desc</column-name><column-value><![CDATA[");
		sb.append(getDesc());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private String _rsnCd;
	private String _desc;
	private BaseModel<?> _failReasonRemoteModel;
	private Class<?> _clpSerializerClass = com.aw.internal.service.ClpSerializer.class;
}