/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model;

import com.aw.internal.service.AaContactLocalServiceUtil;
import com.aw.internal.service.ClpSerializer;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zl
 */
public class AaContactClp extends BaseModelImpl<AaContact> implements AaContact {
	public AaContactClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return AaContact.class;
	}

	@Override
	public String getModelClassName() {
		return AaContact.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _contactId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setContactId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _contactId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("contactId", getContactId());
		attributes.put("phone", getPhone());
		attributes.put("areaCode", getAreaCode());
		attributes.put("email", getEmail());
		attributes.put("wechat", getWechat());
		attributes.put("facebook", getFacebook());
		attributes.put("twitter", getTwitter());
		attributes.put("weibo", getWeibo());
		attributes.put("street1", getStreet1());
		attributes.put("street2", getStreet2());
		attributes.put("city", getCity());
		attributes.put("state", getState());
		attributes.put("country", getCountry());
		attributes.put("zipCode", getZipCode());
		attributes.put("isMailDiff", getIsMailDiff());
		attributes.put("mailstreet1", getMailstreet1());
		attributes.put("mailstreet2", getMailstreet2());
		attributes.put("mailcity", getMailcity());
		attributes.put("mailstate", getMailstate());
		attributes.put("mailcountry", getMailcountry());
		attributes.put("mailzipCode", getMailzipCode());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long contactId = (Long)attributes.get("contactId");

		if (contactId != null) {
			setContactId(contactId);
		}

		Long phone = (Long)attributes.get("phone");

		if (phone != null) {
			setPhone(phone);
		}

		Integer areaCode = (Integer)attributes.get("areaCode");

		if (areaCode != null) {
			setAreaCode(areaCode);
		}

		String email = (String)attributes.get("email");

		if (email != null) {
			setEmail(email);
		}

		String wechat = (String)attributes.get("wechat");

		if (wechat != null) {
			setWechat(wechat);
		}

		String facebook = (String)attributes.get("facebook");

		if (facebook != null) {
			setFacebook(facebook);
		}

		String twitter = (String)attributes.get("twitter");

		if (twitter != null) {
			setTwitter(twitter);
		}

		String weibo = (String)attributes.get("weibo");

		if (weibo != null) {
			setWeibo(weibo);
		}

		String street1 = (String)attributes.get("street1");

		if (street1 != null) {
			setStreet1(street1);
		}

		String street2 = (String)attributes.get("street2");

		if (street2 != null) {
			setStreet2(street2);
		}

		String city = (String)attributes.get("city");

		if (city != null) {
			setCity(city);
		}

		String state = (String)attributes.get("state");

		if (state != null) {
			setState(state);
		}

		String country = (String)attributes.get("country");

		if (country != null) {
			setCountry(country);
		}

		String zipCode = (String)attributes.get("zipCode");

		if (zipCode != null) {
			setZipCode(zipCode);
		}

		String isMailDiff = (String)attributes.get("isMailDiff");

		if (isMailDiff != null) {
			setIsMailDiff(isMailDiff);
		}

		String mailstreet1 = (String)attributes.get("mailstreet1");

		if (mailstreet1 != null) {
			setMailstreet1(mailstreet1);
		}

		String mailstreet2 = (String)attributes.get("mailstreet2");

		if (mailstreet2 != null) {
			setMailstreet2(mailstreet2);
		}

		String mailcity = (String)attributes.get("mailcity");

		if (mailcity != null) {
			setMailcity(mailcity);
		}

		String mailstate = (String)attributes.get("mailstate");

		if (mailstate != null) {
			setMailstate(mailstate);
		}

		String mailcountry = (String)attributes.get("mailcountry");

		if (mailcountry != null) {
			setMailcountry(mailcountry);
		}

		String mailzipCode = (String)attributes.get("mailzipCode");

		if (mailzipCode != null) {
			setMailzipCode(mailzipCode);
		}
	}

	@Override
	public long getContactId() {
		return _contactId;
	}

	@Override
	public void setContactId(long contactId) {
		_contactId = contactId;

		if (_aaContactRemoteModel != null) {
			try {
				Class<?> clazz = _aaContactRemoteModel.getClass();

				Method method = clazz.getMethod("setContactId", long.class);

				method.invoke(_aaContactRemoteModel, contactId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getPhone() {
		return _phone;
	}

	@Override
	public void setPhone(long phone) {
		_phone = phone;

		if (_aaContactRemoteModel != null) {
			try {
				Class<?> clazz = _aaContactRemoteModel.getClass();

				Method method = clazz.getMethod("setPhone", long.class);

				method.invoke(_aaContactRemoteModel, phone);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getAreaCode() {
		return _areaCode;
	}

	@Override
	public void setAreaCode(int areaCode) {
		_areaCode = areaCode;

		if (_aaContactRemoteModel != null) {
			try {
				Class<?> clazz = _aaContactRemoteModel.getClass();

				Method method = clazz.getMethod("setAreaCode", int.class);

				method.invoke(_aaContactRemoteModel, areaCode);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getEmail() {
		return _email;
	}

	@Override
	public void setEmail(String email) {
		_email = email;

		if (_aaContactRemoteModel != null) {
			try {
				Class<?> clazz = _aaContactRemoteModel.getClass();

				Method method = clazz.getMethod("setEmail", String.class);

				method.invoke(_aaContactRemoteModel, email);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getWechat() {
		return _wechat;
	}

	@Override
	public void setWechat(String wechat) {
		_wechat = wechat;

		if (_aaContactRemoteModel != null) {
			try {
				Class<?> clazz = _aaContactRemoteModel.getClass();

				Method method = clazz.getMethod("setWechat", String.class);

				method.invoke(_aaContactRemoteModel, wechat);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getFacebook() {
		return _facebook;
	}

	@Override
	public void setFacebook(String facebook) {
		_facebook = facebook;

		if (_aaContactRemoteModel != null) {
			try {
				Class<?> clazz = _aaContactRemoteModel.getClass();

				Method method = clazz.getMethod("setFacebook", String.class);

				method.invoke(_aaContactRemoteModel, facebook);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTwitter() {
		return _twitter;
	}

	@Override
	public void setTwitter(String twitter) {
		_twitter = twitter;

		if (_aaContactRemoteModel != null) {
			try {
				Class<?> clazz = _aaContactRemoteModel.getClass();

				Method method = clazz.getMethod("setTwitter", String.class);

				method.invoke(_aaContactRemoteModel, twitter);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getWeibo() {
		return _weibo;
	}

	@Override
	public void setWeibo(String weibo) {
		_weibo = weibo;

		if (_aaContactRemoteModel != null) {
			try {
				Class<?> clazz = _aaContactRemoteModel.getClass();

				Method method = clazz.getMethod("setWeibo", String.class);

				method.invoke(_aaContactRemoteModel, weibo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getStreet1() {
		return _street1;
	}

	@Override
	public void setStreet1(String street1) {
		_street1 = street1;

		if (_aaContactRemoteModel != null) {
			try {
				Class<?> clazz = _aaContactRemoteModel.getClass();

				Method method = clazz.getMethod("setStreet1", String.class);

				method.invoke(_aaContactRemoteModel, street1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getStreet2() {
		return _street2;
	}

	@Override
	public void setStreet2(String street2) {
		_street2 = street2;

		if (_aaContactRemoteModel != null) {
			try {
				Class<?> clazz = _aaContactRemoteModel.getClass();

				Method method = clazz.getMethod("setStreet2", String.class);

				method.invoke(_aaContactRemoteModel, street2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCity() {
		return _city;
	}

	@Override
	public void setCity(String city) {
		_city = city;

		if (_aaContactRemoteModel != null) {
			try {
				Class<?> clazz = _aaContactRemoteModel.getClass();

				Method method = clazz.getMethod("setCity", String.class);

				method.invoke(_aaContactRemoteModel, city);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getState() {
		return _state;
	}

	@Override
	public void setState(String state) {
		_state = state;

		if (_aaContactRemoteModel != null) {
			try {
				Class<?> clazz = _aaContactRemoteModel.getClass();

				Method method = clazz.getMethod("setState", String.class);

				method.invoke(_aaContactRemoteModel, state);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCountry() {
		return _country;
	}

	@Override
	public void setCountry(String country) {
		_country = country;

		if (_aaContactRemoteModel != null) {
			try {
				Class<?> clazz = _aaContactRemoteModel.getClass();

				Method method = clazz.getMethod("setCountry", String.class);

				method.invoke(_aaContactRemoteModel, country);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getZipCode() {
		return _zipCode;
	}

	@Override
	public void setZipCode(String zipCode) {
		_zipCode = zipCode;

		if (_aaContactRemoteModel != null) {
			try {
				Class<?> clazz = _aaContactRemoteModel.getClass();

				Method method = clazz.getMethod("setZipCode", String.class);

				method.invoke(_aaContactRemoteModel, zipCode);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getIsMailDiff() {
		return _isMailDiff;
	}

	@Override
	public void setIsMailDiff(String isMailDiff) {
		_isMailDiff = isMailDiff;

		if (_aaContactRemoteModel != null) {
			try {
				Class<?> clazz = _aaContactRemoteModel.getClass();

				Method method = clazz.getMethod("setIsMailDiff", String.class);

				method.invoke(_aaContactRemoteModel, isMailDiff);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getMailstreet1() {
		return _mailstreet1;
	}

	@Override
	public void setMailstreet1(String mailstreet1) {
		_mailstreet1 = mailstreet1;

		if (_aaContactRemoteModel != null) {
			try {
				Class<?> clazz = _aaContactRemoteModel.getClass();

				Method method = clazz.getMethod("setMailstreet1", String.class);

				method.invoke(_aaContactRemoteModel, mailstreet1);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getMailstreet2() {
		return _mailstreet2;
	}

	@Override
	public void setMailstreet2(String mailstreet2) {
		_mailstreet2 = mailstreet2;

		if (_aaContactRemoteModel != null) {
			try {
				Class<?> clazz = _aaContactRemoteModel.getClass();

				Method method = clazz.getMethod("setMailstreet2", String.class);

				method.invoke(_aaContactRemoteModel, mailstreet2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getMailcity() {
		return _mailcity;
	}

	@Override
	public void setMailcity(String mailcity) {
		_mailcity = mailcity;

		if (_aaContactRemoteModel != null) {
			try {
				Class<?> clazz = _aaContactRemoteModel.getClass();

				Method method = clazz.getMethod("setMailcity", String.class);

				method.invoke(_aaContactRemoteModel, mailcity);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getMailstate() {
		return _mailstate;
	}

	@Override
	public void setMailstate(String mailstate) {
		_mailstate = mailstate;

		if (_aaContactRemoteModel != null) {
			try {
				Class<?> clazz = _aaContactRemoteModel.getClass();

				Method method = clazz.getMethod("setMailstate", String.class);

				method.invoke(_aaContactRemoteModel, mailstate);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getMailcountry() {
		return _mailcountry;
	}

	@Override
	public void setMailcountry(String mailcountry) {
		_mailcountry = mailcountry;

		if (_aaContactRemoteModel != null) {
			try {
				Class<?> clazz = _aaContactRemoteModel.getClass();

				Method method = clazz.getMethod("setMailcountry", String.class);

				method.invoke(_aaContactRemoteModel, mailcountry);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getMailzipCode() {
		return _mailzipCode;
	}

	@Override
	public void setMailzipCode(String mailzipCode) {
		_mailzipCode = mailzipCode;

		if (_aaContactRemoteModel != null) {
			try {
				Class<?> clazz = _aaContactRemoteModel.getClass();

				Method method = clazz.getMethod("setMailzipCode", String.class);

				method.invoke(_aaContactRemoteModel, mailzipCode);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getAaContactRemoteModel() {
		return _aaContactRemoteModel;
	}

	public void setAaContactRemoteModel(BaseModel<?> aaContactRemoteModel) {
		_aaContactRemoteModel = aaContactRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _aaContactRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_aaContactRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			AaContactLocalServiceUtil.addAaContact(this);
		}
		else {
			AaContactLocalServiceUtil.updateAaContact(this);
		}
	}

	@Override
	public AaContact toEscapedModel() {
		return (AaContact)ProxyUtil.newProxyInstance(AaContact.class.getClassLoader(),
			new Class[] { AaContact.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		AaContactClp clone = new AaContactClp();

		clone.setContactId(getContactId());
		clone.setPhone(getPhone());
		clone.setAreaCode(getAreaCode());
		clone.setEmail(getEmail());
		clone.setWechat(getWechat());
		clone.setFacebook(getFacebook());
		clone.setTwitter(getTwitter());
		clone.setWeibo(getWeibo());
		clone.setStreet1(getStreet1());
		clone.setStreet2(getStreet2());
		clone.setCity(getCity());
		clone.setState(getState());
		clone.setCountry(getCountry());
		clone.setZipCode(getZipCode());
		clone.setIsMailDiff(getIsMailDiff());
		clone.setMailstreet1(getMailstreet1());
		clone.setMailstreet2(getMailstreet2());
		clone.setMailcity(getMailcity());
		clone.setMailstate(getMailstate());
		clone.setMailcountry(getMailcountry());
		clone.setMailzipCode(getMailzipCode());

		return clone;
	}

	@Override
	public int compareTo(AaContact aaContact) {
		long primaryKey = aaContact.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof AaContactClp)) {
			return false;
		}

		AaContactClp aaContact = (AaContactClp)obj;

		long primaryKey = aaContact.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(43);

		sb.append("{contactId=");
		sb.append(getContactId());
		sb.append(", phone=");
		sb.append(getPhone());
		sb.append(", areaCode=");
		sb.append(getAreaCode());
		sb.append(", email=");
		sb.append(getEmail());
		sb.append(", wechat=");
		sb.append(getWechat());
		sb.append(", facebook=");
		sb.append(getFacebook());
		sb.append(", twitter=");
		sb.append(getTwitter());
		sb.append(", weibo=");
		sb.append(getWeibo());
		sb.append(", street1=");
		sb.append(getStreet1());
		sb.append(", street2=");
		sb.append(getStreet2());
		sb.append(", city=");
		sb.append(getCity());
		sb.append(", state=");
		sb.append(getState());
		sb.append(", country=");
		sb.append(getCountry());
		sb.append(", zipCode=");
		sb.append(getZipCode());
		sb.append(", isMailDiff=");
		sb.append(getIsMailDiff());
		sb.append(", mailstreet1=");
		sb.append(getMailstreet1());
		sb.append(", mailstreet2=");
		sb.append(getMailstreet2());
		sb.append(", mailcity=");
		sb.append(getMailcity());
		sb.append(", mailstate=");
		sb.append(getMailstate());
		sb.append(", mailcountry=");
		sb.append(getMailcountry());
		sb.append(", mailzipCode=");
		sb.append(getMailzipCode());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(67);

		sb.append("<model><model-name>");
		sb.append("com.aw.internal.model.AaContact");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>contactId</column-name><column-value><![CDATA[");
		sb.append(getContactId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>phone</column-name><column-value><![CDATA[");
		sb.append(getPhone());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>areaCode</column-name><column-value><![CDATA[");
		sb.append(getAreaCode());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>email</column-name><column-value><![CDATA[");
		sb.append(getEmail());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>wechat</column-name><column-value><![CDATA[");
		sb.append(getWechat());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>facebook</column-name><column-value><![CDATA[");
		sb.append(getFacebook());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>twitter</column-name><column-value><![CDATA[");
		sb.append(getTwitter());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>weibo</column-name><column-value><![CDATA[");
		sb.append(getWeibo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>street1</column-name><column-value><![CDATA[");
		sb.append(getStreet1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>street2</column-name><column-value><![CDATA[");
		sb.append(getStreet2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>city</column-name><column-value><![CDATA[");
		sb.append(getCity());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>state</column-name><column-value><![CDATA[");
		sb.append(getState());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>country</column-name><column-value><![CDATA[");
		sb.append(getCountry());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>zipCode</column-name><column-value><![CDATA[");
		sb.append(getZipCode());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>isMailDiff</column-name><column-value><![CDATA[");
		sb.append(getIsMailDiff());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>mailstreet1</column-name><column-value><![CDATA[");
		sb.append(getMailstreet1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>mailstreet2</column-name><column-value><![CDATA[");
		sb.append(getMailstreet2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>mailcity</column-name><column-value><![CDATA[");
		sb.append(getMailcity());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>mailstate</column-name><column-value><![CDATA[");
		sb.append(getMailstate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>mailcountry</column-name><column-value><![CDATA[");
		sb.append(getMailcountry());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>mailzipCode</column-name><column-value><![CDATA[");
		sb.append(getMailzipCode());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _contactId;
	private long _phone;
	private int _areaCode;
	private String _email;
	private String _wechat;
	private String _facebook;
	private String _twitter;
	private String _weibo;
	private String _street1;
	private String _street2;
	private String _city;
	private String _state;
	private String _country;
	private String _zipCode;
	private String _isMailDiff;
	private String _mailstreet1;
	private String _mailstreet2;
	private String _mailcity;
	private String _mailstate;
	private String _mailcountry;
	private String _mailzipCode;
	private BaseModel<?> _aaContactRemoteModel;
	private Class<?> _clpSerializerClass = com.aw.internal.service.ClpSerializer.class;
}