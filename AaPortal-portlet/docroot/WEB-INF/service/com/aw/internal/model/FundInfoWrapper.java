/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link FundInfo}.
 * </p>
 *
 * @author zl
 * @see FundInfo
 * @generated
 */
public class FundInfoWrapper implements FundInfo, ModelWrapper<FundInfo> {
	public FundInfoWrapper(FundInfo fundInfo) {
		_fundInfo = fundInfo;
	}

	@Override
	public Class<?> getModelClass() {
		return FundInfo.class;
	}

	@Override
	public String getModelClassName() {
		return FundInfo.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("fundId", getFundId());
		attributes.put("name", getName());
		attributes.put("contactId", getContactId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long fundId = (Long)attributes.get("fundId");

		if (fundId != null) {
			setFundId(fundId);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		Long contactId = (Long)attributes.get("contactId");

		if (contactId != null) {
			setContactId(contactId);
		}
	}

	/**
	* Returns the primary key of this fund info.
	*
	* @return the primary key of this fund info
	*/
	@Override
	public long getPrimaryKey() {
		return _fundInfo.getPrimaryKey();
	}

	/**
	* Sets the primary key of this fund info.
	*
	* @param primaryKey the primary key of this fund info
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_fundInfo.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the fund ID of this fund info.
	*
	* @return the fund ID of this fund info
	*/
	@Override
	public long getFundId() {
		return _fundInfo.getFundId();
	}

	/**
	* Sets the fund ID of this fund info.
	*
	* @param fundId the fund ID of this fund info
	*/
	@Override
	public void setFundId(long fundId) {
		_fundInfo.setFundId(fundId);
	}

	/**
	* Returns the name of this fund info.
	*
	* @return the name of this fund info
	*/
	@Override
	public java.lang.String getName() {
		return _fundInfo.getName();
	}

	/**
	* Sets the name of this fund info.
	*
	* @param name the name of this fund info
	*/
	@Override
	public void setName(java.lang.String name) {
		_fundInfo.setName(name);
	}

	/**
	* Returns the contact ID of this fund info.
	*
	* @return the contact ID of this fund info
	*/
	@Override
	public long getContactId() {
		return _fundInfo.getContactId();
	}

	/**
	* Sets the contact ID of this fund info.
	*
	* @param contactId the contact ID of this fund info
	*/
	@Override
	public void setContactId(long contactId) {
		_fundInfo.setContactId(contactId);
	}

	@Override
	public boolean isNew() {
		return _fundInfo.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_fundInfo.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _fundInfo.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_fundInfo.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _fundInfo.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _fundInfo.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_fundInfo.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _fundInfo.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_fundInfo.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_fundInfo.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_fundInfo.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new FundInfoWrapper((FundInfo)_fundInfo.clone());
	}

	@Override
	public int compareTo(com.aw.internal.model.FundInfo fundInfo) {
		return _fundInfo.compareTo(fundInfo);
	}

	@Override
	public int hashCode() {
		return _fundInfo.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.aw.internal.model.FundInfo> toCacheModel() {
		return _fundInfo.toCacheModel();
	}

	@Override
	public com.aw.internal.model.FundInfo toEscapedModel() {
		return new FundInfoWrapper(_fundInfo.toEscapedModel());
	}

	@Override
	public com.aw.internal.model.FundInfo toUnescapedModel() {
		return new FundInfoWrapper(_fundInfo.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _fundInfo.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _fundInfo.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_fundInfo.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof FundInfoWrapper)) {
			return false;
		}

		FundInfoWrapper fundInfoWrapper = (FundInfoWrapper)obj;

		if (Validator.equals(_fundInfo, fundInfoWrapper._fundInfo)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public FundInfo getWrappedFundInfo() {
		return _fundInfo;
	}

	@Override
	public FundInfo getWrappedModel() {
		return _fundInfo;
	}

	@Override
	public void resetOriginalValues() {
		_fundInfo.resetOriginalValues();
	}

	private FundInfo _fundInfo;
}