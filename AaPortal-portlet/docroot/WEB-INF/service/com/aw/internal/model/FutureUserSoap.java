/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.aw.internal.service.http.FutureUserServiceSoap}.
 *
 * @author zl
 * @see com.aw.internal.service.http.FutureUserServiceSoap
 * @generated
 */
public class FutureUserSoap implements Serializable {
	public static FutureUserSoap toSoapModel(FutureUser model) {
		FutureUserSoap soapModel = new FutureUserSoap();

		soapModel.setEmail(model.getEmail());
		soapModel.setFirstName(model.getFirstName());
		soapModel.setLastName(model.getLastName());
		soapModel.setResidency(model.getResidency());
		soapModel.setPhone(model.getPhone());
		soapModel.setAreaCode(model.getAreaCode());
		soapModel.setActiveFlag(model.getActiveFlag());
		soapModel.setTakenFlag(model.getTakenFlag());
		soapModel.setOwnerId(model.getOwnerId());
		soapModel.setAddTime(model.getAddTime());

		return soapModel;
	}

	public static FutureUserSoap[] toSoapModels(FutureUser[] models) {
		FutureUserSoap[] soapModels = new FutureUserSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static FutureUserSoap[][] toSoapModels(FutureUser[][] models) {
		FutureUserSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new FutureUserSoap[models.length][models[0].length];
		}
		else {
			soapModels = new FutureUserSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static FutureUserSoap[] toSoapModels(List<FutureUser> models) {
		List<FutureUserSoap> soapModels = new ArrayList<FutureUserSoap>(models.size());

		for (FutureUser model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new FutureUserSoap[soapModels.size()]);
	}

	public FutureUserSoap() {
	}

	public String getPrimaryKey() {
		return _email;
	}

	public void setPrimaryKey(String pk) {
		setEmail(pk);
	}

	public String getEmail() {
		return _email;
	}

	public void setEmail(String email) {
		_email = email;
	}

	public String getFirstName() {
		return _firstName;
	}

	public void setFirstName(String firstName) {
		_firstName = firstName;
	}

	public String getLastName() {
		return _lastName;
	}

	public void setLastName(String lastName) {
		_lastName = lastName;
	}

	public String getResidency() {
		return _residency;
	}

	public void setResidency(String residency) {
		_residency = residency;
	}

	public long getPhone() {
		return _phone;
	}

	public void setPhone(long phone) {
		_phone = phone;
	}

	public int getAreaCode() {
		return _areaCode;
	}

	public void setAreaCode(int areaCode) {
		_areaCode = areaCode;
	}

	public String getActiveFlag() {
		return _activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		_activeFlag = activeFlag;
	}

	public String getTakenFlag() {
		return _takenFlag;
	}

	public void setTakenFlag(String takenFlag) {
		_takenFlag = takenFlag;
	}

	public long getOwnerId() {
		return _ownerId;
	}

	public void setOwnerId(long ownerId) {
		_ownerId = ownerId;
	}

	public Date getAddTime() {
		return _addTime;
	}

	public void setAddTime(Date addTime) {
		_addTime = addTime;
	}

	private String _email;
	private String _firstName;
	private String _lastName;
	private String _residency;
	private long _phone;
	private int _areaCode;
	private String _activeFlag;
	private String _takenFlag;
	private long _ownerId;
	private Date _addTime;
}