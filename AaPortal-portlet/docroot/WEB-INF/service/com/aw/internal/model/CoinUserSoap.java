/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.aw.internal.service.http.CoinUserServiceSoap}.
 *
 * @author zl
 * @see com.aw.internal.service.http.CoinUserServiceSoap
 * @generated
 */
public class CoinUserSoap implements Serializable {
	public static CoinUserSoap toSoapModel(CoinUser model) {
		CoinUserSoap soapModel = new CoinUserSoap();

		soapModel.setUserId(model.getUserId());
		soapModel.setFirstName(model.getFirstName());
		soapModel.setLastName(model.getLastName());
		soapModel.setActiveFlag(model.getActiveFlag());
		soapModel.setBirthday(model.getBirthday());
		soapModel.setContactId(model.getContactId());
		soapModel.setUserType(model.getUserType());
		soapModel.setBrokerId(model.getBrokerId());
		soapModel.setFundId(model.getFundId());
		soapModel.setNationality(model.getNationality());
		soapModel.setIdIssueCountry(model.getIdIssueCountry());
		soapModel.setIdType(model.getIdType());
		soapModel.setIdNumber(model.getIdNumber());
		soapModel.setPhotoImage(model.getPhotoImage());
		soapModel.setDocId(model.getDocId());
		soapModel.setSsn(model.getSsn());
		soapModel.setEin(model.getEin());
		soapModel.setIncome(model.getIncome());
		soapModel.setAsset(model.getAsset());
		soapModel.setIsValidated(model.getIsValidated());
		soapModel.setIsCreddited(model.getIsCreddited());
		soapModel.setAddTime(model.getAddTime());
		soapModel.setAddUser(model.getAddUser());

		return soapModel;
	}

	public static CoinUserSoap[] toSoapModels(CoinUser[] models) {
		CoinUserSoap[] soapModels = new CoinUserSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static CoinUserSoap[][] toSoapModels(CoinUser[][] models) {
		CoinUserSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new CoinUserSoap[models.length][models[0].length];
		}
		else {
			soapModels = new CoinUserSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static CoinUserSoap[] toSoapModels(List<CoinUser> models) {
		List<CoinUserSoap> soapModels = new ArrayList<CoinUserSoap>(models.size());

		for (CoinUser model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new CoinUserSoap[soapModels.size()]);
	}

	public CoinUserSoap() {
	}

	public String getPrimaryKey() {
		return _userId;
	}

	public void setPrimaryKey(String pk) {
		setUserId(pk);
	}

	public String getUserId() {
		return _userId;
	}

	public void setUserId(String userId) {
		_userId = userId;
	}

	public String getFirstName() {
		return _firstName;
	}

	public void setFirstName(String firstName) {
		_firstName = firstName;
	}

	public String getLastName() {
		return _lastName;
	}

	public void setLastName(String lastName) {
		_lastName = lastName;
	}

	public String getActiveFlag() {
		return _activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		_activeFlag = activeFlag;
	}

	public String getBirthday() {
		return _birthday;
	}

	public void setBirthday(String birthday) {
		_birthday = birthday;
	}

	public long getContactId() {
		return _contactId;
	}

	public void setContactId(long contactId) {
		_contactId = contactId;
	}

	public String getUserType() {
		return _userType;
	}

	public void setUserType(String userType) {
		_userType = userType;
	}

	public long getBrokerId() {
		return _brokerId;
	}

	public void setBrokerId(long brokerId) {
		_brokerId = brokerId;
	}

	public long getFundId() {
		return _fundId;
	}

	public void setFundId(long fundId) {
		_fundId = fundId;
	}

	public String getNationality() {
		return _nationality;
	}

	public void setNationality(String nationality) {
		_nationality = nationality;
	}

	public String getIdIssueCountry() {
		return _idIssueCountry;
	}

	public void setIdIssueCountry(String idIssueCountry) {
		_idIssueCountry = idIssueCountry;
	}

	public String getIdType() {
		return _idType;
	}

	public void setIdType(String idType) {
		_idType = idType;
	}

	public String getIdNumber() {
		return _idNumber;
	}

	public void setIdNumber(String idNumber) {
		_idNumber = idNumber;
	}

	public long getPhotoImage() {
		return _photoImage;
	}

	public void setPhotoImage(long photoImage) {
		_photoImage = photoImage;
	}

	public long getDocId() {
		return _docId;
	}

	public void setDocId(long docId) {
		_docId = docId;
	}

	public String getSsn() {
		return _ssn;
	}

	public void setSsn(String ssn) {
		_ssn = ssn;
	}

	public String getEin() {
		return _ein;
	}

	public void setEin(String ein) {
		_ein = ein;
	}

	public String getIncome() {
		return _income;
	}

	public void setIncome(String income) {
		_income = income;
	}

	public String getAsset() {
		return _asset;
	}

	public void setAsset(String asset) {
		_asset = asset;
	}

	public String getIsValidated() {
		return _isValidated;
	}

	public void setIsValidated(String isValidated) {
		_isValidated = isValidated;
	}

	public String getIsCreddited() {
		return _isCreddited;
	}

	public void setIsCreddited(String isCreddited) {
		_isCreddited = isCreddited;
	}

	public Date getAddTime() {
		return _addTime;
	}

	public void setAddTime(Date addTime) {
		_addTime = addTime;
	}

	public long getAddUser() {
		return _addUser;
	}

	public void setAddUser(long addUser) {
		_addUser = addUser;
	}

	private String _userId;
	private String _firstName;
	private String _lastName;
	private String _activeFlag;
	private String _birthday;
	private long _contactId;
	private String _userType;
	private long _brokerId;
	private long _fundId;
	private String _nationality;
	private String _idIssueCountry;
	private String _idType;
	private String _idNumber;
	private long _photoImage;
	private long _docId;
	private String _ssn;
	private String _ein;
	private String _income;
	private String _asset;
	private String _isValidated;
	private String _isCreddited;
	private Date _addTime;
	private long _addUser;
}