/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.sql.Blob;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link ImageStore}.
 * </p>
 *
 * @author zl
 * @see ImageStore
 * @generated
 */
public class ImageStoreWrapper implements ImageStore, ModelWrapper<ImageStore> {
	public ImageStoreWrapper(ImageStore imageStore) {
		_imageStore = imageStore;
	}

	@Override
	public Class<?> getModelClass() {
		return ImageStore.class;
	}

	@Override
	public String getModelClassName() {
		return ImageStore.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("imageId", getImageId());
		attributes.put("content", getContent());
		attributes.put("activeFlag", getActiveFlag());
		attributes.put("addDate", getAddDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long imageId = (Long)attributes.get("imageId");

		if (imageId != null) {
			setImageId(imageId);
		}

		Blob content = (Blob)attributes.get("content");

		if (content != null) {
			setContent(content);
		}

		String activeFlag = (String)attributes.get("activeFlag");

		if (activeFlag != null) {
			setActiveFlag(activeFlag);
		}

		Date addDate = (Date)attributes.get("addDate");

		if (addDate != null) {
			setAddDate(addDate);
		}
	}

	/**
	* Returns the primary key of this image store.
	*
	* @return the primary key of this image store
	*/
	@Override
	public long getPrimaryKey() {
		return _imageStore.getPrimaryKey();
	}

	/**
	* Sets the primary key of this image store.
	*
	* @param primaryKey the primary key of this image store
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_imageStore.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the image ID of this image store.
	*
	* @return the image ID of this image store
	*/
	@Override
	public long getImageId() {
		return _imageStore.getImageId();
	}

	/**
	* Sets the image ID of this image store.
	*
	* @param imageId the image ID of this image store
	*/
	@Override
	public void setImageId(long imageId) {
		_imageStore.setImageId(imageId);
	}

	/**
	* Returns the content of this image store.
	*
	* @return the content of this image store
	*/
	@Override
	public java.sql.Blob getContent() {
		return _imageStore.getContent();
	}

	/**
	* Sets the content of this image store.
	*
	* @param content the content of this image store
	*/
	@Override
	public void setContent(java.sql.Blob content) {
		_imageStore.setContent(content);
	}

	/**
	* Returns the active flag of this image store.
	*
	* @return the active flag of this image store
	*/
	@Override
	public java.lang.String getActiveFlag() {
		return _imageStore.getActiveFlag();
	}

	/**
	* Sets the active flag of this image store.
	*
	* @param activeFlag the active flag of this image store
	*/
	@Override
	public void setActiveFlag(java.lang.String activeFlag) {
		_imageStore.setActiveFlag(activeFlag);
	}

	/**
	* Returns the add date of this image store.
	*
	* @return the add date of this image store
	*/
	@Override
	public java.util.Date getAddDate() {
		return _imageStore.getAddDate();
	}

	/**
	* Sets the add date of this image store.
	*
	* @param addDate the add date of this image store
	*/
	@Override
	public void setAddDate(java.util.Date addDate) {
		_imageStore.setAddDate(addDate);
	}

	@Override
	public boolean isNew() {
		return _imageStore.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_imageStore.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _imageStore.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_imageStore.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _imageStore.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _imageStore.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_imageStore.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _imageStore.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_imageStore.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_imageStore.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_imageStore.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ImageStoreWrapper((ImageStore)_imageStore.clone());
	}

	@Override
	public int compareTo(com.aw.internal.model.ImageStore imageStore) {
		return _imageStore.compareTo(imageStore);
	}

	@Override
	public int hashCode() {
		return _imageStore.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.aw.internal.model.ImageStore> toCacheModel() {
		return _imageStore.toCacheModel();
	}

	@Override
	public com.aw.internal.model.ImageStore toEscapedModel() {
		return new ImageStoreWrapper(_imageStore.toEscapedModel());
	}

	@Override
	public com.aw.internal.model.ImageStore toUnescapedModel() {
		return new ImageStoreWrapper(_imageStore.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _imageStore.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _imageStore.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_imageStore.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ImageStoreWrapper)) {
			return false;
		}

		ImageStoreWrapper imageStoreWrapper = (ImageStoreWrapper)obj;

		if (Validator.equals(_imageStore, imageStoreWrapper._imageStore)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public ImageStore getWrappedImageStore() {
		return _imageStore;
	}

	@Override
	public ImageStore getWrappedModel() {
		return _imageStore;
	}

	@Override
	public void resetOriginalValues() {
		_imageStore.resetOriginalValues();
	}

	private ImageStore _imageStore;
}