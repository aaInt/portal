/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link BrokerInfo}.
 * </p>
 *
 * @author zl
 * @see BrokerInfo
 * @generated
 */
public class BrokerInfoWrapper implements BrokerInfo, ModelWrapper<BrokerInfo> {
	public BrokerInfoWrapper(BrokerInfo brokerInfo) {
		_brokerInfo = brokerInfo;
	}

	@Override
	public Class<?> getModelClass() {
		return BrokerInfo.class;
	}

	@Override
	public String getModelClassName() {
		return BrokerInfo.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("brokerId", getBrokerId());
		attributes.put("licenseId", getLicenseId());
		attributes.put("licenseCountry", getLicenseCountry());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long brokerId = (Long)attributes.get("brokerId");

		if (brokerId != null) {
			setBrokerId(brokerId);
		}

		String licenseId = (String)attributes.get("licenseId");

		if (licenseId != null) {
			setLicenseId(licenseId);
		}

		String licenseCountry = (String)attributes.get("licenseCountry");

		if (licenseCountry != null) {
			setLicenseCountry(licenseCountry);
		}
	}

	/**
	* Returns the primary key of this broker info.
	*
	* @return the primary key of this broker info
	*/
	@Override
	public long getPrimaryKey() {
		return _brokerInfo.getPrimaryKey();
	}

	/**
	* Sets the primary key of this broker info.
	*
	* @param primaryKey the primary key of this broker info
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_brokerInfo.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the broker ID of this broker info.
	*
	* @return the broker ID of this broker info
	*/
	@Override
	public long getBrokerId() {
		return _brokerInfo.getBrokerId();
	}

	/**
	* Sets the broker ID of this broker info.
	*
	* @param brokerId the broker ID of this broker info
	*/
	@Override
	public void setBrokerId(long brokerId) {
		_brokerInfo.setBrokerId(brokerId);
	}

	/**
	* Returns the license ID of this broker info.
	*
	* @return the license ID of this broker info
	*/
	@Override
	public java.lang.String getLicenseId() {
		return _brokerInfo.getLicenseId();
	}

	/**
	* Sets the license ID of this broker info.
	*
	* @param licenseId the license ID of this broker info
	*/
	@Override
	public void setLicenseId(java.lang.String licenseId) {
		_brokerInfo.setLicenseId(licenseId);
	}

	/**
	* Returns the license country of this broker info.
	*
	* @return the license country of this broker info
	*/
	@Override
	public java.lang.String getLicenseCountry() {
		return _brokerInfo.getLicenseCountry();
	}

	/**
	* Sets the license country of this broker info.
	*
	* @param licenseCountry the license country of this broker info
	*/
	@Override
	public void setLicenseCountry(java.lang.String licenseCountry) {
		_brokerInfo.setLicenseCountry(licenseCountry);
	}

	@Override
	public boolean isNew() {
		return _brokerInfo.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_brokerInfo.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _brokerInfo.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_brokerInfo.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _brokerInfo.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _brokerInfo.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_brokerInfo.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _brokerInfo.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_brokerInfo.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_brokerInfo.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_brokerInfo.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new BrokerInfoWrapper((BrokerInfo)_brokerInfo.clone());
	}

	@Override
	public int compareTo(com.aw.internal.model.BrokerInfo brokerInfo) {
		return _brokerInfo.compareTo(brokerInfo);
	}

	@Override
	public int hashCode() {
		return _brokerInfo.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.aw.internal.model.BrokerInfo> toCacheModel() {
		return _brokerInfo.toCacheModel();
	}

	@Override
	public com.aw.internal.model.BrokerInfo toEscapedModel() {
		return new BrokerInfoWrapper(_brokerInfo.toEscapedModel());
	}

	@Override
	public com.aw.internal.model.BrokerInfo toUnescapedModel() {
		return new BrokerInfoWrapper(_brokerInfo.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _brokerInfo.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _brokerInfo.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_brokerInfo.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof BrokerInfoWrapper)) {
			return false;
		}

		BrokerInfoWrapper brokerInfoWrapper = (BrokerInfoWrapper)obj;

		if (Validator.equals(_brokerInfo, brokerInfoWrapper._brokerInfo)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public BrokerInfo getWrappedBrokerInfo() {
		return _brokerInfo;
	}

	@Override
	public BrokerInfo getWrappedModel() {
		return _brokerInfo;
	}

	@Override
	public void resetOriginalValues() {
		_brokerInfo.resetOriginalValues();
	}

	private BrokerInfo _brokerInfo;
}