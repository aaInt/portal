/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model;

import com.aw.internal.service.ClpSerializer;
import com.aw.internal.service.CoinOrderLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author zl
 */
public class CoinOrderClp extends BaseModelImpl<CoinOrder> implements CoinOrder {
	public CoinOrderClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return CoinOrder.class;
	}

	@Override
	public String getModelClassName() {
		return CoinOrder.class.getName();
	}

	@Override
	public String getPrimaryKey() {
		return _orderId;
	}

	@Override
	public void setPrimaryKey(String primaryKey) {
		setOrderId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _orderId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((String)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("orderId", getOrderId());
		attributes.put("userId", getUserId());
		attributes.put("saleRegion", getSaleRegion());
		attributes.put("brokerId", getBrokerId());
		attributes.put("salesId", getSalesId());
		attributes.put("activeFlag", getActiveFlag());
		attributes.put("salePhase", getSalePhase());
		attributes.put("unitPrice", getUnitPrice());
		attributes.put("totalPrice", getTotalPrice());
		attributes.put("currency", getCurrency());
		attributes.put("saleAmount", getSaleAmount());
		attributes.put("promoAmount", getPromoAmount());
		attributes.put("totalAmount", getTotalAmount());
		attributes.put("rsnCd", getRsnCd());
		attributes.put("verifyFlag", getVerifyFlag());
		attributes.put("docSentFlag", getDocSentFlag());
		attributes.put("docCarrier", getDocCarrier());
		attributes.put("docTrackingNo", getDocTrackingNo());
		attributes.put("docSignFlag", getDocSignFlag());
		attributes.put("paidFlag", getPaidFlag());
		attributes.put("approvalFlag", getApprovalFlag());
		attributes.put("certSentFlag", getCertSentFlag());
		attributes.put("certCarrier", getCertCarrier());
		attributes.put("certTrackingNo", getCertTrackingNo());
		attributes.put("completeFlag", getCompleteFlag());
		attributes.put("caseCloser", getCaseCloser());
		attributes.put("addTime", getAddTime());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String orderId = (String)attributes.get("orderId");

		if (orderId != null) {
			setOrderId(orderId);
		}

		String userId = (String)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String saleRegion = (String)attributes.get("saleRegion");

		if (saleRegion != null) {
			setSaleRegion(saleRegion);
		}

		Long brokerId = (Long)attributes.get("brokerId");

		if (brokerId != null) {
			setBrokerId(brokerId);
		}

		Long salesId = (Long)attributes.get("salesId");

		if (salesId != null) {
			setSalesId(salesId);
		}

		String activeFlag = (String)attributes.get("activeFlag");

		if (activeFlag != null) {
			setActiveFlag(activeFlag);
		}

		Integer salePhase = (Integer)attributes.get("salePhase");

		if (salePhase != null) {
			setSalePhase(salePhase);
		}

		Double unitPrice = (Double)attributes.get("unitPrice");

		if (unitPrice != null) {
			setUnitPrice(unitPrice);
		}

		Double totalPrice = (Double)attributes.get("totalPrice");

		if (totalPrice != null) {
			setTotalPrice(totalPrice);
		}

		String currency = (String)attributes.get("currency");

		if (currency != null) {
			setCurrency(currency);
		}

		Long saleAmount = (Long)attributes.get("saleAmount");

		if (saleAmount != null) {
			setSaleAmount(saleAmount);
		}

		Long promoAmount = (Long)attributes.get("promoAmount");

		if (promoAmount != null) {
			setPromoAmount(promoAmount);
		}

		Long totalAmount = (Long)attributes.get("totalAmount");

		if (totalAmount != null) {
			setTotalAmount(totalAmount);
		}

		String rsnCd = (String)attributes.get("rsnCd");

		if (rsnCd != null) {
			setRsnCd(rsnCd);
		}

		String verifyFlag = (String)attributes.get("verifyFlag");

		if (verifyFlag != null) {
			setVerifyFlag(verifyFlag);
		}

		String docSentFlag = (String)attributes.get("docSentFlag");

		if (docSentFlag != null) {
			setDocSentFlag(docSentFlag);
		}

		String docCarrier = (String)attributes.get("docCarrier");

		if (docCarrier != null) {
			setDocCarrier(docCarrier);
		}

		String docTrackingNo = (String)attributes.get("docTrackingNo");

		if (docTrackingNo != null) {
			setDocTrackingNo(docTrackingNo);
		}

		String docSignFlag = (String)attributes.get("docSignFlag");

		if (docSignFlag != null) {
			setDocSignFlag(docSignFlag);
		}

		String paidFlag = (String)attributes.get("paidFlag");

		if (paidFlag != null) {
			setPaidFlag(paidFlag);
		}

		String approvalFlag = (String)attributes.get("approvalFlag");

		if (approvalFlag != null) {
			setApprovalFlag(approvalFlag);
		}

		String certSentFlag = (String)attributes.get("certSentFlag");

		if (certSentFlag != null) {
			setCertSentFlag(certSentFlag);
		}

		String certCarrier = (String)attributes.get("certCarrier");

		if (certCarrier != null) {
			setCertCarrier(certCarrier);
		}

		String certTrackingNo = (String)attributes.get("certTrackingNo");

		if (certTrackingNo != null) {
			setCertTrackingNo(certTrackingNo);
		}

		String completeFlag = (String)attributes.get("completeFlag");

		if (completeFlag != null) {
			setCompleteFlag(completeFlag);
		}

		Long caseCloser = (Long)attributes.get("caseCloser");

		if (caseCloser != null) {
			setCaseCloser(caseCloser);
		}

		Date addTime = (Date)attributes.get("addTime");

		if (addTime != null) {
			setAddTime(addTime);
		}
	}

	@Override
	public String getOrderId() {
		return _orderId;
	}

	@Override
	public void setOrderId(String orderId) {
		_orderId = orderId;

		if (_coinOrderRemoteModel != null) {
			try {
				Class<?> clazz = _coinOrderRemoteModel.getClass();

				Method method = clazz.getMethod("setOrderId", String.class);

				method.invoke(_coinOrderRemoteModel, orderId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUserId() {
		return _userId;
	}

	@Override
	public void setUserId(String userId) {
		_userId = userId;

		if (_coinOrderRemoteModel != null) {
			try {
				Class<?> clazz = _coinOrderRemoteModel.getClass();

				Method method = clazz.getMethod("setUserId", String.class);

				method.invoke(_coinOrderRemoteModel, userId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getSaleRegion() {
		return _saleRegion;
	}

	@Override
	public void setSaleRegion(String saleRegion) {
		_saleRegion = saleRegion;

		if (_coinOrderRemoteModel != null) {
			try {
				Class<?> clazz = _coinOrderRemoteModel.getClass();

				Method method = clazz.getMethod("setSaleRegion", String.class);

				method.invoke(_coinOrderRemoteModel, saleRegion);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getBrokerId() {
		return _brokerId;
	}

	@Override
	public void setBrokerId(long brokerId) {
		_brokerId = brokerId;

		if (_coinOrderRemoteModel != null) {
			try {
				Class<?> clazz = _coinOrderRemoteModel.getClass();

				Method method = clazz.getMethod("setBrokerId", long.class);

				method.invoke(_coinOrderRemoteModel, brokerId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getSalesId() {
		return _salesId;
	}

	@Override
	public void setSalesId(long salesId) {
		_salesId = salesId;

		if (_coinOrderRemoteModel != null) {
			try {
				Class<?> clazz = _coinOrderRemoteModel.getClass();

				Method method = clazz.getMethod("setSalesId", long.class);

				method.invoke(_coinOrderRemoteModel, salesId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getActiveFlag() {
		return _activeFlag;
	}

	@Override
	public void setActiveFlag(String activeFlag) {
		_activeFlag = activeFlag;

		if (_coinOrderRemoteModel != null) {
			try {
				Class<?> clazz = _coinOrderRemoteModel.getClass();

				Method method = clazz.getMethod("setActiveFlag", String.class);

				method.invoke(_coinOrderRemoteModel, activeFlag);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getSalePhase() {
		return _salePhase;
	}

	@Override
	public void setSalePhase(int salePhase) {
		_salePhase = salePhase;

		if (_coinOrderRemoteModel != null) {
			try {
				Class<?> clazz = _coinOrderRemoteModel.getClass();

				Method method = clazz.getMethod("setSalePhase", int.class);

				method.invoke(_coinOrderRemoteModel, salePhase);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getUnitPrice() {
		return _unitPrice;
	}

	@Override
	public void setUnitPrice(double unitPrice) {
		_unitPrice = unitPrice;

		if (_coinOrderRemoteModel != null) {
			try {
				Class<?> clazz = _coinOrderRemoteModel.getClass();

				Method method = clazz.getMethod("setUnitPrice", double.class);

				method.invoke(_coinOrderRemoteModel, unitPrice);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getTotalPrice() {
		return _totalPrice;
	}

	@Override
	public void setTotalPrice(double totalPrice) {
		_totalPrice = totalPrice;

		if (_coinOrderRemoteModel != null) {
			try {
				Class<?> clazz = _coinOrderRemoteModel.getClass();

				Method method = clazz.getMethod("setTotalPrice", double.class);

				method.invoke(_coinOrderRemoteModel, totalPrice);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCurrency() {
		return _currency;
	}

	@Override
	public void setCurrency(String currency) {
		_currency = currency;

		if (_coinOrderRemoteModel != null) {
			try {
				Class<?> clazz = _coinOrderRemoteModel.getClass();

				Method method = clazz.getMethod("setCurrency", String.class);

				method.invoke(_coinOrderRemoteModel, currency);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getSaleAmount() {
		return _saleAmount;
	}

	@Override
	public void setSaleAmount(long saleAmount) {
		_saleAmount = saleAmount;

		if (_coinOrderRemoteModel != null) {
			try {
				Class<?> clazz = _coinOrderRemoteModel.getClass();

				Method method = clazz.getMethod("setSaleAmount", long.class);

				method.invoke(_coinOrderRemoteModel, saleAmount);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getPromoAmount() {
		return _promoAmount;
	}

	@Override
	public void setPromoAmount(long promoAmount) {
		_promoAmount = promoAmount;

		if (_coinOrderRemoteModel != null) {
			try {
				Class<?> clazz = _coinOrderRemoteModel.getClass();

				Method method = clazz.getMethod("setPromoAmount", long.class);

				method.invoke(_coinOrderRemoteModel, promoAmount);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getTotalAmount() {
		return _totalAmount;
	}

	@Override
	public void setTotalAmount(long totalAmount) {
		_totalAmount = totalAmount;

		if (_coinOrderRemoteModel != null) {
			try {
				Class<?> clazz = _coinOrderRemoteModel.getClass();

				Method method = clazz.getMethod("setTotalAmount", long.class);

				method.invoke(_coinOrderRemoteModel, totalAmount);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getRsnCd() {
		return _rsnCd;
	}

	@Override
	public void setRsnCd(String rsnCd) {
		_rsnCd = rsnCd;

		if (_coinOrderRemoteModel != null) {
			try {
				Class<?> clazz = _coinOrderRemoteModel.getClass();

				Method method = clazz.getMethod("setRsnCd", String.class);

				method.invoke(_coinOrderRemoteModel, rsnCd);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getVerifyFlag() {
		return _verifyFlag;
	}

	@Override
	public void setVerifyFlag(String verifyFlag) {
		_verifyFlag = verifyFlag;

		if (_coinOrderRemoteModel != null) {
			try {
				Class<?> clazz = _coinOrderRemoteModel.getClass();

				Method method = clazz.getMethod("setVerifyFlag", String.class);

				method.invoke(_coinOrderRemoteModel, verifyFlag);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDocSentFlag() {
		return _docSentFlag;
	}

	@Override
	public void setDocSentFlag(String docSentFlag) {
		_docSentFlag = docSentFlag;

		if (_coinOrderRemoteModel != null) {
			try {
				Class<?> clazz = _coinOrderRemoteModel.getClass();

				Method method = clazz.getMethod("setDocSentFlag", String.class);

				method.invoke(_coinOrderRemoteModel, docSentFlag);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDocCarrier() {
		return _docCarrier;
	}

	@Override
	public void setDocCarrier(String docCarrier) {
		_docCarrier = docCarrier;

		if (_coinOrderRemoteModel != null) {
			try {
				Class<?> clazz = _coinOrderRemoteModel.getClass();

				Method method = clazz.getMethod("setDocCarrier", String.class);

				method.invoke(_coinOrderRemoteModel, docCarrier);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDocTrackingNo() {
		return _docTrackingNo;
	}

	@Override
	public void setDocTrackingNo(String docTrackingNo) {
		_docTrackingNo = docTrackingNo;

		if (_coinOrderRemoteModel != null) {
			try {
				Class<?> clazz = _coinOrderRemoteModel.getClass();

				Method method = clazz.getMethod("setDocTrackingNo", String.class);

				method.invoke(_coinOrderRemoteModel, docTrackingNo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDocSignFlag() {
		return _docSignFlag;
	}

	@Override
	public void setDocSignFlag(String docSignFlag) {
		_docSignFlag = docSignFlag;

		if (_coinOrderRemoteModel != null) {
			try {
				Class<?> clazz = _coinOrderRemoteModel.getClass();

				Method method = clazz.getMethod("setDocSignFlag", String.class);

				method.invoke(_coinOrderRemoteModel, docSignFlag);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getPaidFlag() {
		return _paidFlag;
	}

	@Override
	public void setPaidFlag(String paidFlag) {
		_paidFlag = paidFlag;

		if (_coinOrderRemoteModel != null) {
			try {
				Class<?> clazz = _coinOrderRemoteModel.getClass();

				Method method = clazz.getMethod("setPaidFlag", String.class);

				method.invoke(_coinOrderRemoteModel, paidFlag);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getApprovalFlag() {
		return _approvalFlag;
	}

	@Override
	public void setApprovalFlag(String approvalFlag) {
		_approvalFlag = approvalFlag;

		if (_coinOrderRemoteModel != null) {
			try {
				Class<?> clazz = _coinOrderRemoteModel.getClass();

				Method method = clazz.getMethod("setApprovalFlag", String.class);

				method.invoke(_coinOrderRemoteModel, approvalFlag);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCertSentFlag() {
		return _certSentFlag;
	}

	@Override
	public void setCertSentFlag(String certSentFlag) {
		_certSentFlag = certSentFlag;

		if (_coinOrderRemoteModel != null) {
			try {
				Class<?> clazz = _coinOrderRemoteModel.getClass();

				Method method = clazz.getMethod("setCertSentFlag", String.class);

				method.invoke(_coinOrderRemoteModel, certSentFlag);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCertCarrier() {
		return _certCarrier;
	}

	@Override
	public void setCertCarrier(String certCarrier) {
		_certCarrier = certCarrier;

		if (_coinOrderRemoteModel != null) {
			try {
				Class<?> clazz = _coinOrderRemoteModel.getClass();

				Method method = clazz.getMethod("setCertCarrier", String.class);

				method.invoke(_coinOrderRemoteModel, certCarrier);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCertTrackingNo() {
		return _certTrackingNo;
	}

	@Override
	public void setCertTrackingNo(String certTrackingNo) {
		_certTrackingNo = certTrackingNo;

		if (_coinOrderRemoteModel != null) {
			try {
				Class<?> clazz = _coinOrderRemoteModel.getClass();

				Method method = clazz.getMethod("setCertTrackingNo",
						String.class);

				method.invoke(_coinOrderRemoteModel, certTrackingNo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCompleteFlag() {
		return _completeFlag;
	}

	@Override
	public void setCompleteFlag(String completeFlag) {
		_completeFlag = completeFlag;

		if (_coinOrderRemoteModel != null) {
			try {
				Class<?> clazz = _coinOrderRemoteModel.getClass();

				Method method = clazz.getMethod("setCompleteFlag", String.class);

				method.invoke(_coinOrderRemoteModel, completeFlag);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getCaseCloser() {
		return _caseCloser;
	}

	@Override
	public void setCaseCloser(long caseCloser) {
		_caseCloser = caseCloser;

		if (_coinOrderRemoteModel != null) {
			try {
				Class<?> clazz = _coinOrderRemoteModel.getClass();

				Method method = clazz.getMethod("setCaseCloser", long.class);

				method.invoke(_coinOrderRemoteModel, caseCloser);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getAddTime() {
		return _addTime;
	}

	@Override
	public void setAddTime(Date addTime) {
		_addTime = addTime;

		if (_coinOrderRemoteModel != null) {
			try {
				Class<?> clazz = _coinOrderRemoteModel.getClass();

				Method method = clazz.getMethod("setAddTime", Date.class);

				method.invoke(_coinOrderRemoteModel, addTime);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getCoinOrderRemoteModel() {
		return _coinOrderRemoteModel;
	}

	public void setCoinOrderRemoteModel(BaseModel<?> coinOrderRemoteModel) {
		_coinOrderRemoteModel = coinOrderRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _coinOrderRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_coinOrderRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			CoinOrderLocalServiceUtil.addCoinOrder(this);
		}
		else {
			CoinOrderLocalServiceUtil.updateCoinOrder(this);
		}
	}

	@Override
	public CoinOrder toEscapedModel() {
		return (CoinOrder)ProxyUtil.newProxyInstance(CoinOrder.class.getClassLoader(),
			new Class[] { CoinOrder.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		CoinOrderClp clone = new CoinOrderClp();

		clone.setOrderId(getOrderId());
		clone.setUserId(getUserId());
		clone.setSaleRegion(getSaleRegion());
		clone.setBrokerId(getBrokerId());
		clone.setSalesId(getSalesId());
		clone.setActiveFlag(getActiveFlag());
		clone.setSalePhase(getSalePhase());
		clone.setUnitPrice(getUnitPrice());
		clone.setTotalPrice(getTotalPrice());
		clone.setCurrency(getCurrency());
		clone.setSaleAmount(getSaleAmount());
		clone.setPromoAmount(getPromoAmount());
		clone.setTotalAmount(getTotalAmount());
		clone.setRsnCd(getRsnCd());
		clone.setVerifyFlag(getVerifyFlag());
		clone.setDocSentFlag(getDocSentFlag());
		clone.setDocCarrier(getDocCarrier());
		clone.setDocTrackingNo(getDocTrackingNo());
		clone.setDocSignFlag(getDocSignFlag());
		clone.setPaidFlag(getPaidFlag());
		clone.setApprovalFlag(getApprovalFlag());
		clone.setCertSentFlag(getCertSentFlag());
		clone.setCertCarrier(getCertCarrier());
		clone.setCertTrackingNo(getCertTrackingNo());
		clone.setCompleteFlag(getCompleteFlag());
		clone.setCaseCloser(getCaseCloser());
		clone.setAddTime(getAddTime());

		return clone;
	}

	@Override
	public int compareTo(CoinOrder coinOrder) {
		int value = 0;

		value = DateUtil.compareTo(getAddTime(), coinOrder.getAddTime());

		value = value * -1;

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CoinOrderClp)) {
			return false;
		}

		CoinOrderClp coinOrder = (CoinOrderClp)obj;

		String primaryKey = coinOrder.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(55);

		sb.append("{orderId=");
		sb.append(getOrderId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", saleRegion=");
		sb.append(getSaleRegion());
		sb.append(", brokerId=");
		sb.append(getBrokerId());
		sb.append(", salesId=");
		sb.append(getSalesId());
		sb.append(", activeFlag=");
		sb.append(getActiveFlag());
		sb.append(", salePhase=");
		sb.append(getSalePhase());
		sb.append(", unitPrice=");
		sb.append(getUnitPrice());
		sb.append(", totalPrice=");
		sb.append(getTotalPrice());
		sb.append(", currency=");
		sb.append(getCurrency());
		sb.append(", saleAmount=");
		sb.append(getSaleAmount());
		sb.append(", promoAmount=");
		sb.append(getPromoAmount());
		sb.append(", totalAmount=");
		sb.append(getTotalAmount());
		sb.append(", rsnCd=");
		sb.append(getRsnCd());
		sb.append(", verifyFlag=");
		sb.append(getVerifyFlag());
		sb.append(", docSentFlag=");
		sb.append(getDocSentFlag());
		sb.append(", docCarrier=");
		sb.append(getDocCarrier());
		sb.append(", docTrackingNo=");
		sb.append(getDocTrackingNo());
		sb.append(", docSignFlag=");
		sb.append(getDocSignFlag());
		sb.append(", paidFlag=");
		sb.append(getPaidFlag());
		sb.append(", approvalFlag=");
		sb.append(getApprovalFlag());
		sb.append(", certSentFlag=");
		sb.append(getCertSentFlag());
		sb.append(", certCarrier=");
		sb.append(getCertCarrier());
		sb.append(", certTrackingNo=");
		sb.append(getCertTrackingNo());
		sb.append(", completeFlag=");
		sb.append(getCompleteFlag());
		sb.append(", caseCloser=");
		sb.append(getCaseCloser());
		sb.append(", addTime=");
		sb.append(getAddTime());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(85);

		sb.append("<model><model-name>");
		sb.append("com.aw.internal.model.CoinOrder");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>orderId</column-name><column-value><![CDATA[");
		sb.append(getOrderId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>saleRegion</column-name><column-value><![CDATA[");
		sb.append(getSaleRegion());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>brokerId</column-name><column-value><![CDATA[");
		sb.append(getBrokerId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>salesId</column-name><column-value><![CDATA[");
		sb.append(getSalesId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>activeFlag</column-name><column-value><![CDATA[");
		sb.append(getActiveFlag());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>salePhase</column-name><column-value><![CDATA[");
		sb.append(getSalePhase());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>unitPrice</column-name><column-value><![CDATA[");
		sb.append(getUnitPrice());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>totalPrice</column-name><column-value><![CDATA[");
		sb.append(getTotalPrice());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>currency</column-name><column-value><![CDATA[");
		sb.append(getCurrency());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>saleAmount</column-name><column-value><![CDATA[");
		sb.append(getSaleAmount());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>promoAmount</column-name><column-value><![CDATA[");
		sb.append(getPromoAmount());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>totalAmount</column-name><column-value><![CDATA[");
		sb.append(getTotalAmount());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>rsnCd</column-name><column-value><![CDATA[");
		sb.append(getRsnCd());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>verifyFlag</column-name><column-value><![CDATA[");
		sb.append(getVerifyFlag());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>docSentFlag</column-name><column-value><![CDATA[");
		sb.append(getDocSentFlag());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>docCarrier</column-name><column-value><![CDATA[");
		sb.append(getDocCarrier());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>docTrackingNo</column-name><column-value><![CDATA[");
		sb.append(getDocTrackingNo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>docSignFlag</column-name><column-value><![CDATA[");
		sb.append(getDocSignFlag());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>paidFlag</column-name><column-value><![CDATA[");
		sb.append(getPaidFlag());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>approvalFlag</column-name><column-value><![CDATA[");
		sb.append(getApprovalFlag());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>certSentFlag</column-name><column-value><![CDATA[");
		sb.append(getCertSentFlag());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>certCarrier</column-name><column-value><![CDATA[");
		sb.append(getCertCarrier());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>certTrackingNo</column-name><column-value><![CDATA[");
		sb.append(getCertTrackingNo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>completeFlag</column-name><column-value><![CDATA[");
		sb.append(getCompleteFlag());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>caseCloser</column-name><column-value><![CDATA[");
		sb.append(getCaseCloser());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>addTime</column-name><column-value><![CDATA[");
		sb.append(getAddTime());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private String _orderId;
	private String _userId;
	private String _saleRegion;
	private long _brokerId;
	private long _salesId;
	private String _activeFlag;
	private int _salePhase;
	private double _unitPrice;
	private double _totalPrice;
	private String _currency;
	private long _saleAmount;
	private long _promoAmount;
	private long _totalAmount;
	private String _rsnCd;
	private String _verifyFlag;
	private String _docSentFlag;
	private String _docCarrier;
	private String _docTrackingNo;
	private String _docSignFlag;
	private String _paidFlag;
	private String _approvalFlag;
	private String _certSentFlag;
	private String _certCarrier;
	private String _certTrackingNo;
	private String _completeFlag;
	private long _caseCloser;
	private Date _addTime;
	private BaseModel<?> _coinOrderRemoteModel;
	private Class<?> _clpSerializerClass = com.aw.internal.service.ClpSerializer.class;
}