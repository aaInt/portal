/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author zl
 * @generated
 */
public class FailReasonSoap implements Serializable {
	public static FailReasonSoap toSoapModel(FailReason model) {
		FailReasonSoap soapModel = new FailReasonSoap();

		soapModel.setRsnCd(model.getRsnCd());
		soapModel.setDesc(model.getDesc());

		return soapModel;
	}

	public static FailReasonSoap[] toSoapModels(FailReason[] models) {
		FailReasonSoap[] soapModels = new FailReasonSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static FailReasonSoap[][] toSoapModels(FailReason[][] models) {
		FailReasonSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new FailReasonSoap[models.length][models[0].length];
		}
		else {
			soapModels = new FailReasonSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static FailReasonSoap[] toSoapModels(List<FailReason> models) {
		List<FailReasonSoap> soapModels = new ArrayList<FailReasonSoap>(models.size());

		for (FailReason model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new FailReasonSoap[soapModels.size()]);
	}

	public FailReasonSoap() {
	}

	public String getPrimaryKey() {
		return _rsnCd;
	}

	public void setPrimaryKey(String pk) {
		setRsnCd(pk);
	}

	public String getRsnCd() {
		return _rsnCd;
	}

	public void setRsnCd(String rsnCd) {
		_rsnCd = rsnCd;
	}

	public String getDesc() {
		return _desc;
	}

	public void setDesc(String desc) {
		_desc = desc;
	}

	private String _rsnCd;
	private String _desc;
}