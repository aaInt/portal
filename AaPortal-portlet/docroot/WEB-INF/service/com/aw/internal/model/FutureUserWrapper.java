/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link FutureUser}.
 * </p>
 *
 * @author zl
 * @see FutureUser
 * @generated
 */
public class FutureUserWrapper implements FutureUser, ModelWrapper<FutureUser> {
	public FutureUserWrapper(FutureUser futureUser) {
		_futureUser = futureUser;
	}

	@Override
	public Class<?> getModelClass() {
		return FutureUser.class;
	}

	@Override
	public String getModelClassName() {
		return FutureUser.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("email", getEmail());
		attributes.put("firstName", getFirstName());
		attributes.put("lastName", getLastName());
		attributes.put("residency", getResidency());
		attributes.put("phone", getPhone());
		attributes.put("areaCode", getAreaCode());
		attributes.put("activeFlag", getActiveFlag());
		attributes.put("takenFlag", getTakenFlag());
		attributes.put("ownerId", getOwnerId());
		attributes.put("addTime", getAddTime());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String email = (String)attributes.get("email");

		if (email != null) {
			setEmail(email);
		}

		String firstName = (String)attributes.get("firstName");

		if (firstName != null) {
			setFirstName(firstName);
		}

		String lastName = (String)attributes.get("lastName");

		if (lastName != null) {
			setLastName(lastName);
		}

		String residency = (String)attributes.get("residency");

		if (residency != null) {
			setResidency(residency);
		}

		Long phone = (Long)attributes.get("phone");

		if (phone != null) {
			setPhone(phone);
		}

		Integer areaCode = (Integer)attributes.get("areaCode");

		if (areaCode != null) {
			setAreaCode(areaCode);
		}

		String activeFlag = (String)attributes.get("activeFlag");

		if (activeFlag != null) {
			setActiveFlag(activeFlag);
		}

		String takenFlag = (String)attributes.get("takenFlag");

		if (takenFlag != null) {
			setTakenFlag(takenFlag);
		}

		Long ownerId = (Long)attributes.get("ownerId");

		if (ownerId != null) {
			setOwnerId(ownerId);
		}

		Date addTime = (Date)attributes.get("addTime");

		if (addTime != null) {
			setAddTime(addTime);
		}
	}

	/**
	* Returns the primary key of this future user.
	*
	* @return the primary key of this future user
	*/
	@Override
	public java.lang.String getPrimaryKey() {
		return _futureUser.getPrimaryKey();
	}

	/**
	* Sets the primary key of this future user.
	*
	* @param primaryKey the primary key of this future user
	*/
	@Override
	public void setPrimaryKey(java.lang.String primaryKey) {
		_futureUser.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the email of this future user.
	*
	* @return the email of this future user
	*/
	@Override
	public java.lang.String getEmail() {
		return _futureUser.getEmail();
	}

	/**
	* Sets the email of this future user.
	*
	* @param email the email of this future user
	*/
	@Override
	public void setEmail(java.lang.String email) {
		_futureUser.setEmail(email);
	}

	/**
	* Returns the first name of this future user.
	*
	* @return the first name of this future user
	*/
	@Override
	public java.lang.String getFirstName() {
		return _futureUser.getFirstName();
	}

	/**
	* Sets the first name of this future user.
	*
	* @param firstName the first name of this future user
	*/
	@Override
	public void setFirstName(java.lang.String firstName) {
		_futureUser.setFirstName(firstName);
	}

	/**
	* Returns the last name of this future user.
	*
	* @return the last name of this future user
	*/
	@Override
	public java.lang.String getLastName() {
		return _futureUser.getLastName();
	}

	/**
	* Sets the last name of this future user.
	*
	* @param lastName the last name of this future user
	*/
	@Override
	public void setLastName(java.lang.String lastName) {
		_futureUser.setLastName(lastName);
	}

	/**
	* Returns the residency of this future user.
	*
	* @return the residency of this future user
	*/
	@Override
	public java.lang.String getResidency() {
		return _futureUser.getResidency();
	}

	/**
	* Sets the residency of this future user.
	*
	* @param residency the residency of this future user
	*/
	@Override
	public void setResidency(java.lang.String residency) {
		_futureUser.setResidency(residency);
	}

	/**
	* Returns the phone of this future user.
	*
	* @return the phone of this future user
	*/
	@Override
	public long getPhone() {
		return _futureUser.getPhone();
	}

	/**
	* Sets the phone of this future user.
	*
	* @param phone the phone of this future user
	*/
	@Override
	public void setPhone(long phone) {
		_futureUser.setPhone(phone);
	}

	/**
	* Returns the area code of this future user.
	*
	* @return the area code of this future user
	*/
	@Override
	public int getAreaCode() {
		return _futureUser.getAreaCode();
	}

	/**
	* Sets the area code of this future user.
	*
	* @param areaCode the area code of this future user
	*/
	@Override
	public void setAreaCode(int areaCode) {
		_futureUser.setAreaCode(areaCode);
	}

	/**
	* Returns the active flag of this future user.
	*
	* @return the active flag of this future user
	*/
	@Override
	public java.lang.String getActiveFlag() {
		return _futureUser.getActiveFlag();
	}

	/**
	* Sets the active flag of this future user.
	*
	* @param activeFlag the active flag of this future user
	*/
	@Override
	public void setActiveFlag(java.lang.String activeFlag) {
		_futureUser.setActiveFlag(activeFlag);
	}

	/**
	* Returns the taken flag of this future user.
	*
	* @return the taken flag of this future user
	*/
	@Override
	public java.lang.String getTakenFlag() {
		return _futureUser.getTakenFlag();
	}

	/**
	* Sets the taken flag of this future user.
	*
	* @param takenFlag the taken flag of this future user
	*/
	@Override
	public void setTakenFlag(java.lang.String takenFlag) {
		_futureUser.setTakenFlag(takenFlag);
	}

	/**
	* Returns the owner ID of this future user.
	*
	* @return the owner ID of this future user
	*/
	@Override
	public long getOwnerId() {
		return _futureUser.getOwnerId();
	}

	/**
	* Sets the owner ID of this future user.
	*
	* @param ownerId the owner ID of this future user
	*/
	@Override
	public void setOwnerId(long ownerId) {
		_futureUser.setOwnerId(ownerId);
	}

	/**
	* Returns the add time of this future user.
	*
	* @return the add time of this future user
	*/
	@Override
	public java.util.Date getAddTime() {
		return _futureUser.getAddTime();
	}

	/**
	* Sets the add time of this future user.
	*
	* @param addTime the add time of this future user
	*/
	@Override
	public void setAddTime(java.util.Date addTime) {
		_futureUser.setAddTime(addTime);
	}

	@Override
	public boolean isNew() {
		return _futureUser.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_futureUser.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _futureUser.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_futureUser.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _futureUser.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _futureUser.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_futureUser.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _futureUser.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_futureUser.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_futureUser.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_futureUser.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new FutureUserWrapper((FutureUser)_futureUser.clone());
	}

	@Override
	public int compareTo(com.aw.internal.model.FutureUser futureUser) {
		return _futureUser.compareTo(futureUser);
	}

	@Override
	public int hashCode() {
		return _futureUser.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.aw.internal.model.FutureUser> toCacheModel() {
		return _futureUser.toCacheModel();
	}

	@Override
	public com.aw.internal.model.FutureUser toEscapedModel() {
		return new FutureUserWrapper(_futureUser.toEscapedModel());
	}

	@Override
	public com.aw.internal.model.FutureUser toUnescapedModel() {
		return new FutureUserWrapper(_futureUser.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _futureUser.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _futureUser.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_futureUser.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof FutureUserWrapper)) {
			return false;
		}

		FutureUserWrapper futureUserWrapper = (FutureUserWrapper)obj;

		if (Validator.equals(_futureUser, futureUserWrapper._futureUser)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public FutureUser getWrappedFutureUser() {
		return _futureUser;
	}

	@Override
	public FutureUser getWrappedModel() {
		return _futureUser;
	}

	@Override
	public void resetOriginalValues() {
		_futureUser.resetOriginalValues();
	}

	private FutureUser _futureUser;
}