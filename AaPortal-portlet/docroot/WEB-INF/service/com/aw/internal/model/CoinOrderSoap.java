/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.aw.internal.service.http.CoinOrderServiceSoap}.
 *
 * @author zl
 * @see com.aw.internal.service.http.CoinOrderServiceSoap
 * @generated
 */
public class CoinOrderSoap implements Serializable {
	public static CoinOrderSoap toSoapModel(CoinOrder model) {
		CoinOrderSoap soapModel = new CoinOrderSoap();

		soapModel.setOrderId(model.getOrderId());
		soapModel.setUserId(model.getUserId());
		soapModel.setSaleRegion(model.getSaleRegion());
		soapModel.setBrokerId(model.getBrokerId());
		soapModel.setSalesId(model.getSalesId());
		soapModel.setActiveFlag(model.getActiveFlag());
		soapModel.setSalePhase(model.getSalePhase());
		soapModel.setUnitPrice(model.getUnitPrice());
		soapModel.setTotalPrice(model.getTotalPrice());
		soapModel.setCurrency(model.getCurrency());
		soapModel.setSaleAmount(model.getSaleAmount());
		soapModel.setPromoAmount(model.getPromoAmount());
		soapModel.setTotalAmount(model.getTotalAmount());
		soapModel.setRsnCd(model.getRsnCd());
		soapModel.setVerifyFlag(model.getVerifyFlag());
		soapModel.setDocSentFlag(model.getDocSentFlag());
		soapModel.setDocCarrier(model.getDocCarrier());
		soapModel.setDocTrackingNo(model.getDocTrackingNo());
		soapModel.setDocSignFlag(model.getDocSignFlag());
		soapModel.setPaidFlag(model.getPaidFlag());
		soapModel.setApprovalFlag(model.getApprovalFlag());
		soapModel.setCertSentFlag(model.getCertSentFlag());
		soapModel.setCertCarrier(model.getCertCarrier());
		soapModel.setCertTrackingNo(model.getCertTrackingNo());
		soapModel.setCompleteFlag(model.getCompleteFlag());
		soapModel.setCaseCloser(model.getCaseCloser());
		soapModel.setAddTime(model.getAddTime());

		return soapModel;
	}

	public static CoinOrderSoap[] toSoapModels(CoinOrder[] models) {
		CoinOrderSoap[] soapModels = new CoinOrderSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static CoinOrderSoap[][] toSoapModels(CoinOrder[][] models) {
		CoinOrderSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new CoinOrderSoap[models.length][models[0].length];
		}
		else {
			soapModels = new CoinOrderSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static CoinOrderSoap[] toSoapModels(List<CoinOrder> models) {
		List<CoinOrderSoap> soapModels = new ArrayList<CoinOrderSoap>(models.size());

		for (CoinOrder model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new CoinOrderSoap[soapModels.size()]);
	}

	public CoinOrderSoap() {
	}

	public String getPrimaryKey() {
		return _orderId;
	}

	public void setPrimaryKey(String pk) {
		setOrderId(pk);
	}

	public String getOrderId() {
		return _orderId;
	}

	public void setOrderId(String orderId) {
		_orderId = orderId;
	}

	public String getUserId() {
		return _userId;
	}

	public void setUserId(String userId) {
		_userId = userId;
	}

	public String getSaleRegion() {
		return _saleRegion;
	}

	public void setSaleRegion(String saleRegion) {
		_saleRegion = saleRegion;
	}

	public long getBrokerId() {
		return _brokerId;
	}

	public void setBrokerId(long brokerId) {
		_brokerId = brokerId;
	}

	public long getSalesId() {
		return _salesId;
	}

	public void setSalesId(long salesId) {
		_salesId = salesId;
	}

	public String getActiveFlag() {
		return _activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		_activeFlag = activeFlag;
	}

	public int getSalePhase() {
		return _salePhase;
	}

	public void setSalePhase(int salePhase) {
		_salePhase = salePhase;
	}

	public double getUnitPrice() {
		return _unitPrice;
	}

	public void setUnitPrice(double unitPrice) {
		_unitPrice = unitPrice;
	}

	public double getTotalPrice() {
		return _totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		_totalPrice = totalPrice;
	}

	public String getCurrency() {
		return _currency;
	}

	public void setCurrency(String currency) {
		_currency = currency;
	}

	public long getSaleAmount() {
		return _saleAmount;
	}

	public void setSaleAmount(long saleAmount) {
		_saleAmount = saleAmount;
	}

	public long getPromoAmount() {
		return _promoAmount;
	}

	public void setPromoAmount(long promoAmount) {
		_promoAmount = promoAmount;
	}

	public long getTotalAmount() {
		return _totalAmount;
	}

	public void setTotalAmount(long totalAmount) {
		_totalAmount = totalAmount;
	}

	public String getRsnCd() {
		return _rsnCd;
	}

	public void setRsnCd(String rsnCd) {
		_rsnCd = rsnCd;
	}

	public String getVerifyFlag() {
		return _verifyFlag;
	}

	public void setVerifyFlag(String verifyFlag) {
		_verifyFlag = verifyFlag;
	}

	public String getDocSentFlag() {
		return _docSentFlag;
	}

	public void setDocSentFlag(String docSentFlag) {
		_docSentFlag = docSentFlag;
	}

	public String getDocCarrier() {
		return _docCarrier;
	}

	public void setDocCarrier(String docCarrier) {
		_docCarrier = docCarrier;
	}

	public String getDocTrackingNo() {
		return _docTrackingNo;
	}

	public void setDocTrackingNo(String docTrackingNo) {
		_docTrackingNo = docTrackingNo;
	}

	public String getDocSignFlag() {
		return _docSignFlag;
	}

	public void setDocSignFlag(String docSignFlag) {
		_docSignFlag = docSignFlag;
	}

	public String getPaidFlag() {
		return _paidFlag;
	}

	public void setPaidFlag(String paidFlag) {
		_paidFlag = paidFlag;
	}

	public String getApprovalFlag() {
		return _approvalFlag;
	}

	public void setApprovalFlag(String approvalFlag) {
		_approvalFlag = approvalFlag;
	}

	public String getCertSentFlag() {
		return _certSentFlag;
	}

	public void setCertSentFlag(String certSentFlag) {
		_certSentFlag = certSentFlag;
	}

	public String getCertCarrier() {
		return _certCarrier;
	}

	public void setCertCarrier(String certCarrier) {
		_certCarrier = certCarrier;
	}

	public String getCertTrackingNo() {
		return _certTrackingNo;
	}

	public void setCertTrackingNo(String certTrackingNo) {
		_certTrackingNo = certTrackingNo;
	}

	public String getCompleteFlag() {
		return _completeFlag;
	}

	public void setCompleteFlag(String completeFlag) {
		_completeFlag = completeFlag;
	}

	public long getCaseCloser() {
		return _caseCloser;
	}

	public void setCaseCloser(long caseCloser) {
		_caseCloser = caseCloser;
	}

	public Date getAddTime() {
		return _addTime;
	}

	public void setAddTime(Date addTime) {
		_addTime = addTime;
	}

	private String _orderId;
	private String _userId;
	private String _saleRegion;
	private long _brokerId;
	private long _salesId;
	private String _activeFlag;
	private int _salePhase;
	private double _unitPrice;
	private double _totalPrice;
	private String _currency;
	private long _saleAmount;
	private long _promoAmount;
	private long _totalAmount;
	private String _rsnCd;
	private String _verifyFlag;
	private String _docSentFlag;
	private String _docCarrier;
	private String _docTrackingNo;
	private String _docSignFlag;
	private String _paidFlag;
	private String _approvalFlag;
	private String _certSentFlag;
	private String _certCarrier;
	private String _certTrackingNo;
	private String _completeFlag;
	private long _caseCloser;
	private Date _addTime;
}