/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model;

import java.io.Serializable;

import java.sql.Blob;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author zl
 * @generated
 */
public class ImageStoreSoap implements Serializable {
	public static ImageStoreSoap toSoapModel(ImageStore model) {
		ImageStoreSoap soapModel = new ImageStoreSoap();

		soapModel.setImageId(model.getImageId());
		soapModel.setContent(model.getContent());
		soapModel.setActiveFlag(model.getActiveFlag());
		soapModel.setAddDate(model.getAddDate());

		return soapModel;
	}

	public static ImageStoreSoap[] toSoapModels(ImageStore[] models) {
		ImageStoreSoap[] soapModels = new ImageStoreSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ImageStoreSoap[][] toSoapModels(ImageStore[][] models) {
		ImageStoreSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ImageStoreSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ImageStoreSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ImageStoreSoap[] toSoapModels(List<ImageStore> models) {
		List<ImageStoreSoap> soapModels = new ArrayList<ImageStoreSoap>(models.size());

		for (ImageStore model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ImageStoreSoap[soapModels.size()]);
	}

	public ImageStoreSoap() {
	}

	public long getPrimaryKey() {
		return _imageId;
	}

	public void setPrimaryKey(long pk) {
		setImageId(pk);
	}

	public long getImageId() {
		return _imageId;
	}

	public void setImageId(long imageId) {
		_imageId = imageId;
	}

	public Blob getContent() {
		return _content;
	}

	public void setContent(Blob content) {
		_content = content;
	}

	public String getActiveFlag() {
		return _activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		_activeFlag = activeFlag;
	}

	public Date getAddDate() {
		return _addDate;
	}

	public void setAddDate(Date addDate) {
		_addDate = addDate;
	}

	private long _imageId;
	private Blob _content;
	private String _activeFlag;
	private Date _addDate;
}