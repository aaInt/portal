/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model;

import java.sql.Blob;

/**
 * The Blob model class for lazy loading the content column in ImageStore.
 *
 * @author zl
 * @see ImageStore
 * @generated
 */
public class ImageStoreContentBlobModel {
	public ImageStoreContentBlobModel() {
	}

	public ImageStoreContentBlobModel(long imageId) {
		_imageId = imageId;
	}

	public ImageStoreContentBlobModel(long imageId, Blob contentBlob) {
		_imageId = imageId;
		_contentBlob = contentBlob;
	}

	public long getImageId() {
		return _imageId;
	}

	public void setImageId(long imageId) {
		_imageId = imageId;
	}

	public Blob getContentBlob() {
		return _contentBlob;
	}

	public void setContentBlob(Blob contentBlob) {
		_contentBlob = contentBlob;
	}

	private long _imageId;
	private Blob _contentBlob;
}