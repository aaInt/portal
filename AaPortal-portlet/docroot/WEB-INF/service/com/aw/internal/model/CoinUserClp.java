/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model;

import com.aw.internal.service.ClpSerializer;
import com.aw.internal.service.CoinUserLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author zl
 */
public class CoinUserClp extends BaseModelImpl<CoinUser> implements CoinUser {
	public CoinUserClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return CoinUser.class;
	}

	@Override
	public String getModelClassName() {
		return CoinUser.class.getName();
	}

	@Override
	public String getPrimaryKey() {
		return _userId;
	}

	@Override
	public void setPrimaryKey(String primaryKey) {
		setUserId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _userId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((String)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("userId", getUserId());
		attributes.put("firstName", getFirstName());
		attributes.put("lastName", getLastName());
		attributes.put("activeFlag", getActiveFlag());
		attributes.put("birthday", getBirthday());
		attributes.put("contactId", getContactId());
		attributes.put("userType", getUserType());
		attributes.put("brokerId", getBrokerId());
		attributes.put("fundId", getFundId());
		attributes.put("nationality", getNationality());
		attributes.put("idIssueCountry", getIdIssueCountry());
		attributes.put("idType", getIdType());
		attributes.put("idNumber", getIdNumber());
		attributes.put("photoImage", getPhotoImage());
		attributes.put("docId", getDocId());
		attributes.put("ssn", getSsn());
		attributes.put("ein", getEin());
		attributes.put("income", getIncome());
		attributes.put("asset", getAsset());
		attributes.put("isValidated", getIsValidated());
		attributes.put("isCreddited", getIsCreddited());
		attributes.put("addTime", getAddTime());
		attributes.put("addUser", getAddUser());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String userId = (String)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String firstName = (String)attributes.get("firstName");

		if (firstName != null) {
			setFirstName(firstName);
		}

		String lastName = (String)attributes.get("lastName");

		if (lastName != null) {
			setLastName(lastName);
		}

		String activeFlag = (String)attributes.get("activeFlag");

		if (activeFlag != null) {
			setActiveFlag(activeFlag);
		}

		String birthday = (String)attributes.get("birthday");

		if (birthday != null) {
			setBirthday(birthday);
		}

		Long contactId = (Long)attributes.get("contactId");

		if (contactId != null) {
			setContactId(contactId);
		}

		String userType = (String)attributes.get("userType");

		if (userType != null) {
			setUserType(userType);
		}

		Long brokerId = (Long)attributes.get("brokerId");

		if (brokerId != null) {
			setBrokerId(brokerId);
		}

		Long fundId = (Long)attributes.get("fundId");

		if (fundId != null) {
			setFundId(fundId);
		}

		String nationality = (String)attributes.get("nationality");

		if (nationality != null) {
			setNationality(nationality);
		}

		String idIssueCountry = (String)attributes.get("idIssueCountry");

		if (idIssueCountry != null) {
			setIdIssueCountry(idIssueCountry);
		}

		String idType = (String)attributes.get("idType");

		if (idType != null) {
			setIdType(idType);
		}

		String idNumber = (String)attributes.get("idNumber");

		if (idNumber != null) {
			setIdNumber(idNumber);
		}

		Long photoImage = (Long)attributes.get("photoImage");

		if (photoImage != null) {
			setPhotoImage(photoImage);
		}

		Long docId = (Long)attributes.get("docId");

		if (docId != null) {
			setDocId(docId);
		}

		String ssn = (String)attributes.get("ssn");

		if (ssn != null) {
			setSsn(ssn);
		}

		String ein = (String)attributes.get("ein");

		if (ein != null) {
			setEin(ein);
		}

		String income = (String)attributes.get("income");

		if (income != null) {
			setIncome(income);
		}

		String asset = (String)attributes.get("asset");

		if (asset != null) {
			setAsset(asset);
		}

		String isValidated = (String)attributes.get("isValidated");

		if (isValidated != null) {
			setIsValidated(isValidated);
		}

		String isCreddited = (String)attributes.get("isCreddited");

		if (isCreddited != null) {
			setIsCreddited(isCreddited);
		}

		Date addTime = (Date)attributes.get("addTime");

		if (addTime != null) {
			setAddTime(addTime);
		}

		Long addUser = (Long)attributes.get("addUser");

		if (addUser != null) {
			setAddUser(addUser);
		}
	}

	@Override
	public String getUserId() {
		return _userId;
	}

	@Override
	public void setUserId(String userId) {
		_userId = userId;

		if (_coinUserRemoteModel != null) {
			try {
				Class<?> clazz = _coinUserRemoteModel.getClass();

				Method method = clazz.getMethod("setUserId", String.class);

				method.invoke(_coinUserRemoteModel, userId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getFirstName() {
		return _firstName;
	}

	@Override
	public void setFirstName(String firstName) {
		_firstName = firstName;

		if (_coinUserRemoteModel != null) {
			try {
				Class<?> clazz = _coinUserRemoteModel.getClass();

				Method method = clazz.getMethod("setFirstName", String.class);

				method.invoke(_coinUserRemoteModel, firstName);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLastName() {
		return _lastName;
	}

	@Override
	public void setLastName(String lastName) {
		_lastName = lastName;

		if (_coinUserRemoteModel != null) {
			try {
				Class<?> clazz = _coinUserRemoteModel.getClass();

				Method method = clazz.getMethod("setLastName", String.class);

				method.invoke(_coinUserRemoteModel, lastName);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getActiveFlag() {
		return _activeFlag;
	}

	@Override
	public void setActiveFlag(String activeFlag) {
		_activeFlag = activeFlag;

		if (_coinUserRemoteModel != null) {
			try {
				Class<?> clazz = _coinUserRemoteModel.getClass();

				Method method = clazz.getMethod("setActiveFlag", String.class);

				method.invoke(_coinUserRemoteModel, activeFlag);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getBirthday() {
		return _birthday;
	}

	@Override
	public void setBirthday(String birthday) {
		_birthday = birthday;

		if (_coinUserRemoteModel != null) {
			try {
				Class<?> clazz = _coinUserRemoteModel.getClass();

				Method method = clazz.getMethod("setBirthday", String.class);

				method.invoke(_coinUserRemoteModel, birthday);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getContactId() {
		return _contactId;
	}

	@Override
	public void setContactId(long contactId) {
		_contactId = contactId;

		if (_coinUserRemoteModel != null) {
			try {
				Class<?> clazz = _coinUserRemoteModel.getClass();

				Method method = clazz.getMethod("setContactId", long.class);

				method.invoke(_coinUserRemoteModel, contactId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUserType() {
		return _userType;
	}

	@Override
	public void setUserType(String userType) {
		_userType = userType;

		if (_coinUserRemoteModel != null) {
			try {
				Class<?> clazz = _coinUserRemoteModel.getClass();

				Method method = clazz.getMethod("setUserType", String.class);

				method.invoke(_coinUserRemoteModel, userType);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getBrokerId() {
		return _brokerId;
	}

	@Override
	public void setBrokerId(long brokerId) {
		_brokerId = brokerId;

		if (_coinUserRemoteModel != null) {
			try {
				Class<?> clazz = _coinUserRemoteModel.getClass();

				Method method = clazz.getMethod("setBrokerId", long.class);

				method.invoke(_coinUserRemoteModel, brokerId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getFundId() {
		return _fundId;
	}

	@Override
	public void setFundId(long fundId) {
		_fundId = fundId;

		if (_coinUserRemoteModel != null) {
			try {
				Class<?> clazz = _coinUserRemoteModel.getClass();

				Method method = clazz.getMethod("setFundId", long.class);

				method.invoke(_coinUserRemoteModel, fundId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getNationality() {
		return _nationality;
	}

	@Override
	public void setNationality(String nationality) {
		_nationality = nationality;

		if (_coinUserRemoteModel != null) {
			try {
				Class<?> clazz = _coinUserRemoteModel.getClass();

				Method method = clazz.getMethod("setNationality", String.class);

				method.invoke(_coinUserRemoteModel, nationality);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getIdIssueCountry() {
		return _idIssueCountry;
	}

	@Override
	public void setIdIssueCountry(String idIssueCountry) {
		_idIssueCountry = idIssueCountry;

		if (_coinUserRemoteModel != null) {
			try {
				Class<?> clazz = _coinUserRemoteModel.getClass();

				Method method = clazz.getMethod("setIdIssueCountry",
						String.class);

				method.invoke(_coinUserRemoteModel, idIssueCountry);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getIdType() {
		return _idType;
	}

	@Override
	public void setIdType(String idType) {
		_idType = idType;

		if (_coinUserRemoteModel != null) {
			try {
				Class<?> clazz = _coinUserRemoteModel.getClass();

				Method method = clazz.getMethod("setIdType", String.class);

				method.invoke(_coinUserRemoteModel, idType);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getIdNumber() {
		return _idNumber;
	}

	@Override
	public void setIdNumber(String idNumber) {
		_idNumber = idNumber;

		if (_coinUserRemoteModel != null) {
			try {
				Class<?> clazz = _coinUserRemoteModel.getClass();

				Method method = clazz.getMethod("setIdNumber", String.class);

				method.invoke(_coinUserRemoteModel, idNumber);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getPhotoImage() {
		return _photoImage;
	}

	@Override
	public void setPhotoImage(long photoImage) {
		_photoImage = photoImage;

		if (_coinUserRemoteModel != null) {
			try {
				Class<?> clazz = _coinUserRemoteModel.getClass();

				Method method = clazz.getMethod("setPhotoImage", long.class);

				method.invoke(_coinUserRemoteModel, photoImage);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getDocId() {
		return _docId;
	}

	@Override
	public void setDocId(long docId) {
		_docId = docId;

		if (_coinUserRemoteModel != null) {
			try {
				Class<?> clazz = _coinUserRemoteModel.getClass();

				Method method = clazz.getMethod("setDocId", long.class);

				method.invoke(_coinUserRemoteModel, docId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getSsn() {
		return _ssn;
	}

	@Override
	public void setSsn(String ssn) {
		_ssn = ssn;

		if (_coinUserRemoteModel != null) {
			try {
				Class<?> clazz = _coinUserRemoteModel.getClass();

				Method method = clazz.getMethod("setSsn", String.class);

				method.invoke(_coinUserRemoteModel, ssn);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getEin() {
		return _ein;
	}

	@Override
	public void setEin(String ein) {
		_ein = ein;

		if (_coinUserRemoteModel != null) {
			try {
				Class<?> clazz = _coinUserRemoteModel.getClass();

				Method method = clazz.getMethod("setEin", String.class);

				method.invoke(_coinUserRemoteModel, ein);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getIncome() {
		return _income;
	}

	@Override
	public void setIncome(String income) {
		_income = income;

		if (_coinUserRemoteModel != null) {
			try {
				Class<?> clazz = _coinUserRemoteModel.getClass();

				Method method = clazz.getMethod("setIncome", String.class);

				method.invoke(_coinUserRemoteModel, income);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getAsset() {
		return _asset;
	}

	@Override
	public void setAsset(String asset) {
		_asset = asset;

		if (_coinUserRemoteModel != null) {
			try {
				Class<?> clazz = _coinUserRemoteModel.getClass();

				Method method = clazz.getMethod("setAsset", String.class);

				method.invoke(_coinUserRemoteModel, asset);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getIsValidated() {
		return _isValidated;
	}

	@Override
	public void setIsValidated(String isValidated) {
		_isValidated = isValidated;

		if (_coinUserRemoteModel != null) {
			try {
				Class<?> clazz = _coinUserRemoteModel.getClass();

				Method method = clazz.getMethod("setIsValidated", String.class);

				method.invoke(_coinUserRemoteModel, isValidated);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getIsCreddited() {
		return _isCreddited;
	}

	@Override
	public void setIsCreddited(String isCreddited) {
		_isCreddited = isCreddited;

		if (_coinUserRemoteModel != null) {
			try {
				Class<?> clazz = _coinUserRemoteModel.getClass();

				Method method = clazz.getMethod("setIsCreddited", String.class);

				method.invoke(_coinUserRemoteModel, isCreddited);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getAddTime() {
		return _addTime;
	}

	@Override
	public void setAddTime(Date addTime) {
		_addTime = addTime;

		if (_coinUserRemoteModel != null) {
			try {
				Class<?> clazz = _coinUserRemoteModel.getClass();

				Method method = clazz.getMethod("setAddTime", Date.class);

				method.invoke(_coinUserRemoteModel, addTime);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getAddUser() {
		return _addUser;
	}

	@Override
	public void setAddUser(long addUser) {
		_addUser = addUser;

		if (_coinUserRemoteModel != null) {
			try {
				Class<?> clazz = _coinUserRemoteModel.getClass();

				Method method = clazz.getMethod("setAddUser", long.class);

				method.invoke(_coinUserRemoteModel, addUser);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getCoinUserRemoteModel() {
		return _coinUserRemoteModel;
	}

	public void setCoinUserRemoteModel(BaseModel<?> coinUserRemoteModel) {
		_coinUserRemoteModel = coinUserRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _coinUserRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_coinUserRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			CoinUserLocalServiceUtil.addCoinUser(this);
		}
		else {
			CoinUserLocalServiceUtil.updateCoinUser(this);
		}
	}

	@Override
	public CoinUser toEscapedModel() {
		return (CoinUser)ProxyUtil.newProxyInstance(CoinUser.class.getClassLoader(),
			new Class[] { CoinUser.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		CoinUserClp clone = new CoinUserClp();

		clone.setUserId(getUserId());
		clone.setFirstName(getFirstName());
		clone.setLastName(getLastName());
		clone.setActiveFlag(getActiveFlag());
		clone.setBirthday(getBirthday());
		clone.setContactId(getContactId());
		clone.setUserType(getUserType());
		clone.setBrokerId(getBrokerId());
		clone.setFundId(getFundId());
		clone.setNationality(getNationality());
		clone.setIdIssueCountry(getIdIssueCountry());
		clone.setIdType(getIdType());
		clone.setIdNumber(getIdNumber());
		clone.setPhotoImage(getPhotoImage());
		clone.setDocId(getDocId());
		clone.setSsn(getSsn());
		clone.setEin(getEin());
		clone.setIncome(getIncome());
		clone.setAsset(getAsset());
		clone.setIsValidated(getIsValidated());
		clone.setIsCreddited(getIsCreddited());
		clone.setAddTime(getAddTime());
		clone.setAddUser(getAddUser());

		return clone;
	}

	@Override
	public int compareTo(CoinUser coinUser) {
		String primaryKey = coinUser.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CoinUserClp)) {
			return false;
		}

		CoinUserClp coinUser = (CoinUserClp)obj;

		String primaryKey = coinUser.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(47);

		sb.append("{userId=");
		sb.append(getUserId());
		sb.append(", firstName=");
		sb.append(getFirstName());
		sb.append(", lastName=");
		sb.append(getLastName());
		sb.append(", activeFlag=");
		sb.append(getActiveFlag());
		sb.append(", birthday=");
		sb.append(getBirthday());
		sb.append(", contactId=");
		sb.append(getContactId());
		sb.append(", userType=");
		sb.append(getUserType());
		sb.append(", brokerId=");
		sb.append(getBrokerId());
		sb.append(", fundId=");
		sb.append(getFundId());
		sb.append(", nationality=");
		sb.append(getNationality());
		sb.append(", idIssueCountry=");
		sb.append(getIdIssueCountry());
		sb.append(", idType=");
		sb.append(getIdType());
		sb.append(", idNumber=");
		sb.append(getIdNumber());
		sb.append(", photoImage=");
		sb.append(getPhotoImage());
		sb.append(", docId=");
		sb.append(getDocId());
		sb.append(", ssn=");
		sb.append(getSsn());
		sb.append(", ein=");
		sb.append(getEin());
		sb.append(", income=");
		sb.append(getIncome());
		sb.append(", asset=");
		sb.append(getAsset());
		sb.append(", isValidated=");
		sb.append(getIsValidated());
		sb.append(", isCreddited=");
		sb.append(getIsCreddited());
		sb.append(", addTime=");
		sb.append(getAddTime());
		sb.append(", addUser=");
		sb.append(getAddUser());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(73);

		sb.append("<model><model-name>");
		sb.append("com.aw.internal.model.CoinUser");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>firstName</column-name><column-value><![CDATA[");
		sb.append(getFirstName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>lastName</column-name><column-value><![CDATA[");
		sb.append(getLastName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>activeFlag</column-name><column-value><![CDATA[");
		sb.append(getActiveFlag());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>birthday</column-name><column-value><![CDATA[");
		sb.append(getBirthday());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>contactId</column-name><column-value><![CDATA[");
		sb.append(getContactId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userType</column-name><column-value><![CDATA[");
		sb.append(getUserType());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>brokerId</column-name><column-value><![CDATA[");
		sb.append(getBrokerId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fundId</column-name><column-value><![CDATA[");
		sb.append(getFundId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>nationality</column-name><column-value><![CDATA[");
		sb.append(getNationality());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>idIssueCountry</column-name><column-value><![CDATA[");
		sb.append(getIdIssueCountry());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>idType</column-name><column-value><![CDATA[");
		sb.append(getIdType());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>idNumber</column-name><column-value><![CDATA[");
		sb.append(getIdNumber());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>photoImage</column-name><column-value><![CDATA[");
		sb.append(getPhotoImage());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>docId</column-name><column-value><![CDATA[");
		sb.append(getDocId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>ssn</column-name><column-value><![CDATA[");
		sb.append(getSsn());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>ein</column-name><column-value><![CDATA[");
		sb.append(getEin());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>income</column-name><column-value><![CDATA[");
		sb.append(getIncome());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>asset</column-name><column-value><![CDATA[");
		sb.append(getAsset());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>isValidated</column-name><column-value><![CDATA[");
		sb.append(getIsValidated());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>isCreddited</column-name><column-value><![CDATA[");
		sb.append(getIsCreddited());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>addTime</column-name><column-value><![CDATA[");
		sb.append(getAddTime());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>addUser</column-name><column-value><![CDATA[");
		sb.append(getAddUser());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private String _userId;
	private String _firstName;
	private String _lastName;
	private String _activeFlag;
	private String _birthday;
	private long _contactId;
	private String _userType;
	private long _brokerId;
	private long _fundId;
	private String _nationality;
	private String _idIssueCountry;
	private String _idType;
	private String _idNumber;
	private long _photoImage;
	private long _docId;
	private String _ssn;
	private String _ein;
	private String _income;
	private String _asset;
	private String _isValidated;
	private String _isCreddited;
	private Date _addTime;
	private long _addUser;
	private BaseModel<?> _coinUserRemoteModel;
	private Class<?> _clpSerializerClass = com.aw.internal.service.ClpSerializer.class;
}