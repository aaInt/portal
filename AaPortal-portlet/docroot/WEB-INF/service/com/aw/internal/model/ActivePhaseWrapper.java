/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link ActivePhase}.
 * </p>
 *
 * @author zl
 * @see ActivePhase
 * @generated
 */
public class ActivePhaseWrapper implements ActivePhase,
	ModelWrapper<ActivePhase> {
	public ActivePhaseWrapper(ActivePhase activePhase) {
		_activePhase = activePhase;
	}

	@Override
	public Class<?> getModelClass() {
		return ActivePhase.class;
	}

	@Override
	public String getModelClassName() {
		return ActivePhase.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("region", getRegion());
		attributes.put("salePhase", getSalePhase());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String region = (String)attributes.get("region");

		if (region != null) {
			setRegion(region);
		}

		Integer salePhase = (Integer)attributes.get("salePhase");

		if (salePhase != null) {
			setSalePhase(salePhase);
		}
	}

	/**
	* Returns the primary key of this active phase.
	*
	* @return the primary key of this active phase
	*/
	@Override
	public java.lang.String getPrimaryKey() {
		return _activePhase.getPrimaryKey();
	}

	/**
	* Sets the primary key of this active phase.
	*
	* @param primaryKey the primary key of this active phase
	*/
	@Override
	public void setPrimaryKey(java.lang.String primaryKey) {
		_activePhase.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the region of this active phase.
	*
	* @return the region of this active phase
	*/
	@Override
	public java.lang.String getRegion() {
		return _activePhase.getRegion();
	}

	/**
	* Sets the region of this active phase.
	*
	* @param region the region of this active phase
	*/
	@Override
	public void setRegion(java.lang.String region) {
		_activePhase.setRegion(region);
	}

	/**
	* Returns the sale phase of this active phase.
	*
	* @return the sale phase of this active phase
	*/
	@Override
	public int getSalePhase() {
		return _activePhase.getSalePhase();
	}

	/**
	* Sets the sale phase of this active phase.
	*
	* @param salePhase the sale phase of this active phase
	*/
	@Override
	public void setSalePhase(int salePhase) {
		_activePhase.setSalePhase(salePhase);
	}

	@Override
	public boolean isNew() {
		return _activePhase.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_activePhase.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _activePhase.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_activePhase.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _activePhase.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _activePhase.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_activePhase.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _activePhase.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_activePhase.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_activePhase.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_activePhase.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ActivePhaseWrapper((ActivePhase)_activePhase.clone());
	}

	@Override
	public int compareTo(com.aw.internal.model.ActivePhase activePhase) {
		return _activePhase.compareTo(activePhase);
	}

	@Override
	public int hashCode() {
		return _activePhase.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.aw.internal.model.ActivePhase> toCacheModel() {
		return _activePhase.toCacheModel();
	}

	@Override
	public com.aw.internal.model.ActivePhase toEscapedModel() {
		return new ActivePhaseWrapper(_activePhase.toEscapedModel());
	}

	@Override
	public com.aw.internal.model.ActivePhase toUnescapedModel() {
		return new ActivePhaseWrapper(_activePhase.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _activePhase.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _activePhase.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_activePhase.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ActivePhaseWrapper)) {
			return false;
		}

		ActivePhaseWrapper activePhaseWrapper = (ActivePhaseWrapper)obj;

		if (Validator.equals(_activePhase, activePhaseWrapper._activePhase)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public ActivePhase getWrappedActivePhase() {
		return _activePhase;
	}

	@Override
	public ActivePhase getWrappedModel() {
		return _activePhase;
	}

	@Override
	public void resetOriginalValues() {
		_activePhase.resetOriginalValues();
	}

	private ActivePhase _activePhase;
}