/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link UnitPrice}.
 * </p>
 *
 * @author zl
 * @see UnitPrice
 * @generated
 */
public class UnitPriceWrapper implements UnitPrice, ModelWrapper<UnitPrice> {
	public UnitPriceWrapper(UnitPrice unitPrice) {
		_unitPrice = unitPrice;
	}

	@Override
	public Class<?> getModelClass() {
		return UnitPrice.class;
	}

	@Override
	public String getModelClassName() {
		return UnitPrice.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("currency", getCurrency());
		attributes.put("salePhase", getSalePhase());
		attributes.put("price", getPrice());
		attributes.put("activeFlag", getActiveFlag());
		attributes.put("addDate", getAddDate());
		attributes.put("addUser", getAddUser());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String currency = (String)attributes.get("currency");

		if (currency != null) {
			setCurrency(currency);
		}

		Integer salePhase = (Integer)attributes.get("salePhase");

		if (salePhase != null) {
			setSalePhase(salePhase);
		}

		Double price = (Double)attributes.get("price");

		if (price != null) {
			setPrice(price);
		}

		String activeFlag = (String)attributes.get("activeFlag");

		if (activeFlag != null) {
			setActiveFlag(activeFlag);
		}

		Date addDate = (Date)attributes.get("addDate");

		if (addDate != null) {
			setAddDate(addDate);
		}

		Long addUser = (Long)attributes.get("addUser");

		if (addUser != null) {
			setAddUser(addUser);
		}
	}

	/**
	* Returns the primary key of this unit price.
	*
	* @return the primary key of this unit price
	*/
	@Override
	public com.aw.internal.service.persistence.UnitPricePK getPrimaryKey() {
		return _unitPrice.getPrimaryKey();
	}

	/**
	* Sets the primary key of this unit price.
	*
	* @param primaryKey the primary key of this unit price
	*/
	@Override
	public void setPrimaryKey(
		com.aw.internal.service.persistence.UnitPricePK primaryKey) {
		_unitPrice.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the currency of this unit price.
	*
	* @return the currency of this unit price
	*/
	@Override
	public java.lang.String getCurrency() {
		return _unitPrice.getCurrency();
	}

	/**
	* Sets the currency of this unit price.
	*
	* @param currency the currency of this unit price
	*/
	@Override
	public void setCurrency(java.lang.String currency) {
		_unitPrice.setCurrency(currency);
	}

	/**
	* Returns the sale phase of this unit price.
	*
	* @return the sale phase of this unit price
	*/
	@Override
	public int getSalePhase() {
		return _unitPrice.getSalePhase();
	}

	/**
	* Sets the sale phase of this unit price.
	*
	* @param salePhase the sale phase of this unit price
	*/
	@Override
	public void setSalePhase(int salePhase) {
		_unitPrice.setSalePhase(salePhase);
	}

	/**
	* Returns the price of this unit price.
	*
	* @return the price of this unit price
	*/
	@Override
	public double getPrice() {
		return _unitPrice.getPrice();
	}

	/**
	* Sets the price of this unit price.
	*
	* @param price the price of this unit price
	*/
	@Override
	public void setPrice(double price) {
		_unitPrice.setPrice(price);
	}

	/**
	* Returns the active flag of this unit price.
	*
	* @return the active flag of this unit price
	*/
	@Override
	public java.lang.String getActiveFlag() {
		return _unitPrice.getActiveFlag();
	}

	/**
	* Sets the active flag of this unit price.
	*
	* @param activeFlag the active flag of this unit price
	*/
	@Override
	public void setActiveFlag(java.lang.String activeFlag) {
		_unitPrice.setActiveFlag(activeFlag);
	}

	/**
	* Returns the add date of this unit price.
	*
	* @return the add date of this unit price
	*/
	@Override
	public java.util.Date getAddDate() {
		return _unitPrice.getAddDate();
	}

	/**
	* Sets the add date of this unit price.
	*
	* @param addDate the add date of this unit price
	*/
	@Override
	public void setAddDate(java.util.Date addDate) {
		_unitPrice.setAddDate(addDate);
	}

	/**
	* Returns the add user of this unit price.
	*
	* @return the add user of this unit price
	*/
	@Override
	public long getAddUser() {
		return _unitPrice.getAddUser();
	}

	/**
	* Sets the add user of this unit price.
	*
	* @param addUser the add user of this unit price
	*/
	@Override
	public void setAddUser(long addUser) {
		_unitPrice.setAddUser(addUser);
	}

	@Override
	public boolean isNew() {
		return _unitPrice.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_unitPrice.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _unitPrice.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_unitPrice.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _unitPrice.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _unitPrice.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_unitPrice.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _unitPrice.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_unitPrice.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_unitPrice.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_unitPrice.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new UnitPriceWrapper((UnitPrice)_unitPrice.clone());
	}

	@Override
	public int compareTo(com.aw.internal.model.UnitPrice unitPrice) {
		return _unitPrice.compareTo(unitPrice);
	}

	@Override
	public int hashCode() {
		return _unitPrice.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.aw.internal.model.UnitPrice> toCacheModel() {
		return _unitPrice.toCacheModel();
	}

	@Override
	public com.aw.internal.model.UnitPrice toEscapedModel() {
		return new UnitPriceWrapper(_unitPrice.toEscapedModel());
	}

	@Override
	public com.aw.internal.model.UnitPrice toUnescapedModel() {
		return new UnitPriceWrapper(_unitPrice.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _unitPrice.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _unitPrice.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_unitPrice.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UnitPriceWrapper)) {
			return false;
		}

		UnitPriceWrapper unitPriceWrapper = (UnitPriceWrapper)obj;

		if (Validator.equals(_unitPrice, unitPriceWrapper._unitPrice)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public UnitPrice getWrappedUnitPrice() {
		return _unitPrice;
	}

	@Override
	public UnitPrice getWrappedModel() {
		return _unitPrice;
	}

	@Override
	public void resetOriginalValues() {
		_unitPrice.resetOriginalValues();
	}

	private UnitPrice _unitPrice;
}