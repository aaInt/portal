/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link AaContact}.
 * </p>
 *
 * @author zl
 * @see AaContact
 * @generated
 */
public class AaContactWrapper implements AaContact, ModelWrapper<AaContact> {
	public AaContactWrapper(AaContact aaContact) {
		_aaContact = aaContact;
	}

	@Override
	public Class<?> getModelClass() {
		return AaContact.class;
	}

	@Override
	public String getModelClassName() {
		return AaContact.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("contactId", getContactId());
		attributes.put("phone", getPhone());
		attributes.put("areaCode", getAreaCode());
		attributes.put("email", getEmail());
		attributes.put("wechat", getWechat());
		attributes.put("facebook", getFacebook());
		attributes.put("twitter", getTwitter());
		attributes.put("weibo", getWeibo());
		attributes.put("street1", getStreet1());
		attributes.put("street2", getStreet2());
		attributes.put("city", getCity());
		attributes.put("state", getState());
		attributes.put("country", getCountry());
		attributes.put("zipCode", getZipCode());
		attributes.put("isMailDiff", getIsMailDiff());
		attributes.put("mailstreet1", getMailstreet1());
		attributes.put("mailstreet2", getMailstreet2());
		attributes.put("mailcity", getMailcity());
		attributes.put("mailstate", getMailstate());
		attributes.put("mailcountry", getMailcountry());
		attributes.put("mailzipCode", getMailzipCode());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long contactId = (Long)attributes.get("contactId");

		if (contactId != null) {
			setContactId(contactId);
		}

		Long phone = (Long)attributes.get("phone");

		if (phone != null) {
			setPhone(phone);
		}

		Integer areaCode = (Integer)attributes.get("areaCode");

		if (areaCode != null) {
			setAreaCode(areaCode);
		}

		String email = (String)attributes.get("email");

		if (email != null) {
			setEmail(email);
		}

		String wechat = (String)attributes.get("wechat");

		if (wechat != null) {
			setWechat(wechat);
		}

		String facebook = (String)attributes.get("facebook");

		if (facebook != null) {
			setFacebook(facebook);
		}

		String twitter = (String)attributes.get("twitter");

		if (twitter != null) {
			setTwitter(twitter);
		}

		String weibo = (String)attributes.get("weibo");

		if (weibo != null) {
			setWeibo(weibo);
		}

		String street1 = (String)attributes.get("street1");

		if (street1 != null) {
			setStreet1(street1);
		}

		String street2 = (String)attributes.get("street2");

		if (street2 != null) {
			setStreet2(street2);
		}

		String city = (String)attributes.get("city");

		if (city != null) {
			setCity(city);
		}

		String state = (String)attributes.get("state");

		if (state != null) {
			setState(state);
		}

		String country = (String)attributes.get("country");

		if (country != null) {
			setCountry(country);
		}

		String zipCode = (String)attributes.get("zipCode");

		if (zipCode != null) {
			setZipCode(zipCode);
		}

		String isMailDiff = (String)attributes.get("isMailDiff");

		if (isMailDiff != null) {
			setIsMailDiff(isMailDiff);
		}

		String mailstreet1 = (String)attributes.get("mailstreet1");

		if (mailstreet1 != null) {
			setMailstreet1(mailstreet1);
		}

		String mailstreet2 = (String)attributes.get("mailstreet2");

		if (mailstreet2 != null) {
			setMailstreet2(mailstreet2);
		}

		String mailcity = (String)attributes.get("mailcity");

		if (mailcity != null) {
			setMailcity(mailcity);
		}

		String mailstate = (String)attributes.get("mailstate");

		if (mailstate != null) {
			setMailstate(mailstate);
		}

		String mailcountry = (String)attributes.get("mailcountry");

		if (mailcountry != null) {
			setMailcountry(mailcountry);
		}

		String mailzipCode = (String)attributes.get("mailzipCode");

		if (mailzipCode != null) {
			setMailzipCode(mailzipCode);
		}
	}

	/**
	* Returns the primary key of this aa contact.
	*
	* @return the primary key of this aa contact
	*/
	@Override
	public long getPrimaryKey() {
		return _aaContact.getPrimaryKey();
	}

	/**
	* Sets the primary key of this aa contact.
	*
	* @param primaryKey the primary key of this aa contact
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_aaContact.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the contact ID of this aa contact.
	*
	* @return the contact ID of this aa contact
	*/
	@Override
	public long getContactId() {
		return _aaContact.getContactId();
	}

	/**
	* Sets the contact ID of this aa contact.
	*
	* @param contactId the contact ID of this aa contact
	*/
	@Override
	public void setContactId(long contactId) {
		_aaContact.setContactId(contactId);
	}

	/**
	* Returns the phone of this aa contact.
	*
	* @return the phone of this aa contact
	*/
	@Override
	public long getPhone() {
		return _aaContact.getPhone();
	}

	/**
	* Sets the phone of this aa contact.
	*
	* @param phone the phone of this aa contact
	*/
	@Override
	public void setPhone(long phone) {
		_aaContact.setPhone(phone);
	}

	/**
	* Returns the area code of this aa contact.
	*
	* @return the area code of this aa contact
	*/
	@Override
	public int getAreaCode() {
		return _aaContact.getAreaCode();
	}

	/**
	* Sets the area code of this aa contact.
	*
	* @param areaCode the area code of this aa contact
	*/
	@Override
	public void setAreaCode(int areaCode) {
		_aaContact.setAreaCode(areaCode);
	}

	/**
	* Returns the email of this aa contact.
	*
	* @return the email of this aa contact
	*/
	@Override
	public java.lang.String getEmail() {
		return _aaContact.getEmail();
	}

	/**
	* Sets the email of this aa contact.
	*
	* @param email the email of this aa contact
	*/
	@Override
	public void setEmail(java.lang.String email) {
		_aaContact.setEmail(email);
	}

	/**
	* Returns the wechat of this aa contact.
	*
	* @return the wechat of this aa contact
	*/
	@Override
	public java.lang.String getWechat() {
		return _aaContact.getWechat();
	}

	/**
	* Sets the wechat of this aa contact.
	*
	* @param wechat the wechat of this aa contact
	*/
	@Override
	public void setWechat(java.lang.String wechat) {
		_aaContact.setWechat(wechat);
	}

	/**
	* Returns the facebook of this aa contact.
	*
	* @return the facebook of this aa contact
	*/
	@Override
	public java.lang.String getFacebook() {
		return _aaContact.getFacebook();
	}

	/**
	* Sets the facebook of this aa contact.
	*
	* @param facebook the facebook of this aa contact
	*/
	@Override
	public void setFacebook(java.lang.String facebook) {
		_aaContact.setFacebook(facebook);
	}

	/**
	* Returns the twitter of this aa contact.
	*
	* @return the twitter of this aa contact
	*/
	@Override
	public java.lang.String getTwitter() {
		return _aaContact.getTwitter();
	}

	/**
	* Sets the twitter of this aa contact.
	*
	* @param twitter the twitter of this aa contact
	*/
	@Override
	public void setTwitter(java.lang.String twitter) {
		_aaContact.setTwitter(twitter);
	}

	/**
	* Returns the weibo of this aa contact.
	*
	* @return the weibo of this aa contact
	*/
	@Override
	public java.lang.String getWeibo() {
		return _aaContact.getWeibo();
	}

	/**
	* Sets the weibo of this aa contact.
	*
	* @param weibo the weibo of this aa contact
	*/
	@Override
	public void setWeibo(java.lang.String weibo) {
		_aaContact.setWeibo(weibo);
	}

	/**
	* Returns the street1 of this aa contact.
	*
	* @return the street1 of this aa contact
	*/
	@Override
	public java.lang.String getStreet1() {
		return _aaContact.getStreet1();
	}

	/**
	* Sets the street1 of this aa contact.
	*
	* @param street1 the street1 of this aa contact
	*/
	@Override
	public void setStreet1(java.lang.String street1) {
		_aaContact.setStreet1(street1);
	}

	/**
	* Returns the street2 of this aa contact.
	*
	* @return the street2 of this aa contact
	*/
	@Override
	public java.lang.String getStreet2() {
		return _aaContact.getStreet2();
	}

	/**
	* Sets the street2 of this aa contact.
	*
	* @param street2 the street2 of this aa contact
	*/
	@Override
	public void setStreet2(java.lang.String street2) {
		_aaContact.setStreet2(street2);
	}

	/**
	* Returns the city of this aa contact.
	*
	* @return the city of this aa contact
	*/
	@Override
	public java.lang.String getCity() {
		return _aaContact.getCity();
	}

	/**
	* Sets the city of this aa contact.
	*
	* @param city the city of this aa contact
	*/
	@Override
	public void setCity(java.lang.String city) {
		_aaContact.setCity(city);
	}

	/**
	* Returns the state of this aa contact.
	*
	* @return the state of this aa contact
	*/
	@Override
	public java.lang.String getState() {
		return _aaContact.getState();
	}

	/**
	* Sets the state of this aa contact.
	*
	* @param state the state of this aa contact
	*/
	@Override
	public void setState(java.lang.String state) {
		_aaContact.setState(state);
	}

	/**
	* Returns the country of this aa contact.
	*
	* @return the country of this aa contact
	*/
	@Override
	public java.lang.String getCountry() {
		return _aaContact.getCountry();
	}

	/**
	* Sets the country of this aa contact.
	*
	* @param country the country of this aa contact
	*/
	@Override
	public void setCountry(java.lang.String country) {
		_aaContact.setCountry(country);
	}

	/**
	* Returns the zip code of this aa contact.
	*
	* @return the zip code of this aa contact
	*/
	@Override
	public java.lang.String getZipCode() {
		return _aaContact.getZipCode();
	}

	/**
	* Sets the zip code of this aa contact.
	*
	* @param zipCode the zip code of this aa contact
	*/
	@Override
	public void setZipCode(java.lang.String zipCode) {
		_aaContact.setZipCode(zipCode);
	}

	/**
	* Returns the is mail diff of this aa contact.
	*
	* @return the is mail diff of this aa contact
	*/
	@Override
	public java.lang.String getIsMailDiff() {
		return _aaContact.getIsMailDiff();
	}

	/**
	* Sets the is mail diff of this aa contact.
	*
	* @param isMailDiff the is mail diff of this aa contact
	*/
	@Override
	public void setIsMailDiff(java.lang.String isMailDiff) {
		_aaContact.setIsMailDiff(isMailDiff);
	}

	/**
	* Returns the mailstreet1 of this aa contact.
	*
	* @return the mailstreet1 of this aa contact
	*/
	@Override
	public java.lang.String getMailstreet1() {
		return _aaContact.getMailstreet1();
	}

	/**
	* Sets the mailstreet1 of this aa contact.
	*
	* @param mailstreet1 the mailstreet1 of this aa contact
	*/
	@Override
	public void setMailstreet1(java.lang.String mailstreet1) {
		_aaContact.setMailstreet1(mailstreet1);
	}

	/**
	* Returns the mailstreet2 of this aa contact.
	*
	* @return the mailstreet2 of this aa contact
	*/
	@Override
	public java.lang.String getMailstreet2() {
		return _aaContact.getMailstreet2();
	}

	/**
	* Sets the mailstreet2 of this aa contact.
	*
	* @param mailstreet2 the mailstreet2 of this aa contact
	*/
	@Override
	public void setMailstreet2(java.lang.String mailstreet2) {
		_aaContact.setMailstreet2(mailstreet2);
	}

	/**
	* Returns the mailcity of this aa contact.
	*
	* @return the mailcity of this aa contact
	*/
	@Override
	public java.lang.String getMailcity() {
		return _aaContact.getMailcity();
	}

	/**
	* Sets the mailcity of this aa contact.
	*
	* @param mailcity the mailcity of this aa contact
	*/
	@Override
	public void setMailcity(java.lang.String mailcity) {
		_aaContact.setMailcity(mailcity);
	}

	/**
	* Returns the mailstate of this aa contact.
	*
	* @return the mailstate of this aa contact
	*/
	@Override
	public java.lang.String getMailstate() {
		return _aaContact.getMailstate();
	}

	/**
	* Sets the mailstate of this aa contact.
	*
	* @param mailstate the mailstate of this aa contact
	*/
	@Override
	public void setMailstate(java.lang.String mailstate) {
		_aaContact.setMailstate(mailstate);
	}

	/**
	* Returns the mailcountry of this aa contact.
	*
	* @return the mailcountry of this aa contact
	*/
	@Override
	public java.lang.String getMailcountry() {
		return _aaContact.getMailcountry();
	}

	/**
	* Sets the mailcountry of this aa contact.
	*
	* @param mailcountry the mailcountry of this aa contact
	*/
	@Override
	public void setMailcountry(java.lang.String mailcountry) {
		_aaContact.setMailcountry(mailcountry);
	}

	/**
	* Returns the mailzip code of this aa contact.
	*
	* @return the mailzip code of this aa contact
	*/
	@Override
	public java.lang.String getMailzipCode() {
		return _aaContact.getMailzipCode();
	}

	/**
	* Sets the mailzip code of this aa contact.
	*
	* @param mailzipCode the mailzip code of this aa contact
	*/
	@Override
	public void setMailzipCode(java.lang.String mailzipCode) {
		_aaContact.setMailzipCode(mailzipCode);
	}

	@Override
	public boolean isNew() {
		return _aaContact.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_aaContact.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _aaContact.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_aaContact.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _aaContact.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _aaContact.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_aaContact.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _aaContact.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_aaContact.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_aaContact.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_aaContact.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new AaContactWrapper((AaContact)_aaContact.clone());
	}

	@Override
	public int compareTo(com.aw.internal.model.AaContact aaContact) {
		return _aaContact.compareTo(aaContact);
	}

	@Override
	public int hashCode() {
		return _aaContact.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.aw.internal.model.AaContact> toCacheModel() {
		return _aaContact.toCacheModel();
	}

	@Override
	public com.aw.internal.model.AaContact toEscapedModel() {
		return new AaContactWrapper(_aaContact.toEscapedModel());
	}

	@Override
	public com.aw.internal.model.AaContact toUnescapedModel() {
		return new AaContactWrapper(_aaContact.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _aaContact.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _aaContact.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_aaContact.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof AaContactWrapper)) {
			return false;
		}

		AaContactWrapper aaContactWrapper = (AaContactWrapper)obj;

		if (Validator.equals(_aaContact, aaContactWrapper._aaContact)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public AaContact getWrappedAaContact() {
		return _aaContact;
	}

	@Override
	public AaContact getWrappedModel() {
		return _aaContact;
	}

	@Override
	public void resetOriginalValues() {
		_aaContact.resetOriginalValues();
	}

	private AaContact _aaContact;
}