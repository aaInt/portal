/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model;

import com.aw.internal.service.BrokerInfoLocalServiceUtil;
import com.aw.internal.service.ClpSerializer;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zl
 */
public class BrokerInfoClp extends BaseModelImpl<BrokerInfo>
	implements BrokerInfo {
	public BrokerInfoClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return BrokerInfo.class;
	}

	@Override
	public String getModelClassName() {
		return BrokerInfo.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _brokerId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setBrokerId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _brokerId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("brokerId", getBrokerId());
		attributes.put("licenseId", getLicenseId());
		attributes.put("licenseCountry", getLicenseCountry());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long brokerId = (Long)attributes.get("brokerId");

		if (brokerId != null) {
			setBrokerId(brokerId);
		}

		String licenseId = (String)attributes.get("licenseId");

		if (licenseId != null) {
			setLicenseId(licenseId);
		}

		String licenseCountry = (String)attributes.get("licenseCountry");

		if (licenseCountry != null) {
			setLicenseCountry(licenseCountry);
		}
	}

	@Override
	public long getBrokerId() {
		return _brokerId;
	}

	@Override
	public void setBrokerId(long brokerId) {
		_brokerId = brokerId;

		if (_brokerInfoRemoteModel != null) {
			try {
				Class<?> clazz = _brokerInfoRemoteModel.getClass();

				Method method = clazz.getMethod("setBrokerId", long.class);

				method.invoke(_brokerInfoRemoteModel, brokerId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLicenseId() {
		return _licenseId;
	}

	@Override
	public void setLicenseId(String licenseId) {
		_licenseId = licenseId;

		if (_brokerInfoRemoteModel != null) {
			try {
				Class<?> clazz = _brokerInfoRemoteModel.getClass();

				Method method = clazz.getMethod("setLicenseId", String.class);

				method.invoke(_brokerInfoRemoteModel, licenseId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLicenseCountry() {
		return _licenseCountry;
	}

	@Override
	public void setLicenseCountry(String licenseCountry) {
		_licenseCountry = licenseCountry;

		if (_brokerInfoRemoteModel != null) {
			try {
				Class<?> clazz = _brokerInfoRemoteModel.getClass();

				Method method = clazz.getMethod("setLicenseCountry",
						String.class);

				method.invoke(_brokerInfoRemoteModel, licenseCountry);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getBrokerInfoRemoteModel() {
		return _brokerInfoRemoteModel;
	}

	public void setBrokerInfoRemoteModel(BaseModel<?> brokerInfoRemoteModel) {
		_brokerInfoRemoteModel = brokerInfoRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _brokerInfoRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_brokerInfoRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			BrokerInfoLocalServiceUtil.addBrokerInfo(this);
		}
		else {
			BrokerInfoLocalServiceUtil.updateBrokerInfo(this);
		}
	}

	@Override
	public BrokerInfo toEscapedModel() {
		return (BrokerInfo)ProxyUtil.newProxyInstance(BrokerInfo.class.getClassLoader(),
			new Class[] { BrokerInfo.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		BrokerInfoClp clone = new BrokerInfoClp();

		clone.setBrokerId(getBrokerId());
		clone.setLicenseId(getLicenseId());
		clone.setLicenseCountry(getLicenseCountry());

		return clone;
	}

	@Override
	public int compareTo(BrokerInfo brokerInfo) {
		long primaryKey = brokerInfo.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof BrokerInfoClp)) {
			return false;
		}

		BrokerInfoClp brokerInfo = (BrokerInfoClp)obj;

		long primaryKey = brokerInfo.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{brokerId=");
		sb.append(getBrokerId());
		sb.append(", licenseId=");
		sb.append(getLicenseId());
		sb.append(", licenseCountry=");
		sb.append(getLicenseCountry());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(13);

		sb.append("<model><model-name>");
		sb.append("com.aw.internal.model.BrokerInfo");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>brokerId</column-name><column-value><![CDATA[");
		sb.append(getBrokerId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>licenseId</column-name><column-value><![CDATA[");
		sb.append(getLicenseId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>licenseCountry</column-name><column-value><![CDATA[");
		sb.append(getLicenseCountry());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _brokerId;
	private String _licenseId;
	private String _licenseCountry;
	private BaseModel<?> _brokerInfoRemoteModel;
	private Class<?> _clpSerializerClass = com.aw.internal.service.ClpSerializer.class;
}