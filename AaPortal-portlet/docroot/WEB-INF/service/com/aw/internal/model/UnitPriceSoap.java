/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model;

import com.aw.internal.service.persistence.UnitPricePK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author zl
 * @generated
 */
public class UnitPriceSoap implements Serializable {
	public static UnitPriceSoap toSoapModel(UnitPrice model) {
		UnitPriceSoap soapModel = new UnitPriceSoap();

		soapModel.setCurrency(model.getCurrency());
		soapModel.setSalePhase(model.getSalePhase());
		soapModel.setPrice(model.getPrice());
		soapModel.setActiveFlag(model.getActiveFlag());
		soapModel.setAddDate(model.getAddDate());
		soapModel.setAddUser(model.getAddUser());

		return soapModel;
	}

	public static UnitPriceSoap[] toSoapModels(UnitPrice[] models) {
		UnitPriceSoap[] soapModels = new UnitPriceSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static UnitPriceSoap[][] toSoapModels(UnitPrice[][] models) {
		UnitPriceSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new UnitPriceSoap[models.length][models[0].length];
		}
		else {
			soapModels = new UnitPriceSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static UnitPriceSoap[] toSoapModels(List<UnitPrice> models) {
		List<UnitPriceSoap> soapModels = new ArrayList<UnitPriceSoap>(models.size());

		for (UnitPrice model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new UnitPriceSoap[soapModels.size()]);
	}

	public UnitPriceSoap() {
	}

	public UnitPricePK getPrimaryKey() {
		return new UnitPricePK(_currency, _salePhase);
	}

	public void setPrimaryKey(UnitPricePK pk) {
		setCurrency(pk.currency);
		setSalePhase(pk.salePhase);
	}

	public String getCurrency() {
		return _currency;
	}

	public void setCurrency(String currency) {
		_currency = currency;
	}

	public int getSalePhase() {
		return _salePhase;
	}

	public void setSalePhase(int salePhase) {
		_salePhase = salePhase;
	}

	public double getPrice() {
		return _price;
	}

	public void setPrice(double price) {
		_price = price;
	}

	public String getActiveFlag() {
		return _activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		_activeFlag = activeFlag;
	}

	public Date getAddDate() {
		return _addDate;
	}

	public void setAddDate(Date addDate) {
		_addDate = addDate;
	}

	public long getAddUser() {
		return _addUser;
	}

	public void setAddUser(long addUser) {
		_addUser = addUser;
	}

	private String _currency;
	private int _salePhase;
	private double _price;
	private String _activeFlag;
	private Date _addDate;
	private long _addUser;
}