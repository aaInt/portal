/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author zl
 * @generated
 */
public class FundInfoSoap implements Serializable {
	public static FundInfoSoap toSoapModel(FundInfo model) {
		FundInfoSoap soapModel = new FundInfoSoap();

		soapModel.setFundId(model.getFundId());
		soapModel.setName(model.getName());
		soapModel.setContactId(model.getContactId());

		return soapModel;
	}

	public static FundInfoSoap[] toSoapModels(FundInfo[] models) {
		FundInfoSoap[] soapModels = new FundInfoSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static FundInfoSoap[][] toSoapModels(FundInfo[][] models) {
		FundInfoSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new FundInfoSoap[models.length][models[0].length];
		}
		else {
			soapModels = new FundInfoSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static FundInfoSoap[] toSoapModels(List<FundInfo> models) {
		List<FundInfoSoap> soapModels = new ArrayList<FundInfoSoap>(models.size());

		for (FundInfo model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new FundInfoSoap[soapModels.size()]);
	}

	public FundInfoSoap() {
	}

	public long getPrimaryKey() {
		return _fundId;
	}

	public void setPrimaryKey(long pk) {
		setFundId(pk);
	}

	public long getFundId() {
		return _fundId;
	}

	public void setFundId(long fundId) {
		_fundId = fundId;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	public long getContactId() {
		return _contactId;
	}

	public void setContactId(long contactId) {
		_contactId = contactId;
	}

	private long _fundId;
	private String _name;
	private long _contactId;
}