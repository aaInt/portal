/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model;

import com.aw.internal.service.ClpSerializer;
import com.aw.internal.service.UnitPriceLocalServiceUtil;
import com.aw.internal.service.persistence.UnitPricePK;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author zl
 */
public class UnitPriceClp extends BaseModelImpl<UnitPrice> implements UnitPrice {
	public UnitPriceClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return UnitPrice.class;
	}

	@Override
	public String getModelClassName() {
		return UnitPrice.class.getName();
	}

	@Override
	public UnitPricePK getPrimaryKey() {
		return new UnitPricePK(_currency, _salePhase);
	}

	@Override
	public void setPrimaryKey(UnitPricePK primaryKey) {
		setCurrency(primaryKey.currency);
		setSalePhase(primaryKey.salePhase);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new UnitPricePK(_currency, _salePhase);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((UnitPricePK)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("currency", getCurrency());
		attributes.put("salePhase", getSalePhase());
		attributes.put("price", getPrice());
		attributes.put("activeFlag", getActiveFlag());
		attributes.put("addDate", getAddDate());
		attributes.put("addUser", getAddUser());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String currency = (String)attributes.get("currency");

		if (currency != null) {
			setCurrency(currency);
		}

		Integer salePhase = (Integer)attributes.get("salePhase");

		if (salePhase != null) {
			setSalePhase(salePhase);
		}

		Double price = (Double)attributes.get("price");

		if (price != null) {
			setPrice(price);
		}

		String activeFlag = (String)attributes.get("activeFlag");

		if (activeFlag != null) {
			setActiveFlag(activeFlag);
		}

		Date addDate = (Date)attributes.get("addDate");

		if (addDate != null) {
			setAddDate(addDate);
		}

		Long addUser = (Long)attributes.get("addUser");

		if (addUser != null) {
			setAddUser(addUser);
		}
	}

	@Override
	public String getCurrency() {
		return _currency;
	}

	@Override
	public void setCurrency(String currency) {
		_currency = currency;

		if (_unitPriceRemoteModel != null) {
			try {
				Class<?> clazz = _unitPriceRemoteModel.getClass();

				Method method = clazz.getMethod("setCurrency", String.class);

				method.invoke(_unitPriceRemoteModel, currency);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getSalePhase() {
		return _salePhase;
	}

	@Override
	public void setSalePhase(int salePhase) {
		_salePhase = salePhase;

		if (_unitPriceRemoteModel != null) {
			try {
				Class<?> clazz = _unitPriceRemoteModel.getClass();

				Method method = clazz.getMethod("setSalePhase", int.class);

				method.invoke(_unitPriceRemoteModel, salePhase);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getPrice() {
		return _price;
	}

	@Override
	public void setPrice(double price) {
		_price = price;

		if (_unitPriceRemoteModel != null) {
			try {
				Class<?> clazz = _unitPriceRemoteModel.getClass();

				Method method = clazz.getMethod("setPrice", double.class);

				method.invoke(_unitPriceRemoteModel, price);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getActiveFlag() {
		return _activeFlag;
	}

	@Override
	public void setActiveFlag(String activeFlag) {
		_activeFlag = activeFlag;

		if (_unitPriceRemoteModel != null) {
			try {
				Class<?> clazz = _unitPriceRemoteModel.getClass();

				Method method = clazz.getMethod("setActiveFlag", String.class);

				method.invoke(_unitPriceRemoteModel, activeFlag);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getAddDate() {
		return _addDate;
	}

	@Override
	public void setAddDate(Date addDate) {
		_addDate = addDate;

		if (_unitPriceRemoteModel != null) {
			try {
				Class<?> clazz = _unitPriceRemoteModel.getClass();

				Method method = clazz.getMethod("setAddDate", Date.class);

				method.invoke(_unitPriceRemoteModel, addDate);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getAddUser() {
		return _addUser;
	}

	@Override
	public void setAddUser(long addUser) {
		_addUser = addUser;

		if (_unitPriceRemoteModel != null) {
			try {
				Class<?> clazz = _unitPriceRemoteModel.getClass();

				Method method = clazz.getMethod("setAddUser", long.class);

				method.invoke(_unitPriceRemoteModel, addUser);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getUnitPriceRemoteModel() {
		return _unitPriceRemoteModel;
	}

	public void setUnitPriceRemoteModel(BaseModel<?> unitPriceRemoteModel) {
		_unitPriceRemoteModel = unitPriceRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _unitPriceRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_unitPriceRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			UnitPriceLocalServiceUtil.addUnitPrice(this);
		}
		else {
			UnitPriceLocalServiceUtil.updateUnitPrice(this);
		}
	}

	@Override
	public UnitPrice toEscapedModel() {
		return (UnitPrice)ProxyUtil.newProxyInstance(UnitPrice.class.getClassLoader(),
			new Class[] { UnitPrice.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		UnitPriceClp clone = new UnitPriceClp();

		clone.setCurrency(getCurrency());
		clone.setSalePhase(getSalePhase());
		clone.setPrice(getPrice());
		clone.setActiveFlag(getActiveFlag());
		clone.setAddDate(getAddDate());
		clone.setAddUser(getAddUser());

		return clone;
	}

	@Override
	public int compareTo(UnitPrice unitPrice) {
		UnitPricePK primaryKey = unitPrice.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UnitPriceClp)) {
			return false;
		}

		UnitPriceClp unitPrice = (UnitPriceClp)obj;

		UnitPricePK primaryKey = unitPrice.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{currency=");
		sb.append(getCurrency());
		sb.append(", salePhase=");
		sb.append(getSalePhase());
		sb.append(", price=");
		sb.append(getPrice());
		sb.append(", activeFlag=");
		sb.append(getActiveFlag());
		sb.append(", addDate=");
		sb.append(getAddDate());
		sb.append(", addUser=");
		sb.append(getAddUser());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(22);

		sb.append("<model><model-name>");
		sb.append("com.aw.internal.model.UnitPrice");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>currency</column-name><column-value><![CDATA[");
		sb.append(getCurrency());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>salePhase</column-name><column-value><![CDATA[");
		sb.append(getSalePhase());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>price</column-name><column-value><![CDATA[");
		sb.append(getPrice());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>activeFlag</column-name><column-value><![CDATA[");
		sb.append(getActiveFlag());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>addDate</column-name><column-value><![CDATA[");
		sb.append(getAddDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>addUser</column-name><column-value><![CDATA[");
		sb.append(getAddUser());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private String _currency;
	private int _salePhase;
	private double _price;
	private String _activeFlag;
	private Date _addDate;
	private long _addUser;
	private BaseModel<?> _unitPriceRemoteModel;
	private Class<?> _clpSerializerClass = com.aw.internal.service.ClpSerializer.class;
}