/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CoinCount}.
 * </p>
 *
 * @author zl
 * @see CoinCount
 * @generated
 */
public class CoinCountWrapper implements CoinCount, ModelWrapper<CoinCount> {
	public CoinCountWrapper(CoinCount coinCount) {
		_coinCount = coinCount;
	}

	@Override
	public Class<?> getModelClass() {
		return CoinCount.class;
	}

	@Override
	public String getModelClassName() {
		return CoinCount.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("phase", getPhase());
		attributes.put("totalAmount", getTotalAmount());
		attributes.put("endFlag", getEndFlag());
		attributes.put("rollToPhase", getRollToPhase());
		attributes.put("rollToAmt", getRollToAmt());
		attributes.put("limit", getLimit());
		attributes.put("rollOverAmt", getRollOverAmt());
		attributes.put("lastOrderId", getLastOrderId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer phase = (Integer)attributes.get("phase");

		if (phase != null) {
			setPhase(phase);
		}

		Long totalAmount = (Long)attributes.get("totalAmount");

		if (totalAmount != null) {
			setTotalAmount(totalAmount);
		}

		String endFlag = (String)attributes.get("endFlag");

		if (endFlag != null) {
			setEndFlag(endFlag);
		}

		Integer rollToPhase = (Integer)attributes.get("rollToPhase");

		if (rollToPhase != null) {
			setRollToPhase(rollToPhase);
		}

		Long rollToAmt = (Long)attributes.get("rollToAmt");

		if (rollToAmt != null) {
			setRollToAmt(rollToAmt);
		}

		Long limit = (Long)attributes.get("limit");

		if (limit != null) {
			setLimit(limit);
		}

		Long rollOverAmt = (Long)attributes.get("rollOverAmt");

		if (rollOverAmt != null) {
			setRollOverAmt(rollOverAmt);
		}

		String lastOrderId = (String)attributes.get("lastOrderId");

		if (lastOrderId != null) {
			setLastOrderId(lastOrderId);
		}
	}

	/**
	* Returns the primary key of this coin count.
	*
	* @return the primary key of this coin count
	*/
	@Override
	public int getPrimaryKey() {
		return _coinCount.getPrimaryKey();
	}

	/**
	* Sets the primary key of this coin count.
	*
	* @param primaryKey the primary key of this coin count
	*/
	@Override
	public void setPrimaryKey(int primaryKey) {
		_coinCount.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the phase of this coin count.
	*
	* @return the phase of this coin count
	*/
	@Override
	public int getPhase() {
		return _coinCount.getPhase();
	}

	/**
	* Sets the phase of this coin count.
	*
	* @param phase the phase of this coin count
	*/
	@Override
	public void setPhase(int phase) {
		_coinCount.setPhase(phase);
	}

	/**
	* Returns the total amount of this coin count.
	*
	* @return the total amount of this coin count
	*/
	@Override
	public long getTotalAmount() {
		return _coinCount.getTotalAmount();
	}

	/**
	* Sets the total amount of this coin count.
	*
	* @param totalAmount the total amount of this coin count
	*/
	@Override
	public void setTotalAmount(long totalAmount) {
		_coinCount.setTotalAmount(totalAmount);
	}

	/**
	* Returns the end flag of this coin count.
	*
	* @return the end flag of this coin count
	*/
	@Override
	public java.lang.String getEndFlag() {
		return _coinCount.getEndFlag();
	}

	/**
	* Sets the end flag of this coin count.
	*
	* @param endFlag the end flag of this coin count
	*/
	@Override
	public void setEndFlag(java.lang.String endFlag) {
		_coinCount.setEndFlag(endFlag);
	}

	/**
	* Returns the roll to phase of this coin count.
	*
	* @return the roll to phase of this coin count
	*/
	@Override
	public int getRollToPhase() {
		return _coinCount.getRollToPhase();
	}

	/**
	* Sets the roll to phase of this coin count.
	*
	* @param rollToPhase the roll to phase of this coin count
	*/
	@Override
	public void setRollToPhase(int rollToPhase) {
		_coinCount.setRollToPhase(rollToPhase);
	}

	/**
	* Returns the roll to amt of this coin count.
	*
	* @return the roll to amt of this coin count
	*/
	@Override
	public long getRollToAmt() {
		return _coinCount.getRollToAmt();
	}

	/**
	* Sets the roll to amt of this coin count.
	*
	* @param rollToAmt the roll to amt of this coin count
	*/
	@Override
	public void setRollToAmt(long rollToAmt) {
		_coinCount.setRollToAmt(rollToAmt);
	}

	/**
	* Returns the limit of this coin count.
	*
	* @return the limit of this coin count
	*/
	@Override
	public long getLimit() {
		return _coinCount.getLimit();
	}

	/**
	* Sets the limit of this coin count.
	*
	* @param limit the limit of this coin count
	*/
	@Override
	public void setLimit(long limit) {
		_coinCount.setLimit(limit);
	}

	/**
	* Returns the roll over amt of this coin count.
	*
	* @return the roll over amt of this coin count
	*/
	@Override
	public long getRollOverAmt() {
		return _coinCount.getRollOverAmt();
	}

	/**
	* Sets the roll over amt of this coin count.
	*
	* @param rollOverAmt the roll over amt of this coin count
	*/
	@Override
	public void setRollOverAmt(long rollOverAmt) {
		_coinCount.setRollOverAmt(rollOverAmt);
	}

	/**
	* Returns the last order ID of this coin count.
	*
	* @return the last order ID of this coin count
	*/
	@Override
	public java.lang.String getLastOrderId() {
		return _coinCount.getLastOrderId();
	}

	/**
	* Sets the last order ID of this coin count.
	*
	* @param lastOrderId the last order ID of this coin count
	*/
	@Override
	public void setLastOrderId(java.lang.String lastOrderId) {
		_coinCount.setLastOrderId(lastOrderId);
	}

	@Override
	public boolean isNew() {
		return _coinCount.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_coinCount.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _coinCount.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_coinCount.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _coinCount.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _coinCount.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_coinCount.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _coinCount.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_coinCount.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_coinCount.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_coinCount.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new CoinCountWrapper((CoinCount)_coinCount.clone());
	}

	@Override
	public int compareTo(com.aw.internal.model.CoinCount coinCount) {
		return _coinCount.compareTo(coinCount);
	}

	@Override
	public int hashCode() {
		return _coinCount.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.aw.internal.model.CoinCount> toCacheModel() {
		return _coinCount.toCacheModel();
	}

	@Override
	public com.aw.internal.model.CoinCount toEscapedModel() {
		return new CoinCountWrapper(_coinCount.toEscapedModel());
	}

	@Override
	public com.aw.internal.model.CoinCount toUnescapedModel() {
		return new CoinCountWrapper(_coinCount.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _coinCount.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _coinCount.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_coinCount.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CoinCountWrapper)) {
			return false;
		}

		CoinCountWrapper coinCountWrapper = (CoinCountWrapper)obj;

		if (Validator.equals(_coinCount, coinCountWrapper._coinCount)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public CoinCount getWrappedCoinCount() {
		return _coinCount;
	}

	@Override
	public CoinCount getWrappedModel() {
		return _coinCount;
	}

	@Override
	public void resetOriginalValues() {
		_coinCount.resetOriginalValues();
	}

	private CoinCount _coinCount;
}