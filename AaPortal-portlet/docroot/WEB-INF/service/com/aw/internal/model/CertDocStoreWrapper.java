/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.sql.Blob;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CertDocStore}.
 * </p>
 *
 * @author zl
 * @see CertDocStore
 * @generated
 */
public class CertDocStoreWrapper implements CertDocStore,
	ModelWrapper<CertDocStore> {
	public CertDocStoreWrapper(CertDocStore certDocStore) {
		_certDocStore = certDocStore;
	}

	@Override
	public Class<?> getModelClass() {
		return CertDocStore.class;
	}

	@Override
	public String getModelClassName() {
		return CertDocStore.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("docId", getDocId());
		attributes.put("content", getContent());
		attributes.put("activeFlag", getActiveFlag());
		attributes.put("addDate", getAddDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long docId = (Long)attributes.get("docId");

		if (docId != null) {
			setDocId(docId);
		}

		Blob content = (Blob)attributes.get("content");

		if (content != null) {
			setContent(content);
		}

		String activeFlag = (String)attributes.get("activeFlag");

		if (activeFlag != null) {
			setActiveFlag(activeFlag);
		}

		Date addDate = (Date)attributes.get("addDate");

		if (addDate != null) {
			setAddDate(addDate);
		}
	}

	/**
	* Returns the primary key of this cert doc store.
	*
	* @return the primary key of this cert doc store
	*/
	@Override
	public long getPrimaryKey() {
		return _certDocStore.getPrimaryKey();
	}

	/**
	* Sets the primary key of this cert doc store.
	*
	* @param primaryKey the primary key of this cert doc store
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_certDocStore.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the doc ID of this cert doc store.
	*
	* @return the doc ID of this cert doc store
	*/
	@Override
	public long getDocId() {
		return _certDocStore.getDocId();
	}

	/**
	* Sets the doc ID of this cert doc store.
	*
	* @param docId the doc ID of this cert doc store
	*/
	@Override
	public void setDocId(long docId) {
		_certDocStore.setDocId(docId);
	}

	/**
	* Returns the content of this cert doc store.
	*
	* @return the content of this cert doc store
	*/
	@Override
	public java.sql.Blob getContent() {
		return _certDocStore.getContent();
	}

	/**
	* Sets the content of this cert doc store.
	*
	* @param content the content of this cert doc store
	*/
	@Override
	public void setContent(java.sql.Blob content) {
		_certDocStore.setContent(content);
	}

	/**
	* Returns the active flag of this cert doc store.
	*
	* @return the active flag of this cert doc store
	*/
	@Override
	public java.lang.String getActiveFlag() {
		return _certDocStore.getActiveFlag();
	}

	/**
	* Sets the active flag of this cert doc store.
	*
	* @param activeFlag the active flag of this cert doc store
	*/
	@Override
	public void setActiveFlag(java.lang.String activeFlag) {
		_certDocStore.setActiveFlag(activeFlag);
	}

	/**
	* Returns the add date of this cert doc store.
	*
	* @return the add date of this cert doc store
	*/
	@Override
	public java.util.Date getAddDate() {
		return _certDocStore.getAddDate();
	}

	/**
	* Sets the add date of this cert doc store.
	*
	* @param addDate the add date of this cert doc store
	*/
	@Override
	public void setAddDate(java.util.Date addDate) {
		_certDocStore.setAddDate(addDate);
	}

	@Override
	public boolean isNew() {
		return _certDocStore.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_certDocStore.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _certDocStore.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_certDocStore.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _certDocStore.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _certDocStore.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_certDocStore.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _certDocStore.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_certDocStore.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_certDocStore.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_certDocStore.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new CertDocStoreWrapper((CertDocStore)_certDocStore.clone());
	}

	@Override
	public int compareTo(com.aw.internal.model.CertDocStore certDocStore) {
		return _certDocStore.compareTo(certDocStore);
	}

	@Override
	public int hashCode() {
		return _certDocStore.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.aw.internal.model.CertDocStore> toCacheModel() {
		return _certDocStore.toCacheModel();
	}

	@Override
	public com.aw.internal.model.CertDocStore toEscapedModel() {
		return new CertDocStoreWrapper(_certDocStore.toEscapedModel());
	}

	@Override
	public com.aw.internal.model.CertDocStore toUnescapedModel() {
		return new CertDocStoreWrapper(_certDocStore.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _certDocStore.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _certDocStore.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_certDocStore.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CertDocStoreWrapper)) {
			return false;
		}

		CertDocStoreWrapper certDocStoreWrapper = (CertDocStoreWrapper)obj;

		if (Validator.equals(_certDocStore, certDocStoreWrapper._certDocStore)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public CertDocStore getWrappedCertDocStore() {
		return _certDocStore;
	}

	@Override
	public CertDocStore getWrappedModel() {
		return _certDocStore;
	}

	@Override
	public void resetOriginalValues() {
		_certDocStore.resetOriginalValues();
	}

	private CertDocStore _certDocStore;
}