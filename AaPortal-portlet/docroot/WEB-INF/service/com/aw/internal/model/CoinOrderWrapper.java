/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CoinOrder}.
 * </p>
 *
 * @author zl
 * @see CoinOrder
 * @generated
 */
public class CoinOrderWrapper implements CoinOrder, ModelWrapper<CoinOrder> {
	public CoinOrderWrapper(CoinOrder coinOrder) {
		_coinOrder = coinOrder;
	}

	@Override
	public Class<?> getModelClass() {
		return CoinOrder.class;
	}

	@Override
	public String getModelClassName() {
		return CoinOrder.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("orderId", getOrderId());
		attributes.put("userId", getUserId());
		attributes.put("saleRegion", getSaleRegion());
		attributes.put("brokerId", getBrokerId());
		attributes.put("salesId", getSalesId());
		attributes.put("activeFlag", getActiveFlag());
		attributes.put("salePhase", getSalePhase());
		attributes.put("unitPrice", getUnitPrice());
		attributes.put("totalPrice", getTotalPrice());
		attributes.put("currency", getCurrency());
		attributes.put("saleAmount", getSaleAmount());
		attributes.put("promoAmount", getPromoAmount());
		attributes.put("totalAmount", getTotalAmount());
		attributes.put("rsnCd", getRsnCd());
		attributes.put("verifyFlag", getVerifyFlag());
		attributes.put("docSentFlag", getDocSentFlag());
		attributes.put("docCarrier", getDocCarrier());
		attributes.put("docTrackingNo", getDocTrackingNo());
		attributes.put("docSignFlag", getDocSignFlag());
		attributes.put("paidFlag", getPaidFlag());
		attributes.put("approvalFlag", getApprovalFlag());
		attributes.put("certSentFlag", getCertSentFlag());
		attributes.put("certCarrier", getCertCarrier());
		attributes.put("certTrackingNo", getCertTrackingNo());
		attributes.put("completeFlag", getCompleteFlag());
		attributes.put("caseCloser", getCaseCloser());
		attributes.put("addTime", getAddTime());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String orderId = (String)attributes.get("orderId");

		if (orderId != null) {
			setOrderId(orderId);
		}

		String userId = (String)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String saleRegion = (String)attributes.get("saleRegion");

		if (saleRegion != null) {
			setSaleRegion(saleRegion);
		}

		Long brokerId = (Long)attributes.get("brokerId");

		if (brokerId != null) {
			setBrokerId(brokerId);
		}

		Long salesId = (Long)attributes.get("salesId");

		if (salesId != null) {
			setSalesId(salesId);
		}

		String activeFlag = (String)attributes.get("activeFlag");

		if (activeFlag != null) {
			setActiveFlag(activeFlag);
		}

		Integer salePhase = (Integer)attributes.get("salePhase");

		if (salePhase != null) {
			setSalePhase(salePhase);
		}

		Double unitPrice = (Double)attributes.get("unitPrice");

		if (unitPrice != null) {
			setUnitPrice(unitPrice);
		}

		Double totalPrice = (Double)attributes.get("totalPrice");

		if (totalPrice != null) {
			setTotalPrice(totalPrice);
		}

		String currency = (String)attributes.get("currency");

		if (currency != null) {
			setCurrency(currency);
		}

		Long saleAmount = (Long)attributes.get("saleAmount");

		if (saleAmount != null) {
			setSaleAmount(saleAmount);
		}

		Long promoAmount = (Long)attributes.get("promoAmount");

		if (promoAmount != null) {
			setPromoAmount(promoAmount);
		}

		Long totalAmount = (Long)attributes.get("totalAmount");

		if (totalAmount != null) {
			setTotalAmount(totalAmount);
		}

		String rsnCd = (String)attributes.get("rsnCd");

		if (rsnCd != null) {
			setRsnCd(rsnCd);
		}

		String verifyFlag = (String)attributes.get("verifyFlag");

		if (verifyFlag != null) {
			setVerifyFlag(verifyFlag);
		}

		String docSentFlag = (String)attributes.get("docSentFlag");

		if (docSentFlag != null) {
			setDocSentFlag(docSentFlag);
		}

		String docCarrier = (String)attributes.get("docCarrier");

		if (docCarrier != null) {
			setDocCarrier(docCarrier);
		}

		String docTrackingNo = (String)attributes.get("docTrackingNo");

		if (docTrackingNo != null) {
			setDocTrackingNo(docTrackingNo);
		}

		String docSignFlag = (String)attributes.get("docSignFlag");

		if (docSignFlag != null) {
			setDocSignFlag(docSignFlag);
		}

		String paidFlag = (String)attributes.get("paidFlag");

		if (paidFlag != null) {
			setPaidFlag(paidFlag);
		}

		String approvalFlag = (String)attributes.get("approvalFlag");

		if (approvalFlag != null) {
			setApprovalFlag(approvalFlag);
		}

		String certSentFlag = (String)attributes.get("certSentFlag");

		if (certSentFlag != null) {
			setCertSentFlag(certSentFlag);
		}

		String certCarrier = (String)attributes.get("certCarrier");

		if (certCarrier != null) {
			setCertCarrier(certCarrier);
		}

		String certTrackingNo = (String)attributes.get("certTrackingNo");

		if (certTrackingNo != null) {
			setCertTrackingNo(certTrackingNo);
		}

		String completeFlag = (String)attributes.get("completeFlag");

		if (completeFlag != null) {
			setCompleteFlag(completeFlag);
		}

		Long caseCloser = (Long)attributes.get("caseCloser");

		if (caseCloser != null) {
			setCaseCloser(caseCloser);
		}

		Date addTime = (Date)attributes.get("addTime");

		if (addTime != null) {
			setAddTime(addTime);
		}
	}

	/**
	* Returns the primary key of this coin order.
	*
	* @return the primary key of this coin order
	*/
	@Override
	public java.lang.String getPrimaryKey() {
		return _coinOrder.getPrimaryKey();
	}

	/**
	* Sets the primary key of this coin order.
	*
	* @param primaryKey the primary key of this coin order
	*/
	@Override
	public void setPrimaryKey(java.lang.String primaryKey) {
		_coinOrder.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the order ID of this coin order.
	*
	* @return the order ID of this coin order
	*/
	@Override
	public java.lang.String getOrderId() {
		return _coinOrder.getOrderId();
	}

	/**
	* Sets the order ID of this coin order.
	*
	* @param orderId the order ID of this coin order
	*/
	@Override
	public void setOrderId(java.lang.String orderId) {
		_coinOrder.setOrderId(orderId);
	}

	/**
	* Returns the user ID of this coin order.
	*
	* @return the user ID of this coin order
	*/
	@Override
	public java.lang.String getUserId() {
		return _coinOrder.getUserId();
	}

	/**
	* Sets the user ID of this coin order.
	*
	* @param userId the user ID of this coin order
	*/
	@Override
	public void setUserId(java.lang.String userId) {
		_coinOrder.setUserId(userId);
	}

	/**
	* Returns the sale region of this coin order.
	*
	* @return the sale region of this coin order
	*/
	@Override
	public java.lang.String getSaleRegion() {
		return _coinOrder.getSaleRegion();
	}

	/**
	* Sets the sale region of this coin order.
	*
	* @param saleRegion the sale region of this coin order
	*/
	@Override
	public void setSaleRegion(java.lang.String saleRegion) {
		_coinOrder.setSaleRegion(saleRegion);
	}

	/**
	* Returns the broker ID of this coin order.
	*
	* @return the broker ID of this coin order
	*/
	@Override
	public long getBrokerId() {
		return _coinOrder.getBrokerId();
	}

	/**
	* Sets the broker ID of this coin order.
	*
	* @param brokerId the broker ID of this coin order
	*/
	@Override
	public void setBrokerId(long brokerId) {
		_coinOrder.setBrokerId(brokerId);
	}

	/**
	* Returns the sales ID of this coin order.
	*
	* @return the sales ID of this coin order
	*/
	@Override
	public long getSalesId() {
		return _coinOrder.getSalesId();
	}

	/**
	* Sets the sales ID of this coin order.
	*
	* @param salesId the sales ID of this coin order
	*/
	@Override
	public void setSalesId(long salesId) {
		_coinOrder.setSalesId(salesId);
	}

	/**
	* Returns the active flag of this coin order.
	*
	* @return the active flag of this coin order
	*/
	@Override
	public java.lang.String getActiveFlag() {
		return _coinOrder.getActiveFlag();
	}

	/**
	* Sets the active flag of this coin order.
	*
	* @param activeFlag the active flag of this coin order
	*/
	@Override
	public void setActiveFlag(java.lang.String activeFlag) {
		_coinOrder.setActiveFlag(activeFlag);
	}

	/**
	* Returns the sale phase of this coin order.
	*
	* @return the sale phase of this coin order
	*/
	@Override
	public int getSalePhase() {
		return _coinOrder.getSalePhase();
	}

	/**
	* Sets the sale phase of this coin order.
	*
	* @param salePhase the sale phase of this coin order
	*/
	@Override
	public void setSalePhase(int salePhase) {
		_coinOrder.setSalePhase(salePhase);
	}

	/**
	* Returns the unit price of this coin order.
	*
	* @return the unit price of this coin order
	*/
	@Override
	public double getUnitPrice() {
		return _coinOrder.getUnitPrice();
	}

	/**
	* Sets the unit price of this coin order.
	*
	* @param unitPrice the unit price of this coin order
	*/
	@Override
	public void setUnitPrice(double unitPrice) {
		_coinOrder.setUnitPrice(unitPrice);
	}

	/**
	* Returns the total price of this coin order.
	*
	* @return the total price of this coin order
	*/
	@Override
	public double getTotalPrice() {
		return _coinOrder.getTotalPrice();
	}

	/**
	* Sets the total price of this coin order.
	*
	* @param totalPrice the total price of this coin order
	*/
	@Override
	public void setTotalPrice(double totalPrice) {
		_coinOrder.setTotalPrice(totalPrice);
	}

	/**
	* Returns the currency of this coin order.
	*
	* @return the currency of this coin order
	*/
	@Override
	public java.lang.String getCurrency() {
		return _coinOrder.getCurrency();
	}

	/**
	* Sets the currency of this coin order.
	*
	* @param currency the currency of this coin order
	*/
	@Override
	public void setCurrency(java.lang.String currency) {
		_coinOrder.setCurrency(currency);
	}

	/**
	* Returns the sale amount of this coin order.
	*
	* @return the sale amount of this coin order
	*/
	@Override
	public long getSaleAmount() {
		return _coinOrder.getSaleAmount();
	}

	/**
	* Sets the sale amount of this coin order.
	*
	* @param saleAmount the sale amount of this coin order
	*/
	@Override
	public void setSaleAmount(long saleAmount) {
		_coinOrder.setSaleAmount(saleAmount);
	}

	/**
	* Returns the promo amount of this coin order.
	*
	* @return the promo amount of this coin order
	*/
	@Override
	public long getPromoAmount() {
		return _coinOrder.getPromoAmount();
	}

	/**
	* Sets the promo amount of this coin order.
	*
	* @param promoAmount the promo amount of this coin order
	*/
	@Override
	public void setPromoAmount(long promoAmount) {
		_coinOrder.setPromoAmount(promoAmount);
	}

	/**
	* Returns the total amount of this coin order.
	*
	* @return the total amount of this coin order
	*/
	@Override
	public long getTotalAmount() {
		return _coinOrder.getTotalAmount();
	}

	/**
	* Sets the total amount of this coin order.
	*
	* @param totalAmount the total amount of this coin order
	*/
	@Override
	public void setTotalAmount(long totalAmount) {
		_coinOrder.setTotalAmount(totalAmount);
	}

	/**
	* Returns the rsn cd of this coin order.
	*
	* @return the rsn cd of this coin order
	*/
	@Override
	public java.lang.String getRsnCd() {
		return _coinOrder.getRsnCd();
	}

	/**
	* Sets the rsn cd of this coin order.
	*
	* @param rsnCd the rsn cd of this coin order
	*/
	@Override
	public void setRsnCd(java.lang.String rsnCd) {
		_coinOrder.setRsnCd(rsnCd);
	}

	/**
	* Returns the verify flag of this coin order.
	*
	* @return the verify flag of this coin order
	*/
	@Override
	public java.lang.String getVerifyFlag() {
		return _coinOrder.getVerifyFlag();
	}

	/**
	* Sets the verify flag of this coin order.
	*
	* @param verifyFlag the verify flag of this coin order
	*/
	@Override
	public void setVerifyFlag(java.lang.String verifyFlag) {
		_coinOrder.setVerifyFlag(verifyFlag);
	}

	/**
	* Returns the doc sent flag of this coin order.
	*
	* @return the doc sent flag of this coin order
	*/
	@Override
	public java.lang.String getDocSentFlag() {
		return _coinOrder.getDocSentFlag();
	}

	/**
	* Sets the doc sent flag of this coin order.
	*
	* @param docSentFlag the doc sent flag of this coin order
	*/
	@Override
	public void setDocSentFlag(java.lang.String docSentFlag) {
		_coinOrder.setDocSentFlag(docSentFlag);
	}

	/**
	* Returns the doc carrier of this coin order.
	*
	* @return the doc carrier of this coin order
	*/
	@Override
	public java.lang.String getDocCarrier() {
		return _coinOrder.getDocCarrier();
	}

	/**
	* Sets the doc carrier of this coin order.
	*
	* @param docCarrier the doc carrier of this coin order
	*/
	@Override
	public void setDocCarrier(java.lang.String docCarrier) {
		_coinOrder.setDocCarrier(docCarrier);
	}

	/**
	* Returns the doc tracking no of this coin order.
	*
	* @return the doc tracking no of this coin order
	*/
	@Override
	public java.lang.String getDocTrackingNo() {
		return _coinOrder.getDocTrackingNo();
	}

	/**
	* Sets the doc tracking no of this coin order.
	*
	* @param docTrackingNo the doc tracking no of this coin order
	*/
	@Override
	public void setDocTrackingNo(java.lang.String docTrackingNo) {
		_coinOrder.setDocTrackingNo(docTrackingNo);
	}

	/**
	* Returns the doc sign flag of this coin order.
	*
	* @return the doc sign flag of this coin order
	*/
	@Override
	public java.lang.String getDocSignFlag() {
		return _coinOrder.getDocSignFlag();
	}

	/**
	* Sets the doc sign flag of this coin order.
	*
	* @param docSignFlag the doc sign flag of this coin order
	*/
	@Override
	public void setDocSignFlag(java.lang.String docSignFlag) {
		_coinOrder.setDocSignFlag(docSignFlag);
	}

	/**
	* Returns the paid flag of this coin order.
	*
	* @return the paid flag of this coin order
	*/
	@Override
	public java.lang.String getPaidFlag() {
		return _coinOrder.getPaidFlag();
	}

	/**
	* Sets the paid flag of this coin order.
	*
	* @param paidFlag the paid flag of this coin order
	*/
	@Override
	public void setPaidFlag(java.lang.String paidFlag) {
		_coinOrder.setPaidFlag(paidFlag);
	}

	/**
	* Returns the approval flag of this coin order.
	*
	* @return the approval flag of this coin order
	*/
	@Override
	public java.lang.String getApprovalFlag() {
		return _coinOrder.getApprovalFlag();
	}

	/**
	* Sets the approval flag of this coin order.
	*
	* @param approvalFlag the approval flag of this coin order
	*/
	@Override
	public void setApprovalFlag(java.lang.String approvalFlag) {
		_coinOrder.setApprovalFlag(approvalFlag);
	}

	/**
	* Returns the cert sent flag of this coin order.
	*
	* @return the cert sent flag of this coin order
	*/
	@Override
	public java.lang.String getCertSentFlag() {
		return _coinOrder.getCertSentFlag();
	}

	/**
	* Sets the cert sent flag of this coin order.
	*
	* @param certSentFlag the cert sent flag of this coin order
	*/
	@Override
	public void setCertSentFlag(java.lang.String certSentFlag) {
		_coinOrder.setCertSentFlag(certSentFlag);
	}

	/**
	* Returns the cert carrier of this coin order.
	*
	* @return the cert carrier of this coin order
	*/
	@Override
	public java.lang.String getCertCarrier() {
		return _coinOrder.getCertCarrier();
	}

	/**
	* Sets the cert carrier of this coin order.
	*
	* @param certCarrier the cert carrier of this coin order
	*/
	@Override
	public void setCertCarrier(java.lang.String certCarrier) {
		_coinOrder.setCertCarrier(certCarrier);
	}

	/**
	* Returns the cert tracking no of this coin order.
	*
	* @return the cert tracking no of this coin order
	*/
	@Override
	public java.lang.String getCertTrackingNo() {
		return _coinOrder.getCertTrackingNo();
	}

	/**
	* Sets the cert tracking no of this coin order.
	*
	* @param certTrackingNo the cert tracking no of this coin order
	*/
	@Override
	public void setCertTrackingNo(java.lang.String certTrackingNo) {
		_coinOrder.setCertTrackingNo(certTrackingNo);
	}

	/**
	* Returns the complete flag of this coin order.
	*
	* @return the complete flag of this coin order
	*/
	@Override
	public java.lang.String getCompleteFlag() {
		return _coinOrder.getCompleteFlag();
	}

	/**
	* Sets the complete flag of this coin order.
	*
	* @param completeFlag the complete flag of this coin order
	*/
	@Override
	public void setCompleteFlag(java.lang.String completeFlag) {
		_coinOrder.setCompleteFlag(completeFlag);
	}

	/**
	* Returns the case closer of this coin order.
	*
	* @return the case closer of this coin order
	*/
	@Override
	public long getCaseCloser() {
		return _coinOrder.getCaseCloser();
	}

	/**
	* Sets the case closer of this coin order.
	*
	* @param caseCloser the case closer of this coin order
	*/
	@Override
	public void setCaseCloser(long caseCloser) {
		_coinOrder.setCaseCloser(caseCloser);
	}

	/**
	* Returns the add time of this coin order.
	*
	* @return the add time of this coin order
	*/
	@Override
	public java.util.Date getAddTime() {
		return _coinOrder.getAddTime();
	}

	/**
	* Sets the add time of this coin order.
	*
	* @param addTime the add time of this coin order
	*/
	@Override
	public void setAddTime(java.util.Date addTime) {
		_coinOrder.setAddTime(addTime);
	}

	@Override
	public boolean isNew() {
		return _coinOrder.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_coinOrder.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _coinOrder.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_coinOrder.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _coinOrder.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _coinOrder.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_coinOrder.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _coinOrder.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_coinOrder.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_coinOrder.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_coinOrder.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new CoinOrderWrapper((CoinOrder)_coinOrder.clone());
	}

	@Override
	public int compareTo(com.aw.internal.model.CoinOrder coinOrder) {
		return _coinOrder.compareTo(coinOrder);
	}

	@Override
	public int hashCode() {
		return _coinOrder.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.aw.internal.model.CoinOrder> toCacheModel() {
		return _coinOrder.toCacheModel();
	}

	@Override
	public com.aw.internal.model.CoinOrder toEscapedModel() {
		return new CoinOrderWrapper(_coinOrder.toEscapedModel());
	}

	@Override
	public com.aw.internal.model.CoinOrder toUnescapedModel() {
		return new CoinOrderWrapper(_coinOrder.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _coinOrder.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _coinOrder.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_coinOrder.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CoinOrderWrapper)) {
			return false;
		}

		CoinOrderWrapper coinOrderWrapper = (CoinOrderWrapper)obj;

		if (Validator.equals(_coinOrder, coinOrderWrapper._coinOrder)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public CoinOrder getWrappedCoinOrder() {
		return _coinOrder;
	}

	@Override
	public CoinOrder getWrappedModel() {
		return _coinOrder;
	}

	@Override
	public void resetOriginalValues() {
		_coinOrder.resetOriginalValues();
	}

	private CoinOrder _coinOrder;
}