/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model;

import com.aw.internal.service.ActivePhaseLocalServiceUtil;
import com.aw.internal.service.ClpSerializer;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zl
 */
public class ActivePhaseClp extends BaseModelImpl<ActivePhase>
	implements ActivePhase {
	public ActivePhaseClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return ActivePhase.class;
	}

	@Override
	public String getModelClassName() {
		return ActivePhase.class.getName();
	}

	@Override
	public String getPrimaryKey() {
		return _region;
	}

	@Override
	public void setPrimaryKey(String primaryKey) {
		setRegion(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _region;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((String)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("region", getRegion());
		attributes.put("salePhase", getSalePhase());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String region = (String)attributes.get("region");

		if (region != null) {
			setRegion(region);
		}

		Integer salePhase = (Integer)attributes.get("salePhase");

		if (salePhase != null) {
			setSalePhase(salePhase);
		}
	}

	@Override
	public String getRegion() {
		return _region;
	}

	@Override
	public void setRegion(String region) {
		_region = region;

		if (_activePhaseRemoteModel != null) {
			try {
				Class<?> clazz = _activePhaseRemoteModel.getClass();

				Method method = clazz.getMethod("setRegion", String.class);

				method.invoke(_activePhaseRemoteModel, region);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getSalePhase() {
		return _salePhase;
	}

	@Override
	public void setSalePhase(int salePhase) {
		_salePhase = salePhase;

		if (_activePhaseRemoteModel != null) {
			try {
				Class<?> clazz = _activePhaseRemoteModel.getClass();

				Method method = clazz.getMethod("setSalePhase", int.class);

				method.invoke(_activePhaseRemoteModel, salePhase);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getActivePhaseRemoteModel() {
		return _activePhaseRemoteModel;
	}

	public void setActivePhaseRemoteModel(BaseModel<?> activePhaseRemoteModel) {
		_activePhaseRemoteModel = activePhaseRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _activePhaseRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_activePhaseRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			ActivePhaseLocalServiceUtil.addActivePhase(this);
		}
		else {
			ActivePhaseLocalServiceUtil.updateActivePhase(this);
		}
	}

	@Override
	public ActivePhase toEscapedModel() {
		return (ActivePhase)ProxyUtil.newProxyInstance(ActivePhase.class.getClassLoader(),
			new Class[] { ActivePhase.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		ActivePhaseClp clone = new ActivePhaseClp();

		clone.setRegion(getRegion());
		clone.setSalePhase(getSalePhase());

		return clone;
	}

	@Override
	public int compareTo(ActivePhase activePhase) {
		String primaryKey = activePhase.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ActivePhaseClp)) {
			return false;
		}

		ActivePhaseClp activePhase = (ActivePhaseClp)obj;

		String primaryKey = activePhase.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{region=");
		sb.append(getRegion());
		sb.append(", salePhase=");
		sb.append(getSalePhase());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(10);

		sb.append("<model><model-name>");
		sb.append("com.aw.internal.model.ActivePhase");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>region</column-name><column-value><![CDATA[");
		sb.append(getRegion());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>salePhase</column-name><column-value><![CDATA[");
		sb.append(getSalePhase());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private String _region;
	private int _salePhase;
	private BaseModel<?> _activePhaseRemoteModel;
	private Class<?> _clpSerializerClass = com.aw.internal.service.ClpSerializer.class;
}