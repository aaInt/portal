/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model;

import com.aw.internal.service.ClpSerializer;
import com.aw.internal.service.ImageStoreLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.sql.Blob;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author zl
 */
public class ImageStoreClp extends BaseModelImpl<ImageStore>
	implements ImageStore {
	public ImageStoreClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return ImageStore.class;
	}

	@Override
	public String getModelClassName() {
		return ImageStore.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _imageId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setImageId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _imageId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("imageId", getImageId());
		attributes.put("content", getContent());
		attributes.put("activeFlag", getActiveFlag());
		attributes.put("addDate", getAddDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long imageId = (Long)attributes.get("imageId");

		if (imageId != null) {
			setImageId(imageId);
		}

		Blob content = (Blob)attributes.get("content");

		if (content != null) {
			setContent(content);
		}

		String activeFlag = (String)attributes.get("activeFlag");

		if (activeFlag != null) {
			setActiveFlag(activeFlag);
		}

		Date addDate = (Date)attributes.get("addDate");

		if (addDate != null) {
			setAddDate(addDate);
		}
	}

	@Override
	public long getImageId() {
		return _imageId;
	}

	@Override
	public void setImageId(long imageId) {
		_imageId = imageId;

		if (_imageStoreRemoteModel != null) {
			try {
				Class<?> clazz = _imageStoreRemoteModel.getClass();

				Method method = clazz.getMethod("setImageId", long.class);

				method.invoke(_imageStoreRemoteModel, imageId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Blob getContent() {
		return _content;
	}

	@Override
	public void setContent(Blob content) {
		_content = content;

		if (_imageStoreRemoteModel != null) {
			try {
				Class<?> clazz = _imageStoreRemoteModel.getClass();

				Method method = clazz.getMethod("setContent", Blob.class);

				method.invoke(_imageStoreRemoteModel, content);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getActiveFlag() {
		return _activeFlag;
	}

	@Override
	public void setActiveFlag(String activeFlag) {
		_activeFlag = activeFlag;

		if (_imageStoreRemoteModel != null) {
			try {
				Class<?> clazz = _imageStoreRemoteModel.getClass();

				Method method = clazz.getMethod("setActiveFlag", String.class);

				method.invoke(_imageStoreRemoteModel, activeFlag);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getAddDate() {
		return _addDate;
	}

	@Override
	public void setAddDate(Date addDate) {
		_addDate = addDate;

		if (_imageStoreRemoteModel != null) {
			try {
				Class<?> clazz = _imageStoreRemoteModel.getClass();

				Method method = clazz.getMethod("setAddDate", Date.class);

				method.invoke(_imageStoreRemoteModel, addDate);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getImageStoreRemoteModel() {
		return _imageStoreRemoteModel;
	}

	public void setImageStoreRemoteModel(BaseModel<?> imageStoreRemoteModel) {
		_imageStoreRemoteModel = imageStoreRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _imageStoreRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_imageStoreRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			ImageStoreLocalServiceUtil.addImageStore(this);
		}
		else {
			ImageStoreLocalServiceUtil.updateImageStore(this);
		}
	}

	@Override
	public ImageStore toEscapedModel() {
		return (ImageStore)ProxyUtil.newProxyInstance(ImageStore.class.getClassLoader(),
			new Class[] { ImageStore.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		ImageStoreClp clone = new ImageStoreClp();

		clone.setImageId(getImageId());
		clone.setContent(getContent());
		clone.setActiveFlag(getActiveFlag());
		clone.setAddDate(getAddDate());

		return clone;
	}

	@Override
	public int compareTo(ImageStore imageStore) {
		long primaryKey = imageStore.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ImageStoreClp)) {
			return false;
		}

		ImageStoreClp imageStore = (ImageStoreClp)obj;

		long primaryKey = imageStore.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{imageId=");
		sb.append(getImageId());
		sb.append(", content=");
		sb.append(getContent());
		sb.append(", activeFlag=");
		sb.append(getActiveFlag());
		sb.append(", addDate=");
		sb.append(getAddDate());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(16);

		sb.append("<model><model-name>");
		sb.append("com.aw.internal.model.ImageStore");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>imageId</column-name><column-value><![CDATA[");
		sb.append(getImageId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>content</column-name><column-value><![CDATA[");
		sb.append(getContent());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>activeFlag</column-name><column-value><![CDATA[");
		sb.append(getActiveFlag());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>addDate</column-name><column-value><![CDATA[");
		sb.append(getAddDate());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _imageId;
	private Blob _content;
	private String _activeFlag;
	private Date _addDate;
	private BaseModel<?> _imageStoreRemoteModel;
	private Class<?> _clpSerializerClass = com.aw.internal.service.ClpSerializer.class;
}