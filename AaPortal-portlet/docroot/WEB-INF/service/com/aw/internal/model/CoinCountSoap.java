/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author zl
 * @generated
 */
public class CoinCountSoap implements Serializable {
	public static CoinCountSoap toSoapModel(CoinCount model) {
		CoinCountSoap soapModel = new CoinCountSoap();

		soapModel.setPhase(model.getPhase());
		soapModel.setTotalAmount(model.getTotalAmount());
		soapModel.setEndFlag(model.getEndFlag());
		soapModel.setRollToPhase(model.getRollToPhase());
		soapModel.setRollToAmt(model.getRollToAmt());
		soapModel.setLimit(model.getLimit());
		soapModel.setRollOverAmt(model.getRollOverAmt());
		soapModel.setLastOrderId(model.getLastOrderId());

		return soapModel;
	}

	public static CoinCountSoap[] toSoapModels(CoinCount[] models) {
		CoinCountSoap[] soapModels = new CoinCountSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static CoinCountSoap[][] toSoapModels(CoinCount[][] models) {
		CoinCountSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new CoinCountSoap[models.length][models[0].length];
		}
		else {
			soapModels = new CoinCountSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static CoinCountSoap[] toSoapModels(List<CoinCount> models) {
		List<CoinCountSoap> soapModels = new ArrayList<CoinCountSoap>(models.size());

		for (CoinCount model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new CoinCountSoap[soapModels.size()]);
	}

	public CoinCountSoap() {
	}

	public int getPrimaryKey() {
		return _phase;
	}

	public void setPrimaryKey(int pk) {
		setPhase(pk);
	}

	public int getPhase() {
		return _phase;
	}

	public void setPhase(int phase) {
		_phase = phase;
	}

	public long getTotalAmount() {
		return _totalAmount;
	}

	public void setTotalAmount(long totalAmount) {
		_totalAmount = totalAmount;
	}

	public String getEndFlag() {
		return _endFlag;
	}

	public void setEndFlag(String endFlag) {
		_endFlag = endFlag;
	}

	public int getRollToPhase() {
		return _rollToPhase;
	}

	public void setRollToPhase(int rollToPhase) {
		_rollToPhase = rollToPhase;
	}

	public long getRollToAmt() {
		return _rollToAmt;
	}

	public void setRollToAmt(long rollToAmt) {
		_rollToAmt = rollToAmt;
	}

	public long getLimit() {
		return _limit;
	}

	public void setLimit(long limit) {
		_limit = limit;
	}

	public long getRollOverAmt() {
		return _rollOverAmt;
	}

	public void setRollOverAmt(long rollOverAmt) {
		_rollOverAmt = rollOverAmt;
	}

	public String getLastOrderId() {
		return _lastOrderId;
	}

	public void setLastOrderId(String lastOrderId) {
		_lastOrderId = lastOrderId;
	}

	private int _phase;
	private long _totalAmount;
	private String _endFlag;
	private int _rollToPhase;
	private long _rollToAmt;
	private long _limit;
	private long _rollOverAmt;
	private String _lastOrderId;
}