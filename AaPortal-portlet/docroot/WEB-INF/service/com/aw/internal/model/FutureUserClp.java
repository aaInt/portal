/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model;

import com.aw.internal.service.ClpSerializer;
import com.aw.internal.service.FutureUserLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author zl
 */
public class FutureUserClp extends BaseModelImpl<FutureUser>
	implements FutureUser {
	public FutureUserClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return FutureUser.class;
	}

	@Override
	public String getModelClassName() {
		return FutureUser.class.getName();
	}

	@Override
	public String getPrimaryKey() {
		return _email;
	}

	@Override
	public void setPrimaryKey(String primaryKey) {
		setEmail(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _email;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((String)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("email", getEmail());
		attributes.put("firstName", getFirstName());
		attributes.put("lastName", getLastName());
		attributes.put("residency", getResidency());
		attributes.put("phone", getPhone());
		attributes.put("areaCode", getAreaCode());
		attributes.put("activeFlag", getActiveFlag());
		attributes.put("takenFlag", getTakenFlag());
		attributes.put("ownerId", getOwnerId());
		attributes.put("addTime", getAddTime());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String email = (String)attributes.get("email");

		if (email != null) {
			setEmail(email);
		}

		String firstName = (String)attributes.get("firstName");

		if (firstName != null) {
			setFirstName(firstName);
		}

		String lastName = (String)attributes.get("lastName");

		if (lastName != null) {
			setLastName(lastName);
		}

		String residency = (String)attributes.get("residency");

		if (residency != null) {
			setResidency(residency);
		}

		Long phone = (Long)attributes.get("phone");

		if (phone != null) {
			setPhone(phone);
		}

		Integer areaCode = (Integer)attributes.get("areaCode");

		if (areaCode != null) {
			setAreaCode(areaCode);
		}

		String activeFlag = (String)attributes.get("activeFlag");

		if (activeFlag != null) {
			setActiveFlag(activeFlag);
		}

		String takenFlag = (String)attributes.get("takenFlag");

		if (takenFlag != null) {
			setTakenFlag(takenFlag);
		}

		Long ownerId = (Long)attributes.get("ownerId");

		if (ownerId != null) {
			setOwnerId(ownerId);
		}

		Date addTime = (Date)attributes.get("addTime");

		if (addTime != null) {
			setAddTime(addTime);
		}
	}

	@Override
	public String getEmail() {
		return _email;
	}

	@Override
	public void setEmail(String email) {
		_email = email;

		if (_futureUserRemoteModel != null) {
			try {
				Class<?> clazz = _futureUserRemoteModel.getClass();

				Method method = clazz.getMethod("setEmail", String.class);

				method.invoke(_futureUserRemoteModel, email);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getFirstName() {
		return _firstName;
	}

	@Override
	public void setFirstName(String firstName) {
		_firstName = firstName;

		if (_futureUserRemoteModel != null) {
			try {
				Class<?> clazz = _futureUserRemoteModel.getClass();

				Method method = clazz.getMethod("setFirstName", String.class);

				method.invoke(_futureUserRemoteModel, firstName);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLastName() {
		return _lastName;
	}

	@Override
	public void setLastName(String lastName) {
		_lastName = lastName;

		if (_futureUserRemoteModel != null) {
			try {
				Class<?> clazz = _futureUserRemoteModel.getClass();

				Method method = clazz.getMethod("setLastName", String.class);

				method.invoke(_futureUserRemoteModel, lastName);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getResidency() {
		return _residency;
	}

	@Override
	public void setResidency(String residency) {
		_residency = residency;

		if (_futureUserRemoteModel != null) {
			try {
				Class<?> clazz = _futureUserRemoteModel.getClass();

				Method method = clazz.getMethod("setResidency", String.class);

				method.invoke(_futureUserRemoteModel, residency);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getPhone() {
		return _phone;
	}

	@Override
	public void setPhone(long phone) {
		_phone = phone;

		if (_futureUserRemoteModel != null) {
			try {
				Class<?> clazz = _futureUserRemoteModel.getClass();

				Method method = clazz.getMethod("setPhone", long.class);

				method.invoke(_futureUserRemoteModel, phone);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getAreaCode() {
		return _areaCode;
	}

	@Override
	public void setAreaCode(int areaCode) {
		_areaCode = areaCode;

		if (_futureUserRemoteModel != null) {
			try {
				Class<?> clazz = _futureUserRemoteModel.getClass();

				Method method = clazz.getMethod("setAreaCode", int.class);

				method.invoke(_futureUserRemoteModel, areaCode);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getActiveFlag() {
		return _activeFlag;
	}

	@Override
	public void setActiveFlag(String activeFlag) {
		_activeFlag = activeFlag;

		if (_futureUserRemoteModel != null) {
			try {
				Class<?> clazz = _futureUserRemoteModel.getClass();

				Method method = clazz.getMethod("setActiveFlag", String.class);

				method.invoke(_futureUserRemoteModel, activeFlag);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTakenFlag() {
		return _takenFlag;
	}

	@Override
	public void setTakenFlag(String takenFlag) {
		_takenFlag = takenFlag;

		if (_futureUserRemoteModel != null) {
			try {
				Class<?> clazz = _futureUserRemoteModel.getClass();

				Method method = clazz.getMethod("setTakenFlag", String.class);

				method.invoke(_futureUserRemoteModel, takenFlag);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getOwnerId() {
		return _ownerId;
	}

	@Override
	public void setOwnerId(long ownerId) {
		_ownerId = ownerId;

		if (_futureUserRemoteModel != null) {
			try {
				Class<?> clazz = _futureUserRemoteModel.getClass();

				Method method = clazz.getMethod("setOwnerId", long.class);

				method.invoke(_futureUserRemoteModel, ownerId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getAddTime() {
		return _addTime;
	}

	@Override
	public void setAddTime(Date addTime) {
		_addTime = addTime;

		if (_futureUserRemoteModel != null) {
			try {
				Class<?> clazz = _futureUserRemoteModel.getClass();

				Method method = clazz.getMethod("setAddTime", Date.class);

				method.invoke(_futureUserRemoteModel, addTime);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getFutureUserRemoteModel() {
		return _futureUserRemoteModel;
	}

	public void setFutureUserRemoteModel(BaseModel<?> futureUserRemoteModel) {
		_futureUserRemoteModel = futureUserRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _futureUserRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_futureUserRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			FutureUserLocalServiceUtil.addFutureUser(this);
		}
		else {
			FutureUserLocalServiceUtil.updateFutureUser(this);
		}
	}

	@Override
	public FutureUser toEscapedModel() {
		return (FutureUser)ProxyUtil.newProxyInstance(FutureUser.class.getClassLoader(),
			new Class[] { FutureUser.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		FutureUserClp clone = new FutureUserClp();

		clone.setEmail(getEmail());
		clone.setFirstName(getFirstName());
		clone.setLastName(getLastName());
		clone.setResidency(getResidency());
		clone.setPhone(getPhone());
		clone.setAreaCode(getAreaCode());
		clone.setActiveFlag(getActiveFlag());
		clone.setTakenFlag(getTakenFlag());
		clone.setOwnerId(getOwnerId());
		clone.setAddTime(getAddTime());

		return clone;
	}

	@Override
	public int compareTo(FutureUser futureUser) {
		int value = 0;

		value = DateUtil.compareTo(getAddTime(), futureUser.getAddTime());

		value = value * -1;

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof FutureUserClp)) {
			return false;
		}

		FutureUserClp futureUser = (FutureUserClp)obj;

		String primaryKey = futureUser.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{email=");
		sb.append(getEmail());
		sb.append(", firstName=");
		sb.append(getFirstName());
		sb.append(", lastName=");
		sb.append(getLastName());
		sb.append(", residency=");
		sb.append(getResidency());
		sb.append(", phone=");
		sb.append(getPhone());
		sb.append(", areaCode=");
		sb.append(getAreaCode());
		sb.append(", activeFlag=");
		sb.append(getActiveFlag());
		sb.append(", takenFlag=");
		sb.append(getTakenFlag());
		sb.append(", ownerId=");
		sb.append(getOwnerId());
		sb.append(", addTime=");
		sb.append(getAddTime());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(34);

		sb.append("<model><model-name>");
		sb.append("com.aw.internal.model.FutureUser");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>email</column-name><column-value><![CDATA[");
		sb.append(getEmail());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>firstName</column-name><column-value><![CDATA[");
		sb.append(getFirstName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>lastName</column-name><column-value><![CDATA[");
		sb.append(getLastName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>residency</column-name><column-value><![CDATA[");
		sb.append(getResidency());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>phone</column-name><column-value><![CDATA[");
		sb.append(getPhone());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>areaCode</column-name><column-value><![CDATA[");
		sb.append(getAreaCode());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>activeFlag</column-name><column-value><![CDATA[");
		sb.append(getActiveFlag());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>takenFlag</column-name><column-value><![CDATA[");
		sb.append(getTakenFlag());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>ownerId</column-name><column-value><![CDATA[");
		sb.append(getOwnerId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>addTime</column-name><column-value><![CDATA[");
		sb.append(getAddTime());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private String _email;
	private String _firstName;
	private String _lastName;
	private String _residency;
	private long _phone;
	private int _areaCode;
	private String _activeFlag;
	private String _takenFlag;
	private long _ownerId;
	private Date _addTime;
	private BaseModel<?> _futureUserRemoteModel;
	private Class<?> _clpSerializerClass = com.aw.internal.service.ClpSerializer.class;
}