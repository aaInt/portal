/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model;

import com.aw.internal.service.CertDocStoreLocalServiceUtil;
import com.aw.internal.service.ClpSerializer;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.sql.Blob;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author zl
 */
public class CertDocStoreClp extends BaseModelImpl<CertDocStore>
	implements CertDocStore {
	public CertDocStoreClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return CertDocStore.class;
	}

	@Override
	public String getModelClassName() {
		return CertDocStore.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _docId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setDocId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _docId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("docId", getDocId());
		attributes.put("content", getContent());
		attributes.put("activeFlag", getActiveFlag());
		attributes.put("addDate", getAddDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long docId = (Long)attributes.get("docId");

		if (docId != null) {
			setDocId(docId);
		}

		Blob content = (Blob)attributes.get("content");

		if (content != null) {
			setContent(content);
		}

		String activeFlag = (String)attributes.get("activeFlag");

		if (activeFlag != null) {
			setActiveFlag(activeFlag);
		}

		Date addDate = (Date)attributes.get("addDate");

		if (addDate != null) {
			setAddDate(addDate);
		}
	}

	@Override
	public long getDocId() {
		return _docId;
	}

	@Override
	public void setDocId(long docId) {
		_docId = docId;

		if (_certDocStoreRemoteModel != null) {
			try {
				Class<?> clazz = _certDocStoreRemoteModel.getClass();

				Method method = clazz.getMethod("setDocId", long.class);

				method.invoke(_certDocStoreRemoteModel, docId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Blob getContent() {
		return _content;
	}

	@Override
	public void setContent(Blob content) {
		_content = content;

		if (_certDocStoreRemoteModel != null) {
			try {
				Class<?> clazz = _certDocStoreRemoteModel.getClass();

				Method method = clazz.getMethod("setContent", Blob.class);

				method.invoke(_certDocStoreRemoteModel, content);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getActiveFlag() {
		return _activeFlag;
	}

	@Override
	public void setActiveFlag(String activeFlag) {
		_activeFlag = activeFlag;

		if (_certDocStoreRemoteModel != null) {
			try {
				Class<?> clazz = _certDocStoreRemoteModel.getClass();

				Method method = clazz.getMethod("setActiveFlag", String.class);

				method.invoke(_certDocStoreRemoteModel, activeFlag);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getAddDate() {
		return _addDate;
	}

	@Override
	public void setAddDate(Date addDate) {
		_addDate = addDate;

		if (_certDocStoreRemoteModel != null) {
			try {
				Class<?> clazz = _certDocStoreRemoteModel.getClass();

				Method method = clazz.getMethod("setAddDate", Date.class);

				method.invoke(_certDocStoreRemoteModel, addDate);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getCertDocStoreRemoteModel() {
		return _certDocStoreRemoteModel;
	}

	public void setCertDocStoreRemoteModel(BaseModel<?> certDocStoreRemoteModel) {
		_certDocStoreRemoteModel = certDocStoreRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _certDocStoreRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_certDocStoreRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			CertDocStoreLocalServiceUtil.addCertDocStore(this);
		}
		else {
			CertDocStoreLocalServiceUtil.updateCertDocStore(this);
		}
	}

	@Override
	public CertDocStore toEscapedModel() {
		return (CertDocStore)ProxyUtil.newProxyInstance(CertDocStore.class.getClassLoader(),
			new Class[] { CertDocStore.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		CertDocStoreClp clone = new CertDocStoreClp();

		clone.setDocId(getDocId());
		clone.setContent(getContent());
		clone.setActiveFlag(getActiveFlag());
		clone.setAddDate(getAddDate());

		return clone;
	}

	@Override
	public int compareTo(CertDocStore certDocStore) {
		long primaryKey = certDocStore.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CertDocStoreClp)) {
			return false;
		}

		CertDocStoreClp certDocStore = (CertDocStoreClp)obj;

		long primaryKey = certDocStore.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{docId=");
		sb.append(getDocId());
		sb.append(", content=");
		sb.append(getContent());
		sb.append(", activeFlag=");
		sb.append(getActiveFlag());
		sb.append(", addDate=");
		sb.append(getAddDate());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(16);

		sb.append("<model><model-name>");
		sb.append("com.aw.internal.model.CertDocStore");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>docId</column-name><column-value><![CDATA[");
		sb.append(getDocId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>content</column-name><column-value><![CDATA[");
		sb.append(getContent());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>activeFlag</column-name><column-value><![CDATA[");
		sb.append(getActiveFlag());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>addDate</column-name><column-value><![CDATA[");
		sb.append(getAddDate());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _docId;
	private Blob _content;
	private String _activeFlag;
	private Date _addDate;
	private BaseModel<?> _certDocStoreRemoteModel;
	private Class<?> _clpSerializerClass = com.aw.internal.service.ClpSerializer.class;
}