/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for CoinUser. This utility wraps
 * {@link com.aw.internal.service.impl.CoinUserLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author zl
 * @see CoinUserLocalService
 * @see com.aw.internal.service.base.CoinUserLocalServiceBaseImpl
 * @see com.aw.internal.service.impl.CoinUserLocalServiceImpl
 * @generated
 */
public class CoinUserLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.aw.internal.service.impl.CoinUserLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the coin user to the database. Also notifies the appropriate model listeners.
	*
	* @param coinUser the coin user
	* @return the coin user that was added
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.CoinUser addCoinUser(
		com.aw.internal.model.CoinUser coinUser)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addCoinUser(coinUser);
	}

	/**
	* Creates a new coin user with the primary key. Does not add the coin user to the database.
	*
	* @param userId the primary key for the new coin user
	* @return the new coin user
	*/
	public static com.aw.internal.model.CoinUser createCoinUser(
		java.lang.String userId) {
		return getService().createCoinUser(userId);
	}

	/**
	* Deletes the coin user with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userId the primary key of the coin user
	* @return the coin user that was removed
	* @throws PortalException if a coin user with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.CoinUser deleteCoinUser(
		java.lang.String userId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteCoinUser(userId);
	}

	/**
	* Deletes the coin user from the database. Also notifies the appropriate model listeners.
	*
	* @param coinUser the coin user
	* @return the coin user that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.CoinUser deleteCoinUser(
		com.aw.internal.model.CoinUser coinUser)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteCoinUser(coinUser);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static com.aw.internal.model.CoinUser fetchCoinUser(
		java.lang.String userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchCoinUser(userId);
	}

	/**
	* Returns the coin user with the primary key.
	*
	* @param userId the primary key of the coin user
	* @return the coin user
	* @throws PortalException if a coin user with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.CoinUser getCoinUser(
		java.lang.String userId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getCoinUser(userId);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the coin users.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of coin users
	* @param end the upper bound of the range of coin users (not inclusive)
	* @return the range of coin users
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.aw.internal.model.CoinUser> getCoinUsers(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getCoinUsers(start, end);
	}

	/**
	* Returns the number of coin users.
	*
	* @return the number of coin users
	* @throws SystemException if a system exception occurred
	*/
	public static int getCoinUsersCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getCoinUsersCount();
	}

	/**
	* Updates the coin user in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param coinUser the coin user
	* @return the coin user that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.CoinUser updateCoinUser(
		com.aw.internal.model.CoinUser coinUser)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateCoinUser(coinUser);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static com.liferay.portal.kernel.json.JSONObject addCoinUser(
		long creater, java.lang.String firstName, java.lang.String lastName,
		java.lang.String birthday, java.lang.String userType,
		java.lang.String brkLics, java.lang.String brkCountry, long fundId,
		java.lang.String nationality, java.lang.String idIssueCountry,
		java.lang.String idType, java.lang.String idNumber,
		java.lang.String photoImage, java.lang.String certDocStr,
		java.lang.String ssn, java.lang.String ein, java.lang.String income,
		java.lang.String asset, long phone, int areaCode,
		java.lang.String email, java.lang.String wechat,
		java.lang.String facebook, java.lang.String twitter,
		java.lang.String weibo, java.lang.String street1,
		java.lang.String street2, java.lang.String city,
		java.lang.String state, java.lang.String country,
		java.lang.String zipCode, java.lang.String isMailDiff,
		java.lang.String mailStreet1, java.lang.String mailStreet2,
		java.lang.String mailCity, java.lang.String mailState,
		java.lang.String mailCountry, java.lang.String mailZipCode) {
		return getService()
				   .addCoinUser(creater, firstName, lastName, birthday,
			userType, brkLics, brkCountry, fundId, nationality, idIssueCountry,
			idType, idNumber, photoImage, certDocStr, ssn, ein, income, asset,
			phone, areaCode, email, wechat, facebook, twitter, weibo, street1,
			street2, city, state, country, zipCode, isMailDiff, mailStreet1,
			mailStreet2, mailCity, mailState, mailCountry, mailZipCode);
	}

	public static com.liferay.portal.kernel.json.JSONObject updateCoinUser(
		long userId, java.lang.String coinUserId, java.lang.String firstName,
		java.lang.String lastName, java.lang.String birthday,
		java.lang.String userType, java.lang.String brkLics,
		java.lang.String brkCountry, long fundId, java.lang.String nationality,
		java.lang.String idIssueCountry, java.lang.String idType,
		java.lang.String idNumber, java.lang.String ssn, java.lang.String ein,
		java.lang.String income, java.lang.String asset, long phone,
		int areaCode, java.lang.String email, java.lang.String wechat,
		java.lang.String facebook, java.lang.String twitter,
		java.lang.String weibo, java.lang.String street1,
		java.lang.String street2, java.lang.String city,
		java.lang.String state, java.lang.String country,
		java.lang.String zipCode, java.lang.String isMailDiff,
		java.lang.String mailStreet1, java.lang.String mailStreet2,
		java.lang.String mailCity, java.lang.String mailState,
		java.lang.String mailCountry, java.lang.String mailZipCode) {
		return getService()
				   .updateCoinUser(userId, coinUserId, firstName, lastName,
			birthday, userType, brkLics, brkCountry, fundId, nationality,
			idIssueCountry, idType, idNumber, ssn, ein, income, asset, phone,
			areaCode, email, wechat, facebook, twitter, weibo, street1,
			street2, city, state, country, zipCode, isMailDiff, mailStreet1,
			mailStreet2, mailCity, mailState, mailCountry, mailZipCode);
	}

	public static com.liferay.portal.kernel.json.JSONObject updatePhoto(
		long userId, java.lang.String coinuserId, java.lang.String photoStr) {
		return getService().updatePhoto(userId, coinuserId, photoStr);
	}

	public static com.liferay.portal.kernel.json.JSONObject updateDoc(
		long userId, java.lang.String coinuserId, java.lang.String docStr) {
		return getService().updateDoc(userId, coinuserId, docStr);
	}

	public static boolean deactivateSaleUser(long userId, java.lang.String id) {
		return getService().deactivateSaleUser(userId, id);
	}

	public static boolean deactivateUser(java.lang.String id) {
		return getService().deactivateUser(id);
	}

	public static boolean verifyAcredit(java.lang.String id) {
		return getService().verifyAcredit(id);
	}

	public static boolean pendAcredit(java.lang.String id) {
		return getService().pendAcredit(id);
	}

	public static boolean checkValidtion(java.lang.String id) {
		return getService().checkValidtion(id);
	}

	public static boolean validateUser(java.lang.String id, long userId) {
		return getService().validateUser(id, userId);
	}

	public static void clearService() {
		_service = null;
	}

	public static CoinUserLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					CoinUserLocalService.class.getName());

			if (invokableLocalService instanceof CoinUserLocalService) {
				_service = (CoinUserLocalService)invokableLocalService;
			}
			else {
				_service = new CoinUserLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(CoinUserLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(CoinUserLocalService service) {
	}

	private static CoinUserLocalService _service;
}