/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ImageStoreLocalService}.
 *
 * @author zl
 * @see ImageStoreLocalService
 * @generated
 */
public class ImageStoreLocalServiceWrapper implements ImageStoreLocalService,
	ServiceWrapper<ImageStoreLocalService> {
	public ImageStoreLocalServiceWrapper(
		ImageStoreLocalService imageStoreLocalService) {
		_imageStoreLocalService = imageStoreLocalService;
	}

	/**
	* Adds the image store to the database. Also notifies the appropriate model listeners.
	*
	* @param imageStore the image store
	* @return the image store that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.ImageStore addImageStore(
		com.aw.internal.model.ImageStore imageStore)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _imageStoreLocalService.addImageStore(imageStore);
	}

	/**
	* Creates a new image store with the primary key. Does not add the image store to the database.
	*
	* @param imageId the primary key for the new image store
	* @return the new image store
	*/
	@Override
	public com.aw.internal.model.ImageStore createImageStore(long imageId) {
		return _imageStoreLocalService.createImageStore(imageId);
	}

	/**
	* Deletes the image store with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param imageId the primary key of the image store
	* @return the image store that was removed
	* @throws PortalException if a image store with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.ImageStore deleteImageStore(long imageId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _imageStoreLocalService.deleteImageStore(imageId);
	}

	/**
	* Deletes the image store from the database. Also notifies the appropriate model listeners.
	*
	* @param imageStore the image store
	* @return the image store that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.ImageStore deleteImageStore(
		com.aw.internal.model.ImageStore imageStore)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _imageStoreLocalService.deleteImageStore(imageStore);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _imageStoreLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _imageStoreLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.ImageStoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _imageStoreLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.ImageStoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _imageStoreLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _imageStoreLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _imageStoreLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public com.aw.internal.model.ImageStore fetchImageStore(long imageId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _imageStoreLocalService.fetchImageStore(imageId);
	}

	/**
	* Returns the image store with the primary key.
	*
	* @param imageId the primary key of the image store
	* @return the image store
	* @throws PortalException if a image store with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.ImageStore getImageStore(long imageId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _imageStoreLocalService.getImageStore(imageId);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _imageStoreLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the image stores.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.ImageStoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of image stores
	* @param end the upper bound of the range of image stores (not inclusive)
	* @return the range of image stores
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<com.aw.internal.model.ImageStore> getImageStores(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _imageStoreLocalService.getImageStores(start, end);
	}

	/**
	* Returns the number of image stores.
	*
	* @return the number of image stores
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getImageStoresCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _imageStoreLocalService.getImageStoresCount();
	}

	/**
	* Updates the image store in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param imageStore the image store
	* @return the image store that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.ImageStore updateImageStore(
		com.aw.internal.model.ImageStore imageStore)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _imageStoreLocalService.updateImageStore(imageStore);
	}

	@Override
	public com.aw.internal.model.ImageStoreContentBlobModel getContentBlobModel(
		java.io.Serializable primaryKey)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _imageStoreLocalService.getContentBlobModel(primaryKey);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _imageStoreLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_imageStoreLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _imageStoreLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	@Override
	public com.aw.internal.model.ImageStore addImage(java.lang.String base64Str)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _imageStoreLocalService.addImage(base64Str);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public ImageStoreLocalService getWrappedImageStoreLocalService() {
		return _imageStoreLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedImageStoreLocalService(
		ImageStoreLocalService imageStoreLocalService) {
		_imageStoreLocalService = imageStoreLocalService;
	}

	@Override
	public ImageStoreLocalService getWrappedService() {
		return _imageStoreLocalService;
	}

	@Override
	public void setWrappedService(ImageStoreLocalService imageStoreLocalService) {
		_imageStoreLocalService = imageStoreLocalService;
	}

	private ImageStoreLocalService _imageStoreLocalService;
}