/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.persistence;

import com.aw.internal.model.FutureUser;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the future user service. This utility wraps {@link FutureUserPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author zl
 * @see FutureUserPersistence
 * @see FutureUserPersistenceImpl
 * @generated
 */
public class FutureUserUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(FutureUser futureUser) {
		getPersistence().clearCache(futureUser);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<FutureUser> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<FutureUser> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<FutureUser> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static FutureUser update(FutureUser futureUser)
		throws SystemException {
		return getPersistence().update(futureUser);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static FutureUser update(FutureUser futureUser,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(futureUser, serviceContext);
	}

	/**
	* Returns all the future users where activeFlag = &#63; and takenFlag = &#63;.
	*
	* @param activeFlag the active flag
	* @param takenFlag the taken flag
	* @return the matching future users
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.aw.internal.model.FutureUser> findByactiveUser(
		java.lang.String activeFlag, java.lang.String takenFlag)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByactiveUser(activeFlag, takenFlag);
	}

	/**
	* Returns a range of all the future users where activeFlag = &#63; and takenFlag = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.FutureUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param activeFlag the active flag
	* @param takenFlag the taken flag
	* @param start the lower bound of the range of future users
	* @param end the upper bound of the range of future users (not inclusive)
	* @return the range of matching future users
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.aw.internal.model.FutureUser> findByactiveUser(
		java.lang.String activeFlag, java.lang.String takenFlag, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByactiveUser(activeFlag, takenFlag, start, end);
	}

	/**
	* Returns an ordered range of all the future users where activeFlag = &#63; and takenFlag = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.FutureUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param activeFlag the active flag
	* @param takenFlag the taken flag
	* @param start the lower bound of the range of future users
	* @param end the upper bound of the range of future users (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching future users
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.aw.internal.model.FutureUser> findByactiveUser(
		java.lang.String activeFlag, java.lang.String takenFlag, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByactiveUser(activeFlag, takenFlag, start, end,
			orderByComparator);
	}

	/**
	* Returns the first future user in the ordered set where activeFlag = &#63; and takenFlag = &#63;.
	*
	* @param activeFlag the active flag
	* @param takenFlag the taken flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching future user
	* @throws com.aw.internal.NoSuchFutureUserException if a matching future user could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.FutureUser findByactiveUser_First(
		java.lang.String activeFlag, java.lang.String takenFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.aw.internal.NoSuchFutureUserException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByactiveUser_First(activeFlag, takenFlag,
			orderByComparator);
	}

	/**
	* Returns the first future user in the ordered set where activeFlag = &#63; and takenFlag = &#63;.
	*
	* @param activeFlag the active flag
	* @param takenFlag the taken flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching future user, or <code>null</code> if a matching future user could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.FutureUser fetchByactiveUser_First(
		java.lang.String activeFlag, java.lang.String takenFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByactiveUser_First(activeFlag, takenFlag,
			orderByComparator);
	}

	/**
	* Returns the last future user in the ordered set where activeFlag = &#63; and takenFlag = &#63;.
	*
	* @param activeFlag the active flag
	* @param takenFlag the taken flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching future user
	* @throws com.aw.internal.NoSuchFutureUserException if a matching future user could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.FutureUser findByactiveUser_Last(
		java.lang.String activeFlag, java.lang.String takenFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.aw.internal.NoSuchFutureUserException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByactiveUser_Last(activeFlag, takenFlag,
			orderByComparator);
	}

	/**
	* Returns the last future user in the ordered set where activeFlag = &#63; and takenFlag = &#63;.
	*
	* @param activeFlag the active flag
	* @param takenFlag the taken flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching future user, or <code>null</code> if a matching future user could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.FutureUser fetchByactiveUser_Last(
		java.lang.String activeFlag, java.lang.String takenFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByactiveUser_Last(activeFlag, takenFlag,
			orderByComparator);
	}

	/**
	* Returns the future users before and after the current future user in the ordered set where activeFlag = &#63; and takenFlag = &#63;.
	*
	* @param email the primary key of the current future user
	* @param activeFlag the active flag
	* @param takenFlag the taken flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next future user
	* @throws com.aw.internal.NoSuchFutureUserException if a future user with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.FutureUser[] findByactiveUser_PrevAndNext(
		java.lang.String email, java.lang.String activeFlag,
		java.lang.String takenFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.aw.internal.NoSuchFutureUserException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByactiveUser_PrevAndNext(email, activeFlag, takenFlag,
			orderByComparator);
	}

	/**
	* Removes all the future users where activeFlag = &#63; and takenFlag = &#63; from the database.
	*
	* @param activeFlag the active flag
	* @param takenFlag the taken flag
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByactiveUser(java.lang.String activeFlag,
		java.lang.String takenFlag)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByactiveUser(activeFlag, takenFlag);
	}

	/**
	* Returns the number of future users where activeFlag = &#63; and takenFlag = &#63;.
	*
	* @param activeFlag the active flag
	* @param takenFlag the taken flag
	* @return the number of matching future users
	* @throws SystemException if a system exception occurred
	*/
	public static int countByactiveUser(java.lang.String activeFlag,
		java.lang.String takenFlag)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByactiveUser(activeFlag, takenFlag);
	}

	/**
	* Returns all the future users where activeFlag = &#63; and ownerId = &#63;.
	*
	* @param activeFlag the active flag
	* @param ownerId the owner ID
	* @return the matching future users
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.aw.internal.model.FutureUser> findBymytaken(
		java.lang.String activeFlag, long ownerId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBymytaken(activeFlag, ownerId);
	}

	/**
	* Returns a range of all the future users where activeFlag = &#63; and ownerId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.FutureUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param activeFlag the active flag
	* @param ownerId the owner ID
	* @param start the lower bound of the range of future users
	* @param end the upper bound of the range of future users (not inclusive)
	* @return the range of matching future users
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.aw.internal.model.FutureUser> findBymytaken(
		java.lang.String activeFlag, long ownerId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBymytaken(activeFlag, ownerId, start, end);
	}

	/**
	* Returns an ordered range of all the future users where activeFlag = &#63; and ownerId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.FutureUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param activeFlag the active flag
	* @param ownerId the owner ID
	* @param start the lower bound of the range of future users
	* @param end the upper bound of the range of future users (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching future users
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.aw.internal.model.FutureUser> findBymytaken(
		java.lang.String activeFlag, long ownerId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBymytaken(activeFlag, ownerId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first future user in the ordered set where activeFlag = &#63; and ownerId = &#63;.
	*
	* @param activeFlag the active flag
	* @param ownerId the owner ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching future user
	* @throws com.aw.internal.NoSuchFutureUserException if a matching future user could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.FutureUser findBymytaken_First(
		java.lang.String activeFlag, long ownerId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.aw.internal.NoSuchFutureUserException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBymytaken_First(activeFlag, ownerId, orderByComparator);
	}

	/**
	* Returns the first future user in the ordered set where activeFlag = &#63; and ownerId = &#63;.
	*
	* @param activeFlag the active flag
	* @param ownerId the owner ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching future user, or <code>null</code> if a matching future user could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.FutureUser fetchBymytaken_First(
		java.lang.String activeFlag, long ownerId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBymytaken_First(activeFlag, ownerId, orderByComparator);
	}

	/**
	* Returns the last future user in the ordered set where activeFlag = &#63; and ownerId = &#63;.
	*
	* @param activeFlag the active flag
	* @param ownerId the owner ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching future user
	* @throws com.aw.internal.NoSuchFutureUserException if a matching future user could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.FutureUser findBymytaken_Last(
		java.lang.String activeFlag, long ownerId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.aw.internal.NoSuchFutureUserException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBymytaken_Last(activeFlag, ownerId, orderByComparator);
	}

	/**
	* Returns the last future user in the ordered set where activeFlag = &#63; and ownerId = &#63;.
	*
	* @param activeFlag the active flag
	* @param ownerId the owner ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching future user, or <code>null</code> if a matching future user could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.FutureUser fetchBymytaken_Last(
		java.lang.String activeFlag, long ownerId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBymytaken_Last(activeFlag, ownerId, orderByComparator);
	}

	/**
	* Returns the future users before and after the current future user in the ordered set where activeFlag = &#63; and ownerId = &#63;.
	*
	* @param email the primary key of the current future user
	* @param activeFlag the active flag
	* @param ownerId the owner ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next future user
	* @throws com.aw.internal.NoSuchFutureUserException if a future user with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.FutureUser[] findBymytaken_PrevAndNext(
		java.lang.String email, java.lang.String activeFlag, long ownerId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.aw.internal.NoSuchFutureUserException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBymytaken_PrevAndNext(email, activeFlag, ownerId,
			orderByComparator);
	}

	/**
	* Removes all the future users where activeFlag = &#63; and ownerId = &#63; from the database.
	*
	* @param activeFlag the active flag
	* @param ownerId the owner ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeBymytaken(java.lang.String activeFlag, long ownerId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeBymytaken(activeFlag, ownerId);
	}

	/**
	* Returns the number of future users where activeFlag = &#63; and ownerId = &#63;.
	*
	* @param activeFlag the active flag
	* @param ownerId the owner ID
	* @return the number of matching future users
	* @throws SystemException if a system exception occurred
	*/
	public static int countBymytaken(java.lang.String activeFlag, long ownerId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBymytaken(activeFlag, ownerId);
	}

	/**
	* Caches the future user in the entity cache if it is enabled.
	*
	* @param futureUser the future user
	*/
	public static void cacheResult(com.aw.internal.model.FutureUser futureUser) {
		getPersistence().cacheResult(futureUser);
	}

	/**
	* Caches the future users in the entity cache if it is enabled.
	*
	* @param futureUsers the future users
	*/
	public static void cacheResult(
		java.util.List<com.aw.internal.model.FutureUser> futureUsers) {
		getPersistence().cacheResult(futureUsers);
	}

	/**
	* Creates a new future user with the primary key. Does not add the future user to the database.
	*
	* @param email the primary key for the new future user
	* @return the new future user
	*/
	public static com.aw.internal.model.FutureUser create(
		java.lang.String email) {
		return getPersistence().create(email);
	}

	/**
	* Removes the future user with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param email the primary key of the future user
	* @return the future user that was removed
	* @throws com.aw.internal.NoSuchFutureUserException if a future user with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.FutureUser remove(
		java.lang.String email)
		throws com.aw.internal.NoSuchFutureUserException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().remove(email);
	}

	public static com.aw.internal.model.FutureUser updateImpl(
		com.aw.internal.model.FutureUser futureUser)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(futureUser);
	}

	/**
	* Returns the future user with the primary key or throws a {@link com.aw.internal.NoSuchFutureUserException} if it could not be found.
	*
	* @param email the primary key of the future user
	* @return the future user
	* @throws com.aw.internal.NoSuchFutureUserException if a future user with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.FutureUser findByPrimaryKey(
		java.lang.String email)
		throws com.aw.internal.NoSuchFutureUserException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByPrimaryKey(email);
	}

	/**
	* Returns the future user with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param email the primary key of the future user
	* @return the future user, or <code>null</code> if a future user with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.FutureUser fetchByPrimaryKey(
		java.lang.String email)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(email);
	}

	/**
	* Returns all the future users.
	*
	* @return the future users
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.aw.internal.model.FutureUser> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the future users.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.FutureUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of future users
	* @param end the upper bound of the range of future users (not inclusive)
	* @return the range of future users
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.aw.internal.model.FutureUser> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the future users.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.FutureUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of future users
	* @param end the upper bound of the range of future users (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of future users
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.aw.internal.model.FutureUser> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the future users from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of future users.
	*
	* @return the number of future users
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static FutureUserPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (FutureUserPersistence)PortletBeanLocatorUtil.locate(com.aw.internal.service.ClpSerializer.getServletContextName(),
					FutureUserPersistence.class.getName());

			ReferenceRegistry.registerReference(FutureUserUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(FutureUserPersistence persistence) {
	}

	private static FutureUserPersistence _persistence;
}