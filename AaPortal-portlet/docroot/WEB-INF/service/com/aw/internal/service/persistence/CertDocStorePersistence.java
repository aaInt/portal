/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.persistence;

import com.aw.internal.model.CertDocStore;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the cert doc store service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author zl
 * @see CertDocStorePersistenceImpl
 * @see CertDocStoreUtil
 * @generated
 */
public interface CertDocStorePersistence extends BasePersistence<CertDocStore> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CertDocStoreUtil} to access the cert doc store persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the cert doc store in the entity cache if it is enabled.
	*
	* @param certDocStore the cert doc store
	*/
	public void cacheResult(com.aw.internal.model.CertDocStore certDocStore);

	/**
	* Caches the cert doc stores in the entity cache if it is enabled.
	*
	* @param certDocStores the cert doc stores
	*/
	public void cacheResult(
		java.util.List<com.aw.internal.model.CertDocStore> certDocStores);

	/**
	* Creates a new cert doc store with the primary key. Does not add the cert doc store to the database.
	*
	* @param docId the primary key for the new cert doc store
	* @return the new cert doc store
	*/
	public com.aw.internal.model.CertDocStore create(long docId);

	/**
	* Removes the cert doc store with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param docId the primary key of the cert doc store
	* @return the cert doc store that was removed
	* @throws com.aw.internal.NoSuchCertDocStoreException if a cert doc store with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.CertDocStore remove(long docId)
		throws com.aw.internal.NoSuchCertDocStoreException,
			com.liferay.portal.kernel.exception.SystemException;

	public com.aw.internal.model.CertDocStore updateImpl(
		com.aw.internal.model.CertDocStore certDocStore)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the cert doc store with the primary key or throws a {@link com.aw.internal.NoSuchCertDocStoreException} if it could not be found.
	*
	* @param docId the primary key of the cert doc store
	* @return the cert doc store
	* @throws com.aw.internal.NoSuchCertDocStoreException if a cert doc store with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.CertDocStore findByPrimaryKey(long docId)
		throws com.aw.internal.NoSuchCertDocStoreException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the cert doc store with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param docId the primary key of the cert doc store
	* @return the cert doc store, or <code>null</code> if a cert doc store with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.CertDocStore fetchByPrimaryKey(long docId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the cert doc stores.
	*
	* @return the cert doc stores
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.aw.internal.model.CertDocStore> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the cert doc stores.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CertDocStoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of cert doc stores
	* @param end the upper bound of the range of cert doc stores (not inclusive)
	* @return the range of cert doc stores
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.aw.internal.model.CertDocStore> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the cert doc stores.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CertDocStoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of cert doc stores
	* @param end the upper bound of the range of cert doc stores (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of cert doc stores
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.aw.internal.model.CertDocStore> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the cert doc stores from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of cert doc stores.
	*
	* @return the number of cert doc stores
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}