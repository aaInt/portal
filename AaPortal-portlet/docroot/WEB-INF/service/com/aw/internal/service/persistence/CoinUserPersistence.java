/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.persistence;

import com.aw.internal.model.CoinUser;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the coin user service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author zl
 * @see CoinUserPersistenceImpl
 * @see CoinUserUtil
 * @generated
 */
public interface CoinUserPersistence extends BasePersistence<CoinUser> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CoinUserUtil} to access the coin user persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the coin users where addUser = &#63; and activeFlag = &#63;.
	*
	* @param addUser the add user
	* @param activeFlag the active flag
	* @return the matching coin users
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.aw.internal.model.CoinUser> findBysales(
		long addUser, java.lang.String activeFlag)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the coin users where addUser = &#63; and activeFlag = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param addUser the add user
	* @param activeFlag the active flag
	* @param start the lower bound of the range of coin users
	* @param end the upper bound of the range of coin users (not inclusive)
	* @return the range of matching coin users
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.aw.internal.model.CoinUser> findBysales(
		long addUser, java.lang.String activeFlag, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the coin users where addUser = &#63; and activeFlag = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param addUser the add user
	* @param activeFlag the active flag
	* @param start the lower bound of the range of coin users
	* @param end the upper bound of the range of coin users (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching coin users
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.aw.internal.model.CoinUser> findBysales(
		long addUser, java.lang.String activeFlag, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first coin user in the ordered set where addUser = &#63; and activeFlag = &#63;.
	*
	* @param addUser the add user
	* @param activeFlag the active flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching coin user
	* @throws com.aw.internal.NoSuchCoinUserException if a matching coin user could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.CoinUser findBysales_First(long addUser,
		java.lang.String activeFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.aw.internal.NoSuchCoinUserException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first coin user in the ordered set where addUser = &#63; and activeFlag = &#63;.
	*
	* @param addUser the add user
	* @param activeFlag the active flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching coin user, or <code>null</code> if a matching coin user could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.CoinUser fetchBysales_First(long addUser,
		java.lang.String activeFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last coin user in the ordered set where addUser = &#63; and activeFlag = &#63;.
	*
	* @param addUser the add user
	* @param activeFlag the active flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching coin user
	* @throws com.aw.internal.NoSuchCoinUserException if a matching coin user could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.CoinUser findBysales_Last(long addUser,
		java.lang.String activeFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.aw.internal.NoSuchCoinUserException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last coin user in the ordered set where addUser = &#63; and activeFlag = &#63;.
	*
	* @param addUser the add user
	* @param activeFlag the active flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching coin user, or <code>null</code> if a matching coin user could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.CoinUser fetchBysales_Last(long addUser,
		java.lang.String activeFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the coin users before and after the current coin user in the ordered set where addUser = &#63; and activeFlag = &#63;.
	*
	* @param userId the primary key of the current coin user
	* @param addUser the add user
	* @param activeFlag the active flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next coin user
	* @throws com.aw.internal.NoSuchCoinUserException if a coin user with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.CoinUser[] findBysales_PrevAndNext(
		java.lang.String userId, long addUser, java.lang.String activeFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.aw.internal.NoSuchCoinUserException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the coin users where addUser = &#63; and activeFlag = &#63; from the database.
	*
	* @param addUser the add user
	* @param activeFlag the active flag
	* @throws SystemException if a system exception occurred
	*/
	public void removeBysales(long addUser, java.lang.String activeFlag)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of coin users where addUser = &#63; and activeFlag = &#63;.
	*
	* @param addUser the add user
	* @param activeFlag the active flag
	* @return the number of matching coin users
	* @throws SystemException if a system exception occurred
	*/
	public int countBysales(long addUser, java.lang.String activeFlag)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the coin users where activeFlag = &#63;.
	*
	* @param activeFlag the active flag
	* @return the matching coin users
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.aw.internal.model.CoinUser> findByactiveFlag(
		java.lang.String activeFlag)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the coin users where activeFlag = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param activeFlag the active flag
	* @param start the lower bound of the range of coin users
	* @param end the upper bound of the range of coin users (not inclusive)
	* @return the range of matching coin users
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.aw.internal.model.CoinUser> findByactiveFlag(
		java.lang.String activeFlag, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the coin users where activeFlag = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param activeFlag the active flag
	* @param start the lower bound of the range of coin users
	* @param end the upper bound of the range of coin users (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching coin users
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.aw.internal.model.CoinUser> findByactiveFlag(
		java.lang.String activeFlag, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first coin user in the ordered set where activeFlag = &#63;.
	*
	* @param activeFlag the active flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching coin user
	* @throws com.aw.internal.NoSuchCoinUserException if a matching coin user could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.CoinUser findByactiveFlag_First(
		java.lang.String activeFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.aw.internal.NoSuchCoinUserException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first coin user in the ordered set where activeFlag = &#63;.
	*
	* @param activeFlag the active flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching coin user, or <code>null</code> if a matching coin user could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.CoinUser fetchByactiveFlag_First(
		java.lang.String activeFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last coin user in the ordered set where activeFlag = &#63;.
	*
	* @param activeFlag the active flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching coin user
	* @throws com.aw.internal.NoSuchCoinUserException if a matching coin user could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.CoinUser findByactiveFlag_Last(
		java.lang.String activeFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.aw.internal.NoSuchCoinUserException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last coin user in the ordered set where activeFlag = &#63;.
	*
	* @param activeFlag the active flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching coin user, or <code>null</code> if a matching coin user could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.CoinUser fetchByactiveFlag_Last(
		java.lang.String activeFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the coin users before and after the current coin user in the ordered set where activeFlag = &#63;.
	*
	* @param userId the primary key of the current coin user
	* @param activeFlag the active flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next coin user
	* @throws com.aw.internal.NoSuchCoinUserException if a coin user with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.CoinUser[] findByactiveFlag_PrevAndNext(
		java.lang.String userId, java.lang.String activeFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.aw.internal.NoSuchCoinUserException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the coin users where activeFlag = &#63; from the database.
	*
	* @param activeFlag the active flag
	* @throws SystemException if a system exception occurred
	*/
	public void removeByactiveFlag(java.lang.String activeFlag)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of coin users where activeFlag = &#63;.
	*
	* @param activeFlag the active flag
	* @return the number of matching coin users
	* @throws SystemException if a system exception occurred
	*/
	public int countByactiveFlag(java.lang.String activeFlag)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the coin users where isValidated = &#63; and activeFlag = &#63;.
	*
	* @param isValidated the is validated
	* @param activeFlag the active flag
	* @return the matching coin users
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.aw.internal.model.CoinUser> findByvalidateFlag(
		java.lang.String isValidated, java.lang.String activeFlag)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the coin users where isValidated = &#63; and activeFlag = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param isValidated the is validated
	* @param activeFlag the active flag
	* @param start the lower bound of the range of coin users
	* @param end the upper bound of the range of coin users (not inclusive)
	* @return the range of matching coin users
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.aw.internal.model.CoinUser> findByvalidateFlag(
		java.lang.String isValidated, java.lang.String activeFlag, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the coin users where isValidated = &#63; and activeFlag = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param isValidated the is validated
	* @param activeFlag the active flag
	* @param start the lower bound of the range of coin users
	* @param end the upper bound of the range of coin users (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching coin users
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.aw.internal.model.CoinUser> findByvalidateFlag(
		java.lang.String isValidated, java.lang.String activeFlag, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first coin user in the ordered set where isValidated = &#63; and activeFlag = &#63;.
	*
	* @param isValidated the is validated
	* @param activeFlag the active flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching coin user
	* @throws com.aw.internal.NoSuchCoinUserException if a matching coin user could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.CoinUser findByvalidateFlag_First(
		java.lang.String isValidated, java.lang.String activeFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.aw.internal.NoSuchCoinUserException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first coin user in the ordered set where isValidated = &#63; and activeFlag = &#63;.
	*
	* @param isValidated the is validated
	* @param activeFlag the active flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching coin user, or <code>null</code> if a matching coin user could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.CoinUser fetchByvalidateFlag_First(
		java.lang.String isValidated, java.lang.String activeFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last coin user in the ordered set where isValidated = &#63; and activeFlag = &#63;.
	*
	* @param isValidated the is validated
	* @param activeFlag the active flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching coin user
	* @throws com.aw.internal.NoSuchCoinUserException if a matching coin user could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.CoinUser findByvalidateFlag_Last(
		java.lang.String isValidated, java.lang.String activeFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.aw.internal.NoSuchCoinUserException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last coin user in the ordered set where isValidated = &#63; and activeFlag = &#63;.
	*
	* @param isValidated the is validated
	* @param activeFlag the active flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching coin user, or <code>null</code> if a matching coin user could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.CoinUser fetchByvalidateFlag_Last(
		java.lang.String isValidated, java.lang.String activeFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the coin users before and after the current coin user in the ordered set where isValidated = &#63; and activeFlag = &#63;.
	*
	* @param userId the primary key of the current coin user
	* @param isValidated the is validated
	* @param activeFlag the active flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next coin user
	* @throws com.aw.internal.NoSuchCoinUserException if a coin user with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.CoinUser[] findByvalidateFlag_PrevAndNext(
		java.lang.String userId, java.lang.String isValidated,
		java.lang.String activeFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.aw.internal.NoSuchCoinUserException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the coin users where isValidated = &#63; and activeFlag = &#63; from the database.
	*
	* @param isValidated the is validated
	* @param activeFlag the active flag
	* @throws SystemException if a system exception occurred
	*/
	public void removeByvalidateFlag(java.lang.String isValidated,
		java.lang.String activeFlag)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of coin users where isValidated = &#63; and activeFlag = &#63;.
	*
	* @param isValidated the is validated
	* @param activeFlag the active flag
	* @return the number of matching coin users
	* @throws SystemException if a system exception occurred
	*/
	public int countByvalidateFlag(java.lang.String isValidated,
		java.lang.String activeFlag)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the coin users where firstName = &#63; and lastName = &#63; and idNumber = &#63;.
	*
	* @param firstName the first name
	* @param lastName the last name
	* @param idNumber the id number
	* @return the matching coin users
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.aw.internal.model.CoinUser> findBynameId(
		java.lang.String firstName, java.lang.String lastName,
		java.lang.String idNumber)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the coin users where firstName = &#63; and lastName = &#63; and idNumber = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param firstName the first name
	* @param lastName the last name
	* @param idNumber the id number
	* @param start the lower bound of the range of coin users
	* @param end the upper bound of the range of coin users (not inclusive)
	* @return the range of matching coin users
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.aw.internal.model.CoinUser> findBynameId(
		java.lang.String firstName, java.lang.String lastName,
		java.lang.String idNumber, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the coin users where firstName = &#63; and lastName = &#63; and idNumber = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param firstName the first name
	* @param lastName the last name
	* @param idNumber the id number
	* @param start the lower bound of the range of coin users
	* @param end the upper bound of the range of coin users (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching coin users
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.aw.internal.model.CoinUser> findBynameId(
		java.lang.String firstName, java.lang.String lastName,
		java.lang.String idNumber, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first coin user in the ordered set where firstName = &#63; and lastName = &#63; and idNumber = &#63;.
	*
	* @param firstName the first name
	* @param lastName the last name
	* @param idNumber the id number
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching coin user
	* @throws com.aw.internal.NoSuchCoinUserException if a matching coin user could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.CoinUser findBynameId_First(
		java.lang.String firstName, java.lang.String lastName,
		java.lang.String idNumber,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.aw.internal.NoSuchCoinUserException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first coin user in the ordered set where firstName = &#63; and lastName = &#63; and idNumber = &#63;.
	*
	* @param firstName the first name
	* @param lastName the last name
	* @param idNumber the id number
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching coin user, or <code>null</code> if a matching coin user could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.CoinUser fetchBynameId_First(
		java.lang.String firstName, java.lang.String lastName,
		java.lang.String idNumber,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last coin user in the ordered set where firstName = &#63; and lastName = &#63; and idNumber = &#63;.
	*
	* @param firstName the first name
	* @param lastName the last name
	* @param idNumber the id number
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching coin user
	* @throws com.aw.internal.NoSuchCoinUserException if a matching coin user could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.CoinUser findBynameId_Last(
		java.lang.String firstName, java.lang.String lastName,
		java.lang.String idNumber,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.aw.internal.NoSuchCoinUserException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last coin user in the ordered set where firstName = &#63; and lastName = &#63; and idNumber = &#63;.
	*
	* @param firstName the first name
	* @param lastName the last name
	* @param idNumber the id number
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching coin user, or <code>null</code> if a matching coin user could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.CoinUser fetchBynameId_Last(
		java.lang.String firstName, java.lang.String lastName,
		java.lang.String idNumber,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the coin users before and after the current coin user in the ordered set where firstName = &#63; and lastName = &#63; and idNumber = &#63;.
	*
	* @param userId the primary key of the current coin user
	* @param firstName the first name
	* @param lastName the last name
	* @param idNumber the id number
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next coin user
	* @throws com.aw.internal.NoSuchCoinUserException if a coin user with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.CoinUser[] findBynameId_PrevAndNext(
		java.lang.String userId, java.lang.String firstName,
		java.lang.String lastName, java.lang.String idNumber,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.aw.internal.NoSuchCoinUserException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the coin users where firstName = &#63; and lastName = &#63; and idNumber = &#63; from the database.
	*
	* @param firstName the first name
	* @param lastName the last name
	* @param idNumber the id number
	* @throws SystemException if a system exception occurred
	*/
	public void removeBynameId(java.lang.String firstName,
		java.lang.String lastName, java.lang.String idNumber)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of coin users where firstName = &#63; and lastName = &#63; and idNumber = &#63;.
	*
	* @param firstName the first name
	* @param lastName the last name
	* @param idNumber the id number
	* @return the number of matching coin users
	* @throws SystemException if a system exception occurred
	*/
	public int countBynameId(java.lang.String firstName,
		java.lang.String lastName, java.lang.String idNumber)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the coin user in the entity cache if it is enabled.
	*
	* @param coinUser the coin user
	*/
	public void cacheResult(com.aw.internal.model.CoinUser coinUser);

	/**
	* Caches the coin users in the entity cache if it is enabled.
	*
	* @param coinUsers the coin users
	*/
	public void cacheResult(
		java.util.List<com.aw.internal.model.CoinUser> coinUsers);

	/**
	* Creates a new coin user with the primary key. Does not add the coin user to the database.
	*
	* @param userId the primary key for the new coin user
	* @return the new coin user
	*/
	public com.aw.internal.model.CoinUser create(java.lang.String userId);

	/**
	* Removes the coin user with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userId the primary key of the coin user
	* @return the coin user that was removed
	* @throws com.aw.internal.NoSuchCoinUserException if a coin user with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.CoinUser remove(java.lang.String userId)
		throws com.aw.internal.NoSuchCoinUserException,
			com.liferay.portal.kernel.exception.SystemException;

	public com.aw.internal.model.CoinUser updateImpl(
		com.aw.internal.model.CoinUser coinUser)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the coin user with the primary key or throws a {@link com.aw.internal.NoSuchCoinUserException} if it could not be found.
	*
	* @param userId the primary key of the coin user
	* @return the coin user
	* @throws com.aw.internal.NoSuchCoinUserException if a coin user with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.CoinUser findByPrimaryKey(
		java.lang.String userId)
		throws com.aw.internal.NoSuchCoinUserException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the coin user with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param userId the primary key of the coin user
	* @return the coin user, or <code>null</code> if a coin user with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.CoinUser fetchByPrimaryKey(
		java.lang.String userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the coin users.
	*
	* @return the coin users
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.aw.internal.model.CoinUser> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the coin users.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of coin users
	* @param end the upper bound of the range of coin users (not inclusive)
	* @return the range of coin users
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.aw.internal.model.CoinUser> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the coin users.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of coin users
	* @param end the upper bound of the range of coin users (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of coin users
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.aw.internal.model.CoinUser> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the coin users from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of coin users.
	*
	* @return the number of coin users
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}