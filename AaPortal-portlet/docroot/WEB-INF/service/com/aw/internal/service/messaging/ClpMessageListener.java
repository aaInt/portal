/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.messaging;

import com.aw.internal.service.AaContactLocalServiceUtil;
import com.aw.internal.service.ActivePhaseLocalServiceUtil;
import com.aw.internal.service.BrokerInfoLocalServiceUtil;
import com.aw.internal.service.CertDocStoreLocalServiceUtil;
import com.aw.internal.service.ClpSerializer;
import com.aw.internal.service.CoinCountLocalServiceUtil;
import com.aw.internal.service.CoinOrderLocalServiceUtil;
import com.aw.internal.service.CoinOrderServiceUtil;
import com.aw.internal.service.CoinUserLocalServiceUtil;
import com.aw.internal.service.CoinUserServiceUtil;
import com.aw.internal.service.CommonFunctionServiceUtil;
import com.aw.internal.service.FailReasonLocalServiceUtil;
import com.aw.internal.service.FundInfoLocalServiceUtil;
import com.aw.internal.service.FutureUserLocalServiceUtil;
import com.aw.internal.service.FutureUserServiceUtil;
import com.aw.internal.service.ImageStoreLocalServiceUtil;
import com.aw.internal.service.UnitPriceLocalServiceUtil;

import com.liferay.portal.kernel.messaging.BaseMessageListener;
import com.liferay.portal.kernel.messaging.Message;

/**
 * @author zl
 */
public class ClpMessageListener extends BaseMessageListener {
	public static String getServletContextName() {
		return ClpSerializer.getServletContextName();
	}

	@Override
	protected void doReceive(Message message) throws Exception {
		String command = message.getString("command");
		String servletContextName = message.getString("servletContextName");

		if (command.equals("undeploy") &&
				servletContextName.equals(getServletContextName())) {
			AaContactLocalServiceUtil.clearService();

			ActivePhaseLocalServiceUtil.clearService();

			BrokerInfoLocalServiceUtil.clearService();

			CertDocStoreLocalServiceUtil.clearService();

			CoinCountLocalServiceUtil.clearService();

			CoinOrderLocalServiceUtil.clearService();

			CoinOrderServiceUtil.clearService();
			CoinUserLocalServiceUtil.clearService();

			CoinUserServiceUtil.clearService();

			CommonFunctionServiceUtil.clearService();
			FailReasonLocalServiceUtil.clearService();

			FundInfoLocalServiceUtil.clearService();

			FutureUserLocalServiceUtil.clearService();

			FutureUserServiceUtil.clearService();
			ImageStoreLocalServiceUtil.clearService();

			UnitPriceLocalServiceUtil.clearService();
		}
	}
}