/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link FutureUserService}.
 *
 * @author zl
 * @see FutureUserService
 * @generated
 */
public class FutureUserServiceWrapper implements FutureUserService,
	ServiceWrapper<FutureUserService> {
	public FutureUserServiceWrapper(FutureUserService futureUserService) {
		_futureUserService = futureUserService;
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _futureUserService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_futureUserService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _futureUserService.invokeMethod(name, parameterTypes, arguments);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject recordUser(
		java.lang.String email, java.lang.String firstN,
		java.lang.String lastN, java.lang.String residency, int areaCode,
		long phone) {
		return _futureUserService.recordUser(email, firstN, lastN, residency,
			areaCode, phone);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject takeUser(
		java.lang.String email) {
		return _futureUserService.takeUser(email);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject releaseTaken(
		java.lang.String email) {
		return _futureUserService.releaseTaken(email);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getAllActiveFutureUsers() {
		return _futureUserService.getAllActiveFutureUsers();
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getMyActiveFutureUsers() {
		return _futureUserService.getMyActiveFutureUsers();
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject deactivateUser(
		java.lang.String email) {
		return _futureUserService.deactivateUser(email);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public FutureUserService getWrappedFutureUserService() {
		return _futureUserService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedFutureUserService(FutureUserService futureUserService) {
		_futureUserService = futureUserService;
	}

	@Override
	public FutureUserService getWrappedService() {
		return _futureUserService;
	}

	@Override
	public void setWrappedService(FutureUserService futureUserService) {
		_futureUserService = futureUserService;
	}

	private FutureUserService _futureUserService;
}