/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.persistence;

import com.aw.internal.model.FundInfo;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the fund info service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author zl
 * @see FundInfoPersistenceImpl
 * @see FundInfoUtil
 * @generated
 */
public interface FundInfoPersistence extends BasePersistence<FundInfo> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link FundInfoUtil} to access the fund info persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the fund info in the entity cache if it is enabled.
	*
	* @param fundInfo the fund info
	*/
	public void cacheResult(com.aw.internal.model.FundInfo fundInfo);

	/**
	* Caches the fund infos in the entity cache if it is enabled.
	*
	* @param fundInfos the fund infos
	*/
	public void cacheResult(
		java.util.List<com.aw.internal.model.FundInfo> fundInfos);

	/**
	* Creates a new fund info with the primary key. Does not add the fund info to the database.
	*
	* @param fundId the primary key for the new fund info
	* @return the new fund info
	*/
	public com.aw.internal.model.FundInfo create(long fundId);

	/**
	* Removes the fund info with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param fundId the primary key of the fund info
	* @return the fund info that was removed
	* @throws com.aw.internal.NoSuchFundInfoException if a fund info with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.FundInfo remove(long fundId)
		throws com.aw.internal.NoSuchFundInfoException,
			com.liferay.portal.kernel.exception.SystemException;

	public com.aw.internal.model.FundInfo updateImpl(
		com.aw.internal.model.FundInfo fundInfo)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the fund info with the primary key or throws a {@link com.aw.internal.NoSuchFundInfoException} if it could not be found.
	*
	* @param fundId the primary key of the fund info
	* @return the fund info
	* @throws com.aw.internal.NoSuchFundInfoException if a fund info with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.FundInfo findByPrimaryKey(long fundId)
		throws com.aw.internal.NoSuchFundInfoException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the fund info with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param fundId the primary key of the fund info
	* @return the fund info, or <code>null</code> if a fund info with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.FundInfo fetchByPrimaryKey(long fundId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the fund infos.
	*
	* @return the fund infos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.aw.internal.model.FundInfo> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the fund infos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.FundInfoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of fund infos
	* @param end the upper bound of the range of fund infos (not inclusive)
	* @return the range of fund infos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.aw.internal.model.FundInfo> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the fund infos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.FundInfoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of fund infos
	* @param end the upper bound of the range of fund infos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of fund infos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.aw.internal.model.FundInfo> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the fund infos from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of fund infos.
	*
	* @return the number of fund infos
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}