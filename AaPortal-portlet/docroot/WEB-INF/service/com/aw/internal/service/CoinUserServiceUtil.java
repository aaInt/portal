/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableService;

/**
 * Provides the remote service utility for CoinUser. This utility wraps
 * {@link com.aw.internal.service.impl.CoinUserServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on a remote server. Methods of this service are expected to have security
 * checks based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author zl
 * @see CoinUserService
 * @see com.aw.internal.service.base.CoinUserServiceBaseImpl
 * @see com.aw.internal.service.impl.CoinUserServiceImpl
 * @generated
 */
public class CoinUserServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.aw.internal.service.impl.CoinUserServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static com.liferay.portal.kernel.json.JSONObject addCoinUser(
		java.lang.String firstName, java.lang.String lastName,
		java.lang.String birthday, java.lang.String userType,
		java.lang.String brkLics, java.lang.String brkCountry, long fundId,
		java.lang.String nationality, java.lang.String idIssueCountry,
		java.lang.String idType, java.lang.String idNumber,
		java.lang.String photoImage, java.lang.String certDocStr,
		java.lang.String ssn, java.lang.String ein, java.lang.String income,
		java.lang.String asset, long phone, int areaCode,
		java.lang.String email, java.lang.String wechat,
		java.lang.String facebook, java.lang.String twitter,
		java.lang.String weibo, java.lang.String street1,
		java.lang.String street2, java.lang.String city,
		java.lang.String state, java.lang.String country,
		java.lang.String zipCode, java.lang.String isMailDiff,
		java.lang.String mailStreet1, java.lang.String mailStreet2,
		java.lang.String mailCity, java.lang.String mailState,
		java.lang.String mailCountry, java.lang.String mailZipCode) {
		return getService()
				   .addCoinUser(firstName, lastName, birthday, userType,
			brkLics, brkCountry, fundId, nationality, idIssueCountry, idType,
			idNumber, photoImage, certDocStr, ssn, ein, income, asset, phone,
			areaCode, email, wechat, facebook, twitter, weibo, street1,
			street2, city, state, country, zipCode, isMailDiff, mailStreet1,
			mailStreet2, mailCity, mailState, mailCountry, mailZipCode);
	}

	public static com.liferay.portal.kernel.json.JSONObject updateCoinUser(
		java.lang.String coinUserId, java.lang.String firstName,
		java.lang.String lastName, java.lang.String birthday,
		java.lang.String userType, java.lang.String brkLics,
		java.lang.String brkCountry, long fundId, java.lang.String nationality,
		java.lang.String idIssueCountry, java.lang.String idType,
		java.lang.String idNumber, java.lang.String ssn, java.lang.String ein,
		java.lang.String income, java.lang.String asset, long phone,
		int areaCode, java.lang.String email, java.lang.String wechat,
		java.lang.String facebook, java.lang.String twitter,
		java.lang.String weibo, java.lang.String street1,
		java.lang.String street2, java.lang.String city,
		java.lang.String state, java.lang.String country,
		java.lang.String zipCode, java.lang.String isMailDiff,
		java.lang.String mailStreet1, java.lang.String mailStreet2,
		java.lang.String mailCity, java.lang.String mailState,
		java.lang.String mailCountry, java.lang.String mailZipCode) {
		return getService()
				   .updateCoinUser(coinUserId, firstName, lastName, birthday,
			userType, brkLics, brkCountry, fundId, nationality, idIssueCountry,
			idType, idNumber, ssn, ein, income, asset, phone, areaCode, email,
			wechat, facebook, twitter, weibo, street1, street2, city, state,
			country, zipCode, isMailDiff, mailStreet1, mailStreet2, mailCity,
			mailState, mailCountry, mailZipCode);
	}

	public static com.liferay.portal.kernel.json.JSONObject deleteUser(
		java.lang.String coinuserId) {
		return getService().deleteUser(coinuserId);
	}

	public static com.liferay.portal.kernel.json.JSONObject updatePhoto(
		java.lang.String coinuserId, java.lang.String photoStr) {
		return getService().updatePhoto(coinuserId, photoStr);
	}

	public static com.liferay.portal.kernel.json.JSONObject updateDoc(
		java.lang.String coinuserId, java.lang.String docStr) {
		return getService().updateDoc(coinuserId, docStr);
	}

	public static com.liferay.portal.kernel.json.JSONObject getCoinUser(
		java.lang.String coinUserId) {
		return getService().getCoinUser(coinUserId);
	}

	public static com.liferay.portal.kernel.json.JSONObject getValidatedCoinUsers() {
		return getService().getValidatedCoinUsers();
	}

	public static com.liferay.portal.kernel.json.JSONObject getSaleCoinUsers() {
		return getService().getSaleCoinUsers();
	}

	public static com.liferay.portal.kernel.json.JSONObject deactivateCoinUser(
		java.lang.String coinUserId) {
		return getService().deactivateCoinUser(coinUserId);
	}

	public static com.liferay.portal.kernel.json.JSONObject verifyCoinUser(
		java.lang.String coinUserId) {
		return getService().verifyCoinUser(coinUserId);
	}

	public static com.liferay.portal.kernel.json.JSONObject verifyAcredittedCoinUser(
		java.lang.String coinUserId) {
		return getService().verifyAcredittedCoinUser(coinUserId);
	}

	public static com.liferay.portal.kernel.json.JSONObject pendAcredittedCoinUser(
		java.lang.String coinUserId) {
		return getService().pendAcredittedCoinUser(coinUserId);
	}

	public static com.liferay.portal.kernel.json.JSONObject getBroker(long id) {
		return getService().getBroker(id);
	}

	public static com.liferay.portal.kernel.json.JSONObject addFund(
		java.lang.String name, long phone, int areaCode,
		java.lang.String email, java.lang.String wechat,
		java.lang.String facebook, java.lang.String twitter,
		java.lang.String weibo, java.lang.String street1,
		java.lang.String street2, java.lang.String city,
		java.lang.String state, java.lang.String country,
		java.lang.String zipCode) {
		return getService()
				   .addFund(name, phone, areaCode, email, wechat, facebook,
			twitter, weibo, street1, street2, city, state, country, zipCode);
	}

	public static com.liferay.portal.kernel.json.JSONObject getFund(long fundId) {
		return getService().getFund(fundId);
	}

	public static com.liferay.portal.kernel.json.JSONObject getFunds() {
		return getService().getFunds();
	}

	public static com.liferay.portal.kernel.json.JSONObject getContact(
		long contactId) {
		return getService().getContact(contactId);
	}

	public static java.lang.String getImage(java.lang.String userId) {
		return getService().getImage(userId);
	}

	public static java.lang.String getCertDoc(java.lang.String userId) {
		return getService().getCertDoc(userId);
	}

	public static void clearService() {
		_service = null;
	}

	public static CoinUserService getService() {
		if (_service == null) {
			InvokableService invokableService = (InvokableService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					CoinUserService.class.getName());

			if (invokableService instanceof CoinUserService) {
				_service = (CoinUserService)invokableService;
			}
			else {
				_service = new CoinUserServiceClp(invokableService);
			}

			ReferenceRegistry.registerReference(CoinUserServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(CoinUserService service) {
	}

	private static CoinUserService _service;
}