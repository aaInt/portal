/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.persistence;

import com.aw.internal.model.BrokerInfo;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the broker info service. This utility wraps {@link BrokerInfoPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author zl
 * @see BrokerInfoPersistence
 * @see BrokerInfoPersistenceImpl
 * @generated
 */
public class BrokerInfoUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(BrokerInfo brokerInfo) {
		getPersistence().clearCache(brokerInfo);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<BrokerInfo> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<BrokerInfo> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<BrokerInfo> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static BrokerInfo update(BrokerInfo brokerInfo)
		throws SystemException {
		return getPersistence().update(brokerInfo);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static BrokerInfo update(BrokerInfo brokerInfo,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(brokerInfo, serviceContext);
	}

	/**
	* Caches the broker info in the entity cache if it is enabled.
	*
	* @param brokerInfo the broker info
	*/
	public static void cacheResult(com.aw.internal.model.BrokerInfo brokerInfo) {
		getPersistence().cacheResult(brokerInfo);
	}

	/**
	* Caches the broker infos in the entity cache if it is enabled.
	*
	* @param brokerInfos the broker infos
	*/
	public static void cacheResult(
		java.util.List<com.aw.internal.model.BrokerInfo> brokerInfos) {
		getPersistence().cacheResult(brokerInfos);
	}

	/**
	* Creates a new broker info with the primary key. Does not add the broker info to the database.
	*
	* @param brokerId the primary key for the new broker info
	* @return the new broker info
	*/
	public static com.aw.internal.model.BrokerInfo create(long brokerId) {
		return getPersistence().create(brokerId);
	}

	/**
	* Removes the broker info with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param brokerId the primary key of the broker info
	* @return the broker info that was removed
	* @throws com.aw.internal.NoSuchBrokerInfoException if a broker info with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.BrokerInfo remove(long brokerId)
		throws com.aw.internal.NoSuchBrokerInfoException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().remove(brokerId);
	}

	public static com.aw.internal.model.BrokerInfo updateImpl(
		com.aw.internal.model.BrokerInfo brokerInfo)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(brokerInfo);
	}

	/**
	* Returns the broker info with the primary key or throws a {@link com.aw.internal.NoSuchBrokerInfoException} if it could not be found.
	*
	* @param brokerId the primary key of the broker info
	* @return the broker info
	* @throws com.aw.internal.NoSuchBrokerInfoException if a broker info with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.BrokerInfo findByPrimaryKey(
		long brokerId)
		throws com.aw.internal.NoSuchBrokerInfoException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByPrimaryKey(brokerId);
	}

	/**
	* Returns the broker info with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param brokerId the primary key of the broker info
	* @return the broker info, or <code>null</code> if a broker info with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.BrokerInfo fetchByPrimaryKey(
		long brokerId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(brokerId);
	}

	/**
	* Returns all the broker infos.
	*
	* @return the broker infos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.aw.internal.model.BrokerInfo> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the broker infos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.BrokerInfoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of broker infos
	* @param end the upper bound of the range of broker infos (not inclusive)
	* @return the range of broker infos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.aw.internal.model.BrokerInfo> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the broker infos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.BrokerInfoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of broker infos
	* @param end the upper bound of the range of broker infos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of broker infos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.aw.internal.model.BrokerInfo> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the broker infos from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of broker infos.
	*
	* @return the number of broker infos
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static BrokerInfoPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (BrokerInfoPersistence)PortletBeanLocatorUtil.locate(com.aw.internal.service.ClpSerializer.getServletContextName(),
					BrokerInfoPersistence.class.getName());

			ReferenceRegistry.registerReference(BrokerInfoUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(BrokerInfoPersistence persistence) {
	}

	private static BrokerInfoPersistence _persistence;
}