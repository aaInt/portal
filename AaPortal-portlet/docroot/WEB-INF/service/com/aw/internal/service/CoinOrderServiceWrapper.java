/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CoinOrderService}.
 *
 * @author zl
 * @see CoinOrderService
 * @generated
 */
public class CoinOrderServiceWrapper implements CoinOrderService,
	ServiceWrapper<CoinOrderService> {
	public CoinOrderServiceWrapper(CoinOrderService coinOrderService) {
		_coinOrderService = coinOrderService;
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _coinOrderService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_coinOrderService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _coinOrderService.invokeMethod(name, parameterTypes, arguments);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject verifyOrderInfo(
		java.lang.String orderId, java.lang.String userId,
		java.lang.String lastName) {
		return _coinOrderService.verifyOrderInfo(orderId, userId, lastName);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject createCoinOrder(
		java.lang.String userId, long brokerId, int phase, double unitPrice,
		double totalPrice, java.lang.String currency, long saleAmt,
		long promoAmt) {
		return _coinOrderService.createCoinOrder(userId, brokerId, phase,
			unitPrice, totalPrice, currency, saleAmt, promoAmt);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject splitOrder(
		java.lang.String originOrderId, long salesId, java.lang.String userId,
		long amount) throws com.liferay.portal.kernel.exception.SystemException {
		return _coinOrderService.splitOrder(originOrderId, salesId, userId,
			amount);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getPedingOrder() {
		return _coinOrderService.getPedingOrder();
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getStaffOrders() {
		return _coinOrderService.getStaffOrders();
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getCompletedOrders() {
		return _coinOrderService.getCompletedOrders();
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getFailedOrder() {
		return _coinOrderService.getFailedOrder();
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject verified(
		java.lang.String orderId) {
		return _coinOrderService.verified(orderId);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject contractSent(
		java.lang.String orderId, java.lang.String carrier,
		java.lang.String trackingNo) {
		return _coinOrderService.contractSent(orderId, carrier, trackingNo);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject contractSigned(
		java.lang.String orderId) {
		return _coinOrderService.contractSigned(orderId);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject paid(
		java.lang.String orderId) {
		return _coinOrderService.paid(orderId);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject approve(
		java.lang.String orderId) {
		return _coinOrderService.approve(orderId);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject certSent(
		java.lang.String orderId, java.lang.String carrier,
		java.lang.String trackingNo) {
		return _coinOrderService.certSent(orderId, carrier, trackingNo);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject complete(
		java.lang.String orderId) {
		return _coinOrderService.complete(orderId);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject fail(
		java.lang.String orderId, java.lang.String rsnCd) {
		return _coinOrderService.fail(orderId, rsnCd);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject addReason(
		java.lang.String rsnCd, java.lang.String desc) {
		return _coinOrderService.addReason(rsnCd, desc);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getReason(
		java.lang.String rsnCd) {
		return _coinOrderService.getReason(rsnCd);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getReasons() {
		return _coinOrderService.getReasons();
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getSaleSummary(
		boolean isPaid, boolean isPending, int phase,
		java.lang.String startDate, java.lang.String endDate) {
		return _coinOrderService.getSaleSummary(isPaid, isPending, phase,
			startDate, endDate);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public CoinOrderService getWrappedCoinOrderService() {
		return _coinOrderService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedCoinOrderService(CoinOrderService coinOrderService) {
		_coinOrderService = coinOrderService;
	}

	@Override
	public CoinOrderService getWrappedService() {
		return _coinOrderService;
	}

	@Override
	public void setWrappedService(CoinOrderService coinOrderService) {
		_coinOrderService = coinOrderService;
	}

	private CoinOrderService _coinOrderService;
}