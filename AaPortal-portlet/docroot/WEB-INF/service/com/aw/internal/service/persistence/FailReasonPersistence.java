/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.persistence;

import com.aw.internal.model.FailReason;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the fail reason service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author zl
 * @see FailReasonPersistenceImpl
 * @see FailReasonUtil
 * @generated
 */
public interface FailReasonPersistence extends BasePersistence<FailReason> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link FailReasonUtil} to access the fail reason persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the fail reason in the entity cache if it is enabled.
	*
	* @param failReason the fail reason
	*/
	public void cacheResult(com.aw.internal.model.FailReason failReason);

	/**
	* Caches the fail reasons in the entity cache if it is enabled.
	*
	* @param failReasons the fail reasons
	*/
	public void cacheResult(
		java.util.List<com.aw.internal.model.FailReason> failReasons);

	/**
	* Creates a new fail reason with the primary key. Does not add the fail reason to the database.
	*
	* @param rsnCd the primary key for the new fail reason
	* @return the new fail reason
	*/
	public com.aw.internal.model.FailReason create(java.lang.String rsnCd);

	/**
	* Removes the fail reason with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param rsnCd the primary key of the fail reason
	* @return the fail reason that was removed
	* @throws com.aw.internal.NoSuchFailReasonException if a fail reason with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.FailReason remove(java.lang.String rsnCd)
		throws com.aw.internal.NoSuchFailReasonException,
			com.liferay.portal.kernel.exception.SystemException;

	public com.aw.internal.model.FailReason updateImpl(
		com.aw.internal.model.FailReason failReason)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the fail reason with the primary key or throws a {@link com.aw.internal.NoSuchFailReasonException} if it could not be found.
	*
	* @param rsnCd the primary key of the fail reason
	* @return the fail reason
	* @throws com.aw.internal.NoSuchFailReasonException if a fail reason with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.FailReason findByPrimaryKey(
		java.lang.String rsnCd)
		throws com.aw.internal.NoSuchFailReasonException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the fail reason with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param rsnCd the primary key of the fail reason
	* @return the fail reason, or <code>null</code> if a fail reason with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.FailReason fetchByPrimaryKey(
		java.lang.String rsnCd)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the fail reasons.
	*
	* @return the fail reasons
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.aw.internal.model.FailReason> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the fail reasons.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.FailReasonModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of fail reasons
	* @param end the upper bound of the range of fail reasons (not inclusive)
	* @return the range of fail reasons
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.aw.internal.model.FailReason> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the fail reasons.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.FailReasonModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of fail reasons
	* @param end the upper bound of the range of fail reasons (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of fail reasons
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.aw.internal.model.FailReason> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the fail reasons from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of fail reasons.
	*
	* @return the number of fail reasons
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}