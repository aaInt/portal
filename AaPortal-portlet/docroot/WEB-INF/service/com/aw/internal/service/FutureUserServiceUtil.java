/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableService;

/**
 * Provides the remote service utility for FutureUser. This utility wraps
 * {@link com.aw.internal.service.impl.FutureUserServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on a remote server. Methods of this service are expected to have security
 * checks based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author zl
 * @see FutureUserService
 * @see com.aw.internal.service.base.FutureUserServiceBaseImpl
 * @see com.aw.internal.service.impl.FutureUserServiceImpl
 * @generated
 */
public class FutureUserServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.aw.internal.service.impl.FutureUserServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static com.liferay.portal.kernel.json.JSONObject recordUser(
		java.lang.String email, java.lang.String firstN,
		java.lang.String lastN, java.lang.String residency, int areaCode,
		long phone) {
		return getService()
				   .recordUser(email, firstN, lastN, residency, areaCode, phone);
	}

	public static com.liferay.portal.kernel.json.JSONObject takeUser(
		java.lang.String email) {
		return getService().takeUser(email);
	}

	public static com.liferay.portal.kernel.json.JSONObject releaseTaken(
		java.lang.String email) {
		return getService().releaseTaken(email);
	}

	public static com.liferay.portal.kernel.json.JSONObject getAllActiveFutureUsers() {
		return getService().getAllActiveFutureUsers();
	}

	public static com.liferay.portal.kernel.json.JSONObject getMyActiveFutureUsers() {
		return getService().getMyActiveFutureUsers();
	}

	public static com.liferay.portal.kernel.json.JSONObject deactivateUser(
		java.lang.String email) {
		return getService().deactivateUser(email);
	}

	public static void clearService() {
		_service = null;
	}

	public static FutureUserService getService() {
		if (_service == null) {
			InvokableService invokableService = (InvokableService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					FutureUserService.class.getName());

			if (invokableService instanceof FutureUserService) {
				_service = (FutureUserService)invokableService;
			}
			else {
				_service = new FutureUserServiceClp(invokableService);
			}

			ReferenceRegistry.registerReference(FutureUserServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(FutureUserService service) {
	}

	private static FutureUserService _service;
}