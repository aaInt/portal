/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.persistence;

import com.aw.internal.model.CoinOrder;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the coin order service. This utility wraps {@link CoinOrderPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author zl
 * @see CoinOrderPersistence
 * @see CoinOrderPersistenceImpl
 * @generated
 */
public class CoinOrderUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(CoinOrder coinOrder) {
		getPersistence().clearCache(coinOrder);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<CoinOrder> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<CoinOrder> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<CoinOrder> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static CoinOrder update(CoinOrder coinOrder)
		throws SystemException {
		return getPersistence().update(coinOrder);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static CoinOrder update(CoinOrder coinOrder,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(coinOrder, serviceContext);
	}

	/**
	* Returns all the coin orders where activeFlag = &#63; and completeFlag = &#63;.
	*
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @return the matching coin orders
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.aw.internal.model.CoinOrder> findBypending(
		java.lang.String activeFlag, java.lang.String completeFlag)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBypending(activeFlag, completeFlag);
	}

	/**
	* Returns a range of all the coin orders where activeFlag = &#63; and completeFlag = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinOrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @param start the lower bound of the range of coin orders
	* @param end the upper bound of the range of coin orders (not inclusive)
	* @return the range of matching coin orders
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.aw.internal.model.CoinOrder> findBypending(
		java.lang.String activeFlag, java.lang.String completeFlag, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBypending(activeFlag, completeFlag, start, end);
	}

	/**
	* Returns an ordered range of all the coin orders where activeFlag = &#63; and completeFlag = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinOrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @param start the lower bound of the range of coin orders
	* @param end the upper bound of the range of coin orders (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching coin orders
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.aw.internal.model.CoinOrder> findBypending(
		java.lang.String activeFlag, java.lang.String completeFlag, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBypending(activeFlag, completeFlag, start, end,
			orderByComparator);
	}

	/**
	* Returns the first coin order in the ordered set where activeFlag = &#63; and completeFlag = &#63;.
	*
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching coin order
	* @throws com.aw.internal.NoSuchCoinOrderException if a matching coin order could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.CoinOrder findBypending_First(
		java.lang.String activeFlag, java.lang.String completeFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.aw.internal.NoSuchCoinOrderException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBypending_First(activeFlag, completeFlag,
			orderByComparator);
	}

	/**
	* Returns the first coin order in the ordered set where activeFlag = &#63; and completeFlag = &#63;.
	*
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching coin order, or <code>null</code> if a matching coin order could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.CoinOrder fetchBypending_First(
		java.lang.String activeFlag, java.lang.String completeFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBypending_First(activeFlag, completeFlag,
			orderByComparator);
	}

	/**
	* Returns the last coin order in the ordered set where activeFlag = &#63; and completeFlag = &#63;.
	*
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching coin order
	* @throws com.aw.internal.NoSuchCoinOrderException if a matching coin order could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.CoinOrder findBypending_Last(
		java.lang.String activeFlag, java.lang.String completeFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.aw.internal.NoSuchCoinOrderException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBypending_Last(activeFlag, completeFlag,
			orderByComparator);
	}

	/**
	* Returns the last coin order in the ordered set where activeFlag = &#63; and completeFlag = &#63;.
	*
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching coin order, or <code>null</code> if a matching coin order could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.CoinOrder fetchBypending_Last(
		java.lang.String activeFlag, java.lang.String completeFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBypending_Last(activeFlag, completeFlag,
			orderByComparator);
	}

	/**
	* Returns the coin orders before and after the current coin order in the ordered set where activeFlag = &#63; and completeFlag = &#63;.
	*
	* @param orderId the primary key of the current coin order
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next coin order
	* @throws com.aw.internal.NoSuchCoinOrderException if a coin order with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.CoinOrder[] findBypending_PrevAndNext(
		java.lang.String orderId, java.lang.String activeFlag,
		java.lang.String completeFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.aw.internal.NoSuchCoinOrderException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBypending_PrevAndNext(orderId, activeFlag,
			completeFlag, orderByComparator);
	}

	/**
	* Removes all the coin orders where activeFlag = &#63; and completeFlag = &#63; from the database.
	*
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @throws SystemException if a system exception occurred
	*/
	public static void removeBypending(java.lang.String activeFlag,
		java.lang.String completeFlag)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeBypending(activeFlag, completeFlag);
	}

	/**
	* Returns the number of coin orders where activeFlag = &#63; and completeFlag = &#63;.
	*
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @return the number of matching coin orders
	* @throws SystemException if a system exception occurred
	*/
	public static int countBypending(java.lang.String activeFlag,
		java.lang.String completeFlag)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBypending(activeFlag, completeFlag);
	}

	/**
	* Returns all the coin orders where activeFlag = &#63; and completeFlag = &#63;.
	*
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @return the matching coin orders
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.aw.internal.model.CoinOrder> findBycomplete(
		java.lang.String activeFlag, java.lang.String completeFlag)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBycomplete(activeFlag, completeFlag);
	}

	/**
	* Returns a range of all the coin orders where activeFlag = &#63; and completeFlag = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinOrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @param start the lower bound of the range of coin orders
	* @param end the upper bound of the range of coin orders (not inclusive)
	* @return the range of matching coin orders
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.aw.internal.model.CoinOrder> findBycomplete(
		java.lang.String activeFlag, java.lang.String completeFlag, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBycomplete(activeFlag, completeFlag, start, end);
	}

	/**
	* Returns an ordered range of all the coin orders where activeFlag = &#63; and completeFlag = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinOrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @param start the lower bound of the range of coin orders
	* @param end the upper bound of the range of coin orders (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching coin orders
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.aw.internal.model.CoinOrder> findBycomplete(
		java.lang.String activeFlag, java.lang.String completeFlag, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBycomplete(activeFlag, completeFlag, start, end,
			orderByComparator);
	}

	/**
	* Returns the first coin order in the ordered set where activeFlag = &#63; and completeFlag = &#63;.
	*
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching coin order
	* @throws com.aw.internal.NoSuchCoinOrderException if a matching coin order could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.CoinOrder findBycomplete_First(
		java.lang.String activeFlag, java.lang.String completeFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.aw.internal.NoSuchCoinOrderException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBycomplete_First(activeFlag, completeFlag,
			orderByComparator);
	}

	/**
	* Returns the first coin order in the ordered set where activeFlag = &#63; and completeFlag = &#63;.
	*
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching coin order, or <code>null</code> if a matching coin order could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.CoinOrder fetchBycomplete_First(
		java.lang.String activeFlag, java.lang.String completeFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBycomplete_First(activeFlag, completeFlag,
			orderByComparator);
	}

	/**
	* Returns the last coin order in the ordered set where activeFlag = &#63; and completeFlag = &#63;.
	*
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching coin order
	* @throws com.aw.internal.NoSuchCoinOrderException if a matching coin order could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.CoinOrder findBycomplete_Last(
		java.lang.String activeFlag, java.lang.String completeFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.aw.internal.NoSuchCoinOrderException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBycomplete_Last(activeFlag, completeFlag,
			orderByComparator);
	}

	/**
	* Returns the last coin order in the ordered set where activeFlag = &#63; and completeFlag = &#63;.
	*
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching coin order, or <code>null</code> if a matching coin order could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.CoinOrder fetchBycomplete_Last(
		java.lang.String activeFlag, java.lang.String completeFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBycomplete_Last(activeFlag, completeFlag,
			orderByComparator);
	}

	/**
	* Returns the coin orders before and after the current coin order in the ordered set where activeFlag = &#63; and completeFlag = &#63;.
	*
	* @param orderId the primary key of the current coin order
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next coin order
	* @throws com.aw.internal.NoSuchCoinOrderException if a coin order with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.CoinOrder[] findBycomplete_PrevAndNext(
		java.lang.String orderId, java.lang.String activeFlag,
		java.lang.String completeFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.aw.internal.NoSuchCoinOrderException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBycomplete_PrevAndNext(orderId, activeFlag,
			completeFlag, orderByComparator);
	}

	/**
	* Removes all the coin orders where activeFlag = &#63; and completeFlag = &#63; from the database.
	*
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @throws SystemException if a system exception occurred
	*/
	public static void removeBycomplete(java.lang.String activeFlag,
		java.lang.String completeFlag)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeBycomplete(activeFlag, completeFlag);
	}

	/**
	* Returns the number of coin orders where activeFlag = &#63; and completeFlag = &#63;.
	*
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @return the number of matching coin orders
	* @throws SystemException if a system exception occurred
	*/
	public static int countBycomplete(java.lang.String activeFlag,
		java.lang.String completeFlag)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBycomplete(activeFlag, completeFlag);
	}

	/**
	* Caches the coin order in the entity cache if it is enabled.
	*
	* @param coinOrder the coin order
	*/
	public static void cacheResult(com.aw.internal.model.CoinOrder coinOrder) {
		getPersistence().cacheResult(coinOrder);
	}

	/**
	* Caches the coin orders in the entity cache if it is enabled.
	*
	* @param coinOrders the coin orders
	*/
	public static void cacheResult(
		java.util.List<com.aw.internal.model.CoinOrder> coinOrders) {
		getPersistence().cacheResult(coinOrders);
	}

	/**
	* Creates a new coin order with the primary key. Does not add the coin order to the database.
	*
	* @param orderId the primary key for the new coin order
	* @return the new coin order
	*/
	public static com.aw.internal.model.CoinOrder create(
		java.lang.String orderId) {
		return getPersistence().create(orderId);
	}

	/**
	* Removes the coin order with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param orderId the primary key of the coin order
	* @return the coin order that was removed
	* @throws com.aw.internal.NoSuchCoinOrderException if a coin order with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.CoinOrder remove(
		java.lang.String orderId)
		throws com.aw.internal.NoSuchCoinOrderException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().remove(orderId);
	}

	public static com.aw.internal.model.CoinOrder updateImpl(
		com.aw.internal.model.CoinOrder coinOrder)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(coinOrder);
	}

	/**
	* Returns the coin order with the primary key or throws a {@link com.aw.internal.NoSuchCoinOrderException} if it could not be found.
	*
	* @param orderId the primary key of the coin order
	* @return the coin order
	* @throws com.aw.internal.NoSuchCoinOrderException if a coin order with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.CoinOrder findByPrimaryKey(
		java.lang.String orderId)
		throws com.aw.internal.NoSuchCoinOrderException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByPrimaryKey(orderId);
	}

	/**
	* Returns the coin order with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param orderId the primary key of the coin order
	* @return the coin order, or <code>null</code> if a coin order with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.CoinOrder fetchByPrimaryKey(
		java.lang.String orderId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(orderId);
	}

	/**
	* Returns all the coin orders.
	*
	* @return the coin orders
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.aw.internal.model.CoinOrder> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the coin orders.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinOrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of coin orders
	* @param end the upper bound of the range of coin orders (not inclusive)
	* @return the range of coin orders
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.aw.internal.model.CoinOrder> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the coin orders.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinOrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of coin orders
	* @param end the upper bound of the range of coin orders (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of coin orders
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.aw.internal.model.CoinOrder> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the coin orders from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of coin orders.
	*
	* @return the number of coin orders
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static CoinOrderPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (CoinOrderPersistence)PortletBeanLocatorUtil.locate(com.aw.internal.service.ClpSerializer.getServletContextName(),
					CoinOrderPersistence.class.getName());

			ReferenceRegistry.registerReference(CoinOrderUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(CoinOrderPersistence persistence) {
	}

	private static CoinOrderPersistence _persistence;
}