/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.persistence;

import com.aw.internal.model.ActivePhase;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the active phase service. This utility wraps {@link ActivePhasePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author zl
 * @see ActivePhasePersistence
 * @see ActivePhasePersistenceImpl
 * @generated
 */
public class ActivePhaseUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(ActivePhase activePhase) {
		getPersistence().clearCache(activePhase);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<ActivePhase> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<ActivePhase> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<ActivePhase> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static ActivePhase update(ActivePhase activePhase)
		throws SystemException {
		return getPersistence().update(activePhase);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static ActivePhase update(ActivePhase activePhase,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(activePhase, serviceContext);
	}

	/**
	* Caches the active phase in the entity cache if it is enabled.
	*
	* @param activePhase the active phase
	*/
	public static void cacheResult(
		com.aw.internal.model.ActivePhase activePhase) {
		getPersistence().cacheResult(activePhase);
	}

	/**
	* Caches the active phases in the entity cache if it is enabled.
	*
	* @param activePhases the active phases
	*/
	public static void cacheResult(
		java.util.List<com.aw.internal.model.ActivePhase> activePhases) {
		getPersistence().cacheResult(activePhases);
	}

	/**
	* Creates a new active phase with the primary key. Does not add the active phase to the database.
	*
	* @param region the primary key for the new active phase
	* @return the new active phase
	*/
	public static com.aw.internal.model.ActivePhase create(
		java.lang.String region) {
		return getPersistence().create(region);
	}

	/**
	* Removes the active phase with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param region the primary key of the active phase
	* @return the active phase that was removed
	* @throws com.aw.internal.NoSuchActivePhaseException if a active phase with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.ActivePhase remove(
		java.lang.String region)
		throws com.aw.internal.NoSuchActivePhaseException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().remove(region);
	}

	public static com.aw.internal.model.ActivePhase updateImpl(
		com.aw.internal.model.ActivePhase activePhase)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(activePhase);
	}

	/**
	* Returns the active phase with the primary key or throws a {@link com.aw.internal.NoSuchActivePhaseException} if it could not be found.
	*
	* @param region the primary key of the active phase
	* @return the active phase
	* @throws com.aw.internal.NoSuchActivePhaseException if a active phase with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.ActivePhase findByPrimaryKey(
		java.lang.String region)
		throws com.aw.internal.NoSuchActivePhaseException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByPrimaryKey(region);
	}

	/**
	* Returns the active phase with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param region the primary key of the active phase
	* @return the active phase, or <code>null</code> if a active phase with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.ActivePhase fetchByPrimaryKey(
		java.lang.String region)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(region);
	}

	/**
	* Returns all the active phases.
	*
	* @return the active phases
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.aw.internal.model.ActivePhase> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the active phases.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.ActivePhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of active phases
	* @param end the upper bound of the range of active phases (not inclusive)
	* @return the range of active phases
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.aw.internal.model.ActivePhase> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the active phases.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.ActivePhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of active phases
	* @param end the upper bound of the range of active phases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of active phases
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.aw.internal.model.ActivePhase> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the active phases from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of active phases.
	*
	* @return the number of active phases
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static ActivePhasePersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (ActivePhasePersistence)PortletBeanLocatorUtil.locate(com.aw.internal.service.ClpSerializer.getServletContextName(),
					ActivePhasePersistence.class.getName());

			ReferenceRegistry.registerReference(ActivePhaseUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(ActivePhasePersistence persistence) {
	}

	private static ActivePhasePersistence _persistence;
}