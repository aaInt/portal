/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service;

import com.liferay.portal.service.InvokableService;

/**
 * @author zl
 * @generated
 */
public class CoinUserServiceClp implements CoinUserService {
	public CoinUserServiceClp(InvokableService invokableService) {
		_invokableService = invokableService;

		_methodName0 = "getBeanIdentifier";

		_methodParameterTypes0 = new String[] {  };

		_methodName1 = "setBeanIdentifier";

		_methodParameterTypes1 = new String[] { "java.lang.String" };

		_methodName3 = "addCoinUser";

		_methodParameterTypes3 = new String[] {
				"java.lang.String", "java.lang.String", "java.lang.String",
				"java.lang.String", "java.lang.String", "java.lang.String",
				"long", "java.lang.String", "java.lang.String",
				"java.lang.String", "java.lang.String", "java.lang.String",
				"java.lang.String", "java.lang.String", "java.lang.String",
				"java.lang.String", "java.lang.String", "long", "int",
				"java.lang.String", "java.lang.String", "java.lang.String",
				"java.lang.String", "java.lang.String", "java.lang.String",
				"java.lang.String", "java.lang.String", "java.lang.String",
				"java.lang.String", "java.lang.String", "java.lang.String",
				"java.lang.String", "java.lang.String", "java.lang.String",
				"java.lang.String", "java.lang.String", "java.lang.String"
			};

		_methodName4 = "updateCoinUser";

		_methodParameterTypes4 = new String[] {
				"java.lang.String", "java.lang.String", "java.lang.String",
				"java.lang.String", "java.lang.String", "java.lang.String",
				"java.lang.String", "long", "java.lang.String",
				"java.lang.String", "java.lang.String", "java.lang.String",
				"java.lang.String", "java.lang.String", "java.lang.String",
				"java.lang.String", "long", "int", "java.lang.String",
				"java.lang.String", "java.lang.String", "java.lang.String",
				"java.lang.String", "java.lang.String", "java.lang.String",
				"java.lang.String", "java.lang.String", "java.lang.String",
				"java.lang.String", "java.lang.String", "java.lang.String",
				"java.lang.String", "java.lang.String", "java.lang.String",
				"java.lang.String", "java.lang.String"
			};

		_methodName5 = "deleteUser";

		_methodParameterTypes5 = new String[] { "java.lang.String" };

		_methodName6 = "updatePhoto";

		_methodParameterTypes6 = new String[] {
				"java.lang.String", "java.lang.String"
			};

		_methodName7 = "updateDoc";

		_methodParameterTypes7 = new String[] {
				"java.lang.String", "java.lang.String"
			};

		_methodName8 = "getCoinUser";

		_methodParameterTypes8 = new String[] { "java.lang.String" };

		_methodName9 = "getValidatedCoinUsers";

		_methodParameterTypes9 = new String[] {  };

		_methodName10 = "getSaleCoinUsers";

		_methodParameterTypes10 = new String[] {  };

		_methodName11 = "deactivateCoinUser";

		_methodParameterTypes11 = new String[] { "java.lang.String" };

		_methodName12 = "verifyCoinUser";

		_methodParameterTypes12 = new String[] { "java.lang.String" };

		_methodName13 = "verifyAcredittedCoinUser";

		_methodParameterTypes13 = new String[] { "java.lang.String" };

		_methodName14 = "pendAcredittedCoinUser";

		_methodParameterTypes14 = new String[] { "java.lang.String" };

		_methodName15 = "getBroker";

		_methodParameterTypes15 = new String[] { "long" };

		_methodName16 = "addFund";

		_methodParameterTypes16 = new String[] {
				"java.lang.String", "long", "int", "java.lang.String",
				"java.lang.String", "java.lang.String", "java.lang.String",
				"java.lang.String", "java.lang.String", "java.lang.String",
				"java.lang.String", "java.lang.String", "java.lang.String",
				"java.lang.String"
			};

		_methodName17 = "getFund";

		_methodParameterTypes17 = new String[] { "long" };

		_methodName18 = "getFunds";

		_methodParameterTypes18 = new String[] {  };

		_methodName19 = "getContact";

		_methodParameterTypes19 = new String[] { "long" };

		_methodName20 = "getImage";

		_methodParameterTypes20 = new String[] { "java.lang.String" };

		_methodName21 = "getCertDoc";

		_methodParameterTypes21 = new String[] { "java.lang.String" };
	}

	@Override
	public java.lang.String getBeanIdentifier() {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName0,
					_methodParameterTypes0, new Object[] {  });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.lang.String)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		try {
			_invokableService.invokeMethod(_methodName1,
				_methodParameterTypes1,
				new Object[] { ClpSerializer.translateInput(beanIdentifier) });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		throw new UnsupportedOperationException();
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject addCoinUser(
		java.lang.String firstName, java.lang.String lastName,
		java.lang.String birthday, java.lang.String userType,
		java.lang.String brkLics, java.lang.String brkCountry, long fundId,
		java.lang.String nationality, java.lang.String idIssueCountry,
		java.lang.String idType, java.lang.String idNumber,
		java.lang.String photoImage, java.lang.String certDocStr,
		java.lang.String ssn, java.lang.String ein, java.lang.String income,
		java.lang.String asset, long phone, int areaCode,
		java.lang.String email, java.lang.String wechat,
		java.lang.String facebook, java.lang.String twitter,
		java.lang.String weibo, java.lang.String street1,
		java.lang.String street2, java.lang.String city,
		java.lang.String state, java.lang.String country,
		java.lang.String zipCode, java.lang.String isMailDiff,
		java.lang.String mailStreet1, java.lang.String mailStreet2,
		java.lang.String mailCity, java.lang.String mailState,
		java.lang.String mailCountry, java.lang.String mailZipCode) {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName3,
					_methodParameterTypes3,
					new Object[] {
						ClpSerializer.translateInput(firstName),
						
					ClpSerializer.translateInput(lastName),
						
					ClpSerializer.translateInput(birthday),
						
					ClpSerializer.translateInput(userType),
						
					ClpSerializer.translateInput(brkLics),
						
					ClpSerializer.translateInput(brkCountry),
						
					fundId,
						
					ClpSerializer.translateInput(nationality),
						
					ClpSerializer.translateInput(idIssueCountry),
						
					ClpSerializer.translateInput(idType),
						
					ClpSerializer.translateInput(idNumber),
						
					ClpSerializer.translateInput(photoImage),
						
					ClpSerializer.translateInput(certDocStr),
						
					ClpSerializer.translateInput(ssn),
						
					ClpSerializer.translateInput(ein),
						
					ClpSerializer.translateInput(income),
						
					ClpSerializer.translateInput(asset),
						
					phone,
						
					areaCode,
						
					ClpSerializer.translateInput(email),
						
					ClpSerializer.translateInput(wechat),
						
					ClpSerializer.translateInput(facebook),
						
					ClpSerializer.translateInput(twitter),
						
					ClpSerializer.translateInput(weibo),
						
					ClpSerializer.translateInput(street1),
						
					ClpSerializer.translateInput(street2),
						
					ClpSerializer.translateInput(city),
						
					ClpSerializer.translateInput(state),
						
					ClpSerializer.translateInput(country),
						
					ClpSerializer.translateInput(zipCode),
						
					ClpSerializer.translateInput(isMailDiff),
						
					ClpSerializer.translateInput(mailStreet1),
						
					ClpSerializer.translateInput(mailStreet2),
						
					ClpSerializer.translateInput(mailCity),
						
					ClpSerializer.translateInput(mailState),
						
					ClpSerializer.translateInput(mailCountry),
						
					ClpSerializer.translateInput(mailZipCode)
					});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (com.liferay.portal.kernel.json.JSONObject)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject updateCoinUser(
		java.lang.String coinUserId, java.lang.String firstName,
		java.lang.String lastName, java.lang.String birthday,
		java.lang.String userType, java.lang.String brkLics,
		java.lang.String brkCountry, long fundId, java.lang.String nationality,
		java.lang.String idIssueCountry, java.lang.String idType,
		java.lang.String idNumber, java.lang.String ssn, java.lang.String ein,
		java.lang.String income, java.lang.String asset, long phone,
		int areaCode, java.lang.String email, java.lang.String wechat,
		java.lang.String facebook, java.lang.String twitter,
		java.lang.String weibo, java.lang.String street1,
		java.lang.String street2, java.lang.String city,
		java.lang.String state, java.lang.String country,
		java.lang.String zipCode, java.lang.String isMailDiff,
		java.lang.String mailStreet1, java.lang.String mailStreet2,
		java.lang.String mailCity, java.lang.String mailState,
		java.lang.String mailCountry, java.lang.String mailZipCode) {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName4,
					_methodParameterTypes4,
					new Object[] {
						ClpSerializer.translateInput(coinUserId),
						
					ClpSerializer.translateInput(firstName),
						
					ClpSerializer.translateInput(lastName),
						
					ClpSerializer.translateInput(birthday),
						
					ClpSerializer.translateInput(userType),
						
					ClpSerializer.translateInput(brkLics),
						
					ClpSerializer.translateInput(brkCountry),
						
					fundId,
						
					ClpSerializer.translateInput(nationality),
						
					ClpSerializer.translateInput(idIssueCountry),
						
					ClpSerializer.translateInput(idType),
						
					ClpSerializer.translateInput(idNumber),
						
					ClpSerializer.translateInput(ssn),
						
					ClpSerializer.translateInput(ein),
						
					ClpSerializer.translateInput(income),
						
					ClpSerializer.translateInput(asset),
						
					phone,
						
					areaCode,
						
					ClpSerializer.translateInput(email),
						
					ClpSerializer.translateInput(wechat),
						
					ClpSerializer.translateInput(facebook),
						
					ClpSerializer.translateInput(twitter),
						
					ClpSerializer.translateInput(weibo),
						
					ClpSerializer.translateInput(street1),
						
					ClpSerializer.translateInput(street2),
						
					ClpSerializer.translateInput(city),
						
					ClpSerializer.translateInput(state),
						
					ClpSerializer.translateInput(country),
						
					ClpSerializer.translateInput(zipCode),
						
					ClpSerializer.translateInput(isMailDiff),
						
					ClpSerializer.translateInput(mailStreet1),
						
					ClpSerializer.translateInput(mailStreet2),
						
					ClpSerializer.translateInput(mailCity),
						
					ClpSerializer.translateInput(mailState),
						
					ClpSerializer.translateInput(mailCountry),
						
					ClpSerializer.translateInput(mailZipCode)
					});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (com.liferay.portal.kernel.json.JSONObject)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject deleteUser(
		java.lang.String coinuserId) {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName5,
					_methodParameterTypes5,
					new Object[] { ClpSerializer.translateInput(coinuserId) });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (com.liferay.portal.kernel.json.JSONObject)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject updatePhoto(
		java.lang.String coinuserId, java.lang.String photoStr) {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName6,
					_methodParameterTypes6,
					new Object[] {
						ClpSerializer.translateInput(coinuserId),
						
					ClpSerializer.translateInput(photoStr)
					});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (com.liferay.portal.kernel.json.JSONObject)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject updateDoc(
		java.lang.String coinuserId, java.lang.String docStr) {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName7,
					_methodParameterTypes7,
					new Object[] {
						ClpSerializer.translateInput(coinuserId),
						
					ClpSerializer.translateInput(docStr)
					});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (com.liferay.portal.kernel.json.JSONObject)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getCoinUser(
		java.lang.String coinUserId) {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName8,
					_methodParameterTypes8,
					new Object[] { ClpSerializer.translateInput(coinUserId) });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (com.liferay.portal.kernel.json.JSONObject)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getValidatedCoinUsers() {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName9,
					_methodParameterTypes9, new Object[] {  });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (com.liferay.portal.kernel.json.JSONObject)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getSaleCoinUsers() {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName10,
					_methodParameterTypes10, new Object[] {  });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (com.liferay.portal.kernel.json.JSONObject)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject deactivateCoinUser(
		java.lang.String coinUserId) {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName11,
					_methodParameterTypes11,
					new Object[] { ClpSerializer.translateInput(coinUserId) });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (com.liferay.portal.kernel.json.JSONObject)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject verifyCoinUser(
		java.lang.String coinUserId) {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName12,
					_methodParameterTypes12,
					new Object[] { ClpSerializer.translateInput(coinUserId) });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (com.liferay.portal.kernel.json.JSONObject)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject verifyAcredittedCoinUser(
		java.lang.String coinUserId) {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName13,
					_methodParameterTypes13,
					new Object[] { ClpSerializer.translateInput(coinUserId) });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (com.liferay.portal.kernel.json.JSONObject)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject pendAcredittedCoinUser(
		java.lang.String coinUserId) {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName14,
					_methodParameterTypes14,
					new Object[] { ClpSerializer.translateInput(coinUserId) });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (com.liferay.portal.kernel.json.JSONObject)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getBroker(long id) {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName15,
					_methodParameterTypes15, new Object[] { id });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (com.liferay.portal.kernel.json.JSONObject)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject addFund(
		java.lang.String name, long phone, int areaCode,
		java.lang.String email, java.lang.String wechat,
		java.lang.String facebook, java.lang.String twitter,
		java.lang.String weibo, java.lang.String street1,
		java.lang.String street2, java.lang.String city,
		java.lang.String state, java.lang.String country,
		java.lang.String zipCode) {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName16,
					_methodParameterTypes16,
					new Object[] {
						ClpSerializer.translateInput(name),
						
					phone,
						
					areaCode,
						
					ClpSerializer.translateInput(email),
						
					ClpSerializer.translateInput(wechat),
						
					ClpSerializer.translateInput(facebook),
						
					ClpSerializer.translateInput(twitter),
						
					ClpSerializer.translateInput(weibo),
						
					ClpSerializer.translateInput(street1),
						
					ClpSerializer.translateInput(street2),
						
					ClpSerializer.translateInput(city),
						
					ClpSerializer.translateInput(state),
						
					ClpSerializer.translateInput(country),
						
					ClpSerializer.translateInput(zipCode)
					});
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (com.liferay.portal.kernel.json.JSONObject)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getFund(long fundId) {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName17,
					_methodParameterTypes17, new Object[] { fundId });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (com.liferay.portal.kernel.json.JSONObject)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getFunds() {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName18,
					_methodParameterTypes18, new Object[] {  });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (com.liferay.portal.kernel.json.JSONObject)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getContact(long contactId) {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName19,
					_methodParameterTypes19, new Object[] { contactId });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (com.liferay.portal.kernel.json.JSONObject)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.lang.String getImage(java.lang.String userId) {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName20,
					_methodParameterTypes20,
					new Object[] { ClpSerializer.translateInput(userId) });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.lang.String)ClpSerializer.translateOutput(returnObj);
	}

	@Override
	public java.lang.String getCertDoc(java.lang.String userId) {
		Object returnObj = null;

		try {
			returnObj = _invokableService.invokeMethod(_methodName21,
					_methodParameterTypes21,
					new Object[] { ClpSerializer.translateInput(userId) });
		}
		catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException)t;
			}
			else {
				throw new RuntimeException(t.getClass().getName() +
					" is not a valid exception");
			}
		}

		return (java.lang.String)ClpSerializer.translateOutput(returnObj);
	}

	private InvokableService _invokableService;
	private String _methodName0;
	private String[] _methodParameterTypes0;
	private String _methodName1;
	private String[] _methodParameterTypes1;
	private String _methodName3;
	private String[] _methodParameterTypes3;
	private String _methodName4;
	private String[] _methodParameterTypes4;
	private String _methodName5;
	private String[] _methodParameterTypes5;
	private String _methodName6;
	private String[] _methodParameterTypes6;
	private String _methodName7;
	private String[] _methodParameterTypes7;
	private String _methodName8;
	private String[] _methodParameterTypes8;
	private String _methodName9;
	private String[] _methodParameterTypes9;
	private String _methodName10;
	private String[] _methodParameterTypes10;
	private String _methodName11;
	private String[] _methodParameterTypes11;
	private String _methodName12;
	private String[] _methodParameterTypes12;
	private String _methodName13;
	private String[] _methodParameterTypes13;
	private String _methodName14;
	private String[] _methodParameterTypes14;
	private String _methodName15;
	private String[] _methodParameterTypes15;
	private String _methodName16;
	private String[] _methodParameterTypes16;
	private String _methodName17;
	private String[] _methodParameterTypes17;
	private String _methodName18;
	private String[] _methodParameterTypes18;
	private String _methodName19;
	private String[] _methodParameterTypes19;
	private String _methodName20;
	private String[] _methodParameterTypes20;
	private String _methodName21;
	private String[] _methodParameterTypes21;
}