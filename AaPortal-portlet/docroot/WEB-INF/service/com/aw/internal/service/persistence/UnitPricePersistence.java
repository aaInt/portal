/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.persistence;

import com.aw.internal.model.UnitPrice;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the unit price service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author zl
 * @see UnitPricePersistenceImpl
 * @see UnitPriceUtil
 * @generated
 */
public interface UnitPricePersistence extends BasePersistence<UnitPrice> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link UnitPriceUtil} to access the unit price persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the unit price in the entity cache if it is enabled.
	*
	* @param unitPrice the unit price
	*/
	public void cacheResult(com.aw.internal.model.UnitPrice unitPrice);

	/**
	* Caches the unit prices in the entity cache if it is enabled.
	*
	* @param unitPrices the unit prices
	*/
	public void cacheResult(
		java.util.List<com.aw.internal.model.UnitPrice> unitPrices);

	/**
	* Creates a new unit price with the primary key. Does not add the unit price to the database.
	*
	* @param unitPricePK the primary key for the new unit price
	* @return the new unit price
	*/
	public com.aw.internal.model.UnitPrice create(
		com.aw.internal.service.persistence.UnitPricePK unitPricePK);

	/**
	* Removes the unit price with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param unitPricePK the primary key of the unit price
	* @return the unit price that was removed
	* @throws com.aw.internal.NoSuchUnitPriceException if a unit price with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.UnitPrice remove(
		com.aw.internal.service.persistence.UnitPricePK unitPricePK)
		throws com.aw.internal.NoSuchUnitPriceException,
			com.liferay.portal.kernel.exception.SystemException;

	public com.aw.internal.model.UnitPrice updateImpl(
		com.aw.internal.model.UnitPrice unitPrice)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the unit price with the primary key or throws a {@link com.aw.internal.NoSuchUnitPriceException} if it could not be found.
	*
	* @param unitPricePK the primary key of the unit price
	* @return the unit price
	* @throws com.aw.internal.NoSuchUnitPriceException if a unit price with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.UnitPrice findByPrimaryKey(
		com.aw.internal.service.persistence.UnitPricePK unitPricePK)
		throws com.aw.internal.NoSuchUnitPriceException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the unit price with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param unitPricePK the primary key of the unit price
	* @return the unit price, or <code>null</code> if a unit price with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.UnitPrice fetchByPrimaryKey(
		com.aw.internal.service.persistence.UnitPricePK unitPricePK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the unit prices.
	*
	* @return the unit prices
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.aw.internal.model.UnitPrice> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the unit prices.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.UnitPriceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of unit prices
	* @param end the upper bound of the range of unit prices (not inclusive)
	* @return the range of unit prices
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.aw.internal.model.UnitPrice> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the unit prices.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.UnitPriceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of unit prices
	* @param end the upper bound of the range of unit prices (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of unit prices
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.aw.internal.model.UnitPrice> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the unit prices from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of unit prices.
	*
	* @return the number of unit prices
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}