/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.persistence;

import com.aw.internal.model.ActivePhase;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the active phase service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author zl
 * @see ActivePhasePersistenceImpl
 * @see ActivePhaseUtil
 * @generated
 */
public interface ActivePhasePersistence extends BasePersistence<ActivePhase> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ActivePhaseUtil} to access the active phase persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the active phase in the entity cache if it is enabled.
	*
	* @param activePhase the active phase
	*/
	public void cacheResult(com.aw.internal.model.ActivePhase activePhase);

	/**
	* Caches the active phases in the entity cache if it is enabled.
	*
	* @param activePhases the active phases
	*/
	public void cacheResult(
		java.util.List<com.aw.internal.model.ActivePhase> activePhases);

	/**
	* Creates a new active phase with the primary key. Does not add the active phase to the database.
	*
	* @param region the primary key for the new active phase
	* @return the new active phase
	*/
	public com.aw.internal.model.ActivePhase create(java.lang.String region);

	/**
	* Removes the active phase with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param region the primary key of the active phase
	* @return the active phase that was removed
	* @throws com.aw.internal.NoSuchActivePhaseException if a active phase with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.ActivePhase remove(java.lang.String region)
		throws com.aw.internal.NoSuchActivePhaseException,
			com.liferay.portal.kernel.exception.SystemException;

	public com.aw.internal.model.ActivePhase updateImpl(
		com.aw.internal.model.ActivePhase activePhase)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the active phase with the primary key or throws a {@link com.aw.internal.NoSuchActivePhaseException} if it could not be found.
	*
	* @param region the primary key of the active phase
	* @return the active phase
	* @throws com.aw.internal.NoSuchActivePhaseException if a active phase with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.ActivePhase findByPrimaryKey(
		java.lang.String region)
		throws com.aw.internal.NoSuchActivePhaseException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the active phase with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param region the primary key of the active phase
	* @return the active phase, or <code>null</code> if a active phase with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.ActivePhase fetchByPrimaryKey(
		java.lang.String region)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the active phases.
	*
	* @return the active phases
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.aw.internal.model.ActivePhase> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the active phases.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.ActivePhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of active phases
	* @param end the upper bound of the range of active phases (not inclusive)
	* @return the range of active phases
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.aw.internal.model.ActivePhase> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the active phases.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.ActivePhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of active phases
	* @param end the upper bound of the range of active phases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of active phases
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.aw.internal.model.ActivePhase> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the active phases from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of active phases.
	*
	* @return the number of active phases
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}