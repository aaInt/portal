/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service;

import com.aw.internal.model.AaContactClp;
import com.aw.internal.model.ActivePhaseClp;
import com.aw.internal.model.BrokerInfoClp;
import com.aw.internal.model.CertDocStoreClp;
import com.aw.internal.model.CoinCountClp;
import com.aw.internal.model.CoinOrderClp;
import com.aw.internal.model.CoinUserClp;
import com.aw.internal.model.FailReasonClp;
import com.aw.internal.model.FundInfoClp;
import com.aw.internal.model.FutureUserClp;
import com.aw.internal.model.ImageStoreClp;
import com.aw.internal.model.UnitPriceClp;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayInputStream;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayOutputStream;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ClassLoaderObjectInputStream;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.BaseModel;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.lang.reflect.Method;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zl
 */
public class ClpSerializer {
	public static String getServletContextName() {
		if (Validator.isNotNull(_servletContextName)) {
			return _servletContextName;
		}

		synchronized (ClpSerializer.class) {
			if (Validator.isNotNull(_servletContextName)) {
				return _servletContextName;
			}

			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Class<?> portletPropsClass = classLoader.loadClass(
						"com.liferay.util.portlet.PortletProps");

				Method getMethod = portletPropsClass.getMethod("get",
						new Class<?>[] { String.class });

				String portletPropsServletContextName = (String)getMethod.invoke(null,
						"AaPortal-portlet-deployment-context");

				if (Validator.isNotNull(portletPropsServletContextName)) {
					_servletContextName = portletPropsServletContextName;
				}
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info(
						"Unable to locate deployment context from portlet properties");
				}
			}

			if (Validator.isNull(_servletContextName)) {
				try {
					String propsUtilServletContextName = PropsUtil.get(
							"AaPortal-portlet-deployment-context");

					if (Validator.isNotNull(propsUtilServletContextName)) {
						_servletContextName = propsUtilServletContextName;
					}
				}
				catch (Throwable t) {
					if (_log.isInfoEnabled()) {
						_log.info(
							"Unable to locate deployment context from portal properties");
					}
				}
			}

			if (Validator.isNull(_servletContextName)) {
				_servletContextName = "AaPortal-portlet";
			}

			return _servletContextName;
		}
	}

	public static Object translateInput(BaseModel<?> oldModel) {
		Class<?> oldModelClass = oldModel.getClass();

		String oldModelClassName = oldModelClass.getName();

		if (oldModelClassName.equals(AaContactClp.class.getName())) {
			return translateInputAaContact(oldModel);
		}

		if (oldModelClassName.equals(ActivePhaseClp.class.getName())) {
			return translateInputActivePhase(oldModel);
		}

		if (oldModelClassName.equals(BrokerInfoClp.class.getName())) {
			return translateInputBrokerInfo(oldModel);
		}

		if (oldModelClassName.equals(CertDocStoreClp.class.getName())) {
			return translateInputCertDocStore(oldModel);
		}

		if (oldModelClassName.equals(CoinCountClp.class.getName())) {
			return translateInputCoinCount(oldModel);
		}

		if (oldModelClassName.equals(CoinOrderClp.class.getName())) {
			return translateInputCoinOrder(oldModel);
		}

		if (oldModelClassName.equals(CoinUserClp.class.getName())) {
			return translateInputCoinUser(oldModel);
		}

		if (oldModelClassName.equals(FailReasonClp.class.getName())) {
			return translateInputFailReason(oldModel);
		}

		if (oldModelClassName.equals(FundInfoClp.class.getName())) {
			return translateInputFundInfo(oldModel);
		}

		if (oldModelClassName.equals(FutureUserClp.class.getName())) {
			return translateInputFutureUser(oldModel);
		}

		if (oldModelClassName.equals(ImageStoreClp.class.getName())) {
			return translateInputImageStore(oldModel);
		}

		if (oldModelClassName.equals(UnitPriceClp.class.getName())) {
			return translateInputUnitPrice(oldModel);
		}

		return oldModel;
	}

	public static Object translateInput(List<Object> oldList) {
		List<Object> newList = new ArrayList<Object>(oldList.size());

		for (int i = 0; i < oldList.size(); i++) {
			Object curObj = oldList.get(i);

			newList.add(translateInput(curObj));
		}

		return newList;
	}

	public static Object translateInputAaContact(BaseModel<?> oldModel) {
		AaContactClp oldClpModel = (AaContactClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getAaContactRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputActivePhase(BaseModel<?> oldModel) {
		ActivePhaseClp oldClpModel = (ActivePhaseClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getActivePhaseRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputBrokerInfo(BaseModel<?> oldModel) {
		BrokerInfoClp oldClpModel = (BrokerInfoClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getBrokerInfoRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputCertDocStore(BaseModel<?> oldModel) {
		CertDocStoreClp oldClpModel = (CertDocStoreClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getCertDocStoreRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputCoinCount(BaseModel<?> oldModel) {
		CoinCountClp oldClpModel = (CoinCountClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getCoinCountRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputCoinOrder(BaseModel<?> oldModel) {
		CoinOrderClp oldClpModel = (CoinOrderClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getCoinOrderRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputCoinUser(BaseModel<?> oldModel) {
		CoinUserClp oldClpModel = (CoinUserClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getCoinUserRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputFailReason(BaseModel<?> oldModel) {
		FailReasonClp oldClpModel = (FailReasonClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getFailReasonRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputFundInfo(BaseModel<?> oldModel) {
		FundInfoClp oldClpModel = (FundInfoClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getFundInfoRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputFutureUser(BaseModel<?> oldModel) {
		FutureUserClp oldClpModel = (FutureUserClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getFutureUserRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputImageStore(BaseModel<?> oldModel) {
		ImageStoreClp oldClpModel = (ImageStoreClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getImageStoreRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputUnitPrice(BaseModel<?> oldModel) {
		UnitPriceClp oldClpModel = (UnitPriceClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getUnitPriceRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInput(Object obj) {
		if (obj instanceof BaseModel<?>) {
			return translateInput((BaseModel<?>)obj);
		}
		else if (obj instanceof List<?>) {
			return translateInput((List<Object>)obj);
		}
		else {
			return obj;
		}
	}

	public static Object translateOutput(BaseModel<?> oldModel) {
		Class<?> oldModelClass = oldModel.getClass();

		String oldModelClassName = oldModelClass.getName();

		if (oldModelClassName.equals("com.aw.internal.model.impl.AaContactImpl")) {
			return translateOutputAaContact(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"com.aw.internal.model.impl.ActivePhaseImpl")) {
			return translateOutputActivePhase(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"com.aw.internal.model.impl.BrokerInfoImpl")) {
			return translateOutputBrokerInfo(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"com.aw.internal.model.impl.CertDocStoreImpl")) {
			return translateOutputCertDocStore(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals("com.aw.internal.model.impl.CoinCountImpl")) {
			return translateOutputCoinCount(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals("com.aw.internal.model.impl.CoinOrderImpl")) {
			return translateOutputCoinOrder(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals("com.aw.internal.model.impl.CoinUserImpl")) {
			return translateOutputCoinUser(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"com.aw.internal.model.impl.FailReasonImpl")) {
			return translateOutputFailReason(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals("com.aw.internal.model.impl.FundInfoImpl")) {
			return translateOutputFundInfo(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"com.aw.internal.model.impl.FutureUserImpl")) {
			return translateOutputFutureUser(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"com.aw.internal.model.impl.ImageStoreImpl")) {
			return translateOutputImageStore(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals("com.aw.internal.model.impl.UnitPriceImpl")) {
			return translateOutputUnitPrice(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		return oldModel;
	}

	public static Object translateOutput(List<Object> oldList) {
		List<Object> newList = new ArrayList<Object>(oldList.size());

		for (int i = 0; i < oldList.size(); i++) {
			Object curObj = oldList.get(i);

			newList.add(translateOutput(curObj));
		}

		return newList;
	}

	public static Object translateOutput(Object obj) {
		if (obj instanceof BaseModel<?>) {
			return translateOutput((BaseModel<?>)obj);
		}
		else if (obj instanceof List<?>) {
			return translateOutput((List<Object>)obj);
		}
		else {
			return obj;
		}
	}

	public static Throwable translateThrowable(Throwable throwable) {
		if (_useReflectionToTranslateThrowable) {
			try {
				UnsyncByteArrayOutputStream unsyncByteArrayOutputStream = new UnsyncByteArrayOutputStream();
				ObjectOutputStream objectOutputStream = new ObjectOutputStream(unsyncByteArrayOutputStream);

				objectOutputStream.writeObject(throwable);

				objectOutputStream.flush();
				objectOutputStream.close();

				UnsyncByteArrayInputStream unsyncByteArrayInputStream = new UnsyncByteArrayInputStream(unsyncByteArrayOutputStream.unsafeGetByteArray(),
						0, unsyncByteArrayOutputStream.size());

				Thread currentThread = Thread.currentThread();

				ClassLoader contextClassLoader = currentThread.getContextClassLoader();

				ObjectInputStream objectInputStream = new ClassLoaderObjectInputStream(unsyncByteArrayInputStream,
						contextClassLoader);

				throwable = (Throwable)objectInputStream.readObject();

				objectInputStream.close();

				return throwable;
			}
			catch (SecurityException se) {
				if (_log.isInfoEnabled()) {
					_log.info("Do not use reflection to translate throwable");
				}

				_useReflectionToTranslateThrowable = false;
			}
			catch (Throwable throwable2) {
				_log.error(throwable2, throwable2);

				return throwable2;
			}
		}

		Class<?> clazz = throwable.getClass();

		String className = clazz.getName();

		if (className.equals(PortalException.class.getName())) {
			return new PortalException();
		}

		if (className.equals(SystemException.class.getName())) {
			return new SystemException();
		}

		if (className.equals("com.aw.internal.NoSuchAaContactException")) {
			return new com.aw.internal.NoSuchAaContactException();
		}

		if (className.equals("com.aw.internal.NoSuchActivePhaseException")) {
			return new com.aw.internal.NoSuchActivePhaseException();
		}

		if (className.equals("com.aw.internal.NoSuchBrokerInfoException")) {
			return new com.aw.internal.NoSuchBrokerInfoException();
		}

		if (className.equals("com.aw.internal.NoSuchCertDocStoreException")) {
			return new com.aw.internal.NoSuchCertDocStoreException();
		}

		if (className.equals("com.aw.internal.NoSuchCoinCountException")) {
			return new com.aw.internal.NoSuchCoinCountException();
		}

		if (className.equals("com.aw.internal.NoSuchCoinOrderException")) {
			return new com.aw.internal.NoSuchCoinOrderException();
		}

		if (className.equals("com.aw.internal.NoSuchCoinUserException")) {
			return new com.aw.internal.NoSuchCoinUserException();
		}

		if (className.equals("com.aw.internal.NoSuchFailReasonException")) {
			return new com.aw.internal.NoSuchFailReasonException();
		}

		if (className.equals("com.aw.internal.NoSuchFundInfoException")) {
			return new com.aw.internal.NoSuchFundInfoException();
		}

		if (className.equals("com.aw.internal.NoSuchFutureUserException")) {
			return new com.aw.internal.NoSuchFutureUserException();
		}

		if (className.equals("com.aw.internal.NoSuchImageStoreException")) {
			return new com.aw.internal.NoSuchImageStoreException();
		}

		if (className.equals("com.aw.internal.NoSuchUnitPriceException")) {
			return new com.aw.internal.NoSuchUnitPriceException();
		}

		return throwable;
	}

	public static Object translateOutputAaContact(BaseModel<?> oldModel) {
		AaContactClp newModel = new AaContactClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setAaContactRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputActivePhase(BaseModel<?> oldModel) {
		ActivePhaseClp newModel = new ActivePhaseClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setActivePhaseRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputBrokerInfo(BaseModel<?> oldModel) {
		BrokerInfoClp newModel = new BrokerInfoClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setBrokerInfoRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputCertDocStore(BaseModel<?> oldModel) {
		CertDocStoreClp newModel = new CertDocStoreClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setCertDocStoreRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputCoinCount(BaseModel<?> oldModel) {
		CoinCountClp newModel = new CoinCountClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setCoinCountRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputCoinOrder(BaseModel<?> oldModel) {
		CoinOrderClp newModel = new CoinOrderClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setCoinOrderRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputCoinUser(BaseModel<?> oldModel) {
		CoinUserClp newModel = new CoinUserClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setCoinUserRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputFailReason(BaseModel<?> oldModel) {
		FailReasonClp newModel = new FailReasonClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setFailReasonRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputFundInfo(BaseModel<?> oldModel) {
		FundInfoClp newModel = new FundInfoClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setFundInfoRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputFutureUser(BaseModel<?> oldModel) {
		FutureUserClp newModel = new FutureUserClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setFutureUserRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputImageStore(BaseModel<?> oldModel) {
		ImageStoreClp newModel = new ImageStoreClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setImageStoreRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputUnitPrice(BaseModel<?> oldModel) {
		UnitPriceClp newModel = new UnitPriceClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setUnitPriceRemoteModel(oldModel);

		return newModel;
	}

	private static Log _log = LogFactoryUtil.getLog(ClpSerializer.class);
	private static String _servletContextName;
	private static boolean _useReflectionToTranslateThrowable = true;
}