/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link AaContactLocalService}.
 *
 * @author zl
 * @see AaContactLocalService
 * @generated
 */
public class AaContactLocalServiceWrapper implements AaContactLocalService,
	ServiceWrapper<AaContactLocalService> {
	public AaContactLocalServiceWrapper(
		AaContactLocalService aaContactLocalService) {
		_aaContactLocalService = aaContactLocalService;
	}

	/**
	* Adds the aa contact to the database. Also notifies the appropriate model listeners.
	*
	* @param aaContact the aa contact
	* @return the aa contact that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.AaContact addAaContact(
		com.aw.internal.model.AaContact aaContact)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _aaContactLocalService.addAaContact(aaContact);
	}

	/**
	* Creates a new aa contact with the primary key. Does not add the aa contact to the database.
	*
	* @param contactId the primary key for the new aa contact
	* @return the new aa contact
	*/
	@Override
	public com.aw.internal.model.AaContact createAaContact(long contactId) {
		return _aaContactLocalService.createAaContact(contactId);
	}

	/**
	* Deletes the aa contact with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param contactId the primary key of the aa contact
	* @return the aa contact that was removed
	* @throws PortalException if a aa contact with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.AaContact deleteAaContact(long contactId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _aaContactLocalService.deleteAaContact(contactId);
	}

	/**
	* Deletes the aa contact from the database. Also notifies the appropriate model listeners.
	*
	* @param aaContact the aa contact
	* @return the aa contact that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.AaContact deleteAaContact(
		com.aw.internal.model.AaContact aaContact)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _aaContactLocalService.deleteAaContact(aaContact);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _aaContactLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _aaContactLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.AaContactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _aaContactLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.AaContactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _aaContactLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _aaContactLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _aaContactLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public com.aw.internal.model.AaContact fetchAaContact(long contactId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _aaContactLocalService.fetchAaContact(contactId);
	}

	/**
	* Returns the aa contact with the primary key.
	*
	* @param contactId the primary key of the aa contact
	* @return the aa contact
	* @throws PortalException if a aa contact with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.AaContact getAaContact(long contactId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _aaContactLocalService.getAaContact(contactId);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _aaContactLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the aa contacts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.AaContactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of aa contacts
	* @param end the upper bound of the range of aa contacts (not inclusive)
	* @return the range of aa contacts
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<com.aw.internal.model.AaContact> getAaContacts(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _aaContactLocalService.getAaContacts(start, end);
	}

	/**
	* Returns the number of aa contacts.
	*
	* @return the number of aa contacts
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getAaContactsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _aaContactLocalService.getAaContactsCount();
	}

	/**
	* Updates the aa contact in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param aaContact the aa contact
	* @return the aa contact that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.AaContact updateAaContact(
		com.aw.internal.model.AaContact aaContact)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _aaContactLocalService.updateAaContact(aaContact);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _aaContactLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_aaContactLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _aaContactLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	@Override
	public boolean createContact(long id, long phone, int areaCode,
		java.lang.String email, java.lang.String wechat,
		java.lang.String facebook, java.lang.String twitter,
		java.lang.String weibo, java.lang.String street1,
		java.lang.String street2, java.lang.String city,
		java.lang.String state, java.lang.String country,
		java.lang.String zipCode, java.lang.String isMailDiff,
		java.lang.String mailStreet1, java.lang.String mailStreet2,
		java.lang.String mailCity, java.lang.String mailState,
		java.lang.String mailCountry, java.lang.String mailZipCode) {
		return _aaContactLocalService.createContact(id, phone, areaCode, email,
			wechat, facebook, twitter, weibo, street1, street2, city, state,
			country, zipCode, isMailDiff, mailStreet1, mailStreet2, mailCity,
			mailState, mailCountry, mailZipCode);
	}

	@Override
	public boolean updateContact(long id, long phone, int areaCode,
		java.lang.String email, java.lang.String wechat,
		java.lang.String facebook, java.lang.String twitter,
		java.lang.String weibo, java.lang.String street1,
		java.lang.String street2, java.lang.String city,
		java.lang.String state, java.lang.String country,
		java.lang.String zipCode, java.lang.String isMailDiff,
		java.lang.String mailStreet1, java.lang.String mailStreet2,
		java.lang.String mailCity, java.lang.String mailState,
		java.lang.String mailCountry, java.lang.String mailZipCode) {
		return _aaContactLocalService.updateContact(id, phone, areaCode, email,
			wechat, facebook, twitter, weibo, street1, street2, city, state,
			country, zipCode, isMailDiff, mailStreet1, mailStreet2, mailCity,
			mailState, mailCountry, mailZipCode);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getContact(long contactId) {
		return _aaContactLocalService.getContact(contactId);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public AaContactLocalService getWrappedAaContactLocalService() {
		return _aaContactLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedAaContactLocalService(
		AaContactLocalService aaContactLocalService) {
		_aaContactLocalService = aaContactLocalService;
	}

	@Override
	public AaContactLocalService getWrappedService() {
		return _aaContactLocalService;
	}

	@Override
	public void setWrappedService(AaContactLocalService aaContactLocalService) {
		_aaContactLocalService = aaContactLocalService;
	}

	private AaContactLocalService _aaContactLocalService;
}