/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for FundInfo. This utility wraps
 * {@link com.aw.internal.service.impl.FundInfoLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author zl
 * @see FundInfoLocalService
 * @see com.aw.internal.service.base.FundInfoLocalServiceBaseImpl
 * @see com.aw.internal.service.impl.FundInfoLocalServiceImpl
 * @generated
 */
public class FundInfoLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.aw.internal.service.impl.FundInfoLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the fund info to the database. Also notifies the appropriate model listeners.
	*
	* @param fundInfo the fund info
	* @return the fund info that was added
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.FundInfo addFundInfo(
		com.aw.internal.model.FundInfo fundInfo)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addFundInfo(fundInfo);
	}

	/**
	* Creates a new fund info with the primary key. Does not add the fund info to the database.
	*
	* @param fundId the primary key for the new fund info
	* @return the new fund info
	*/
	public static com.aw.internal.model.FundInfo createFundInfo(long fundId) {
		return getService().createFundInfo(fundId);
	}

	/**
	* Deletes the fund info with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param fundId the primary key of the fund info
	* @return the fund info that was removed
	* @throws PortalException if a fund info with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.FundInfo deleteFundInfo(long fundId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteFundInfo(fundId);
	}

	/**
	* Deletes the fund info from the database. Also notifies the appropriate model listeners.
	*
	* @param fundInfo the fund info
	* @return the fund info that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.FundInfo deleteFundInfo(
		com.aw.internal.model.FundInfo fundInfo)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteFundInfo(fundInfo);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.FundInfoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.FundInfoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static com.aw.internal.model.FundInfo fetchFundInfo(long fundId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchFundInfo(fundId);
	}

	/**
	* Returns the fund info with the primary key.
	*
	* @param fundId the primary key of the fund info
	* @return the fund info
	* @throws PortalException if a fund info with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.FundInfo getFundInfo(long fundId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getFundInfo(fundId);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the fund infos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.FundInfoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of fund infos
	* @param end the upper bound of the range of fund infos (not inclusive)
	* @return the range of fund infos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.aw.internal.model.FundInfo> getFundInfos(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getFundInfos(start, end);
	}

	/**
	* Returns the number of fund infos.
	*
	* @return the number of fund infos
	* @throws SystemException if a system exception occurred
	*/
	public static int getFundInfosCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getFundInfosCount();
	}

	/**
	* Updates the fund info in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param fundInfo the fund info
	* @return the fund info that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static com.aw.internal.model.FundInfo updateFundInfo(
		com.aw.internal.model.FundInfo fundInfo)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateFundInfo(fundInfo);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static com.liferay.portal.kernel.json.JSONObject addFund(
		java.lang.String name, long phone, int areaCode,
		java.lang.String email, java.lang.String wechat,
		java.lang.String facebook, java.lang.String twitter,
		java.lang.String weibo, java.lang.String street1,
		java.lang.String street2, java.lang.String city,
		java.lang.String state, java.lang.String country,
		java.lang.String zipCode) {
		return getService()
				   .addFund(name, phone, areaCode, email, wechat, facebook,
			twitter, weibo, street1, street2, city, state, country, zipCode);
	}

	public static com.aw.internal.model.FundInfo getFund(long fundId) {
		return getService().getFund(fundId);
	}

	public static java.lang.String updateFund() {
		return getService().updateFund();
	}

	public static boolean verifyFund(long fundId) {
		return getService().verifyFund(fundId);
	}

	public static void clearService() {
		_service = null;
	}

	public static FundInfoLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					FundInfoLocalService.class.getName());

			if (invokableLocalService instanceof FundInfoLocalService) {
				_service = (FundInfoLocalService)invokableLocalService;
			}
			else {
				_service = new FundInfoLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(FundInfoLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(FundInfoLocalService service) {
	}

	private static FundInfoLocalService _service;
}