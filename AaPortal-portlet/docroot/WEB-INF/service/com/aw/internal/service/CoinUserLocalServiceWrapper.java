/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CoinUserLocalService}.
 *
 * @author zl
 * @see CoinUserLocalService
 * @generated
 */
public class CoinUserLocalServiceWrapper implements CoinUserLocalService,
	ServiceWrapper<CoinUserLocalService> {
	public CoinUserLocalServiceWrapper(
		CoinUserLocalService coinUserLocalService) {
		_coinUserLocalService = coinUserLocalService;
	}

	/**
	* Adds the coin user to the database. Also notifies the appropriate model listeners.
	*
	* @param coinUser the coin user
	* @return the coin user that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.CoinUser addCoinUser(
		com.aw.internal.model.CoinUser coinUser)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _coinUserLocalService.addCoinUser(coinUser);
	}

	/**
	* Creates a new coin user with the primary key. Does not add the coin user to the database.
	*
	* @param userId the primary key for the new coin user
	* @return the new coin user
	*/
	@Override
	public com.aw.internal.model.CoinUser createCoinUser(
		java.lang.String userId) {
		return _coinUserLocalService.createCoinUser(userId);
	}

	/**
	* Deletes the coin user with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userId the primary key of the coin user
	* @return the coin user that was removed
	* @throws PortalException if a coin user with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.CoinUser deleteCoinUser(
		java.lang.String userId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _coinUserLocalService.deleteCoinUser(userId);
	}

	/**
	* Deletes the coin user from the database. Also notifies the appropriate model listeners.
	*
	* @param coinUser the coin user
	* @return the coin user that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.CoinUser deleteCoinUser(
		com.aw.internal.model.CoinUser coinUser)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _coinUserLocalService.deleteCoinUser(coinUser);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _coinUserLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _coinUserLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _coinUserLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _coinUserLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _coinUserLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _coinUserLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public com.aw.internal.model.CoinUser fetchCoinUser(java.lang.String userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _coinUserLocalService.fetchCoinUser(userId);
	}

	/**
	* Returns the coin user with the primary key.
	*
	* @param userId the primary key of the coin user
	* @return the coin user
	* @throws PortalException if a coin user with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.CoinUser getCoinUser(java.lang.String userId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _coinUserLocalService.getCoinUser(userId);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _coinUserLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the coin users.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of coin users
	* @param end the upper bound of the range of coin users (not inclusive)
	* @return the range of coin users
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<com.aw.internal.model.CoinUser> getCoinUsers(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _coinUserLocalService.getCoinUsers(start, end);
	}

	/**
	* Returns the number of coin users.
	*
	* @return the number of coin users
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getCoinUsersCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _coinUserLocalService.getCoinUsersCount();
	}

	/**
	* Updates the coin user in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param coinUser the coin user
	* @return the coin user that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.CoinUser updateCoinUser(
		com.aw.internal.model.CoinUser coinUser)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _coinUserLocalService.updateCoinUser(coinUser);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _coinUserLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_coinUserLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _coinUserLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject addCoinUser(long creater,
		java.lang.String firstName, java.lang.String lastName,
		java.lang.String birthday, java.lang.String userType,
		java.lang.String brkLics, java.lang.String brkCountry, long fundId,
		java.lang.String nationality, java.lang.String idIssueCountry,
		java.lang.String idType, java.lang.String idNumber,
		java.lang.String photoImage, java.lang.String certDocStr,
		java.lang.String ssn, java.lang.String ein, java.lang.String income,
		java.lang.String asset, long phone, int areaCode,
		java.lang.String email, java.lang.String wechat,
		java.lang.String facebook, java.lang.String twitter,
		java.lang.String weibo, java.lang.String street1,
		java.lang.String street2, java.lang.String city,
		java.lang.String state, java.lang.String country,
		java.lang.String zipCode, java.lang.String isMailDiff,
		java.lang.String mailStreet1, java.lang.String mailStreet2,
		java.lang.String mailCity, java.lang.String mailState,
		java.lang.String mailCountry, java.lang.String mailZipCode) {
		return _coinUserLocalService.addCoinUser(creater, firstName, lastName,
			birthday, userType, brkLics, brkCountry, fundId, nationality,
			idIssueCountry, idType, idNumber, photoImage, certDocStr, ssn, ein,
			income, asset, phone, areaCode, email, wechat, facebook, twitter,
			weibo, street1, street2, city, state, country, zipCode, isMailDiff,
			mailStreet1, mailStreet2, mailCity, mailState, mailCountry,
			mailZipCode);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject updateCoinUser(
		long userId, java.lang.String coinUserId, java.lang.String firstName,
		java.lang.String lastName, java.lang.String birthday,
		java.lang.String userType, java.lang.String brkLics,
		java.lang.String brkCountry, long fundId, java.lang.String nationality,
		java.lang.String idIssueCountry, java.lang.String idType,
		java.lang.String idNumber, java.lang.String ssn, java.lang.String ein,
		java.lang.String income, java.lang.String asset, long phone,
		int areaCode, java.lang.String email, java.lang.String wechat,
		java.lang.String facebook, java.lang.String twitter,
		java.lang.String weibo, java.lang.String street1,
		java.lang.String street2, java.lang.String city,
		java.lang.String state, java.lang.String country,
		java.lang.String zipCode, java.lang.String isMailDiff,
		java.lang.String mailStreet1, java.lang.String mailStreet2,
		java.lang.String mailCity, java.lang.String mailState,
		java.lang.String mailCountry, java.lang.String mailZipCode) {
		return _coinUserLocalService.updateCoinUser(userId, coinUserId,
			firstName, lastName, birthday, userType, brkLics, brkCountry,
			fundId, nationality, idIssueCountry, idType, idNumber, ssn, ein,
			income, asset, phone, areaCode, email, wechat, facebook, twitter,
			weibo, street1, street2, city, state, country, zipCode, isMailDiff,
			mailStreet1, mailStreet2, mailCity, mailState, mailCountry,
			mailZipCode);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject updatePhoto(long userId,
		java.lang.String coinuserId, java.lang.String photoStr) {
		return _coinUserLocalService.updatePhoto(userId, coinuserId, photoStr);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject updateDoc(long userId,
		java.lang.String coinuserId, java.lang.String docStr) {
		return _coinUserLocalService.updateDoc(userId, coinuserId, docStr);
	}

	@Override
	public boolean deactivateSaleUser(long userId, java.lang.String id) {
		return _coinUserLocalService.deactivateSaleUser(userId, id);
	}

	@Override
	public boolean deactivateUser(java.lang.String id) {
		return _coinUserLocalService.deactivateUser(id);
	}

	@Override
	public boolean verifyAcredit(java.lang.String id) {
		return _coinUserLocalService.verifyAcredit(id);
	}

	@Override
	public boolean pendAcredit(java.lang.String id) {
		return _coinUserLocalService.pendAcredit(id);
	}

	@Override
	public boolean checkValidtion(java.lang.String id) {
		return _coinUserLocalService.checkValidtion(id);
	}

	@Override
	public boolean validateUser(java.lang.String id, long userId) {
		return _coinUserLocalService.validateUser(id, userId);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public CoinUserLocalService getWrappedCoinUserLocalService() {
		return _coinUserLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedCoinUserLocalService(
		CoinUserLocalService coinUserLocalService) {
		_coinUserLocalService = coinUserLocalService;
	}

	@Override
	public CoinUserLocalService getWrappedService() {
		return _coinUserLocalService;
	}

	@Override
	public void setWrappedService(CoinUserLocalService coinUserLocalService) {
		_coinUserLocalService = coinUserLocalService;
	}

	private CoinUserLocalService _coinUserLocalService;
}