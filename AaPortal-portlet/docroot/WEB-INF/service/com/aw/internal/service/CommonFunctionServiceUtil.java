/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableService;

/**
 * Provides the remote service utility for CommonFunction. This utility wraps
 * {@link com.aw.internal.service.impl.CommonFunctionServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on a remote server. Methods of this service are expected to have security
 * checks based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author zl
 * @see CommonFunctionService
 * @see com.aw.internal.service.base.CommonFunctionServiceBaseImpl
 * @see com.aw.internal.service.impl.CommonFunctionServiceImpl
 * @generated
 */
public class CommonFunctionServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.aw.internal.service.impl.CommonFunctionServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static boolean checkPassword(java.lang.String password) {
		return getService().checkPassword(password);
	}

	public static com.liferay.portal.kernel.json.JSONObject getCoinCounts() {
		return getService().getCoinCounts();
	}

	public static boolean endCoinCountPhase(int endPhase, int toPhase) {
		return getService().endCoinCountPhase(endPhase, toPhase);
	}

	public static com.liferay.portal.kernel.json.JSONObject getPhasePrice(
		java.lang.String currency, java.lang.String userId) {
		return getService().getPhasePrice(currency, userId);
	}

	public static com.liferay.portal.kernel.json.JSONObject getUserInfo() {
		return getService().getUserInfo();
	}

	public static boolean updatePassword(java.lang.String origin,
		java.lang.String new1, java.lang.String new2) {
		return getService().updatePassword(origin, new1, new2);
	}

	public static void clearService() {
		_service = null;
	}

	public static CommonFunctionService getService() {
		if (_service == null) {
			InvokableService invokableService = (InvokableService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					CommonFunctionService.class.getName());

			if (invokableService instanceof CommonFunctionService) {
				_service = (CommonFunctionService)invokableService;
			}
			else {
				_service = new CommonFunctionServiceClp(invokableService);
			}

			ReferenceRegistry.registerReference(CommonFunctionServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(CommonFunctionService service) {
	}

	private static CommonFunctionService _service;
}