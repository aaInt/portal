/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link FailReasonLocalService}.
 *
 * @author zl
 * @see FailReasonLocalService
 * @generated
 */
public class FailReasonLocalServiceWrapper implements FailReasonLocalService,
	ServiceWrapper<FailReasonLocalService> {
	public FailReasonLocalServiceWrapper(
		FailReasonLocalService failReasonLocalService) {
		_failReasonLocalService = failReasonLocalService;
	}

	/**
	* Adds the fail reason to the database. Also notifies the appropriate model listeners.
	*
	* @param failReason the fail reason
	* @return the fail reason that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.FailReason addFailReason(
		com.aw.internal.model.FailReason failReason)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _failReasonLocalService.addFailReason(failReason);
	}

	/**
	* Creates a new fail reason with the primary key. Does not add the fail reason to the database.
	*
	* @param rsnCd the primary key for the new fail reason
	* @return the new fail reason
	*/
	@Override
	public com.aw.internal.model.FailReason createFailReason(
		java.lang.String rsnCd) {
		return _failReasonLocalService.createFailReason(rsnCd);
	}

	/**
	* Deletes the fail reason with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param rsnCd the primary key of the fail reason
	* @return the fail reason that was removed
	* @throws PortalException if a fail reason with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.FailReason deleteFailReason(
		java.lang.String rsnCd)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _failReasonLocalService.deleteFailReason(rsnCd);
	}

	/**
	* Deletes the fail reason from the database. Also notifies the appropriate model listeners.
	*
	* @param failReason the fail reason
	* @return the fail reason that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.FailReason deleteFailReason(
		com.aw.internal.model.FailReason failReason)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _failReasonLocalService.deleteFailReason(failReason);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _failReasonLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _failReasonLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.FailReasonModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _failReasonLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.FailReasonModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _failReasonLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _failReasonLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _failReasonLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public com.aw.internal.model.FailReason fetchFailReason(
		java.lang.String rsnCd)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _failReasonLocalService.fetchFailReason(rsnCd);
	}

	/**
	* Returns the fail reason with the primary key.
	*
	* @param rsnCd the primary key of the fail reason
	* @return the fail reason
	* @throws PortalException if a fail reason with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.FailReason getFailReason(
		java.lang.String rsnCd)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _failReasonLocalService.getFailReason(rsnCd);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _failReasonLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the fail reasons.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.FailReasonModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of fail reasons
	* @param end the upper bound of the range of fail reasons (not inclusive)
	* @return the range of fail reasons
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<com.aw.internal.model.FailReason> getFailReasons(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _failReasonLocalService.getFailReasons(start, end);
	}

	/**
	* Returns the number of fail reasons.
	*
	* @return the number of fail reasons
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getFailReasonsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _failReasonLocalService.getFailReasonsCount();
	}

	/**
	* Updates the fail reason in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param failReason the fail reason
	* @return the fail reason that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.FailReason updateFailReason(
		com.aw.internal.model.FailReason failReason)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _failReasonLocalService.updateFailReason(failReason);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _failReasonLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_failReasonLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _failReasonLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject addReason(
		java.lang.String rsnCd, java.lang.String desc) {
		return _failReasonLocalService.addReason(rsnCd, desc);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getReason(
		java.lang.String rsnCd) {
		return _failReasonLocalService.getReason(rsnCd);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getReasons() {
		return _failReasonLocalService.getReasons();
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public FailReasonLocalService getWrappedFailReasonLocalService() {
		return _failReasonLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedFailReasonLocalService(
		FailReasonLocalService failReasonLocalService) {
		_failReasonLocalService = failReasonLocalService;
	}

	@Override
	public FailReasonLocalService getWrappedService() {
		return _failReasonLocalService;
	}

	@Override
	public void setWrappedService(FailReasonLocalService failReasonLocalService) {
		_failReasonLocalService = failReasonLocalService;
	}

	private FailReasonLocalService _failReasonLocalService;
}