/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.persistence;

import com.aw.internal.model.CertDocStore;
import com.aw.internal.service.CertDocStoreLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author zl
 * @generated
 */
public abstract class CertDocStoreActionableDynamicQuery
	extends BaseActionableDynamicQuery {
	public CertDocStoreActionableDynamicQuery() throws SystemException {
		setBaseLocalService(CertDocStoreLocalServiceUtil.getService());
		setClass(CertDocStore.class);

		setClassLoader(com.aw.internal.service.ClpSerializer.class.getClassLoader());

		setPrimaryKeyPropertyName("docId");
	}
}