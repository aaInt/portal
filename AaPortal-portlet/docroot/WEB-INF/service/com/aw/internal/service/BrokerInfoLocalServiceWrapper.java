/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link BrokerInfoLocalService}.
 *
 * @author zl
 * @see BrokerInfoLocalService
 * @generated
 */
public class BrokerInfoLocalServiceWrapper implements BrokerInfoLocalService,
	ServiceWrapper<BrokerInfoLocalService> {
	public BrokerInfoLocalServiceWrapper(
		BrokerInfoLocalService brokerInfoLocalService) {
		_brokerInfoLocalService = brokerInfoLocalService;
	}

	/**
	* Adds the broker info to the database. Also notifies the appropriate model listeners.
	*
	* @param brokerInfo the broker info
	* @return the broker info that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.BrokerInfo addBrokerInfo(
		com.aw.internal.model.BrokerInfo brokerInfo)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _brokerInfoLocalService.addBrokerInfo(brokerInfo);
	}

	/**
	* Creates a new broker info with the primary key. Does not add the broker info to the database.
	*
	* @param brokerId the primary key for the new broker info
	* @return the new broker info
	*/
	@Override
	public com.aw.internal.model.BrokerInfo createBrokerInfo(long brokerId) {
		return _brokerInfoLocalService.createBrokerInfo(brokerId);
	}

	/**
	* Deletes the broker info with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param brokerId the primary key of the broker info
	* @return the broker info that was removed
	* @throws PortalException if a broker info with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.BrokerInfo deleteBrokerInfo(long brokerId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _brokerInfoLocalService.deleteBrokerInfo(brokerId);
	}

	/**
	* Deletes the broker info from the database. Also notifies the appropriate model listeners.
	*
	* @param brokerInfo the broker info
	* @return the broker info that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.BrokerInfo deleteBrokerInfo(
		com.aw.internal.model.BrokerInfo brokerInfo)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _brokerInfoLocalService.deleteBrokerInfo(brokerInfo);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _brokerInfoLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _brokerInfoLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.BrokerInfoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _brokerInfoLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.BrokerInfoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _brokerInfoLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _brokerInfoLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _brokerInfoLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public com.aw.internal.model.BrokerInfo fetchBrokerInfo(long brokerId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _brokerInfoLocalService.fetchBrokerInfo(brokerId);
	}

	/**
	* Returns the broker info with the primary key.
	*
	* @param brokerId the primary key of the broker info
	* @return the broker info
	* @throws PortalException if a broker info with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.BrokerInfo getBrokerInfo(long brokerId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _brokerInfoLocalService.getBrokerInfo(brokerId);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _brokerInfoLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the broker infos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.BrokerInfoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of broker infos
	* @param end the upper bound of the range of broker infos (not inclusive)
	* @return the range of broker infos
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<com.aw.internal.model.BrokerInfo> getBrokerInfos(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _brokerInfoLocalService.getBrokerInfos(start, end);
	}

	/**
	* Returns the number of broker infos.
	*
	* @return the number of broker infos
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getBrokerInfosCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _brokerInfoLocalService.getBrokerInfosCount();
	}

	/**
	* Updates the broker info in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param brokerInfo the broker info
	* @return the broker info that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.BrokerInfo updateBrokerInfo(
		com.aw.internal.model.BrokerInfo brokerInfo)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _brokerInfoLocalService.updateBrokerInfo(brokerInfo);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _brokerInfoLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_brokerInfoLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _brokerInfoLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	@Override
	public long addBroker(java.lang.String lics, java.lang.String country) {
		return _brokerInfoLocalService.addBroker(lics, country);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getBroker(long id) {
		return _brokerInfoLocalService.getBroker(id);
	}

	@Override
	public java.lang.String updatedBroker() {
		return _brokerInfoLocalService.updatedBroker();
	}

	@Override
	public boolean validateBroker(long brokerId) {
		return _brokerInfoLocalService.validateBroker(brokerId);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public BrokerInfoLocalService getWrappedBrokerInfoLocalService() {
		return _brokerInfoLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedBrokerInfoLocalService(
		BrokerInfoLocalService brokerInfoLocalService) {
		_brokerInfoLocalService = brokerInfoLocalService;
	}

	@Override
	public BrokerInfoLocalService getWrappedService() {
		return _brokerInfoLocalService;
	}

	@Override
	public void setWrappedService(BrokerInfoLocalService brokerInfoLocalService) {
		_brokerInfoLocalService = brokerInfoLocalService;
	}

	private BrokerInfoLocalService _brokerInfoLocalService;
}