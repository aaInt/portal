/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.jsonwebservice.JSONWebService;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.security.ac.AccessControlled;
import com.liferay.portal.service.BaseService;
import com.liferay.portal.service.InvokableService;

/**
 * Provides the remote service interface for CoinOrder. Methods of this
 * service are expected to have security checks based on the propagated JAAS
 * credentials because this service can be accessed remotely.
 *
 * @author zl
 * @see CoinOrderServiceUtil
 * @see com.aw.internal.service.base.CoinOrderServiceBaseImpl
 * @see com.aw.internal.service.impl.CoinOrderServiceImpl
 * @generated
 */
@AccessControlled
@JSONWebService
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
	PortalException.class, SystemException.class})
public interface CoinOrderService extends BaseService, InvokableService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CoinOrderServiceUtil} to access the coin order remote service. Add custom service methods to {@link com.aw.internal.service.impl.CoinOrderServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier();

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier);

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable;

	@com.liferay.portal.security.ac.AccessControlled(guestAccessEnabled = true)
	public com.liferay.portal.kernel.json.JSONObject verifyOrderInfo(
		java.lang.String orderId, java.lang.String userId,
		java.lang.String lastName);

	public com.liferay.portal.kernel.json.JSONObject createCoinOrder(
		java.lang.String userId, long brokerId, int phase, double unitPrice,
		double totalPrice, java.lang.String currency, long saleAmt,
		long promoAmt);

	public com.liferay.portal.kernel.json.JSONObject splitOrder(
		java.lang.String originOrderId, long salesId, java.lang.String userId,
		long amount) throws com.liferay.portal.kernel.exception.SystemException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.liferay.portal.kernel.json.JSONObject getPedingOrder();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.liferay.portal.kernel.json.JSONObject getStaffOrders();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.liferay.portal.kernel.json.JSONObject getCompletedOrders();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.liferay.portal.kernel.json.JSONObject getFailedOrder();

	public com.liferay.portal.kernel.json.JSONObject verified(
		java.lang.String orderId);

	public com.liferay.portal.kernel.json.JSONObject contractSent(
		java.lang.String orderId, java.lang.String carrier,
		java.lang.String trackingNo);

	public com.liferay.portal.kernel.json.JSONObject contractSigned(
		java.lang.String orderId);

	public com.liferay.portal.kernel.json.JSONObject paid(
		java.lang.String orderId);

	public com.liferay.portal.kernel.json.JSONObject approve(
		java.lang.String orderId);

	public com.liferay.portal.kernel.json.JSONObject certSent(
		java.lang.String orderId, java.lang.String carrier,
		java.lang.String trackingNo);

	public com.liferay.portal.kernel.json.JSONObject complete(
		java.lang.String orderId);

	public com.liferay.portal.kernel.json.JSONObject fail(
		java.lang.String orderId, java.lang.String rsnCd);

	public com.liferay.portal.kernel.json.JSONObject addReason(
		java.lang.String rsnCd, java.lang.String desc);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.liferay.portal.kernel.json.JSONObject getReason(
		java.lang.String rsnCd);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.liferay.portal.kernel.json.JSONObject getReasons();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.liferay.portal.kernel.json.JSONObject getSaleSummary(
		boolean isPaid, boolean isPending, int phase,
		java.lang.String startDate, java.lang.String endDate);
}