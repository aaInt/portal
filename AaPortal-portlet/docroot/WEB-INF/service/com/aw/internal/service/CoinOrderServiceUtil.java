/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableService;

/**
 * Provides the remote service utility for CoinOrder. This utility wraps
 * {@link com.aw.internal.service.impl.CoinOrderServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on a remote server. Methods of this service are expected to have security
 * checks based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author zl
 * @see CoinOrderService
 * @see com.aw.internal.service.base.CoinOrderServiceBaseImpl
 * @see com.aw.internal.service.impl.CoinOrderServiceImpl
 * @generated
 */
public class CoinOrderServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.aw.internal.service.impl.CoinOrderServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static com.liferay.portal.kernel.json.JSONObject verifyOrderInfo(
		java.lang.String orderId, java.lang.String userId,
		java.lang.String lastName) {
		return getService().verifyOrderInfo(orderId, userId, lastName);
	}

	public static com.liferay.portal.kernel.json.JSONObject createCoinOrder(
		java.lang.String userId, long brokerId, int phase, double unitPrice,
		double totalPrice, java.lang.String currency, long saleAmt,
		long promoAmt) {
		return getService()
				   .createCoinOrder(userId, brokerId, phase, unitPrice,
			totalPrice, currency, saleAmt, promoAmt);
	}

	public static com.liferay.portal.kernel.json.JSONObject splitOrder(
		java.lang.String originOrderId, long salesId, java.lang.String userId,
		long amount) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().splitOrder(originOrderId, salesId, userId, amount);
	}

	public static com.liferay.portal.kernel.json.JSONObject getPedingOrder() {
		return getService().getPedingOrder();
	}

	public static com.liferay.portal.kernel.json.JSONObject getStaffOrders() {
		return getService().getStaffOrders();
	}

	public static com.liferay.portal.kernel.json.JSONObject getCompletedOrders() {
		return getService().getCompletedOrders();
	}

	public static com.liferay.portal.kernel.json.JSONObject getFailedOrder() {
		return getService().getFailedOrder();
	}

	public static com.liferay.portal.kernel.json.JSONObject verified(
		java.lang.String orderId) {
		return getService().verified(orderId);
	}

	public static com.liferay.portal.kernel.json.JSONObject contractSent(
		java.lang.String orderId, java.lang.String carrier,
		java.lang.String trackingNo) {
		return getService().contractSent(orderId, carrier, trackingNo);
	}

	public static com.liferay.portal.kernel.json.JSONObject contractSigned(
		java.lang.String orderId) {
		return getService().contractSigned(orderId);
	}

	public static com.liferay.portal.kernel.json.JSONObject paid(
		java.lang.String orderId) {
		return getService().paid(orderId);
	}

	public static com.liferay.portal.kernel.json.JSONObject approve(
		java.lang.String orderId) {
		return getService().approve(orderId);
	}

	public static com.liferay.portal.kernel.json.JSONObject certSent(
		java.lang.String orderId, java.lang.String carrier,
		java.lang.String trackingNo) {
		return getService().certSent(orderId, carrier, trackingNo);
	}

	public static com.liferay.portal.kernel.json.JSONObject complete(
		java.lang.String orderId) {
		return getService().complete(orderId);
	}

	public static com.liferay.portal.kernel.json.JSONObject fail(
		java.lang.String orderId, java.lang.String rsnCd) {
		return getService().fail(orderId, rsnCd);
	}

	public static com.liferay.portal.kernel.json.JSONObject addReason(
		java.lang.String rsnCd, java.lang.String desc) {
		return getService().addReason(rsnCd, desc);
	}

	public static com.liferay.portal.kernel.json.JSONObject getReason(
		java.lang.String rsnCd) {
		return getService().getReason(rsnCd);
	}

	public static com.liferay.portal.kernel.json.JSONObject getReasons() {
		return getService().getReasons();
	}

	public static com.liferay.portal.kernel.json.JSONObject getSaleSummary(
		boolean isPaid, boolean isPending, int phase,
		java.lang.String startDate, java.lang.String endDate) {
		return getService()
				   .getSaleSummary(isPaid, isPending, phase, startDate, endDate);
	}

	public static void clearService() {
		_service = null;
	}

	public static CoinOrderService getService() {
		if (_service == null) {
			InvokableService invokableService = (InvokableService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					CoinOrderService.class.getName());

			if (invokableService instanceof CoinOrderService) {
				_service = (CoinOrderService)invokableService;
			}
			else {
				_service = new CoinOrderServiceClp(invokableService);
			}

			ReferenceRegistry.registerReference(CoinOrderServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(CoinOrderService service) {
	}

	private static CoinOrderService _service;
}