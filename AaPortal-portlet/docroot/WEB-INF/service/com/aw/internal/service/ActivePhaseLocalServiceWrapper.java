/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ActivePhaseLocalService}.
 *
 * @author zl
 * @see ActivePhaseLocalService
 * @generated
 */
public class ActivePhaseLocalServiceWrapper implements ActivePhaseLocalService,
	ServiceWrapper<ActivePhaseLocalService> {
	public ActivePhaseLocalServiceWrapper(
		ActivePhaseLocalService activePhaseLocalService) {
		_activePhaseLocalService = activePhaseLocalService;
	}

	/**
	* Adds the active phase to the database. Also notifies the appropriate model listeners.
	*
	* @param activePhase the active phase
	* @return the active phase that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.ActivePhase addActivePhase(
		com.aw.internal.model.ActivePhase activePhase)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _activePhaseLocalService.addActivePhase(activePhase);
	}

	/**
	* Creates a new active phase with the primary key. Does not add the active phase to the database.
	*
	* @param region the primary key for the new active phase
	* @return the new active phase
	*/
	@Override
	public com.aw.internal.model.ActivePhase createActivePhase(
		java.lang.String region) {
		return _activePhaseLocalService.createActivePhase(region);
	}

	/**
	* Deletes the active phase with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param region the primary key of the active phase
	* @return the active phase that was removed
	* @throws PortalException if a active phase with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.ActivePhase deleteActivePhase(
		java.lang.String region)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _activePhaseLocalService.deleteActivePhase(region);
	}

	/**
	* Deletes the active phase from the database. Also notifies the appropriate model listeners.
	*
	* @param activePhase the active phase
	* @return the active phase that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.ActivePhase deleteActivePhase(
		com.aw.internal.model.ActivePhase activePhase)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _activePhaseLocalService.deleteActivePhase(activePhase);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _activePhaseLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _activePhaseLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.ActivePhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _activePhaseLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.ActivePhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _activePhaseLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _activePhaseLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _activePhaseLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public com.aw.internal.model.ActivePhase fetchActivePhase(
		java.lang.String region)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _activePhaseLocalService.fetchActivePhase(region);
	}

	/**
	* Returns the active phase with the primary key.
	*
	* @param region the primary key of the active phase
	* @return the active phase
	* @throws PortalException if a active phase with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.ActivePhase getActivePhase(
		java.lang.String region)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _activePhaseLocalService.getActivePhase(region);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _activePhaseLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the active phases.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.ActivePhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of active phases
	* @param end the upper bound of the range of active phases (not inclusive)
	* @return the range of active phases
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<com.aw.internal.model.ActivePhase> getActivePhases(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _activePhaseLocalService.getActivePhases(start, end);
	}

	/**
	* Returns the number of active phases.
	*
	* @return the number of active phases
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getActivePhasesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _activePhaseLocalService.getActivePhasesCount();
	}

	/**
	* Updates the active phase in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param activePhase the active phase
	* @return the active phase that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.ActivePhase updateActivePhase(
		com.aw.internal.model.ActivePhase activePhase)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _activePhaseLocalService.updateActivePhase(activePhase);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _activePhaseLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_activePhaseLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _activePhaseLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public ActivePhaseLocalService getWrappedActivePhaseLocalService() {
		return _activePhaseLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedActivePhaseLocalService(
		ActivePhaseLocalService activePhaseLocalService) {
		_activePhaseLocalService = activePhaseLocalService;
	}

	@Override
	public ActivePhaseLocalService getWrappedService() {
		return _activePhaseLocalService;
	}

	@Override
	public void setWrappedService(
		ActivePhaseLocalService activePhaseLocalService) {
		_activePhaseLocalService = activePhaseLocalService;
	}

	private ActivePhaseLocalService _activePhaseLocalService;
}