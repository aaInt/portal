/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CertDocStoreLocalService}.
 *
 * @author zl
 * @see CertDocStoreLocalService
 * @generated
 */
public class CertDocStoreLocalServiceWrapper implements CertDocStoreLocalService,
	ServiceWrapper<CertDocStoreLocalService> {
	public CertDocStoreLocalServiceWrapper(
		CertDocStoreLocalService certDocStoreLocalService) {
		_certDocStoreLocalService = certDocStoreLocalService;
	}

	/**
	* Adds the cert doc store to the database. Also notifies the appropriate model listeners.
	*
	* @param certDocStore the cert doc store
	* @return the cert doc store that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.CertDocStore addCertDocStore(
		com.aw.internal.model.CertDocStore certDocStore)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _certDocStoreLocalService.addCertDocStore(certDocStore);
	}

	/**
	* Creates a new cert doc store with the primary key. Does not add the cert doc store to the database.
	*
	* @param docId the primary key for the new cert doc store
	* @return the new cert doc store
	*/
	@Override
	public com.aw.internal.model.CertDocStore createCertDocStore(long docId) {
		return _certDocStoreLocalService.createCertDocStore(docId);
	}

	/**
	* Deletes the cert doc store with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param docId the primary key of the cert doc store
	* @return the cert doc store that was removed
	* @throws PortalException if a cert doc store with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.CertDocStore deleteCertDocStore(long docId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _certDocStoreLocalService.deleteCertDocStore(docId);
	}

	/**
	* Deletes the cert doc store from the database. Also notifies the appropriate model listeners.
	*
	* @param certDocStore the cert doc store
	* @return the cert doc store that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.CertDocStore deleteCertDocStore(
		com.aw.internal.model.CertDocStore certDocStore)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _certDocStoreLocalService.deleteCertDocStore(certDocStore);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _certDocStoreLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _certDocStoreLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CertDocStoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _certDocStoreLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CertDocStoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _certDocStoreLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _certDocStoreLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _certDocStoreLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public com.aw.internal.model.CertDocStore fetchCertDocStore(long docId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _certDocStoreLocalService.fetchCertDocStore(docId);
	}

	/**
	* Returns the cert doc store with the primary key.
	*
	* @param docId the primary key of the cert doc store
	* @return the cert doc store
	* @throws PortalException if a cert doc store with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.CertDocStore getCertDocStore(long docId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _certDocStoreLocalService.getCertDocStore(docId);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _certDocStoreLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the cert doc stores.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CertDocStoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of cert doc stores
	* @param end the upper bound of the range of cert doc stores (not inclusive)
	* @return the range of cert doc stores
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<com.aw.internal.model.CertDocStore> getCertDocStores(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _certDocStoreLocalService.getCertDocStores(start, end);
	}

	/**
	* Returns the number of cert doc stores.
	*
	* @return the number of cert doc stores
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getCertDocStoresCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _certDocStoreLocalService.getCertDocStoresCount();
	}

	/**
	* Updates the cert doc store in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param certDocStore the cert doc store
	* @return the cert doc store that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.CertDocStore updateCertDocStore(
		com.aw.internal.model.CertDocStore certDocStore)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _certDocStoreLocalService.updateCertDocStore(certDocStore);
	}

	@Override
	public com.aw.internal.model.CertDocStoreContentBlobModel getContentBlobModel(
		java.io.Serializable primaryKey)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _certDocStoreLocalService.getContentBlobModel(primaryKey);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _certDocStoreLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_certDocStoreLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _certDocStoreLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	@Override
	public com.aw.internal.model.CertDocStore addDoc(java.lang.String base64Str)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _certDocStoreLocalService.addDoc(base64Str);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public CertDocStoreLocalService getWrappedCertDocStoreLocalService() {
		return _certDocStoreLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedCertDocStoreLocalService(
		CertDocStoreLocalService certDocStoreLocalService) {
		_certDocStoreLocalService = certDocStoreLocalService;
	}

	@Override
	public CertDocStoreLocalService getWrappedService() {
		return _certDocStoreLocalService;
	}

	@Override
	public void setWrappedService(
		CertDocStoreLocalService certDocStoreLocalService) {
		_certDocStoreLocalService = certDocStoreLocalService;
	}

	private CertDocStoreLocalService _certDocStoreLocalService;
}