/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CoinOrderLocalService}.
 *
 * @author zl
 * @see CoinOrderLocalService
 * @generated
 */
public class CoinOrderLocalServiceWrapper implements CoinOrderLocalService,
	ServiceWrapper<CoinOrderLocalService> {
	public CoinOrderLocalServiceWrapper(
		CoinOrderLocalService coinOrderLocalService) {
		_coinOrderLocalService = coinOrderLocalService;
	}

	/**
	* Adds the coin order to the database. Also notifies the appropriate model listeners.
	*
	* @param coinOrder the coin order
	* @return the coin order that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.CoinOrder addCoinOrder(
		com.aw.internal.model.CoinOrder coinOrder)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _coinOrderLocalService.addCoinOrder(coinOrder);
	}

	/**
	* Creates a new coin order with the primary key. Does not add the coin order to the database.
	*
	* @param orderId the primary key for the new coin order
	* @return the new coin order
	*/
	@Override
	public com.aw.internal.model.CoinOrder createCoinOrder(
		java.lang.String orderId) {
		return _coinOrderLocalService.createCoinOrder(orderId);
	}

	/**
	* Deletes the coin order with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param orderId the primary key of the coin order
	* @return the coin order that was removed
	* @throws PortalException if a coin order with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.CoinOrder deleteCoinOrder(
		java.lang.String orderId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _coinOrderLocalService.deleteCoinOrder(orderId);
	}

	/**
	* Deletes the coin order from the database. Also notifies the appropriate model listeners.
	*
	* @param coinOrder the coin order
	* @return the coin order that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.CoinOrder deleteCoinOrder(
		com.aw.internal.model.CoinOrder coinOrder)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _coinOrderLocalService.deleteCoinOrder(coinOrder);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _coinOrderLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _coinOrderLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinOrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _coinOrderLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinOrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _coinOrderLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _coinOrderLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _coinOrderLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public com.aw.internal.model.CoinOrder fetchCoinOrder(
		java.lang.String orderId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _coinOrderLocalService.fetchCoinOrder(orderId);
	}

	/**
	* Returns the coin order with the primary key.
	*
	* @param orderId the primary key of the coin order
	* @return the coin order
	* @throws PortalException if a coin order with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.CoinOrder getCoinOrder(
		java.lang.String orderId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _coinOrderLocalService.getCoinOrder(orderId);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _coinOrderLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the coin orders.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinOrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of coin orders
	* @param end the upper bound of the range of coin orders (not inclusive)
	* @return the range of coin orders
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<com.aw.internal.model.CoinOrder> getCoinOrders(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _coinOrderLocalService.getCoinOrders(start, end);
	}

	/**
	* Returns the number of coin orders.
	*
	* @return the number of coin orders
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getCoinOrdersCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _coinOrderLocalService.getCoinOrdersCount();
	}

	/**
	* Updates the coin order in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param coinOrder the coin order
	* @return the coin order that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.aw.internal.model.CoinOrder updateCoinOrder(
		com.aw.internal.model.CoinOrder coinOrder)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _coinOrderLocalService.updateCoinOrder(coinOrder);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _coinOrderLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_coinOrderLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _coinOrderLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public CoinOrderLocalService getWrappedCoinOrderLocalService() {
		return _coinOrderLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedCoinOrderLocalService(
		CoinOrderLocalService coinOrderLocalService) {
		_coinOrderLocalService = coinOrderLocalService;
	}

	@Override
	public CoinOrderLocalService getWrappedService() {
		return _coinOrderLocalService;
	}

	@Override
	public void setWrappedService(CoinOrderLocalService coinOrderLocalService) {
		_coinOrderLocalService = coinOrderLocalService;
	}

	private CoinOrderLocalService _coinOrderLocalService;
}