/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.persistence;

import com.aw.internal.model.CoinOrder;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the coin order service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author zl
 * @see CoinOrderPersistenceImpl
 * @see CoinOrderUtil
 * @generated
 */
public interface CoinOrderPersistence extends BasePersistence<CoinOrder> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CoinOrderUtil} to access the coin order persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the coin orders where activeFlag = &#63; and completeFlag = &#63;.
	*
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @return the matching coin orders
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.aw.internal.model.CoinOrder> findBypending(
		java.lang.String activeFlag, java.lang.String completeFlag)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the coin orders where activeFlag = &#63; and completeFlag = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinOrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @param start the lower bound of the range of coin orders
	* @param end the upper bound of the range of coin orders (not inclusive)
	* @return the range of matching coin orders
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.aw.internal.model.CoinOrder> findBypending(
		java.lang.String activeFlag, java.lang.String completeFlag, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the coin orders where activeFlag = &#63; and completeFlag = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinOrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @param start the lower bound of the range of coin orders
	* @param end the upper bound of the range of coin orders (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching coin orders
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.aw.internal.model.CoinOrder> findBypending(
		java.lang.String activeFlag, java.lang.String completeFlag, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first coin order in the ordered set where activeFlag = &#63; and completeFlag = &#63;.
	*
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching coin order
	* @throws com.aw.internal.NoSuchCoinOrderException if a matching coin order could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.CoinOrder findBypending_First(
		java.lang.String activeFlag, java.lang.String completeFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.aw.internal.NoSuchCoinOrderException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first coin order in the ordered set where activeFlag = &#63; and completeFlag = &#63;.
	*
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching coin order, or <code>null</code> if a matching coin order could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.CoinOrder fetchBypending_First(
		java.lang.String activeFlag, java.lang.String completeFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last coin order in the ordered set where activeFlag = &#63; and completeFlag = &#63;.
	*
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching coin order
	* @throws com.aw.internal.NoSuchCoinOrderException if a matching coin order could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.CoinOrder findBypending_Last(
		java.lang.String activeFlag, java.lang.String completeFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.aw.internal.NoSuchCoinOrderException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last coin order in the ordered set where activeFlag = &#63; and completeFlag = &#63;.
	*
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching coin order, or <code>null</code> if a matching coin order could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.CoinOrder fetchBypending_Last(
		java.lang.String activeFlag, java.lang.String completeFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the coin orders before and after the current coin order in the ordered set where activeFlag = &#63; and completeFlag = &#63;.
	*
	* @param orderId the primary key of the current coin order
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next coin order
	* @throws com.aw.internal.NoSuchCoinOrderException if a coin order with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.CoinOrder[] findBypending_PrevAndNext(
		java.lang.String orderId, java.lang.String activeFlag,
		java.lang.String completeFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.aw.internal.NoSuchCoinOrderException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the coin orders where activeFlag = &#63; and completeFlag = &#63; from the database.
	*
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @throws SystemException if a system exception occurred
	*/
	public void removeBypending(java.lang.String activeFlag,
		java.lang.String completeFlag)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of coin orders where activeFlag = &#63; and completeFlag = &#63;.
	*
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @return the number of matching coin orders
	* @throws SystemException if a system exception occurred
	*/
	public int countBypending(java.lang.String activeFlag,
		java.lang.String completeFlag)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the coin orders where activeFlag = &#63; and completeFlag = &#63;.
	*
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @return the matching coin orders
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.aw.internal.model.CoinOrder> findBycomplete(
		java.lang.String activeFlag, java.lang.String completeFlag)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the coin orders where activeFlag = &#63; and completeFlag = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinOrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @param start the lower bound of the range of coin orders
	* @param end the upper bound of the range of coin orders (not inclusive)
	* @return the range of matching coin orders
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.aw.internal.model.CoinOrder> findBycomplete(
		java.lang.String activeFlag, java.lang.String completeFlag, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the coin orders where activeFlag = &#63; and completeFlag = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinOrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @param start the lower bound of the range of coin orders
	* @param end the upper bound of the range of coin orders (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching coin orders
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.aw.internal.model.CoinOrder> findBycomplete(
		java.lang.String activeFlag, java.lang.String completeFlag, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first coin order in the ordered set where activeFlag = &#63; and completeFlag = &#63;.
	*
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching coin order
	* @throws com.aw.internal.NoSuchCoinOrderException if a matching coin order could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.CoinOrder findBycomplete_First(
		java.lang.String activeFlag, java.lang.String completeFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.aw.internal.NoSuchCoinOrderException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first coin order in the ordered set where activeFlag = &#63; and completeFlag = &#63;.
	*
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching coin order, or <code>null</code> if a matching coin order could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.CoinOrder fetchBycomplete_First(
		java.lang.String activeFlag, java.lang.String completeFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last coin order in the ordered set where activeFlag = &#63; and completeFlag = &#63;.
	*
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching coin order
	* @throws com.aw.internal.NoSuchCoinOrderException if a matching coin order could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.CoinOrder findBycomplete_Last(
		java.lang.String activeFlag, java.lang.String completeFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.aw.internal.NoSuchCoinOrderException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last coin order in the ordered set where activeFlag = &#63; and completeFlag = &#63;.
	*
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching coin order, or <code>null</code> if a matching coin order could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.CoinOrder fetchBycomplete_Last(
		java.lang.String activeFlag, java.lang.String completeFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the coin orders before and after the current coin order in the ordered set where activeFlag = &#63; and completeFlag = &#63;.
	*
	* @param orderId the primary key of the current coin order
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next coin order
	* @throws com.aw.internal.NoSuchCoinOrderException if a coin order with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.CoinOrder[] findBycomplete_PrevAndNext(
		java.lang.String orderId, java.lang.String activeFlag,
		java.lang.String completeFlag,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.aw.internal.NoSuchCoinOrderException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the coin orders where activeFlag = &#63; and completeFlag = &#63; from the database.
	*
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @throws SystemException if a system exception occurred
	*/
	public void removeBycomplete(java.lang.String activeFlag,
		java.lang.String completeFlag)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of coin orders where activeFlag = &#63; and completeFlag = &#63;.
	*
	* @param activeFlag the active flag
	* @param completeFlag the complete flag
	* @return the number of matching coin orders
	* @throws SystemException if a system exception occurred
	*/
	public int countBycomplete(java.lang.String activeFlag,
		java.lang.String completeFlag)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the coin order in the entity cache if it is enabled.
	*
	* @param coinOrder the coin order
	*/
	public void cacheResult(com.aw.internal.model.CoinOrder coinOrder);

	/**
	* Caches the coin orders in the entity cache if it is enabled.
	*
	* @param coinOrders the coin orders
	*/
	public void cacheResult(
		java.util.List<com.aw.internal.model.CoinOrder> coinOrders);

	/**
	* Creates a new coin order with the primary key. Does not add the coin order to the database.
	*
	* @param orderId the primary key for the new coin order
	* @return the new coin order
	*/
	public com.aw.internal.model.CoinOrder create(java.lang.String orderId);

	/**
	* Removes the coin order with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param orderId the primary key of the coin order
	* @return the coin order that was removed
	* @throws com.aw.internal.NoSuchCoinOrderException if a coin order with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.CoinOrder remove(java.lang.String orderId)
		throws com.aw.internal.NoSuchCoinOrderException,
			com.liferay.portal.kernel.exception.SystemException;

	public com.aw.internal.model.CoinOrder updateImpl(
		com.aw.internal.model.CoinOrder coinOrder)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the coin order with the primary key or throws a {@link com.aw.internal.NoSuchCoinOrderException} if it could not be found.
	*
	* @param orderId the primary key of the coin order
	* @return the coin order
	* @throws com.aw.internal.NoSuchCoinOrderException if a coin order with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.CoinOrder findByPrimaryKey(
		java.lang.String orderId)
		throws com.aw.internal.NoSuchCoinOrderException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the coin order with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param orderId the primary key of the coin order
	* @return the coin order, or <code>null</code> if a coin order with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.aw.internal.model.CoinOrder fetchByPrimaryKey(
		java.lang.String orderId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the coin orders.
	*
	* @return the coin orders
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.aw.internal.model.CoinOrder> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the coin orders.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinOrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of coin orders
	* @param end the upper bound of the range of coin orders (not inclusive)
	* @return the range of coin orders
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.aw.internal.model.CoinOrder> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the coin orders.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinOrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of coin orders
	* @param end the upper bound of the range of coin orders (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of coin orders
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.aw.internal.model.CoinOrder> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the coin orders from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of coin orders.
	*
	* @return the number of coin orders
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}