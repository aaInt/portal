/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.jsonwebservice.JSONWebService;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.security.ac.AccessControlled;
import com.liferay.portal.service.BaseService;
import com.liferay.portal.service.InvokableService;

/**
 * Provides the remote service interface for CoinUser. Methods of this
 * service are expected to have security checks based on the propagated JAAS
 * credentials because this service can be accessed remotely.
 *
 * @author zl
 * @see CoinUserServiceUtil
 * @see com.aw.internal.service.base.CoinUserServiceBaseImpl
 * @see com.aw.internal.service.impl.CoinUserServiceImpl
 * @generated
 */
@AccessControlled
@JSONWebService
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
	PortalException.class, SystemException.class})
public interface CoinUserService extends BaseService, InvokableService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CoinUserServiceUtil} to access the coin user remote service. Add custom service methods to {@link com.aw.internal.service.impl.CoinUserServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier();

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier);

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable;

	public com.liferay.portal.kernel.json.JSONObject addCoinUser(
		java.lang.String firstName, java.lang.String lastName,
		java.lang.String birthday, java.lang.String userType,
		java.lang.String brkLics, java.lang.String brkCountry, long fundId,
		java.lang.String nationality, java.lang.String idIssueCountry,
		java.lang.String idType, java.lang.String idNumber,
		java.lang.String photoImage, java.lang.String certDocStr,
		java.lang.String ssn, java.lang.String ein, java.lang.String income,
		java.lang.String asset, long phone, int areaCode,
		java.lang.String email, java.lang.String wechat,
		java.lang.String facebook, java.lang.String twitter,
		java.lang.String weibo, java.lang.String street1,
		java.lang.String street2, java.lang.String city,
		java.lang.String state, java.lang.String country,
		java.lang.String zipCode, java.lang.String isMailDiff,
		java.lang.String mailStreet1, java.lang.String mailStreet2,
		java.lang.String mailCity, java.lang.String mailState,
		java.lang.String mailCountry, java.lang.String mailZipCode);

	public com.liferay.portal.kernel.json.JSONObject updateCoinUser(
		java.lang.String coinUserId, java.lang.String firstName,
		java.lang.String lastName, java.lang.String birthday,
		java.lang.String userType, java.lang.String brkLics,
		java.lang.String brkCountry, long fundId, java.lang.String nationality,
		java.lang.String idIssueCountry, java.lang.String idType,
		java.lang.String idNumber, java.lang.String ssn, java.lang.String ein,
		java.lang.String income, java.lang.String asset, long phone,
		int areaCode, java.lang.String email, java.lang.String wechat,
		java.lang.String facebook, java.lang.String twitter,
		java.lang.String weibo, java.lang.String street1,
		java.lang.String street2, java.lang.String city,
		java.lang.String state, java.lang.String country,
		java.lang.String zipCode, java.lang.String isMailDiff,
		java.lang.String mailStreet1, java.lang.String mailStreet2,
		java.lang.String mailCity, java.lang.String mailState,
		java.lang.String mailCountry, java.lang.String mailZipCode);

	public com.liferay.portal.kernel.json.JSONObject deleteUser(
		java.lang.String coinuserId);

	public com.liferay.portal.kernel.json.JSONObject updatePhoto(
		java.lang.String coinuserId, java.lang.String photoStr);

	public com.liferay.portal.kernel.json.JSONObject updateDoc(
		java.lang.String coinuserId, java.lang.String docStr);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.liferay.portal.kernel.json.JSONObject getCoinUser(
		java.lang.String coinUserId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.liferay.portal.kernel.json.JSONObject getValidatedCoinUsers();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.liferay.portal.kernel.json.JSONObject getSaleCoinUsers();

	public com.liferay.portal.kernel.json.JSONObject deactivateCoinUser(
		java.lang.String coinUserId);

	public com.liferay.portal.kernel.json.JSONObject verifyCoinUser(
		java.lang.String coinUserId);

	public com.liferay.portal.kernel.json.JSONObject verifyAcredittedCoinUser(
		java.lang.String coinUserId);

	public com.liferay.portal.kernel.json.JSONObject pendAcredittedCoinUser(
		java.lang.String coinUserId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.liferay.portal.kernel.json.JSONObject getBroker(long id);

	public com.liferay.portal.kernel.json.JSONObject addFund(
		java.lang.String name, long phone, int areaCode,
		java.lang.String email, java.lang.String wechat,
		java.lang.String facebook, java.lang.String twitter,
		java.lang.String weibo, java.lang.String street1,
		java.lang.String street2, java.lang.String city,
		java.lang.String state, java.lang.String country,
		java.lang.String zipCode);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.liferay.portal.kernel.json.JSONObject getFund(long fundId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.liferay.portal.kernel.json.JSONObject getFunds();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.liferay.portal.kernel.json.JSONObject getContact(long contactId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.lang.String getImage(java.lang.String userId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.lang.String getCertDoc(java.lang.String userId);
}