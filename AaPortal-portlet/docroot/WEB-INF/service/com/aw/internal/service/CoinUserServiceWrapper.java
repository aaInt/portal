/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CoinUserService}.
 *
 * @author zl
 * @see CoinUserService
 * @generated
 */
public class CoinUserServiceWrapper implements CoinUserService,
	ServiceWrapper<CoinUserService> {
	public CoinUserServiceWrapper(CoinUserService coinUserService) {
		_coinUserService = coinUserService;
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _coinUserService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_coinUserService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _coinUserService.invokeMethod(name, parameterTypes, arguments);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject addCoinUser(
		java.lang.String firstName, java.lang.String lastName,
		java.lang.String birthday, java.lang.String userType,
		java.lang.String brkLics, java.lang.String brkCountry, long fundId,
		java.lang.String nationality, java.lang.String idIssueCountry,
		java.lang.String idType, java.lang.String idNumber,
		java.lang.String photoImage, java.lang.String certDocStr,
		java.lang.String ssn, java.lang.String ein, java.lang.String income,
		java.lang.String asset, long phone, int areaCode,
		java.lang.String email, java.lang.String wechat,
		java.lang.String facebook, java.lang.String twitter,
		java.lang.String weibo, java.lang.String street1,
		java.lang.String street2, java.lang.String city,
		java.lang.String state, java.lang.String country,
		java.lang.String zipCode, java.lang.String isMailDiff,
		java.lang.String mailStreet1, java.lang.String mailStreet2,
		java.lang.String mailCity, java.lang.String mailState,
		java.lang.String mailCountry, java.lang.String mailZipCode) {
		return _coinUserService.addCoinUser(firstName, lastName, birthday,
			userType, brkLics, brkCountry, fundId, nationality, idIssueCountry,
			idType, idNumber, photoImage, certDocStr, ssn, ein, income, asset,
			phone, areaCode, email, wechat, facebook, twitter, weibo, street1,
			street2, city, state, country, zipCode, isMailDiff, mailStreet1,
			mailStreet2, mailCity, mailState, mailCountry, mailZipCode);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject updateCoinUser(
		java.lang.String coinUserId, java.lang.String firstName,
		java.lang.String lastName, java.lang.String birthday,
		java.lang.String userType, java.lang.String brkLics,
		java.lang.String brkCountry, long fundId, java.lang.String nationality,
		java.lang.String idIssueCountry, java.lang.String idType,
		java.lang.String idNumber, java.lang.String ssn, java.lang.String ein,
		java.lang.String income, java.lang.String asset, long phone,
		int areaCode, java.lang.String email, java.lang.String wechat,
		java.lang.String facebook, java.lang.String twitter,
		java.lang.String weibo, java.lang.String street1,
		java.lang.String street2, java.lang.String city,
		java.lang.String state, java.lang.String country,
		java.lang.String zipCode, java.lang.String isMailDiff,
		java.lang.String mailStreet1, java.lang.String mailStreet2,
		java.lang.String mailCity, java.lang.String mailState,
		java.lang.String mailCountry, java.lang.String mailZipCode) {
		return _coinUserService.updateCoinUser(coinUserId, firstName, lastName,
			birthday, userType, brkLics, brkCountry, fundId, nationality,
			idIssueCountry, idType, idNumber, ssn, ein, income, asset, phone,
			areaCode, email, wechat, facebook, twitter, weibo, street1,
			street2, city, state, country, zipCode, isMailDiff, mailStreet1,
			mailStreet2, mailCity, mailState, mailCountry, mailZipCode);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject deleteUser(
		java.lang.String coinuserId) {
		return _coinUserService.deleteUser(coinuserId);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject updatePhoto(
		java.lang.String coinuserId, java.lang.String photoStr) {
		return _coinUserService.updatePhoto(coinuserId, photoStr);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject updateDoc(
		java.lang.String coinuserId, java.lang.String docStr) {
		return _coinUserService.updateDoc(coinuserId, docStr);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getCoinUser(
		java.lang.String coinUserId) {
		return _coinUserService.getCoinUser(coinUserId);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getValidatedCoinUsers() {
		return _coinUserService.getValidatedCoinUsers();
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getSaleCoinUsers() {
		return _coinUserService.getSaleCoinUsers();
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject deactivateCoinUser(
		java.lang.String coinUserId) {
		return _coinUserService.deactivateCoinUser(coinUserId);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject verifyCoinUser(
		java.lang.String coinUserId) {
		return _coinUserService.verifyCoinUser(coinUserId);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject verifyAcredittedCoinUser(
		java.lang.String coinUserId) {
		return _coinUserService.verifyAcredittedCoinUser(coinUserId);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject pendAcredittedCoinUser(
		java.lang.String coinUserId) {
		return _coinUserService.pendAcredittedCoinUser(coinUserId);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getBroker(long id) {
		return _coinUserService.getBroker(id);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject addFund(
		java.lang.String name, long phone, int areaCode,
		java.lang.String email, java.lang.String wechat,
		java.lang.String facebook, java.lang.String twitter,
		java.lang.String weibo, java.lang.String street1,
		java.lang.String street2, java.lang.String city,
		java.lang.String state, java.lang.String country,
		java.lang.String zipCode) {
		return _coinUserService.addFund(name, phone, areaCode, email, wechat,
			facebook, twitter, weibo, street1, street2, city, state, country,
			zipCode);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getFund(long fundId) {
		return _coinUserService.getFund(fundId);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getFunds() {
		return _coinUserService.getFunds();
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getContact(long contactId) {
		return _coinUserService.getContact(contactId);
	}

	@Override
	public java.lang.String getImage(java.lang.String userId) {
		return _coinUserService.getImage(userId);
	}

	@Override
	public java.lang.String getCertDoc(java.lang.String userId) {
		return _coinUserService.getCertDoc(userId);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public CoinUserService getWrappedCoinUserService() {
		return _coinUserService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedCoinUserService(CoinUserService coinUserService) {
		_coinUserService = coinUserService;
	}

	@Override
	public CoinUserService getWrappedService() {
		return _coinUserService;
	}

	@Override
	public void setWrappedService(CoinUserService coinUserService) {
		_coinUserService = coinUserService;
	}

	private CoinUserService _coinUserService;
}