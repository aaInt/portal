/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CommonFunctionService}.
 *
 * @author zl
 * @see CommonFunctionService
 * @generated
 */
public class CommonFunctionServiceWrapper implements CommonFunctionService,
	ServiceWrapper<CommonFunctionService> {
	public CommonFunctionServiceWrapper(
		CommonFunctionService commonFunctionService) {
		_commonFunctionService = commonFunctionService;
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _commonFunctionService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_commonFunctionService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _commonFunctionService.invokeMethod(name, parameterTypes,
			arguments);
	}

	@Override
	public boolean checkPassword(java.lang.String password) {
		return _commonFunctionService.checkPassword(password);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getCoinCounts() {
		return _commonFunctionService.getCoinCounts();
	}

	@Override
	public boolean endCoinCountPhase(int endPhase, int toPhase) {
		return _commonFunctionService.endCoinCountPhase(endPhase, toPhase);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getPhasePrice(
		java.lang.String currency, java.lang.String userId) {
		return _commonFunctionService.getPhasePrice(currency, userId);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject getUserInfo() {
		return _commonFunctionService.getUserInfo();
	}

	@Override
	public boolean updatePassword(java.lang.String origin,
		java.lang.String new1, java.lang.String new2) {
		return _commonFunctionService.updatePassword(origin, new1, new2);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public CommonFunctionService getWrappedCommonFunctionService() {
		return _commonFunctionService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedCommonFunctionService(
		CommonFunctionService commonFunctionService) {
		_commonFunctionService = commonFunctionService;
	}

	@Override
	public CommonFunctionService getWrappedService() {
		return _commonFunctionService;
	}

	@Override
	public void setWrappedService(CommonFunctionService commonFunctionService) {
		_commonFunctionService = commonFunctionService;
	}

	private CommonFunctionService _commonFunctionService;
}