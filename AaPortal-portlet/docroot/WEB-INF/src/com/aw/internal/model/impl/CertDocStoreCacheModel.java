/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model.impl;

import com.aw.internal.model.CertDocStore;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing CertDocStore in entity cache.
 *
 * @author zl
 * @see CertDocStore
 * @generated
 */
public class CertDocStoreCacheModel implements CacheModel<CertDocStore>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{docId=");
		sb.append(docId);
		sb.append(", activeFlag=");
		sb.append(activeFlag);
		sb.append(", addDate=");
		sb.append(addDate);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public CertDocStore toEntityModel() {
		CertDocStoreImpl certDocStoreImpl = new CertDocStoreImpl();

		certDocStoreImpl.setDocId(docId);

		if (activeFlag == null) {
			certDocStoreImpl.setActiveFlag(StringPool.BLANK);
		}
		else {
			certDocStoreImpl.setActiveFlag(activeFlag);
		}

		if (addDate == Long.MIN_VALUE) {
			certDocStoreImpl.setAddDate(null);
		}
		else {
			certDocStoreImpl.setAddDate(new Date(addDate));
		}

		certDocStoreImpl.resetOriginalValues();

		return certDocStoreImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		docId = objectInput.readLong();
		activeFlag = objectInput.readUTF();
		addDate = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(docId);

		if (activeFlag == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(activeFlag);
		}

		objectOutput.writeLong(addDate);
	}

	public long docId;
	public String activeFlag;
	public long addDate;
}