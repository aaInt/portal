/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model.impl;

import com.aw.internal.model.ActivePhase;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing ActivePhase in entity cache.
 *
 * @author zl
 * @see ActivePhase
 * @generated
 */
public class ActivePhaseCacheModel implements CacheModel<ActivePhase>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{region=");
		sb.append(region);
		sb.append(", salePhase=");
		sb.append(salePhase);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ActivePhase toEntityModel() {
		ActivePhaseImpl activePhaseImpl = new ActivePhaseImpl();

		if (region == null) {
			activePhaseImpl.setRegion(StringPool.BLANK);
		}
		else {
			activePhaseImpl.setRegion(region);
		}

		activePhaseImpl.setSalePhase(salePhase);

		activePhaseImpl.resetOriginalValues();

		return activePhaseImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		region = objectInput.readUTF();
		salePhase = objectInput.readInt();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (region == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(region);
		}

		objectOutput.writeInt(salePhase);
	}

	public String region;
	public int salePhase;
}