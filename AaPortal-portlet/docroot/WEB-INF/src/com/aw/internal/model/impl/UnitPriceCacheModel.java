/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model.impl;

import com.aw.internal.model.UnitPrice;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing UnitPrice in entity cache.
 *
 * @author zl
 * @see UnitPrice
 * @generated
 */
public class UnitPriceCacheModel implements CacheModel<UnitPrice>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{currency=");
		sb.append(currency);
		sb.append(", salePhase=");
		sb.append(salePhase);
		sb.append(", price=");
		sb.append(price);
		sb.append(", activeFlag=");
		sb.append(activeFlag);
		sb.append(", addDate=");
		sb.append(addDate);
		sb.append(", addUser=");
		sb.append(addUser);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public UnitPrice toEntityModel() {
		UnitPriceImpl unitPriceImpl = new UnitPriceImpl();

		if (currency == null) {
			unitPriceImpl.setCurrency(StringPool.BLANK);
		}
		else {
			unitPriceImpl.setCurrency(currency);
		}

		unitPriceImpl.setSalePhase(salePhase);
		unitPriceImpl.setPrice(price);

		if (activeFlag == null) {
			unitPriceImpl.setActiveFlag(StringPool.BLANK);
		}
		else {
			unitPriceImpl.setActiveFlag(activeFlag);
		}

		if (addDate == Long.MIN_VALUE) {
			unitPriceImpl.setAddDate(null);
		}
		else {
			unitPriceImpl.setAddDate(new Date(addDate));
		}

		unitPriceImpl.setAddUser(addUser);

		unitPriceImpl.resetOriginalValues();

		return unitPriceImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		currency = objectInput.readUTF();
		salePhase = objectInput.readInt();
		price = objectInput.readDouble();
		activeFlag = objectInput.readUTF();
		addDate = objectInput.readLong();
		addUser = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (currency == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(currency);
		}

		objectOutput.writeInt(salePhase);
		objectOutput.writeDouble(price);

		if (activeFlag == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(activeFlag);
		}

		objectOutput.writeLong(addDate);
		objectOutput.writeLong(addUser);
	}

	public String currency;
	public int salePhase;
	public double price;
	public String activeFlag;
	public long addDate;
	public long addUser;
}