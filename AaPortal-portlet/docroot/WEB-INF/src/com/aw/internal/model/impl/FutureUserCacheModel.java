/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model.impl;

import com.aw.internal.model.FutureUser;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing FutureUser in entity cache.
 *
 * @author zl
 * @see FutureUser
 * @generated
 */
public class FutureUserCacheModel implements CacheModel<FutureUser>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{email=");
		sb.append(email);
		sb.append(", firstName=");
		sb.append(firstName);
		sb.append(", lastName=");
		sb.append(lastName);
		sb.append(", residency=");
		sb.append(residency);
		sb.append(", phone=");
		sb.append(phone);
		sb.append(", areaCode=");
		sb.append(areaCode);
		sb.append(", activeFlag=");
		sb.append(activeFlag);
		sb.append(", takenFlag=");
		sb.append(takenFlag);
		sb.append(", ownerId=");
		sb.append(ownerId);
		sb.append(", addTime=");
		sb.append(addTime);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public FutureUser toEntityModel() {
		FutureUserImpl futureUserImpl = new FutureUserImpl();

		if (email == null) {
			futureUserImpl.setEmail(StringPool.BLANK);
		}
		else {
			futureUserImpl.setEmail(email);
		}

		if (firstName == null) {
			futureUserImpl.setFirstName(StringPool.BLANK);
		}
		else {
			futureUserImpl.setFirstName(firstName);
		}

		if (lastName == null) {
			futureUserImpl.setLastName(StringPool.BLANK);
		}
		else {
			futureUserImpl.setLastName(lastName);
		}

		if (residency == null) {
			futureUserImpl.setResidency(StringPool.BLANK);
		}
		else {
			futureUserImpl.setResidency(residency);
		}

		futureUserImpl.setPhone(phone);
		futureUserImpl.setAreaCode(areaCode);

		if (activeFlag == null) {
			futureUserImpl.setActiveFlag(StringPool.BLANK);
		}
		else {
			futureUserImpl.setActiveFlag(activeFlag);
		}

		if (takenFlag == null) {
			futureUserImpl.setTakenFlag(StringPool.BLANK);
		}
		else {
			futureUserImpl.setTakenFlag(takenFlag);
		}

		futureUserImpl.setOwnerId(ownerId);

		if (addTime == Long.MIN_VALUE) {
			futureUserImpl.setAddTime(null);
		}
		else {
			futureUserImpl.setAddTime(new Date(addTime));
		}

		futureUserImpl.resetOriginalValues();

		return futureUserImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		email = objectInput.readUTF();
		firstName = objectInput.readUTF();
		lastName = objectInput.readUTF();
		residency = objectInput.readUTF();
		phone = objectInput.readLong();
		areaCode = objectInput.readInt();
		activeFlag = objectInput.readUTF();
		takenFlag = objectInput.readUTF();
		ownerId = objectInput.readLong();
		addTime = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (email == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(email);
		}

		if (firstName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(firstName);
		}

		if (lastName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(lastName);
		}

		if (residency == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(residency);
		}

		objectOutput.writeLong(phone);
		objectOutput.writeInt(areaCode);

		if (activeFlag == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(activeFlag);
		}

		if (takenFlag == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(takenFlag);
		}

		objectOutput.writeLong(ownerId);
		objectOutput.writeLong(addTime);
	}

	public String email;
	public String firstName;
	public String lastName;
	public String residency;
	public long phone;
	public int areaCode;
	public String activeFlag;
	public String takenFlag;
	public long ownerId;
	public long addTime;
}