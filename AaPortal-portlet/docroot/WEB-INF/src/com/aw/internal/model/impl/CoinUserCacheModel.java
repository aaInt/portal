/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model.impl;

import com.aw.internal.model.CoinUser;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing CoinUser in entity cache.
 *
 * @author zl
 * @see CoinUser
 * @generated
 */
public class CoinUserCacheModel implements CacheModel<CoinUser>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(47);

		sb.append("{userId=");
		sb.append(userId);
		sb.append(", firstName=");
		sb.append(firstName);
		sb.append(", lastName=");
		sb.append(lastName);
		sb.append(", activeFlag=");
		sb.append(activeFlag);
		sb.append(", birthday=");
		sb.append(birthday);
		sb.append(", contactId=");
		sb.append(contactId);
		sb.append(", userType=");
		sb.append(userType);
		sb.append(", brokerId=");
		sb.append(brokerId);
		sb.append(", fundId=");
		sb.append(fundId);
		sb.append(", nationality=");
		sb.append(nationality);
		sb.append(", idIssueCountry=");
		sb.append(idIssueCountry);
		sb.append(", idType=");
		sb.append(idType);
		sb.append(", idNumber=");
		sb.append(idNumber);
		sb.append(", photoImage=");
		sb.append(photoImage);
		sb.append(", docId=");
		sb.append(docId);
		sb.append(", ssn=");
		sb.append(ssn);
		sb.append(", ein=");
		sb.append(ein);
		sb.append(", income=");
		sb.append(income);
		sb.append(", asset=");
		sb.append(asset);
		sb.append(", isValidated=");
		sb.append(isValidated);
		sb.append(", isCreddited=");
		sb.append(isCreddited);
		sb.append(", addTime=");
		sb.append(addTime);
		sb.append(", addUser=");
		sb.append(addUser);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public CoinUser toEntityModel() {
		CoinUserImpl coinUserImpl = new CoinUserImpl();

		if (userId == null) {
			coinUserImpl.setUserId(StringPool.BLANK);
		}
		else {
			coinUserImpl.setUserId(userId);
		}

		if (firstName == null) {
			coinUserImpl.setFirstName(StringPool.BLANK);
		}
		else {
			coinUserImpl.setFirstName(firstName);
		}

		if (lastName == null) {
			coinUserImpl.setLastName(StringPool.BLANK);
		}
		else {
			coinUserImpl.setLastName(lastName);
		}

		if (activeFlag == null) {
			coinUserImpl.setActiveFlag(StringPool.BLANK);
		}
		else {
			coinUserImpl.setActiveFlag(activeFlag);
		}

		if (birthday == null) {
			coinUserImpl.setBirthday(StringPool.BLANK);
		}
		else {
			coinUserImpl.setBirthday(birthday);
		}

		coinUserImpl.setContactId(contactId);

		if (userType == null) {
			coinUserImpl.setUserType(StringPool.BLANK);
		}
		else {
			coinUserImpl.setUserType(userType);
		}

		coinUserImpl.setBrokerId(brokerId);
		coinUserImpl.setFundId(fundId);

		if (nationality == null) {
			coinUserImpl.setNationality(StringPool.BLANK);
		}
		else {
			coinUserImpl.setNationality(nationality);
		}

		if (idIssueCountry == null) {
			coinUserImpl.setIdIssueCountry(StringPool.BLANK);
		}
		else {
			coinUserImpl.setIdIssueCountry(idIssueCountry);
		}

		if (idType == null) {
			coinUserImpl.setIdType(StringPool.BLANK);
		}
		else {
			coinUserImpl.setIdType(idType);
		}

		if (idNumber == null) {
			coinUserImpl.setIdNumber(StringPool.BLANK);
		}
		else {
			coinUserImpl.setIdNumber(idNumber);
		}

		coinUserImpl.setPhotoImage(photoImage);
		coinUserImpl.setDocId(docId);

		if (ssn == null) {
			coinUserImpl.setSsn(StringPool.BLANK);
		}
		else {
			coinUserImpl.setSsn(ssn);
		}

		if (ein == null) {
			coinUserImpl.setEin(StringPool.BLANK);
		}
		else {
			coinUserImpl.setEin(ein);
		}

		if (income == null) {
			coinUserImpl.setIncome(StringPool.BLANK);
		}
		else {
			coinUserImpl.setIncome(income);
		}

		if (asset == null) {
			coinUserImpl.setAsset(StringPool.BLANK);
		}
		else {
			coinUserImpl.setAsset(asset);
		}

		if (isValidated == null) {
			coinUserImpl.setIsValidated(StringPool.BLANK);
		}
		else {
			coinUserImpl.setIsValidated(isValidated);
		}

		if (isCreddited == null) {
			coinUserImpl.setIsCreddited(StringPool.BLANK);
		}
		else {
			coinUserImpl.setIsCreddited(isCreddited);
		}

		if (addTime == Long.MIN_VALUE) {
			coinUserImpl.setAddTime(null);
		}
		else {
			coinUserImpl.setAddTime(new Date(addTime));
		}

		coinUserImpl.setAddUser(addUser);

		coinUserImpl.resetOriginalValues();

		return coinUserImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		userId = objectInput.readUTF();
		firstName = objectInput.readUTF();
		lastName = objectInput.readUTF();
		activeFlag = objectInput.readUTF();
		birthday = objectInput.readUTF();
		contactId = objectInput.readLong();
		userType = objectInput.readUTF();
		brokerId = objectInput.readLong();
		fundId = objectInput.readLong();
		nationality = objectInput.readUTF();
		idIssueCountry = objectInput.readUTF();
		idType = objectInput.readUTF();
		idNumber = objectInput.readUTF();
		photoImage = objectInput.readLong();
		docId = objectInput.readLong();
		ssn = objectInput.readUTF();
		ein = objectInput.readUTF();
		income = objectInput.readUTF();
		asset = objectInput.readUTF();
		isValidated = objectInput.readUTF();
		isCreddited = objectInput.readUTF();
		addTime = objectInput.readLong();
		addUser = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (userId == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userId);
		}

		if (firstName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(firstName);
		}

		if (lastName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(lastName);
		}

		if (activeFlag == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(activeFlag);
		}

		if (birthday == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(birthday);
		}

		objectOutput.writeLong(contactId);

		if (userType == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userType);
		}

		objectOutput.writeLong(brokerId);
		objectOutput.writeLong(fundId);

		if (nationality == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(nationality);
		}

		if (idIssueCountry == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(idIssueCountry);
		}

		if (idType == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(idType);
		}

		if (idNumber == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(idNumber);
		}

		objectOutput.writeLong(photoImage);
		objectOutput.writeLong(docId);

		if (ssn == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ssn);
		}

		if (ein == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ein);
		}

		if (income == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(income);
		}

		if (asset == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(asset);
		}

		if (isValidated == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(isValidated);
		}

		if (isCreddited == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(isCreddited);
		}

		objectOutput.writeLong(addTime);
		objectOutput.writeLong(addUser);
	}

	public String userId;
	public String firstName;
	public String lastName;
	public String activeFlag;
	public String birthday;
	public long contactId;
	public String userType;
	public long brokerId;
	public long fundId;
	public String nationality;
	public String idIssueCountry;
	public String idType;
	public String idNumber;
	public long photoImage;
	public long docId;
	public String ssn;
	public String ein;
	public String income;
	public String asset;
	public String isValidated;
	public String isCreddited;
	public long addTime;
	public long addUser;
}