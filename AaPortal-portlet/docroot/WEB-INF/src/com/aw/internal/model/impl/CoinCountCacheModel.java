/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model.impl;

import com.aw.internal.model.CoinCount;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing CoinCount in entity cache.
 *
 * @author zl
 * @see CoinCount
 * @generated
 */
public class CoinCountCacheModel implements CacheModel<CoinCount>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(17);

		sb.append("{phase=");
		sb.append(phase);
		sb.append(", totalAmount=");
		sb.append(totalAmount);
		sb.append(", endFlag=");
		sb.append(endFlag);
		sb.append(", rollToPhase=");
		sb.append(rollToPhase);
		sb.append(", rollToAmt=");
		sb.append(rollToAmt);
		sb.append(", limit=");
		sb.append(limit);
		sb.append(", rollOverAmt=");
		sb.append(rollOverAmt);
		sb.append(", lastOrderId=");
		sb.append(lastOrderId);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public CoinCount toEntityModel() {
		CoinCountImpl coinCountImpl = new CoinCountImpl();

		coinCountImpl.setPhase(phase);
		coinCountImpl.setTotalAmount(totalAmount);

		if (endFlag == null) {
			coinCountImpl.setEndFlag(StringPool.BLANK);
		}
		else {
			coinCountImpl.setEndFlag(endFlag);
		}

		coinCountImpl.setRollToPhase(rollToPhase);
		coinCountImpl.setRollToAmt(rollToAmt);
		coinCountImpl.setLimit(limit);
		coinCountImpl.setRollOverAmt(rollOverAmt);

		if (lastOrderId == null) {
			coinCountImpl.setLastOrderId(StringPool.BLANK);
		}
		else {
			coinCountImpl.setLastOrderId(lastOrderId);
		}

		coinCountImpl.resetOriginalValues();

		return coinCountImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		phase = objectInput.readInt();
		totalAmount = objectInput.readLong();
		endFlag = objectInput.readUTF();
		rollToPhase = objectInput.readInt();
		rollToAmt = objectInput.readLong();
		limit = objectInput.readLong();
		rollOverAmt = objectInput.readLong();
		lastOrderId = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeInt(phase);
		objectOutput.writeLong(totalAmount);

		if (endFlag == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(endFlag);
		}

		objectOutput.writeInt(rollToPhase);
		objectOutput.writeLong(rollToAmt);
		objectOutput.writeLong(limit);
		objectOutput.writeLong(rollOverAmt);

		if (lastOrderId == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(lastOrderId);
		}
	}

	public int phase;
	public long totalAmount;
	public String endFlag;
	public int rollToPhase;
	public long rollToAmt;
	public long limit;
	public long rollOverAmt;
	public String lastOrderId;
}