/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model.impl;

import com.aw.internal.model.BrokerInfo;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing BrokerInfo in entity cache.
 *
 * @author zl
 * @see BrokerInfo
 * @generated
 */
public class BrokerInfoCacheModel implements CacheModel<BrokerInfo>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{brokerId=");
		sb.append(brokerId);
		sb.append(", licenseId=");
		sb.append(licenseId);
		sb.append(", licenseCountry=");
		sb.append(licenseCountry);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public BrokerInfo toEntityModel() {
		BrokerInfoImpl brokerInfoImpl = new BrokerInfoImpl();

		brokerInfoImpl.setBrokerId(brokerId);

		if (licenseId == null) {
			brokerInfoImpl.setLicenseId(StringPool.BLANK);
		}
		else {
			brokerInfoImpl.setLicenseId(licenseId);
		}

		if (licenseCountry == null) {
			brokerInfoImpl.setLicenseCountry(StringPool.BLANK);
		}
		else {
			brokerInfoImpl.setLicenseCountry(licenseCountry);
		}

		brokerInfoImpl.resetOriginalValues();

		return brokerInfoImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		brokerId = objectInput.readLong();
		licenseId = objectInput.readUTF();
		licenseCountry = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(brokerId);

		if (licenseId == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(licenseId);
		}

		if (licenseCountry == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(licenseCountry);
		}
	}

	public long brokerId;
	public String licenseId;
	public String licenseCountry;
}