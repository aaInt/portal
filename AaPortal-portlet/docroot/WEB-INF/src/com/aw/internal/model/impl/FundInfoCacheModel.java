/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model.impl;

import com.aw.internal.model.FundInfo;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing FundInfo in entity cache.
 *
 * @author zl
 * @see FundInfo
 * @generated
 */
public class FundInfoCacheModel implements CacheModel<FundInfo>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{fundId=");
		sb.append(fundId);
		sb.append(", name=");
		sb.append(name);
		sb.append(", contactId=");
		sb.append(contactId);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public FundInfo toEntityModel() {
		FundInfoImpl fundInfoImpl = new FundInfoImpl();

		fundInfoImpl.setFundId(fundId);

		if (name == null) {
			fundInfoImpl.setName(StringPool.BLANK);
		}
		else {
			fundInfoImpl.setName(name);
		}

		fundInfoImpl.setContactId(contactId);

		fundInfoImpl.resetOriginalValues();

		return fundInfoImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		fundId = objectInput.readLong();
		name = objectInput.readUTF();
		contactId = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(fundId);

		if (name == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(name);
		}

		objectOutput.writeLong(contactId);
	}

	public long fundId;
	public String name;
	public long contactId;
}