/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model.impl;

import com.aw.internal.model.FailReason;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing FailReason in entity cache.
 *
 * @author zl
 * @see FailReason
 * @generated
 */
public class FailReasonCacheModel implements CacheModel<FailReason>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{rsnCd=");
		sb.append(rsnCd);
		sb.append(", desc=");
		sb.append(desc);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public FailReason toEntityModel() {
		FailReasonImpl failReasonImpl = new FailReasonImpl();

		if (rsnCd == null) {
			failReasonImpl.setRsnCd(StringPool.BLANK);
		}
		else {
			failReasonImpl.setRsnCd(rsnCd);
		}

		if (desc == null) {
			failReasonImpl.setDesc(StringPool.BLANK);
		}
		else {
			failReasonImpl.setDesc(desc);
		}

		failReasonImpl.resetOriginalValues();

		return failReasonImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		rsnCd = objectInput.readUTF();
		desc = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (rsnCd == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(rsnCd);
		}

		if (desc == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(desc);
		}
	}

	public String rsnCd;
	public String desc;
}