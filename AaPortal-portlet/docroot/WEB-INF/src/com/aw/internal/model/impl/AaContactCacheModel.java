/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model.impl;

import com.aw.internal.model.AaContact;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing AaContact in entity cache.
 *
 * @author zl
 * @see AaContact
 * @generated
 */
public class AaContactCacheModel implements CacheModel<AaContact>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(43);

		sb.append("{contactId=");
		sb.append(contactId);
		sb.append(", phone=");
		sb.append(phone);
		sb.append(", areaCode=");
		sb.append(areaCode);
		sb.append(", email=");
		sb.append(email);
		sb.append(", wechat=");
		sb.append(wechat);
		sb.append(", facebook=");
		sb.append(facebook);
		sb.append(", twitter=");
		sb.append(twitter);
		sb.append(", weibo=");
		sb.append(weibo);
		sb.append(", street1=");
		sb.append(street1);
		sb.append(", street2=");
		sb.append(street2);
		sb.append(", city=");
		sb.append(city);
		sb.append(", state=");
		sb.append(state);
		sb.append(", country=");
		sb.append(country);
		sb.append(", zipCode=");
		sb.append(zipCode);
		sb.append(", isMailDiff=");
		sb.append(isMailDiff);
		sb.append(", mailstreet1=");
		sb.append(mailstreet1);
		sb.append(", mailstreet2=");
		sb.append(mailstreet2);
		sb.append(", mailcity=");
		sb.append(mailcity);
		sb.append(", mailstate=");
		sb.append(mailstate);
		sb.append(", mailcountry=");
		sb.append(mailcountry);
		sb.append(", mailzipCode=");
		sb.append(mailzipCode);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public AaContact toEntityModel() {
		AaContactImpl aaContactImpl = new AaContactImpl();

		aaContactImpl.setContactId(contactId);
		aaContactImpl.setPhone(phone);
		aaContactImpl.setAreaCode(areaCode);

		if (email == null) {
			aaContactImpl.setEmail(StringPool.BLANK);
		}
		else {
			aaContactImpl.setEmail(email);
		}

		if (wechat == null) {
			aaContactImpl.setWechat(StringPool.BLANK);
		}
		else {
			aaContactImpl.setWechat(wechat);
		}

		if (facebook == null) {
			aaContactImpl.setFacebook(StringPool.BLANK);
		}
		else {
			aaContactImpl.setFacebook(facebook);
		}

		if (twitter == null) {
			aaContactImpl.setTwitter(StringPool.BLANK);
		}
		else {
			aaContactImpl.setTwitter(twitter);
		}

		if (weibo == null) {
			aaContactImpl.setWeibo(StringPool.BLANK);
		}
		else {
			aaContactImpl.setWeibo(weibo);
		}

		if (street1 == null) {
			aaContactImpl.setStreet1(StringPool.BLANK);
		}
		else {
			aaContactImpl.setStreet1(street1);
		}

		if (street2 == null) {
			aaContactImpl.setStreet2(StringPool.BLANK);
		}
		else {
			aaContactImpl.setStreet2(street2);
		}

		if (city == null) {
			aaContactImpl.setCity(StringPool.BLANK);
		}
		else {
			aaContactImpl.setCity(city);
		}

		if (state == null) {
			aaContactImpl.setState(StringPool.BLANK);
		}
		else {
			aaContactImpl.setState(state);
		}

		if (country == null) {
			aaContactImpl.setCountry(StringPool.BLANK);
		}
		else {
			aaContactImpl.setCountry(country);
		}

		if (zipCode == null) {
			aaContactImpl.setZipCode(StringPool.BLANK);
		}
		else {
			aaContactImpl.setZipCode(zipCode);
		}

		if (isMailDiff == null) {
			aaContactImpl.setIsMailDiff(StringPool.BLANK);
		}
		else {
			aaContactImpl.setIsMailDiff(isMailDiff);
		}

		if (mailstreet1 == null) {
			aaContactImpl.setMailstreet1(StringPool.BLANK);
		}
		else {
			aaContactImpl.setMailstreet1(mailstreet1);
		}

		if (mailstreet2 == null) {
			aaContactImpl.setMailstreet2(StringPool.BLANK);
		}
		else {
			aaContactImpl.setMailstreet2(mailstreet2);
		}

		if (mailcity == null) {
			aaContactImpl.setMailcity(StringPool.BLANK);
		}
		else {
			aaContactImpl.setMailcity(mailcity);
		}

		if (mailstate == null) {
			aaContactImpl.setMailstate(StringPool.BLANK);
		}
		else {
			aaContactImpl.setMailstate(mailstate);
		}

		if (mailcountry == null) {
			aaContactImpl.setMailcountry(StringPool.BLANK);
		}
		else {
			aaContactImpl.setMailcountry(mailcountry);
		}

		if (mailzipCode == null) {
			aaContactImpl.setMailzipCode(StringPool.BLANK);
		}
		else {
			aaContactImpl.setMailzipCode(mailzipCode);
		}

		aaContactImpl.resetOriginalValues();

		return aaContactImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		contactId = objectInput.readLong();
		phone = objectInput.readLong();
		areaCode = objectInput.readInt();
		email = objectInput.readUTF();
		wechat = objectInput.readUTF();
		facebook = objectInput.readUTF();
		twitter = objectInput.readUTF();
		weibo = objectInput.readUTF();
		street1 = objectInput.readUTF();
		street2 = objectInput.readUTF();
		city = objectInput.readUTF();
		state = objectInput.readUTF();
		country = objectInput.readUTF();
		zipCode = objectInput.readUTF();
		isMailDiff = objectInput.readUTF();
		mailstreet1 = objectInput.readUTF();
		mailstreet2 = objectInput.readUTF();
		mailcity = objectInput.readUTF();
		mailstate = objectInput.readUTF();
		mailcountry = objectInput.readUTF();
		mailzipCode = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(contactId);
		objectOutput.writeLong(phone);
		objectOutput.writeInt(areaCode);

		if (email == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(email);
		}

		if (wechat == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(wechat);
		}

		if (facebook == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(facebook);
		}

		if (twitter == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(twitter);
		}

		if (weibo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(weibo);
		}

		if (street1 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(street1);
		}

		if (street2 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(street2);
		}

		if (city == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(city);
		}

		if (state == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(state);
		}

		if (country == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(country);
		}

		if (zipCode == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(zipCode);
		}

		if (isMailDiff == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(isMailDiff);
		}

		if (mailstreet1 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(mailstreet1);
		}

		if (mailstreet2 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(mailstreet2);
		}

		if (mailcity == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(mailcity);
		}

		if (mailstate == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(mailstate);
		}

		if (mailcountry == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(mailcountry);
		}

		if (mailzipCode == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(mailzipCode);
		}
	}

	public long contactId;
	public long phone;
	public int areaCode;
	public String email;
	public String wechat;
	public String facebook;
	public String twitter;
	public String weibo;
	public String street1;
	public String street2;
	public String city;
	public String state;
	public String country;
	public String zipCode;
	public String isMailDiff;
	public String mailstreet1;
	public String mailstreet2;
	public String mailcity;
	public String mailstate;
	public String mailcountry;
	public String mailzipCode;
}