/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model.impl;

import com.aw.internal.model.CoinOrder;
import com.aw.internal.model.CoinOrderModel;
import com.aw.internal.model.CoinOrderSoap;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.json.JSON;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.sql.Types;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The base model implementation for the CoinOrder service. Represents a row in the &quot;AW_CoinOrder&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface {@link com.aw.internal.model.CoinOrderModel} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link CoinOrderImpl}.
 * </p>
 *
 * @author zl
 * @see CoinOrderImpl
 * @see com.aw.internal.model.CoinOrder
 * @see com.aw.internal.model.CoinOrderModel
 * @generated
 */
@JSON(strict = true)
public class CoinOrderModelImpl extends BaseModelImpl<CoinOrder>
	implements CoinOrderModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a coin order model instance should use the {@link com.aw.internal.model.CoinOrder} interface instead.
	 */
	public static final String TABLE_NAME = "AW_CoinOrder";
	public static final Object[][] TABLE_COLUMNS = {
			{ "orderId", Types.VARCHAR },
			{ "userId", Types.VARCHAR },
			{ "saleRegion", Types.VARCHAR },
			{ "brokerId", Types.BIGINT },
			{ "salesId", Types.BIGINT },
			{ "activeFlag", Types.VARCHAR },
			{ "salePhase", Types.INTEGER },
			{ "unitPrice", Types.DOUBLE },
			{ "totalPrice", Types.DOUBLE },
			{ "currency_", Types.VARCHAR },
			{ "saleAmount", Types.BIGINT },
			{ "promoAmount", Types.BIGINT },
			{ "totalAmount", Types.BIGINT },
			{ "rsnCd", Types.VARCHAR },
			{ "verifyFlag", Types.VARCHAR },
			{ "docSentFlag", Types.VARCHAR },
			{ "docCarrier", Types.VARCHAR },
			{ "docTrackingNo", Types.VARCHAR },
			{ "docSignFlag", Types.VARCHAR },
			{ "paidFlag", Types.VARCHAR },
			{ "approvalFlag", Types.VARCHAR },
			{ "certSentFlag", Types.VARCHAR },
			{ "certCarrier", Types.VARCHAR },
			{ "certTrackingNo", Types.VARCHAR },
			{ "completeFlag", Types.VARCHAR },
			{ "caseCloser", Types.BIGINT },
			{ "addTime", Types.TIMESTAMP }
		};
	public static final String TABLE_SQL_CREATE = "create table AW_CoinOrder (orderId VARCHAR(75) not null primary key,userId VARCHAR(75) null,saleRegion VARCHAR(75) null,brokerId LONG,salesId LONG,activeFlag VARCHAR(75) null,salePhase INTEGER,unitPrice DOUBLE,totalPrice DOUBLE,currency_ VARCHAR(75) null,saleAmount LONG,promoAmount LONG,totalAmount LONG,rsnCd VARCHAR(75) null,verifyFlag VARCHAR(75) null,docSentFlag VARCHAR(75) null,docCarrier VARCHAR(75) null,docTrackingNo VARCHAR(75) null,docSignFlag VARCHAR(75) null,paidFlag VARCHAR(75) null,approvalFlag VARCHAR(75) null,certSentFlag VARCHAR(75) null,certCarrier VARCHAR(75) null,certTrackingNo VARCHAR(75) null,completeFlag VARCHAR(75) null,caseCloser LONG,addTime DATE null)";
	public static final String TABLE_SQL_DROP = "drop table AW_CoinOrder";
	public static final String ORDER_BY_JPQL = " ORDER BY coinOrder.addTime DESC";
	public static final String ORDER_BY_SQL = " ORDER BY AW_CoinOrder.addTime DESC";
	public static final String DATA_SOURCE = "liferayDataSource";
	public static final String SESSION_FACTORY = "liferaySessionFactory";
	public static final String TX_MANAGER = "liferayTransactionManager";
	public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.entity.cache.enabled.com.aw.internal.model.CoinOrder"),
			true);
	public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.finder.cache.enabled.com.aw.internal.model.CoinOrder"),
			true);
	public static final boolean COLUMN_BITMASK_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.column.bitmask.enabled.com.aw.internal.model.CoinOrder"),
			true);
	public static long ACTIVEFLAG_COLUMN_BITMASK = 1L;
	public static long COMPLETEFLAG_COLUMN_BITMASK = 2L;
	public static long ADDTIME_COLUMN_BITMASK = 4L;

	/**
	 * Converts the soap model instance into a normal model instance.
	 *
	 * @param soapModel the soap model instance to convert
	 * @return the normal model instance
	 */
	public static CoinOrder toModel(CoinOrderSoap soapModel) {
		if (soapModel == null) {
			return null;
		}

		CoinOrder model = new CoinOrderImpl();

		model.setOrderId(soapModel.getOrderId());
		model.setUserId(soapModel.getUserId());
		model.setSaleRegion(soapModel.getSaleRegion());
		model.setBrokerId(soapModel.getBrokerId());
		model.setSalesId(soapModel.getSalesId());
		model.setActiveFlag(soapModel.getActiveFlag());
		model.setSalePhase(soapModel.getSalePhase());
		model.setUnitPrice(soapModel.getUnitPrice());
		model.setTotalPrice(soapModel.getTotalPrice());
		model.setCurrency(soapModel.getCurrency());
		model.setSaleAmount(soapModel.getSaleAmount());
		model.setPromoAmount(soapModel.getPromoAmount());
		model.setTotalAmount(soapModel.getTotalAmount());
		model.setRsnCd(soapModel.getRsnCd());
		model.setVerifyFlag(soapModel.getVerifyFlag());
		model.setDocSentFlag(soapModel.getDocSentFlag());
		model.setDocCarrier(soapModel.getDocCarrier());
		model.setDocTrackingNo(soapModel.getDocTrackingNo());
		model.setDocSignFlag(soapModel.getDocSignFlag());
		model.setPaidFlag(soapModel.getPaidFlag());
		model.setApprovalFlag(soapModel.getApprovalFlag());
		model.setCertSentFlag(soapModel.getCertSentFlag());
		model.setCertCarrier(soapModel.getCertCarrier());
		model.setCertTrackingNo(soapModel.getCertTrackingNo());
		model.setCompleteFlag(soapModel.getCompleteFlag());
		model.setCaseCloser(soapModel.getCaseCloser());
		model.setAddTime(soapModel.getAddTime());

		return model;
	}

	/**
	 * Converts the soap model instances into normal model instances.
	 *
	 * @param soapModels the soap model instances to convert
	 * @return the normal model instances
	 */
	public static List<CoinOrder> toModels(CoinOrderSoap[] soapModels) {
		if (soapModels == null) {
			return null;
		}

		List<CoinOrder> models = new ArrayList<CoinOrder>(soapModels.length);

		for (CoinOrderSoap soapModel : soapModels) {
			models.add(toModel(soapModel));
		}

		return models;
	}

	public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.util.service.ServiceProps.get(
				"lock.expiration.time.com.aw.internal.model.CoinOrder"));

	public CoinOrderModelImpl() {
	}

	@Override
	public String getPrimaryKey() {
		return _orderId;
	}

	@Override
	public void setPrimaryKey(String primaryKey) {
		setOrderId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _orderId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((String)primaryKeyObj);
	}

	@Override
	public Class<?> getModelClass() {
		return CoinOrder.class;
	}

	@Override
	public String getModelClassName() {
		return CoinOrder.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("orderId", getOrderId());
		attributes.put("userId", getUserId());
		attributes.put("saleRegion", getSaleRegion());
		attributes.put("brokerId", getBrokerId());
		attributes.put("salesId", getSalesId());
		attributes.put("activeFlag", getActiveFlag());
		attributes.put("salePhase", getSalePhase());
		attributes.put("unitPrice", getUnitPrice());
		attributes.put("totalPrice", getTotalPrice());
		attributes.put("currency", getCurrency());
		attributes.put("saleAmount", getSaleAmount());
		attributes.put("promoAmount", getPromoAmount());
		attributes.put("totalAmount", getTotalAmount());
		attributes.put("rsnCd", getRsnCd());
		attributes.put("verifyFlag", getVerifyFlag());
		attributes.put("docSentFlag", getDocSentFlag());
		attributes.put("docCarrier", getDocCarrier());
		attributes.put("docTrackingNo", getDocTrackingNo());
		attributes.put("docSignFlag", getDocSignFlag());
		attributes.put("paidFlag", getPaidFlag());
		attributes.put("approvalFlag", getApprovalFlag());
		attributes.put("certSentFlag", getCertSentFlag());
		attributes.put("certCarrier", getCertCarrier());
		attributes.put("certTrackingNo", getCertTrackingNo());
		attributes.put("completeFlag", getCompleteFlag());
		attributes.put("caseCloser", getCaseCloser());
		attributes.put("addTime", getAddTime());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String orderId = (String)attributes.get("orderId");

		if (orderId != null) {
			setOrderId(orderId);
		}

		String userId = (String)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String saleRegion = (String)attributes.get("saleRegion");

		if (saleRegion != null) {
			setSaleRegion(saleRegion);
		}

		Long brokerId = (Long)attributes.get("brokerId");

		if (brokerId != null) {
			setBrokerId(brokerId);
		}

		Long salesId = (Long)attributes.get("salesId");

		if (salesId != null) {
			setSalesId(salesId);
		}

		String activeFlag = (String)attributes.get("activeFlag");

		if (activeFlag != null) {
			setActiveFlag(activeFlag);
		}

		Integer salePhase = (Integer)attributes.get("salePhase");

		if (salePhase != null) {
			setSalePhase(salePhase);
		}

		Double unitPrice = (Double)attributes.get("unitPrice");

		if (unitPrice != null) {
			setUnitPrice(unitPrice);
		}

		Double totalPrice = (Double)attributes.get("totalPrice");

		if (totalPrice != null) {
			setTotalPrice(totalPrice);
		}

		String currency = (String)attributes.get("currency");

		if (currency != null) {
			setCurrency(currency);
		}

		Long saleAmount = (Long)attributes.get("saleAmount");

		if (saleAmount != null) {
			setSaleAmount(saleAmount);
		}

		Long promoAmount = (Long)attributes.get("promoAmount");

		if (promoAmount != null) {
			setPromoAmount(promoAmount);
		}

		Long totalAmount = (Long)attributes.get("totalAmount");

		if (totalAmount != null) {
			setTotalAmount(totalAmount);
		}

		String rsnCd = (String)attributes.get("rsnCd");

		if (rsnCd != null) {
			setRsnCd(rsnCd);
		}

		String verifyFlag = (String)attributes.get("verifyFlag");

		if (verifyFlag != null) {
			setVerifyFlag(verifyFlag);
		}

		String docSentFlag = (String)attributes.get("docSentFlag");

		if (docSentFlag != null) {
			setDocSentFlag(docSentFlag);
		}

		String docCarrier = (String)attributes.get("docCarrier");

		if (docCarrier != null) {
			setDocCarrier(docCarrier);
		}

		String docTrackingNo = (String)attributes.get("docTrackingNo");

		if (docTrackingNo != null) {
			setDocTrackingNo(docTrackingNo);
		}

		String docSignFlag = (String)attributes.get("docSignFlag");

		if (docSignFlag != null) {
			setDocSignFlag(docSignFlag);
		}

		String paidFlag = (String)attributes.get("paidFlag");

		if (paidFlag != null) {
			setPaidFlag(paidFlag);
		}

		String approvalFlag = (String)attributes.get("approvalFlag");

		if (approvalFlag != null) {
			setApprovalFlag(approvalFlag);
		}

		String certSentFlag = (String)attributes.get("certSentFlag");

		if (certSentFlag != null) {
			setCertSentFlag(certSentFlag);
		}

		String certCarrier = (String)attributes.get("certCarrier");

		if (certCarrier != null) {
			setCertCarrier(certCarrier);
		}

		String certTrackingNo = (String)attributes.get("certTrackingNo");

		if (certTrackingNo != null) {
			setCertTrackingNo(certTrackingNo);
		}

		String completeFlag = (String)attributes.get("completeFlag");

		if (completeFlag != null) {
			setCompleteFlag(completeFlag);
		}

		Long caseCloser = (Long)attributes.get("caseCloser");

		if (caseCloser != null) {
			setCaseCloser(caseCloser);
		}

		Date addTime = (Date)attributes.get("addTime");

		if (addTime != null) {
			setAddTime(addTime);
		}
	}

	@JSON
	@Override
	public String getOrderId() {
		if (_orderId == null) {
			return StringPool.BLANK;
		}
		else {
			return _orderId;
		}
	}

	@Override
	public void setOrderId(String orderId) {
		_orderId = orderId;
	}

	@JSON
	@Override
	public String getUserId() {
		if (_userId == null) {
			return StringPool.BLANK;
		}
		else {
			return _userId;
		}
	}

	@Override
	public void setUserId(String userId) {
		_userId = userId;
	}

	@JSON
	@Override
	public String getSaleRegion() {
		if (_saleRegion == null) {
			return StringPool.BLANK;
		}
		else {
			return _saleRegion;
		}
	}

	@Override
	public void setSaleRegion(String saleRegion) {
		_saleRegion = saleRegion;
	}

	@JSON
	@Override
	public long getBrokerId() {
		return _brokerId;
	}

	@Override
	public void setBrokerId(long brokerId) {
		_brokerId = brokerId;
	}

	@JSON
	@Override
	public long getSalesId() {
		return _salesId;
	}

	@Override
	public void setSalesId(long salesId) {
		_salesId = salesId;
	}

	@JSON
	@Override
	public String getActiveFlag() {
		if (_activeFlag == null) {
			return StringPool.BLANK;
		}
		else {
			return _activeFlag;
		}
	}

	@Override
	public void setActiveFlag(String activeFlag) {
		_columnBitmask |= ACTIVEFLAG_COLUMN_BITMASK;

		if (_originalActiveFlag == null) {
			_originalActiveFlag = _activeFlag;
		}

		_activeFlag = activeFlag;
	}

	public String getOriginalActiveFlag() {
		return GetterUtil.getString(_originalActiveFlag);
	}

	@JSON
	@Override
	public int getSalePhase() {
		return _salePhase;
	}

	@Override
	public void setSalePhase(int salePhase) {
		_salePhase = salePhase;
	}

	@JSON
	@Override
	public double getUnitPrice() {
		return _unitPrice;
	}

	@Override
	public void setUnitPrice(double unitPrice) {
		_unitPrice = unitPrice;
	}

	@JSON
	@Override
	public double getTotalPrice() {
		return _totalPrice;
	}

	@Override
	public void setTotalPrice(double totalPrice) {
		_totalPrice = totalPrice;
	}

	@JSON
	@Override
	public String getCurrency() {
		if (_currency == null) {
			return StringPool.BLANK;
		}
		else {
			return _currency;
		}
	}

	@Override
	public void setCurrency(String currency) {
		_currency = currency;
	}

	@JSON
	@Override
	public long getSaleAmount() {
		return _saleAmount;
	}

	@Override
	public void setSaleAmount(long saleAmount) {
		_saleAmount = saleAmount;
	}

	@JSON
	@Override
	public long getPromoAmount() {
		return _promoAmount;
	}

	@Override
	public void setPromoAmount(long promoAmount) {
		_promoAmount = promoAmount;
	}

	@JSON
	@Override
	public long getTotalAmount() {
		return _totalAmount;
	}

	@Override
	public void setTotalAmount(long totalAmount) {
		_totalAmount = totalAmount;
	}

	@JSON
	@Override
	public String getRsnCd() {
		if (_rsnCd == null) {
			return StringPool.BLANK;
		}
		else {
			return _rsnCd;
		}
	}

	@Override
	public void setRsnCd(String rsnCd) {
		_rsnCd = rsnCd;
	}

	@JSON
	@Override
	public String getVerifyFlag() {
		if (_verifyFlag == null) {
			return StringPool.BLANK;
		}
		else {
			return _verifyFlag;
		}
	}

	@Override
	public void setVerifyFlag(String verifyFlag) {
		_verifyFlag = verifyFlag;
	}

	@JSON
	@Override
	public String getDocSentFlag() {
		if (_docSentFlag == null) {
			return StringPool.BLANK;
		}
		else {
			return _docSentFlag;
		}
	}

	@Override
	public void setDocSentFlag(String docSentFlag) {
		_docSentFlag = docSentFlag;
	}

	@JSON
	@Override
	public String getDocCarrier() {
		if (_docCarrier == null) {
			return StringPool.BLANK;
		}
		else {
			return _docCarrier;
		}
	}

	@Override
	public void setDocCarrier(String docCarrier) {
		_docCarrier = docCarrier;
	}

	@JSON
	@Override
	public String getDocTrackingNo() {
		if (_docTrackingNo == null) {
			return StringPool.BLANK;
		}
		else {
			return _docTrackingNo;
		}
	}

	@Override
	public void setDocTrackingNo(String docTrackingNo) {
		_docTrackingNo = docTrackingNo;
	}

	@JSON
	@Override
	public String getDocSignFlag() {
		if (_docSignFlag == null) {
			return StringPool.BLANK;
		}
		else {
			return _docSignFlag;
		}
	}

	@Override
	public void setDocSignFlag(String docSignFlag) {
		_docSignFlag = docSignFlag;
	}

	@JSON
	@Override
	public String getPaidFlag() {
		if (_paidFlag == null) {
			return StringPool.BLANK;
		}
		else {
			return _paidFlag;
		}
	}

	@Override
	public void setPaidFlag(String paidFlag) {
		_paidFlag = paidFlag;
	}

	@JSON
	@Override
	public String getApprovalFlag() {
		if (_approvalFlag == null) {
			return StringPool.BLANK;
		}
		else {
			return _approvalFlag;
		}
	}

	@Override
	public void setApprovalFlag(String approvalFlag) {
		_approvalFlag = approvalFlag;
	}

	@JSON
	@Override
	public String getCertSentFlag() {
		if (_certSentFlag == null) {
			return StringPool.BLANK;
		}
		else {
			return _certSentFlag;
		}
	}

	@Override
	public void setCertSentFlag(String certSentFlag) {
		_certSentFlag = certSentFlag;
	}

	@JSON
	@Override
	public String getCertCarrier() {
		if (_certCarrier == null) {
			return StringPool.BLANK;
		}
		else {
			return _certCarrier;
		}
	}

	@Override
	public void setCertCarrier(String certCarrier) {
		_certCarrier = certCarrier;
	}

	@JSON
	@Override
	public String getCertTrackingNo() {
		if (_certTrackingNo == null) {
			return StringPool.BLANK;
		}
		else {
			return _certTrackingNo;
		}
	}

	@Override
	public void setCertTrackingNo(String certTrackingNo) {
		_certTrackingNo = certTrackingNo;
	}

	@JSON
	@Override
	public String getCompleteFlag() {
		if (_completeFlag == null) {
			return StringPool.BLANK;
		}
		else {
			return _completeFlag;
		}
	}

	@Override
	public void setCompleteFlag(String completeFlag) {
		_columnBitmask |= COMPLETEFLAG_COLUMN_BITMASK;

		if (_originalCompleteFlag == null) {
			_originalCompleteFlag = _completeFlag;
		}

		_completeFlag = completeFlag;
	}

	public String getOriginalCompleteFlag() {
		return GetterUtil.getString(_originalCompleteFlag);
	}

	@JSON
	@Override
	public long getCaseCloser() {
		return _caseCloser;
	}

	@Override
	public void setCaseCloser(long caseCloser) {
		_caseCloser = caseCloser;
	}

	@JSON
	@Override
	public Date getAddTime() {
		return _addTime;
	}

	@Override
	public void setAddTime(Date addTime) {
		_columnBitmask = -1L;

		_addTime = addTime;
	}

	public long getColumnBitmask() {
		return _columnBitmask;
	}

	@Override
	public CoinOrder toEscapedModel() {
		if (_escapedModel == null) {
			_escapedModel = (CoinOrder)ProxyUtil.newProxyInstance(_classLoader,
					_escapedModelInterfaces, new AutoEscapeBeanHandler(this));
		}

		return _escapedModel;
	}

	@Override
	public Object clone() {
		CoinOrderImpl coinOrderImpl = new CoinOrderImpl();

		coinOrderImpl.setOrderId(getOrderId());
		coinOrderImpl.setUserId(getUserId());
		coinOrderImpl.setSaleRegion(getSaleRegion());
		coinOrderImpl.setBrokerId(getBrokerId());
		coinOrderImpl.setSalesId(getSalesId());
		coinOrderImpl.setActiveFlag(getActiveFlag());
		coinOrderImpl.setSalePhase(getSalePhase());
		coinOrderImpl.setUnitPrice(getUnitPrice());
		coinOrderImpl.setTotalPrice(getTotalPrice());
		coinOrderImpl.setCurrency(getCurrency());
		coinOrderImpl.setSaleAmount(getSaleAmount());
		coinOrderImpl.setPromoAmount(getPromoAmount());
		coinOrderImpl.setTotalAmount(getTotalAmount());
		coinOrderImpl.setRsnCd(getRsnCd());
		coinOrderImpl.setVerifyFlag(getVerifyFlag());
		coinOrderImpl.setDocSentFlag(getDocSentFlag());
		coinOrderImpl.setDocCarrier(getDocCarrier());
		coinOrderImpl.setDocTrackingNo(getDocTrackingNo());
		coinOrderImpl.setDocSignFlag(getDocSignFlag());
		coinOrderImpl.setPaidFlag(getPaidFlag());
		coinOrderImpl.setApprovalFlag(getApprovalFlag());
		coinOrderImpl.setCertSentFlag(getCertSentFlag());
		coinOrderImpl.setCertCarrier(getCertCarrier());
		coinOrderImpl.setCertTrackingNo(getCertTrackingNo());
		coinOrderImpl.setCompleteFlag(getCompleteFlag());
		coinOrderImpl.setCaseCloser(getCaseCloser());
		coinOrderImpl.setAddTime(getAddTime());

		coinOrderImpl.resetOriginalValues();

		return coinOrderImpl;
	}

	@Override
	public int compareTo(CoinOrder coinOrder) {
		int value = 0;

		value = DateUtil.compareTo(getAddTime(), coinOrder.getAddTime());

		value = value * -1;

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CoinOrder)) {
			return false;
		}

		CoinOrder coinOrder = (CoinOrder)obj;

		String primaryKey = coinOrder.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public void resetOriginalValues() {
		CoinOrderModelImpl coinOrderModelImpl = this;

		coinOrderModelImpl._originalActiveFlag = coinOrderModelImpl._activeFlag;

		coinOrderModelImpl._originalCompleteFlag = coinOrderModelImpl._completeFlag;

		coinOrderModelImpl._columnBitmask = 0;
	}

	@Override
	public CacheModel<CoinOrder> toCacheModel() {
		CoinOrderCacheModel coinOrderCacheModel = new CoinOrderCacheModel();

		coinOrderCacheModel.orderId = getOrderId();

		String orderId = coinOrderCacheModel.orderId;

		if ((orderId != null) && (orderId.length() == 0)) {
			coinOrderCacheModel.orderId = null;
		}

		coinOrderCacheModel.userId = getUserId();

		String userId = coinOrderCacheModel.userId;

		if ((userId != null) && (userId.length() == 0)) {
			coinOrderCacheModel.userId = null;
		}

		coinOrderCacheModel.saleRegion = getSaleRegion();

		String saleRegion = coinOrderCacheModel.saleRegion;

		if ((saleRegion != null) && (saleRegion.length() == 0)) {
			coinOrderCacheModel.saleRegion = null;
		}

		coinOrderCacheModel.brokerId = getBrokerId();

		coinOrderCacheModel.salesId = getSalesId();

		coinOrderCacheModel.activeFlag = getActiveFlag();

		String activeFlag = coinOrderCacheModel.activeFlag;

		if ((activeFlag != null) && (activeFlag.length() == 0)) {
			coinOrderCacheModel.activeFlag = null;
		}

		coinOrderCacheModel.salePhase = getSalePhase();

		coinOrderCacheModel.unitPrice = getUnitPrice();

		coinOrderCacheModel.totalPrice = getTotalPrice();

		coinOrderCacheModel.currency = getCurrency();

		String currency = coinOrderCacheModel.currency;

		if ((currency != null) && (currency.length() == 0)) {
			coinOrderCacheModel.currency = null;
		}

		coinOrderCacheModel.saleAmount = getSaleAmount();

		coinOrderCacheModel.promoAmount = getPromoAmount();

		coinOrderCacheModel.totalAmount = getTotalAmount();

		coinOrderCacheModel.rsnCd = getRsnCd();

		String rsnCd = coinOrderCacheModel.rsnCd;

		if ((rsnCd != null) && (rsnCd.length() == 0)) {
			coinOrderCacheModel.rsnCd = null;
		}

		coinOrderCacheModel.verifyFlag = getVerifyFlag();

		String verifyFlag = coinOrderCacheModel.verifyFlag;

		if ((verifyFlag != null) && (verifyFlag.length() == 0)) {
			coinOrderCacheModel.verifyFlag = null;
		}

		coinOrderCacheModel.docSentFlag = getDocSentFlag();

		String docSentFlag = coinOrderCacheModel.docSentFlag;

		if ((docSentFlag != null) && (docSentFlag.length() == 0)) {
			coinOrderCacheModel.docSentFlag = null;
		}

		coinOrderCacheModel.docCarrier = getDocCarrier();

		String docCarrier = coinOrderCacheModel.docCarrier;

		if ((docCarrier != null) && (docCarrier.length() == 0)) {
			coinOrderCacheModel.docCarrier = null;
		}

		coinOrderCacheModel.docTrackingNo = getDocTrackingNo();

		String docTrackingNo = coinOrderCacheModel.docTrackingNo;

		if ((docTrackingNo != null) && (docTrackingNo.length() == 0)) {
			coinOrderCacheModel.docTrackingNo = null;
		}

		coinOrderCacheModel.docSignFlag = getDocSignFlag();

		String docSignFlag = coinOrderCacheModel.docSignFlag;

		if ((docSignFlag != null) && (docSignFlag.length() == 0)) {
			coinOrderCacheModel.docSignFlag = null;
		}

		coinOrderCacheModel.paidFlag = getPaidFlag();

		String paidFlag = coinOrderCacheModel.paidFlag;

		if ((paidFlag != null) && (paidFlag.length() == 0)) {
			coinOrderCacheModel.paidFlag = null;
		}

		coinOrderCacheModel.approvalFlag = getApprovalFlag();

		String approvalFlag = coinOrderCacheModel.approvalFlag;

		if ((approvalFlag != null) && (approvalFlag.length() == 0)) {
			coinOrderCacheModel.approvalFlag = null;
		}

		coinOrderCacheModel.certSentFlag = getCertSentFlag();

		String certSentFlag = coinOrderCacheModel.certSentFlag;

		if ((certSentFlag != null) && (certSentFlag.length() == 0)) {
			coinOrderCacheModel.certSentFlag = null;
		}

		coinOrderCacheModel.certCarrier = getCertCarrier();

		String certCarrier = coinOrderCacheModel.certCarrier;

		if ((certCarrier != null) && (certCarrier.length() == 0)) {
			coinOrderCacheModel.certCarrier = null;
		}

		coinOrderCacheModel.certTrackingNo = getCertTrackingNo();

		String certTrackingNo = coinOrderCacheModel.certTrackingNo;

		if ((certTrackingNo != null) && (certTrackingNo.length() == 0)) {
			coinOrderCacheModel.certTrackingNo = null;
		}

		coinOrderCacheModel.completeFlag = getCompleteFlag();

		String completeFlag = coinOrderCacheModel.completeFlag;

		if ((completeFlag != null) && (completeFlag.length() == 0)) {
			coinOrderCacheModel.completeFlag = null;
		}

		coinOrderCacheModel.caseCloser = getCaseCloser();

		Date addTime = getAddTime();

		if (addTime != null) {
			coinOrderCacheModel.addTime = addTime.getTime();
		}
		else {
			coinOrderCacheModel.addTime = Long.MIN_VALUE;
		}

		return coinOrderCacheModel;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(55);

		sb.append("{orderId=");
		sb.append(getOrderId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", saleRegion=");
		sb.append(getSaleRegion());
		sb.append(", brokerId=");
		sb.append(getBrokerId());
		sb.append(", salesId=");
		sb.append(getSalesId());
		sb.append(", activeFlag=");
		sb.append(getActiveFlag());
		sb.append(", salePhase=");
		sb.append(getSalePhase());
		sb.append(", unitPrice=");
		sb.append(getUnitPrice());
		sb.append(", totalPrice=");
		sb.append(getTotalPrice());
		sb.append(", currency=");
		sb.append(getCurrency());
		sb.append(", saleAmount=");
		sb.append(getSaleAmount());
		sb.append(", promoAmount=");
		sb.append(getPromoAmount());
		sb.append(", totalAmount=");
		sb.append(getTotalAmount());
		sb.append(", rsnCd=");
		sb.append(getRsnCd());
		sb.append(", verifyFlag=");
		sb.append(getVerifyFlag());
		sb.append(", docSentFlag=");
		sb.append(getDocSentFlag());
		sb.append(", docCarrier=");
		sb.append(getDocCarrier());
		sb.append(", docTrackingNo=");
		sb.append(getDocTrackingNo());
		sb.append(", docSignFlag=");
		sb.append(getDocSignFlag());
		sb.append(", paidFlag=");
		sb.append(getPaidFlag());
		sb.append(", approvalFlag=");
		sb.append(getApprovalFlag());
		sb.append(", certSentFlag=");
		sb.append(getCertSentFlag());
		sb.append(", certCarrier=");
		sb.append(getCertCarrier());
		sb.append(", certTrackingNo=");
		sb.append(getCertTrackingNo());
		sb.append(", completeFlag=");
		sb.append(getCompleteFlag());
		sb.append(", caseCloser=");
		sb.append(getCaseCloser());
		sb.append(", addTime=");
		sb.append(getAddTime());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(85);

		sb.append("<model><model-name>");
		sb.append("com.aw.internal.model.CoinOrder");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>orderId</column-name><column-value><![CDATA[");
		sb.append(getOrderId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>saleRegion</column-name><column-value><![CDATA[");
		sb.append(getSaleRegion());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>brokerId</column-name><column-value><![CDATA[");
		sb.append(getBrokerId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>salesId</column-name><column-value><![CDATA[");
		sb.append(getSalesId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>activeFlag</column-name><column-value><![CDATA[");
		sb.append(getActiveFlag());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>salePhase</column-name><column-value><![CDATA[");
		sb.append(getSalePhase());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>unitPrice</column-name><column-value><![CDATA[");
		sb.append(getUnitPrice());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>totalPrice</column-name><column-value><![CDATA[");
		sb.append(getTotalPrice());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>currency</column-name><column-value><![CDATA[");
		sb.append(getCurrency());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>saleAmount</column-name><column-value><![CDATA[");
		sb.append(getSaleAmount());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>promoAmount</column-name><column-value><![CDATA[");
		sb.append(getPromoAmount());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>totalAmount</column-name><column-value><![CDATA[");
		sb.append(getTotalAmount());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>rsnCd</column-name><column-value><![CDATA[");
		sb.append(getRsnCd());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>verifyFlag</column-name><column-value><![CDATA[");
		sb.append(getVerifyFlag());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>docSentFlag</column-name><column-value><![CDATA[");
		sb.append(getDocSentFlag());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>docCarrier</column-name><column-value><![CDATA[");
		sb.append(getDocCarrier());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>docTrackingNo</column-name><column-value><![CDATA[");
		sb.append(getDocTrackingNo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>docSignFlag</column-name><column-value><![CDATA[");
		sb.append(getDocSignFlag());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>paidFlag</column-name><column-value><![CDATA[");
		sb.append(getPaidFlag());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>approvalFlag</column-name><column-value><![CDATA[");
		sb.append(getApprovalFlag());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>certSentFlag</column-name><column-value><![CDATA[");
		sb.append(getCertSentFlag());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>certCarrier</column-name><column-value><![CDATA[");
		sb.append(getCertCarrier());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>certTrackingNo</column-name><column-value><![CDATA[");
		sb.append(getCertTrackingNo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>completeFlag</column-name><column-value><![CDATA[");
		sb.append(getCompleteFlag());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>caseCloser</column-name><column-value><![CDATA[");
		sb.append(getCaseCloser());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>addTime</column-name><column-value><![CDATA[");
		sb.append(getAddTime());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private static ClassLoader _classLoader = CoinOrder.class.getClassLoader();
	private static Class<?>[] _escapedModelInterfaces = new Class[] {
			CoinOrder.class
		};
	private String _orderId;
	private String _userId;
	private String _saleRegion;
	private long _brokerId;
	private long _salesId;
	private String _activeFlag;
	private String _originalActiveFlag;
	private int _salePhase;
	private double _unitPrice;
	private double _totalPrice;
	private String _currency;
	private long _saleAmount;
	private long _promoAmount;
	private long _totalAmount;
	private String _rsnCd;
	private String _verifyFlag;
	private String _docSentFlag;
	private String _docCarrier;
	private String _docTrackingNo;
	private String _docSignFlag;
	private String _paidFlag;
	private String _approvalFlag;
	private String _certSentFlag;
	private String _certCarrier;
	private String _certTrackingNo;
	private String _completeFlag;
	private String _originalCompleteFlag;
	private long _caseCloser;
	private Date _addTime;
	private long _columnBitmask;
	private CoinOrder _escapedModel;
}