/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model.impl;

import com.aw.internal.model.ActivePhase;
import com.aw.internal.model.ActivePhaseModel;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.sql.Types;

import java.util.HashMap;
import java.util.Map;

/**
 * The base model implementation for the ActivePhase service. Represents a row in the &quot;AW_ActivePhase&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface {@link com.aw.internal.model.ActivePhaseModel} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link ActivePhaseImpl}.
 * </p>
 *
 * @author zl
 * @see ActivePhaseImpl
 * @see com.aw.internal.model.ActivePhase
 * @see com.aw.internal.model.ActivePhaseModel
 * @generated
 */
public class ActivePhaseModelImpl extends BaseModelImpl<ActivePhase>
	implements ActivePhaseModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a active phase model instance should use the {@link com.aw.internal.model.ActivePhase} interface instead.
	 */
	public static final String TABLE_NAME = "AW_ActivePhase";
	public static final Object[][] TABLE_COLUMNS = {
			{ "region", Types.VARCHAR },
			{ "salePhase", Types.INTEGER }
		};
	public static final String TABLE_SQL_CREATE = "create table AW_ActivePhase (region VARCHAR(75) not null primary key,salePhase INTEGER)";
	public static final String TABLE_SQL_DROP = "drop table AW_ActivePhase";
	public static final String ORDER_BY_JPQL = " ORDER BY activePhase.region ASC";
	public static final String ORDER_BY_SQL = " ORDER BY AW_ActivePhase.region ASC";
	public static final String DATA_SOURCE = "liferayDataSource";
	public static final String SESSION_FACTORY = "liferaySessionFactory";
	public static final String TX_MANAGER = "liferayTransactionManager";
	public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.entity.cache.enabled.com.aw.internal.model.ActivePhase"),
			true);
	public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.finder.cache.enabled.com.aw.internal.model.ActivePhase"),
			true);
	public static final boolean COLUMN_BITMASK_ENABLED = false;
	public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.util.service.ServiceProps.get(
				"lock.expiration.time.com.aw.internal.model.ActivePhase"));

	public ActivePhaseModelImpl() {
	}

	@Override
	public String getPrimaryKey() {
		return _region;
	}

	@Override
	public void setPrimaryKey(String primaryKey) {
		setRegion(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _region;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((String)primaryKeyObj);
	}

	@Override
	public Class<?> getModelClass() {
		return ActivePhase.class;
	}

	@Override
	public String getModelClassName() {
		return ActivePhase.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("region", getRegion());
		attributes.put("salePhase", getSalePhase());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String region = (String)attributes.get("region");

		if (region != null) {
			setRegion(region);
		}

		Integer salePhase = (Integer)attributes.get("salePhase");

		if (salePhase != null) {
			setSalePhase(salePhase);
		}
	}

	@Override
	public String getRegion() {
		if (_region == null) {
			return StringPool.BLANK;
		}
		else {
			return _region;
		}
	}

	@Override
	public void setRegion(String region) {
		_region = region;
	}

	@Override
	public int getSalePhase() {
		return _salePhase;
	}

	@Override
	public void setSalePhase(int salePhase) {
		_salePhase = salePhase;
	}

	@Override
	public ActivePhase toEscapedModel() {
		if (_escapedModel == null) {
			_escapedModel = (ActivePhase)ProxyUtil.newProxyInstance(_classLoader,
					_escapedModelInterfaces, new AutoEscapeBeanHandler(this));
		}

		return _escapedModel;
	}

	@Override
	public Object clone() {
		ActivePhaseImpl activePhaseImpl = new ActivePhaseImpl();

		activePhaseImpl.setRegion(getRegion());
		activePhaseImpl.setSalePhase(getSalePhase());

		activePhaseImpl.resetOriginalValues();

		return activePhaseImpl;
	}

	@Override
	public int compareTo(ActivePhase activePhase) {
		String primaryKey = activePhase.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ActivePhase)) {
			return false;
		}

		ActivePhase activePhase = (ActivePhase)obj;

		String primaryKey = activePhase.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public void resetOriginalValues() {
	}

	@Override
	public CacheModel<ActivePhase> toCacheModel() {
		ActivePhaseCacheModel activePhaseCacheModel = new ActivePhaseCacheModel();

		activePhaseCacheModel.region = getRegion();

		String region = activePhaseCacheModel.region;

		if ((region != null) && (region.length() == 0)) {
			activePhaseCacheModel.region = null;
		}

		activePhaseCacheModel.salePhase = getSalePhase();

		return activePhaseCacheModel;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{region=");
		sb.append(getRegion());
		sb.append(", salePhase=");
		sb.append(getSalePhase());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(10);

		sb.append("<model><model-name>");
		sb.append("com.aw.internal.model.ActivePhase");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>region</column-name><column-value><![CDATA[");
		sb.append(getRegion());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>salePhase</column-name><column-value><![CDATA[");
		sb.append(getSalePhase());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private static ClassLoader _classLoader = ActivePhase.class.getClassLoader();
	private static Class<?>[] _escapedModelInterfaces = new Class[] {
			ActivePhase.class
		};
	private String _region;
	private int _salePhase;
	private ActivePhase _escapedModel;
}