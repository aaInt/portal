/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model.impl;

import com.aw.internal.model.ImageStore;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing ImageStore in entity cache.
 *
 * @author zl
 * @see ImageStore
 * @generated
 */
public class ImageStoreCacheModel implements CacheModel<ImageStore>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{imageId=");
		sb.append(imageId);
		sb.append(", activeFlag=");
		sb.append(activeFlag);
		sb.append(", addDate=");
		sb.append(addDate);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ImageStore toEntityModel() {
		ImageStoreImpl imageStoreImpl = new ImageStoreImpl();

		imageStoreImpl.setImageId(imageId);

		if (activeFlag == null) {
			imageStoreImpl.setActiveFlag(StringPool.BLANK);
		}
		else {
			imageStoreImpl.setActiveFlag(activeFlag);
		}

		if (addDate == Long.MIN_VALUE) {
			imageStoreImpl.setAddDate(null);
		}
		else {
			imageStoreImpl.setAddDate(new Date(addDate));
		}

		imageStoreImpl.resetOriginalValues();

		return imageStoreImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		imageId = objectInput.readLong();
		activeFlag = objectInput.readUTF();
		addDate = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(imageId);

		if (activeFlag == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(activeFlag);
		}

		objectOutput.writeLong(addDate);
	}

	public long imageId;
	public String activeFlag;
	public long addDate;
}