/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.model.impl;

import com.aw.internal.model.CoinOrder;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing CoinOrder in entity cache.
 *
 * @author zl
 * @see CoinOrder
 * @generated
 */
public class CoinOrderCacheModel implements CacheModel<CoinOrder>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(55);

		sb.append("{orderId=");
		sb.append(orderId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", saleRegion=");
		sb.append(saleRegion);
		sb.append(", brokerId=");
		sb.append(brokerId);
		sb.append(", salesId=");
		sb.append(salesId);
		sb.append(", activeFlag=");
		sb.append(activeFlag);
		sb.append(", salePhase=");
		sb.append(salePhase);
		sb.append(", unitPrice=");
		sb.append(unitPrice);
		sb.append(", totalPrice=");
		sb.append(totalPrice);
		sb.append(", currency=");
		sb.append(currency);
		sb.append(", saleAmount=");
		sb.append(saleAmount);
		sb.append(", promoAmount=");
		sb.append(promoAmount);
		sb.append(", totalAmount=");
		sb.append(totalAmount);
		sb.append(", rsnCd=");
		sb.append(rsnCd);
		sb.append(", verifyFlag=");
		sb.append(verifyFlag);
		sb.append(", docSentFlag=");
		sb.append(docSentFlag);
		sb.append(", docCarrier=");
		sb.append(docCarrier);
		sb.append(", docTrackingNo=");
		sb.append(docTrackingNo);
		sb.append(", docSignFlag=");
		sb.append(docSignFlag);
		sb.append(", paidFlag=");
		sb.append(paidFlag);
		sb.append(", approvalFlag=");
		sb.append(approvalFlag);
		sb.append(", certSentFlag=");
		sb.append(certSentFlag);
		sb.append(", certCarrier=");
		sb.append(certCarrier);
		sb.append(", certTrackingNo=");
		sb.append(certTrackingNo);
		sb.append(", completeFlag=");
		sb.append(completeFlag);
		sb.append(", caseCloser=");
		sb.append(caseCloser);
		sb.append(", addTime=");
		sb.append(addTime);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public CoinOrder toEntityModel() {
		CoinOrderImpl coinOrderImpl = new CoinOrderImpl();

		if (orderId == null) {
			coinOrderImpl.setOrderId(StringPool.BLANK);
		}
		else {
			coinOrderImpl.setOrderId(orderId);
		}

		if (userId == null) {
			coinOrderImpl.setUserId(StringPool.BLANK);
		}
		else {
			coinOrderImpl.setUserId(userId);
		}

		if (saleRegion == null) {
			coinOrderImpl.setSaleRegion(StringPool.BLANK);
		}
		else {
			coinOrderImpl.setSaleRegion(saleRegion);
		}

		coinOrderImpl.setBrokerId(brokerId);
		coinOrderImpl.setSalesId(salesId);

		if (activeFlag == null) {
			coinOrderImpl.setActiveFlag(StringPool.BLANK);
		}
		else {
			coinOrderImpl.setActiveFlag(activeFlag);
		}

		coinOrderImpl.setSalePhase(salePhase);
		coinOrderImpl.setUnitPrice(unitPrice);
		coinOrderImpl.setTotalPrice(totalPrice);

		if (currency == null) {
			coinOrderImpl.setCurrency(StringPool.BLANK);
		}
		else {
			coinOrderImpl.setCurrency(currency);
		}

		coinOrderImpl.setSaleAmount(saleAmount);
		coinOrderImpl.setPromoAmount(promoAmount);
		coinOrderImpl.setTotalAmount(totalAmount);

		if (rsnCd == null) {
			coinOrderImpl.setRsnCd(StringPool.BLANK);
		}
		else {
			coinOrderImpl.setRsnCd(rsnCd);
		}

		if (verifyFlag == null) {
			coinOrderImpl.setVerifyFlag(StringPool.BLANK);
		}
		else {
			coinOrderImpl.setVerifyFlag(verifyFlag);
		}

		if (docSentFlag == null) {
			coinOrderImpl.setDocSentFlag(StringPool.BLANK);
		}
		else {
			coinOrderImpl.setDocSentFlag(docSentFlag);
		}

		if (docCarrier == null) {
			coinOrderImpl.setDocCarrier(StringPool.BLANK);
		}
		else {
			coinOrderImpl.setDocCarrier(docCarrier);
		}

		if (docTrackingNo == null) {
			coinOrderImpl.setDocTrackingNo(StringPool.BLANK);
		}
		else {
			coinOrderImpl.setDocTrackingNo(docTrackingNo);
		}

		if (docSignFlag == null) {
			coinOrderImpl.setDocSignFlag(StringPool.BLANK);
		}
		else {
			coinOrderImpl.setDocSignFlag(docSignFlag);
		}

		if (paidFlag == null) {
			coinOrderImpl.setPaidFlag(StringPool.BLANK);
		}
		else {
			coinOrderImpl.setPaidFlag(paidFlag);
		}

		if (approvalFlag == null) {
			coinOrderImpl.setApprovalFlag(StringPool.BLANK);
		}
		else {
			coinOrderImpl.setApprovalFlag(approvalFlag);
		}

		if (certSentFlag == null) {
			coinOrderImpl.setCertSentFlag(StringPool.BLANK);
		}
		else {
			coinOrderImpl.setCertSentFlag(certSentFlag);
		}

		if (certCarrier == null) {
			coinOrderImpl.setCertCarrier(StringPool.BLANK);
		}
		else {
			coinOrderImpl.setCertCarrier(certCarrier);
		}

		if (certTrackingNo == null) {
			coinOrderImpl.setCertTrackingNo(StringPool.BLANK);
		}
		else {
			coinOrderImpl.setCertTrackingNo(certTrackingNo);
		}

		if (completeFlag == null) {
			coinOrderImpl.setCompleteFlag(StringPool.BLANK);
		}
		else {
			coinOrderImpl.setCompleteFlag(completeFlag);
		}

		coinOrderImpl.setCaseCloser(caseCloser);

		if (addTime == Long.MIN_VALUE) {
			coinOrderImpl.setAddTime(null);
		}
		else {
			coinOrderImpl.setAddTime(new Date(addTime));
		}

		coinOrderImpl.resetOriginalValues();

		return coinOrderImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		orderId = objectInput.readUTF();
		userId = objectInput.readUTF();
		saleRegion = objectInput.readUTF();
		brokerId = objectInput.readLong();
		salesId = objectInput.readLong();
		activeFlag = objectInput.readUTF();
		salePhase = objectInput.readInt();
		unitPrice = objectInput.readDouble();
		totalPrice = objectInput.readDouble();
		currency = objectInput.readUTF();
		saleAmount = objectInput.readLong();
		promoAmount = objectInput.readLong();
		totalAmount = objectInput.readLong();
		rsnCd = objectInput.readUTF();
		verifyFlag = objectInput.readUTF();
		docSentFlag = objectInput.readUTF();
		docCarrier = objectInput.readUTF();
		docTrackingNo = objectInput.readUTF();
		docSignFlag = objectInput.readUTF();
		paidFlag = objectInput.readUTF();
		approvalFlag = objectInput.readUTF();
		certSentFlag = objectInput.readUTF();
		certCarrier = objectInput.readUTF();
		certTrackingNo = objectInput.readUTF();
		completeFlag = objectInput.readUTF();
		caseCloser = objectInput.readLong();
		addTime = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (orderId == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(orderId);
		}

		if (userId == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userId);
		}

		if (saleRegion == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(saleRegion);
		}

		objectOutput.writeLong(brokerId);
		objectOutput.writeLong(salesId);

		if (activeFlag == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(activeFlag);
		}

		objectOutput.writeInt(salePhase);
		objectOutput.writeDouble(unitPrice);
		objectOutput.writeDouble(totalPrice);

		if (currency == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(currency);
		}

		objectOutput.writeLong(saleAmount);
		objectOutput.writeLong(promoAmount);
		objectOutput.writeLong(totalAmount);

		if (rsnCd == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(rsnCd);
		}

		if (verifyFlag == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(verifyFlag);
		}

		if (docSentFlag == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(docSentFlag);
		}

		if (docCarrier == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(docCarrier);
		}

		if (docTrackingNo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(docTrackingNo);
		}

		if (docSignFlag == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(docSignFlag);
		}

		if (paidFlag == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(paidFlag);
		}

		if (approvalFlag == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(approvalFlag);
		}

		if (certSentFlag == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(certSentFlag);
		}

		if (certCarrier == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(certCarrier);
		}

		if (certTrackingNo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(certTrackingNo);
		}

		if (completeFlag == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(completeFlag);
		}

		objectOutput.writeLong(caseCloser);
		objectOutput.writeLong(addTime);
	}

	public String orderId;
	public String userId;
	public String saleRegion;
	public long brokerId;
	public long salesId;
	public String activeFlag;
	public int salePhase;
	public double unitPrice;
	public double totalPrice;
	public String currency;
	public long saleAmount;
	public long promoAmount;
	public long totalAmount;
	public String rsnCd;
	public String verifyFlag;
	public String docSentFlag;
	public String docCarrier;
	public String docTrackingNo;
	public String docSignFlag;
	public String paidFlag;
	public String approvalFlag;
	public String certSentFlag;
	public String certCarrier;
	public String certTrackingNo;
	public String completeFlag;
	public long caseCloser;
	public long addTime;
}