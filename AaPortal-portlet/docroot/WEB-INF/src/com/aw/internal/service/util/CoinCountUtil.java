package com.aw.internal.service.util;

import com.aw.internal.model.CoinCount;
import com.aw.internal.service.CoinCountLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;

public class CoinCountUtil {
//	public synchronized boolean updateTotalCoinAmount(long amt){
//		
//	}
	
	public static synchronized boolean checkAndUpdate(long amt, int phase, String orderId){
		
		boolean updated = false;
		
		if(amt < 0){
						
			try {
				CoinCount cc = CoinCountLocalServiceUtil.fetchCoinCount(phase);
				if(cc != null){
					cc.setTotalAmount(cc.getTotalAmount()+amt);
					cc.setLastOrderId(orderId);
					
					cc.persist();
					updated = true;
				}
			} catch (SystemException e) {			
				e.printStackTrace();
			}
								
		}else{
			
			try {
				CoinCount cc = CoinCountLocalServiceUtil.fetchCoinCount(phase);
				if(cc != null && (cc.getTotalAmount()+amt < cc.getLimit())){
					cc.setTotalAmount(cc.getTotalAmount()+amt);
					cc.persist();
					updated = true;
				}
			} catch (SystemException e) {			
				e.printStackTrace();
			}
			
		}
		return updated;
	}
}
