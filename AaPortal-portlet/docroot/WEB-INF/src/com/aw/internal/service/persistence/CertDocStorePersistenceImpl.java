/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.persistence;

import com.aw.internal.NoSuchCertDocStoreException;
import com.aw.internal.model.CertDocStore;
import com.aw.internal.model.impl.CertDocStoreImpl;
import com.aw.internal.model.impl.CertDocStoreModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the cert doc store service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author zl
 * @see CertDocStorePersistence
 * @see CertDocStoreUtil
 * @generated
 */
public class CertDocStorePersistenceImpl extends BasePersistenceImpl<CertDocStore>
	implements CertDocStorePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CertDocStoreUtil} to access the cert doc store persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CertDocStoreImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CertDocStoreModelImpl.ENTITY_CACHE_ENABLED,
			CertDocStoreModelImpl.FINDER_CACHE_ENABLED, CertDocStoreImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CertDocStoreModelImpl.ENTITY_CACHE_ENABLED,
			CertDocStoreModelImpl.FINDER_CACHE_ENABLED, CertDocStoreImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CertDocStoreModelImpl.ENTITY_CACHE_ENABLED,
			CertDocStoreModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public CertDocStorePersistenceImpl() {
		setModelClass(CertDocStore.class);
	}

	/**
	 * Caches the cert doc store in the entity cache if it is enabled.
	 *
	 * @param certDocStore the cert doc store
	 */
	@Override
	public void cacheResult(CertDocStore certDocStore) {
		EntityCacheUtil.putResult(CertDocStoreModelImpl.ENTITY_CACHE_ENABLED,
			CertDocStoreImpl.class, certDocStore.getPrimaryKey(), certDocStore);

		certDocStore.resetOriginalValues();
	}

	/**
	 * Caches the cert doc stores in the entity cache if it is enabled.
	 *
	 * @param certDocStores the cert doc stores
	 */
	@Override
	public void cacheResult(List<CertDocStore> certDocStores) {
		for (CertDocStore certDocStore : certDocStores) {
			if (EntityCacheUtil.getResult(
						CertDocStoreModelImpl.ENTITY_CACHE_ENABLED,
						CertDocStoreImpl.class, certDocStore.getPrimaryKey()) == null) {
				cacheResult(certDocStore);
			}
			else {
				certDocStore.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all cert doc stores.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(CertDocStoreImpl.class.getName());
		}

		EntityCacheUtil.clearCache(CertDocStoreImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the cert doc store.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(CertDocStore certDocStore) {
		EntityCacheUtil.removeResult(CertDocStoreModelImpl.ENTITY_CACHE_ENABLED,
			CertDocStoreImpl.class, certDocStore.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<CertDocStore> certDocStores) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (CertDocStore certDocStore : certDocStores) {
			EntityCacheUtil.removeResult(CertDocStoreModelImpl.ENTITY_CACHE_ENABLED,
				CertDocStoreImpl.class, certDocStore.getPrimaryKey());
		}
	}

	/**
	 * Creates a new cert doc store with the primary key. Does not add the cert doc store to the database.
	 *
	 * @param docId the primary key for the new cert doc store
	 * @return the new cert doc store
	 */
	@Override
	public CertDocStore create(long docId) {
		CertDocStore certDocStore = new CertDocStoreImpl();

		certDocStore.setNew(true);
		certDocStore.setPrimaryKey(docId);

		return certDocStore;
	}

	/**
	 * Removes the cert doc store with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param docId the primary key of the cert doc store
	 * @return the cert doc store that was removed
	 * @throws com.aw.internal.NoSuchCertDocStoreException if a cert doc store with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CertDocStore remove(long docId)
		throws NoSuchCertDocStoreException, SystemException {
		return remove((Serializable)docId);
	}

	/**
	 * Removes the cert doc store with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the cert doc store
	 * @return the cert doc store that was removed
	 * @throws com.aw.internal.NoSuchCertDocStoreException if a cert doc store with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CertDocStore remove(Serializable primaryKey)
		throws NoSuchCertDocStoreException, SystemException {
		Session session = null;

		try {
			session = openSession();

			CertDocStore certDocStore = (CertDocStore)session.get(CertDocStoreImpl.class,
					primaryKey);

			if (certDocStore == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCertDocStoreException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(certDocStore);
		}
		catch (NoSuchCertDocStoreException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected CertDocStore removeImpl(CertDocStore certDocStore)
		throws SystemException {
		certDocStore = toUnwrappedModel(certDocStore);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(certDocStore)) {
				certDocStore = (CertDocStore)session.get(CertDocStoreImpl.class,
						certDocStore.getPrimaryKeyObj());
			}

			if (certDocStore != null) {
				session.delete(certDocStore);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (certDocStore != null) {
			clearCache(certDocStore);
		}

		return certDocStore;
	}

	@Override
	public CertDocStore updateImpl(
		com.aw.internal.model.CertDocStore certDocStore)
		throws SystemException {
		certDocStore = toUnwrappedModel(certDocStore);

		boolean isNew = certDocStore.isNew();

		Session session = null;

		try {
			session = openSession();

			if (certDocStore.isNew()) {
				session.save(certDocStore);

				certDocStore.setNew(false);
			}
			else {
				session.evict(certDocStore);
				session.saveOrUpdate(certDocStore);
			}

			session.flush();
			session.clear();
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(CertDocStoreModelImpl.ENTITY_CACHE_ENABLED,
			CertDocStoreImpl.class, certDocStore.getPrimaryKey(), certDocStore);

		certDocStore.resetOriginalValues();

		return certDocStore;
	}

	protected CertDocStore toUnwrappedModel(CertDocStore certDocStore) {
		if (certDocStore instanceof CertDocStoreImpl) {
			return certDocStore;
		}

		CertDocStoreImpl certDocStoreImpl = new CertDocStoreImpl();

		certDocStoreImpl.setNew(certDocStore.isNew());
		certDocStoreImpl.setPrimaryKey(certDocStore.getPrimaryKey());

		certDocStoreImpl.setDocId(certDocStore.getDocId());
		certDocStoreImpl.setContent(certDocStore.getContent());
		certDocStoreImpl.setActiveFlag(certDocStore.getActiveFlag());
		certDocStoreImpl.setAddDate(certDocStore.getAddDate());

		return certDocStoreImpl;
	}

	/**
	 * Returns the cert doc store with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the cert doc store
	 * @return the cert doc store
	 * @throws com.aw.internal.NoSuchCertDocStoreException if a cert doc store with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CertDocStore findByPrimaryKey(Serializable primaryKey)
		throws NoSuchCertDocStoreException, SystemException {
		CertDocStore certDocStore = fetchByPrimaryKey(primaryKey);

		if (certDocStore == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchCertDocStoreException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return certDocStore;
	}

	/**
	 * Returns the cert doc store with the primary key or throws a {@link com.aw.internal.NoSuchCertDocStoreException} if it could not be found.
	 *
	 * @param docId the primary key of the cert doc store
	 * @return the cert doc store
	 * @throws com.aw.internal.NoSuchCertDocStoreException if a cert doc store with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CertDocStore findByPrimaryKey(long docId)
		throws NoSuchCertDocStoreException, SystemException {
		return findByPrimaryKey((Serializable)docId);
	}

	/**
	 * Returns the cert doc store with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the cert doc store
	 * @return the cert doc store, or <code>null</code> if a cert doc store with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CertDocStore fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		CertDocStore certDocStore = (CertDocStore)EntityCacheUtil.getResult(CertDocStoreModelImpl.ENTITY_CACHE_ENABLED,
				CertDocStoreImpl.class, primaryKey);

		if (certDocStore == _nullCertDocStore) {
			return null;
		}

		if (certDocStore == null) {
			Session session = null;

			try {
				session = openSession();

				certDocStore = (CertDocStore)session.get(CertDocStoreImpl.class,
						primaryKey);

				if (certDocStore != null) {
					cacheResult(certDocStore);
				}
				else {
					EntityCacheUtil.putResult(CertDocStoreModelImpl.ENTITY_CACHE_ENABLED,
						CertDocStoreImpl.class, primaryKey, _nullCertDocStore);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(CertDocStoreModelImpl.ENTITY_CACHE_ENABLED,
					CertDocStoreImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return certDocStore;
	}

	/**
	 * Returns the cert doc store with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param docId the primary key of the cert doc store
	 * @return the cert doc store, or <code>null</code> if a cert doc store with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CertDocStore fetchByPrimaryKey(long docId) throws SystemException {
		return fetchByPrimaryKey((Serializable)docId);
	}

	/**
	 * Returns all the cert doc stores.
	 *
	 * @return the cert doc stores
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CertDocStore> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the cert doc stores.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CertDocStoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of cert doc stores
	 * @param end the upper bound of the range of cert doc stores (not inclusive)
	 * @return the range of cert doc stores
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CertDocStore> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the cert doc stores.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CertDocStoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of cert doc stores
	 * @param end the upper bound of the range of cert doc stores (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of cert doc stores
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CertDocStore> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<CertDocStore> list = (List<CertDocStore>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_CERTDOCSTORE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CERTDOCSTORE;

				if (pagination) {
					sql = sql.concat(CertDocStoreModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<CertDocStore>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CertDocStore>(list);
				}
				else {
					list = (List<CertDocStore>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the cert doc stores from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (CertDocStore certDocStore : findAll()) {
			remove(certDocStore);
		}
	}

	/**
	 * Returns the number of cert doc stores.
	 *
	 * @return the number of cert doc stores
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CERTDOCSTORE);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the cert doc store persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.aw.internal.model.CertDocStore")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<CertDocStore>> listenersList = new ArrayList<ModelListener<CertDocStore>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<CertDocStore>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(CertDocStoreImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_CERTDOCSTORE = "SELECT certDocStore FROM CertDocStore certDocStore";
	private static final String _SQL_COUNT_CERTDOCSTORE = "SELECT COUNT(certDocStore) FROM CertDocStore certDocStore";
	private static final String _ORDER_BY_ENTITY_ALIAS = "certDocStore.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No CertDocStore exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(CertDocStorePersistenceImpl.class);
	private static CertDocStore _nullCertDocStore = new CertDocStoreImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<CertDocStore> toCacheModel() {
				return _nullCertDocStoreCacheModel;
			}
		};

	private static CacheModel<CertDocStore> _nullCertDocStoreCacheModel = new CacheModel<CertDocStore>() {
			@Override
			public CertDocStore toEntityModel() {
				return _nullCertDocStore;
			}
		};
}