/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.persistence;

import com.aw.internal.NoSuchUnitPriceException;
import com.aw.internal.model.UnitPrice;
import com.aw.internal.model.impl.UnitPriceImpl;
import com.aw.internal.model.impl.UnitPriceModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the unit price service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author zl
 * @see UnitPricePersistence
 * @see UnitPriceUtil
 * @generated
 */
public class UnitPricePersistenceImpl extends BasePersistenceImpl<UnitPrice>
	implements UnitPricePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link UnitPriceUtil} to access the unit price persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = UnitPriceImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(UnitPriceModelImpl.ENTITY_CACHE_ENABLED,
			UnitPriceModelImpl.FINDER_CACHE_ENABLED, UnitPriceImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(UnitPriceModelImpl.ENTITY_CACHE_ENABLED,
			UnitPriceModelImpl.FINDER_CACHE_ENABLED, UnitPriceImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(UnitPriceModelImpl.ENTITY_CACHE_ENABLED,
			UnitPriceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public UnitPricePersistenceImpl() {
		setModelClass(UnitPrice.class);
	}

	/**
	 * Caches the unit price in the entity cache if it is enabled.
	 *
	 * @param unitPrice the unit price
	 */
	@Override
	public void cacheResult(UnitPrice unitPrice) {
		EntityCacheUtil.putResult(UnitPriceModelImpl.ENTITY_CACHE_ENABLED,
			UnitPriceImpl.class, unitPrice.getPrimaryKey(), unitPrice);

		unitPrice.resetOriginalValues();
	}

	/**
	 * Caches the unit prices in the entity cache if it is enabled.
	 *
	 * @param unitPrices the unit prices
	 */
	@Override
	public void cacheResult(List<UnitPrice> unitPrices) {
		for (UnitPrice unitPrice : unitPrices) {
			if (EntityCacheUtil.getResult(
						UnitPriceModelImpl.ENTITY_CACHE_ENABLED,
						UnitPriceImpl.class, unitPrice.getPrimaryKey()) == null) {
				cacheResult(unitPrice);
			}
			else {
				unitPrice.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all unit prices.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(UnitPriceImpl.class.getName());
		}

		EntityCacheUtil.clearCache(UnitPriceImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the unit price.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(UnitPrice unitPrice) {
		EntityCacheUtil.removeResult(UnitPriceModelImpl.ENTITY_CACHE_ENABLED,
			UnitPriceImpl.class, unitPrice.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<UnitPrice> unitPrices) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (UnitPrice unitPrice : unitPrices) {
			EntityCacheUtil.removeResult(UnitPriceModelImpl.ENTITY_CACHE_ENABLED,
				UnitPriceImpl.class, unitPrice.getPrimaryKey());
		}
	}

	/**
	 * Creates a new unit price with the primary key. Does not add the unit price to the database.
	 *
	 * @param unitPricePK the primary key for the new unit price
	 * @return the new unit price
	 */
	@Override
	public UnitPrice create(UnitPricePK unitPricePK) {
		UnitPrice unitPrice = new UnitPriceImpl();

		unitPrice.setNew(true);
		unitPrice.setPrimaryKey(unitPricePK);

		return unitPrice;
	}

	/**
	 * Removes the unit price with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param unitPricePK the primary key of the unit price
	 * @return the unit price that was removed
	 * @throws com.aw.internal.NoSuchUnitPriceException if a unit price with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UnitPrice remove(UnitPricePK unitPricePK)
		throws NoSuchUnitPriceException, SystemException {
		return remove((Serializable)unitPricePK);
	}

	/**
	 * Removes the unit price with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the unit price
	 * @return the unit price that was removed
	 * @throws com.aw.internal.NoSuchUnitPriceException if a unit price with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UnitPrice remove(Serializable primaryKey)
		throws NoSuchUnitPriceException, SystemException {
		Session session = null;

		try {
			session = openSession();

			UnitPrice unitPrice = (UnitPrice)session.get(UnitPriceImpl.class,
					primaryKey);

			if (unitPrice == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchUnitPriceException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(unitPrice);
		}
		catch (NoSuchUnitPriceException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected UnitPrice removeImpl(UnitPrice unitPrice)
		throws SystemException {
		unitPrice = toUnwrappedModel(unitPrice);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(unitPrice)) {
				unitPrice = (UnitPrice)session.get(UnitPriceImpl.class,
						unitPrice.getPrimaryKeyObj());
			}

			if (unitPrice != null) {
				session.delete(unitPrice);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (unitPrice != null) {
			clearCache(unitPrice);
		}

		return unitPrice;
	}

	@Override
	public UnitPrice updateImpl(com.aw.internal.model.UnitPrice unitPrice)
		throws SystemException {
		unitPrice = toUnwrappedModel(unitPrice);

		boolean isNew = unitPrice.isNew();

		Session session = null;

		try {
			session = openSession();

			if (unitPrice.isNew()) {
				session.save(unitPrice);

				unitPrice.setNew(false);
			}
			else {
				session.merge(unitPrice);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(UnitPriceModelImpl.ENTITY_CACHE_ENABLED,
			UnitPriceImpl.class, unitPrice.getPrimaryKey(), unitPrice);

		return unitPrice;
	}

	protected UnitPrice toUnwrappedModel(UnitPrice unitPrice) {
		if (unitPrice instanceof UnitPriceImpl) {
			return unitPrice;
		}

		UnitPriceImpl unitPriceImpl = new UnitPriceImpl();

		unitPriceImpl.setNew(unitPrice.isNew());
		unitPriceImpl.setPrimaryKey(unitPrice.getPrimaryKey());

		unitPriceImpl.setCurrency(unitPrice.getCurrency());
		unitPriceImpl.setSalePhase(unitPrice.getSalePhase());
		unitPriceImpl.setPrice(unitPrice.getPrice());
		unitPriceImpl.setActiveFlag(unitPrice.getActiveFlag());
		unitPriceImpl.setAddDate(unitPrice.getAddDate());
		unitPriceImpl.setAddUser(unitPrice.getAddUser());

		return unitPriceImpl;
	}

	/**
	 * Returns the unit price with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the unit price
	 * @return the unit price
	 * @throws com.aw.internal.NoSuchUnitPriceException if a unit price with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UnitPrice findByPrimaryKey(Serializable primaryKey)
		throws NoSuchUnitPriceException, SystemException {
		UnitPrice unitPrice = fetchByPrimaryKey(primaryKey);

		if (unitPrice == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchUnitPriceException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return unitPrice;
	}

	/**
	 * Returns the unit price with the primary key or throws a {@link com.aw.internal.NoSuchUnitPriceException} if it could not be found.
	 *
	 * @param unitPricePK the primary key of the unit price
	 * @return the unit price
	 * @throws com.aw.internal.NoSuchUnitPriceException if a unit price with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UnitPrice findByPrimaryKey(UnitPricePK unitPricePK)
		throws NoSuchUnitPriceException, SystemException {
		return findByPrimaryKey((Serializable)unitPricePK);
	}

	/**
	 * Returns the unit price with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the unit price
	 * @return the unit price, or <code>null</code> if a unit price with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UnitPrice fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		UnitPrice unitPrice = (UnitPrice)EntityCacheUtil.getResult(UnitPriceModelImpl.ENTITY_CACHE_ENABLED,
				UnitPriceImpl.class, primaryKey);

		if (unitPrice == _nullUnitPrice) {
			return null;
		}

		if (unitPrice == null) {
			Session session = null;

			try {
				session = openSession();

				unitPrice = (UnitPrice)session.get(UnitPriceImpl.class,
						primaryKey);

				if (unitPrice != null) {
					cacheResult(unitPrice);
				}
				else {
					EntityCacheUtil.putResult(UnitPriceModelImpl.ENTITY_CACHE_ENABLED,
						UnitPriceImpl.class, primaryKey, _nullUnitPrice);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(UnitPriceModelImpl.ENTITY_CACHE_ENABLED,
					UnitPriceImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return unitPrice;
	}

	/**
	 * Returns the unit price with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param unitPricePK the primary key of the unit price
	 * @return the unit price, or <code>null</code> if a unit price with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public UnitPrice fetchByPrimaryKey(UnitPricePK unitPricePK)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)unitPricePK);
	}

	/**
	 * Returns all the unit prices.
	 *
	 * @return the unit prices
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UnitPrice> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the unit prices.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.UnitPriceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of unit prices
	 * @param end the upper bound of the range of unit prices (not inclusive)
	 * @return the range of unit prices
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UnitPrice> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the unit prices.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.UnitPriceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of unit prices
	 * @param end the upper bound of the range of unit prices (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of unit prices
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<UnitPrice> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<UnitPrice> list = (List<UnitPrice>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_UNITPRICE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_UNITPRICE;

				if (pagination) {
					sql = sql.concat(UnitPriceModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<UnitPrice>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<UnitPrice>(list);
				}
				else {
					list = (List<UnitPrice>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the unit prices from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (UnitPrice unitPrice : findAll()) {
			remove(unitPrice);
		}
	}

	/**
	 * Returns the number of unit prices.
	 *
	 * @return the number of unit prices
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_UNITPRICE);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the unit price persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.aw.internal.model.UnitPrice")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<UnitPrice>> listenersList = new ArrayList<ModelListener<UnitPrice>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<UnitPrice>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(UnitPriceImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_UNITPRICE = "SELECT unitPrice FROM UnitPrice unitPrice";
	private static final String _SQL_COUNT_UNITPRICE = "SELECT COUNT(unitPrice) FROM UnitPrice unitPrice";
	private static final String _ORDER_BY_ENTITY_ALIAS = "unitPrice.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No UnitPrice exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(UnitPricePersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"currency"
			});
	private static UnitPrice _nullUnitPrice = new UnitPriceImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<UnitPrice> toCacheModel() {
				return _nullUnitPriceCacheModel;
			}
		};

	private static CacheModel<UnitPrice> _nullUnitPriceCacheModel = new CacheModel<UnitPrice>() {
			@Override
			public UnitPrice toEntityModel() {
				return _nullUnitPrice;
			}
		};
}