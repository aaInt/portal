/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.base;

import com.aw.internal.service.CoinOrderServiceUtil;

import java.util.Arrays;

/**
 * @author zl
 * @generated
 */
public class CoinOrderServiceClpInvoker {
	public CoinOrderServiceClpInvoker() {
		_methodName68 = "getBeanIdentifier";

		_methodParameterTypes68 = new String[] {  };

		_methodName69 = "setBeanIdentifier";

		_methodParameterTypes69 = new String[] { "java.lang.String" };

		_methodName74 = "verifyOrderInfo";

		_methodParameterTypes74 = new String[] {
				"java.lang.String", "java.lang.String", "java.lang.String"
			};

		_methodName75 = "createCoinOrder";

		_methodParameterTypes75 = new String[] {
				"java.lang.String", "long", "int", "double", "double",
				"java.lang.String", "long", "long"
			};

		_methodName76 = "splitOrder";

		_methodParameterTypes76 = new String[] {
				"java.lang.String", "long", "java.lang.String", "long"
			};

		_methodName77 = "getPedingOrder";

		_methodParameterTypes77 = new String[] {  };

		_methodName78 = "getStaffOrders";

		_methodParameterTypes78 = new String[] {  };

		_methodName79 = "getCompletedOrders";

		_methodParameterTypes79 = new String[] {  };

		_methodName80 = "getFailedOrder";

		_methodParameterTypes80 = new String[] {  };

		_methodName81 = "verified";

		_methodParameterTypes81 = new String[] { "java.lang.String" };

		_methodName82 = "contractSent";

		_methodParameterTypes82 = new String[] {
				"java.lang.String", "java.lang.String", "java.lang.String"
			};

		_methodName83 = "contractSigned";

		_methodParameterTypes83 = new String[] { "java.lang.String" };

		_methodName84 = "paid";

		_methodParameterTypes84 = new String[] { "java.lang.String" };

		_methodName85 = "approve";

		_methodParameterTypes85 = new String[] { "java.lang.String" };

		_methodName86 = "certSent";

		_methodParameterTypes86 = new String[] {
				"java.lang.String", "java.lang.String", "java.lang.String"
			};

		_methodName87 = "complete";

		_methodParameterTypes87 = new String[] { "java.lang.String" };

		_methodName88 = "fail";

		_methodParameterTypes88 = new String[] {
				"java.lang.String", "java.lang.String"
			};

		_methodName89 = "addReason";

		_methodParameterTypes89 = new String[] {
				"java.lang.String", "java.lang.String"
			};

		_methodName90 = "getReason";

		_methodParameterTypes90 = new String[] { "java.lang.String" };

		_methodName91 = "getReasons";

		_methodParameterTypes91 = new String[] {  };

		_methodName92 = "getSaleSummary";

		_methodParameterTypes92 = new String[] {
				"boolean", "boolean", "int", "java.lang.String",
				"java.lang.String"
			};
	}

	public Object invokeMethod(String name, String[] parameterTypes,
		Object[] arguments) throws Throwable {
		if (_methodName68.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes68, parameterTypes)) {
			return CoinOrderServiceUtil.getBeanIdentifier();
		}

		if (_methodName69.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes69, parameterTypes)) {
			CoinOrderServiceUtil.setBeanIdentifier((java.lang.String)arguments[0]);

			return null;
		}

		if (_methodName74.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes74, parameterTypes)) {
			return CoinOrderServiceUtil.verifyOrderInfo((java.lang.String)arguments[0],
				(java.lang.String)arguments[1], (java.lang.String)arguments[2]);
		}

		if (_methodName75.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes75, parameterTypes)) {
			return CoinOrderServiceUtil.createCoinOrder((java.lang.String)arguments[0],
				((Long)arguments[1]).longValue(),
				((Integer)arguments[2]).intValue(),
				((Double)arguments[3]).doubleValue(),
				((Double)arguments[4]).doubleValue(),
				(java.lang.String)arguments[5],
				((Long)arguments[6]).longValue(),
				((Long)arguments[7]).longValue());
		}

		if (_methodName76.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes76, parameterTypes)) {
			return CoinOrderServiceUtil.splitOrder((java.lang.String)arguments[0],
				((Long)arguments[1]).longValue(),
				(java.lang.String)arguments[2], ((Long)arguments[3]).longValue());
		}

		if (_methodName77.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes77, parameterTypes)) {
			return CoinOrderServiceUtil.getPedingOrder();
		}

		if (_methodName78.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes78, parameterTypes)) {
			return CoinOrderServiceUtil.getStaffOrders();
		}

		if (_methodName79.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes79, parameterTypes)) {
			return CoinOrderServiceUtil.getCompletedOrders();
		}

		if (_methodName80.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes80, parameterTypes)) {
			return CoinOrderServiceUtil.getFailedOrder();
		}

		if (_methodName81.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes81, parameterTypes)) {
			return CoinOrderServiceUtil.verified((java.lang.String)arguments[0]);
		}

		if (_methodName82.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes82, parameterTypes)) {
			return CoinOrderServiceUtil.contractSent((java.lang.String)arguments[0],
				(java.lang.String)arguments[1], (java.lang.String)arguments[2]);
		}

		if (_methodName83.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes83, parameterTypes)) {
			return CoinOrderServiceUtil.contractSigned((java.lang.String)arguments[0]);
		}

		if (_methodName84.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes84, parameterTypes)) {
			return CoinOrderServiceUtil.paid((java.lang.String)arguments[0]);
		}

		if (_methodName85.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes85, parameterTypes)) {
			return CoinOrderServiceUtil.approve((java.lang.String)arguments[0]);
		}

		if (_methodName86.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes86, parameterTypes)) {
			return CoinOrderServiceUtil.certSent((java.lang.String)arguments[0],
				(java.lang.String)arguments[1], (java.lang.String)arguments[2]);
		}

		if (_methodName87.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes87, parameterTypes)) {
			return CoinOrderServiceUtil.complete((java.lang.String)arguments[0]);
		}

		if (_methodName88.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes88, parameterTypes)) {
			return CoinOrderServiceUtil.fail((java.lang.String)arguments[0],
				(java.lang.String)arguments[1]);
		}

		if (_methodName89.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes89, parameterTypes)) {
			return CoinOrderServiceUtil.addReason((java.lang.String)arguments[0],
				(java.lang.String)arguments[1]);
		}

		if (_methodName90.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes90, parameterTypes)) {
			return CoinOrderServiceUtil.getReason((java.lang.String)arguments[0]);
		}

		if (_methodName91.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes91, parameterTypes)) {
			return CoinOrderServiceUtil.getReasons();
		}

		if (_methodName92.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes92, parameterTypes)) {
			return CoinOrderServiceUtil.getSaleSummary(((Boolean)arguments[0]).booleanValue(),
				((Boolean)arguments[1]).booleanValue(),
				((Integer)arguments[2]).intValue(),
				(java.lang.String)arguments[3], (java.lang.String)arguments[4]);
		}

		throw new UnsupportedOperationException();
	}

	private String _methodName68;
	private String[] _methodParameterTypes68;
	private String _methodName69;
	private String[] _methodParameterTypes69;
	private String _methodName74;
	private String[] _methodParameterTypes74;
	private String _methodName75;
	private String[] _methodParameterTypes75;
	private String _methodName76;
	private String[] _methodParameterTypes76;
	private String _methodName77;
	private String[] _methodParameterTypes77;
	private String _methodName78;
	private String[] _methodParameterTypes78;
	private String _methodName79;
	private String[] _methodParameterTypes79;
	private String _methodName80;
	private String[] _methodParameterTypes80;
	private String _methodName81;
	private String[] _methodParameterTypes81;
	private String _methodName82;
	private String[] _methodParameterTypes82;
	private String _methodName83;
	private String[] _methodParameterTypes83;
	private String _methodName84;
	private String[] _methodParameterTypes84;
	private String _methodName85;
	private String[] _methodParameterTypes85;
	private String _methodName86;
	private String[] _methodParameterTypes86;
	private String _methodName87;
	private String[] _methodParameterTypes87;
	private String _methodName88;
	private String[] _methodParameterTypes88;
	private String _methodName89;
	private String[] _methodParameterTypes89;
	private String _methodName90;
	private String[] _methodParameterTypes90;
	private String _methodName91;
	private String[] _methodParameterTypes91;
	private String _methodName92;
	private String[] _methodParameterTypes92;
}