/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.persistence;

import com.aw.internal.NoSuchActivePhaseException;
import com.aw.internal.model.ActivePhase;
import com.aw.internal.model.impl.ActivePhaseImpl;
import com.aw.internal.model.impl.ActivePhaseModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the active phase service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author zl
 * @see ActivePhasePersistence
 * @see ActivePhaseUtil
 * @generated
 */
public class ActivePhasePersistenceImpl extends BasePersistenceImpl<ActivePhase>
	implements ActivePhasePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ActivePhaseUtil} to access the active phase persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ActivePhaseImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ActivePhaseModelImpl.ENTITY_CACHE_ENABLED,
			ActivePhaseModelImpl.FINDER_CACHE_ENABLED, ActivePhaseImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ActivePhaseModelImpl.ENTITY_CACHE_ENABLED,
			ActivePhaseModelImpl.FINDER_CACHE_ENABLED, ActivePhaseImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ActivePhaseModelImpl.ENTITY_CACHE_ENABLED,
			ActivePhaseModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public ActivePhasePersistenceImpl() {
		setModelClass(ActivePhase.class);
	}

	/**
	 * Caches the active phase in the entity cache if it is enabled.
	 *
	 * @param activePhase the active phase
	 */
	@Override
	public void cacheResult(ActivePhase activePhase) {
		EntityCacheUtil.putResult(ActivePhaseModelImpl.ENTITY_CACHE_ENABLED,
			ActivePhaseImpl.class, activePhase.getPrimaryKey(), activePhase);

		activePhase.resetOriginalValues();
	}

	/**
	 * Caches the active phases in the entity cache if it is enabled.
	 *
	 * @param activePhases the active phases
	 */
	@Override
	public void cacheResult(List<ActivePhase> activePhases) {
		for (ActivePhase activePhase : activePhases) {
			if (EntityCacheUtil.getResult(
						ActivePhaseModelImpl.ENTITY_CACHE_ENABLED,
						ActivePhaseImpl.class, activePhase.getPrimaryKey()) == null) {
				cacheResult(activePhase);
			}
			else {
				activePhase.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all active phases.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(ActivePhaseImpl.class.getName());
		}

		EntityCacheUtil.clearCache(ActivePhaseImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the active phase.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ActivePhase activePhase) {
		EntityCacheUtil.removeResult(ActivePhaseModelImpl.ENTITY_CACHE_ENABLED,
			ActivePhaseImpl.class, activePhase.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<ActivePhase> activePhases) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (ActivePhase activePhase : activePhases) {
			EntityCacheUtil.removeResult(ActivePhaseModelImpl.ENTITY_CACHE_ENABLED,
				ActivePhaseImpl.class, activePhase.getPrimaryKey());
		}
	}

	/**
	 * Creates a new active phase with the primary key. Does not add the active phase to the database.
	 *
	 * @param region the primary key for the new active phase
	 * @return the new active phase
	 */
	@Override
	public ActivePhase create(String region) {
		ActivePhase activePhase = new ActivePhaseImpl();

		activePhase.setNew(true);
		activePhase.setPrimaryKey(region);

		return activePhase;
	}

	/**
	 * Removes the active phase with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param region the primary key of the active phase
	 * @return the active phase that was removed
	 * @throws com.aw.internal.NoSuchActivePhaseException if a active phase with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ActivePhase remove(String region)
		throws NoSuchActivePhaseException, SystemException {
		return remove((Serializable)region);
	}

	/**
	 * Removes the active phase with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the active phase
	 * @return the active phase that was removed
	 * @throws com.aw.internal.NoSuchActivePhaseException if a active phase with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ActivePhase remove(Serializable primaryKey)
		throws NoSuchActivePhaseException, SystemException {
		Session session = null;

		try {
			session = openSession();

			ActivePhase activePhase = (ActivePhase)session.get(ActivePhaseImpl.class,
					primaryKey);

			if (activePhase == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchActivePhaseException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(activePhase);
		}
		catch (NoSuchActivePhaseException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ActivePhase removeImpl(ActivePhase activePhase)
		throws SystemException {
		activePhase = toUnwrappedModel(activePhase);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(activePhase)) {
				activePhase = (ActivePhase)session.get(ActivePhaseImpl.class,
						activePhase.getPrimaryKeyObj());
			}

			if (activePhase != null) {
				session.delete(activePhase);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (activePhase != null) {
			clearCache(activePhase);
		}

		return activePhase;
	}

	@Override
	public ActivePhase updateImpl(com.aw.internal.model.ActivePhase activePhase)
		throws SystemException {
		activePhase = toUnwrappedModel(activePhase);

		boolean isNew = activePhase.isNew();

		Session session = null;

		try {
			session = openSession();

			if (activePhase.isNew()) {
				session.save(activePhase);

				activePhase.setNew(false);
			}
			else {
				session.merge(activePhase);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(ActivePhaseModelImpl.ENTITY_CACHE_ENABLED,
			ActivePhaseImpl.class, activePhase.getPrimaryKey(), activePhase);

		return activePhase;
	}

	protected ActivePhase toUnwrappedModel(ActivePhase activePhase) {
		if (activePhase instanceof ActivePhaseImpl) {
			return activePhase;
		}

		ActivePhaseImpl activePhaseImpl = new ActivePhaseImpl();

		activePhaseImpl.setNew(activePhase.isNew());
		activePhaseImpl.setPrimaryKey(activePhase.getPrimaryKey());

		activePhaseImpl.setRegion(activePhase.getRegion());
		activePhaseImpl.setSalePhase(activePhase.getSalePhase());

		return activePhaseImpl;
	}

	/**
	 * Returns the active phase with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the active phase
	 * @return the active phase
	 * @throws com.aw.internal.NoSuchActivePhaseException if a active phase with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ActivePhase findByPrimaryKey(Serializable primaryKey)
		throws NoSuchActivePhaseException, SystemException {
		ActivePhase activePhase = fetchByPrimaryKey(primaryKey);

		if (activePhase == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchActivePhaseException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return activePhase;
	}

	/**
	 * Returns the active phase with the primary key or throws a {@link com.aw.internal.NoSuchActivePhaseException} if it could not be found.
	 *
	 * @param region the primary key of the active phase
	 * @return the active phase
	 * @throws com.aw.internal.NoSuchActivePhaseException if a active phase with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ActivePhase findByPrimaryKey(String region)
		throws NoSuchActivePhaseException, SystemException {
		return findByPrimaryKey((Serializable)region);
	}

	/**
	 * Returns the active phase with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the active phase
	 * @return the active phase, or <code>null</code> if a active phase with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ActivePhase fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		ActivePhase activePhase = (ActivePhase)EntityCacheUtil.getResult(ActivePhaseModelImpl.ENTITY_CACHE_ENABLED,
				ActivePhaseImpl.class, primaryKey);

		if (activePhase == _nullActivePhase) {
			return null;
		}

		if (activePhase == null) {
			Session session = null;

			try {
				session = openSession();

				activePhase = (ActivePhase)session.get(ActivePhaseImpl.class,
						primaryKey);

				if (activePhase != null) {
					cacheResult(activePhase);
				}
				else {
					EntityCacheUtil.putResult(ActivePhaseModelImpl.ENTITY_CACHE_ENABLED,
						ActivePhaseImpl.class, primaryKey, _nullActivePhase);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(ActivePhaseModelImpl.ENTITY_CACHE_ENABLED,
					ActivePhaseImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return activePhase;
	}

	/**
	 * Returns the active phase with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param region the primary key of the active phase
	 * @return the active phase, or <code>null</code> if a active phase with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ActivePhase fetchByPrimaryKey(String region)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)region);
	}

	/**
	 * Returns all the active phases.
	 *
	 * @return the active phases
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ActivePhase> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the active phases.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.ActivePhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of active phases
	 * @param end the upper bound of the range of active phases (not inclusive)
	 * @return the range of active phases
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ActivePhase> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the active phases.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.ActivePhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of active phases
	 * @param end the upper bound of the range of active phases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of active phases
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ActivePhase> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<ActivePhase> list = (List<ActivePhase>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_ACTIVEPHASE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_ACTIVEPHASE;

				if (pagination) {
					sql = sql.concat(ActivePhaseModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<ActivePhase>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ActivePhase>(list);
				}
				else {
					list = (List<ActivePhase>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the active phases from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (ActivePhase activePhase : findAll()) {
			remove(activePhase);
		}
	}

	/**
	 * Returns the number of active phases.
	 *
	 * @return the number of active phases
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_ACTIVEPHASE);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the active phase persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.aw.internal.model.ActivePhase")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<ActivePhase>> listenersList = new ArrayList<ModelListener<ActivePhase>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<ActivePhase>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(ActivePhaseImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_ACTIVEPHASE = "SELECT activePhase FROM ActivePhase activePhase";
	private static final String _SQL_COUNT_ACTIVEPHASE = "SELECT COUNT(activePhase) FROM ActivePhase activePhase";
	private static final String _ORDER_BY_ENTITY_ALIAS = "activePhase.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ActivePhase exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(ActivePhasePersistenceImpl.class);
	private static ActivePhase _nullActivePhase = new ActivePhaseImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<ActivePhase> toCacheModel() {
				return _nullActivePhaseCacheModel;
			}
		};

	private static CacheModel<ActivePhase> _nullActivePhaseCacheModel = new CacheModel<ActivePhase>() {
			@Override
			public ActivePhase toEntityModel() {
				return _nullActivePhase;
			}
		};
}