/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.impl;

import com.aw.internal.model.AaContact;
import com.aw.internal.service.AaContactLocalServiceUtil;
import com.aw.internal.service.base.AaContactLocalServiceBaseImpl;
import com.aw.internal.service.persistence.AaContactUtil;
import com.aw.internal.service.util.DataUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONObject;


/**
 * The implementation of the aa contact local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.liferay.sample.service.AaContactLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author zl
 * @see com.aw.internal.service.base.AaContactLocalServiceBaseImpl
 * @see com.liferay.sample.service.AaContactLocalServiceUtil
 */
public class AaContactLocalServiceImpl extends AaContactLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.liferay.sample.service.AaContactLocalServiceUtil} to access the aa contact local service.
	 */
	public boolean createContact(
			long id,
	        long phone,
	        int areaCode,
	        String email,
	        String wechat,
	        String facebook,
	        String twitter,
	        String weibo,
	        String street1,
	        String street2,
	        String city,
	        String state,
	        String country,
	        String zipCode,
	        String isMailDiff, 
	        String mailStreet1, 
	        String mailStreet2, 
	        String mailCity, 
	        String mailState, 
	        String mailCountry, 
	        String mailZipCode
			)
	{
		boolean saved = true;//
		AaContact c = AaContactLocalServiceUtil.createAaContact(id);
		c.setPhone(phone);
		c.setAreaCode(areaCode);
		c.setEmail(email);
		c.setWechat(wechat);
		c.setFacebook(facebook);
		c.setTwitter(twitter);
		c.setWeibo(weibo);
		c.setStreet1(street1);
		c.setStreet2(street2);
		c.setCity(city);
		c.setState(state);
		c.setCountry(country);
		c.setZipCode(zipCode);
		
		c.setIsMailDiff(isMailDiff);
		if("Y".equals(isMailDiff)){
			c.setMailstreet1(mailStreet1);
			c.setMailstreet2(mailStreet2);
			c.setMailcity(mailCity);
			c.setMailstate(mailState);
			c.setMailcountry(mailCountry);
			c.setMailzipCode(mailZipCode);
		}
		
		try {
			c.persist();
		} catch (Exception e) {
			e.printStackTrace();
			saved = false;
		}
		return saved;
	}
	
	public boolean updateContact(
			long id,
	        long phone,
	        int areaCode,
	        String email,
	        String wechat,
	        String facebook,
	        String twitter,
	        String weibo,
	        String street1,
	        String street2,
	        String city,
	        String state,
	        String country,
	        String zipCode,
	        String isMailDiff, 
	        String mailStreet1, 
	        String mailStreet2, 
	        String mailCity, 
	        String mailState, 
	        String mailCountry, 
	        String mailZipCode
			)
	{
		boolean saved = false;//
		AaContact c;
		try {
			c = AaContactLocalServiceUtil.fetchAaContact(id);
			if(c != null){
				c.setPhone(phone);
				c.setAreaCode(areaCode);
				c.setEmail(email);
				c.setWechat(wechat);
				c.setFacebook(facebook);
				c.setTwitter(twitter);
				c.setWeibo(weibo);
				c.setStreet1(street1);
				c.setStreet2(street2);
				c.setCity(city);
				c.setState(state);
				c.setCountry(country);
				c.setZipCode(zipCode);	
				
				c.setIsMailDiff(isMailDiff);
				if("Y".equals(isMailDiff)){
					c.setMailstreet1(mailStreet1);
					c.setMailstreet2(mailStreet2);
					c.setMailcity(mailCity);
					c.setMailstate(mailState);
					c.setMailcountry(mailCountry);
					c.setMailzipCode(mailZipCode);
				}
				
				c.persist();
				saved = true;
			}
		} catch (SystemException e1) {
			
			e1.printStackTrace();
			
		}
		
		return saved;
	}
	
	public JSONObject getContact(long contactId){
		AaContact ac = null;
		try {
			ac = AaContactUtil.fetchByPrimaryKey(contactId);
		} catch (SystemException e) {
			e.printStackTrace();
		}
		if(ac != null){
			return DataUtil.getContactJson(ac);
		}else{
			return null;
		}
	}
	
	
}