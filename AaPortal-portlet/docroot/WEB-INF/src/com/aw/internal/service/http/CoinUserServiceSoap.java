/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.http;

import com.aw.internal.service.CoinUserServiceUtil;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.rmi.RemoteException;

/**
 * Provides the SOAP utility for the
 * {@link com.aw.internal.service.CoinUserServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it is difficult for SOAP to
 * support certain types.
 *
 * <p>
 * ServiceBuilder follows certain rules in translating the methods. For example,
 * if the method in the service utility returns a {@link java.util.List}, that
 * is translated to an array of {@link com.aw.internal.model.CoinUserSoap}.
 * If the method in the service utility returns a
 * {@link com.aw.internal.model.CoinUser}, that is translated to a
 * {@link com.aw.internal.model.CoinUserSoap}. Methods that SOAP cannot
 * safely wire are skipped.
 * </p>
 *
 * <p>
 * The benefits of using the SOAP utility is that it is cross platform
 * compatible. SOAP allows different languages like Java, .NET, C++, PHP, and
 * even Perl, to call the generated services. One drawback of SOAP is that it is
 * slow because it needs to serialize all calls into a text format (XML).
 * </p>
 *
 * <p>
 * You can see a list of services at http://localhost:8080/api/axis. Set the
 * property <b>axis.servlet.hosts.allowed</b> in portal.properties to configure
 * security.
 * </p>
 *
 * <p>
 * The SOAP utility is only generated for remote services.
 * </p>
 *
 * @author zl
 * @see CoinUserServiceHttp
 * @see com.aw.internal.model.CoinUserSoap
 * @see com.aw.internal.service.CoinUserServiceUtil
 * @generated
 */
public class CoinUserServiceSoap {
	public static java.lang.String addCoinUser(java.lang.String firstName,
		java.lang.String lastName, java.lang.String birthday,
		java.lang.String userType, java.lang.String brkLics,
		java.lang.String brkCountry, long fundId, java.lang.String nationality,
		java.lang.String idIssueCountry, java.lang.String idType,
		java.lang.String idNumber, java.lang.String photoImage,
		java.lang.String certDocStr, java.lang.String ssn,
		java.lang.String ein, java.lang.String income, java.lang.String asset,
		long phone, int areaCode, java.lang.String email,
		java.lang.String wechat, java.lang.String facebook,
		java.lang.String twitter, java.lang.String weibo,
		java.lang.String street1, java.lang.String street2,
		java.lang.String city, java.lang.String state,
		java.lang.String country, java.lang.String zipCode,
		java.lang.String isMailDiff, java.lang.String mailStreet1,
		java.lang.String mailStreet2, java.lang.String mailCity,
		java.lang.String mailState, java.lang.String mailCountry,
		java.lang.String mailZipCode) throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CoinUserServiceUtil.addCoinUser(firstName,
					lastName, birthday, userType, brkLics, brkCountry, fundId,
					nationality, idIssueCountry, idType, idNumber, photoImage,
					certDocStr, ssn, ein, income, asset, phone, areaCode,
					email, wechat, facebook, twitter, weibo, street1, street2,
					city, state, country, zipCode, isMailDiff, mailStreet1,
					mailStreet2, mailCity, mailState, mailCountry, mailZipCode);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String updateCoinUser(java.lang.String coinUserId,
		java.lang.String firstName, java.lang.String lastName,
		java.lang.String birthday, java.lang.String userType,
		java.lang.String brkLics, java.lang.String brkCountry, long fundId,
		java.lang.String nationality, java.lang.String idIssueCountry,
		java.lang.String idType, java.lang.String idNumber,
		java.lang.String ssn, java.lang.String ein, java.lang.String income,
		java.lang.String asset, long phone, int areaCode,
		java.lang.String email, java.lang.String wechat,
		java.lang.String facebook, java.lang.String twitter,
		java.lang.String weibo, java.lang.String street1,
		java.lang.String street2, java.lang.String city,
		java.lang.String state, java.lang.String country,
		java.lang.String zipCode, java.lang.String isMailDiff,
		java.lang.String mailStreet1, java.lang.String mailStreet2,
		java.lang.String mailCity, java.lang.String mailState,
		java.lang.String mailCountry, java.lang.String mailZipCode)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CoinUserServiceUtil.updateCoinUser(coinUserId,
					firstName, lastName, birthday, userType, brkLics,
					brkCountry, fundId, nationality, idIssueCountry, idType,
					idNumber, ssn, ein, income, asset, phone, areaCode, email,
					wechat, facebook, twitter, weibo, street1, street2, city,
					state, country, zipCode, isMailDiff, mailStreet1,
					mailStreet2, mailCity, mailState, mailCountry, mailZipCode);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String deleteUser(java.lang.String coinuserId)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CoinUserServiceUtil.deleteUser(coinuserId);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String updatePhoto(java.lang.String coinuserId,
		java.lang.String photoStr) throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CoinUserServiceUtil.updatePhoto(coinuserId,
					photoStr);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String updateDoc(java.lang.String coinuserId,
		java.lang.String docStr) throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CoinUserServiceUtil.updateDoc(coinuserId,
					docStr);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getCoinUser(java.lang.String coinUserId)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CoinUserServiceUtil.getCoinUser(coinUserId);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getValidatedCoinUsers()
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CoinUserServiceUtil.getValidatedCoinUsers();

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getSaleCoinUsers() throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CoinUserServiceUtil.getSaleCoinUsers();

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String deactivateCoinUser(
		java.lang.String coinUserId) throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CoinUserServiceUtil.deactivateCoinUser(coinUserId);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String verifyCoinUser(java.lang.String coinUserId)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CoinUserServiceUtil.verifyCoinUser(coinUserId);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String verifyAcredittedCoinUser(
		java.lang.String coinUserId) throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CoinUserServiceUtil.verifyAcredittedCoinUser(coinUserId);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String pendAcredittedCoinUser(
		java.lang.String coinUserId) throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CoinUserServiceUtil.pendAcredittedCoinUser(coinUserId);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getBroker(long id) throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CoinUserServiceUtil.getBroker(id);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String addFund(java.lang.String name, long phone,
		int areaCode, java.lang.String email, java.lang.String wechat,
		java.lang.String facebook, java.lang.String twitter,
		java.lang.String weibo, java.lang.String street1,
		java.lang.String street2, java.lang.String city,
		java.lang.String state, java.lang.String country,
		java.lang.String zipCode) throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CoinUserServiceUtil.addFund(name,
					phone, areaCode, email, wechat, facebook, twitter, weibo,
					street1, street2, city, state, country, zipCode);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getFund(long fundId)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CoinUserServiceUtil.getFund(fundId);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getFunds() throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CoinUserServiceUtil.getFunds();

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getContact(long contactId)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CoinUserServiceUtil.getContact(contactId);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getImage(java.lang.String userId)
		throws RemoteException {
		try {
			java.lang.String returnValue = CoinUserServiceUtil.getImage(userId);

			return returnValue;
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getCertDoc(java.lang.String userId)
		throws RemoteException {
		try {
			java.lang.String returnValue = CoinUserServiceUtil.getCertDoc(userId);

			return returnValue;
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	private static Log _log = LogFactoryUtil.getLog(CoinUserServiceSoap.class);
}