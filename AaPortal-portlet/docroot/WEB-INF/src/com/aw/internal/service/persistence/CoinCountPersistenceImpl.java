/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.persistence;

import com.aw.internal.NoSuchCoinCountException;
import com.aw.internal.model.CoinCount;
import com.aw.internal.model.impl.CoinCountImpl;
import com.aw.internal.model.impl.CoinCountModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the coin count service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author zl
 * @see CoinCountPersistence
 * @see CoinCountUtil
 * @generated
 */
public class CoinCountPersistenceImpl extends BasePersistenceImpl<CoinCount>
	implements CoinCountPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CoinCountUtil} to access the coin count persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CoinCountImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CoinCountModelImpl.ENTITY_CACHE_ENABLED,
			CoinCountModelImpl.FINDER_CACHE_ENABLED, CoinCountImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CoinCountModelImpl.ENTITY_CACHE_ENABLED,
			CoinCountModelImpl.FINDER_CACHE_ENABLED, CoinCountImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CoinCountModelImpl.ENTITY_CACHE_ENABLED,
			CoinCountModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public CoinCountPersistenceImpl() {
		setModelClass(CoinCount.class);
	}

	/**
	 * Caches the coin count in the entity cache if it is enabled.
	 *
	 * @param coinCount the coin count
	 */
	@Override
	public void cacheResult(CoinCount coinCount) {
		EntityCacheUtil.putResult(CoinCountModelImpl.ENTITY_CACHE_ENABLED,
			CoinCountImpl.class, coinCount.getPrimaryKey(), coinCount);

		coinCount.resetOriginalValues();
	}

	/**
	 * Caches the coin counts in the entity cache if it is enabled.
	 *
	 * @param coinCounts the coin counts
	 */
	@Override
	public void cacheResult(List<CoinCount> coinCounts) {
		for (CoinCount coinCount : coinCounts) {
			if (EntityCacheUtil.getResult(
						CoinCountModelImpl.ENTITY_CACHE_ENABLED,
						CoinCountImpl.class, coinCount.getPrimaryKey()) == null) {
				cacheResult(coinCount);
			}
			else {
				coinCount.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all coin counts.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(CoinCountImpl.class.getName());
		}

		EntityCacheUtil.clearCache(CoinCountImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the coin count.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(CoinCount coinCount) {
		EntityCacheUtil.removeResult(CoinCountModelImpl.ENTITY_CACHE_ENABLED,
			CoinCountImpl.class, coinCount.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<CoinCount> coinCounts) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (CoinCount coinCount : coinCounts) {
			EntityCacheUtil.removeResult(CoinCountModelImpl.ENTITY_CACHE_ENABLED,
				CoinCountImpl.class, coinCount.getPrimaryKey());
		}
	}

	/**
	 * Creates a new coin count with the primary key. Does not add the coin count to the database.
	 *
	 * @param phase the primary key for the new coin count
	 * @return the new coin count
	 */
	@Override
	public CoinCount create(int phase) {
		CoinCount coinCount = new CoinCountImpl();

		coinCount.setNew(true);
		coinCount.setPrimaryKey(phase);

		return coinCount;
	}

	/**
	 * Removes the coin count with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param phase the primary key of the coin count
	 * @return the coin count that was removed
	 * @throws com.aw.internal.NoSuchCoinCountException if a coin count with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinCount remove(int phase)
		throws NoSuchCoinCountException, SystemException {
		return remove((Serializable)phase);
	}

	/**
	 * Removes the coin count with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the coin count
	 * @return the coin count that was removed
	 * @throws com.aw.internal.NoSuchCoinCountException if a coin count with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinCount remove(Serializable primaryKey)
		throws NoSuchCoinCountException, SystemException {
		Session session = null;

		try {
			session = openSession();

			CoinCount coinCount = (CoinCount)session.get(CoinCountImpl.class,
					primaryKey);

			if (coinCount == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCoinCountException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(coinCount);
		}
		catch (NoSuchCoinCountException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected CoinCount removeImpl(CoinCount coinCount)
		throws SystemException {
		coinCount = toUnwrappedModel(coinCount);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(coinCount)) {
				coinCount = (CoinCount)session.get(CoinCountImpl.class,
						coinCount.getPrimaryKeyObj());
			}

			if (coinCount != null) {
				session.delete(coinCount);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (coinCount != null) {
			clearCache(coinCount);
		}

		return coinCount;
	}

	@Override
	public CoinCount updateImpl(com.aw.internal.model.CoinCount coinCount)
		throws SystemException {
		coinCount = toUnwrappedModel(coinCount);

		boolean isNew = coinCount.isNew();

		Session session = null;

		try {
			session = openSession();

			if (coinCount.isNew()) {
				session.save(coinCount);

				coinCount.setNew(false);
			}
			else {
				session.merge(coinCount);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(CoinCountModelImpl.ENTITY_CACHE_ENABLED,
			CoinCountImpl.class, coinCount.getPrimaryKey(), coinCount);

		return coinCount;
	}

	protected CoinCount toUnwrappedModel(CoinCount coinCount) {
		if (coinCount instanceof CoinCountImpl) {
			return coinCount;
		}

		CoinCountImpl coinCountImpl = new CoinCountImpl();

		coinCountImpl.setNew(coinCount.isNew());
		coinCountImpl.setPrimaryKey(coinCount.getPrimaryKey());

		coinCountImpl.setPhase(coinCount.getPhase());
		coinCountImpl.setTotalAmount(coinCount.getTotalAmount());
		coinCountImpl.setEndFlag(coinCount.getEndFlag());
		coinCountImpl.setRollToPhase(coinCount.getRollToPhase());
		coinCountImpl.setRollToAmt(coinCount.getRollToAmt());
		coinCountImpl.setLimit(coinCount.getLimit());
		coinCountImpl.setRollOverAmt(coinCount.getRollOverAmt());
		coinCountImpl.setLastOrderId(coinCount.getLastOrderId());

		return coinCountImpl;
	}

	/**
	 * Returns the coin count with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the coin count
	 * @return the coin count
	 * @throws com.aw.internal.NoSuchCoinCountException if a coin count with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinCount findByPrimaryKey(Serializable primaryKey)
		throws NoSuchCoinCountException, SystemException {
		CoinCount coinCount = fetchByPrimaryKey(primaryKey);

		if (coinCount == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchCoinCountException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return coinCount;
	}

	/**
	 * Returns the coin count with the primary key or throws a {@link com.aw.internal.NoSuchCoinCountException} if it could not be found.
	 *
	 * @param phase the primary key of the coin count
	 * @return the coin count
	 * @throws com.aw.internal.NoSuchCoinCountException if a coin count with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinCount findByPrimaryKey(int phase)
		throws NoSuchCoinCountException, SystemException {
		return findByPrimaryKey((Serializable)phase);
	}

	/**
	 * Returns the coin count with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the coin count
	 * @return the coin count, or <code>null</code> if a coin count with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinCount fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		CoinCount coinCount = (CoinCount)EntityCacheUtil.getResult(CoinCountModelImpl.ENTITY_CACHE_ENABLED,
				CoinCountImpl.class, primaryKey);

		if (coinCount == _nullCoinCount) {
			return null;
		}

		if (coinCount == null) {
			Session session = null;

			try {
				session = openSession();

				coinCount = (CoinCount)session.get(CoinCountImpl.class,
						primaryKey);

				if (coinCount != null) {
					cacheResult(coinCount);
				}
				else {
					EntityCacheUtil.putResult(CoinCountModelImpl.ENTITY_CACHE_ENABLED,
						CoinCountImpl.class, primaryKey, _nullCoinCount);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(CoinCountModelImpl.ENTITY_CACHE_ENABLED,
					CoinCountImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return coinCount;
	}

	/**
	 * Returns the coin count with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param phase the primary key of the coin count
	 * @return the coin count, or <code>null</code> if a coin count with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinCount fetchByPrimaryKey(int phase) throws SystemException {
		return fetchByPrimaryKey((Serializable)phase);
	}

	/**
	 * Returns all the coin counts.
	 *
	 * @return the coin counts
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CoinCount> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the coin counts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinCountModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of coin counts
	 * @param end the upper bound of the range of coin counts (not inclusive)
	 * @return the range of coin counts
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CoinCount> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the coin counts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinCountModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of coin counts
	 * @param end the upper bound of the range of coin counts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of coin counts
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CoinCount> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<CoinCount> list = (List<CoinCount>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_COINCOUNT);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_COINCOUNT;

				if (pagination) {
					sql = sql.concat(CoinCountModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<CoinCount>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CoinCount>(list);
				}
				else {
					list = (List<CoinCount>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the coin counts from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (CoinCount coinCount : findAll()) {
			remove(coinCount);
		}
	}

	/**
	 * Returns the number of coin counts.
	 *
	 * @return the number of coin counts
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_COINCOUNT);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the coin count persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.aw.internal.model.CoinCount")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<CoinCount>> listenersList = new ArrayList<ModelListener<CoinCount>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<CoinCount>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(CoinCountImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_COINCOUNT = "SELECT coinCount FROM CoinCount coinCount";
	private static final String _SQL_COUNT_COINCOUNT = "SELECT COUNT(coinCount) FROM CoinCount coinCount";
	private static final String _ORDER_BY_ENTITY_ALIAS = "coinCount.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No CoinCount exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(CoinCountPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"limit"
			});
	private static CoinCount _nullCoinCount = new CoinCountImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<CoinCount> toCacheModel() {
				return _nullCoinCountCacheModel;
			}
		};

	private static CacheModel<CoinCount> _nullCoinCountCacheModel = new CacheModel<CoinCount>() {
			@Override
			public CoinCount toEntityModel() {
				return _nullCoinCount;
			}
		};
}