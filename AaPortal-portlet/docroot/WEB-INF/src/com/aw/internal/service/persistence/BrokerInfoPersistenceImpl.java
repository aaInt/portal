/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.persistence;

import com.aw.internal.NoSuchBrokerInfoException;
import com.aw.internal.model.BrokerInfo;
import com.aw.internal.model.impl.BrokerInfoImpl;
import com.aw.internal.model.impl.BrokerInfoModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the broker info service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author zl
 * @see BrokerInfoPersistence
 * @see BrokerInfoUtil
 * @generated
 */
public class BrokerInfoPersistenceImpl extends BasePersistenceImpl<BrokerInfo>
	implements BrokerInfoPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link BrokerInfoUtil} to access the broker info persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = BrokerInfoImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(BrokerInfoModelImpl.ENTITY_CACHE_ENABLED,
			BrokerInfoModelImpl.FINDER_CACHE_ENABLED, BrokerInfoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(BrokerInfoModelImpl.ENTITY_CACHE_ENABLED,
			BrokerInfoModelImpl.FINDER_CACHE_ENABLED, BrokerInfoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(BrokerInfoModelImpl.ENTITY_CACHE_ENABLED,
			BrokerInfoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public BrokerInfoPersistenceImpl() {
		setModelClass(BrokerInfo.class);
	}

	/**
	 * Caches the broker info in the entity cache if it is enabled.
	 *
	 * @param brokerInfo the broker info
	 */
	@Override
	public void cacheResult(BrokerInfo brokerInfo) {
		EntityCacheUtil.putResult(BrokerInfoModelImpl.ENTITY_CACHE_ENABLED,
			BrokerInfoImpl.class, brokerInfo.getPrimaryKey(), brokerInfo);

		brokerInfo.resetOriginalValues();
	}

	/**
	 * Caches the broker infos in the entity cache if it is enabled.
	 *
	 * @param brokerInfos the broker infos
	 */
	@Override
	public void cacheResult(List<BrokerInfo> brokerInfos) {
		for (BrokerInfo brokerInfo : brokerInfos) {
			if (EntityCacheUtil.getResult(
						BrokerInfoModelImpl.ENTITY_CACHE_ENABLED,
						BrokerInfoImpl.class, brokerInfo.getPrimaryKey()) == null) {
				cacheResult(brokerInfo);
			}
			else {
				brokerInfo.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all broker infos.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(BrokerInfoImpl.class.getName());
		}

		EntityCacheUtil.clearCache(BrokerInfoImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the broker info.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(BrokerInfo brokerInfo) {
		EntityCacheUtil.removeResult(BrokerInfoModelImpl.ENTITY_CACHE_ENABLED,
			BrokerInfoImpl.class, brokerInfo.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<BrokerInfo> brokerInfos) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (BrokerInfo brokerInfo : brokerInfos) {
			EntityCacheUtil.removeResult(BrokerInfoModelImpl.ENTITY_CACHE_ENABLED,
				BrokerInfoImpl.class, brokerInfo.getPrimaryKey());
		}
	}

	/**
	 * Creates a new broker info with the primary key. Does not add the broker info to the database.
	 *
	 * @param brokerId the primary key for the new broker info
	 * @return the new broker info
	 */
	@Override
	public BrokerInfo create(long brokerId) {
		BrokerInfo brokerInfo = new BrokerInfoImpl();

		brokerInfo.setNew(true);
		brokerInfo.setPrimaryKey(brokerId);

		return brokerInfo;
	}

	/**
	 * Removes the broker info with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param brokerId the primary key of the broker info
	 * @return the broker info that was removed
	 * @throws com.aw.internal.NoSuchBrokerInfoException if a broker info with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BrokerInfo remove(long brokerId)
		throws NoSuchBrokerInfoException, SystemException {
		return remove((Serializable)brokerId);
	}

	/**
	 * Removes the broker info with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the broker info
	 * @return the broker info that was removed
	 * @throws com.aw.internal.NoSuchBrokerInfoException if a broker info with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BrokerInfo remove(Serializable primaryKey)
		throws NoSuchBrokerInfoException, SystemException {
		Session session = null;

		try {
			session = openSession();

			BrokerInfo brokerInfo = (BrokerInfo)session.get(BrokerInfoImpl.class,
					primaryKey);

			if (brokerInfo == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchBrokerInfoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(brokerInfo);
		}
		catch (NoSuchBrokerInfoException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected BrokerInfo removeImpl(BrokerInfo brokerInfo)
		throws SystemException {
		brokerInfo = toUnwrappedModel(brokerInfo);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(brokerInfo)) {
				brokerInfo = (BrokerInfo)session.get(BrokerInfoImpl.class,
						brokerInfo.getPrimaryKeyObj());
			}

			if (brokerInfo != null) {
				session.delete(brokerInfo);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (brokerInfo != null) {
			clearCache(brokerInfo);
		}

		return brokerInfo;
	}

	@Override
	public BrokerInfo updateImpl(com.aw.internal.model.BrokerInfo brokerInfo)
		throws SystemException {
		brokerInfo = toUnwrappedModel(brokerInfo);

		boolean isNew = brokerInfo.isNew();

		Session session = null;

		try {
			session = openSession();

			if (brokerInfo.isNew()) {
				session.save(brokerInfo);

				brokerInfo.setNew(false);
			}
			else {
				session.merge(brokerInfo);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(BrokerInfoModelImpl.ENTITY_CACHE_ENABLED,
			BrokerInfoImpl.class, brokerInfo.getPrimaryKey(), brokerInfo);

		return brokerInfo;
	}

	protected BrokerInfo toUnwrappedModel(BrokerInfo brokerInfo) {
		if (brokerInfo instanceof BrokerInfoImpl) {
			return brokerInfo;
		}

		BrokerInfoImpl brokerInfoImpl = new BrokerInfoImpl();

		brokerInfoImpl.setNew(brokerInfo.isNew());
		brokerInfoImpl.setPrimaryKey(brokerInfo.getPrimaryKey());

		brokerInfoImpl.setBrokerId(brokerInfo.getBrokerId());
		brokerInfoImpl.setLicenseId(brokerInfo.getLicenseId());
		brokerInfoImpl.setLicenseCountry(brokerInfo.getLicenseCountry());

		return brokerInfoImpl;
	}

	/**
	 * Returns the broker info with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the broker info
	 * @return the broker info
	 * @throws com.aw.internal.NoSuchBrokerInfoException if a broker info with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BrokerInfo findByPrimaryKey(Serializable primaryKey)
		throws NoSuchBrokerInfoException, SystemException {
		BrokerInfo brokerInfo = fetchByPrimaryKey(primaryKey);

		if (brokerInfo == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchBrokerInfoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return brokerInfo;
	}

	/**
	 * Returns the broker info with the primary key or throws a {@link com.aw.internal.NoSuchBrokerInfoException} if it could not be found.
	 *
	 * @param brokerId the primary key of the broker info
	 * @return the broker info
	 * @throws com.aw.internal.NoSuchBrokerInfoException if a broker info with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BrokerInfo findByPrimaryKey(long brokerId)
		throws NoSuchBrokerInfoException, SystemException {
		return findByPrimaryKey((Serializable)brokerId);
	}

	/**
	 * Returns the broker info with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the broker info
	 * @return the broker info, or <code>null</code> if a broker info with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BrokerInfo fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		BrokerInfo brokerInfo = (BrokerInfo)EntityCacheUtil.getResult(BrokerInfoModelImpl.ENTITY_CACHE_ENABLED,
				BrokerInfoImpl.class, primaryKey);

		if (brokerInfo == _nullBrokerInfo) {
			return null;
		}

		if (brokerInfo == null) {
			Session session = null;

			try {
				session = openSession();

				brokerInfo = (BrokerInfo)session.get(BrokerInfoImpl.class,
						primaryKey);

				if (brokerInfo != null) {
					cacheResult(brokerInfo);
				}
				else {
					EntityCacheUtil.putResult(BrokerInfoModelImpl.ENTITY_CACHE_ENABLED,
						BrokerInfoImpl.class, primaryKey, _nullBrokerInfo);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(BrokerInfoModelImpl.ENTITY_CACHE_ENABLED,
					BrokerInfoImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return brokerInfo;
	}

	/**
	 * Returns the broker info with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param brokerId the primary key of the broker info
	 * @return the broker info, or <code>null</code> if a broker info with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public BrokerInfo fetchByPrimaryKey(long brokerId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)brokerId);
	}

	/**
	 * Returns all the broker infos.
	 *
	 * @return the broker infos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<BrokerInfo> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the broker infos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.BrokerInfoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of broker infos
	 * @param end the upper bound of the range of broker infos (not inclusive)
	 * @return the range of broker infos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<BrokerInfo> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the broker infos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.BrokerInfoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of broker infos
	 * @param end the upper bound of the range of broker infos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of broker infos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<BrokerInfo> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<BrokerInfo> list = (List<BrokerInfo>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_BROKERINFO);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_BROKERINFO;

				if (pagination) {
					sql = sql.concat(BrokerInfoModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<BrokerInfo>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<BrokerInfo>(list);
				}
				else {
					list = (List<BrokerInfo>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the broker infos from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (BrokerInfo brokerInfo : findAll()) {
			remove(brokerInfo);
		}
	}

	/**
	 * Returns the number of broker infos.
	 *
	 * @return the number of broker infos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_BROKERINFO);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the broker info persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.aw.internal.model.BrokerInfo")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<BrokerInfo>> listenersList = new ArrayList<ModelListener<BrokerInfo>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<BrokerInfo>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(BrokerInfoImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_BROKERINFO = "SELECT brokerInfo FROM BrokerInfo brokerInfo";
	private static final String _SQL_COUNT_BROKERINFO = "SELECT COUNT(brokerInfo) FROM BrokerInfo brokerInfo";
	private static final String _ORDER_BY_ENTITY_ALIAS = "brokerInfo.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No BrokerInfo exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(BrokerInfoPersistenceImpl.class);
	private static BrokerInfo _nullBrokerInfo = new BrokerInfoImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<BrokerInfo> toCacheModel() {
				return _nullBrokerInfoCacheModel;
			}
		};

	private static CacheModel<BrokerInfo> _nullBrokerInfoCacheModel = new CacheModel<BrokerInfo>() {
			@Override
			public BrokerInfo toEntityModel() {
				return _nullBrokerInfo;
			}
		};
}