/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.persistence;

import com.aw.internal.NoSuchAaContactException;
import com.aw.internal.model.AaContact;
import com.aw.internal.model.impl.AaContactImpl;
import com.aw.internal.model.impl.AaContactModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the aa contact service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author zl
 * @see AaContactPersistence
 * @see AaContactUtil
 * @generated
 */
public class AaContactPersistenceImpl extends BasePersistenceImpl<AaContact>
	implements AaContactPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link AaContactUtil} to access the aa contact persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = AaContactImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(AaContactModelImpl.ENTITY_CACHE_ENABLED,
			AaContactModelImpl.FINDER_CACHE_ENABLED, AaContactImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(AaContactModelImpl.ENTITY_CACHE_ENABLED,
			AaContactModelImpl.FINDER_CACHE_ENABLED, AaContactImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(AaContactModelImpl.ENTITY_CACHE_ENABLED,
			AaContactModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_EMAIL = new FinderPath(AaContactModelImpl.ENTITY_CACHE_ENABLED,
			AaContactModelImpl.FINDER_CACHE_ENABLED, AaContactImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByemail",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMAIL = new FinderPath(AaContactModelImpl.ENTITY_CACHE_ENABLED,
			AaContactModelImpl.FINDER_CACHE_ENABLED, AaContactImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByemail",
			new String[] { String.class.getName() },
			AaContactModelImpl.EMAIL_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_EMAIL = new FinderPath(AaContactModelImpl.ENTITY_CACHE_ENABLED,
			AaContactModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByemail",
			new String[] { String.class.getName() });

	/**
	 * Returns all the aa contacts where email = &#63;.
	 *
	 * @param email the email
	 * @return the matching aa contacts
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<AaContact> findByemail(String email) throws SystemException {
		return findByemail(email, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the aa contacts where email = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.AaContactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param email the email
	 * @param start the lower bound of the range of aa contacts
	 * @param end the upper bound of the range of aa contacts (not inclusive)
	 * @return the range of matching aa contacts
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<AaContact> findByemail(String email, int start, int end)
		throws SystemException {
		return findByemail(email, start, end, null);
	}

	/**
	 * Returns an ordered range of all the aa contacts where email = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.AaContactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param email the email
	 * @param start the lower bound of the range of aa contacts
	 * @param end the upper bound of the range of aa contacts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching aa contacts
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<AaContact> findByemail(String email, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMAIL;
			finderArgs = new Object[] { email };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_EMAIL;
			finderArgs = new Object[] { email, start, end, orderByComparator };
		}

		List<AaContact> list = (List<AaContact>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (AaContact aaContact : list) {
				if (!Validator.equals(email, aaContact.getEmail())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_AACONTACT_WHERE);

			boolean bindEmail = false;

			if (email == null) {
				query.append(_FINDER_COLUMN_EMAIL_EMAIL_1);
			}
			else if (email.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_EMAIL_EMAIL_3);
			}
			else {
				bindEmail = true;

				query.append(_FINDER_COLUMN_EMAIL_EMAIL_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(AaContactModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindEmail) {
					qPos.add(email);
				}

				if (!pagination) {
					list = (List<AaContact>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<AaContact>(list);
				}
				else {
					list = (List<AaContact>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first aa contact in the ordered set where email = &#63;.
	 *
	 * @param email the email
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching aa contact
	 * @throws com.aw.internal.NoSuchAaContactException if a matching aa contact could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AaContact findByemail_First(String email,
		OrderByComparator orderByComparator)
		throws NoSuchAaContactException, SystemException {
		AaContact aaContact = fetchByemail_First(email, orderByComparator);

		if (aaContact != null) {
			return aaContact;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("email=");
		msg.append(email);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAaContactException(msg.toString());
	}

	/**
	 * Returns the first aa contact in the ordered set where email = &#63;.
	 *
	 * @param email the email
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching aa contact, or <code>null</code> if a matching aa contact could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AaContact fetchByemail_First(String email,
		OrderByComparator orderByComparator) throws SystemException {
		List<AaContact> list = findByemail(email, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last aa contact in the ordered set where email = &#63;.
	 *
	 * @param email the email
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching aa contact
	 * @throws com.aw.internal.NoSuchAaContactException if a matching aa contact could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AaContact findByemail_Last(String email,
		OrderByComparator orderByComparator)
		throws NoSuchAaContactException, SystemException {
		AaContact aaContact = fetchByemail_Last(email, orderByComparator);

		if (aaContact != null) {
			return aaContact;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("email=");
		msg.append(email);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchAaContactException(msg.toString());
	}

	/**
	 * Returns the last aa contact in the ordered set where email = &#63;.
	 *
	 * @param email the email
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching aa contact, or <code>null</code> if a matching aa contact could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AaContact fetchByemail_Last(String email,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByemail(email);

		if (count == 0) {
			return null;
		}

		List<AaContact> list = findByemail(email, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the aa contacts before and after the current aa contact in the ordered set where email = &#63;.
	 *
	 * @param contactId the primary key of the current aa contact
	 * @param email the email
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next aa contact
	 * @throws com.aw.internal.NoSuchAaContactException if a aa contact with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AaContact[] findByemail_PrevAndNext(long contactId, String email,
		OrderByComparator orderByComparator)
		throws NoSuchAaContactException, SystemException {
		AaContact aaContact = findByPrimaryKey(contactId);

		Session session = null;

		try {
			session = openSession();

			AaContact[] array = new AaContactImpl[3];

			array[0] = getByemail_PrevAndNext(session, aaContact, email,
					orderByComparator, true);

			array[1] = aaContact;

			array[2] = getByemail_PrevAndNext(session, aaContact, email,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected AaContact getByemail_PrevAndNext(Session session,
		AaContact aaContact, String email, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_AACONTACT_WHERE);

		boolean bindEmail = false;

		if (email == null) {
			query.append(_FINDER_COLUMN_EMAIL_EMAIL_1);
		}
		else if (email.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_EMAIL_EMAIL_3);
		}
		else {
			bindEmail = true;

			query.append(_FINDER_COLUMN_EMAIL_EMAIL_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(AaContactModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindEmail) {
			qPos.add(email);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(aaContact);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<AaContact> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the aa contacts where email = &#63; from the database.
	 *
	 * @param email the email
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByemail(String email) throws SystemException {
		for (AaContact aaContact : findByemail(email, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(aaContact);
		}
	}

	/**
	 * Returns the number of aa contacts where email = &#63;.
	 *
	 * @param email the email
	 * @return the number of matching aa contacts
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByemail(String email) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_EMAIL;

		Object[] finderArgs = new Object[] { email };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_AACONTACT_WHERE);

			boolean bindEmail = false;

			if (email == null) {
				query.append(_FINDER_COLUMN_EMAIL_EMAIL_1);
			}
			else if (email.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_EMAIL_EMAIL_3);
			}
			else {
				bindEmail = true;

				query.append(_FINDER_COLUMN_EMAIL_EMAIL_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindEmail) {
					qPos.add(email);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_EMAIL_EMAIL_1 = "aaContact.email IS NULL";
	private static final String _FINDER_COLUMN_EMAIL_EMAIL_2 = "aaContact.email = ?";
	private static final String _FINDER_COLUMN_EMAIL_EMAIL_3 = "(aaContact.email IS NULL OR aaContact.email = '')";

	public AaContactPersistenceImpl() {
		setModelClass(AaContact.class);
	}

	/**
	 * Caches the aa contact in the entity cache if it is enabled.
	 *
	 * @param aaContact the aa contact
	 */
	@Override
	public void cacheResult(AaContact aaContact) {
		EntityCacheUtil.putResult(AaContactModelImpl.ENTITY_CACHE_ENABLED,
			AaContactImpl.class, aaContact.getPrimaryKey(), aaContact);

		aaContact.resetOriginalValues();
	}

	/**
	 * Caches the aa contacts in the entity cache if it is enabled.
	 *
	 * @param aaContacts the aa contacts
	 */
	@Override
	public void cacheResult(List<AaContact> aaContacts) {
		for (AaContact aaContact : aaContacts) {
			if (EntityCacheUtil.getResult(
						AaContactModelImpl.ENTITY_CACHE_ENABLED,
						AaContactImpl.class, aaContact.getPrimaryKey()) == null) {
				cacheResult(aaContact);
			}
			else {
				aaContact.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all aa contacts.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(AaContactImpl.class.getName());
		}

		EntityCacheUtil.clearCache(AaContactImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the aa contact.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(AaContact aaContact) {
		EntityCacheUtil.removeResult(AaContactModelImpl.ENTITY_CACHE_ENABLED,
			AaContactImpl.class, aaContact.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<AaContact> aaContacts) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (AaContact aaContact : aaContacts) {
			EntityCacheUtil.removeResult(AaContactModelImpl.ENTITY_CACHE_ENABLED,
				AaContactImpl.class, aaContact.getPrimaryKey());
		}
	}

	/**
	 * Creates a new aa contact with the primary key. Does not add the aa contact to the database.
	 *
	 * @param contactId the primary key for the new aa contact
	 * @return the new aa contact
	 */
	@Override
	public AaContact create(long contactId) {
		AaContact aaContact = new AaContactImpl();

		aaContact.setNew(true);
		aaContact.setPrimaryKey(contactId);

		return aaContact;
	}

	/**
	 * Removes the aa contact with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param contactId the primary key of the aa contact
	 * @return the aa contact that was removed
	 * @throws com.aw.internal.NoSuchAaContactException if a aa contact with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AaContact remove(long contactId)
		throws NoSuchAaContactException, SystemException {
		return remove((Serializable)contactId);
	}

	/**
	 * Removes the aa contact with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the aa contact
	 * @return the aa contact that was removed
	 * @throws com.aw.internal.NoSuchAaContactException if a aa contact with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AaContact remove(Serializable primaryKey)
		throws NoSuchAaContactException, SystemException {
		Session session = null;

		try {
			session = openSession();

			AaContact aaContact = (AaContact)session.get(AaContactImpl.class,
					primaryKey);

			if (aaContact == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchAaContactException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(aaContact);
		}
		catch (NoSuchAaContactException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected AaContact removeImpl(AaContact aaContact)
		throws SystemException {
		aaContact = toUnwrappedModel(aaContact);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(aaContact)) {
				aaContact = (AaContact)session.get(AaContactImpl.class,
						aaContact.getPrimaryKeyObj());
			}

			if (aaContact != null) {
				session.delete(aaContact);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (aaContact != null) {
			clearCache(aaContact);
		}

		return aaContact;
	}

	@Override
	public AaContact updateImpl(com.aw.internal.model.AaContact aaContact)
		throws SystemException {
		aaContact = toUnwrappedModel(aaContact);

		boolean isNew = aaContact.isNew();

		AaContactModelImpl aaContactModelImpl = (AaContactModelImpl)aaContact;

		Session session = null;

		try {
			session = openSession();

			if (aaContact.isNew()) {
				session.save(aaContact);

				aaContact.setNew(false);
			}
			else {
				session.merge(aaContact);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !AaContactModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((aaContactModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMAIL.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						aaContactModelImpl.getOriginalEmail()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_EMAIL, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMAIL,
					args);

				args = new Object[] { aaContactModelImpl.getEmail() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_EMAIL, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_EMAIL,
					args);
			}
		}

		EntityCacheUtil.putResult(AaContactModelImpl.ENTITY_CACHE_ENABLED,
			AaContactImpl.class, aaContact.getPrimaryKey(), aaContact);

		return aaContact;
	}

	protected AaContact toUnwrappedModel(AaContact aaContact) {
		if (aaContact instanceof AaContactImpl) {
			return aaContact;
		}

		AaContactImpl aaContactImpl = new AaContactImpl();

		aaContactImpl.setNew(aaContact.isNew());
		aaContactImpl.setPrimaryKey(aaContact.getPrimaryKey());

		aaContactImpl.setContactId(aaContact.getContactId());
		aaContactImpl.setPhone(aaContact.getPhone());
		aaContactImpl.setAreaCode(aaContact.getAreaCode());
		aaContactImpl.setEmail(aaContact.getEmail());
		aaContactImpl.setWechat(aaContact.getWechat());
		aaContactImpl.setFacebook(aaContact.getFacebook());
		aaContactImpl.setTwitter(aaContact.getTwitter());
		aaContactImpl.setWeibo(aaContact.getWeibo());
		aaContactImpl.setStreet1(aaContact.getStreet1());
		aaContactImpl.setStreet2(aaContact.getStreet2());
		aaContactImpl.setCity(aaContact.getCity());
		aaContactImpl.setState(aaContact.getState());
		aaContactImpl.setCountry(aaContact.getCountry());
		aaContactImpl.setZipCode(aaContact.getZipCode());
		aaContactImpl.setIsMailDiff(aaContact.getIsMailDiff());
		aaContactImpl.setMailstreet1(aaContact.getMailstreet1());
		aaContactImpl.setMailstreet2(aaContact.getMailstreet2());
		aaContactImpl.setMailcity(aaContact.getMailcity());
		aaContactImpl.setMailstate(aaContact.getMailstate());
		aaContactImpl.setMailcountry(aaContact.getMailcountry());
		aaContactImpl.setMailzipCode(aaContact.getMailzipCode());

		return aaContactImpl;
	}

	/**
	 * Returns the aa contact with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the aa contact
	 * @return the aa contact
	 * @throws com.aw.internal.NoSuchAaContactException if a aa contact with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AaContact findByPrimaryKey(Serializable primaryKey)
		throws NoSuchAaContactException, SystemException {
		AaContact aaContact = fetchByPrimaryKey(primaryKey);

		if (aaContact == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchAaContactException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return aaContact;
	}

	/**
	 * Returns the aa contact with the primary key or throws a {@link com.aw.internal.NoSuchAaContactException} if it could not be found.
	 *
	 * @param contactId the primary key of the aa contact
	 * @return the aa contact
	 * @throws com.aw.internal.NoSuchAaContactException if a aa contact with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AaContact findByPrimaryKey(long contactId)
		throws NoSuchAaContactException, SystemException {
		return findByPrimaryKey((Serializable)contactId);
	}

	/**
	 * Returns the aa contact with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the aa contact
	 * @return the aa contact, or <code>null</code> if a aa contact with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AaContact fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		AaContact aaContact = (AaContact)EntityCacheUtil.getResult(AaContactModelImpl.ENTITY_CACHE_ENABLED,
				AaContactImpl.class, primaryKey);

		if (aaContact == _nullAaContact) {
			return null;
		}

		if (aaContact == null) {
			Session session = null;

			try {
				session = openSession();

				aaContact = (AaContact)session.get(AaContactImpl.class,
						primaryKey);

				if (aaContact != null) {
					cacheResult(aaContact);
				}
				else {
					EntityCacheUtil.putResult(AaContactModelImpl.ENTITY_CACHE_ENABLED,
						AaContactImpl.class, primaryKey, _nullAaContact);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(AaContactModelImpl.ENTITY_CACHE_ENABLED,
					AaContactImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return aaContact;
	}

	/**
	 * Returns the aa contact with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param contactId the primary key of the aa contact
	 * @return the aa contact, or <code>null</code> if a aa contact with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public AaContact fetchByPrimaryKey(long contactId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)contactId);
	}

	/**
	 * Returns all the aa contacts.
	 *
	 * @return the aa contacts
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<AaContact> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the aa contacts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.AaContactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of aa contacts
	 * @param end the upper bound of the range of aa contacts (not inclusive)
	 * @return the range of aa contacts
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<AaContact> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the aa contacts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.AaContactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of aa contacts
	 * @param end the upper bound of the range of aa contacts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of aa contacts
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<AaContact> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<AaContact> list = (List<AaContact>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_AACONTACT);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_AACONTACT;

				if (pagination) {
					sql = sql.concat(AaContactModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<AaContact>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<AaContact>(list);
				}
				else {
					list = (List<AaContact>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the aa contacts from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (AaContact aaContact : findAll()) {
			remove(aaContact);
		}
	}

	/**
	 * Returns the number of aa contacts.
	 *
	 * @return the number of aa contacts
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_AACONTACT);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the aa contact persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.aw.internal.model.AaContact")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<AaContact>> listenersList = new ArrayList<ModelListener<AaContact>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<AaContact>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(AaContactImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_AACONTACT = "SELECT aaContact FROM AaContact aaContact";
	private static final String _SQL_SELECT_AACONTACT_WHERE = "SELECT aaContact FROM AaContact aaContact WHERE ";
	private static final String _SQL_COUNT_AACONTACT = "SELECT COUNT(aaContact) FROM AaContact aaContact";
	private static final String _SQL_COUNT_AACONTACT_WHERE = "SELECT COUNT(aaContact) FROM AaContact aaContact WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "aaContact.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No AaContact exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No AaContact exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(AaContactPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"state"
			});
	private static AaContact _nullAaContact = new AaContactImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<AaContact> toCacheModel() {
				return _nullAaContactCacheModel;
			}
		};

	private static CacheModel<AaContact> _nullAaContactCacheModel = new CacheModel<AaContact>() {
			@Override
			public AaContact toEntityModel() {
				return _nullAaContact;
			}
		};
}