/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.impl;

import com.aw.internal.model.BrokerInfo;
import com.aw.internal.service.base.BrokerInfoLocalServiceBaseImpl;
import com.aw.internal.service.persistence.BrokerInfoUtil;
import com.aw.internal.service.util.DataUtil;
import com.aw.internal.service.util.ErrorConstants;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONObject;

/**
 * The implementation of the broker info local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.liferay.sample.service.BrokerInfoLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author zl
 * @see com.aw.internal.service.base.BrokerInfoLocalServiceBaseImpl
 * @see com.liferay.sample.service.BrokerInfoLocalServiceUtil
 */
public class BrokerInfoLocalServiceImpl extends BrokerInfoLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.liferay.sample.service.BrokerInfoLocalServiceUtil} to access the broker info local service.
	 */
	public long addBroker(String lics, String country){
		long id = -1;
		try {
			id = CounterLocalServiceUtil.increment(BrokerInfo.class.getName());
			BrokerInfo br = BrokerInfoUtil.create(id);
			br.setLicenseId(lics);
			br.setLicenseCountry(country);
			br.persist();
			return id;
		} catch (SystemException e) {
			e.printStackTrace();
			return -1;
		}
	}
	
	public JSONObject getBroker(long id){
		BrokerInfo bi;
		try {
			bi = BrokerInfoUtil.fetchByPrimaryKey(id);
		} catch (SystemException e) {
			return DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
		}
		return DataUtil.getResponse(DataUtil.getBrokerJson(bi), null, ErrorConstants.SUCCESS);
	}
	
	public String updatedBroker(){
		return "funtion not open yet";
	}
	
	public boolean validateBroker(long brokerId){
		boolean isValid = false;
		
		try {
			BrokerInfo bi = BrokerInfoUtil.fetchByPrimaryKey(brokerId);
			if(bi != null){
				isValid = true;
			}
		} catch (SystemException e) {
			
		}
		
		return isValid;
	}
}