package com.aw.internal.service.util;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.security.permission.PermissionChecker;
import com.liferay.portal.security.permission.PermissionThreadLocal;

public class PermisionChecker {
	
	public static final String SALES_US = "SALES_US";
	public static final String SALES_CN = "SALES_CN";
	public static final String SALES_JP = "SALES_JP";
	public static final String SALES_HK = "SALES_HK";
	public static final String SALES_GL_MNG = "Global_Sale_Manager";
	
	public static final String OPERATOR = "Operator";
	public static final String ACCOUNTING = "Accounting";
	public static final String MANAGEMENT = "Management";
	public static final String HR = "Hr";
	public static final String ADMIN = "Administrator";

	private Set<String>  permitedRoles;  
	
	//add ADMIN by default
	public PermisionChecker(){
		this.permitedRoles = new HashSet<String>();
		this.permitedRoles.add(ADMIN);
	}
	
	public PermisionChecker addRole(String role){
		this.permitedRoles.add(role);
		return this;
	}
	
	public Set<String> getRolesForUser(){
		PermissionChecker pc = PermissionThreadLocal.getPermissionChecker();
		User user = pc.getUser();
		
		Set<String> set = new HashSet<String>();
		
		try {
			List<Role> roles = user.getRoles();
			
			if(roles != null && !roles.isEmpty()){
				for (Role role : roles) {
					set.add(role.getName());
				}	
				
			}else{
				
			}
		} catch (SystemException e) { 
			e.printStackTrace();			
		}
		return set;
	}
	
	public User permissionCheck(){
		
//		for(String role : this.permitedRoles){
//			System.out.println(role);
//		}
		
		PermissionChecker pc = PermissionThreadLocal.getPermissionChecker();
		User user = pc.getUser();
		
		try {
			List<Role> roles = user.getRoles();
			
			if(roles != null && !roles.isEmpty()){
				for (Role role : roles) {
					if (this.permitedRoles.contains(role.getName())){
						return user;
					}
				}	
				return null;
			}else{
				return null;
			}
		} catch (SystemException e) { 
			e.printStackTrace();
			return null;
		}	
			
	}
	
	public User permissionCheck(String permitedRole){
		
		PermissionChecker pc = PermissionThreadLocal.getPermissionChecker();
		User user = pc.getUser();
		
		try {
			List<Role> roles = user.getRoles();
			
			if(roles != null && !roles.isEmpty()){
				for (Role role : roles) {
					if (role.getName().equals(permitedRole) || role.getName().equals(ADMIN)){
						return user;
					}
				}	
				return null;
			}else{
				return null;
			}
		} catch (SystemException e) { 
			e.printStackTrace();
			return null;
		}	
	}
}
