/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.persistence;

import com.aw.internal.NoSuchFailReasonException;
import com.aw.internal.model.FailReason;
import com.aw.internal.model.impl.FailReasonImpl;
import com.aw.internal.model.impl.FailReasonModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the fail reason service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author zl
 * @see FailReasonPersistence
 * @see FailReasonUtil
 * @generated
 */
public class FailReasonPersistenceImpl extends BasePersistenceImpl<FailReason>
	implements FailReasonPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link FailReasonUtil} to access the fail reason persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = FailReasonImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(FailReasonModelImpl.ENTITY_CACHE_ENABLED,
			FailReasonModelImpl.FINDER_CACHE_ENABLED, FailReasonImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(FailReasonModelImpl.ENTITY_CACHE_ENABLED,
			FailReasonModelImpl.FINDER_CACHE_ENABLED, FailReasonImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(FailReasonModelImpl.ENTITY_CACHE_ENABLED,
			FailReasonModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public FailReasonPersistenceImpl() {
		setModelClass(FailReason.class);
	}

	/**
	 * Caches the fail reason in the entity cache if it is enabled.
	 *
	 * @param failReason the fail reason
	 */
	@Override
	public void cacheResult(FailReason failReason) {
		EntityCacheUtil.putResult(FailReasonModelImpl.ENTITY_CACHE_ENABLED,
			FailReasonImpl.class, failReason.getPrimaryKey(), failReason);

		failReason.resetOriginalValues();
	}

	/**
	 * Caches the fail reasons in the entity cache if it is enabled.
	 *
	 * @param failReasons the fail reasons
	 */
	@Override
	public void cacheResult(List<FailReason> failReasons) {
		for (FailReason failReason : failReasons) {
			if (EntityCacheUtil.getResult(
						FailReasonModelImpl.ENTITY_CACHE_ENABLED,
						FailReasonImpl.class, failReason.getPrimaryKey()) == null) {
				cacheResult(failReason);
			}
			else {
				failReason.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all fail reasons.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(FailReasonImpl.class.getName());
		}

		EntityCacheUtil.clearCache(FailReasonImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the fail reason.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(FailReason failReason) {
		EntityCacheUtil.removeResult(FailReasonModelImpl.ENTITY_CACHE_ENABLED,
			FailReasonImpl.class, failReason.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<FailReason> failReasons) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (FailReason failReason : failReasons) {
			EntityCacheUtil.removeResult(FailReasonModelImpl.ENTITY_CACHE_ENABLED,
				FailReasonImpl.class, failReason.getPrimaryKey());
		}
	}

	/**
	 * Creates a new fail reason with the primary key. Does not add the fail reason to the database.
	 *
	 * @param rsnCd the primary key for the new fail reason
	 * @return the new fail reason
	 */
	@Override
	public FailReason create(String rsnCd) {
		FailReason failReason = new FailReasonImpl();

		failReason.setNew(true);
		failReason.setPrimaryKey(rsnCd);

		return failReason;
	}

	/**
	 * Removes the fail reason with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param rsnCd the primary key of the fail reason
	 * @return the fail reason that was removed
	 * @throws com.aw.internal.NoSuchFailReasonException if a fail reason with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FailReason remove(String rsnCd)
		throws NoSuchFailReasonException, SystemException {
		return remove((Serializable)rsnCd);
	}

	/**
	 * Removes the fail reason with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the fail reason
	 * @return the fail reason that was removed
	 * @throws com.aw.internal.NoSuchFailReasonException if a fail reason with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FailReason remove(Serializable primaryKey)
		throws NoSuchFailReasonException, SystemException {
		Session session = null;

		try {
			session = openSession();

			FailReason failReason = (FailReason)session.get(FailReasonImpl.class,
					primaryKey);

			if (failReason == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchFailReasonException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(failReason);
		}
		catch (NoSuchFailReasonException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected FailReason removeImpl(FailReason failReason)
		throws SystemException {
		failReason = toUnwrappedModel(failReason);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(failReason)) {
				failReason = (FailReason)session.get(FailReasonImpl.class,
						failReason.getPrimaryKeyObj());
			}

			if (failReason != null) {
				session.delete(failReason);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (failReason != null) {
			clearCache(failReason);
		}

		return failReason;
	}

	@Override
	public FailReason updateImpl(com.aw.internal.model.FailReason failReason)
		throws SystemException {
		failReason = toUnwrappedModel(failReason);

		boolean isNew = failReason.isNew();

		Session session = null;

		try {
			session = openSession();

			if (failReason.isNew()) {
				session.save(failReason);

				failReason.setNew(false);
			}
			else {
				session.merge(failReason);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(FailReasonModelImpl.ENTITY_CACHE_ENABLED,
			FailReasonImpl.class, failReason.getPrimaryKey(), failReason);

		return failReason;
	}

	protected FailReason toUnwrappedModel(FailReason failReason) {
		if (failReason instanceof FailReasonImpl) {
			return failReason;
		}

		FailReasonImpl failReasonImpl = new FailReasonImpl();

		failReasonImpl.setNew(failReason.isNew());
		failReasonImpl.setPrimaryKey(failReason.getPrimaryKey());

		failReasonImpl.setRsnCd(failReason.getRsnCd());
		failReasonImpl.setDesc(failReason.getDesc());

		return failReasonImpl;
	}

	/**
	 * Returns the fail reason with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the fail reason
	 * @return the fail reason
	 * @throws com.aw.internal.NoSuchFailReasonException if a fail reason with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FailReason findByPrimaryKey(Serializable primaryKey)
		throws NoSuchFailReasonException, SystemException {
		FailReason failReason = fetchByPrimaryKey(primaryKey);

		if (failReason == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchFailReasonException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return failReason;
	}

	/**
	 * Returns the fail reason with the primary key or throws a {@link com.aw.internal.NoSuchFailReasonException} if it could not be found.
	 *
	 * @param rsnCd the primary key of the fail reason
	 * @return the fail reason
	 * @throws com.aw.internal.NoSuchFailReasonException if a fail reason with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FailReason findByPrimaryKey(String rsnCd)
		throws NoSuchFailReasonException, SystemException {
		return findByPrimaryKey((Serializable)rsnCd);
	}

	/**
	 * Returns the fail reason with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the fail reason
	 * @return the fail reason, or <code>null</code> if a fail reason with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FailReason fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		FailReason failReason = (FailReason)EntityCacheUtil.getResult(FailReasonModelImpl.ENTITY_CACHE_ENABLED,
				FailReasonImpl.class, primaryKey);

		if (failReason == _nullFailReason) {
			return null;
		}

		if (failReason == null) {
			Session session = null;

			try {
				session = openSession();

				failReason = (FailReason)session.get(FailReasonImpl.class,
						primaryKey);

				if (failReason != null) {
					cacheResult(failReason);
				}
				else {
					EntityCacheUtil.putResult(FailReasonModelImpl.ENTITY_CACHE_ENABLED,
						FailReasonImpl.class, primaryKey, _nullFailReason);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(FailReasonModelImpl.ENTITY_CACHE_ENABLED,
					FailReasonImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return failReason;
	}

	/**
	 * Returns the fail reason with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param rsnCd the primary key of the fail reason
	 * @return the fail reason, or <code>null</code> if a fail reason with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FailReason fetchByPrimaryKey(String rsnCd) throws SystemException {
		return fetchByPrimaryKey((Serializable)rsnCd);
	}

	/**
	 * Returns all the fail reasons.
	 *
	 * @return the fail reasons
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<FailReason> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the fail reasons.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.FailReasonModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of fail reasons
	 * @param end the upper bound of the range of fail reasons (not inclusive)
	 * @return the range of fail reasons
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<FailReason> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the fail reasons.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.FailReasonModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of fail reasons
	 * @param end the upper bound of the range of fail reasons (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of fail reasons
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<FailReason> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<FailReason> list = (List<FailReason>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_FAILREASON);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_FAILREASON;

				if (pagination) {
					sql = sql.concat(FailReasonModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<FailReason>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<FailReason>(list);
				}
				else {
					list = (List<FailReason>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the fail reasons from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (FailReason failReason : findAll()) {
			remove(failReason);
		}
	}

	/**
	 * Returns the number of fail reasons.
	 *
	 * @return the number of fail reasons
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_FAILREASON);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the fail reason persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.aw.internal.model.FailReason")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<FailReason>> listenersList = new ArrayList<ModelListener<FailReason>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<FailReason>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(FailReasonImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_FAILREASON = "SELECT failReason FROM FailReason failReason";
	private static final String _SQL_COUNT_FAILREASON = "SELECT COUNT(failReason) FROM FailReason failReason";
	private static final String _ORDER_BY_ENTITY_ALIAS = "failReason.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No FailReason exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(FailReasonPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"desc"
			});
	private static FailReason _nullFailReason = new FailReasonImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<FailReason> toCacheModel() {
				return _nullFailReasonCacheModel;
			}
		};

	private static CacheModel<FailReason> _nullFailReasonCacheModel = new CacheModel<FailReason>() {
			@Override
			public FailReason toEntityModel() {
				return _nullFailReason;
			}
		};
}