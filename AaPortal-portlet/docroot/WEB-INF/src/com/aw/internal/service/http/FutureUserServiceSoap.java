/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.http;

import com.aw.internal.service.FutureUserServiceUtil;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.rmi.RemoteException;

/**
 * Provides the SOAP utility for the
 * {@link com.aw.internal.service.FutureUserServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it is difficult for SOAP to
 * support certain types.
 *
 * <p>
 * ServiceBuilder follows certain rules in translating the methods. For example,
 * if the method in the service utility returns a {@link java.util.List}, that
 * is translated to an array of {@link com.aw.internal.model.FutureUserSoap}.
 * If the method in the service utility returns a
 * {@link com.aw.internal.model.FutureUser}, that is translated to a
 * {@link com.aw.internal.model.FutureUserSoap}. Methods that SOAP cannot
 * safely wire are skipped.
 * </p>
 *
 * <p>
 * The benefits of using the SOAP utility is that it is cross platform
 * compatible. SOAP allows different languages like Java, .NET, C++, PHP, and
 * even Perl, to call the generated services. One drawback of SOAP is that it is
 * slow because it needs to serialize all calls into a text format (XML).
 * </p>
 *
 * <p>
 * You can see a list of services at http://localhost:8080/api/axis. Set the
 * property <b>axis.servlet.hosts.allowed</b> in portal.properties to configure
 * security.
 * </p>
 *
 * <p>
 * The SOAP utility is only generated for remote services.
 * </p>
 *
 * @author zl
 * @see FutureUserServiceHttp
 * @see com.aw.internal.model.FutureUserSoap
 * @see com.aw.internal.service.FutureUserServiceUtil
 * @generated
 */
public class FutureUserServiceSoap {
	public static java.lang.String recordUser(java.lang.String email,
		java.lang.String firstN, java.lang.String lastN,
		java.lang.String residency, int areaCode, long phone)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = FutureUserServiceUtil.recordUser(email,
					firstN, lastN, residency, areaCode, phone);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String takeUser(java.lang.String email)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = FutureUserServiceUtil.takeUser(email);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String releaseTaken(java.lang.String email)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = FutureUserServiceUtil.releaseTaken(email);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getAllActiveFutureUsers()
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = FutureUserServiceUtil.getAllActiveFutureUsers();

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getMyActiveFutureUsers()
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = FutureUserServiceUtil.getMyActiveFutureUsers();

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String deactivateUser(java.lang.String email)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = FutureUserServiceUtil.deactivateUser(email);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	private static Log _log = LogFactoryUtil.getLog(FutureUserServiceSoap.class);
}