package com.aw.internal.service.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.User;
import com.liferay.portal.security.permission.PermissionChecker;
import com.liferay.portal.security.permission.PermissionThreadLocal;
import com.liferay.portal.service.PasswordTrackerLocalServiceUtil;


public class CommonUtil {
	
	public static String getTimeString(){
		StringBuffer sb = new StringBuffer();
        SimpleDateFormat sdf=new SimpleDateFormat("yyMMddHHmmss");
        sdf.setTimeZone(TimeZone.getTimeZone("Etc/GMT"));
        sb.append(sdf.format(new Date()));
        return sb.toString();
	}
	
	public static boolean withinRangeYMD(Date date, String startDate, String endDate){
		
		boolean isWithin = true;
		
		try{
			SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMdd");
	        int dateInt = Integer.parseInt(sdf.format(date));
			
			if(startDate != null && startDate.trim().length() > 0 && Integer.parseInt(startDate) > dateInt){
				isWithin = false;
			}
			
			if(endDate != null && endDate.trim().length() > 0 && Integer.parseInt(endDate) < dateInt){
				isWithin = false;
			}
		}catch(NumberFormatException e){
			//then the input format is something wrong, just return true, ignore the filter
		}
		
		return isWithin;
	}
	
	public boolean checkAccountPassword(String inputPassword) throws PortalException, SystemException {
		
		PermissionChecker pc = PermissionThreadLocal.getPermissionChecker();
		User user = pc.getUser();
		if(user != null){
			return PasswordTrackerLocalServiceUtil.isSameAsCurrentPassword(user.getUserId(), inputPassword);
		}else{
			return false;
		}
				
	}
}
