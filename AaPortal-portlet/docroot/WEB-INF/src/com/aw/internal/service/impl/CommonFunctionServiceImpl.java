/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.aw.internal.model.ActivePhase;
import com.aw.internal.model.CoinOrder;
import com.aw.internal.model.CoinUser;
import com.aw.internal.model.UnitPrice;
import com.aw.internal.service.CoinCountLocalServiceUtil;
import com.aw.internal.service.UnitPriceLocalServiceUtil;
import com.aw.internal.service.base.CommonFunctionServiceBaseImpl;
import com.aw.internal.service.persistence.ActivePhaseUtil;
import com.aw.internal.service.persistence.CoinOrderUtil;
import com.aw.internal.service.persistence.CoinUserUtil;
import com.aw.internal.service.persistence.UnitPricePK;
import com.aw.internal.service.util.CommonUtil;
import com.aw.internal.service.util.DataUtil;
import com.aw.internal.service.util.ErrorConstants;
import com.aw.internal.service.util.PermisionChecker;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.model.Contact;
import com.liferay.portal.model.Phone;
import com.liferay.portal.model.User;
import com.liferay.portal.security.permission.PermissionChecker;
import com.liferay.portal.security.permission.PermissionThreadLocal;
import com.liferay.portal.service.UserLocalServiceUtil;

/**
 * The implementation of the common function remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.aw.internal.service.CommonFunctionService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author zl
 * @see com.aw.internal.service.base.CommonFunctionServiceBaseImpl
 * @see com.aw.internal.service.CommonFunctionServiceUtil
 */
public class CommonFunctionServiceImpl extends CommonFunctionServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.aw.internal.service.CommonFunctionServiceUtil} to access the common function remote service.
	 */
	public boolean checkPassword(String password){
		
		boolean matched =  false;
		try {
			matched = new CommonUtil().checkAccountPassword(password);
		} catch (PortalException e) {			
		} catch (SystemException e) {			
		}
		return matched;
	}
	
	public JSONObject getCoinCounts(){
		
		User user = new PermisionChecker().permissionCheck();//only for Admin
		
		if(user != null){
			
			return CoinCountLocalServiceUtil.getCoinCounts();
			
		}else{
			return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
		}
		
	}
	
	public boolean endCoinCountPhase(int endPhase, int toPhase){
		User user = new PermisionChecker().permissionCheck();//only for Admin
		
		if(user != null){
			
			return CoinCountLocalServiceUtil.endPhase(endPhase, toPhase);
			
		}else{
			return false;
		}
	}
	
	public JSONObject getPhasePrice(String currency, String userId){
		
		//first get the active phase
		String region = null;
		try {
			CoinUser cu = CoinUserUtil.fetchByPrimaryKey(userId);
			if(cu != null){
				region = cu.getNationality();
			}
		} catch (SystemException e1) {
			e1.printStackTrace();
		}
		if(region == null){
			return DataUtil.getResponse(null, null, ErrorConstants.INVALID_USR_INFO);
		}
		
		int phase = 0;
		ActivePhase ap = null;
		try {
			ap = ActivePhaseUtil.fetchByPrimaryKey(region);
		} catch (SystemException e1) {
			e1.printStackTrace();
		}
		if(ap != null){
			phase = ap.getSalePhase();
		}else{//if not found, use "US" to search again
			try {
				ap = ActivePhaseUtil.fetchByPrimaryKey("US");
			} catch (SystemException e) {
				e.printStackTrace();
			}
			if(ap != null){
				phase = ap.getSalePhase();
			}
		}
		
		UnitPrice up;
		try {
			up = UnitPriceLocalServiceUtil.fetchUnitPrice(new UnitPricePK(currency, phase));
			if(up != null && "Y".equals(up.getActiveFlag())){
				return DataUtil.getResponse(DataUtil.getUnitPrice(up), null, ErrorConstants.SUCCESS);
			}else{
				return DataUtil.getResponse(null, null, ErrorConstants.INVALID_PHASE_OR_PRICE);
			}
		} catch (SystemException e) {
			
			e.printStackTrace();
			return DataUtil.getResponse(null, null, ErrorConstants.INVALID_PHASE_OR_PRICE);
		}
		
	}
	
	public JSONObject getUserInfo(){
		
		PermissionChecker pc = PermissionThreadLocal.getPermissionChecker();
		User user = pc.getUser();
				
		JSONObject dataJson = JSONFactoryUtil.createJSONObject();		
	
		String emailAddress = "";
		String fname = "";
		String lname = "";
		Contact userContact = null;
		String birthday = "";
		String phoneNumber = "";
 
		
		if (user != null){

				//Get Personal Information
				emailAddress = user.getEmailAddress();
				fname = user.getFirstName();
				lname = user.getLastName();
				
				//Get Contact/Birthday/Addresses/Phone Numbers Information
				try {
					userContact = user.getContact();
					if (userContact != null){
						Date date = userContact.getBirthday();
						DateFormat df = new SimpleDateFormat("MM/dd/yyyy");			
					    birthday = df.format(date);
					    					  
					    // Get phone number					    
						List<Phone> phoneList=user.getPhones();				
						if(phoneList !=null && phoneList.size() > 0 )
						{
							for(int i=0; i <phoneList.size(); ++i)
							{
								Phone phone =(Phone) phoneList.get(i);
								if(phone !=null && phone.isPrimary())
								{
								  phoneNumber = phone.getNumber();
								  break;
								}		
							}
						}
					    
					    
					}
				} catch (PortalException | SystemException e) {
				
				}

			    dataJson.put("userId", user.getUserId());		    
			    dataJson.put("emailAddress", emailAddress);
			    dataJson.put("firstName",fname);
			    dataJson.put("lastName",lname);
			    dataJson.put("birthday",birthday);
			    dataJson.put("phoneNumber", phoneNumber);		    
		}
		return DataUtil.getResponse(dataJson, null, ErrorConstants.SUCCESS);     
	}
	
	public boolean updatePassword(String origin, String new1, String new2){
		
		PermissionChecker pc = PermissionThreadLocal.getPermissionChecker();
		User user = pc.getUser();
		
		if(checkPassword(origin)){
			try {
				UserLocalServiceUtil.updatePassword(user.getUserId(), new1, new2, false);
				return true;
			} catch (PortalException | SystemException e) {				
				return false;
			}
		}else{
			return false;
		}
		
	}
	
}