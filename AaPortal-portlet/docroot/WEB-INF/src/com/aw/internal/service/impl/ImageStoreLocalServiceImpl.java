/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.impl;

import java.util.Date;

import org.apache.commons.codec.binary.Base64;

import com.aw.internal.model.ImageStore;
import com.aw.internal.service.base.ImageStoreLocalServiceBaseImpl;
import com.aw.internal.service.persistence.ImageStoreUtil;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.dao.jdbc.OutputBlob;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayInputStream;

/**
 * The implementation of the image store local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.aw.internal.service.ImageStoreLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author zl
 * @see com.aw.internal.service.base.ImageStoreLocalServiceBaseImpl
 * @see com.aw.internal.service.ImageStoreLocalServiceUtil
 */
public class ImageStoreLocalServiceImpl extends ImageStoreLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.aw.internal.service.ImageStoreLocalServiceUtil} to access the image store local service.
	 */
	public ImageStore addImage(String base64Str) throws SystemException{
				
		long id = CounterLocalServiceUtil.increment(ImageStore.class.getName());
		
		ImageStore ims = ImageStoreUtil.create(id);
		
	
		
		//create blob 
		byte[] bytes = Base64.decodeBase64(base64Str);
		UnsyncByteArrayInputStream inputStream =new UnsyncByteArrayInputStream(bytes);
		OutputBlob imageBlob = new OutputBlob(inputStream, bytes.length);
		
		ims.setContent(imageBlob);
		ims.setActiveFlag("Y");
		ims.setAddDate(new Date());
		
		ims.persist();
		
		return ims;
	}
}