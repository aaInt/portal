/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.persistence;

import com.aw.internal.NoSuchImageStoreException;
import com.aw.internal.model.ImageStore;
import com.aw.internal.model.impl.ImageStoreImpl;
import com.aw.internal.model.impl.ImageStoreModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the image store service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author zl
 * @see ImageStorePersistence
 * @see ImageStoreUtil
 * @generated
 */
public class ImageStorePersistenceImpl extends BasePersistenceImpl<ImageStore>
	implements ImageStorePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ImageStoreUtil} to access the image store persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ImageStoreImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ImageStoreModelImpl.ENTITY_CACHE_ENABLED,
			ImageStoreModelImpl.FINDER_CACHE_ENABLED, ImageStoreImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ImageStoreModelImpl.ENTITY_CACHE_ENABLED,
			ImageStoreModelImpl.FINDER_CACHE_ENABLED, ImageStoreImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ImageStoreModelImpl.ENTITY_CACHE_ENABLED,
			ImageStoreModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public ImageStorePersistenceImpl() {
		setModelClass(ImageStore.class);
	}

	/**
	 * Caches the image store in the entity cache if it is enabled.
	 *
	 * @param imageStore the image store
	 */
	@Override
	public void cacheResult(ImageStore imageStore) {
		EntityCacheUtil.putResult(ImageStoreModelImpl.ENTITY_CACHE_ENABLED,
			ImageStoreImpl.class, imageStore.getPrimaryKey(), imageStore);

		imageStore.resetOriginalValues();
	}

	/**
	 * Caches the image stores in the entity cache if it is enabled.
	 *
	 * @param imageStores the image stores
	 */
	@Override
	public void cacheResult(List<ImageStore> imageStores) {
		for (ImageStore imageStore : imageStores) {
			if (EntityCacheUtil.getResult(
						ImageStoreModelImpl.ENTITY_CACHE_ENABLED,
						ImageStoreImpl.class, imageStore.getPrimaryKey()) == null) {
				cacheResult(imageStore);
			}
			else {
				imageStore.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all image stores.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(ImageStoreImpl.class.getName());
		}

		EntityCacheUtil.clearCache(ImageStoreImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the image store.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ImageStore imageStore) {
		EntityCacheUtil.removeResult(ImageStoreModelImpl.ENTITY_CACHE_ENABLED,
			ImageStoreImpl.class, imageStore.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<ImageStore> imageStores) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (ImageStore imageStore : imageStores) {
			EntityCacheUtil.removeResult(ImageStoreModelImpl.ENTITY_CACHE_ENABLED,
				ImageStoreImpl.class, imageStore.getPrimaryKey());
		}
	}

	/**
	 * Creates a new image store with the primary key. Does not add the image store to the database.
	 *
	 * @param imageId the primary key for the new image store
	 * @return the new image store
	 */
	@Override
	public ImageStore create(long imageId) {
		ImageStore imageStore = new ImageStoreImpl();

		imageStore.setNew(true);
		imageStore.setPrimaryKey(imageId);

		return imageStore;
	}

	/**
	 * Removes the image store with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param imageId the primary key of the image store
	 * @return the image store that was removed
	 * @throws com.aw.internal.NoSuchImageStoreException if a image store with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ImageStore remove(long imageId)
		throws NoSuchImageStoreException, SystemException {
		return remove((Serializable)imageId);
	}

	/**
	 * Removes the image store with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the image store
	 * @return the image store that was removed
	 * @throws com.aw.internal.NoSuchImageStoreException if a image store with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ImageStore remove(Serializable primaryKey)
		throws NoSuchImageStoreException, SystemException {
		Session session = null;

		try {
			session = openSession();

			ImageStore imageStore = (ImageStore)session.get(ImageStoreImpl.class,
					primaryKey);

			if (imageStore == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchImageStoreException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(imageStore);
		}
		catch (NoSuchImageStoreException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ImageStore removeImpl(ImageStore imageStore)
		throws SystemException {
		imageStore = toUnwrappedModel(imageStore);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(imageStore)) {
				imageStore = (ImageStore)session.get(ImageStoreImpl.class,
						imageStore.getPrimaryKeyObj());
			}

			if (imageStore != null) {
				session.delete(imageStore);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (imageStore != null) {
			clearCache(imageStore);
		}

		return imageStore;
	}

	@Override
	public ImageStore updateImpl(com.aw.internal.model.ImageStore imageStore)
		throws SystemException {
		imageStore = toUnwrappedModel(imageStore);

		boolean isNew = imageStore.isNew();

		Session session = null;

		try {
			session = openSession();

			if (imageStore.isNew()) {
				session.save(imageStore);

				imageStore.setNew(false);
			}
			else {
				session.evict(imageStore);
				session.saveOrUpdate(imageStore);
			}

			session.flush();
			session.clear();
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(ImageStoreModelImpl.ENTITY_CACHE_ENABLED,
			ImageStoreImpl.class, imageStore.getPrimaryKey(), imageStore);

		imageStore.resetOriginalValues();

		return imageStore;
	}

	protected ImageStore toUnwrappedModel(ImageStore imageStore) {
		if (imageStore instanceof ImageStoreImpl) {
			return imageStore;
		}

		ImageStoreImpl imageStoreImpl = new ImageStoreImpl();

		imageStoreImpl.setNew(imageStore.isNew());
		imageStoreImpl.setPrimaryKey(imageStore.getPrimaryKey());

		imageStoreImpl.setImageId(imageStore.getImageId());
		imageStoreImpl.setContent(imageStore.getContent());
		imageStoreImpl.setActiveFlag(imageStore.getActiveFlag());
		imageStoreImpl.setAddDate(imageStore.getAddDate());

		return imageStoreImpl;
	}

	/**
	 * Returns the image store with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the image store
	 * @return the image store
	 * @throws com.aw.internal.NoSuchImageStoreException if a image store with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ImageStore findByPrimaryKey(Serializable primaryKey)
		throws NoSuchImageStoreException, SystemException {
		ImageStore imageStore = fetchByPrimaryKey(primaryKey);

		if (imageStore == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchImageStoreException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return imageStore;
	}

	/**
	 * Returns the image store with the primary key or throws a {@link com.aw.internal.NoSuchImageStoreException} if it could not be found.
	 *
	 * @param imageId the primary key of the image store
	 * @return the image store
	 * @throws com.aw.internal.NoSuchImageStoreException if a image store with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ImageStore findByPrimaryKey(long imageId)
		throws NoSuchImageStoreException, SystemException {
		return findByPrimaryKey((Serializable)imageId);
	}

	/**
	 * Returns the image store with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the image store
	 * @return the image store, or <code>null</code> if a image store with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ImageStore fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		ImageStore imageStore = (ImageStore)EntityCacheUtil.getResult(ImageStoreModelImpl.ENTITY_CACHE_ENABLED,
				ImageStoreImpl.class, primaryKey);

		if (imageStore == _nullImageStore) {
			return null;
		}

		if (imageStore == null) {
			Session session = null;

			try {
				session = openSession();

				imageStore = (ImageStore)session.get(ImageStoreImpl.class,
						primaryKey);

				if (imageStore != null) {
					cacheResult(imageStore);
				}
				else {
					EntityCacheUtil.putResult(ImageStoreModelImpl.ENTITY_CACHE_ENABLED,
						ImageStoreImpl.class, primaryKey, _nullImageStore);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(ImageStoreModelImpl.ENTITY_CACHE_ENABLED,
					ImageStoreImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return imageStore;
	}

	/**
	 * Returns the image store with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param imageId the primary key of the image store
	 * @return the image store, or <code>null</code> if a image store with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ImageStore fetchByPrimaryKey(long imageId) throws SystemException {
		return fetchByPrimaryKey((Serializable)imageId);
	}

	/**
	 * Returns all the image stores.
	 *
	 * @return the image stores
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ImageStore> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the image stores.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.ImageStoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of image stores
	 * @param end the upper bound of the range of image stores (not inclusive)
	 * @return the range of image stores
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ImageStore> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the image stores.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.ImageStoreModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of image stores
	 * @param end the upper bound of the range of image stores (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of image stores
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ImageStore> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<ImageStore> list = (List<ImageStore>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_IMAGESTORE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_IMAGESTORE;

				if (pagination) {
					sql = sql.concat(ImageStoreModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<ImageStore>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ImageStore>(list);
				}
				else {
					list = (List<ImageStore>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the image stores from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (ImageStore imageStore : findAll()) {
			remove(imageStore);
		}
	}

	/**
	 * Returns the number of image stores.
	 *
	 * @return the number of image stores
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_IMAGESTORE);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the image store persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.aw.internal.model.ImageStore")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<ImageStore>> listenersList = new ArrayList<ModelListener<ImageStore>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<ImageStore>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(ImageStoreImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_IMAGESTORE = "SELECT imageStore FROM ImageStore imageStore";
	private static final String _SQL_COUNT_IMAGESTORE = "SELECT COUNT(imageStore) FROM ImageStore imageStore";
	private static final String _ORDER_BY_ENTITY_ALIAS = "imageStore.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ImageStore exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(ImageStorePersistenceImpl.class);
	private static ImageStore _nullImageStore = new ImageStoreImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<ImageStore> toCacheModel() {
				return _nullImageStoreCacheModel;
			}
		};

	private static CacheModel<ImageStore> _nullImageStoreCacheModel = new CacheModel<ImageStore>() {
			@Override
			public ImageStore toEntityModel() {
				return _nullImageStore;
			}
		};
}