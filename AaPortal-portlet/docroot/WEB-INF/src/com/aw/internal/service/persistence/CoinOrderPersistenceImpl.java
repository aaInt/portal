/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.persistence;

import com.aw.internal.NoSuchCoinOrderException;
import com.aw.internal.model.CoinOrder;
import com.aw.internal.model.impl.CoinOrderImpl;
import com.aw.internal.model.impl.CoinOrderModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the coin order service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author zl
 * @see CoinOrderPersistence
 * @see CoinOrderUtil
 * @generated
 */
public class CoinOrderPersistenceImpl extends BasePersistenceImpl<CoinOrder>
	implements CoinOrderPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CoinOrderUtil} to access the coin order persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CoinOrderImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CoinOrderModelImpl.ENTITY_CACHE_ENABLED,
			CoinOrderModelImpl.FINDER_CACHE_ENABLED, CoinOrderImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CoinOrderModelImpl.ENTITY_CACHE_ENABLED,
			CoinOrderModelImpl.FINDER_CACHE_ENABLED, CoinOrderImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CoinOrderModelImpl.ENTITY_CACHE_ENABLED,
			CoinOrderModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PENDING = new FinderPath(CoinOrderModelImpl.ENTITY_CACHE_ENABLED,
			CoinOrderModelImpl.FINDER_CACHE_ENABLED, CoinOrderImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBypending",
			new String[] {
				String.class.getName(), String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PENDING =
		new FinderPath(CoinOrderModelImpl.ENTITY_CACHE_ENABLED,
			CoinOrderModelImpl.FINDER_CACHE_ENABLED, CoinOrderImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBypending",
			new String[] { String.class.getName(), String.class.getName() },
			CoinOrderModelImpl.ACTIVEFLAG_COLUMN_BITMASK |
			CoinOrderModelImpl.COMPLETEFLAG_COLUMN_BITMASK |
			CoinOrderModelImpl.ADDTIME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PENDING = new FinderPath(CoinOrderModelImpl.ENTITY_CACHE_ENABLED,
			CoinOrderModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBypending",
			new String[] { String.class.getName(), String.class.getName() });

	/**
	 * Returns all the coin orders where activeFlag = &#63; and completeFlag = &#63;.
	 *
	 * @param activeFlag the active flag
	 * @param completeFlag the complete flag
	 * @return the matching coin orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CoinOrder> findBypending(String activeFlag, String completeFlag)
		throws SystemException {
		return findBypending(activeFlag, completeFlag, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the coin orders where activeFlag = &#63; and completeFlag = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinOrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param activeFlag the active flag
	 * @param completeFlag the complete flag
	 * @param start the lower bound of the range of coin orders
	 * @param end the upper bound of the range of coin orders (not inclusive)
	 * @return the range of matching coin orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CoinOrder> findBypending(String activeFlag,
		String completeFlag, int start, int end) throws SystemException {
		return findBypending(activeFlag, completeFlag, start, end, null);
	}

	/**
	 * Returns an ordered range of all the coin orders where activeFlag = &#63; and completeFlag = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinOrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param activeFlag the active flag
	 * @param completeFlag the complete flag
	 * @param start the lower bound of the range of coin orders
	 * @param end the upper bound of the range of coin orders (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching coin orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CoinOrder> findBypending(String activeFlag,
		String completeFlag, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PENDING;
			finderArgs = new Object[] { activeFlag, completeFlag };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PENDING;
			finderArgs = new Object[] {
					activeFlag, completeFlag,
					
					start, end, orderByComparator
				};
		}

		List<CoinOrder> list = (List<CoinOrder>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CoinOrder coinOrder : list) {
				if (!Validator.equals(activeFlag, coinOrder.getActiveFlag()) ||
						!Validator.equals(completeFlag,
							coinOrder.getCompleteFlag())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_COINORDER_WHERE);

			boolean bindActiveFlag = false;

			if (activeFlag == null) {
				query.append(_FINDER_COLUMN_PENDING_ACTIVEFLAG_1);
			}
			else if (activeFlag.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_PENDING_ACTIVEFLAG_3);
			}
			else {
				bindActiveFlag = true;

				query.append(_FINDER_COLUMN_PENDING_ACTIVEFLAG_2);
			}

			boolean bindCompleteFlag = false;

			if (completeFlag == null) {
				query.append(_FINDER_COLUMN_PENDING_COMPLETEFLAG_1);
			}
			else if (completeFlag.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_PENDING_COMPLETEFLAG_3);
			}
			else {
				bindCompleteFlag = true;

				query.append(_FINDER_COLUMN_PENDING_COMPLETEFLAG_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CoinOrderModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindActiveFlag) {
					qPos.add(activeFlag);
				}

				if (bindCompleteFlag) {
					qPos.add(completeFlag);
				}

				if (!pagination) {
					list = (List<CoinOrder>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CoinOrder>(list);
				}
				else {
					list = (List<CoinOrder>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first coin order in the ordered set where activeFlag = &#63; and completeFlag = &#63;.
	 *
	 * @param activeFlag the active flag
	 * @param completeFlag the complete flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching coin order
	 * @throws com.aw.internal.NoSuchCoinOrderException if a matching coin order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinOrder findBypending_First(String activeFlag,
		String completeFlag, OrderByComparator orderByComparator)
		throws NoSuchCoinOrderException, SystemException {
		CoinOrder coinOrder = fetchBypending_First(activeFlag, completeFlag,
				orderByComparator);

		if (coinOrder != null) {
			return coinOrder;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("activeFlag=");
		msg.append(activeFlag);

		msg.append(", completeFlag=");
		msg.append(completeFlag);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCoinOrderException(msg.toString());
	}

	/**
	 * Returns the first coin order in the ordered set where activeFlag = &#63; and completeFlag = &#63;.
	 *
	 * @param activeFlag the active flag
	 * @param completeFlag the complete flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching coin order, or <code>null</code> if a matching coin order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinOrder fetchBypending_First(String activeFlag,
		String completeFlag, OrderByComparator orderByComparator)
		throws SystemException {
		List<CoinOrder> list = findBypending(activeFlag, completeFlag, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last coin order in the ordered set where activeFlag = &#63; and completeFlag = &#63;.
	 *
	 * @param activeFlag the active flag
	 * @param completeFlag the complete flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching coin order
	 * @throws com.aw.internal.NoSuchCoinOrderException if a matching coin order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinOrder findBypending_Last(String activeFlag, String completeFlag,
		OrderByComparator orderByComparator)
		throws NoSuchCoinOrderException, SystemException {
		CoinOrder coinOrder = fetchBypending_Last(activeFlag, completeFlag,
				orderByComparator);

		if (coinOrder != null) {
			return coinOrder;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("activeFlag=");
		msg.append(activeFlag);

		msg.append(", completeFlag=");
		msg.append(completeFlag);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCoinOrderException(msg.toString());
	}

	/**
	 * Returns the last coin order in the ordered set where activeFlag = &#63; and completeFlag = &#63;.
	 *
	 * @param activeFlag the active flag
	 * @param completeFlag the complete flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching coin order, or <code>null</code> if a matching coin order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinOrder fetchBypending_Last(String activeFlag,
		String completeFlag, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countBypending(activeFlag, completeFlag);

		if (count == 0) {
			return null;
		}

		List<CoinOrder> list = findBypending(activeFlag, completeFlag,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the coin orders before and after the current coin order in the ordered set where activeFlag = &#63; and completeFlag = &#63;.
	 *
	 * @param orderId the primary key of the current coin order
	 * @param activeFlag the active flag
	 * @param completeFlag the complete flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next coin order
	 * @throws com.aw.internal.NoSuchCoinOrderException if a coin order with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinOrder[] findBypending_PrevAndNext(String orderId,
		String activeFlag, String completeFlag,
		OrderByComparator orderByComparator)
		throws NoSuchCoinOrderException, SystemException {
		CoinOrder coinOrder = findByPrimaryKey(orderId);

		Session session = null;

		try {
			session = openSession();

			CoinOrder[] array = new CoinOrderImpl[3];

			array[0] = getBypending_PrevAndNext(session, coinOrder, activeFlag,
					completeFlag, orderByComparator, true);

			array[1] = coinOrder;

			array[2] = getBypending_PrevAndNext(session, coinOrder, activeFlag,
					completeFlag, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CoinOrder getBypending_PrevAndNext(Session session,
		CoinOrder coinOrder, String activeFlag, String completeFlag,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_COINORDER_WHERE);

		boolean bindActiveFlag = false;

		if (activeFlag == null) {
			query.append(_FINDER_COLUMN_PENDING_ACTIVEFLAG_1);
		}
		else if (activeFlag.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_PENDING_ACTIVEFLAG_3);
		}
		else {
			bindActiveFlag = true;

			query.append(_FINDER_COLUMN_PENDING_ACTIVEFLAG_2);
		}

		boolean bindCompleteFlag = false;

		if (completeFlag == null) {
			query.append(_FINDER_COLUMN_PENDING_COMPLETEFLAG_1);
		}
		else if (completeFlag.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_PENDING_COMPLETEFLAG_3);
		}
		else {
			bindCompleteFlag = true;

			query.append(_FINDER_COLUMN_PENDING_COMPLETEFLAG_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CoinOrderModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindActiveFlag) {
			qPos.add(activeFlag);
		}

		if (bindCompleteFlag) {
			qPos.add(completeFlag);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(coinOrder);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CoinOrder> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the coin orders where activeFlag = &#63; and completeFlag = &#63; from the database.
	 *
	 * @param activeFlag the active flag
	 * @param completeFlag the complete flag
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeBypending(String activeFlag, String completeFlag)
		throws SystemException {
		for (CoinOrder coinOrder : findBypending(activeFlag, completeFlag,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(coinOrder);
		}
	}

	/**
	 * Returns the number of coin orders where activeFlag = &#63; and completeFlag = &#63;.
	 *
	 * @param activeFlag the active flag
	 * @param completeFlag the complete flag
	 * @return the number of matching coin orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countBypending(String activeFlag, String completeFlag)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PENDING;

		Object[] finderArgs = new Object[] { activeFlag, completeFlag };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_COINORDER_WHERE);

			boolean bindActiveFlag = false;

			if (activeFlag == null) {
				query.append(_FINDER_COLUMN_PENDING_ACTIVEFLAG_1);
			}
			else if (activeFlag.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_PENDING_ACTIVEFLAG_3);
			}
			else {
				bindActiveFlag = true;

				query.append(_FINDER_COLUMN_PENDING_ACTIVEFLAG_2);
			}

			boolean bindCompleteFlag = false;

			if (completeFlag == null) {
				query.append(_FINDER_COLUMN_PENDING_COMPLETEFLAG_1);
			}
			else if (completeFlag.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_PENDING_COMPLETEFLAG_3);
			}
			else {
				bindCompleteFlag = true;

				query.append(_FINDER_COLUMN_PENDING_COMPLETEFLAG_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindActiveFlag) {
					qPos.add(activeFlag);
				}

				if (bindCompleteFlag) {
					qPos.add(completeFlag);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PENDING_ACTIVEFLAG_1 = "coinOrder.activeFlag IS NULL AND ";
	private static final String _FINDER_COLUMN_PENDING_ACTIVEFLAG_2 = "coinOrder.activeFlag = ? AND ";
	private static final String _FINDER_COLUMN_PENDING_ACTIVEFLAG_3 = "(coinOrder.activeFlag IS NULL OR coinOrder.activeFlag = '') AND ";
	private static final String _FINDER_COLUMN_PENDING_COMPLETEFLAG_1 = "coinOrder.completeFlag IS NULL";
	private static final String _FINDER_COLUMN_PENDING_COMPLETEFLAG_2 = "coinOrder.completeFlag = ?";
	private static final String _FINDER_COLUMN_PENDING_COMPLETEFLAG_3 = "(coinOrder.completeFlag IS NULL OR coinOrder.completeFlag = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPLETE = new FinderPath(CoinOrderModelImpl.ENTITY_CACHE_ENABLED,
			CoinOrderModelImpl.FINDER_CACHE_ENABLED, CoinOrderImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBycomplete",
			new String[] {
				String.class.getName(), String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPLETE =
		new FinderPath(CoinOrderModelImpl.ENTITY_CACHE_ENABLED,
			CoinOrderModelImpl.FINDER_CACHE_ENABLED, CoinOrderImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBycomplete",
			new String[] { String.class.getName(), String.class.getName() },
			CoinOrderModelImpl.ACTIVEFLAG_COLUMN_BITMASK |
			CoinOrderModelImpl.COMPLETEFLAG_COLUMN_BITMASK |
			CoinOrderModelImpl.ADDTIME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COMPLETE = new FinderPath(CoinOrderModelImpl.ENTITY_CACHE_ENABLED,
			CoinOrderModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBycomplete",
			new String[] { String.class.getName(), String.class.getName() });

	/**
	 * Returns all the coin orders where activeFlag = &#63; and completeFlag = &#63;.
	 *
	 * @param activeFlag the active flag
	 * @param completeFlag the complete flag
	 * @return the matching coin orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CoinOrder> findBycomplete(String activeFlag, String completeFlag)
		throws SystemException {
		return findBycomplete(activeFlag, completeFlag, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the coin orders where activeFlag = &#63; and completeFlag = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinOrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param activeFlag the active flag
	 * @param completeFlag the complete flag
	 * @param start the lower bound of the range of coin orders
	 * @param end the upper bound of the range of coin orders (not inclusive)
	 * @return the range of matching coin orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CoinOrder> findBycomplete(String activeFlag,
		String completeFlag, int start, int end) throws SystemException {
		return findBycomplete(activeFlag, completeFlag, start, end, null);
	}

	/**
	 * Returns an ordered range of all the coin orders where activeFlag = &#63; and completeFlag = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinOrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param activeFlag the active flag
	 * @param completeFlag the complete flag
	 * @param start the lower bound of the range of coin orders
	 * @param end the upper bound of the range of coin orders (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching coin orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CoinOrder> findBycomplete(String activeFlag,
		String completeFlag, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPLETE;
			finderArgs = new Object[] { activeFlag, completeFlag };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPLETE;
			finderArgs = new Object[] {
					activeFlag, completeFlag,
					
					start, end, orderByComparator
				};
		}

		List<CoinOrder> list = (List<CoinOrder>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CoinOrder coinOrder : list) {
				if (!Validator.equals(activeFlag, coinOrder.getActiveFlag()) ||
						!Validator.equals(completeFlag,
							coinOrder.getCompleteFlag())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_COINORDER_WHERE);

			boolean bindActiveFlag = false;

			if (activeFlag == null) {
				query.append(_FINDER_COLUMN_COMPLETE_ACTIVEFLAG_1);
			}
			else if (activeFlag.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_COMPLETE_ACTIVEFLAG_3);
			}
			else {
				bindActiveFlag = true;

				query.append(_FINDER_COLUMN_COMPLETE_ACTIVEFLAG_2);
			}

			boolean bindCompleteFlag = false;

			if (completeFlag == null) {
				query.append(_FINDER_COLUMN_COMPLETE_COMPLETEFLAG_1);
			}
			else if (completeFlag.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_COMPLETE_COMPLETEFLAG_3);
			}
			else {
				bindCompleteFlag = true;

				query.append(_FINDER_COLUMN_COMPLETE_COMPLETEFLAG_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CoinOrderModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindActiveFlag) {
					qPos.add(activeFlag);
				}

				if (bindCompleteFlag) {
					qPos.add(completeFlag);
				}

				if (!pagination) {
					list = (List<CoinOrder>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CoinOrder>(list);
				}
				else {
					list = (List<CoinOrder>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first coin order in the ordered set where activeFlag = &#63; and completeFlag = &#63;.
	 *
	 * @param activeFlag the active flag
	 * @param completeFlag the complete flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching coin order
	 * @throws com.aw.internal.NoSuchCoinOrderException if a matching coin order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinOrder findBycomplete_First(String activeFlag,
		String completeFlag, OrderByComparator orderByComparator)
		throws NoSuchCoinOrderException, SystemException {
		CoinOrder coinOrder = fetchBycomplete_First(activeFlag, completeFlag,
				orderByComparator);

		if (coinOrder != null) {
			return coinOrder;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("activeFlag=");
		msg.append(activeFlag);

		msg.append(", completeFlag=");
		msg.append(completeFlag);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCoinOrderException(msg.toString());
	}

	/**
	 * Returns the first coin order in the ordered set where activeFlag = &#63; and completeFlag = &#63;.
	 *
	 * @param activeFlag the active flag
	 * @param completeFlag the complete flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching coin order, or <code>null</code> if a matching coin order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinOrder fetchBycomplete_First(String activeFlag,
		String completeFlag, OrderByComparator orderByComparator)
		throws SystemException {
		List<CoinOrder> list = findBycomplete(activeFlag, completeFlag, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last coin order in the ordered set where activeFlag = &#63; and completeFlag = &#63;.
	 *
	 * @param activeFlag the active flag
	 * @param completeFlag the complete flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching coin order
	 * @throws com.aw.internal.NoSuchCoinOrderException if a matching coin order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinOrder findBycomplete_Last(String activeFlag,
		String completeFlag, OrderByComparator orderByComparator)
		throws NoSuchCoinOrderException, SystemException {
		CoinOrder coinOrder = fetchBycomplete_Last(activeFlag, completeFlag,
				orderByComparator);

		if (coinOrder != null) {
			return coinOrder;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("activeFlag=");
		msg.append(activeFlag);

		msg.append(", completeFlag=");
		msg.append(completeFlag);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCoinOrderException(msg.toString());
	}

	/**
	 * Returns the last coin order in the ordered set where activeFlag = &#63; and completeFlag = &#63;.
	 *
	 * @param activeFlag the active flag
	 * @param completeFlag the complete flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching coin order, or <code>null</code> if a matching coin order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinOrder fetchBycomplete_Last(String activeFlag,
		String completeFlag, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countBycomplete(activeFlag, completeFlag);

		if (count == 0) {
			return null;
		}

		List<CoinOrder> list = findBycomplete(activeFlag, completeFlag,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the coin orders before and after the current coin order in the ordered set where activeFlag = &#63; and completeFlag = &#63;.
	 *
	 * @param orderId the primary key of the current coin order
	 * @param activeFlag the active flag
	 * @param completeFlag the complete flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next coin order
	 * @throws com.aw.internal.NoSuchCoinOrderException if a coin order with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinOrder[] findBycomplete_PrevAndNext(String orderId,
		String activeFlag, String completeFlag,
		OrderByComparator orderByComparator)
		throws NoSuchCoinOrderException, SystemException {
		CoinOrder coinOrder = findByPrimaryKey(orderId);

		Session session = null;

		try {
			session = openSession();

			CoinOrder[] array = new CoinOrderImpl[3];

			array[0] = getBycomplete_PrevAndNext(session, coinOrder,
					activeFlag, completeFlag, orderByComparator, true);

			array[1] = coinOrder;

			array[2] = getBycomplete_PrevAndNext(session, coinOrder,
					activeFlag, completeFlag, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CoinOrder getBycomplete_PrevAndNext(Session session,
		CoinOrder coinOrder, String activeFlag, String completeFlag,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_COINORDER_WHERE);

		boolean bindActiveFlag = false;

		if (activeFlag == null) {
			query.append(_FINDER_COLUMN_COMPLETE_ACTIVEFLAG_1);
		}
		else if (activeFlag.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_COMPLETE_ACTIVEFLAG_3);
		}
		else {
			bindActiveFlag = true;

			query.append(_FINDER_COLUMN_COMPLETE_ACTIVEFLAG_2);
		}

		boolean bindCompleteFlag = false;

		if (completeFlag == null) {
			query.append(_FINDER_COLUMN_COMPLETE_COMPLETEFLAG_1);
		}
		else if (completeFlag.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_COMPLETE_COMPLETEFLAG_3);
		}
		else {
			bindCompleteFlag = true;

			query.append(_FINDER_COLUMN_COMPLETE_COMPLETEFLAG_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CoinOrderModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindActiveFlag) {
			qPos.add(activeFlag);
		}

		if (bindCompleteFlag) {
			qPos.add(completeFlag);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(coinOrder);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CoinOrder> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the coin orders where activeFlag = &#63; and completeFlag = &#63; from the database.
	 *
	 * @param activeFlag the active flag
	 * @param completeFlag the complete flag
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeBycomplete(String activeFlag, String completeFlag)
		throws SystemException {
		for (CoinOrder coinOrder : findBycomplete(activeFlag, completeFlag,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(coinOrder);
		}
	}

	/**
	 * Returns the number of coin orders where activeFlag = &#63; and completeFlag = &#63;.
	 *
	 * @param activeFlag the active flag
	 * @param completeFlag the complete flag
	 * @return the number of matching coin orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countBycomplete(String activeFlag, String completeFlag)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_COMPLETE;

		Object[] finderArgs = new Object[] { activeFlag, completeFlag };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_COINORDER_WHERE);

			boolean bindActiveFlag = false;

			if (activeFlag == null) {
				query.append(_FINDER_COLUMN_COMPLETE_ACTIVEFLAG_1);
			}
			else if (activeFlag.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_COMPLETE_ACTIVEFLAG_3);
			}
			else {
				bindActiveFlag = true;

				query.append(_FINDER_COLUMN_COMPLETE_ACTIVEFLAG_2);
			}

			boolean bindCompleteFlag = false;

			if (completeFlag == null) {
				query.append(_FINDER_COLUMN_COMPLETE_COMPLETEFLAG_1);
			}
			else if (completeFlag.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_COMPLETE_COMPLETEFLAG_3);
			}
			else {
				bindCompleteFlag = true;

				query.append(_FINDER_COLUMN_COMPLETE_COMPLETEFLAG_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindActiveFlag) {
					qPos.add(activeFlag);
				}

				if (bindCompleteFlag) {
					qPos.add(completeFlag);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_COMPLETE_ACTIVEFLAG_1 = "coinOrder.activeFlag IS NULL AND ";
	private static final String _FINDER_COLUMN_COMPLETE_ACTIVEFLAG_2 = "coinOrder.activeFlag = ? AND ";
	private static final String _FINDER_COLUMN_COMPLETE_ACTIVEFLAG_3 = "(coinOrder.activeFlag IS NULL OR coinOrder.activeFlag = '') AND ";
	private static final String _FINDER_COLUMN_COMPLETE_COMPLETEFLAG_1 = "coinOrder.completeFlag IS NULL";
	private static final String _FINDER_COLUMN_COMPLETE_COMPLETEFLAG_2 = "coinOrder.completeFlag = ?";
	private static final String _FINDER_COLUMN_COMPLETE_COMPLETEFLAG_3 = "(coinOrder.completeFlag IS NULL OR coinOrder.completeFlag = '')";

	public CoinOrderPersistenceImpl() {
		setModelClass(CoinOrder.class);
	}

	/**
	 * Caches the coin order in the entity cache if it is enabled.
	 *
	 * @param coinOrder the coin order
	 */
	@Override
	public void cacheResult(CoinOrder coinOrder) {
		EntityCacheUtil.putResult(CoinOrderModelImpl.ENTITY_CACHE_ENABLED,
			CoinOrderImpl.class, coinOrder.getPrimaryKey(), coinOrder);

		coinOrder.resetOriginalValues();
	}

	/**
	 * Caches the coin orders in the entity cache if it is enabled.
	 *
	 * @param coinOrders the coin orders
	 */
	@Override
	public void cacheResult(List<CoinOrder> coinOrders) {
		for (CoinOrder coinOrder : coinOrders) {
			if (EntityCacheUtil.getResult(
						CoinOrderModelImpl.ENTITY_CACHE_ENABLED,
						CoinOrderImpl.class, coinOrder.getPrimaryKey()) == null) {
				cacheResult(coinOrder);
			}
			else {
				coinOrder.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all coin orders.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(CoinOrderImpl.class.getName());
		}

		EntityCacheUtil.clearCache(CoinOrderImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the coin order.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(CoinOrder coinOrder) {
		EntityCacheUtil.removeResult(CoinOrderModelImpl.ENTITY_CACHE_ENABLED,
			CoinOrderImpl.class, coinOrder.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<CoinOrder> coinOrders) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (CoinOrder coinOrder : coinOrders) {
			EntityCacheUtil.removeResult(CoinOrderModelImpl.ENTITY_CACHE_ENABLED,
				CoinOrderImpl.class, coinOrder.getPrimaryKey());
		}
	}

	/**
	 * Creates a new coin order with the primary key. Does not add the coin order to the database.
	 *
	 * @param orderId the primary key for the new coin order
	 * @return the new coin order
	 */
	@Override
	public CoinOrder create(String orderId) {
		CoinOrder coinOrder = new CoinOrderImpl();

		coinOrder.setNew(true);
		coinOrder.setPrimaryKey(orderId);

		return coinOrder;
	}

	/**
	 * Removes the coin order with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param orderId the primary key of the coin order
	 * @return the coin order that was removed
	 * @throws com.aw.internal.NoSuchCoinOrderException if a coin order with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinOrder remove(String orderId)
		throws NoSuchCoinOrderException, SystemException {
		return remove((Serializable)orderId);
	}

	/**
	 * Removes the coin order with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the coin order
	 * @return the coin order that was removed
	 * @throws com.aw.internal.NoSuchCoinOrderException if a coin order with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinOrder remove(Serializable primaryKey)
		throws NoSuchCoinOrderException, SystemException {
		Session session = null;

		try {
			session = openSession();

			CoinOrder coinOrder = (CoinOrder)session.get(CoinOrderImpl.class,
					primaryKey);

			if (coinOrder == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCoinOrderException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(coinOrder);
		}
		catch (NoSuchCoinOrderException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected CoinOrder removeImpl(CoinOrder coinOrder)
		throws SystemException {
		coinOrder = toUnwrappedModel(coinOrder);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(coinOrder)) {
				coinOrder = (CoinOrder)session.get(CoinOrderImpl.class,
						coinOrder.getPrimaryKeyObj());
			}

			if (coinOrder != null) {
				session.delete(coinOrder);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (coinOrder != null) {
			clearCache(coinOrder);
		}

		return coinOrder;
	}

	@Override
	public CoinOrder updateImpl(com.aw.internal.model.CoinOrder coinOrder)
		throws SystemException {
		coinOrder = toUnwrappedModel(coinOrder);

		boolean isNew = coinOrder.isNew();

		CoinOrderModelImpl coinOrderModelImpl = (CoinOrderModelImpl)coinOrder;

		Session session = null;

		try {
			session = openSession();

			if (coinOrder.isNew()) {
				session.save(coinOrder);

				coinOrder.setNew(false);
			}
			else {
				session.merge(coinOrder);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !CoinOrderModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((coinOrderModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PENDING.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						coinOrderModelImpl.getOriginalActiveFlag(),
						coinOrderModelImpl.getOriginalCompleteFlag()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PENDING, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PENDING,
					args);

				args = new Object[] {
						coinOrderModelImpl.getActiveFlag(),
						coinOrderModelImpl.getCompleteFlag()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PENDING, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PENDING,
					args);
			}

			if ((coinOrderModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPLETE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						coinOrderModelImpl.getOriginalActiveFlag(),
						coinOrderModelImpl.getOriginalCompleteFlag()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPLETE, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPLETE,
					args);

				args = new Object[] {
						coinOrderModelImpl.getActiveFlag(),
						coinOrderModelImpl.getCompleteFlag()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPLETE, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPLETE,
					args);
			}
		}

		EntityCacheUtil.putResult(CoinOrderModelImpl.ENTITY_CACHE_ENABLED,
			CoinOrderImpl.class, coinOrder.getPrimaryKey(), coinOrder);

		return coinOrder;
	}

	protected CoinOrder toUnwrappedModel(CoinOrder coinOrder) {
		if (coinOrder instanceof CoinOrderImpl) {
			return coinOrder;
		}

		CoinOrderImpl coinOrderImpl = new CoinOrderImpl();

		coinOrderImpl.setNew(coinOrder.isNew());
		coinOrderImpl.setPrimaryKey(coinOrder.getPrimaryKey());

		coinOrderImpl.setOrderId(coinOrder.getOrderId());
		coinOrderImpl.setUserId(coinOrder.getUserId());
		coinOrderImpl.setSaleRegion(coinOrder.getSaleRegion());
		coinOrderImpl.setBrokerId(coinOrder.getBrokerId());
		coinOrderImpl.setSalesId(coinOrder.getSalesId());
		coinOrderImpl.setActiveFlag(coinOrder.getActiveFlag());
		coinOrderImpl.setSalePhase(coinOrder.getSalePhase());
		coinOrderImpl.setUnitPrice(coinOrder.getUnitPrice());
		coinOrderImpl.setTotalPrice(coinOrder.getTotalPrice());
		coinOrderImpl.setCurrency(coinOrder.getCurrency());
		coinOrderImpl.setSaleAmount(coinOrder.getSaleAmount());
		coinOrderImpl.setPromoAmount(coinOrder.getPromoAmount());
		coinOrderImpl.setTotalAmount(coinOrder.getTotalAmount());
		coinOrderImpl.setRsnCd(coinOrder.getRsnCd());
		coinOrderImpl.setVerifyFlag(coinOrder.getVerifyFlag());
		coinOrderImpl.setDocSentFlag(coinOrder.getDocSentFlag());
		coinOrderImpl.setDocCarrier(coinOrder.getDocCarrier());
		coinOrderImpl.setDocTrackingNo(coinOrder.getDocTrackingNo());
		coinOrderImpl.setDocSignFlag(coinOrder.getDocSignFlag());
		coinOrderImpl.setPaidFlag(coinOrder.getPaidFlag());
		coinOrderImpl.setApprovalFlag(coinOrder.getApprovalFlag());
		coinOrderImpl.setCertSentFlag(coinOrder.getCertSentFlag());
		coinOrderImpl.setCertCarrier(coinOrder.getCertCarrier());
		coinOrderImpl.setCertTrackingNo(coinOrder.getCertTrackingNo());
		coinOrderImpl.setCompleteFlag(coinOrder.getCompleteFlag());
		coinOrderImpl.setCaseCloser(coinOrder.getCaseCloser());
		coinOrderImpl.setAddTime(coinOrder.getAddTime());

		return coinOrderImpl;
	}

	/**
	 * Returns the coin order with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the coin order
	 * @return the coin order
	 * @throws com.aw.internal.NoSuchCoinOrderException if a coin order with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinOrder findByPrimaryKey(Serializable primaryKey)
		throws NoSuchCoinOrderException, SystemException {
		CoinOrder coinOrder = fetchByPrimaryKey(primaryKey);

		if (coinOrder == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchCoinOrderException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return coinOrder;
	}

	/**
	 * Returns the coin order with the primary key or throws a {@link com.aw.internal.NoSuchCoinOrderException} if it could not be found.
	 *
	 * @param orderId the primary key of the coin order
	 * @return the coin order
	 * @throws com.aw.internal.NoSuchCoinOrderException if a coin order with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinOrder findByPrimaryKey(String orderId)
		throws NoSuchCoinOrderException, SystemException {
		return findByPrimaryKey((Serializable)orderId);
	}

	/**
	 * Returns the coin order with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the coin order
	 * @return the coin order, or <code>null</code> if a coin order with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinOrder fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		CoinOrder coinOrder = (CoinOrder)EntityCacheUtil.getResult(CoinOrderModelImpl.ENTITY_CACHE_ENABLED,
				CoinOrderImpl.class, primaryKey);

		if (coinOrder == _nullCoinOrder) {
			return null;
		}

		if (coinOrder == null) {
			Session session = null;

			try {
				session = openSession();

				coinOrder = (CoinOrder)session.get(CoinOrderImpl.class,
						primaryKey);

				if (coinOrder != null) {
					cacheResult(coinOrder);
				}
				else {
					EntityCacheUtil.putResult(CoinOrderModelImpl.ENTITY_CACHE_ENABLED,
						CoinOrderImpl.class, primaryKey, _nullCoinOrder);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(CoinOrderModelImpl.ENTITY_CACHE_ENABLED,
					CoinOrderImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return coinOrder;
	}

	/**
	 * Returns the coin order with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param orderId the primary key of the coin order
	 * @return the coin order, or <code>null</code> if a coin order with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinOrder fetchByPrimaryKey(String orderId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)orderId);
	}

	/**
	 * Returns all the coin orders.
	 *
	 * @return the coin orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CoinOrder> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the coin orders.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinOrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of coin orders
	 * @param end the upper bound of the range of coin orders (not inclusive)
	 * @return the range of coin orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CoinOrder> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the coin orders.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinOrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of coin orders
	 * @param end the upper bound of the range of coin orders (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of coin orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CoinOrder> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<CoinOrder> list = (List<CoinOrder>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_COINORDER);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_COINORDER;

				if (pagination) {
					sql = sql.concat(CoinOrderModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<CoinOrder>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CoinOrder>(list);
				}
				else {
					list = (List<CoinOrder>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the coin orders from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (CoinOrder coinOrder : findAll()) {
			remove(coinOrder);
		}
	}

	/**
	 * Returns the number of coin orders.
	 *
	 * @return the number of coin orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_COINORDER);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the coin order persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.aw.internal.model.CoinOrder")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<CoinOrder>> listenersList = new ArrayList<ModelListener<CoinOrder>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<CoinOrder>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(CoinOrderImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_COINORDER = "SELECT coinOrder FROM CoinOrder coinOrder";
	private static final String _SQL_SELECT_COINORDER_WHERE = "SELECT coinOrder FROM CoinOrder coinOrder WHERE ";
	private static final String _SQL_COUNT_COINORDER = "SELECT COUNT(coinOrder) FROM CoinOrder coinOrder";
	private static final String _SQL_COUNT_COINORDER_WHERE = "SELECT COUNT(coinOrder) FROM CoinOrder coinOrder WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "coinOrder.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No CoinOrder exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No CoinOrder exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(CoinOrderPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"currency"
			});
	private static CoinOrder _nullCoinOrder = new CoinOrderImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<CoinOrder> toCacheModel() {
				return _nullCoinOrderCacheModel;
			}
		};

	private static CacheModel<CoinOrder> _nullCoinOrderCacheModel = new CacheModel<CoinOrder>() {
			@Override
			public CoinOrder toEntityModel() {
				return _nullCoinOrder;
			}
		};
}