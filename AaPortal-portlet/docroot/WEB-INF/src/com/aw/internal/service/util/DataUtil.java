package com.aw.internal.service.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import com.aw.internal.model.AaContact;
import com.aw.internal.model.BrokerInfo;
import com.aw.internal.model.CoinCount;
import com.aw.internal.model.CoinOrder;
import com.aw.internal.model.CoinUser;
import com.aw.internal.model.FailReason;
import com.aw.internal.model.FundInfo;
import com.aw.internal.model.FutureUser;
import com.aw.internal.model.UnitPrice;
import com.aw.internal.service.AaContactLocalServiceUtil;
import com.aw.internal.service.BrokerInfoLocalServiceUtil;
import com.aw.internal.service.persistence.AaContactUtil;
import com.aw.internal.service.persistence.CoinUserUtil;
import com.aw.internal.service.persistence.FailReasonUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.model.User;
import com.liferay.portal.service.persistence.UserUtil;

public class DataUtil {
	
	public static final String STATUS = "status";
	public static final String STATUS_CODE = "code";
	public static final String DATA = "data";
	
	public static JSONArray getUserJsonArray(List<CoinUser> list){
		
		JSONArray array = JSONFactoryUtil.createJSONArray();
		if(list != null && !list.isEmpty()){
			
			for(CoinUser cu: list){
				array.put(getUserJson(cu));
			}
			
		}
		return array;
	}
	
	public static JSONObject getUnitPrice(UnitPrice up){
		
		if(up != null){
			JSONObject j = JSONFactoryUtil.createJSONObject();
			j.put("phase", up.getSalePhase());
			j.put("unitPrice", up.getPrice());

			return j;
		}
		return null;
		
	}
	
	public static JSONObject getCertInfoJson(CoinUser cu, CoinOrder co){
		
		
		JSONObject j = JSONFactoryUtil.createJSONObject();
		j.put("lastName", cu.getLastName());
		j.put("birthday", cu.getBirthday());
		j.put("purchaseAmount", co.getSaleAmount());
		j.put("freeAmount", co.getPromoAmount());
		j.put("salesId", co.getSalesId());
		j.put("saleRegion", co.getSaleRegion());

		return j;
	
	}
	
	public static JSONObject getUserJson(CoinUser cu){
				
		if(cu != null){
			JSONObject j = JSONFactoryUtil.createJSONObject();
			j.put(EntityConstants.coinUser.userId.toString(), cu.getUserId());
			j.put(EntityConstants.coinUser.firstName.toString(), cu.getFirstName());
			j.put(EntityConstants.coinUser.lastName.toString(), cu.getLastName());
			j.put(EntityConstants.coinUser.activeFlag.toString(), cu.getActiveFlag());
			j.put(EntityConstants.coinUser.birthday.toString(), cu.getBirthday());
			
			AaContact ac = null;
			try {
				ac = AaContactLocalServiceUtil.getAaContact(cu.getContactId());
			} catch (PortalException e) {
				e.printStackTrace();
			} catch (SystemException e) {
				e.printStackTrace();
			}
			
			j.put(EntityConstants.coinUser.contact.toString(), getContactJson(ac));
			
			j.put(EntityConstants.coinUser.userType.toString(), cu.getUserType());
			j.put(EntityConstants.coinUser.brokerId.toString(), cu.getBrokerId());
			
			BrokerInfo bi = null;
			try {
				bi = BrokerInfoLocalServiceUtil.fetchBrokerInfo(cu.getBrokerId());
				if(bi != null){
					j.put(EntityConstants.BrokerInfo.licenseId.toString(), bi.getLicenseId());
					j.put(EntityConstants.BrokerInfo.licenseCountry.toString(), bi.getLicenseCountry());
				}else{
					j.put(EntityConstants.BrokerInfo.licenseId.toString(), "-1");
					j.put(EntityConstants.BrokerInfo.licenseCountry.toString(), "");
				}
			}catch (SystemException e) {
				e.printStackTrace();
			}
			
			j.put(EntityConstants.coinUser.fundId.toString(), cu.getFundId());
			j.put(EntityConstants.coinUser.nationality.toString(), cu.getNationality());
			j.put(EntityConstants.coinUser.idIssueCountry.toString(), cu.getIdIssueCountry());
			j.put(EntityConstants.coinUser.idType.toString(), cu.getIdType());
			j.put(EntityConstants.coinUser.idNumber.toString(), cu.getIdNumber());
			j.put(EntityConstants.coinUser.photoImage.toString(), cu.getPhotoImage());
			
			if(cu.getSsn() != null && !cu.getSsn().isEmpty()){
				j.put(EntityConstants.coinUser.ssn.toString(), EncryptionUtil.decrypt(cu.getSsn()));
			}else{
				j.put(EntityConstants.coinUser.ssn.toString(), "");
			}
			
			if(cu.getEin() != null && !cu.getEin().isEmpty()){
				j.put(EntityConstants.coinUser.ein.toString(), EncryptionUtil.decrypt(cu.getEin()));
			}else{
				j.put(EntityConstants.coinUser.ein.toString(), "");
			}
			
			j.put(EntityConstants.coinUser.income.toString(), cu.getIncome());
			j.put(EntityConstants.coinUser.asset.toString(), cu.getAsset());
			j.put(EntityConstants.coinUser.isValidated.toString(), cu.getIsValidated());
			j.put(EntityConstants.coinUser.isCreddited.toString(), cu.getIsCreddited());
			j.put(EntityConstants.coinUser.addUser.toString(), cu.getAddUser());
			return j;
		}
		return null;
		
	}
	
	//TODO
	public static JSONObject getCoinOrderJson(CoinOrder co){
		
		if(co != null && co.getUserId().contains("U")){//not null and is regular user
			JSONObject j = JSONFactoryUtil.createJSONObject();
			j.put(EntityConstants.CoinOrder.orderId.toString(), co.getOrderId());
			j.put(EntityConstants.CoinOrder.userId.toString(), co.getUserId());
			
			addBasicUserInfo(co.getUserId(), j);
			
			j.put(EntityConstants.CoinOrder.brokerId.toString(), co.getBrokerId());
			j.put(EntityConstants.CoinOrder.salesId.toString(), co.getSalesId());
			j.put(EntityConstants.CoinOrder.activeFlag.toString(), co.getActiveFlag());
			j.put(EntityConstants.CoinOrder.salePhase.toString(), co.getSalePhase());
			j.put(EntityConstants.CoinOrder.unitPrice.toString(), co.getUnitPrice());
			j.put(EntityConstants.CoinOrder.totalPrice.toString(), co.getTotalPrice());
			j.put(EntityConstants.CoinOrder.currency.toString(), co.getCurrency());
			j.put(EntityConstants.CoinOrder.saleAmount.toString(), co.getSaleAmount());
			j.put(EntityConstants.CoinOrder.promoAmount.toString(), co.getPromoAmount());
			j.put(EntityConstants.CoinOrder.totalAmount.toString(), co.getTotalAmount());
			
			String reason = "";
			try{
				reason = FailReasonUtil.fetchByPrimaryKey(co.getRsnCd()).getDesc();
			}catch(Exception e){
				//do nothing
			}
			
			j.put(EntityConstants.CoinOrder.rsnCd.toString(), reason);
			j.put(EntityConstants.CoinOrder.verifyFlag.toString(), co.getVerifyFlag());
			j.put(EntityConstants.CoinOrder.docSentFlag.toString(), co.getDocSentFlag());
			j.put(EntityConstants.CoinOrder.docCarrier.toString(), co.getDocCarrier());
			j.put(EntityConstants.CoinOrder.docTrackingNo.toString(), co.getDocTrackingNo());
			j.put(EntityConstants.CoinOrder.paidFlag.toString(), co.getPaidFlag());
			j.put(EntityConstants.CoinOrder.docSignFlag.toString(), co.getDocSignFlag());
			j.put(EntityConstants.CoinOrder.approvalFlag.toString(), co.getApprovalFlag());
			j.put(EntityConstants.CoinOrder.certSentFlag.toString(), co.getCertSentFlag());
			j.put(EntityConstants.CoinOrder.certCarrier.toString(), co.getCertCarrier());
			j.put(EntityConstants.CoinOrder.certTrackingNo.toString(), co.getCertTrackingNo());
			j.put(EntityConstants.CoinOrder.completeFlag.toString(), co.getCompleteFlag());
			j.put(EntityConstants.CoinOrder.caseCloser.toString(), co.getCaseCloser());
			
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy");			
		    String date = df.format(co.getAddTime());
		    
			j.put(EntityConstants.CoinOrder.addTime.toString(), date);

			return j;
		}
		return null;
		
	}
	
	public static JSONObject getStaffCoinOrderJson(CoinOrder co){
		
		if(co != null && !co.getUserId().contains("U")){//not null and is staff user
			JSONObject j = JSONFactoryUtil.createJSONObject();
			j.put(EntityConstants.CoinOrder.orderId.toString(), co.getOrderId());
			j.put(EntityConstants.CoinOrder.userId.toString(), co.getUserId());
			
			addBasicUserInfo(co.getUserId(), j);
			
			j.put(EntityConstants.CoinOrder.brokerId.toString(), co.getBrokerId());
			j.put(EntityConstants.CoinOrder.salesId.toString(), co.getSalesId());
			j.put(EntityConstants.CoinOrder.activeFlag.toString(), co.getActiveFlag());
			j.put(EntityConstants.CoinOrder.salePhase.toString(), co.getSalePhase());
			j.put(EntityConstants.CoinOrder.unitPrice.toString(), co.getUnitPrice());
			j.put(EntityConstants.CoinOrder.totalPrice.toString(), co.getTotalPrice());
			j.put(EntityConstants.CoinOrder.currency.toString(), co.getCurrency());
			j.put(EntityConstants.CoinOrder.saleAmount.toString(), co.getSaleAmount());
			j.put(EntityConstants.CoinOrder.promoAmount.toString(), co.getPromoAmount());
			j.put(EntityConstants.CoinOrder.totalAmount.toString(), co.getTotalAmount());
			
			String reason = "";
			try{
				reason = FailReasonUtil.fetchByPrimaryKey(co.getRsnCd()).getDesc();
			}catch(Exception e){
				//do nothing
			}
			
			j.put(EntityConstants.CoinOrder.rsnCd.toString(), reason);
			j.put(EntityConstants.CoinOrder.verifyFlag.toString(), co.getVerifyFlag());
			j.put(EntityConstants.CoinOrder.docSentFlag.toString(), co.getDocSentFlag());
			j.put(EntityConstants.CoinOrder.docCarrier.toString(), co.getDocCarrier());
			j.put(EntityConstants.CoinOrder.docTrackingNo.toString(), co.getDocTrackingNo());
			j.put(EntityConstants.CoinOrder.paidFlag.toString(), co.getPaidFlag());
			j.put(EntityConstants.CoinOrder.docSignFlag.toString(), co.getDocSignFlag());
			j.put(EntityConstants.CoinOrder.approvalFlag.toString(), co.getApprovalFlag());
			j.put(EntityConstants.CoinOrder.certSentFlag.toString(), co.getCertSentFlag());
			j.put(EntityConstants.CoinOrder.certCarrier.toString(), co.getCertCarrier());
			j.put(EntityConstants.CoinOrder.certTrackingNo.toString(), co.getCertTrackingNo());
			j.put(EntityConstants.CoinOrder.completeFlag.toString(), co.getCompleteFlag());
			j.put(EntityConstants.CoinOrder.caseCloser.toString(), co.getCaseCloser());
			
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy");			
		    String date = df.format(co.getAddTime());
		    
			j.put(EntityConstants.CoinOrder.addTime.toString(), date);

			return j;
		}
		return null;
		
	}
	
	public static JSONArray getCoinCounts(List<CoinCount> list){ 
		
		JSONArray array = JSONFactoryUtil.createJSONArray();
		
		if(list != null && !list.isEmpty()){
			for(CoinCount cc : list){
				JSONObject j = JSONFactoryUtil.createJSONObject();
				j.put("phase", cc.getPhase());
				j.put("totalAmount", cc.getTotalAmount());
				j.put("endFlag", cc.getEndFlag());
				j.put("rollToPhase", cc.getRollToPhase());
				j.put("rollToAmt", cc.getRollToAmt());
				j.put("limit", cc.getLimit());
				j.put("rollOverAmt", cc.getRollOverAmt());
				
				array.put(j);
			}
		}
		
		return array;
		
	}
	
//	private static void addStaffUserInfo(String userId, JSONObject j){
//		User u;
//		try {
//			u = UserUtil.fetchByPrimaryKey(Long.parseLong(userId));
//			if(u != null){
//				j.put(EntityConstants.coinUser.firstName.toString(), u.getFirstName());
//				j.put(EntityConstants.coinUser.lastName.toString(), u.getLastName());
//				j.put(EntityConstants.coinUser.birthday.toString(), "");
//				j.put(EntityConstants.AaContact.email.toString(), u.getEmailAddress());
//			}
//		} catch (NumberFormatException e) {
//			e.printStackTrace();
//		} catch (SystemException e) {
//			e.printStackTrace();
//		}
//		
//	}
	
	private static void addBasicUserInfo(String userId, JSONObject j){
		CoinUser cu;
		try {
			cu = CoinUserUtil.fetchByPrimaryKey(userId);
			if(cu !=  null){
				j.put(EntityConstants.coinUser.firstName.toString(), cu.getFirstName());
				j.put(EntityConstants.coinUser.lastName.toString(), cu.getLastName());
				j.put(EntityConstants.coinUser.birthday.toString(), cu.getBirthday());
				
				AaContact contact = AaContactUtil.fetchByPrimaryKey(cu.getContactId());
				if(contact != null){
					j.put(EntityConstants.AaContact.email.toString(), contact.getEmail());
				}else{
					j.put(EntityConstants.AaContact.email.toString(), "");
				}
				
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
	}
	
	public static JSONObject getBrokerJson(BrokerInfo br){
		
		if(br != null){
			JSONObject json = JSONFactoryUtil.createJSONObject();
			json.put("brokerId", br.getBrokerId());
			json.put("licenseId", br.getLicenseId());
			json.put("licenseCountry", br.getLicenseCountry());
			return json;
		}else{
			return null;
		}
		
	}
	
	public static JSONObject getRsnJson(FailReason fr){
		
		if(fr != null){
			JSONObject json = JSONFactoryUtil.createJSONObject();
			json.put("rsnCd", fr.getRsnCd());
			json.put("desc", fr.getDesc());
			return json;
		}
		return null;
	}
	
	public static JSONObject getFundJson(FundInfo fi){
		
		if(fi != null){
			JSONObject json = JSONFactoryUtil.createJSONObject();
			json.put("fundId", fi.getFundId());
			json.put("name", fi.getName());
			
			AaContact ac = null;
			try {
				ac = AaContactLocalServiceUtil.getAaContact(fi.getContactId());
			} catch (PortalException e) {
				e.printStackTrace();
			} catch (SystemException e) {
				e.printStackTrace();
			}
			
			json.put("contact", getContactJson(ac));
			
			return json;
		}
		
		return null;	
	}
	
	public static JSONObject getContactJson(AaContact contact){
		
		JSONObject json = JSONFactoryUtil.createJSONObject();
		json.put(EntityConstants.AaContact.contactId.toString(), contact.getContactId());
		json.put(EntityConstants.AaContact.phone.toString(), contact.getPhone());
		json.put(EntityConstants.AaContact.areaCode.toString(), contact.getAreaCode());
		json.put(EntityConstants.AaContact.email.toString(), contact.getEmail());
		json.put(EntityConstants.AaContact.wechat.toString(), contact.getWechat());
		json.put(EntityConstants.AaContact.facebook.toString(), contact.getFacebook());
		json.put(EntityConstants.AaContact.twitter.toString(), contact.getTwitter());
		json.put(EntityConstants.AaContact.weibo.toString(), contact.getWeibo());
		json.put(EntityConstants.AaContact.street1.toString(), contact.getStreet1());
		json.put(EntityConstants.AaContact.street2.toString(), contact.getStreet2());
		json.put(EntityConstants.AaContact.city.toString(), contact.getCity());
		json.put(EntityConstants.AaContact.state.toString(), contact.getState());
		json.put(EntityConstants.AaContact.country.toString(), contact.getCountry());
		json.put(EntityConstants.AaContact.zipCode.toString(), contact.getZipCode());
				
		json.put(EntityConstants.AaContact.mailstreet1.toString(), contact.getMailstreet1());
		json.put(EntityConstants.AaContact.mailstreet2.toString(), contact.getMailstreet2());
		json.put(EntityConstants.AaContact.mailcity.toString(), contact.getMailcity());
		json.put(EntityConstants.AaContact.mailstate.toString(), contact.getMailstate());
		json.put(EntityConstants.AaContact.mailcountry.toString(), contact.getMailcountry());
		json.put(EntityConstants.AaContact.mailzipCode.toString(), contact.getMailzipCode());
		
		return json;
		
	}
	
	public static JSONArray getFutureUserListJson(List<FutureUser> list){
		JSONArray json = JSONFactoryUtil.createJSONArray();
		if(list != null && !list.isEmpty()){
			for(FutureUser fu : list){
				JSONObject j = JSONFactoryUtil.createJSONObject();
				j.put("email", fu.getEmail());
				j.put("firstName", fu.getFirstName());
				j.put("lastName", fu.getLastName());
				j.put("residency", fu.getResidency());
				j.put("areaCd", fu.getAreaCode());
				j.put("phone", fu.getPhone());
				json.put(j);
			}
			
		}
		return json;
	}
	
	public static JSONObject getResponse(JSONObject jo, JSONArray ja, String code){
		JSONObject json = JSONFactoryUtil.createJSONObject();
		
		JSONObject status = JSONFactoryUtil.createJSONObject();
		
		status.put(STATUS_CODE, code);
		json.put(STATUS, status);
		
		if(ErrorConstants.SUCCESS.equals(code)){
			if(jo != null){
			json.put(DATA, jo);
			}else if(ja != null){
				json.put(DATA, ja);
			}
		}
				
		return json;
	}
	
	public static JSONObject getOrderSummaryByCurrency(List<CoinOrder> list, int phase, String startDate, String endDate){
		
		JSONObject json = JSONFactoryUtil.createJSONObject();
		
		/*
		 * phase n:
		 *	-currency : 
		 *		-region : amount
		 *		-region : amount
		 *			.
		 *			.
		 *			.
		 *	-currency : total
		 *		.
		 *		.
		 *		.
		 */
		if(list != null && !list.isEmpty()){
			for(CoinOrder co : list){
				if(phase == co.getSalePhase() && CommonUtil.withinRangeYMD(co.getAddTime(), startDate, endDate)){
					String region = co.getSaleRegion();
					if(region == null || region.length() < 1){
						region = "other";
					}
					String currency = co.getCurrency();
					
					if(json.has(currency)){
						JSONObject jCur = json.getJSONObject(currency);
						if(jCur.has(region)){
							//add
							JSONObject jreg = jCur.getJSONObject(region);
							
							double newTotalMoney = co.getTotalPrice() + jreg.getDouble("money");
							long newTotalCoinSale = co.getSaleAmount() + jreg.getLong("salecoin");
							long newTotalCoinPromo = co.getPromoAmount() + jreg.getLong("promocoin");
							
							jreg.put("money", newTotalMoney);
							jreg.put("salecoin", newTotalCoinSale);
							jreg.put("promocoin", newTotalCoinPromo);
							
						}else{
							JSONObject jreg = JSONFactoryUtil.createJSONObject();
							
							jreg.put("money", co.getTotalPrice());
							jreg.put("salecoin", co.getSaleAmount());
							jreg.put("promocoin", co.getPromoAmount());
							
							jCur.put(region, jreg);
						}
					}else{
						JSONObject jCur = JSONFactoryUtil.createJSONObject();						
						
						JSONObject jreg = JSONFactoryUtil.createJSONObject();
						
						jreg.put("money", co.getTotalPrice());
						jreg.put("salecoin", co.getSaleAmount());
						jreg.put("promocoin", co.getPromoAmount());
						
						jCur.put(region, jreg);
						
						json.put(currency, jCur);
					}
				}				
			}
		}
		
		return json;
		
	}
	
	
}
