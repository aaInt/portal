/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.aw.internal.model.CoinOrder;
import com.aw.internal.model.CoinUser;
import com.aw.internal.model.UnitPrice;
import com.aw.internal.service.BrokerInfoLocalServiceUtil;
import com.aw.internal.service.CoinOrderLocalServiceUtil;
import com.aw.internal.service.CoinUserLocalServiceUtil;
import com.aw.internal.service.FailReasonLocalServiceUtil;
import com.aw.internal.service.UnitPriceLocalServiceUtil;
import com.aw.internal.service.base.CoinOrderServiceBaseImpl;
import com.aw.internal.service.persistence.CoinOrderUtil;
import com.aw.internal.service.persistence.CoinUserUtil;
import com.aw.internal.service.persistence.FailReasonUtil;
import com.aw.internal.service.persistence.UnitPricePK;
import com.aw.internal.service.util.CoinCountUtil;
import com.aw.internal.service.util.CommonUtil;
import com.aw.internal.service.util.DataUtil;
import com.aw.internal.service.util.ErrorConstants;
import com.aw.internal.service.util.PermisionChecker;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.model.User;
import com.liferay.portal.security.ac.AccessControlled;
import com.liferay.portal.security.permission.PermissionChecker;
import com.liferay.portal.security.permission.PermissionThreadLocal;


/**
 * The implementation of the coin order remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.liferay.sample.service.CoinOrderService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author zl
 * @see com.aw.internal.service.base.CoinOrderServiceBaseImpl
 * @see com.liferay.sample.service.CoinOrderServiceUtil
 */
public class CoinOrderServiceImpl extends CoinOrderServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.liferay.sample.service.CoinOrderServiceUtil} to access the coin order remote service.
	 */
	
	@AccessControlled(guestAccessEnabled=true)
	public JSONObject verifyOrderInfo(String orderId, String userId, String lastName){
		try {
			CoinOrder co = CoinOrderUtil.fetchByPrimaryKey(orderId);
			
			if(co == null || !userId.equals(co.getUserId())){//exit if no order or order-user not match
				throw new SystemException();
			}
			
			CoinUser cu = CoinUserUtil.fetchByPrimaryKey(userId);
			if(cu == null || !lastName.equals(cu.getLastName())){//exit if no user or last name not match
				throw new SystemException();
			}
			
			return DataUtil.getResponse(DataUtil.getCertInfoJson(cu, co), null, ErrorConstants.SUCCESS);
			
		} catch (SystemException e) {
			return DataUtil.getResponse(null, null, ErrorConstants.ORDER_NOT_ACTIVE);
		}
	}
	
	
	public JSONObject createCoinOrder(String userId, long brokerId, int phase, double unitPrice
			,double totalPrice, String currency, long saleAmt, long promoAmt){
		
		long salesId = -1;		
		
		//verify sales, login role check
		User user = new PermisionChecker().addRole(PermisionChecker.SALES_US)
				.addRole(PermisionChecker.SALES_CN).permissionCheck();
		
		if(user != null){
			salesId = user.getUserId();
		}else{
			return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
		}				
		
		//verify price
		try {
			UnitPrice up = UnitPriceLocalServiceUtil.fetchUnitPrice(new UnitPricePK(currency, phase));
			
			if(up == null || !"Y".equals(up.getActiveFlag()) || unitPrice != up.getPrice() || saleAmt * up.getPrice() > totalPrice+0.01){
				
				return DataUtil.getResponse(null, null, ErrorConstants.INVALID_PHASE_OR_PRICE);
				
			}else{
				
			}
		} catch (SystemException e1) {
			e1.printStackTrace();
			return DataUtil.getResponse(null, null, ErrorConstants.INVALID_PHASE_OR_PRICE);
		}
		
		//verify user info and brokerInfo
		if(userId != null && !userId.isEmpty()){
			if(!CoinUserLocalServiceUtil.validateUser(userId, user.getUserId())){
				return DataUtil.getResponse(null, null, ErrorConstants.USER_NOT_VERIFIED);
			}
		}else{
			return DataUtil.getResponse(null, null, ErrorConstants.USER_NOT_VERIFIED);
		}
		
		if(brokerId > 0){
			if(!BrokerInfoLocalServiceUtil.validateBroker(brokerId)){
				return DataUtil.getResponse(null, null, ErrorConstants.BROKER_NOT_VERIFIED);
			}
		}
		
		//                      creater       ++      gmt YYMMDDHHMMss
		String orderId = "D"+user.getUserId()+"AT"+CommonUtil.getTimeString();
		
		//verify the amount
		if(!CoinCountUtil.checkAndUpdate(saleAmt+promoAmt, phase, orderId)){
			return DataUtil.getResponse(null, null, ErrorConstants.TOTAL_COIN_REACH_LIMIT);
		}
		
		String region = null;
		try {
			CoinUser cu = CoinUserUtil.fetchByPrimaryKey(userId);
			if(cu != null){
				region = cu.getNationality();
			}
		} catch (SystemException e1) {
			e1.printStackTrace();
		}

		//create entity                               
		CoinOrder co = CoinOrderLocalServiceUtil.createCoinOrder(orderId);
		co.setUserId(userId);
		co.setSaleRegion(region);
		co.setBrokerId(brokerId);
		
		co.setSalesId(salesId);
		co.setActiveFlag("Y");
		
		co.setSalePhase(phase);
		co.setUnitPrice(unitPrice);
		co.setTotalPrice(totalPrice);
		co.setCurrency(currency);
		co.setSaleAmount(saleAmt);
		co.setPromoAmount(promoAmt);
		co.setTotalAmount(saleAmt+promoAmt);
		
		co.setVerifyFlag("N");
		co.setDocSentFlag("N");
		co.setDocSignFlag("N");
		co.setPaidFlag("N");
		co.setApprovalFlag("N");
		co.setCertSentFlag("N");
		co.setCompleteFlag("N");
		
		co.setAddTime(new Date());
		
		try {
			co.persist();
		} catch (SystemException e) {			
			e.printStackTrace();
			return DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
		}
		
		if(co.getUserId().contains("U")){
			return DataUtil.getResponse(DataUtil.getCoinOrderJson(co), null, ErrorConstants.SUCCESS);
		}else{
			return DataUtil.getResponse(DataUtil.getStaffCoinOrderJson(co), null, ErrorConstants.SUCCESS);
		}
		
		
	}
	
//	private String updateOrder(){
//		
//		//TODO remember to change the total count!!!!!!!!!!!!!!
//		
//		return "function under development";
//	}
	
	public JSONObject splitOrder(String originOrderId, long salesId, String userId
			, long amount) throws SystemException{
		
		//verify sales, login role check
		User user = new PermisionChecker().addRole(PermisionChecker.ACCOUNTING)
				.addRole(PermisionChecker.HR).permissionCheck();
		
		if(user != null){
			//get order
			CoinOrder originOrder = null;
			
			try {
				originOrder = CoinOrderUtil.fetchByPrimaryKey(originOrderId);
			} catch (SystemException e) {
				e.printStackTrace();
			}
			
			if(originOrder != null){
				if(CoinUserLocalServiceUtil.validateUser(userId, salesId)){
					//long restTotalAmt = originOrder.getTotalAmount() - ;
					long restAmt = originOrder.getSaleAmount() - amount;
					if(restAmt > 0){
						//usr info and amount all valid, do update and create
						double unitprice = originOrder.getUnitPrice();
						
						originOrder.setSaleAmount(restAmt);
						originOrder.setTotalAmount(restAmt+originOrder.getPromoAmount());
						originOrder.setTotalPrice(restAmt*unitprice);
						
						originOrder.persist();
						
						CoinOrder newOrder = (CoinOrder) originOrder.clone();
						
						//                 creater       ++      gmt YYMMDDHHMMss
						String newId = "D"+salesId+"AT"+CommonUtil.getTimeString();

						String region = null;
						try {
							CoinUser cu = CoinUserUtil.fetchByPrimaryKey(userId);
							if(cu != null){
								region = cu.getNationality();
							}
						} catch (SystemException e1) {
							e1.printStackTrace();
						}
						
						newOrder.setOrderId(newId);
						newOrder.setUserId(userId);
						newOrder.setSalesId(salesId);
						newOrder.setSaleRegion(region);
						newOrder.setSaleAmount(amount);
						newOrder.setPromoAmount(0);
						newOrder.setTotalAmount(amount);
						newOrder.setTotalPrice(unitprice*amount);
						newOrder.setVerifyFlag("N");
						newOrder.setDocSentFlag("N");
						newOrder.setDocSignFlag("N");
						newOrder.setPaidFlag("N");
						newOrder.setApprovalFlag("N");
						newOrder.setCertSentFlag("N");
						newOrder.setCompleteFlag("N");						
						newOrder.setAddTime(new Date());
						
						newOrder.persist();
						return DataUtil.getResponse(DataUtil.getCoinOrderJson(newOrder), null, ErrorConstants.SUCCESS);
						
					}else{
						return DataUtil.getResponse(null, null, ErrorConstants.INVALID_PHASE_OR_PRICE);
					}
				}else{
					return DataUtil.getResponse(null, null, ErrorConstants.USER_NOT_VERIFIED);
				}
			}else{
				return DataUtil.getResponse(null, null, ErrorConstants.ORDER_NOT_ACTIVE);
			}
			
		}else{
			return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
		}
	}
	
	public JSONObject getPedingOrder(){
		
		//TODO check role
		//if role valid
		//then according to the role
		//decide which query to use
		Set<String> roles = new PermisionChecker().getRolesForUser();
		
		long idFilter = -1;
		
		if(roles.contains(PermisionChecker.ADMIN) || roles.contains(PermisionChecker.OPERATOR) 
				|| roles.contains(PermisionChecker.ACCOUNTING) || roles.contains(PermisionChecker.SALES_GL_MNG)){
			idFilter = 0;//see all
		}else if(roles.contains(PermisionChecker.SALES_US) || roles.contains(PermisionChecker.SALES_CN)){
			PermissionChecker pc = PermissionThreadLocal.getPermissionChecker();
			User user = pc.getUser();
			idFilter = user.getUserId();
		}else{
			return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
		}
		
		try {
			List<CoinOrder> list = CoinOrderUtil.findBypending("Y", "N");
			List<CoinOrder> finalList;// = new ArrayList<CoinOrder>();
			if(list != null && !list.isEmpty()){
				if(idFilter == 0){
					finalList = list;
				}else{
					finalList = new ArrayList<CoinOrder>();
					for(CoinOrder co : list){
						if(co.getSalesId() == idFilter){
							finalList.add(co);
						}
					}
				}
			}else{
				finalList = null;
			}
			
			JSONArray j = JSONFactoryUtil.createJSONArray();
			if(finalList != null && !finalList.isEmpty()){
				for(CoinOrder co : finalList){
					JSONObject jsn = DataUtil.getCoinOrderJson(co);
					if(jsn != null){
						j.put(jsn);
					}				
				}
			}
			
			return DataUtil.getResponse(null, j, ErrorConstants.SUCCESS);
			
			
		} catch (SystemException e) {
			
			e.printStackTrace();
			return null;
		}
		
	}
	
	public JSONObject getStaffOrders(){
		
		User user = new PermisionChecker().addRole(PermisionChecker.HR).permissionCheck();//only for Admin
		
		if(user != null){
			List<CoinOrder> list = null;
			try {
				list = CoinOrderUtil.findBypending("Y", "N");
			} catch (SystemException e) {				
				e.printStackTrace();
				return null;
			}
			
			JSONArray j = JSONFactoryUtil.createJSONArray();
			if(list != null && !list.isEmpty()){
				for(CoinOrder co : list){
					JSONObject jsn = DataUtil.getStaffCoinOrderJson(co);
					if(jsn != null){
						j.put(jsn);
					}				
				}
			}			
			return DataUtil.getResponse(null, j, ErrorConstants.SUCCESS);
			
		}else{
			return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
		}
		
	}
	
	public JSONObject getCompletedOrders(){
		
		//TODO check role
		//if role valid
		//then according to the role
		//decide which query to use
		Set<String> roles = new PermisionChecker().getRolesForUser();
		
		long idFilter = -1;
		
		if(roles.contains(PermisionChecker.ADMIN) || roles.contains(PermisionChecker.OPERATOR) 
				|| roles.contains(PermisionChecker.ACCOUNTING) || roles.contains(PermisionChecker.SALES_GL_MNG)){
			idFilter = 0;//see all
		}else if(roles.contains(PermisionChecker.SALES_US) || roles.contains(PermisionChecker.SALES_CN)){
			PermissionChecker pc = PermissionThreadLocal.getPermissionChecker();
			User user = pc.getUser();
			idFilter = user.getUserId();
		}else{
			return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
		}
		
		try {
			List<CoinOrder> list = CoinOrderUtil.findBycomplete("N", "Y");
			List<CoinOrder> finalList;// = new ArrayList<CoinOrder>();
			if(list != null && !list.isEmpty()){
				if(idFilter == 0){
					finalList = list;
				}else{
					finalList = new ArrayList<CoinOrder>();
					for(CoinOrder co : list){
						if(co.getSalesId() == idFilter){
							finalList.add(co);
						}
					}
				}
			}else{
				finalList = null;
			}
			
			JSONArray j = JSONFactoryUtil.createJSONArray();
			if(finalList != null && !finalList.isEmpty()){
				for(CoinOrder co : finalList){
					JSONObject jtemp = DataUtil.getCoinOrderJson(co);
					if(null != jtemp){
						j.put(jtemp);
					}else if(roles.contains(PermisionChecker.ADMIN)){
						jtemp = DataUtil.getStaffCoinOrderJson(co);
						if(null != jtemp){
							j.put(jtemp);
						}
					}
				}
			}
			
			return DataUtil.getResponse(null, j, ErrorConstants.SUCCESS);
			
			
		} catch (SystemException e) {
			
			e.printStackTrace();
			return null;
		}
		
	}
	
	
	public JSONObject getFailedOrder(){
		
		//TODO check role
		//if role valid
		//then according to the role
		//decide which query to use
		Set<String> roles = new PermisionChecker().getRolesForUser();
		
		long idFilter = -1;
		
		if(roles.contains(PermisionChecker.ADMIN) || roles.contains(PermisionChecker.OPERATOR) 
				|| roles.contains(PermisionChecker.ACCOUNTING)|| roles.contains(PermisionChecker.SALES_GL_MNG)){
			idFilter = 0;//see all
		}else if(roles.contains(PermisionChecker.SALES_US) || roles.contains(PermisionChecker.SALES_CN)){
			PermissionChecker pc = PermissionThreadLocal.getPermissionChecker();
			User user = pc.getUser();
			idFilter = user.getUserId();
		}else{
			return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
		}
		
		try {
			List<CoinOrder> list = CoinOrderUtil.findBycomplete("N", "N");
			List<CoinOrder> finalList;// = new ArrayList<CoinOrder>();
			if(list != null && !list.isEmpty()){
				if(idFilter == 0){
					finalList = list;
				}else{
					finalList = new ArrayList<CoinOrder>();
					for(CoinOrder co : list){
						if(co.getSalesId() == idFilter){
							finalList.add(co);
						}
					}
				}
			}else{
				finalList = null;
			}
			
			JSONArray j = JSONFactoryUtil.createJSONArray();
			if(finalList != null && !finalList.isEmpty()){
				for(CoinOrder co : finalList){
					JSONObject jtemp = DataUtil.getCoinOrderJson(co);
					if(null != jtemp){
						j.put(jtemp);
					}else if(roles.contains(PermisionChecker.ADMIN)){
						jtemp = DataUtil.getStaffCoinOrderJson(co);
						if(null != jtemp){
							j.put(jtemp);
						}
					}
					
				}
			}
			
			return DataUtil.getResponse(null, j, ErrorConstants.SUCCESS);
			
			
		} catch (SystemException e) {
			
			e.printStackTrace();
			return null;
		}
		
	}
	 
	//sale verified then update and send to op
	public JSONObject verified(String orderId){
		
		User user = new PermisionChecker().addRole(PermisionChecker.SALES_US)
				.addRole(PermisionChecker.SALES_CN).permissionCheck();
		
		if(null == user){
			return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
		}
		
		try {
			CoinOrder co = CoinOrderLocalServiceUtil.fetchCoinOrder(orderId);
			
			if(co != null){
				
				if("N".equals(co.getActiveFlag())){
					return DataUtil.getResponse(null, null, ErrorConstants.ORDER_NOT_ACTIVE);
				}
				
				if("N".equals(co.getVerifyFlag())){
					co.setVerifyFlag("Y-"+user.getUserId());
					co.persist();					
				}
				return DataUtil.getResponse(null, null, ErrorConstants.SUCCESS);
				
			}else{
				return DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
			}
			
		} catch (SystemException e) {			
			e.printStackTrace();
			return DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
		}
	}
	
	//contract prepared and sent
	public JSONObject contractSent(String orderId, String carrier, String trackingNo){
		
		User user = new PermisionChecker().permissionCheck(PermisionChecker.OPERATOR);
		if(null == user){
			return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
		}
		
		try {
			CoinOrder co = CoinOrderLocalServiceUtil.fetchCoinOrder(orderId);
			
			if(co != null){
				
				if("N".equals(co.getActiveFlag())){
					return DataUtil.getResponse(null, null, ErrorConstants.ORDER_NOT_ACTIVE);
				}
				
				if(!co.getVerifyFlag().contains("Y")){
					return DataUtil.getResponse(null, null, ErrorConstants.NEED_VERIFY);
				}
				
				if("N".equals(co.getDocSentFlag())){
					co.setDocSentFlag("Y-"+user.getUserId());
					co.setDocCarrier(carrier);
					co.setDocTrackingNo(trackingNo);
					co.persist();					
				}
				return DataUtil.getResponse(null, null, ErrorConstants.SUCCESS);
				
			}else{
				return DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
			}
			
		} catch (SystemException e) {			
			e.printStackTrace();
			return DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
		}
	}
	
	//contract signed
	public JSONObject contractSigned(String orderId){
		User user = new PermisionChecker().permissionCheck(PermisionChecker.OPERATOR);
		if(null == user){
			return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
		}
		
		try {
			CoinOrder co = CoinOrderLocalServiceUtil.fetchCoinOrder(orderId);
			
			if(co != null){
				
				if("N".equals(co.getActiveFlag())){
					return DataUtil.getResponse(null, null, ErrorConstants.ORDER_NOT_ACTIVE);
				}
				
				if(!co.getVerifyFlag().contains("Y")){
					return DataUtil.getResponse(null, null, ErrorConstants.NEED_VERIFY);
				}
				
				if(!co.getDocSentFlag().contains("Y")){
					return DataUtil.getResponse(null, null, ErrorConstants.NEED_SEND_DOC);
				}
				
				if("N".equals(co.getDocSignFlag())){
					co.setDocSignFlag("Y-"+user.getUserId());
					co.persist();					
				}
				return DataUtil.getResponse(null, null, ErrorConstants.SUCCESS);
				
			}else{
				return DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
			}
			
		} catch (SystemException e) {			
			e.printStackTrace();
			return DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
		}
	}
	
	//payment received
	public JSONObject paid(String orderId){
		User user = new PermisionChecker().permissionCheck(PermisionChecker.ACCOUNTING);
		if(null == user){
			return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
		}
		
		try {
			CoinOrder co = CoinOrderLocalServiceUtil.fetchCoinOrder(orderId);
			
			if(co != null){
				
				if("N".equals(co.getActiveFlag())){
					return DataUtil.getResponse(null, null, ErrorConstants.ORDER_NOT_ACTIVE);
				}
				
				if(!co.getVerifyFlag().contains("Y")){
					return DataUtil.getResponse(null, null, ErrorConstants.NEED_VERIFY);
				}
				
				if("N".equals(co.getPaidFlag())){
					co.setPaidFlag("Y-"+user.getUserId());
					co.persist();					
				}
				return DataUtil.getResponse(null, null, ErrorConstants.SUCCESS);
				
			}else{
				return DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
			}
			
		} catch (SystemException e) {			
			e.printStackTrace();
			return DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
		}
	}
	
	//manage do final review and approve
	public JSONObject approve(String orderId){
		User user = new PermisionChecker().permissionCheck(PermisionChecker.ADMIN);
		if(null == user){
			return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
		}
		
		try {
			CoinOrder co = CoinOrderLocalServiceUtil.fetchCoinOrder(orderId);
			
			if(co != null){
				
				if(co.getSaleAmount() == 0){//not a sale order, then can be approved
					co.setApprovalFlag("Y-"+user.getUserId());
					co.persist();
					return DataUtil.getResponse(null, null, ErrorConstants.SUCCESS);
				}
				
				if("N".equals(co.getActiveFlag())){
					return DataUtil.getResponse(null, null, ErrorConstants.ORDER_NOT_ACTIVE);
				}
				
				if(!co.getDocSignFlag().contains("Y")){
					return DataUtil.getResponse(null, null, ErrorConstants.NEED_SIGNED);
				}
				
				if(!co.getPaidFlag().contains("Y")){
					return DataUtil.getResponse(null, null, ErrorConstants.NEED_PAID);
				}
				
				if("N".equals(co.getApprovalFlag())){
					co.setApprovalFlag("Y-"+user.getUserId());
					co.persist();					
				}
				return DataUtil.getResponse(null, null, ErrorConstants.SUCCESS);
				
			}else{
				return DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
			}
			
		} catch (SystemException e) {			
			e.printStackTrace();
			return DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
		}
	}
	
	//op send the certificate
	public JSONObject certSent(String orderId, String carrier, String trackingNo){
		User user = new PermisionChecker().permissionCheck(PermisionChecker.OPERATOR);
		if(null == user){
			return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
		}
		
		try {
			CoinOrder co = CoinOrderLocalServiceUtil.fetchCoinOrder(orderId);
			
			if(co != null){
				
				if("N".equals(co.getActiveFlag())){
					return DataUtil.getResponse(null, null, ErrorConstants.ORDER_NOT_ACTIVE);
				}
				
				if(!co.getApprovalFlag().contains("Y")){
					return DataUtil.getResponse(null, null, ErrorConstants.NEED_APPROVE);
				}
				
				if("N".equals(co.getCertSentFlag())){
					co.setCertSentFlag("Y-"+user.getUserId());
					co.setCertCarrier(carrier);
					co.setCertTrackingNo(trackingNo);
					co.persist();					
				}
				return DataUtil.getResponse(null, null, ErrorConstants.SUCCESS);
				
			}else{
				return DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
			}
			
		} catch (SystemException e) {			
			e.printStackTrace();
			return DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
		}
	}
	
	public JSONObject complete(String orderId){
		User user = new PermisionChecker().permissionCheck(PermisionChecker.OPERATOR);//TODO who?
		if(null == user){
			return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
		}
		
		try {
			CoinOrder co = CoinOrderLocalServiceUtil.fetchCoinOrder(orderId);
			
			if(co != null){
				
				if("N".equals(co.getActiveFlag())){
					return DataUtil.getResponse(null, null, ErrorConstants.ORDER_NOT_ACTIVE);
				}
				
				if(!co.getCertSentFlag().contains("Y")){
					return DataUtil.getResponse(null, null, ErrorConstants.NEED_SEND_CERT);
				}
				
				if("N".equals(co.getCompleteFlag())){
					co.setCompleteFlag("Y");
					co.setActiveFlag("N");
					co.setCaseCloser(user.getUserId());
					co.persist();					
				}
				return DataUtil.getResponse(null, null, ErrorConstants.SUCCESS);
				
			}else{
				return DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
			}
			
		} catch (SystemException e) {			
			e.printStackTrace();
			return DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
		}
	}
	
	public JSONObject fail(String orderId, String rsnCd){
		
		User op_ac_user = new PermisionChecker().addRole(PermisionChecker.ACCOUNTING)
				.addRole(PermisionChecker.OPERATOR).permissionCheck();
		
		User sale_user = new PermisionChecker().addRole(PermisionChecker.SALES_CN)
				.addRole(PermisionChecker.SALES_US).permissionCheck();
		
		boolean anyOrder = false;
		if(null == op_ac_user && sale_user == null){
			return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
		}
		
		if(op_ac_user != null){
			anyOrder = true;
		}
		
		try {
			CoinOrder co = CoinOrderLocalServiceUtil.fetchCoinOrder(orderId);
			
			if(co != null){
				
				if("N".equals(co.getActiveFlag())){
					return DataUtil.getResponse(null, null, ErrorConstants.ORDER_NOT_ACTIVE);
				}
				if(null == FailReasonUtil.fetchByPrimaryKey(rsnCd)){
					return DataUtil.getResponse(null, null, ErrorConstants.INVALID_RSN_CD);
				}
				
				if(anyOrder){
					co.setActiveFlag("N");
					co.setRsnCd(rsnCd);
					co.setCaseCloser(op_ac_user.getUserId());
				}else{
					//only allow sales to fail themselves not verified orders
					if("N".equals(co.getVerifyFlag()) && sale_user.getUserId() == co.getSalesId()){
						co.setActiveFlag("N");
						co.setRsnCd(rsnCd);
						co.setCaseCloser(sale_user.getUserId());
					}else{
						return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
					}
				}												
				
				//release the amount in total count
				if(CoinCountUtil.checkAndUpdate(-(co.getSaleAmount()+co.getPromoAmount()), co.getSalePhase(), co.getOrderId())){
					co.persist();
					return DataUtil.getResponse(null, null, ErrorConstants.SUCCESS);
				}else{
					return DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
				}		
				
			}else{
				return DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
			}
			
		} catch (SystemException e) {			
			e.printStackTrace();
			return DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
		}
	}
	
	public JSONObject addReason(String rsnCd, String desc){
		if(null != new PermisionChecker().permissionCheck(PermisionChecker.ADMIN)){
			return FailReasonLocalServiceUtil.addReason(rsnCd, desc);
		}else{
			return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
		}
	}
	
	public JSONObject getReason(String rsnCd){
		return FailReasonLocalServiceUtil.getReason(rsnCd);
	}
	
	public JSONObject getReasons(){
		return FailReasonLocalServiceUtil.getReasons();
	}
	
	//............................................................. has to be format yyyyMMdd         
	public JSONObject getSaleSummary(boolean isPaid, boolean isPending, int phase, String startDate, String endDate){//yyyy-mm-dd
		
		if(null != new PermisionChecker().addRole(PermisionChecker.ACCOUNTING)
				.addRole(PermisionChecker.MANAGEMENT).permissionCheck()){//only admin and accounting and management
			
			List<CoinOrder> list = null;
			
			String isPaidStr = "";
			if(isPaid){
				isPaidStr = "Y";
			}else{
				isPaidStr = "N";
			}
			
			if(isPending){
				try {
					list = CoinOrderUtil.findBypending("Y", "N");
					List<CoinOrder> finalList = new ArrayList<CoinOrder>();
					//only get verified orders
					if(list != null && !list.isEmpty()){
						for(CoinOrder c : list){
							if(c.getVerifyFlag().contains("Y") && (c.getPaidFlag().contains(isPaidStr) || c.getTotalPrice() == 0)){
								finalList.add(c);
							}
						}
					}
					return DataUtil.getResponse(DataUtil.getOrderSummaryByCurrency(finalList, phase, startDate, endDate), null, ErrorConstants.SUCCESS);
				} catch (SystemException e) {
					e.printStackTrace();
					return DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
				}
			}else{
				try {
					list = CoinOrderUtil.findBypending("N", "Y");
					return DataUtil.getResponse(DataUtil.getOrderSummaryByCurrency(list, phase, startDate, endDate), null, ErrorConstants.SUCCESS);
				} catch (SystemException e) {
					e.printStackTrace();
					return DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
				}
			}		
		}else{
			return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
		}
	}
}