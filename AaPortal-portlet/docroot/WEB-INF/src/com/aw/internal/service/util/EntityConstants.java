package com.aw.internal.service.util;

public class EntityConstants {
	
	
	
	public static enum coinUser{
		userId("userId"),
        firstName("firstName"),
        lastName("lastName"),
        activeFlag("activeFlag"),
        birthday("birthday"),
        contact("contact"),
        userType("userType"),
        brokerId("brokerId"),
        fundId("fundId"),
        nationality("nationality"),
        idIssueCountry("idIssueCountry"),
        idType("idType"),
        idNumber("idNumber"),
        photoImage("photoImage"),
        ssn("ssn"),
        ein("ein"),
        income("income"),
        asset("asset"),
        isValidated("isValidated"),
        isCreddited("isCreddited"),
        addTime("addTime"), 
        addUser("addUser");
		
		private String coinUser;

		private coinUser(String coinUser) {
			this.coinUser = coinUser;
		}
			 
		public String toString() { return this.coinUser;}
	}
	
	public enum AaContact{
		contactId,
        phone,
        areaCode,
        email,
        wechat,
        facebook,
        twitter,
        weibo,
        street1,
        street2,
        city,
        state,
        country,
        zipCode,
        isMailDiff,        
        mailstreet1,
        mailstreet2,
        mailcity,
        mailstate,
        mailcountry,
        mailzipCode,
	}
	
	public enum CoinOrder{
		orderId,
    	userId,
    	brokerId,
    	salesId,
    	activeFlag,
    	
    	
    	salePhase,
    	unitPrice,
    	totalPrice,
    	currency,
    	saleAmount,
    	promoAmount,
    	totalAmount,
    	
    	
    	rsnCd,
    	verifyFlag,
    	docSentFlag,
    	docCarrier,
    	docTrackingNo,
    	docSignFlag,
    	paidFlag,
    	approvalFlag,
    	certSentFlag,
    	certCarrier,
    	certTrackingNo,
    	completeFlag,
    	caseCloser,
    	addTime
	}
	
	public enum BrokerInfo{
		brokerId,
		licenseId,
		licenseCountry
	}
}
