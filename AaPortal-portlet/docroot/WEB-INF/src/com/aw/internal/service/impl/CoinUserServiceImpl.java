/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.impl;

import java.sql.Blob;
import java.sql.SQLException;
import java.util.List;

import com.aw.internal.model.CertDocStore;
import com.aw.internal.model.CoinUser;
import com.aw.internal.model.FundInfo;
import com.aw.internal.model.ImageStore;
import com.aw.internal.service.AaContactLocalServiceUtil;
import com.aw.internal.service.BrokerInfoLocalServiceUtil;
import com.aw.internal.service.CoinUserLocalServiceUtil;
import com.aw.internal.service.FundInfoLocalServiceUtil;
import com.aw.internal.service.base.CoinUserServiceBaseImpl;
import com.aw.internal.service.persistence.CertDocStoreUtil;
import com.aw.internal.service.persistence.CoinUserUtil;
import com.aw.internal.service.persistence.FundInfoUtil;
import com.aw.internal.service.persistence.ImageStoreUtil;
import com.aw.internal.service.util.DataUtil;
import com.aw.internal.service.util.ErrorConstants;
import com.aw.internal.service.util.PermisionChecker;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.model.User;

import org.apache.commons.codec.binary.Base64;


/**
 * The implementation of the coin user remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.liferay.sample.service.CoinUserService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author zl
 * @see com.liferay.sample.service.base.CoinUserServiceBaseImpl
 * @see com.liferay.sample.service.CoinUserServiceUtil
 */
public class CoinUserServiceImpl extends CoinUserServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.liferay.sample.service.CoinUserServiceUtil} to access the coin user remote service.
	 */
	public JSONObject addCoinUser(String firstName, String lastName, String birthday, String userType
			, String brkLics, String brkCountry, long fundId, String nationality, String idIssueCountry
			, String idType, String idNumber, String photoImage, String certDocStr, String ssn, String ein
			, String income, String asset, long phone, int areaCode, String email, String wechat
			, String facebook, String twitter, String weibo, String street1, String street2, String city
			, String state, String country, String zipCode, String isMailDiff, String mailStreet1
			, String mailStreet2, String mailCity, String mailState
			, String mailCountry, String mailZipCode){
		
		//maybe more later
		User user = new PermisionChecker().addRole(PermisionChecker.SALES_US)
				.addRole(PermisionChecker.SALES_CN).permissionCheck();
		
		if(user != null){
			
			return CoinUserLocalServiceUtil.addCoinUser(user.getUserId(), firstName, lastName, birthday, userType
					, brkLics, brkCountry, fundId, nationality, idIssueCountry, idType, idNumber, photoImage
					, certDocStr, ssn, ein, income, asset, phone, areaCode, email, wechat, facebook, twitter
					, weibo, street1, street2, city, state, country, zipCode, isMailDiff
					, mailStreet1, mailStreet2, mailCity, mailState, mailCountry, mailZipCode);
		}else{
			return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
		}
	}
	
	public JSONObject updateCoinUser(String coinUserId, String firstName, String lastName, String birthday, String userType
			, String brkLics, String brkCountry, long fundId
			, String nationality, String idIssueCountry, String idType, String idNumber, String ssn, String ein
			, String income, String asset, long phone, int areaCode, String email, String wechat
			, String facebook, String twitter, String weibo, String street1, String street2, String city
			, String state, String country, String zipCode, String isMailDiff, String mailStreet1
			, String mailStreet2, String mailCity, String mailState
			, String mailCountry, String mailZipCode){
		
		//maybe more later
		User user = new PermisionChecker().addRole(PermisionChecker.SALES_US)
				.addRole(PermisionChecker.SALES_CN).permissionCheck();
		
		if(user != null){
			
			return CoinUserLocalServiceUtil.updateCoinUser(user.getUserId(), coinUserId, firstName, lastName, birthday, userType
					, brkLics, brkCountry, fundId, nationality, idIssueCountry, idType, idNumber, ssn, ein
					, income, asset, phone, areaCode, email, wechat, facebook, twitter, weibo, street1, street2
					, city, state, country, zipCode, isMailDiff, mailStreet1, mailStreet2, mailCity, mailState, mailCountry, mailZipCode);
		}else{
			return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
		}
	}
	
	public JSONObject deleteUser(String coinuserId){
		
		//now only allow admin
		User user = new PermisionChecker().permissionCheck();
		
		if(user == null){
			return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
		}
	
		CoinUser cu;
		try {
			cu = CoinUserUtil.fetchByPrimaryKey(coinuserId);
			if(cu != null){
				if("Y".equals(cu.getActiveFlag())){
					return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
				}
				CoinUserLocalServiceUtil.deleteCoinUser(cu);
				return DataUtil.getResponse(null, null, ErrorConstants.SUCCESS);
				
			}else{
				return DataUtil.getResponse(null, null, ErrorConstants.USER_NOT_VERIFIED);
			}
		} catch (SystemException e) {
			
			e.printStackTrace();
			return DataUtil.getResponse(null, null, ErrorConstants.USER_NOT_VERIFIED);
		}
		
	}
	
	public JSONObject updatePhoto(String coinuserId, String photoStr){
		
		//maybe more later
		User user = new PermisionChecker().addRole(PermisionChecker.SALES_US)
				.addRole(PermisionChecker.SALES_CN).permissionCheck();
		
		if(user != null){
			
			return CoinUserLocalServiceUtil.updatePhoto(user.getUserId(), coinuserId, photoStr);
		}else{
			return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
		}
		
	}
	
	public JSONObject updateDoc(String coinuserId, String docStr){
		
		//maybe more later
		User user = new PermisionChecker().addRole(PermisionChecker.SALES_US)
				.addRole(PermisionChecker.SALES_CN).permissionCheck();
		
		if(user != null){
			
			return CoinUserLocalServiceUtil.updateDoc(user.getUserId(), coinuserId, docStr);
		}else{
			return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
		}
		
	}
	
	public JSONObject getCoinUser(String coinUserId){
		
		//Maybe more
		User sale_user = new PermisionChecker().addRole(PermisionChecker.SALES_US)
				.addRole(PermisionChecker.SALES_CN).permissionCheck();
		
		User op_user = new PermisionChecker().addRole(PermisionChecker.OPERATOR).addRole(PermisionChecker.ACCOUNTING)
				.addRole(PermisionChecker.SALES_GL_MNG).permissionCheck();
		
		if(op_user != null){
			CoinUser cu = null;
			try {
				cu = CoinUserUtil.fetchByPrimaryKey(coinUserId);
			} catch (SystemException e) {
				e.printStackTrace();
				DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
			}
			return DataUtil.getResponse(DataUtil.getUserJson(cu), null, ErrorConstants.SUCCESS);
		}else if(sale_user != null){
			CoinUser cu = null;
			try {
				cu = CoinUserUtil.fetchByPrimaryKey(coinUserId);
				//if not sale's user and not admin, return null
				if(cu.getAddUser() != sale_user.getUserId() && null == new PermisionChecker().permissionCheck()){
					cu = null;
				}
			} catch (SystemException e) {
				e.printStackTrace();
				DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
			}
			return DataUtil.getResponse(DataUtil.getUserJson(cu), null, ErrorConstants.SUCCESS);
		}else{
			return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
		}
	}
	
	//get all validated users
	public JSONObject getValidatedCoinUsers(){
		
		//TODO Maybe more
		User user = new PermisionChecker().addRole(PermisionChecker.SALES_GL_MNG)
				.addRole(PermisionChecker.OPERATOR).permissionCheck();
		
		if(user != null){
			
			try {
				List<CoinUser> list = CoinUserUtil.findByvalidateFlag("Y", "Y");
				
				return DataUtil.getResponse(null, DataUtil.getUserJsonArray(list), ErrorConstants.SUCCESS);
			} catch (SystemException e) {
				return DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
			}
		}else{
			return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
		}
	}
	
	public JSONObject getSaleCoinUsers(){
		
		//TODO Maybe more
		User user = new PermisionChecker().addRole(PermisionChecker.SALES_US)
				.addRole(PermisionChecker.SALES_CN).permissionCheck();
		
		User op_user = new PermisionChecker().addRole(PermisionChecker.SALES_GL_MNG)
				.addRole(PermisionChecker.OPERATOR).permissionCheck();
		
		User admin_user = new PermisionChecker().permissionCheck();
		
		if(admin_user != null){
			try {
				List<CoinUser> list = CoinUserUtil.findByactiveFlag("Y");
				return DataUtil.getResponse(null, DataUtil.getUserJsonArray(list), ErrorConstants.SUCCESS);
			} catch (SystemException e) {
				return DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
			}
		}else if(op_user != null){//return all validated for operators
			try {
				//TODO verify what to show later
				List<CoinUser> list = CoinUserUtil.findByactiveFlag("Y");//CoinUserUtil.findByvalidateFlag("N");
				return DataUtil.getResponse(null, DataUtil.getUserJsonArray(list), ErrorConstants.SUCCESS);
			} catch (SystemException e) {
				return DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
			}
		}else if(user != null){//return only he/she created users
			
			try {
				List<CoinUser> list = CoinUserUtil.findBysales(user.getUserId(), "Y");
				return DataUtil.getResponse(null, DataUtil.getUserJsonArray(list), ErrorConstants.SUCCESS);
			} catch (SystemException e) {
				return DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
			}
		}else{
			return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
		}
	}
	
	public JSONObject deactivateCoinUser(String coinUserId){//change activeFlag to N  //TODO
		
		User aUser = new PermisionChecker().permissionCheck();
		
		User sUser = new PermisionChecker().addRole(PermisionChecker.SALES_US)
				.addRole(PermisionChecker.SALES_CN).permissionCheck();
		
		if(aUser != null){
			//is admin, do the force deactivate
			if(CoinUserLocalServiceUtil.deactivateUser(coinUserId)){
				return DataUtil.getResponse(null, null, ErrorConstants.SUCCESS);
			}else{
				return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
			}
		}
		
		if(sUser != null){		
			if(CoinUserLocalServiceUtil.deactivateSaleUser(sUser.getUserId(), coinUserId)){
				return DataUtil.getResponse(null, null, ErrorConstants.SUCCESS);
			}else{
				return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
			}
		}else{
			return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
		}
	}
	
	public JSONObject verifyCoinUser(String coinUserId){//change validatedFlag to Y 
		//Maybe more
		User user = new PermisionChecker().addRole(PermisionChecker.SALES_US)
				.addRole(PermisionChecker.SALES_CN).permissionCheck();
		
		if(user != null){		
			if(CoinUserLocalServiceUtil.checkValidtion(coinUserId)){
				return DataUtil.getResponse(null, null, ErrorConstants.SUCCESS);
			}else{
				return DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
			}
		}else{
			return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
		}
	}
	
	public JSONObject verifyAcredittedCoinUser(String coinUserId){//change validatedFlag to Y 
		//Maybe more
		User user = new PermisionChecker().addRole(PermisionChecker.OPERATOR).addRole(PermisionChecker.HR).permissionCheck();
		
		if(user != null){		
			if(CoinUserLocalServiceUtil.verifyAcredit(coinUserId)){
				return DataUtil.getResponse(null, null, ErrorConstants.SUCCESS);
			}else{
				return DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
			}
		}else{
			return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
		}
	}
	
	public JSONObject pendAcredittedCoinUser(String coinUserId){//change validatedFlag to P 
		//Maybe more
		User user = new PermisionChecker().addRole(PermisionChecker.OPERATOR).addRole(PermisionChecker.HR).permissionCheck();
		
		if(user != null){		
			if(CoinUserLocalServiceUtil.pendAcredit(coinUserId)){
				return DataUtil.getResponse(null, null, ErrorConstants.SUCCESS);
			}else{
				return DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
			}
		}else{
			return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
		}
	}
	
//	public long addBroker(String lics, String country){
//		return BrokerInfoLocalServiceUtil.addBroker(lics, country);
//	}
	
	public JSONObject getBroker(long id){
		
		User user = new PermisionChecker().addRole(PermisionChecker.SALES_US)
				.addRole(PermisionChecker.SALES_CN).permissionCheck();
		
		if(user != null){
			return BrokerInfoLocalServiceUtil.getBroker(id);
		}else{
			return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
		}
		
	}
	
//	public String updatedBroker(){
//		return BrokerInfoLocalServiceUtil.updatedBroker();
//	}
	
	public JSONObject addFund(String name,
			long phone, int areaCode, String email, String wechat, String facebook, String twitter, String weibo
			, String street1, String street2, String city, String state, String country, String zipCode){
		
		User user = new PermisionChecker().addRole(PermisionChecker.SALES_US)
				.addRole(PermisionChecker.SALES_CN).permissionCheck();
		if(user == null){
			return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
		}
		return FundInfoLocalServiceUtil.addFund(name, phone, areaCode, email, 
				wechat, facebook, twitter, weibo, street1, street2, city, state, country, zipCode);
		
	}
	
	public JSONObject getFund(long fundId){
		
		User user = new PermisionChecker().addRole(PermisionChecker.SALES_US)
				.addRole(PermisionChecker.SALES_CN).addRole(PermisionChecker.OPERATOR)
				.addRole(PermisionChecker.ACCOUNTING).permissionCheck();
		
		if(user != null){
			FundInfo fi = FundInfoLocalServiceUtil.getFund(fundId);			
			return DataUtil.getResponse(DataUtil.getFundJson(fi), null, ErrorConstants.SUCCESS);
		}else{
			return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
		}
		
				
	}
	
	public JSONObject getFunds(){
		
		User user = new PermisionChecker().addRole(PermisionChecker.SALES_US)
				.addRole(PermisionChecker.SALES_CN).addRole(PermisionChecker.OPERATOR)
				.addRole(PermisionChecker.ACCOUNTING).permissionCheck();
		
		if(user != null){
			try {
				List<FundInfo> list = FundInfoUtil.findAll();
				if(list != null && ! list.isEmpty()){
					JSONArray ja = JSONFactoryUtil.createJSONArray();
					for(FundInfo fi : list){
						ja.put(DataUtil.getFundJson(fi));
					}
					return DataUtil.getResponse(null, ja, ErrorConstants.SUCCESS);
				}else{
					return null;
				}
				
			} catch (SystemException e) {
				
				e.printStackTrace();
				return null;
			}
		}else{
			return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
		}
		
		
	}
	
	public JSONObject getContact(long contactId){
		
		User user = new PermisionChecker().addRole(PermisionChecker.SALES_US)
				.addRole(PermisionChecker.SALES_CN).addRole(PermisionChecker.OPERATOR)
				.addRole(PermisionChecker.ACCOUNTING).addRole(PermisionChecker.SALES_GL_MNG).permissionCheck();
		
		if(user != null){
			return DataUtil.getResponse(AaContactLocalServiceUtil.getContact(contactId), null, ErrorConstants.SUCCESS);
		}else{
			return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
		}		
	}
	
	public String getImage(String userId){
		
		User opUser = new PermisionChecker().addRole(PermisionChecker.OPERATOR).addRole(PermisionChecker.SALES_GL_MNG)
				.addRole(PermisionChecker.ACCOUNTING).permissionCheck();
		
		User user = new PermisionChecker().addRole(PermisionChecker.SALES_US)
				.addRole(PermisionChecker.SALES_CN).permissionCheck();
		
		
		String result = null;
		if(user != null || opUser != null){			
			try {
				CoinUser cu = CoinUserUtil.fetchByPrimaryKey(userId);
				
				if(opUser == null && user.getUserId() != cu.getAddUser() ){
					return null;
				}
				
				if(cu != null){
					ImageStore is = ImageStoreUtil.fetchByPrimaryKey(cu.getPhotoImage());
					
					if(is == null){
						return null;
					}
					
					Blob image=is.getContent();//DatatypeConverter.printBase64Binary(codes)		
		            byte[ ] imgData = image.getBytes(1,(int)image.length());
		            result = Base64.encodeBase64String(imgData);
		         
				}			          
				
			} catch (SystemException e) {			
				e.printStackTrace();
			} catch (SQLException e) {			
				e.printStackTrace();
			}

		}
		return result;				
	}
	
	public String getCertDoc(String userId){
		
		User user = new PermisionChecker().addRole(PermisionChecker.SALES_US)
				.addRole(PermisionChecker.SALES_CN).addRole(PermisionChecker.OPERATOR)
				.addRole(PermisionChecker.ACCOUNTING).permissionCheck();
				
		String result = null;
		if(user != null){
			try {
				CoinUser cu = CoinUserUtil.fetchByPrimaryKey(userId);
				if(cu != null){
					CertDocStore ds = CertDocStoreUtil.fetchByPrimaryKey(cu.getDocId());
					
					if(ds == null){
						return null;
					}
					
					Blob doc = ds.getContent();//DatatypeConverter.printBase64Binary(codes)		
		            byte[ ] docData = doc.getBytes(1,(int)doc.length());
		            result = Base64.encodeBase64String(docData);
		         
				}			          
				
			} catch (SystemException e) {			
				e.printStackTrace();
			} catch (SQLException e) {			
				e.printStackTrace();
			}
		}	
		return result;
	}
	
//	public File getImageFile(long imageId) throws IOException{
//		
//		File f = null;
//		try {
//			ImageStore is = ImageStoreUtil.fetchByPrimaryKey(imageId);
//			
//			Blob image=is.getContent();//DatatypeConverter.printBase64Binary(codes)
//            byte[ ] imgData = image.getBytes(1,(int)image.length());
//            OutputStream o = new FileOutputStream(f);
//            o.write(imgData);
//            o.flush();
//            o.close();          
//			
//		} catch (SystemException e) {			
//			e.printStackTrace();
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return f;
//	}
	
//	public long addImage(File f) throws SystemException, IOException{
//		
//		long id = CounterLocalServiceUtil.increment(ImageStore.class.getName());
//		
//		ImageStore ims = ImageStoreUtil.create(id);
//		
//		//create blob 
//		InputStream fis =new FileInputStream(f);
//		byte[] bytes = new byte[(int) f.length()];
//		fis.read(bytes);
//		fis.close();
//		UnsyncByteArrayInputStream unsyncByteArrayInputStream =new UnsyncByteArrayInputStream(bytes);
//		OutputBlob dataOutputBlob = new OutputBlob(unsyncByteArrayInputStream, bytes.length);
//		
//		ims.setContent(dataOutputBlob);
//		ims.setActiveFlag("Y");
//		ims.setAddDate(new Date());
//		
//		ims.persist();
//		
//		return ims.getImageId();
//	}
	
//	public String updateFund(){
//		return FundInfoLocalServiceUtil.updateFund();
//	}
}