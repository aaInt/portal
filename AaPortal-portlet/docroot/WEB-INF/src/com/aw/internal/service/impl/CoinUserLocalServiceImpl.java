/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.impl;

import java.util.Date;
import java.util.List;

import com.aw.internal.model.AaContact;
import com.aw.internal.model.CertDocStore;
import com.aw.internal.model.CoinUser;
import com.aw.internal.model.ImageStore;
import com.aw.internal.service.AaContactLocalServiceUtil;
import com.aw.internal.service.BrokerInfoLocalServiceUtil;
import com.aw.internal.service.CertDocStoreLocalServiceUtil;
import com.aw.internal.service.FundInfoLocalServiceUtil;
import com.aw.internal.service.ImageStoreLocalServiceUtil;
import com.aw.internal.service.base.CoinUserLocalServiceBaseImpl;
import com.aw.internal.service.persistence.AaContactUtil;
import com.aw.internal.service.persistence.CoinUserUtil;
import com.aw.internal.service.util.CommonUtil;
import com.aw.internal.service.util.DataUtil;
import com.aw.internal.service.util.EncryptionUtil;
import com.aw.internal.service.util.ErrorConstants;
import com.aw.internal.service.util.PermisionChecker;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;


/**
 * The implementation of the coin user local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.liferay.sample.service.CoinUserLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author zl
 * @see com.aw.internal.service.base.CoinUserLocalServiceBaseImpl
 * @see com.liferay.sample.service.CoinUserLocalServiceUtil
 */
public class CoinUserLocalServiceImpl extends CoinUserLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.liferay.sample.service.CoinUserLocalServiceUtil} to access the coin user local service.
	 */
	public static final String BROKER = "BRK";
	public static final String FUND = "FND";
	public static final String INDIVIDUAL = "IDV";
	
	public JSONObject addCoinUser(long creater, String firstName, String lastName, String birthday, String userType
			, String brkLics, String brkCountry, long fundId//parse 0 if not a fund
			, String nationality, String idIssueCountry, String idType, String idNumber, String photoImage
			, String certDocStr, String ssn, String ein, String income, String asset, long phone, int areaCode
			, String email, String wechat, String facebook, String twitter, String weibo, String street1
			, String street2, String city, String state, String country, String zipCode
			, String isMailDiff, String mailStreet1, String mailStreet2, String mailCity, String mailState
			, String mailCountry, String mailZipCode){			
		
		if(existCheck(email, firstName, lastName, idNumber)){
			return DataUtil.getResponse(null, null, ErrorConstants.DUPLI_EMAIL);
		}
		
		long contactId = 0;
		//long coinUserId = 0;
		try {
			contactId = CounterLocalServiceUtil.increment(AaContact.class.getName());
			//coinUserId = CounterLocalServiceUtil.increment(FundInfo.class.getName());
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
		}
		
		if(AaContactLocalServiceUtil.createContact(contactId, phone, areaCode, email, wechat, facebook
				, twitter, weibo, street1, street2, city, state, country, zipCode, isMailDiff
				, mailStreet1, mailStreet2, mailCity, mailState, mailCountry, mailZipCode)){
			
			//create entity                  gmt YYMMDDHHMMss    +  creater
			CoinUser cu;
			if(nationality.equalsIgnoreCase("NAEX")){
				cu = CoinUserUtil.create("0000"+CommonUtil.getTimeString());
			}else{
				cu = CoinUserUtil.create("U"+creater+"AT"+CommonUtil.getTimeString());
			}
			
			
			cu.setFirstName(firstName);
			cu.setLastName(lastName);
			cu.setActiveFlag("Y");
			cu.setBirthday(birthday);
			cu.setContactId(contactId);
			
			
			if(INDIVIDUAL.equals(userType)){
				
				cu.setUserType(userType);
				
			}else if(FUND.equals(userType)){
				if(!FundInfoLocalServiceUtil.verifyFund(fundId)){
					return DataUtil.getResponse(null, null, ErrorConstants.INVALID_FUND_INFO);
				}
				cu.setUserType(userType);
				cu.setFundId(fundId);
				
			}else if(BROKER.equals(userType)){
				
				cu.setUserType(userType);
				long brkId = BrokerInfoLocalServiceUtil.addBroker(brkLics, brkCountry);
				if(brkId > 0){
					cu.setBrokerId(brkId);
				}else{
					return DataUtil.getResponse(null, null, ErrorConstants.INVALID_BROKER_INFO);
				}				
				
			}else{
				return DataUtil.getResponse(null, null, ErrorConstants.INVALID_USR_TYP);
			}
			
			cu.setNationality(nationality);
			cu.setIdIssueCountry(idIssueCountry);
			cu.setIdType(idType);
			cu.setIdNumber(idNumber);
			if(ssn != null){
				cu.setSsn(EncryptionUtil.encrypt(ssn));
			}
			if(ein != null){
				cu.setEin(EncryptionUtil.encrypt(ein));
			}
			
			long imageId = saveImage(photoImage);
			long docId = saveDoc(certDocStr);

			cu.setPhotoImage(imageId);
			cu.setDocId(docId);
			cu.setIncome(income);
			cu.setAsset(asset);
			cu.setIsValidated("N");
			cu.setIsCreddited("N");
			cu.setAddTime(new Date());
			cu.setAddUser(creater);

			try {
				cu.persist();
				return DataUtil.getResponse(null, null, ErrorConstants.SUCCESS);
			} catch (SystemException e) {
				e.printStackTrace();
				return DataUtil.getResponse(null, null, ErrorConstants.INVALID_USR_INFO);
			}
			
		}else{
			return DataUtil.getResponse(null, null, ErrorConstants.INVALID_CONTACT_INFO);
		}				
	}
	
	private boolean existCheck(String email, String firstName, String lastName, String id){
		boolean isExist = false;
		
		try{
			List<AaContact> clist = AaContactUtil.findByemail(email);
			if(clist != null && !clist.isEmpty()){
				isExist = true;
			}
			List<CoinUser> culist = CoinUserUtil.findBynameId(firstName, lastName, id);
			if(culist != null && !culist.isEmpty()){
				isExist = true;
			}
		}catch(SystemException e){
			//do nothing then
		}	
		return isExist;
	}
	
	public JSONObject updateCoinUser(long userId, String coinUserId, String firstName, String lastName, String birthday, String userType
			, String brkLics, String brkCountry, long fundId
			, String nationality, String idIssueCountry, String idType, String idNumber, String ssn, String ein
			, String income, String asset, long phone, int areaCode, String email, String wechat
			, String facebook, String twitter, String weibo, String street1, String street2, String city
			, String state, String country, String zipCode, String isMailDiff, String mailStreet1, String mailStreet2, String mailCity, String mailState
			, String mailCountry, String mailZipCode){
		
		try {
			CoinUser cu = CoinUserUtil.fetchByPrimaryKey(coinUserId);
			if(cu != null){
				if(userId != cu.getAddUser() || "Y".equals(cu.getIsValidated())){
					return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
				}
				
				cu.setFirstName(firstName);
				cu.setLastName(lastName);		
				cu.setBirthday(birthday);

				//update contact
				if(!AaContactLocalServiceUtil.updateContact(cu.getContactId(), phone, areaCode, email, wechat
						, facebook, twitter, weibo, street1, street2, city, state, country, zipCode, isMailDiff
						, mailStreet1, mailStreet2, mailCity, mailState, mailCountry, mailZipCode)){
					return DataUtil.getResponse(null, null, ErrorConstants.INVALID_CONTACT_INFO);
				}
				
				if(INDIVIDUAL.equals(userType)){
					
					cu.setUserType(userType);
					
				}else if(FUND.equals(userType)){
					if(!FundInfoLocalServiceUtil.verifyFund(fundId)){
						return DataUtil.getResponse(null, null, ErrorConstants.INVALID_FUND_INFO);
					}
					cu.setUserType(userType);
					cu.setFundId(fundId);
					
				}else if(BROKER.equals(userType)){
					
					cu.setUserType(userType);
					long brkId = BrokerInfoLocalServiceUtil.addBroker(brkLics, brkCountry);
					if(brkId > 0){
						cu.setBrokerId(brkId);
					}else{
						return DataUtil.getResponse(null, null, ErrorConstants.INVALID_BROKER_INFO);
					}				
					
				}else{
					return DataUtil.getResponse(null, null, ErrorConstants.INVALID_USR_TYP);
				}
				
				cu.setNationality(nationality);
				cu.setIdIssueCountry(idIssueCountry);
				cu.setIdType(idType);
				cu.setIdNumber(idNumber);
				
				if(ssn != null){
					cu.setSsn(EncryptionUtil.encrypt(ssn));
				}
				if(ein != null){
					cu.setEin(EncryptionUtil.encrypt(ein));
				}
				
				cu.setIncome(income);
				cu.setAsset(asset);							
				
				cu.persist();
				return DataUtil.getResponse(null, null, ErrorConstants.SUCCESS);
				
			}else{
				return DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
			}
		} catch (SystemException e) {
			e.printStackTrace();
			return DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
		}
		
	}
	
	public JSONObject updatePhoto(long userId, String coinuserId, String photoStr){
		
		try {
			CoinUser cu = CoinUserUtil.fetchByPrimaryKey(coinuserId);
			if(cu != null){
				if(userId != cu.getAddUser() || "Y".equals(cu.getIsValidated())){
					return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
				}
				long imageId = saveImage(photoStr);
				if(imageId > 0){
					cu.setPhotoImage(imageId);
				}
				
				cu.persist();
				return DataUtil.getResponse(null, null, ErrorConstants.SUCCESS);
				
			}else{
				return DataUtil.getResponse(null, null, ErrorConstants.USER_NOT_VERIFIED);
			}
		} catch (SystemException e) {
			e.printStackTrace();
			return DataUtil.getResponse(null, null, ErrorConstants.INVALID_IMAGE_INFO);
		}		
		
	}
	
	public JSONObject updateDoc(long userId, String coinuserId, String docStr){
		
		try {
			CoinUser cu = CoinUserUtil.fetchByPrimaryKey(coinuserId);
			if(cu != null){
				if(userId != cu.getAddUser() || "Y".equals(cu.getIsValidated())){
					return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
				}
				long docId = saveDoc(docStr);
				if(docId > 0){
					cu.setDocId(docId);
				}
				
				cu.persist();
				return DataUtil.getResponse(null, null, ErrorConstants.SUCCESS);
				
			}else{
				return DataUtil.getResponse(null, null, ErrorConstants.USER_NOT_VERIFIED);
			}
		} catch (SystemException e) {
			e.printStackTrace();
			return DataUtil.getResponse(null, null, ErrorConstants.INVALID_IMAGE_INFO);
		}		
		
	}
	
	private long saveImage(String base64){
		long id = -1;
		
		if(base64 == null || base64.length() < 50){
			return id;
		}
		try {
			ImageStore is = ImageStoreLocalServiceUtil.addImage(base64);
			if(is != null){
				id = is.getImageId();
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		return id;
	}
	
	private long saveDoc(String base64){
		long id = -1;
		
		if(base64 == null || base64.length() < 50){
			return id;
		}
		try {
			CertDocStore ds = CertDocStoreLocalServiceUtil.addDoc(base64);
			if(ds != null){
				id = ds.getDocId();
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		return id;
	}
	
	public boolean deactivateSaleUser(long userId, String id){
		boolean success = false;
		
		try {
			CoinUser cu = CoinUserUtil.fetchByPrimaryKey(id);
			
			//only can deactive not-validated and self's user
			if(cu != null && "N".equals(cu.getIsValidated()) && userId == cu.getAddUser()){
				cu.setActiveFlag("N");
				cu.persist();
				success = true;
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}		
		
		return success;
	}
	
	//force deactivate
	public boolean deactivateUser(String id){
		boolean success = false;
		
		try {
			CoinUser cu = CoinUserUtil.fetchByPrimaryKey(id);
						
			if(cu != null){
				cu.setActiveFlag("N");
				cu.persist();
				success = true;
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}		
		
		return success;
	}
	
	public boolean verifyAcredit(String id){
		boolean isVerified = false;
		
		try {
			CoinUser cu = CoinUserUtil.fetchByPrimaryKey(id);
			if(cu != null){
				cu.setIsCreddited("Y");;
				cu.persist();
				isVerified = true;
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}		
		
		return isVerified;
	}
	
	public boolean pendAcredit(String id){
		boolean isVerified = false;
		
		try {
			CoinUser cu = CoinUserUtil.fetchByPrimaryKey(id);
			if(cu != null){
				cu.setIsCreddited("P");;
				cu.persist();
				isVerified = true;
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}		
		
		return isVerified;
	}
	
	//
	public boolean checkValidtion(String id){
		boolean isVerified = false;
		
		try {
			CoinUser cu = CoinUserUtil.fetchByPrimaryKey(id);
			if(cu != null && validationPreReq(cu)){
				cu.setIsValidated("Y");
				cu.persist();
				isVerified = true;
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}		
		
		return isVerified;
	}
	
	//TODO add later
	private boolean validationPreReq(CoinUser cu){
		if(!(cu.getPhotoImage() > 0)){
			return false;
		}else{
			return true;
		}
	}
	
	//check if the user is validated for purchasing validatedFlag=Y and activeFlag=Y
	public boolean validateUser(String id, long userId){
		boolean isValid = false;
		
		// && userId == cu.getAddUser()
		//TODO figure out later to see if only can place order for self's user
		
		//see if is admin adding a staff order
		try{
			long empId = Long.parseLong(id);
			User user = new PermisionChecker().addRole(PermisionChecker.HR).permissionCheck();
			if(user != null){//is admin or HR
				User empUser = UserLocalServiceUtil.fetchUser(empId);
				if(empUser != null){
					isValid = true;
				}
			}
		}catch(NumberFormatException e){
			//then go to the regular validation
		} catch (SystemException e) {			
			e.printStackTrace();
			return false;
		}
		
		try {
			CoinUser cu = CoinUserUtil.fetchByPrimaryKey(id);
			if(cu != null){
				if("Y".equals(cu.getIsValidated()) && "Y".equals(cu.getActiveFlag())){
					isValid = true;
				}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}		
		
		return isValid;
	}
	
//	public void saveImage(String base64Str){
//		
//		byte[] bytes = Base64.decodeBase64(base64Str);
//		
//		InputStream input = new ByteArrayInputStream(bytes);
//	}
}