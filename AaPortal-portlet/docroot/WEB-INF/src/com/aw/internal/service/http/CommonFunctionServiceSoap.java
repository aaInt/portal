/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.http;

import com.aw.internal.service.CommonFunctionServiceUtil;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.rmi.RemoteException;

/**
 * Provides the SOAP utility for the
 * {@link com.aw.internal.service.CommonFunctionServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it is difficult for SOAP to
 * support certain types.
 *
 * <p>
 * The benefits of using the SOAP utility is that it is cross platform
 * compatible. SOAP allows different languages like Java, .NET, C++, PHP, and
 * even Perl, to call the generated services. One drawback of SOAP is that it is
 * slow because it needs to serialize all calls into a text format (XML).
 * </p>
 *
 * <p>
 * You can see a list of services at http://localhost:8080/api/axis. Set the
 * property <b>axis.servlet.hosts.allowed</b> in portal.properties to configure
 * security.
 * </p>
 *
 * <p>
 * The SOAP utility is only generated for remote services.
 * </p>
 *
 * @author zl
 * @see CommonFunctionServiceHttp
 * @see com.aw.internal.service.CommonFunctionServiceUtil
 * @generated
 */
public class CommonFunctionServiceSoap {
	public static boolean checkPassword(java.lang.String password)
		throws RemoteException {
		try {
			boolean returnValue = CommonFunctionServiceUtil.checkPassword(password);

			return returnValue;
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getCoinCounts() throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CommonFunctionServiceUtil.getCoinCounts();

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static boolean endCoinCountPhase(int endPhase, int toPhase)
		throws RemoteException {
		try {
			boolean returnValue = CommonFunctionServiceUtil.endCoinCountPhase(endPhase,
					toPhase);

			return returnValue;
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getPhasePrice(java.lang.String currency,
		java.lang.String userId) throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CommonFunctionServiceUtil.getPhasePrice(currency,
					userId);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getUserInfo() throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CommonFunctionServiceUtil.getUserInfo();

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static boolean updatePassword(java.lang.String origin,
		java.lang.String new1, java.lang.String new2) throws RemoteException {
		try {
			boolean returnValue = CommonFunctionServiceUtil.updatePassword(origin,
					new1, new2);

			return returnValue;
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	private static Log _log = LogFactoryUtil.getLog(CommonFunctionServiceSoap.class);
}