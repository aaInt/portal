/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.aw.internal.model.FutureUser;
import com.aw.internal.service.FutureUserLocalServiceUtil;
import com.aw.internal.service.base.FutureUserServiceBaseImpl;
import com.aw.internal.service.persistence.AaContactUtil;
import com.aw.internal.service.persistence.FutureUserUtil;
import com.aw.internal.service.util.DataUtil;
import com.aw.internal.service.util.ErrorConstants;
import com.aw.internal.service.util.PermisionChecker;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.model.User;
import com.liferay.portal.security.ac.AccessControlled;

/**
 * The implementation of the future user remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.liferay.sample.service.FutureUserService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author zl
 * @see com.aw.internal.service.base.FutureUserServiceBaseImpl
 * @see com.liferay.sample.service.FutureUserServiceUtil
 */
public class FutureUserServiceImpl extends FutureUserServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.liferay.sample.service.FutureUserServiceUtil} to access the future user remote service.
	 */
	@AccessControlled(guestAccessEnabled=true)
	public JSONObject recordUser(String email, String firstN, String lastN, String residency, int areaCode, long phone){
		FutureUser fu = null;
		try {
			fu = FutureUserLocalServiceUtil.fetchFutureUser(email);
		} catch (SystemException e) {
			e.printStackTrace();
		}
		if(fu != null){
			return DataUtil.getResponse(null, null, ErrorConstants.DUPLI_EMAIL);
		}else{
			fu = FutureUserLocalServiceUtil.createFutureUser(email);
			fu.setFirstName(firstN);
			fu.setLastName(lastN);
			fu.setAreaCode(areaCode);
			fu.setPhone(phone);
			fu.setResidency(residency);
			fu.setActiveFlag("Y");
			fu.setAddTime(new Date());
			fu.setTakenFlag("N");
			fu.setOwnerId(-1);
			try {
				fu.persist();				
			} catch (SystemException e) {
				e.printStackTrace();
				return DataUtil.getResponse(null, null, ErrorConstants.INVALID_USR_INFO);
			}
			return DataUtil.getResponse(null, null, ErrorConstants.SUCCESS);
		}
	}
	
	public JSONObject takeUser(String email){
		
		
		User user = new PermisionChecker().addRole(PermisionChecker.SALES_US)
				.addRole(PermisionChecker.SALES_CN).permissionCheck();
		
		if(user != null){
			
			if(checkFull(user.getUserId())){
				return DataUtil.getResponse(null, null, ErrorConstants.TAKEN_REACH_LIMIT);
			}
			
			FutureUser fu = null;
			try {
				fu = FutureUserLocalServiceUtil.fetchFutureUser(email);
				if(fu != null && "Y".equals(fu.getActiveFlag())){
					fu.setTakenFlag("Y");
					fu.setOwnerId(user.getUserId());
					fu.persist();
					return DataUtil.getResponse(null, null, ErrorConstants.SUCCESS);
				}else{
					return DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
				}
			} catch (SystemException e) {
				e.printStackTrace();
				return DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
			}
		}else{
			return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
		}				
	}
	
	public JSONObject releaseTaken(String email){
		
		PermisionChecker pc = new PermisionChecker();
		User user = pc.addRole(PermisionChecker.SALES_US)
				.addRole(PermisionChecker.SALES_CN).permissionCheck();
		
		if( user != null){
			FutureUser fu;
			try {
				fu = FutureUserUtil.fetchByPrimaryKey(email);
				
				if(fu != null && (fu.getOwnerId() == user.getUserId() || null != pc.permissionCheck(PermisionChecker.ADMIN))){
					fu.setTakenFlag("N");
					fu.setOwnerId(-1);
					fu.persist();
					return DataUtil.getResponse(null, null, ErrorConstants.SUCCESS);
				}else{
					return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
				}
			} catch (SystemException e) {
				
				e.printStackTrace();
				return DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
			}
			
		}else{
			return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
		}
	}
	
	private boolean checkFull(long userId){
		boolean isFull = false;
						
		try {
			List<FutureUser> list = FutureUserUtil.findBymytaken("Y", userId);
			if(list != null && list.size() > 2){
				isFull = true;
			}
		} catch (SystemException e) {				
			e.printStackTrace();			
		}
		return isFull;
	}
	
	//all active and not taken users
	public JSONObject getAllActiveFutureUsers(){
		
		//TODO
		//check the sale's region and only show region users for TAs
		
//		if(null == new PermisionChecker().permissionCheck(PermisionChecker.SALES)){
//			return null;
//		}
		
		Set<String> roles = new PermisionChecker().getRolesForUser();
		
		Set<String> countries = new HashSet<String>();
		
		if(roles.contains(PermisionChecker.ADMIN) || roles.contains(PermisionChecker.OPERATOR)){
//			countries.add("US");
//			countries.add("CN");
		}else if(roles.contains(PermisionChecker.SALES_US)){
//			countries.add("US");
//			countries.add("CN");
		}else if(roles.contains(PermisionChecker.SALES_CN)){
			countries.add("CN");
		}else if(roles.contains(PermisionChecker.SALES_JP)){
			countries.add("JP");
		}else if(roles.contains(PermisionChecker.SALES_HK)){
			countries.add("HK");
		}else{
			return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
		}
		
		
		List<FutureUser> list = null;
		List<FutureUser> finalList = null;
		try {
			list = FutureUserUtil.findByactiveUser("Y", "N");
			if(list != null){
				finalList = new ArrayList<FutureUser>();
				if(countries.isEmpty()){// no filter then add all
					finalList = list;
				}else{
					for(FutureUser fu : list){
						if(countries.contains(fu.getResidency())){
							finalList.add(fu);
						}
					}
				}
				
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return DataUtil.getResponse(null, DataUtil.getFutureUserListJson(finalList), ErrorConstants.SUCCESS);

	}
	
	
	public JSONObject getMyActiveFutureUsers(){
		
		User user = new PermisionChecker().addRole(PermisionChecker.SALES_US)
				.addRole(PermisionChecker.SALES_CN).permissionCheck();
		
		List<FutureUser> list = null;
		
		if(user != null){			
			try {
				list = FutureUserUtil.findBymytaken("Y", user.getUserId());
			} catch (SystemException e) {				
				e.printStackTrace();
			}			
		}else{
			return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
		}

		return DataUtil.getResponse(null, DataUtil.getFutureUserListJson(list), ErrorConstants.SUCCESS);
	}
	
	
	public JSONObject deactivateUser(String email){
		
		User user = new PermisionChecker().addRole(PermisionChecker.SALES_US)
				.addRole(PermisionChecker.SALES_CN).permissionCheck();
		
		if(null == user){
			return DataUtil.getResponse(null, null, ErrorConstants.NO_PERMISSION);
		}
		
		FutureUser fu = null;
		try {
			fu = FutureUserLocalServiceUtil.fetchFutureUser(email);
			
			if(fu != null && user.getUserId() == fu.getOwnerId()){
				fu.setActiveFlag("N");
				fu.persist();
				return DataUtil.getResponse(checkComplete(email), null, ErrorConstants.SUCCESS);
			}else{
				return DataUtil.getResponse(null, null, ErrorConstants.INVALID_OWNERSHIP);
			}
			
		} catch (SystemException e) {
			e.printStackTrace();
			return DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
		}
	}
	
	private JSONObject checkComplete(String email){
		JSONObject j = null;
		
		//TODO check if email exist in coinUser,
		//if yes then return null, add log later
		try {
			if(null == AaContactUtil.findByemail(email)){
				j = JSONFactoryUtil.createJSONObject();
				j.put("warning", "deactivated a not stored user");
			}
		} catch (SystemException e) {
			e.printStackTrace();
			j = JSONFactoryUtil.createJSONObject();
			j.put("warning", "deactivated a not stored user");
		}
		
		//if no, then return generate waring json
		
		return j;
	}
}