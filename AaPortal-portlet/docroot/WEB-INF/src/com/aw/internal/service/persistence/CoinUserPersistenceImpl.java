/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.persistence;

import com.aw.internal.NoSuchCoinUserException;
import com.aw.internal.model.CoinUser;
import com.aw.internal.model.impl.CoinUserImpl;
import com.aw.internal.model.impl.CoinUserModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the coin user service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author zl
 * @see CoinUserPersistence
 * @see CoinUserUtil
 * @generated
 */
public class CoinUserPersistenceImpl extends BasePersistenceImpl<CoinUser>
	implements CoinUserPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CoinUserUtil} to access the coin user persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CoinUserImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CoinUserModelImpl.ENTITY_CACHE_ENABLED,
			CoinUserModelImpl.FINDER_CACHE_ENABLED, CoinUserImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CoinUserModelImpl.ENTITY_CACHE_ENABLED,
			CoinUserModelImpl.FINDER_CACHE_ENABLED, CoinUserImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CoinUserModelImpl.ENTITY_CACHE_ENABLED,
			CoinUserModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_SALES = new FinderPath(CoinUserModelImpl.ENTITY_CACHE_ENABLED,
			CoinUserModelImpl.FINDER_CACHE_ENABLED, CoinUserImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBysales",
			new String[] {
				Long.class.getName(), String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SALES = new FinderPath(CoinUserModelImpl.ENTITY_CACHE_ENABLED,
			CoinUserModelImpl.FINDER_CACHE_ENABLED, CoinUserImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBysales",
			new String[] { Long.class.getName(), String.class.getName() },
			CoinUserModelImpl.ADDUSER_COLUMN_BITMASK |
			CoinUserModelImpl.ACTIVEFLAG_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_SALES = new FinderPath(CoinUserModelImpl.ENTITY_CACHE_ENABLED,
			CoinUserModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBysales",
			new String[] { Long.class.getName(), String.class.getName() });

	/**
	 * Returns all the coin users where addUser = &#63; and activeFlag = &#63;.
	 *
	 * @param addUser the add user
	 * @param activeFlag the active flag
	 * @return the matching coin users
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CoinUser> findBysales(long addUser, String activeFlag)
		throws SystemException {
		return findBysales(addUser, activeFlag, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the coin users where addUser = &#63; and activeFlag = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param addUser the add user
	 * @param activeFlag the active flag
	 * @param start the lower bound of the range of coin users
	 * @param end the upper bound of the range of coin users (not inclusive)
	 * @return the range of matching coin users
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CoinUser> findBysales(long addUser, String activeFlag,
		int start, int end) throws SystemException {
		return findBysales(addUser, activeFlag, start, end, null);
	}

	/**
	 * Returns an ordered range of all the coin users where addUser = &#63; and activeFlag = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param addUser the add user
	 * @param activeFlag the active flag
	 * @param start the lower bound of the range of coin users
	 * @param end the upper bound of the range of coin users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching coin users
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CoinUser> findBysales(long addUser, String activeFlag,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SALES;
			finderArgs = new Object[] { addUser, activeFlag };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_SALES;
			finderArgs = new Object[] {
					addUser, activeFlag,
					
					start, end, orderByComparator
				};
		}

		List<CoinUser> list = (List<CoinUser>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CoinUser coinUser : list) {
				if ((addUser != coinUser.getAddUser()) ||
						!Validator.equals(activeFlag, coinUser.getActiveFlag())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_COINUSER_WHERE);

			query.append(_FINDER_COLUMN_SALES_ADDUSER_2);

			boolean bindActiveFlag = false;

			if (activeFlag == null) {
				query.append(_FINDER_COLUMN_SALES_ACTIVEFLAG_1);
			}
			else if (activeFlag.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_SALES_ACTIVEFLAG_3);
			}
			else {
				bindActiveFlag = true;

				query.append(_FINDER_COLUMN_SALES_ACTIVEFLAG_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CoinUserModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(addUser);

				if (bindActiveFlag) {
					qPos.add(activeFlag);
				}

				if (!pagination) {
					list = (List<CoinUser>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CoinUser>(list);
				}
				else {
					list = (List<CoinUser>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first coin user in the ordered set where addUser = &#63; and activeFlag = &#63;.
	 *
	 * @param addUser the add user
	 * @param activeFlag the active flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching coin user
	 * @throws com.aw.internal.NoSuchCoinUserException if a matching coin user could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinUser findBysales_First(long addUser, String activeFlag,
		OrderByComparator orderByComparator)
		throws NoSuchCoinUserException, SystemException {
		CoinUser coinUser = fetchBysales_First(addUser, activeFlag,
				orderByComparator);

		if (coinUser != null) {
			return coinUser;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("addUser=");
		msg.append(addUser);

		msg.append(", activeFlag=");
		msg.append(activeFlag);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCoinUserException(msg.toString());
	}

	/**
	 * Returns the first coin user in the ordered set where addUser = &#63; and activeFlag = &#63;.
	 *
	 * @param addUser the add user
	 * @param activeFlag the active flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching coin user, or <code>null</code> if a matching coin user could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinUser fetchBysales_First(long addUser, String activeFlag,
		OrderByComparator orderByComparator) throws SystemException {
		List<CoinUser> list = findBysales(addUser, activeFlag, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last coin user in the ordered set where addUser = &#63; and activeFlag = &#63;.
	 *
	 * @param addUser the add user
	 * @param activeFlag the active flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching coin user
	 * @throws com.aw.internal.NoSuchCoinUserException if a matching coin user could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinUser findBysales_Last(long addUser, String activeFlag,
		OrderByComparator orderByComparator)
		throws NoSuchCoinUserException, SystemException {
		CoinUser coinUser = fetchBysales_Last(addUser, activeFlag,
				orderByComparator);

		if (coinUser != null) {
			return coinUser;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("addUser=");
		msg.append(addUser);

		msg.append(", activeFlag=");
		msg.append(activeFlag);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCoinUserException(msg.toString());
	}

	/**
	 * Returns the last coin user in the ordered set where addUser = &#63; and activeFlag = &#63;.
	 *
	 * @param addUser the add user
	 * @param activeFlag the active flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching coin user, or <code>null</code> if a matching coin user could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinUser fetchBysales_Last(long addUser, String activeFlag,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBysales(addUser, activeFlag);

		if (count == 0) {
			return null;
		}

		List<CoinUser> list = findBysales(addUser, activeFlag, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the coin users before and after the current coin user in the ordered set where addUser = &#63; and activeFlag = &#63;.
	 *
	 * @param userId the primary key of the current coin user
	 * @param addUser the add user
	 * @param activeFlag the active flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next coin user
	 * @throws com.aw.internal.NoSuchCoinUserException if a coin user with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinUser[] findBysales_PrevAndNext(String userId, long addUser,
		String activeFlag, OrderByComparator orderByComparator)
		throws NoSuchCoinUserException, SystemException {
		CoinUser coinUser = findByPrimaryKey(userId);

		Session session = null;

		try {
			session = openSession();

			CoinUser[] array = new CoinUserImpl[3];

			array[0] = getBysales_PrevAndNext(session, coinUser, addUser,
					activeFlag, orderByComparator, true);

			array[1] = coinUser;

			array[2] = getBysales_PrevAndNext(session, coinUser, addUser,
					activeFlag, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CoinUser getBysales_PrevAndNext(Session session,
		CoinUser coinUser, long addUser, String activeFlag,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_COINUSER_WHERE);

		query.append(_FINDER_COLUMN_SALES_ADDUSER_2);

		boolean bindActiveFlag = false;

		if (activeFlag == null) {
			query.append(_FINDER_COLUMN_SALES_ACTIVEFLAG_1);
		}
		else if (activeFlag.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_SALES_ACTIVEFLAG_3);
		}
		else {
			bindActiveFlag = true;

			query.append(_FINDER_COLUMN_SALES_ACTIVEFLAG_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CoinUserModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(addUser);

		if (bindActiveFlag) {
			qPos.add(activeFlag);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(coinUser);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CoinUser> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the coin users where addUser = &#63; and activeFlag = &#63; from the database.
	 *
	 * @param addUser the add user
	 * @param activeFlag the active flag
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeBysales(long addUser, String activeFlag)
		throws SystemException {
		for (CoinUser coinUser : findBysales(addUser, activeFlag,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(coinUser);
		}
	}

	/**
	 * Returns the number of coin users where addUser = &#63; and activeFlag = &#63;.
	 *
	 * @param addUser the add user
	 * @param activeFlag the active flag
	 * @return the number of matching coin users
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countBysales(long addUser, String activeFlag)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_SALES;

		Object[] finderArgs = new Object[] { addUser, activeFlag };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_COINUSER_WHERE);

			query.append(_FINDER_COLUMN_SALES_ADDUSER_2);

			boolean bindActiveFlag = false;

			if (activeFlag == null) {
				query.append(_FINDER_COLUMN_SALES_ACTIVEFLAG_1);
			}
			else if (activeFlag.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_SALES_ACTIVEFLAG_3);
			}
			else {
				bindActiveFlag = true;

				query.append(_FINDER_COLUMN_SALES_ACTIVEFLAG_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(addUser);

				if (bindActiveFlag) {
					qPos.add(activeFlag);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_SALES_ADDUSER_2 = "coinUser.addUser = ? AND ";
	private static final String _FINDER_COLUMN_SALES_ACTIVEFLAG_1 = "coinUser.activeFlag IS NULL";
	private static final String _FINDER_COLUMN_SALES_ACTIVEFLAG_2 = "coinUser.activeFlag = ?";
	private static final String _FINDER_COLUMN_SALES_ACTIVEFLAG_3 = "(coinUser.activeFlag IS NULL OR coinUser.activeFlag = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ACTIVEFLAG =
		new FinderPath(CoinUserModelImpl.ENTITY_CACHE_ENABLED,
			CoinUserModelImpl.FINDER_CACHE_ENABLED, CoinUserImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByactiveFlag",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ACTIVEFLAG =
		new FinderPath(CoinUserModelImpl.ENTITY_CACHE_ENABLED,
			CoinUserModelImpl.FINDER_CACHE_ENABLED, CoinUserImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByactiveFlag",
			new String[] { String.class.getName() },
			CoinUserModelImpl.ACTIVEFLAG_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ACTIVEFLAG = new FinderPath(CoinUserModelImpl.ENTITY_CACHE_ENABLED,
			CoinUserModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByactiveFlag",
			new String[] { String.class.getName() });

	/**
	 * Returns all the coin users where activeFlag = &#63;.
	 *
	 * @param activeFlag the active flag
	 * @return the matching coin users
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CoinUser> findByactiveFlag(String activeFlag)
		throws SystemException {
		return findByactiveFlag(activeFlag, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the coin users where activeFlag = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param activeFlag the active flag
	 * @param start the lower bound of the range of coin users
	 * @param end the upper bound of the range of coin users (not inclusive)
	 * @return the range of matching coin users
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CoinUser> findByactiveFlag(String activeFlag, int start, int end)
		throws SystemException {
		return findByactiveFlag(activeFlag, start, end, null);
	}

	/**
	 * Returns an ordered range of all the coin users where activeFlag = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param activeFlag the active flag
	 * @param start the lower bound of the range of coin users
	 * @param end the upper bound of the range of coin users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching coin users
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CoinUser> findByactiveFlag(String activeFlag, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ACTIVEFLAG;
			finderArgs = new Object[] { activeFlag };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ACTIVEFLAG;
			finderArgs = new Object[] { activeFlag, start, end, orderByComparator };
		}

		List<CoinUser> list = (List<CoinUser>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CoinUser coinUser : list) {
				if (!Validator.equals(activeFlag, coinUser.getActiveFlag())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_COINUSER_WHERE);

			boolean bindActiveFlag = false;

			if (activeFlag == null) {
				query.append(_FINDER_COLUMN_ACTIVEFLAG_ACTIVEFLAG_1);
			}
			else if (activeFlag.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ACTIVEFLAG_ACTIVEFLAG_3);
			}
			else {
				bindActiveFlag = true;

				query.append(_FINDER_COLUMN_ACTIVEFLAG_ACTIVEFLAG_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CoinUserModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindActiveFlag) {
					qPos.add(activeFlag);
				}

				if (!pagination) {
					list = (List<CoinUser>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CoinUser>(list);
				}
				else {
					list = (List<CoinUser>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first coin user in the ordered set where activeFlag = &#63;.
	 *
	 * @param activeFlag the active flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching coin user
	 * @throws com.aw.internal.NoSuchCoinUserException if a matching coin user could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinUser findByactiveFlag_First(String activeFlag,
		OrderByComparator orderByComparator)
		throws NoSuchCoinUserException, SystemException {
		CoinUser coinUser = fetchByactiveFlag_First(activeFlag,
				orderByComparator);

		if (coinUser != null) {
			return coinUser;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("activeFlag=");
		msg.append(activeFlag);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCoinUserException(msg.toString());
	}

	/**
	 * Returns the first coin user in the ordered set where activeFlag = &#63;.
	 *
	 * @param activeFlag the active flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching coin user, or <code>null</code> if a matching coin user could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinUser fetchByactiveFlag_First(String activeFlag,
		OrderByComparator orderByComparator) throws SystemException {
		List<CoinUser> list = findByactiveFlag(activeFlag, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last coin user in the ordered set where activeFlag = &#63;.
	 *
	 * @param activeFlag the active flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching coin user
	 * @throws com.aw.internal.NoSuchCoinUserException if a matching coin user could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinUser findByactiveFlag_Last(String activeFlag,
		OrderByComparator orderByComparator)
		throws NoSuchCoinUserException, SystemException {
		CoinUser coinUser = fetchByactiveFlag_Last(activeFlag, orderByComparator);

		if (coinUser != null) {
			return coinUser;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("activeFlag=");
		msg.append(activeFlag);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCoinUserException(msg.toString());
	}

	/**
	 * Returns the last coin user in the ordered set where activeFlag = &#63;.
	 *
	 * @param activeFlag the active flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching coin user, or <code>null</code> if a matching coin user could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinUser fetchByactiveFlag_Last(String activeFlag,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByactiveFlag(activeFlag);

		if (count == 0) {
			return null;
		}

		List<CoinUser> list = findByactiveFlag(activeFlag, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the coin users before and after the current coin user in the ordered set where activeFlag = &#63;.
	 *
	 * @param userId the primary key of the current coin user
	 * @param activeFlag the active flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next coin user
	 * @throws com.aw.internal.NoSuchCoinUserException if a coin user with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinUser[] findByactiveFlag_PrevAndNext(String userId,
		String activeFlag, OrderByComparator orderByComparator)
		throws NoSuchCoinUserException, SystemException {
		CoinUser coinUser = findByPrimaryKey(userId);

		Session session = null;

		try {
			session = openSession();

			CoinUser[] array = new CoinUserImpl[3];

			array[0] = getByactiveFlag_PrevAndNext(session, coinUser,
					activeFlag, orderByComparator, true);

			array[1] = coinUser;

			array[2] = getByactiveFlag_PrevAndNext(session, coinUser,
					activeFlag, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CoinUser getByactiveFlag_PrevAndNext(Session session,
		CoinUser coinUser, String activeFlag,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_COINUSER_WHERE);

		boolean bindActiveFlag = false;

		if (activeFlag == null) {
			query.append(_FINDER_COLUMN_ACTIVEFLAG_ACTIVEFLAG_1);
		}
		else if (activeFlag.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_ACTIVEFLAG_ACTIVEFLAG_3);
		}
		else {
			bindActiveFlag = true;

			query.append(_FINDER_COLUMN_ACTIVEFLAG_ACTIVEFLAG_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CoinUserModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindActiveFlag) {
			qPos.add(activeFlag);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(coinUser);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CoinUser> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the coin users where activeFlag = &#63; from the database.
	 *
	 * @param activeFlag the active flag
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByactiveFlag(String activeFlag) throws SystemException {
		for (CoinUser coinUser : findByactiveFlag(activeFlag,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(coinUser);
		}
	}

	/**
	 * Returns the number of coin users where activeFlag = &#63;.
	 *
	 * @param activeFlag the active flag
	 * @return the number of matching coin users
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByactiveFlag(String activeFlag) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_ACTIVEFLAG;

		Object[] finderArgs = new Object[] { activeFlag };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_COINUSER_WHERE);

			boolean bindActiveFlag = false;

			if (activeFlag == null) {
				query.append(_FINDER_COLUMN_ACTIVEFLAG_ACTIVEFLAG_1);
			}
			else if (activeFlag.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ACTIVEFLAG_ACTIVEFLAG_3);
			}
			else {
				bindActiveFlag = true;

				query.append(_FINDER_COLUMN_ACTIVEFLAG_ACTIVEFLAG_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindActiveFlag) {
					qPos.add(activeFlag);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ACTIVEFLAG_ACTIVEFLAG_1 = "coinUser.activeFlag IS NULL";
	private static final String _FINDER_COLUMN_ACTIVEFLAG_ACTIVEFLAG_2 = "coinUser.activeFlag = ?";
	private static final String _FINDER_COLUMN_ACTIVEFLAG_ACTIVEFLAG_3 = "(coinUser.activeFlag IS NULL OR coinUser.activeFlag = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_VALIDATEFLAG =
		new FinderPath(CoinUserModelImpl.ENTITY_CACHE_ENABLED,
			CoinUserModelImpl.FINDER_CACHE_ENABLED, CoinUserImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByvalidateFlag",
			new String[] {
				String.class.getName(), String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VALIDATEFLAG =
		new FinderPath(CoinUserModelImpl.ENTITY_CACHE_ENABLED,
			CoinUserModelImpl.FINDER_CACHE_ENABLED, CoinUserImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByvalidateFlag",
			new String[] { String.class.getName(), String.class.getName() },
			CoinUserModelImpl.ISVALIDATED_COLUMN_BITMASK |
			CoinUserModelImpl.ACTIVEFLAG_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_VALIDATEFLAG = new FinderPath(CoinUserModelImpl.ENTITY_CACHE_ENABLED,
			CoinUserModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByvalidateFlag",
			new String[] { String.class.getName(), String.class.getName() });

	/**
	 * Returns all the coin users where isValidated = &#63; and activeFlag = &#63;.
	 *
	 * @param isValidated the is validated
	 * @param activeFlag the active flag
	 * @return the matching coin users
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CoinUser> findByvalidateFlag(String isValidated,
		String activeFlag) throws SystemException {
		return findByvalidateFlag(isValidated, activeFlag, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the coin users where isValidated = &#63; and activeFlag = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param isValidated the is validated
	 * @param activeFlag the active flag
	 * @param start the lower bound of the range of coin users
	 * @param end the upper bound of the range of coin users (not inclusive)
	 * @return the range of matching coin users
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CoinUser> findByvalidateFlag(String isValidated,
		String activeFlag, int start, int end) throws SystemException {
		return findByvalidateFlag(isValidated, activeFlag, start, end, null);
	}

	/**
	 * Returns an ordered range of all the coin users where isValidated = &#63; and activeFlag = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param isValidated the is validated
	 * @param activeFlag the active flag
	 * @param start the lower bound of the range of coin users
	 * @param end the upper bound of the range of coin users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching coin users
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CoinUser> findByvalidateFlag(String isValidated,
		String activeFlag, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VALIDATEFLAG;
			finderArgs = new Object[] { isValidated, activeFlag };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_VALIDATEFLAG;
			finderArgs = new Object[] {
					isValidated, activeFlag,
					
					start, end, orderByComparator
				};
		}

		List<CoinUser> list = (List<CoinUser>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CoinUser coinUser : list) {
				if (!Validator.equals(isValidated, coinUser.getIsValidated()) ||
						!Validator.equals(activeFlag, coinUser.getActiveFlag())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_COINUSER_WHERE);

			boolean bindIsValidated = false;

			if (isValidated == null) {
				query.append(_FINDER_COLUMN_VALIDATEFLAG_ISVALIDATED_1);
			}
			else if (isValidated.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_VALIDATEFLAG_ISVALIDATED_3);
			}
			else {
				bindIsValidated = true;

				query.append(_FINDER_COLUMN_VALIDATEFLAG_ISVALIDATED_2);
			}

			boolean bindActiveFlag = false;

			if (activeFlag == null) {
				query.append(_FINDER_COLUMN_VALIDATEFLAG_ACTIVEFLAG_1);
			}
			else if (activeFlag.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_VALIDATEFLAG_ACTIVEFLAG_3);
			}
			else {
				bindActiveFlag = true;

				query.append(_FINDER_COLUMN_VALIDATEFLAG_ACTIVEFLAG_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CoinUserModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindIsValidated) {
					qPos.add(isValidated);
				}

				if (bindActiveFlag) {
					qPos.add(activeFlag);
				}

				if (!pagination) {
					list = (List<CoinUser>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CoinUser>(list);
				}
				else {
					list = (List<CoinUser>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first coin user in the ordered set where isValidated = &#63; and activeFlag = &#63;.
	 *
	 * @param isValidated the is validated
	 * @param activeFlag the active flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching coin user
	 * @throws com.aw.internal.NoSuchCoinUserException if a matching coin user could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinUser findByvalidateFlag_First(String isValidated,
		String activeFlag, OrderByComparator orderByComparator)
		throws NoSuchCoinUserException, SystemException {
		CoinUser coinUser = fetchByvalidateFlag_First(isValidated, activeFlag,
				orderByComparator);

		if (coinUser != null) {
			return coinUser;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("isValidated=");
		msg.append(isValidated);

		msg.append(", activeFlag=");
		msg.append(activeFlag);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCoinUserException(msg.toString());
	}

	/**
	 * Returns the first coin user in the ordered set where isValidated = &#63; and activeFlag = &#63;.
	 *
	 * @param isValidated the is validated
	 * @param activeFlag the active flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching coin user, or <code>null</code> if a matching coin user could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinUser fetchByvalidateFlag_First(String isValidated,
		String activeFlag, OrderByComparator orderByComparator)
		throws SystemException {
		List<CoinUser> list = findByvalidateFlag(isValidated, activeFlag, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last coin user in the ordered set where isValidated = &#63; and activeFlag = &#63;.
	 *
	 * @param isValidated the is validated
	 * @param activeFlag the active flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching coin user
	 * @throws com.aw.internal.NoSuchCoinUserException if a matching coin user could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinUser findByvalidateFlag_Last(String isValidated,
		String activeFlag, OrderByComparator orderByComparator)
		throws NoSuchCoinUserException, SystemException {
		CoinUser coinUser = fetchByvalidateFlag_Last(isValidated, activeFlag,
				orderByComparator);

		if (coinUser != null) {
			return coinUser;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("isValidated=");
		msg.append(isValidated);

		msg.append(", activeFlag=");
		msg.append(activeFlag);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCoinUserException(msg.toString());
	}

	/**
	 * Returns the last coin user in the ordered set where isValidated = &#63; and activeFlag = &#63;.
	 *
	 * @param isValidated the is validated
	 * @param activeFlag the active flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching coin user, or <code>null</code> if a matching coin user could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinUser fetchByvalidateFlag_Last(String isValidated,
		String activeFlag, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByvalidateFlag(isValidated, activeFlag);

		if (count == 0) {
			return null;
		}

		List<CoinUser> list = findByvalidateFlag(isValidated, activeFlag,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the coin users before and after the current coin user in the ordered set where isValidated = &#63; and activeFlag = &#63;.
	 *
	 * @param userId the primary key of the current coin user
	 * @param isValidated the is validated
	 * @param activeFlag the active flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next coin user
	 * @throws com.aw.internal.NoSuchCoinUserException if a coin user with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinUser[] findByvalidateFlag_PrevAndNext(String userId,
		String isValidated, String activeFlag,
		OrderByComparator orderByComparator)
		throws NoSuchCoinUserException, SystemException {
		CoinUser coinUser = findByPrimaryKey(userId);

		Session session = null;

		try {
			session = openSession();

			CoinUser[] array = new CoinUserImpl[3];

			array[0] = getByvalidateFlag_PrevAndNext(session, coinUser,
					isValidated, activeFlag, orderByComparator, true);

			array[1] = coinUser;

			array[2] = getByvalidateFlag_PrevAndNext(session, coinUser,
					isValidated, activeFlag, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CoinUser getByvalidateFlag_PrevAndNext(Session session,
		CoinUser coinUser, String isValidated, String activeFlag,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_COINUSER_WHERE);

		boolean bindIsValidated = false;

		if (isValidated == null) {
			query.append(_FINDER_COLUMN_VALIDATEFLAG_ISVALIDATED_1);
		}
		else if (isValidated.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_VALIDATEFLAG_ISVALIDATED_3);
		}
		else {
			bindIsValidated = true;

			query.append(_FINDER_COLUMN_VALIDATEFLAG_ISVALIDATED_2);
		}

		boolean bindActiveFlag = false;

		if (activeFlag == null) {
			query.append(_FINDER_COLUMN_VALIDATEFLAG_ACTIVEFLAG_1);
		}
		else if (activeFlag.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_VALIDATEFLAG_ACTIVEFLAG_3);
		}
		else {
			bindActiveFlag = true;

			query.append(_FINDER_COLUMN_VALIDATEFLAG_ACTIVEFLAG_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CoinUserModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindIsValidated) {
			qPos.add(isValidated);
		}

		if (bindActiveFlag) {
			qPos.add(activeFlag);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(coinUser);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CoinUser> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the coin users where isValidated = &#63; and activeFlag = &#63; from the database.
	 *
	 * @param isValidated the is validated
	 * @param activeFlag the active flag
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByvalidateFlag(String isValidated, String activeFlag)
		throws SystemException {
		for (CoinUser coinUser : findByvalidateFlag(isValidated, activeFlag,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(coinUser);
		}
	}

	/**
	 * Returns the number of coin users where isValidated = &#63; and activeFlag = &#63;.
	 *
	 * @param isValidated the is validated
	 * @param activeFlag the active flag
	 * @return the number of matching coin users
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByvalidateFlag(String isValidated, String activeFlag)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_VALIDATEFLAG;

		Object[] finderArgs = new Object[] { isValidated, activeFlag };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_COINUSER_WHERE);

			boolean bindIsValidated = false;

			if (isValidated == null) {
				query.append(_FINDER_COLUMN_VALIDATEFLAG_ISVALIDATED_1);
			}
			else if (isValidated.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_VALIDATEFLAG_ISVALIDATED_3);
			}
			else {
				bindIsValidated = true;

				query.append(_FINDER_COLUMN_VALIDATEFLAG_ISVALIDATED_2);
			}

			boolean bindActiveFlag = false;

			if (activeFlag == null) {
				query.append(_FINDER_COLUMN_VALIDATEFLAG_ACTIVEFLAG_1);
			}
			else if (activeFlag.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_VALIDATEFLAG_ACTIVEFLAG_3);
			}
			else {
				bindActiveFlag = true;

				query.append(_FINDER_COLUMN_VALIDATEFLAG_ACTIVEFLAG_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindIsValidated) {
					qPos.add(isValidated);
				}

				if (bindActiveFlag) {
					qPos.add(activeFlag);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_VALIDATEFLAG_ISVALIDATED_1 = "coinUser.isValidated IS NULL AND ";
	private static final String _FINDER_COLUMN_VALIDATEFLAG_ISVALIDATED_2 = "coinUser.isValidated = ? AND ";
	private static final String _FINDER_COLUMN_VALIDATEFLAG_ISVALIDATED_3 = "(coinUser.isValidated IS NULL OR coinUser.isValidated = '') AND ";
	private static final String _FINDER_COLUMN_VALIDATEFLAG_ACTIVEFLAG_1 = "coinUser.activeFlag IS NULL";
	private static final String _FINDER_COLUMN_VALIDATEFLAG_ACTIVEFLAG_2 = "coinUser.activeFlag = ?";
	private static final String _FINDER_COLUMN_VALIDATEFLAG_ACTIVEFLAG_3 = "(coinUser.activeFlag IS NULL OR coinUser.activeFlag = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_NAMEID = new FinderPath(CoinUserModelImpl.ENTITY_CACHE_ENABLED,
			CoinUserModelImpl.FINDER_CACHE_ENABLED, CoinUserImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBynameId",
			new String[] {
				String.class.getName(), String.class.getName(),
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAMEID =
		new FinderPath(CoinUserModelImpl.ENTITY_CACHE_ENABLED,
			CoinUserModelImpl.FINDER_CACHE_ENABLED, CoinUserImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBynameId",
			new String[] {
				String.class.getName(), String.class.getName(),
				String.class.getName()
			},
			CoinUserModelImpl.FIRSTNAME_COLUMN_BITMASK |
			CoinUserModelImpl.LASTNAME_COLUMN_BITMASK |
			CoinUserModelImpl.IDNUMBER_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_NAMEID = new FinderPath(CoinUserModelImpl.ENTITY_CACHE_ENABLED,
			CoinUserModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBynameId",
			new String[] {
				String.class.getName(), String.class.getName(),
				String.class.getName()
			});

	/**
	 * Returns all the coin users where firstName = &#63; and lastName = &#63; and idNumber = &#63;.
	 *
	 * @param firstName the first name
	 * @param lastName the last name
	 * @param idNumber the id number
	 * @return the matching coin users
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CoinUser> findBynameId(String firstName, String lastName,
		String idNumber) throws SystemException {
		return findBynameId(firstName, lastName, idNumber, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the coin users where firstName = &#63; and lastName = &#63; and idNumber = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param firstName the first name
	 * @param lastName the last name
	 * @param idNumber the id number
	 * @param start the lower bound of the range of coin users
	 * @param end the upper bound of the range of coin users (not inclusive)
	 * @return the range of matching coin users
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CoinUser> findBynameId(String firstName, String lastName,
		String idNumber, int start, int end) throws SystemException {
		return findBynameId(firstName, lastName, idNumber, start, end, null);
	}

	/**
	 * Returns an ordered range of all the coin users where firstName = &#63; and lastName = &#63; and idNumber = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param firstName the first name
	 * @param lastName the last name
	 * @param idNumber the id number
	 * @param start the lower bound of the range of coin users
	 * @param end the upper bound of the range of coin users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching coin users
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CoinUser> findBynameId(String firstName, String lastName,
		String idNumber, int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAMEID;
			finderArgs = new Object[] { firstName, lastName, idNumber };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_NAMEID;
			finderArgs = new Object[] {
					firstName, lastName, idNumber,
					
					start, end, orderByComparator
				};
		}

		List<CoinUser> list = (List<CoinUser>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CoinUser coinUser : list) {
				if (!Validator.equals(firstName, coinUser.getFirstName()) ||
						!Validator.equals(lastName, coinUser.getLastName()) ||
						!Validator.equals(idNumber, coinUser.getIdNumber())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(5 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(5);
			}

			query.append(_SQL_SELECT_COINUSER_WHERE);

			boolean bindFirstName = false;

			if (firstName == null) {
				query.append(_FINDER_COLUMN_NAMEID_FIRSTNAME_1);
			}
			else if (firstName.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_NAMEID_FIRSTNAME_3);
			}
			else {
				bindFirstName = true;

				query.append(_FINDER_COLUMN_NAMEID_FIRSTNAME_2);
			}

			boolean bindLastName = false;

			if (lastName == null) {
				query.append(_FINDER_COLUMN_NAMEID_LASTNAME_1);
			}
			else if (lastName.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_NAMEID_LASTNAME_3);
			}
			else {
				bindLastName = true;

				query.append(_FINDER_COLUMN_NAMEID_LASTNAME_2);
			}

			boolean bindIdNumber = false;

			if (idNumber == null) {
				query.append(_FINDER_COLUMN_NAMEID_IDNUMBER_1);
			}
			else if (idNumber.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_NAMEID_IDNUMBER_3);
			}
			else {
				bindIdNumber = true;

				query.append(_FINDER_COLUMN_NAMEID_IDNUMBER_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CoinUserModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindFirstName) {
					qPos.add(firstName);
				}

				if (bindLastName) {
					qPos.add(lastName);
				}

				if (bindIdNumber) {
					qPos.add(idNumber);
				}

				if (!pagination) {
					list = (List<CoinUser>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CoinUser>(list);
				}
				else {
					list = (List<CoinUser>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first coin user in the ordered set where firstName = &#63; and lastName = &#63; and idNumber = &#63;.
	 *
	 * @param firstName the first name
	 * @param lastName the last name
	 * @param idNumber the id number
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching coin user
	 * @throws com.aw.internal.NoSuchCoinUserException if a matching coin user could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinUser findBynameId_First(String firstName, String lastName,
		String idNumber, OrderByComparator orderByComparator)
		throws NoSuchCoinUserException, SystemException {
		CoinUser coinUser = fetchBynameId_First(firstName, lastName, idNumber,
				orderByComparator);

		if (coinUser != null) {
			return coinUser;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("firstName=");
		msg.append(firstName);

		msg.append(", lastName=");
		msg.append(lastName);

		msg.append(", idNumber=");
		msg.append(idNumber);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCoinUserException(msg.toString());
	}

	/**
	 * Returns the first coin user in the ordered set where firstName = &#63; and lastName = &#63; and idNumber = &#63;.
	 *
	 * @param firstName the first name
	 * @param lastName the last name
	 * @param idNumber the id number
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching coin user, or <code>null</code> if a matching coin user could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinUser fetchBynameId_First(String firstName, String lastName,
		String idNumber, OrderByComparator orderByComparator)
		throws SystemException {
		List<CoinUser> list = findBynameId(firstName, lastName, idNumber, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last coin user in the ordered set where firstName = &#63; and lastName = &#63; and idNumber = &#63;.
	 *
	 * @param firstName the first name
	 * @param lastName the last name
	 * @param idNumber the id number
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching coin user
	 * @throws com.aw.internal.NoSuchCoinUserException if a matching coin user could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinUser findBynameId_Last(String firstName, String lastName,
		String idNumber, OrderByComparator orderByComparator)
		throws NoSuchCoinUserException, SystemException {
		CoinUser coinUser = fetchBynameId_Last(firstName, lastName, idNumber,
				orderByComparator);

		if (coinUser != null) {
			return coinUser;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("firstName=");
		msg.append(firstName);

		msg.append(", lastName=");
		msg.append(lastName);

		msg.append(", idNumber=");
		msg.append(idNumber);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCoinUserException(msg.toString());
	}

	/**
	 * Returns the last coin user in the ordered set where firstName = &#63; and lastName = &#63; and idNumber = &#63;.
	 *
	 * @param firstName the first name
	 * @param lastName the last name
	 * @param idNumber the id number
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching coin user, or <code>null</code> if a matching coin user could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinUser fetchBynameId_Last(String firstName, String lastName,
		String idNumber, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countBynameId(firstName, lastName, idNumber);

		if (count == 0) {
			return null;
		}

		List<CoinUser> list = findBynameId(firstName, lastName, idNumber,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the coin users before and after the current coin user in the ordered set where firstName = &#63; and lastName = &#63; and idNumber = &#63;.
	 *
	 * @param userId the primary key of the current coin user
	 * @param firstName the first name
	 * @param lastName the last name
	 * @param idNumber the id number
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next coin user
	 * @throws com.aw.internal.NoSuchCoinUserException if a coin user with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinUser[] findBynameId_PrevAndNext(String userId, String firstName,
		String lastName, String idNumber, OrderByComparator orderByComparator)
		throws NoSuchCoinUserException, SystemException {
		CoinUser coinUser = findByPrimaryKey(userId);

		Session session = null;

		try {
			session = openSession();

			CoinUser[] array = new CoinUserImpl[3];

			array[0] = getBynameId_PrevAndNext(session, coinUser, firstName,
					lastName, idNumber, orderByComparator, true);

			array[1] = coinUser;

			array[2] = getBynameId_PrevAndNext(session, coinUser, firstName,
					lastName, idNumber, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CoinUser getBynameId_PrevAndNext(Session session,
		CoinUser coinUser, String firstName, String lastName, String idNumber,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_COINUSER_WHERE);

		boolean bindFirstName = false;

		if (firstName == null) {
			query.append(_FINDER_COLUMN_NAMEID_FIRSTNAME_1);
		}
		else if (firstName.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_NAMEID_FIRSTNAME_3);
		}
		else {
			bindFirstName = true;

			query.append(_FINDER_COLUMN_NAMEID_FIRSTNAME_2);
		}

		boolean bindLastName = false;

		if (lastName == null) {
			query.append(_FINDER_COLUMN_NAMEID_LASTNAME_1);
		}
		else if (lastName.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_NAMEID_LASTNAME_3);
		}
		else {
			bindLastName = true;

			query.append(_FINDER_COLUMN_NAMEID_LASTNAME_2);
		}

		boolean bindIdNumber = false;

		if (idNumber == null) {
			query.append(_FINDER_COLUMN_NAMEID_IDNUMBER_1);
		}
		else if (idNumber.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_NAMEID_IDNUMBER_3);
		}
		else {
			bindIdNumber = true;

			query.append(_FINDER_COLUMN_NAMEID_IDNUMBER_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CoinUserModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindFirstName) {
			qPos.add(firstName);
		}

		if (bindLastName) {
			qPos.add(lastName);
		}

		if (bindIdNumber) {
			qPos.add(idNumber);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(coinUser);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CoinUser> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the coin users where firstName = &#63; and lastName = &#63; and idNumber = &#63; from the database.
	 *
	 * @param firstName the first name
	 * @param lastName the last name
	 * @param idNumber the id number
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeBynameId(String firstName, String lastName,
		String idNumber) throws SystemException {
		for (CoinUser coinUser : findBynameId(firstName, lastName, idNumber,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(coinUser);
		}
	}

	/**
	 * Returns the number of coin users where firstName = &#63; and lastName = &#63; and idNumber = &#63;.
	 *
	 * @param firstName the first name
	 * @param lastName the last name
	 * @param idNumber the id number
	 * @return the number of matching coin users
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countBynameId(String firstName, String lastName, String idNumber)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_NAMEID;

		Object[] finderArgs = new Object[] { firstName, lastName, idNumber };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_COUNT_COINUSER_WHERE);

			boolean bindFirstName = false;

			if (firstName == null) {
				query.append(_FINDER_COLUMN_NAMEID_FIRSTNAME_1);
			}
			else if (firstName.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_NAMEID_FIRSTNAME_3);
			}
			else {
				bindFirstName = true;

				query.append(_FINDER_COLUMN_NAMEID_FIRSTNAME_2);
			}

			boolean bindLastName = false;

			if (lastName == null) {
				query.append(_FINDER_COLUMN_NAMEID_LASTNAME_1);
			}
			else if (lastName.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_NAMEID_LASTNAME_3);
			}
			else {
				bindLastName = true;

				query.append(_FINDER_COLUMN_NAMEID_LASTNAME_2);
			}

			boolean bindIdNumber = false;

			if (idNumber == null) {
				query.append(_FINDER_COLUMN_NAMEID_IDNUMBER_1);
			}
			else if (idNumber.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_NAMEID_IDNUMBER_3);
			}
			else {
				bindIdNumber = true;

				query.append(_FINDER_COLUMN_NAMEID_IDNUMBER_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindFirstName) {
					qPos.add(firstName);
				}

				if (bindLastName) {
					qPos.add(lastName);
				}

				if (bindIdNumber) {
					qPos.add(idNumber);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_NAMEID_FIRSTNAME_1 = "coinUser.firstName IS NULL AND ";
	private static final String _FINDER_COLUMN_NAMEID_FIRSTNAME_2 = "coinUser.firstName = ? AND ";
	private static final String _FINDER_COLUMN_NAMEID_FIRSTNAME_3 = "(coinUser.firstName IS NULL OR coinUser.firstName = '') AND ";
	private static final String _FINDER_COLUMN_NAMEID_LASTNAME_1 = "coinUser.lastName IS NULL AND ";
	private static final String _FINDER_COLUMN_NAMEID_LASTNAME_2 = "coinUser.lastName = ? AND ";
	private static final String _FINDER_COLUMN_NAMEID_LASTNAME_3 = "(coinUser.lastName IS NULL OR coinUser.lastName = '') AND ";
	private static final String _FINDER_COLUMN_NAMEID_IDNUMBER_1 = "coinUser.idNumber IS NULL";
	private static final String _FINDER_COLUMN_NAMEID_IDNUMBER_2 = "coinUser.idNumber = ?";
	private static final String _FINDER_COLUMN_NAMEID_IDNUMBER_3 = "(coinUser.idNumber IS NULL OR coinUser.idNumber = '')";

	public CoinUserPersistenceImpl() {
		setModelClass(CoinUser.class);
	}

	/**
	 * Caches the coin user in the entity cache if it is enabled.
	 *
	 * @param coinUser the coin user
	 */
	@Override
	public void cacheResult(CoinUser coinUser) {
		EntityCacheUtil.putResult(CoinUserModelImpl.ENTITY_CACHE_ENABLED,
			CoinUserImpl.class, coinUser.getPrimaryKey(), coinUser);

		coinUser.resetOriginalValues();
	}

	/**
	 * Caches the coin users in the entity cache if it is enabled.
	 *
	 * @param coinUsers the coin users
	 */
	@Override
	public void cacheResult(List<CoinUser> coinUsers) {
		for (CoinUser coinUser : coinUsers) {
			if (EntityCacheUtil.getResult(
						CoinUserModelImpl.ENTITY_CACHE_ENABLED,
						CoinUserImpl.class, coinUser.getPrimaryKey()) == null) {
				cacheResult(coinUser);
			}
			else {
				coinUser.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all coin users.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(CoinUserImpl.class.getName());
		}

		EntityCacheUtil.clearCache(CoinUserImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the coin user.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(CoinUser coinUser) {
		EntityCacheUtil.removeResult(CoinUserModelImpl.ENTITY_CACHE_ENABLED,
			CoinUserImpl.class, coinUser.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<CoinUser> coinUsers) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (CoinUser coinUser : coinUsers) {
			EntityCacheUtil.removeResult(CoinUserModelImpl.ENTITY_CACHE_ENABLED,
				CoinUserImpl.class, coinUser.getPrimaryKey());
		}
	}

	/**
	 * Creates a new coin user with the primary key. Does not add the coin user to the database.
	 *
	 * @param userId the primary key for the new coin user
	 * @return the new coin user
	 */
	@Override
	public CoinUser create(String userId) {
		CoinUser coinUser = new CoinUserImpl();

		coinUser.setNew(true);
		coinUser.setPrimaryKey(userId);

		return coinUser;
	}

	/**
	 * Removes the coin user with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param userId the primary key of the coin user
	 * @return the coin user that was removed
	 * @throws com.aw.internal.NoSuchCoinUserException if a coin user with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinUser remove(String userId)
		throws NoSuchCoinUserException, SystemException {
		return remove((Serializable)userId);
	}

	/**
	 * Removes the coin user with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the coin user
	 * @return the coin user that was removed
	 * @throws com.aw.internal.NoSuchCoinUserException if a coin user with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinUser remove(Serializable primaryKey)
		throws NoSuchCoinUserException, SystemException {
		Session session = null;

		try {
			session = openSession();

			CoinUser coinUser = (CoinUser)session.get(CoinUserImpl.class,
					primaryKey);

			if (coinUser == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCoinUserException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(coinUser);
		}
		catch (NoSuchCoinUserException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected CoinUser removeImpl(CoinUser coinUser) throws SystemException {
		coinUser = toUnwrappedModel(coinUser);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(coinUser)) {
				coinUser = (CoinUser)session.get(CoinUserImpl.class,
						coinUser.getPrimaryKeyObj());
			}

			if (coinUser != null) {
				session.delete(coinUser);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (coinUser != null) {
			clearCache(coinUser);
		}

		return coinUser;
	}

	@Override
	public CoinUser updateImpl(com.aw.internal.model.CoinUser coinUser)
		throws SystemException {
		coinUser = toUnwrappedModel(coinUser);

		boolean isNew = coinUser.isNew();

		CoinUserModelImpl coinUserModelImpl = (CoinUserModelImpl)coinUser;

		Session session = null;

		try {
			session = openSession();

			if (coinUser.isNew()) {
				session.save(coinUser);

				coinUser.setNew(false);
			}
			else {
				session.merge(coinUser);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !CoinUserModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((coinUserModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SALES.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						coinUserModelImpl.getOriginalAddUser(),
						coinUserModelImpl.getOriginalActiveFlag()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SALES, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SALES,
					args);

				args = new Object[] {
						coinUserModelImpl.getAddUser(),
						coinUserModelImpl.getActiveFlag()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SALES, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SALES,
					args);
			}

			if ((coinUserModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ACTIVEFLAG.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						coinUserModelImpl.getOriginalActiveFlag()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ACTIVEFLAG,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ACTIVEFLAG,
					args);

				args = new Object[] { coinUserModelImpl.getActiveFlag() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ACTIVEFLAG,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ACTIVEFLAG,
					args);
			}

			if ((coinUserModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VALIDATEFLAG.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						coinUserModelImpl.getOriginalIsValidated(),
						coinUserModelImpl.getOriginalActiveFlag()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_VALIDATEFLAG,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VALIDATEFLAG,
					args);

				args = new Object[] {
						coinUserModelImpl.getIsValidated(),
						coinUserModelImpl.getActiveFlag()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_VALIDATEFLAG,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_VALIDATEFLAG,
					args);
			}

			if ((coinUserModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAMEID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						coinUserModelImpl.getOriginalFirstName(),
						coinUserModelImpl.getOriginalLastName(),
						coinUserModelImpl.getOriginalIdNumber()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_NAMEID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAMEID,
					args);

				args = new Object[] {
						coinUserModelImpl.getFirstName(),
						coinUserModelImpl.getLastName(),
						coinUserModelImpl.getIdNumber()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_NAMEID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_NAMEID,
					args);
			}
		}

		EntityCacheUtil.putResult(CoinUserModelImpl.ENTITY_CACHE_ENABLED,
			CoinUserImpl.class, coinUser.getPrimaryKey(), coinUser);

		return coinUser;
	}

	protected CoinUser toUnwrappedModel(CoinUser coinUser) {
		if (coinUser instanceof CoinUserImpl) {
			return coinUser;
		}

		CoinUserImpl coinUserImpl = new CoinUserImpl();

		coinUserImpl.setNew(coinUser.isNew());
		coinUserImpl.setPrimaryKey(coinUser.getPrimaryKey());

		coinUserImpl.setUserId(coinUser.getUserId());
		coinUserImpl.setFirstName(coinUser.getFirstName());
		coinUserImpl.setLastName(coinUser.getLastName());
		coinUserImpl.setActiveFlag(coinUser.getActiveFlag());
		coinUserImpl.setBirthday(coinUser.getBirthday());
		coinUserImpl.setContactId(coinUser.getContactId());
		coinUserImpl.setUserType(coinUser.getUserType());
		coinUserImpl.setBrokerId(coinUser.getBrokerId());
		coinUserImpl.setFundId(coinUser.getFundId());
		coinUserImpl.setNationality(coinUser.getNationality());
		coinUserImpl.setIdIssueCountry(coinUser.getIdIssueCountry());
		coinUserImpl.setIdType(coinUser.getIdType());
		coinUserImpl.setIdNumber(coinUser.getIdNumber());
		coinUserImpl.setPhotoImage(coinUser.getPhotoImage());
		coinUserImpl.setDocId(coinUser.getDocId());
		coinUserImpl.setSsn(coinUser.getSsn());
		coinUserImpl.setEin(coinUser.getEin());
		coinUserImpl.setIncome(coinUser.getIncome());
		coinUserImpl.setAsset(coinUser.getAsset());
		coinUserImpl.setIsValidated(coinUser.getIsValidated());
		coinUserImpl.setIsCreddited(coinUser.getIsCreddited());
		coinUserImpl.setAddTime(coinUser.getAddTime());
		coinUserImpl.setAddUser(coinUser.getAddUser());

		return coinUserImpl;
	}

	/**
	 * Returns the coin user with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the coin user
	 * @return the coin user
	 * @throws com.aw.internal.NoSuchCoinUserException if a coin user with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinUser findByPrimaryKey(Serializable primaryKey)
		throws NoSuchCoinUserException, SystemException {
		CoinUser coinUser = fetchByPrimaryKey(primaryKey);

		if (coinUser == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchCoinUserException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return coinUser;
	}

	/**
	 * Returns the coin user with the primary key or throws a {@link com.aw.internal.NoSuchCoinUserException} if it could not be found.
	 *
	 * @param userId the primary key of the coin user
	 * @return the coin user
	 * @throws com.aw.internal.NoSuchCoinUserException if a coin user with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinUser findByPrimaryKey(String userId)
		throws NoSuchCoinUserException, SystemException {
		return findByPrimaryKey((Serializable)userId);
	}

	/**
	 * Returns the coin user with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the coin user
	 * @return the coin user, or <code>null</code> if a coin user with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinUser fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		CoinUser coinUser = (CoinUser)EntityCacheUtil.getResult(CoinUserModelImpl.ENTITY_CACHE_ENABLED,
				CoinUserImpl.class, primaryKey);

		if (coinUser == _nullCoinUser) {
			return null;
		}

		if (coinUser == null) {
			Session session = null;

			try {
				session = openSession();

				coinUser = (CoinUser)session.get(CoinUserImpl.class, primaryKey);

				if (coinUser != null) {
					cacheResult(coinUser);
				}
				else {
					EntityCacheUtil.putResult(CoinUserModelImpl.ENTITY_CACHE_ENABLED,
						CoinUserImpl.class, primaryKey, _nullCoinUser);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(CoinUserModelImpl.ENTITY_CACHE_ENABLED,
					CoinUserImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return coinUser;
	}

	/**
	 * Returns the coin user with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param userId the primary key of the coin user
	 * @return the coin user, or <code>null</code> if a coin user with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CoinUser fetchByPrimaryKey(String userId) throws SystemException {
		return fetchByPrimaryKey((Serializable)userId);
	}

	/**
	 * Returns all the coin users.
	 *
	 * @return the coin users
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CoinUser> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the coin users.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of coin users
	 * @param end the upper bound of the range of coin users (not inclusive)
	 * @return the range of coin users
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CoinUser> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the coin users.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.CoinUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of coin users
	 * @param end the upper bound of the range of coin users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of coin users
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CoinUser> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<CoinUser> list = (List<CoinUser>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_COINUSER);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_COINUSER;

				if (pagination) {
					sql = sql.concat(CoinUserModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<CoinUser>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CoinUser>(list);
				}
				else {
					list = (List<CoinUser>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the coin users from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (CoinUser coinUser : findAll()) {
			remove(coinUser);
		}
	}

	/**
	 * Returns the number of coin users.
	 *
	 * @return the number of coin users
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_COINUSER);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the coin user persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.aw.internal.model.CoinUser")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<CoinUser>> listenersList = new ArrayList<ModelListener<CoinUser>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<CoinUser>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(CoinUserImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_COINUSER = "SELECT coinUser FROM CoinUser coinUser";
	private static final String _SQL_SELECT_COINUSER_WHERE = "SELECT coinUser FROM CoinUser coinUser WHERE ";
	private static final String _SQL_COUNT_COINUSER = "SELECT COUNT(coinUser) FROM CoinUser coinUser";
	private static final String _SQL_COUNT_COINUSER_WHERE = "SELECT COUNT(coinUser) FROM CoinUser coinUser WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "coinUser.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No CoinUser exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No CoinUser exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(CoinUserPersistenceImpl.class);
	private static CoinUser _nullCoinUser = new CoinUserImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<CoinUser> toCacheModel() {
				return _nullCoinUserCacheModel;
			}
		};

	private static CacheModel<CoinUser> _nullCoinUserCacheModel = new CacheModel<CoinUser>() {
			@Override
			public CoinUser toEntityModel() {
				return _nullCoinUser;
			}
		};
}