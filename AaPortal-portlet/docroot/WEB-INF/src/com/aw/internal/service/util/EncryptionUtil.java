package com.aw.internal.service.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

/*This class is duplicated in ***encryption.jspf***, please check and change 
 * that file accordingly when making any change on this class*/
public class EncryptionUtil {
	
	
	private static final char[] PWD = "aw_store_pwd".toCharArray();
	private static final String PATH = "/apps/aw_key/keystore.jceks";
	private static final String KEY_NAME = "aw_data_key";
	private static final String KEY_PWD = "aw__data_key_pwd";
	
	private static SecretKeySpec secKey;//internal, never give out!
	
	static{
		try (java.io.FileInputStream fis = new java.io.FileInputStream(PATH)){
			KeyStore keyStore = KeyStore.getInstance("jceks");
			keyStore.load(fis, PWD);
			secKey = (SecretKeySpec) keyStore.getKey(KEY_NAME, KEY_PWD.toCharArray());
	    } catch(IOException e){
	    	System.out.println("*******Attention! Error when loading encryption module!*******");
	    	e.printStackTrace();
	    } catch (NoSuchAlgorithmException e) {
	    	System.out.println("*******Attention! Error when loading encryption module!*******");
			e.printStackTrace();
		} catch (CertificateException e) {
			System.out.println("*******Attention! Error when loading encryption module!*******");
			e.printStackTrace();
		} catch (KeyStoreException e) {
			System.out.println("*******Attention! Error when loading encryption module!*******");
			e.printStackTrace();
		} catch (UnrecoverableEntryException e) {
			e.printStackTrace();
		}
	}
	
	public static String encrypt(String input) {//default using the internal one
		return encrypt(input, secKey);
	}

	private static String encrypt(String input, SecretKeySpec key) {
		
		Cipher cipher;
		try {
			
			cipher = Cipher.getInstance(key.getAlgorithm());
			cipher.init(Cipher.ENCRYPT_MODE, key);			
			byte[] cipherText = cipher.doFinal(input.getBytes("UTF-8"));			
			return Base64.encodeBase64String(cipherText);
			
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		} catch (InvalidKeyException e) {
			e.printStackTrace();
			return null;
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
			return null;
		} catch (BadPaddingException e) {
			e.printStackTrace();
			return null;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
			return null;
		}		

	}
	
	public static String decrypt(String input){//default using internal one
		return decrypt(input, secKey);
	}

	
	private static String decrypt(String input, SecretKeySpec key){
		
		try {
			Cipher cipher = Cipher.getInstance(key.getAlgorithm());
			cipher.init(Cipher.DECRYPT_MODE, key);			
			byte[] cipherText = cipher.doFinal(Base64.decodeBase64(input));
			return new String(cipherText,"UTF-8");
			
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		} catch (InvalidKeyException e) {
			e.printStackTrace();
			return null;
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
			return null;
		} catch (BadPaddingException e) {
			e.printStackTrace();
			return null;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
			return null;
		}
				
	}
}