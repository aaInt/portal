/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.persistence;

import com.aw.internal.NoSuchFutureUserException;
import com.aw.internal.model.FutureUser;
import com.aw.internal.model.impl.FutureUserImpl;
import com.aw.internal.model.impl.FutureUserModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the future user service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author zl
 * @see FutureUserPersistence
 * @see FutureUserUtil
 * @generated
 */
public class FutureUserPersistenceImpl extends BasePersistenceImpl<FutureUser>
	implements FutureUserPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link FutureUserUtil} to access the future user persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = FutureUserImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(FutureUserModelImpl.ENTITY_CACHE_ENABLED,
			FutureUserModelImpl.FINDER_CACHE_ENABLED, FutureUserImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(FutureUserModelImpl.ENTITY_CACHE_ENABLED,
			FutureUserModelImpl.FINDER_CACHE_ENABLED, FutureUserImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(FutureUserModelImpl.ENTITY_CACHE_ENABLED,
			FutureUserModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ACTIVEUSER =
		new FinderPath(FutureUserModelImpl.ENTITY_CACHE_ENABLED,
			FutureUserModelImpl.FINDER_CACHE_ENABLED, FutureUserImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByactiveUser",
			new String[] {
				String.class.getName(), String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ACTIVEUSER =
		new FinderPath(FutureUserModelImpl.ENTITY_CACHE_ENABLED,
			FutureUserModelImpl.FINDER_CACHE_ENABLED, FutureUserImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByactiveUser",
			new String[] { String.class.getName(), String.class.getName() },
			FutureUserModelImpl.ACTIVEFLAG_COLUMN_BITMASK |
			FutureUserModelImpl.TAKENFLAG_COLUMN_BITMASK |
			FutureUserModelImpl.ADDTIME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ACTIVEUSER = new FinderPath(FutureUserModelImpl.ENTITY_CACHE_ENABLED,
			FutureUserModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByactiveUser",
			new String[] { String.class.getName(), String.class.getName() });

	/**
	 * Returns all the future users where activeFlag = &#63; and takenFlag = &#63;.
	 *
	 * @param activeFlag the active flag
	 * @param takenFlag the taken flag
	 * @return the matching future users
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<FutureUser> findByactiveUser(String activeFlag, String takenFlag)
		throws SystemException {
		return findByactiveUser(activeFlag, takenFlag, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the future users where activeFlag = &#63; and takenFlag = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.FutureUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param activeFlag the active flag
	 * @param takenFlag the taken flag
	 * @param start the lower bound of the range of future users
	 * @param end the upper bound of the range of future users (not inclusive)
	 * @return the range of matching future users
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<FutureUser> findByactiveUser(String activeFlag,
		String takenFlag, int start, int end) throws SystemException {
		return findByactiveUser(activeFlag, takenFlag, start, end, null);
	}

	/**
	 * Returns an ordered range of all the future users where activeFlag = &#63; and takenFlag = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.FutureUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param activeFlag the active flag
	 * @param takenFlag the taken flag
	 * @param start the lower bound of the range of future users
	 * @param end the upper bound of the range of future users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching future users
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<FutureUser> findByactiveUser(String activeFlag,
		String takenFlag, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ACTIVEUSER;
			finderArgs = new Object[] { activeFlag, takenFlag };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ACTIVEUSER;
			finderArgs = new Object[] {
					activeFlag, takenFlag,
					
					start, end, orderByComparator
				};
		}

		List<FutureUser> list = (List<FutureUser>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (FutureUser futureUser : list) {
				if (!Validator.equals(activeFlag, futureUser.getActiveFlag()) ||
						!Validator.equals(takenFlag, futureUser.getTakenFlag())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_FUTUREUSER_WHERE);

			boolean bindActiveFlag = false;

			if (activeFlag == null) {
				query.append(_FINDER_COLUMN_ACTIVEUSER_ACTIVEFLAG_1);
			}
			else if (activeFlag.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ACTIVEUSER_ACTIVEFLAG_3);
			}
			else {
				bindActiveFlag = true;

				query.append(_FINDER_COLUMN_ACTIVEUSER_ACTIVEFLAG_2);
			}

			boolean bindTakenFlag = false;

			if (takenFlag == null) {
				query.append(_FINDER_COLUMN_ACTIVEUSER_TAKENFLAG_1);
			}
			else if (takenFlag.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ACTIVEUSER_TAKENFLAG_3);
			}
			else {
				bindTakenFlag = true;

				query.append(_FINDER_COLUMN_ACTIVEUSER_TAKENFLAG_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(FutureUserModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindActiveFlag) {
					qPos.add(activeFlag);
				}

				if (bindTakenFlag) {
					qPos.add(takenFlag);
				}

				if (!pagination) {
					list = (List<FutureUser>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<FutureUser>(list);
				}
				else {
					list = (List<FutureUser>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first future user in the ordered set where activeFlag = &#63; and takenFlag = &#63;.
	 *
	 * @param activeFlag the active flag
	 * @param takenFlag the taken flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching future user
	 * @throws com.aw.internal.NoSuchFutureUserException if a matching future user could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FutureUser findByactiveUser_First(String activeFlag,
		String takenFlag, OrderByComparator orderByComparator)
		throws NoSuchFutureUserException, SystemException {
		FutureUser futureUser = fetchByactiveUser_First(activeFlag, takenFlag,
				orderByComparator);

		if (futureUser != null) {
			return futureUser;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("activeFlag=");
		msg.append(activeFlag);

		msg.append(", takenFlag=");
		msg.append(takenFlag);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchFutureUserException(msg.toString());
	}

	/**
	 * Returns the first future user in the ordered set where activeFlag = &#63; and takenFlag = &#63;.
	 *
	 * @param activeFlag the active flag
	 * @param takenFlag the taken flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching future user, or <code>null</code> if a matching future user could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FutureUser fetchByactiveUser_First(String activeFlag,
		String takenFlag, OrderByComparator orderByComparator)
		throws SystemException {
		List<FutureUser> list = findByactiveUser(activeFlag, takenFlag, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last future user in the ordered set where activeFlag = &#63; and takenFlag = &#63;.
	 *
	 * @param activeFlag the active flag
	 * @param takenFlag the taken flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching future user
	 * @throws com.aw.internal.NoSuchFutureUserException if a matching future user could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FutureUser findByactiveUser_Last(String activeFlag,
		String takenFlag, OrderByComparator orderByComparator)
		throws NoSuchFutureUserException, SystemException {
		FutureUser futureUser = fetchByactiveUser_Last(activeFlag, takenFlag,
				orderByComparator);

		if (futureUser != null) {
			return futureUser;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("activeFlag=");
		msg.append(activeFlag);

		msg.append(", takenFlag=");
		msg.append(takenFlag);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchFutureUserException(msg.toString());
	}

	/**
	 * Returns the last future user in the ordered set where activeFlag = &#63; and takenFlag = &#63;.
	 *
	 * @param activeFlag the active flag
	 * @param takenFlag the taken flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching future user, or <code>null</code> if a matching future user could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FutureUser fetchByactiveUser_Last(String activeFlag,
		String takenFlag, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByactiveUser(activeFlag, takenFlag);

		if (count == 0) {
			return null;
		}

		List<FutureUser> list = findByactiveUser(activeFlag, takenFlag,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the future users before and after the current future user in the ordered set where activeFlag = &#63; and takenFlag = &#63;.
	 *
	 * @param email the primary key of the current future user
	 * @param activeFlag the active flag
	 * @param takenFlag the taken flag
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next future user
	 * @throws com.aw.internal.NoSuchFutureUserException if a future user with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FutureUser[] findByactiveUser_PrevAndNext(String email,
		String activeFlag, String takenFlag, OrderByComparator orderByComparator)
		throws NoSuchFutureUserException, SystemException {
		FutureUser futureUser = findByPrimaryKey(email);

		Session session = null;

		try {
			session = openSession();

			FutureUser[] array = new FutureUserImpl[3];

			array[0] = getByactiveUser_PrevAndNext(session, futureUser,
					activeFlag, takenFlag, orderByComparator, true);

			array[1] = futureUser;

			array[2] = getByactiveUser_PrevAndNext(session, futureUser,
					activeFlag, takenFlag, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected FutureUser getByactiveUser_PrevAndNext(Session session,
		FutureUser futureUser, String activeFlag, String takenFlag,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_FUTUREUSER_WHERE);

		boolean bindActiveFlag = false;

		if (activeFlag == null) {
			query.append(_FINDER_COLUMN_ACTIVEUSER_ACTIVEFLAG_1);
		}
		else if (activeFlag.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_ACTIVEUSER_ACTIVEFLAG_3);
		}
		else {
			bindActiveFlag = true;

			query.append(_FINDER_COLUMN_ACTIVEUSER_ACTIVEFLAG_2);
		}

		boolean bindTakenFlag = false;

		if (takenFlag == null) {
			query.append(_FINDER_COLUMN_ACTIVEUSER_TAKENFLAG_1);
		}
		else if (takenFlag.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_ACTIVEUSER_TAKENFLAG_3);
		}
		else {
			bindTakenFlag = true;

			query.append(_FINDER_COLUMN_ACTIVEUSER_TAKENFLAG_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(FutureUserModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindActiveFlag) {
			qPos.add(activeFlag);
		}

		if (bindTakenFlag) {
			qPos.add(takenFlag);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(futureUser);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<FutureUser> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the future users where activeFlag = &#63; and takenFlag = &#63; from the database.
	 *
	 * @param activeFlag the active flag
	 * @param takenFlag the taken flag
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByactiveUser(String activeFlag, String takenFlag)
		throws SystemException {
		for (FutureUser futureUser : findByactiveUser(activeFlag, takenFlag,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(futureUser);
		}
	}

	/**
	 * Returns the number of future users where activeFlag = &#63; and takenFlag = &#63;.
	 *
	 * @param activeFlag the active flag
	 * @param takenFlag the taken flag
	 * @return the number of matching future users
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByactiveUser(String activeFlag, String takenFlag)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_ACTIVEUSER;

		Object[] finderArgs = new Object[] { activeFlag, takenFlag };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_FUTUREUSER_WHERE);

			boolean bindActiveFlag = false;

			if (activeFlag == null) {
				query.append(_FINDER_COLUMN_ACTIVEUSER_ACTIVEFLAG_1);
			}
			else if (activeFlag.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ACTIVEUSER_ACTIVEFLAG_3);
			}
			else {
				bindActiveFlag = true;

				query.append(_FINDER_COLUMN_ACTIVEUSER_ACTIVEFLAG_2);
			}

			boolean bindTakenFlag = false;

			if (takenFlag == null) {
				query.append(_FINDER_COLUMN_ACTIVEUSER_TAKENFLAG_1);
			}
			else if (takenFlag.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ACTIVEUSER_TAKENFLAG_3);
			}
			else {
				bindTakenFlag = true;

				query.append(_FINDER_COLUMN_ACTIVEUSER_TAKENFLAG_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindActiveFlag) {
					qPos.add(activeFlag);
				}

				if (bindTakenFlag) {
					qPos.add(takenFlag);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ACTIVEUSER_ACTIVEFLAG_1 = "futureUser.activeFlag IS NULL AND ";
	private static final String _FINDER_COLUMN_ACTIVEUSER_ACTIVEFLAG_2 = "futureUser.activeFlag = ? AND ";
	private static final String _FINDER_COLUMN_ACTIVEUSER_ACTIVEFLAG_3 = "(futureUser.activeFlag IS NULL OR futureUser.activeFlag = '') AND ";
	private static final String _FINDER_COLUMN_ACTIVEUSER_TAKENFLAG_1 = "futureUser.takenFlag IS NULL";
	private static final String _FINDER_COLUMN_ACTIVEUSER_TAKENFLAG_2 = "futureUser.takenFlag = ?";
	private static final String _FINDER_COLUMN_ACTIVEUSER_TAKENFLAG_3 = "(futureUser.takenFlag IS NULL OR futureUser.takenFlag = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_MYTAKEN = new FinderPath(FutureUserModelImpl.ENTITY_CACHE_ENABLED,
			FutureUserModelImpl.FINDER_CACHE_ENABLED, FutureUserImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBymytaken",
			new String[] {
				String.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MYTAKEN =
		new FinderPath(FutureUserModelImpl.ENTITY_CACHE_ENABLED,
			FutureUserModelImpl.FINDER_CACHE_ENABLED, FutureUserImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBymytaken",
			new String[] { String.class.getName(), Long.class.getName() },
			FutureUserModelImpl.ACTIVEFLAG_COLUMN_BITMASK |
			FutureUserModelImpl.OWNERID_COLUMN_BITMASK |
			FutureUserModelImpl.ADDTIME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_MYTAKEN = new FinderPath(FutureUserModelImpl.ENTITY_CACHE_ENABLED,
			FutureUserModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBymytaken",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns all the future users where activeFlag = &#63; and ownerId = &#63;.
	 *
	 * @param activeFlag the active flag
	 * @param ownerId the owner ID
	 * @return the matching future users
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<FutureUser> findBymytaken(String activeFlag, long ownerId)
		throws SystemException {
		return findBymytaken(activeFlag, ownerId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the future users where activeFlag = &#63; and ownerId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.FutureUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param activeFlag the active flag
	 * @param ownerId the owner ID
	 * @param start the lower bound of the range of future users
	 * @param end the upper bound of the range of future users (not inclusive)
	 * @return the range of matching future users
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<FutureUser> findBymytaken(String activeFlag, long ownerId,
		int start, int end) throws SystemException {
		return findBymytaken(activeFlag, ownerId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the future users where activeFlag = &#63; and ownerId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.FutureUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param activeFlag the active flag
	 * @param ownerId the owner ID
	 * @param start the lower bound of the range of future users
	 * @param end the upper bound of the range of future users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching future users
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<FutureUser> findBymytaken(String activeFlag, long ownerId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MYTAKEN;
			finderArgs = new Object[] { activeFlag, ownerId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_MYTAKEN;
			finderArgs = new Object[] {
					activeFlag, ownerId,
					
					start, end, orderByComparator
				};
		}

		List<FutureUser> list = (List<FutureUser>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (FutureUser futureUser : list) {
				if (!Validator.equals(activeFlag, futureUser.getActiveFlag()) ||
						(ownerId != futureUser.getOwnerId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_FUTUREUSER_WHERE);

			boolean bindActiveFlag = false;

			if (activeFlag == null) {
				query.append(_FINDER_COLUMN_MYTAKEN_ACTIVEFLAG_1);
			}
			else if (activeFlag.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_MYTAKEN_ACTIVEFLAG_3);
			}
			else {
				bindActiveFlag = true;

				query.append(_FINDER_COLUMN_MYTAKEN_ACTIVEFLAG_2);
			}

			query.append(_FINDER_COLUMN_MYTAKEN_OWNERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(FutureUserModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindActiveFlag) {
					qPos.add(activeFlag);
				}

				qPos.add(ownerId);

				if (!pagination) {
					list = (List<FutureUser>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<FutureUser>(list);
				}
				else {
					list = (List<FutureUser>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first future user in the ordered set where activeFlag = &#63; and ownerId = &#63;.
	 *
	 * @param activeFlag the active flag
	 * @param ownerId the owner ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching future user
	 * @throws com.aw.internal.NoSuchFutureUserException if a matching future user could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FutureUser findBymytaken_First(String activeFlag, long ownerId,
		OrderByComparator orderByComparator)
		throws NoSuchFutureUserException, SystemException {
		FutureUser futureUser = fetchBymytaken_First(activeFlag, ownerId,
				orderByComparator);

		if (futureUser != null) {
			return futureUser;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("activeFlag=");
		msg.append(activeFlag);

		msg.append(", ownerId=");
		msg.append(ownerId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchFutureUserException(msg.toString());
	}

	/**
	 * Returns the first future user in the ordered set where activeFlag = &#63; and ownerId = &#63;.
	 *
	 * @param activeFlag the active flag
	 * @param ownerId the owner ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching future user, or <code>null</code> if a matching future user could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FutureUser fetchBymytaken_First(String activeFlag, long ownerId,
		OrderByComparator orderByComparator) throws SystemException {
		List<FutureUser> list = findBymytaken(activeFlag, ownerId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last future user in the ordered set where activeFlag = &#63; and ownerId = &#63;.
	 *
	 * @param activeFlag the active flag
	 * @param ownerId the owner ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching future user
	 * @throws com.aw.internal.NoSuchFutureUserException if a matching future user could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FutureUser findBymytaken_Last(String activeFlag, long ownerId,
		OrderByComparator orderByComparator)
		throws NoSuchFutureUserException, SystemException {
		FutureUser futureUser = fetchBymytaken_Last(activeFlag, ownerId,
				orderByComparator);

		if (futureUser != null) {
			return futureUser;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("activeFlag=");
		msg.append(activeFlag);

		msg.append(", ownerId=");
		msg.append(ownerId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchFutureUserException(msg.toString());
	}

	/**
	 * Returns the last future user in the ordered set where activeFlag = &#63; and ownerId = &#63;.
	 *
	 * @param activeFlag the active flag
	 * @param ownerId the owner ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching future user, or <code>null</code> if a matching future user could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FutureUser fetchBymytaken_Last(String activeFlag, long ownerId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBymytaken(activeFlag, ownerId);

		if (count == 0) {
			return null;
		}

		List<FutureUser> list = findBymytaken(activeFlag, ownerId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the future users before and after the current future user in the ordered set where activeFlag = &#63; and ownerId = &#63;.
	 *
	 * @param email the primary key of the current future user
	 * @param activeFlag the active flag
	 * @param ownerId the owner ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next future user
	 * @throws com.aw.internal.NoSuchFutureUserException if a future user with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FutureUser[] findBymytaken_PrevAndNext(String email,
		String activeFlag, long ownerId, OrderByComparator orderByComparator)
		throws NoSuchFutureUserException, SystemException {
		FutureUser futureUser = findByPrimaryKey(email);

		Session session = null;

		try {
			session = openSession();

			FutureUser[] array = new FutureUserImpl[3];

			array[0] = getBymytaken_PrevAndNext(session, futureUser,
					activeFlag, ownerId, orderByComparator, true);

			array[1] = futureUser;

			array[2] = getBymytaken_PrevAndNext(session, futureUser,
					activeFlag, ownerId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected FutureUser getBymytaken_PrevAndNext(Session session,
		FutureUser futureUser, String activeFlag, long ownerId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_FUTUREUSER_WHERE);

		boolean bindActiveFlag = false;

		if (activeFlag == null) {
			query.append(_FINDER_COLUMN_MYTAKEN_ACTIVEFLAG_1);
		}
		else if (activeFlag.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_MYTAKEN_ACTIVEFLAG_3);
		}
		else {
			bindActiveFlag = true;

			query.append(_FINDER_COLUMN_MYTAKEN_ACTIVEFLAG_2);
		}

		query.append(_FINDER_COLUMN_MYTAKEN_OWNERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(FutureUserModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindActiveFlag) {
			qPos.add(activeFlag);
		}

		qPos.add(ownerId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(futureUser);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<FutureUser> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the future users where activeFlag = &#63; and ownerId = &#63; from the database.
	 *
	 * @param activeFlag the active flag
	 * @param ownerId the owner ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeBymytaken(String activeFlag, long ownerId)
		throws SystemException {
		for (FutureUser futureUser : findBymytaken(activeFlag, ownerId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(futureUser);
		}
	}

	/**
	 * Returns the number of future users where activeFlag = &#63; and ownerId = &#63;.
	 *
	 * @param activeFlag the active flag
	 * @param ownerId the owner ID
	 * @return the number of matching future users
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countBymytaken(String activeFlag, long ownerId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_MYTAKEN;

		Object[] finderArgs = new Object[] { activeFlag, ownerId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_FUTUREUSER_WHERE);

			boolean bindActiveFlag = false;

			if (activeFlag == null) {
				query.append(_FINDER_COLUMN_MYTAKEN_ACTIVEFLAG_1);
			}
			else if (activeFlag.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_MYTAKEN_ACTIVEFLAG_3);
			}
			else {
				bindActiveFlag = true;

				query.append(_FINDER_COLUMN_MYTAKEN_ACTIVEFLAG_2);
			}

			query.append(_FINDER_COLUMN_MYTAKEN_OWNERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindActiveFlag) {
					qPos.add(activeFlag);
				}

				qPos.add(ownerId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_MYTAKEN_ACTIVEFLAG_1 = "futureUser.activeFlag IS NULL AND ";
	private static final String _FINDER_COLUMN_MYTAKEN_ACTIVEFLAG_2 = "futureUser.activeFlag = ? AND ";
	private static final String _FINDER_COLUMN_MYTAKEN_ACTIVEFLAG_3 = "(futureUser.activeFlag IS NULL OR futureUser.activeFlag = '') AND ";
	private static final String _FINDER_COLUMN_MYTAKEN_OWNERID_2 = "futureUser.ownerId = ?";

	public FutureUserPersistenceImpl() {
		setModelClass(FutureUser.class);
	}

	/**
	 * Caches the future user in the entity cache if it is enabled.
	 *
	 * @param futureUser the future user
	 */
	@Override
	public void cacheResult(FutureUser futureUser) {
		EntityCacheUtil.putResult(FutureUserModelImpl.ENTITY_CACHE_ENABLED,
			FutureUserImpl.class, futureUser.getPrimaryKey(), futureUser);

		futureUser.resetOriginalValues();
	}

	/**
	 * Caches the future users in the entity cache if it is enabled.
	 *
	 * @param futureUsers the future users
	 */
	@Override
	public void cacheResult(List<FutureUser> futureUsers) {
		for (FutureUser futureUser : futureUsers) {
			if (EntityCacheUtil.getResult(
						FutureUserModelImpl.ENTITY_CACHE_ENABLED,
						FutureUserImpl.class, futureUser.getPrimaryKey()) == null) {
				cacheResult(futureUser);
			}
			else {
				futureUser.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all future users.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(FutureUserImpl.class.getName());
		}

		EntityCacheUtil.clearCache(FutureUserImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the future user.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(FutureUser futureUser) {
		EntityCacheUtil.removeResult(FutureUserModelImpl.ENTITY_CACHE_ENABLED,
			FutureUserImpl.class, futureUser.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<FutureUser> futureUsers) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (FutureUser futureUser : futureUsers) {
			EntityCacheUtil.removeResult(FutureUserModelImpl.ENTITY_CACHE_ENABLED,
				FutureUserImpl.class, futureUser.getPrimaryKey());
		}
	}

	/**
	 * Creates a new future user with the primary key. Does not add the future user to the database.
	 *
	 * @param email the primary key for the new future user
	 * @return the new future user
	 */
	@Override
	public FutureUser create(String email) {
		FutureUser futureUser = new FutureUserImpl();

		futureUser.setNew(true);
		futureUser.setPrimaryKey(email);

		return futureUser;
	}

	/**
	 * Removes the future user with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param email the primary key of the future user
	 * @return the future user that was removed
	 * @throws com.aw.internal.NoSuchFutureUserException if a future user with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FutureUser remove(String email)
		throws NoSuchFutureUserException, SystemException {
		return remove((Serializable)email);
	}

	/**
	 * Removes the future user with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the future user
	 * @return the future user that was removed
	 * @throws com.aw.internal.NoSuchFutureUserException if a future user with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FutureUser remove(Serializable primaryKey)
		throws NoSuchFutureUserException, SystemException {
		Session session = null;

		try {
			session = openSession();

			FutureUser futureUser = (FutureUser)session.get(FutureUserImpl.class,
					primaryKey);

			if (futureUser == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchFutureUserException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(futureUser);
		}
		catch (NoSuchFutureUserException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected FutureUser removeImpl(FutureUser futureUser)
		throws SystemException {
		futureUser = toUnwrappedModel(futureUser);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(futureUser)) {
				futureUser = (FutureUser)session.get(FutureUserImpl.class,
						futureUser.getPrimaryKeyObj());
			}

			if (futureUser != null) {
				session.delete(futureUser);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (futureUser != null) {
			clearCache(futureUser);
		}

		return futureUser;
	}

	@Override
	public FutureUser updateImpl(com.aw.internal.model.FutureUser futureUser)
		throws SystemException {
		futureUser = toUnwrappedModel(futureUser);

		boolean isNew = futureUser.isNew();

		FutureUserModelImpl futureUserModelImpl = (FutureUserModelImpl)futureUser;

		Session session = null;

		try {
			session = openSession();

			if (futureUser.isNew()) {
				session.save(futureUser);

				futureUser.setNew(false);
			}
			else {
				session.merge(futureUser);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !FutureUserModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((futureUserModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ACTIVEUSER.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						futureUserModelImpl.getOriginalActiveFlag(),
						futureUserModelImpl.getOriginalTakenFlag()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ACTIVEUSER,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ACTIVEUSER,
					args);

				args = new Object[] {
						futureUserModelImpl.getActiveFlag(),
						futureUserModelImpl.getTakenFlag()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ACTIVEUSER,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ACTIVEUSER,
					args);
			}

			if ((futureUserModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MYTAKEN.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						futureUserModelImpl.getOriginalActiveFlag(),
						futureUserModelImpl.getOriginalOwnerId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_MYTAKEN, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MYTAKEN,
					args);

				args = new Object[] {
						futureUserModelImpl.getActiveFlag(),
						futureUserModelImpl.getOwnerId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_MYTAKEN, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MYTAKEN,
					args);
			}
		}

		EntityCacheUtil.putResult(FutureUserModelImpl.ENTITY_CACHE_ENABLED,
			FutureUserImpl.class, futureUser.getPrimaryKey(), futureUser);

		return futureUser;
	}

	protected FutureUser toUnwrappedModel(FutureUser futureUser) {
		if (futureUser instanceof FutureUserImpl) {
			return futureUser;
		}

		FutureUserImpl futureUserImpl = new FutureUserImpl();

		futureUserImpl.setNew(futureUser.isNew());
		futureUserImpl.setPrimaryKey(futureUser.getPrimaryKey());

		futureUserImpl.setEmail(futureUser.getEmail());
		futureUserImpl.setFirstName(futureUser.getFirstName());
		futureUserImpl.setLastName(futureUser.getLastName());
		futureUserImpl.setResidency(futureUser.getResidency());
		futureUserImpl.setPhone(futureUser.getPhone());
		futureUserImpl.setAreaCode(futureUser.getAreaCode());
		futureUserImpl.setActiveFlag(futureUser.getActiveFlag());
		futureUserImpl.setTakenFlag(futureUser.getTakenFlag());
		futureUserImpl.setOwnerId(futureUser.getOwnerId());
		futureUserImpl.setAddTime(futureUser.getAddTime());

		return futureUserImpl;
	}

	/**
	 * Returns the future user with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the future user
	 * @return the future user
	 * @throws com.aw.internal.NoSuchFutureUserException if a future user with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FutureUser findByPrimaryKey(Serializable primaryKey)
		throws NoSuchFutureUserException, SystemException {
		FutureUser futureUser = fetchByPrimaryKey(primaryKey);

		if (futureUser == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchFutureUserException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return futureUser;
	}

	/**
	 * Returns the future user with the primary key or throws a {@link com.aw.internal.NoSuchFutureUserException} if it could not be found.
	 *
	 * @param email the primary key of the future user
	 * @return the future user
	 * @throws com.aw.internal.NoSuchFutureUserException if a future user with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FutureUser findByPrimaryKey(String email)
		throws NoSuchFutureUserException, SystemException {
		return findByPrimaryKey((Serializable)email);
	}

	/**
	 * Returns the future user with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the future user
	 * @return the future user, or <code>null</code> if a future user with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FutureUser fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		FutureUser futureUser = (FutureUser)EntityCacheUtil.getResult(FutureUserModelImpl.ENTITY_CACHE_ENABLED,
				FutureUserImpl.class, primaryKey);

		if (futureUser == _nullFutureUser) {
			return null;
		}

		if (futureUser == null) {
			Session session = null;

			try {
				session = openSession();

				futureUser = (FutureUser)session.get(FutureUserImpl.class,
						primaryKey);

				if (futureUser != null) {
					cacheResult(futureUser);
				}
				else {
					EntityCacheUtil.putResult(FutureUserModelImpl.ENTITY_CACHE_ENABLED,
						FutureUserImpl.class, primaryKey, _nullFutureUser);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(FutureUserModelImpl.ENTITY_CACHE_ENABLED,
					FutureUserImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return futureUser;
	}

	/**
	 * Returns the future user with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param email the primary key of the future user
	 * @return the future user, or <code>null</code> if a future user with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FutureUser fetchByPrimaryKey(String email) throws SystemException {
		return fetchByPrimaryKey((Serializable)email);
	}

	/**
	 * Returns all the future users.
	 *
	 * @return the future users
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<FutureUser> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the future users.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.FutureUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of future users
	 * @param end the upper bound of the range of future users (not inclusive)
	 * @return the range of future users
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<FutureUser> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the future users.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.FutureUserModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of future users
	 * @param end the upper bound of the range of future users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of future users
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<FutureUser> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<FutureUser> list = (List<FutureUser>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_FUTUREUSER);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_FUTUREUSER;

				if (pagination) {
					sql = sql.concat(FutureUserModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<FutureUser>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<FutureUser>(list);
				}
				else {
					list = (List<FutureUser>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the future users from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (FutureUser futureUser : findAll()) {
			remove(futureUser);
		}
	}

	/**
	 * Returns the number of future users.
	 *
	 * @return the number of future users
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_FUTUREUSER);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the future user persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.aw.internal.model.FutureUser")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<FutureUser>> listenersList = new ArrayList<ModelListener<FutureUser>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<FutureUser>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(FutureUserImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_FUTUREUSER = "SELECT futureUser FROM FutureUser futureUser";
	private static final String _SQL_SELECT_FUTUREUSER_WHERE = "SELECT futureUser FROM FutureUser futureUser WHERE ";
	private static final String _SQL_COUNT_FUTUREUSER = "SELECT COUNT(futureUser) FROM FutureUser futureUser";
	private static final String _SQL_COUNT_FUTUREUSER_WHERE = "SELECT COUNT(futureUser) FROM FutureUser futureUser WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "futureUser.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No FutureUser exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No FutureUser exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(FutureUserPersistenceImpl.class);
	private static FutureUser _nullFutureUser = new FutureUserImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<FutureUser> toCacheModel() {
				return _nullFutureUserCacheModel;
			}
		};

	private static CacheModel<FutureUser> _nullFutureUserCacheModel = new CacheModel<FutureUser>() {
			@Override
			public FutureUser toEntityModel() {
				return _nullFutureUser;
			}
		};
}