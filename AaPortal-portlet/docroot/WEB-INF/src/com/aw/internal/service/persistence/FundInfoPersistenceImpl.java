/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.persistence;

import com.aw.internal.NoSuchFundInfoException;
import com.aw.internal.model.FundInfo;
import com.aw.internal.model.impl.FundInfoImpl;
import com.aw.internal.model.impl.FundInfoModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the fund info service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author zl
 * @see FundInfoPersistence
 * @see FundInfoUtil
 * @generated
 */
public class FundInfoPersistenceImpl extends BasePersistenceImpl<FundInfo>
	implements FundInfoPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link FundInfoUtil} to access the fund info persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = FundInfoImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(FundInfoModelImpl.ENTITY_CACHE_ENABLED,
			FundInfoModelImpl.FINDER_CACHE_ENABLED, FundInfoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(FundInfoModelImpl.ENTITY_CACHE_ENABLED,
			FundInfoModelImpl.FINDER_CACHE_ENABLED, FundInfoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(FundInfoModelImpl.ENTITY_CACHE_ENABLED,
			FundInfoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public FundInfoPersistenceImpl() {
		setModelClass(FundInfo.class);
	}

	/**
	 * Caches the fund info in the entity cache if it is enabled.
	 *
	 * @param fundInfo the fund info
	 */
	@Override
	public void cacheResult(FundInfo fundInfo) {
		EntityCacheUtil.putResult(FundInfoModelImpl.ENTITY_CACHE_ENABLED,
			FundInfoImpl.class, fundInfo.getPrimaryKey(), fundInfo);

		fundInfo.resetOriginalValues();
	}

	/**
	 * Caches the fund infos in the entity cache if it is enabled.
	 *
	 * @param fundInfos the fund infos
	 */
	@Override
	public void cacheResult(List<FundInfo> fundInfos) {
		for (FundInfo fundInfo : fundInfos) {
			if (EntityCacheUtil.getResult(
						FundInfoModelImpl.ENTITY_CACHE_ENABLED,
						FundInfoImpl.class, fundInfo.getPrimaryKey()) == null) {
				cacheResult(fundInfo);
			}
			else {
				fundInfo.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all fund infos.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(FundInfoImpl.class.getName());
		}

		EntityCacheUtil.clearCache(FundInfoImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the fund info.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(FundInfo fundInfo) {
		EntityCacheUtil.removeResult(FundInfoModelImpl.ENTITY_CACHE_ENABLED,
			FundInfoImpl.class, fundInfo.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<FundInfo> fundInfos) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (FundInfo fundInfo : fundInfos) {
			EntityCacheUtil.removeResult(FundInfoModelImpl.ENTITY_CACHE_ENABLED,
				FundInfoImpl.class, fundInfo.getPrimaryKey());
		}
	}

	/**
	 * Creates a new fund info with the primary key. Does not add the fund info to the database.
	 *
	 * @param fundId the primary key for the new fund info
	 * @return the new fund info
	 */
	@Override
	public FundInfo create(long fundId) {
		FundInfo fundInfo = new FundInfoImpl();

		fundInfo.setNew(true);
		fundInfo.setPrimaryKey(fundId);

		return fundInfo;
	}

	/**
	 * Removes the fund info with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param fundId the primary key of the fund info
	 * @return the fund info that was removed
	 * @throws com.aw.internal.NoSuchFundInfoException if a fund info with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FundInfo remove(long fundId)
		throws NoSuchFundInfoException, SystemException {
		return remove((Serializable)fundId);
	}

	/**
	 * Removes the fund info with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the fund info
	 * @return the fund info that was removed
	 * @throws com.aw.internal.NoSuchFundInfoException if a fund info with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FundInfo remove(Serializable primaryKey)
		throws NoSuchFundInfoException, SystemException {
		Session session = null;

		try {
			session = openSession();

			FundInfo fundInfo = (FundInfo)session.get(FundInfoImpl.class,
					primaryKey);

			if (fundInfo == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchFundInfoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(fundInfo);
		}
		catch (NoSuchFundInfoException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected FundInfo removeImpl(FundInfo fundInfo) throws SystemException {
		fundInfo = toUnwrappedModel(fundInfo);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(fundInfo)) {
				fundInfo = (FundInfo)session.get(FundInfoImpl.class,
						fundInfo.getPrimaryKeyObj());
			}

			if (fundInfo != null) {
				session.delete(fundInfo);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (fundInfo != null) {
			clearCache(fundInfo);
		}

		return fundInfo;
	}

	@Override
	public FundInfo updateImpl(com.aw.internal.model.FundInfo fundInfo)
		throws SystemException {
		fundInfo = toUnwrappedModel(fundInfo);

		boolean isNew = fundInfo.isNew();

		Session session = null;

		try {
			session = openSession();

			if (fundInfo.isNew()) {
				session.save(fundInfo);

				fundInfo.setNew(false);
			}
			else {
				session.merge(fundInfo);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(FundInfoModelImpl.ENTITY_CACHE_ENABLED,
			FundInfoImpl.class, fundInfo.getPrimaryKey(), fundInfo);

		return fundInfo;
	}

	protected FundInfo toUnwrappedModel(FundInfo fundInfo) {
		if (fundInfo instanceof FundInfoImpl) {
			return fundInfo;
		}

		FundInfoImpl fundInfoImpl = new FundInfoImpl();

		fundInfoImpl.setNew(fundInfo.isNew());
		fundInfoImpl.setPrimaryKey(fundInfo.getPrimaryKey());

		fundInfoImpl.setFundId(fundInfo.getFundId());
		fundInfoImpl.setName(fundInfo.getName());
		fundInfoImpl.setContactId(fundInfo.getContactId());

		return fundInfoImpl;
	}

	/**
	 * Returns the fund info with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the fund info
	 * @return the fund info
	 * @throws com.aw.internal.NoSuchFundInfoException if a fund info with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FundInfo findByPrimaryKey(Serializable primaryKey)
		throws NoSuchFundInfoException, SystemException {
		FundInfo fundInfo = fetchByPrimaryKey(primaryKey);

		if (fundInfo == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchFundInfoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return fundInfo;
	}

	/**
	 * Returns the fund info with the primary key or throws a {@link com.aw.internal.NoSuchFundInfoException} if it could not be found.
	 *
	 * @param fundId the primary key of the fund info
	 * @return the fund info
	 * @throws com.aw.internal.NoSuchFundInfoException if a fund info with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FundInfo findByPrimaryKey(long fundId)
		throws NoSuchFundInfoException, SystemException {
		return findByPrimaryKey((Serializable)fundId);
	}

	/**
	 * Returns the fund info with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the fund info
	 * @return the fund info, or <code>null</code> if a fund info with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FundInfo fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		FundInfo fundInfo = (FundInfo)EntityCacheUtil.getResult(FundInfoModelImpl.ENTITY_CACHE_ENABLED,
				FundInfoImpl.class, primaryKey);

		if (fundInfo == _nullFundInfo) {
			return null;
		}

		if (fundInfo == null) {
			Session session = null;

			try {
				session = openSession();

				fundInfo = (FundInfo)session.get(FundInfoImpl.class, primaryKey);

				if (fundInfo != null) {
					cacheResult(fundInfo);
				}
				else {
					EntityCacheUtil.putResult(FundInfoModelImpl.ENTITY_CACHE_ENABLED,
						FundInfoImpl.class, primaryKey, _nullFundInfo);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(FundInfoModelImpl.ENTITY_CACHE_ENABLED,
					FundInfoImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return fundInfo;
	}

	/**
	 * Returns the fund info with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param fundId the primary key of the fund info
	 * @return the fund info, or <code>null</code> if a fund info with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public FundInfo fetchByPrimaryKey(long fundId) throws SystemException {
		return fetchByPrimaryKey((Serializable)fundId);
	}

	/**
	 * Returns all the fund infos.
	 *
	 * @return the fund infos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<FundInfo> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the fund infos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.FundInfoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of fund infos
	 * @param end the upper bound of the range of fund infos (not inclusive)
	 * @return the range of fund infos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<FundInfo> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the fund infos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.aw.internal.model.impl.FundInfoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of fund infos
	 * @param end the upper bound of the range of fund infos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of fund infos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<FundInfo> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<FundInfo> list = (List<FundInfo>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_FUNDINFO);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_FUNDINFO;

				if (pagination) {
					sql = sql.concat(FundInfoModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<FundInfo>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<FundInfo>(list);
				}
				else {
					list = (List<FundInfo>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the fund infos from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (FundInfo fundInfo : findAll()) {
			remove(fundInfo);
		}
	}

	/**
	 * Returns the number of fund infos.
	 *
	 * @return the number of fund infos
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_FUNDINFO);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the fund info persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.aw.internal.model.FundInfo")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<FundInfo>> listenersList = new ArrayList<ModelListener<FundInfo>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<FundInfo>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(FundInfoImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_FUNDINFO = "SELECT fundInfo FROM FundInfo fundInfo";
	private static final String _SQL_COUNT_FUNDINFO = "SELECT COUNT(fundInfo) FROM FundInfo fundInfo";
	private static final String _ORDER_BY_ENTITY_ALIAS = "fundInfo.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No FundInfo exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(FundInfoPersistenceImpl.class);
	private static FundInfo _nullFundInfo = new FundInfoImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<FundInfo> toCacheModel() {
				return _nullFundInfoCacheModel;
			}
		};

	private static CacheModel<FundInfo> _nullFundInfoCacheModel = new CacheModel<FundInfo>() {
			@Override
			public FundInfo toEntityModel() {
				return _nullFundInfo;
			}
		};
}