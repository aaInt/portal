package com.aw.internal.service.util;

public class ErrorConstants {	
	
	/*
	 * error code
	 */
	
	//success
	public static final String  SUCCESS = "00";
	
	//permission
	public static final String  NO_PERMISSION = "010";
	public static final String  INVALID_OWNERSHIP = "011";
	
	//sys error
	public static final String  SYSTEM_ERROR = "040";
	
	//invalid data
	public static final String  INVALID_BROKER_INFO = "050";
	public static final String  INVALID_USR_TYP = "051";
	public static final String  INVALID_USR_INFO = "052";
	public static final String  INVALID_CONTACT_INFO = "053";
	public static final String  INVALID_FUND_INFO = "054";
	public static final String  INVALID_IMAGE_INFO = "055";
	public static final String  INVALID_RSN_CD = "056";
	public static final String  INVALID_PHASE_OR_PRICE = "057";
	
	//limitation
	public static final String  TAKEN_REACH_LIMIT = "060";
	public static final String  TOTAL_COIN_REACH_LIMIT = "061";
	//public static final String  TAKEN_REACH_LIMIT = "062";
	//public static final String  TAKEN_REACH_LIMIT = "063";
	
	//exist primary key
	public static final String  DUPLI_EMAIL= "020";
	
	//process filter
	public static final String  NEED_VERIFY= "080";
	public static final String  NEED_SEND_DOC= "081";
	public static final String  NEED_PAID= "082";
	public static final String  NEED_SIGNED= "083";
	public static final String  NEED_APPROVE= "084";
	public static final String  NEED_SEND_CERT= "085";
	public static final String  ORDER_NOT_ACTIVE= "086";
	
	//validation error
	public static final String  USER_NOT_VERIFIED= "090";
	public static final String  BROKER_NOT_VERIFIED= "091";
	public static final String  FUND_NOT_VERIFIED= "092";
	//public static final String  USER_NOT_VERIFIED= "090";
	
	
}
