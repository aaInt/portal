/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.impl;

import java.util.Date;

import org.apache.commons.codec.binary.Base64;

import com.aw.internal.model.CertDocStore;
import com.aw.internal.service.base.CertDocStoreLocalServiceBaseImpl;
import com.aw.internal.service.persistence.CertDocStoreUtil;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.dao.jdbc.OutputBlob;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayInputStream;

/**
 * The implementation of the cert doc store local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.aw.internal.service.CertDocStoreLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author zl
 * @see com.aw.internal.service.base.CertDocStoreLocalServiceBaseImpl
 * @see com.aw.internal.service.CertDocStoreLocalServiceUtil
 */
public class CertDocStoreLocalServiceImpl
	extends CertDocStoreLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.aw.internal.service.CertDocStoreLocalServiceUtil} to access the cert doc store local service.
	 */
	public CertDocStore addDoc(String base64Str) throws SystemException{
		
		long id = CounterLocalServiceUtil.increment(CertDocStore.class.getName());
		
		CertDocStore doc = CertDocStoreUtil.create(id);
		
	
		
		//create blob 
		byte[] bytes = Base64.decodeBase64(base64Str);
		UnsyncByteArrayInputStream inputStream =new UnsyncByteArrayInputStream(bytes);
		OutputBlob docBlob = new OutputBlob(inputStream, bytes.length);
		
		doc.setContent(docBlob);
		doc.setActiveFlag("Y");
		doc.setAddDate(new Date());
		
		doc.persist();
		
		return doc;
	}
}