/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.impl;

import com.aw.internal.model.AaContact;
import com.aw.internal.model.FundInfo;
import com.aw.internal.service.AaContactLocalServiceUtil;
import com.aw.internal.service.FundInfoLocalServiceUtil;
import com.aw.internal.service.base.FundInfoLocalServiceBaseImpl;
import com.aw.internal.service.persistence.FundInfoUtil;
import com.aw.internal.service.util.DataUtil;
import com.aw.internal.service.util.ErrorConstants;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONObject;


/**
 * The implementation of the fund info local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.liferay.sample.service.FundInfoLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author zl
 * @see com.aw.internal.service.base.FundInfoLocalServiceBaseImpl
 * @see com.liferay.sample.service.FundInfoLocalServiceUtil
 */
public class FundInfoLocalServiceImpl extends FundInfoLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.liferay.sample.service.FundInfoLocalServiceUtil} to access the fund info local service.
	 */
	public JSONObject addFund(String name,
			long phone, int areaCode, String email, String wechat, String facebook, String twitter, String weibo
			, String street1, String street2, String city, String state, String country, String zipCode){
		
		long contactId = 0;
		long fundId = 0;
		try {
			contactId = CounterLocalServiceUtil.increment(AaContact.class.getName());
			fundId = CounterLocalServiceUtil.increment(FundInfo.class.getName());
		} catch (SystemException e) {
			e.printStackTrace();
			return DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
		}
		
		if(AaContactLocalServiceUtil.createContact(contactId, phone, areaCode, email, wechat, facebook, twitter
				, weibo, street1, street2, city, state, country, zipCode, "N", null, null, null, null, null, null)){
			FundInfo f = FundInfoLocalServiceUtil.createFundInfo(fundId);
			f.setName(name);
			f.setContactId(contactId);
			try {
				f.persist();
				return DataUtil.getResponse(DataUtil.getFundJson(f), null, ErrorConstants.SUCCESS);
			} catch (SystemException e) {
				e.printStackTrace();
				return DataUtil.getResponse(null, null, ErrorConstants.INVALID_FUND_INFO);
			}
			
		}else{
			return DataUtil.getResponse(null, null, ErrorConstants.INVALID_CONTACT_INFO);
		}

	}
	
	public FundInfo getFund(long fundId){
		try {
			return FundInfoLocalServiceUtil.fetchFundInfo(fundId);
		} catch (SystemException e) {			
			e.printStackTrace();
			return null;
		}
	}
	
	public String updateFund(){
		return null;
	}
	
	public boolean verifyFund(long fundId){
		boolean b = false;
		try {
			if(null != FundInfoUtil.fetchByPrimaryKey(fundId)){
				b = true;
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return b;
	}
	
}