/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.impl;

import java.util.List;

import com.aw.internal.model.FailReason;
import com.aw.internal.service.FailReasonLocalServiceUtil;
import com.aw.internal.service.base.FailReasonLocalServiceBaseImpl;
import com.aw.internal.service.persistence.FailReasonUtil;
import com.aw.internal.service.util.DataUtil;
import com.aw.internal.service.util.ErrorConstants;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
/**
 * The implementation of the fail reason local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.liferay.sample.service.FailReasonLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author zl
 * @see com.aw.internal.service.base.FailReasonLocalServiceBaseImpl
 * @see com.liferay.sample.service.FailReasonLocalServiceUtil
 */
public class FailReasonLocalServiceImpl extends FailReasonLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.liferay.sample.service.FailReasonLocalServiceUtil} to access the fail reason local service.
	 */
	public JSONObject addReason(String rsnCd, String desc){
		FailReason fr = FailReasonLocalServiceUtil.createFailReason(rsnCd);
		fr.setDesc(desc);
		try {
			fr.persist();
			return DataUtil.getResponse(null, null, ErrorConstants.SUCCESS);
		} catch (SystemException e) {
			e.printStackTrace();
			return DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
		}
	}
	
	public JSONObject getReason(String rsnCd){
		FailReason fr = null;;
		try {
			fr = FailReasonLocalServiceUtil.fetchFailReason(rsnCd);
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return DataUtil.getResponse(null, null, ErrorConstants.SYSTEM_ERROR);
		}
		if(fr != null){
			return DataUtil.getResponse(DataUtil.getRsnJson(fr), null, ErrorConstants.SUCCESS);
		}else{
			return null;
		}
	}
	
	public JSONObject getReasons(){
		List<FailReason> list = null;
		try {
			list = FailReasonUtil.findAll();
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONObject json = JSONFactoryUtil.createJSONObject();
		if(list != null && !list.isEmpty()){
			for(FailReason fr : list){
				json.put(fr.getRsnCd(), fr.getDesc());
			}
		}
		return DataUtil.getResponse(json, null, ErrorConstants.SUCCESS);
		
	}
}