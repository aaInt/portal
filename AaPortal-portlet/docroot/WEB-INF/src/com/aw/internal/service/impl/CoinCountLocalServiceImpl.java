/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.impl;

import java.util.List;

import com.aw.internal.model.CoinCount;
import com.aw.internal.service.base.CoinCountLocalServiceBaseImpl;
import com.aw.internal.service.persistence.CoinCountUtil;
import com.aw.internal.service.util.DataUtil;
import com.aw.internal.service.util.ErrorConstants;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONObject;

/**
 * The implementation of the coin count local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.aw.internal.service.CoinCountLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author zl
 * @see com.aw.internal.service.base.CoinCountLocalServiceBaseImpl
 * @see com.aw.internal.service.CoinCountLocalServiceUtil
 */
public class CoinCountLocalServiceImpl extends CoinCountLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.aw.internal.service.CoinCountLocalServiceUtil} to access the coin count local service.
	 */
	public boolean endPhase(int endPhase, int toPhase){
		
		boolean success = false;
		
		if(!(toPhase > endPhase)){// can only move forward
			return success;
		}
		
		try {
			CoinCount ccFrom = CoinCountUtil.fetchByPrimaryKey(endPhase);
			CoinCount ccTo = CoinCountUtil.fetchByPrimaryKey(toPhase);			
			
			if(ccFrom != null && ccTo != null){
				
				if("Y".equalsIgnoreCase(ccFrom.getEndFlag())){//return if is already end
					return success;
				}
				
				long rest = ccFrom.getLimit() + ccFrom.getRollOverAmt() - ccFrom.getTotalAmount();
				ccFrom.setRollToAmt(rest);
				ccFrom.setEndFlag("Y");
				ccFrom.setRollToPhase(toPhase);
				ccFrom.persist();
				
				long newRollOver = ccTo.getRollOverAmt() + rest;
				ccTo.setRollOverAmt(newRollOver);
				ccTo.persist();
				
				success = true;
			}
			
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		return success;
	
	}
	
	public JSONObject getCoinCounts(){
		List<CoinCount> list = null;
		try {
			list = CoinCountUtil.findAll();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return DataUtil.getResponse(null, DataUtil.getCoinCounts(list), ErrorConstants.SUCCESS);
	}
		
}