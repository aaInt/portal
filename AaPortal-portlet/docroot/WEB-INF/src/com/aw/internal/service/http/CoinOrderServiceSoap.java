/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aw.internal.service.http;

import com.aw.internal.service.CoinOrderServiceUtil;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.rmi.RemoteException;

/**
 * Provides the SOAP utility for the
 * {@link com.aw.internal.service.CoinOrderServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it is difficult for SOAP to
 * support certain types.
 *
 * <p>
 * ServiceBuilder follows certain rules in translating the methods. For example,
 * if the method in the service utility returns a {@link java.util.List}, that
 * is translated to an array of {@link com.aw.internal.model.CoinOrderSoap}.
 * If the method in the service utility returns a
 * {@link com.aw.internal.model.CoinOrder}, that is translated to a
 * {@link com.aw.internal.model.CoinOrderSoap}. Methods that SOAP cannot
 * safely wire are skipped.
 * </p>
 *
 * <p>
 * The benefits of using the SOAP utility is that it is cross platform
 * compatible. SOAP allows different languages like Java, .NET, C++, PHP, and
 * even Perl, to call the generated services. One drawback of SOAP is that it is
 * slow because it needs to serialize all calls into a text format (XML).
 * </p>
 *
 * <p>
 * You can see a list of services at http://localhost:8080/api/axis. Set the
 * property <b>axis.servlet.hosts.allowed</b> in portal.properties to configure
 * security.
 * </p>
 *
 * <p>
 * The SOAP utility is only generated for remote services.
 * </p>
 *
 * @author zl
 * @see CoinOrderServiceHttp
 * @see com.aw.internal.model.CoinOrderSoap
 * @see com.aw.internal.service.CoinOrderServiceUtil
 * @generated
 */
public class CoinOrderServiceSoap {
	public static java.lang.String verifyOrderInfo(java.lang.String orderId,
		java.lang.String userId, java.lang.String lastName)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CoinOrderServiceUtil.verifyOrderInfo(orderId,
					userId, lastName);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String createCoinOrder(java.lang.String userId,
		long brokerId, int phase, double unitPrice, double totalPrice,
		java.lang.String currency, long saleAmt, long promoAmt)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CoinOrderServiceUtil.createCoinOrder(userId,
					brokerId, phase, unitPrice, totalPrice, currency, saleAmt,
					promoAmt);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String splitOrder(java.lang.String originOrderId,
		long salesId, java.lang.String userId, long amount)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CoinOrderServiceUtil.splitOrder(originOrderId,
					salesId, userId, amount);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getPedingOrder() throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CoinOrderServiceUtil.getPedingOrder();

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getStaffOrders() throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CoinOrderServiceUtil.getStaffOrders();

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getCompletedOrders()
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CoinOrderServiceUtil.getCompletedOrders();

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getFailedOrder() throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CoinOrderServiceUtil.getFailedOrder();

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String verified(java.lang.String orderId)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CoinOrderServiceUtil.verified(orderId);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String contractSent(java.lang.String orderId,
		java.lang.String carrier, java.lang.String trackingNo)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CoinOrderServiceUtil.contractSent(orderId,
					carrier, trackingNo);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String contractSigned(java.lang.String orderId)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CoinOrderServiceUtil.contractSigned(orderId);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String paid(java.lang.String orderId)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CoinOrderServiceUtil.paid(orderId);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String approve(java.lang.String orderId)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CoinOrderServiceUtil.approve(orderId);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String certSent(java.lang.String orderId,
		java.lang.String carrier, java.lang.String trackingNo)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CoinOrderServiceUtil.certSent(orderId,
					carrier, trackingNo);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String complete(java.lang.String orderId)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CoinOrderServiceUtil.complete(orderId);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String fail(java.lang.String orderId,
		java.lang.String rsnCd) throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CoinOrderServiceUtil.fail(orderId,
					rsnCd);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String addReason(java.lang.String rsnCd,
		java.lang.String desc) throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CoinOrderServiceUtil.addReason(rsnCd,
					desc);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getReason(java.lang.String rsnCd)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CoinOrderServiceUtil.getReason(rsnCd);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getReasons() throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CoinOrderServiceUtil.getReasons();

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getSaleSummary(boolean isPaid,
		boolean isPending, int phase, java.lang.String startDate,
		java.lang.String endDate) throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = CoinOrderServiceUtil.getSaleSummary(isPaid,
					isPending, phase, startDate, endDate);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	private static Log _log = LogFactoryUtil.getLog(CoinOrderServiceSoap.class);
}