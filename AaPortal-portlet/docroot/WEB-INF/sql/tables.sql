create table AW_AaContact (
	contactId LONG not null primary key,
	phone LONG,
	areaCode INTEGER,
	email VARCHAR(75) null,
	wechat VARCHAR(75) null,
	facebook VARCHAR(75) null,
	twitter VARCHAR(75) null,
	weibo VARCHAR(75) null,
	street1 VARCHAR(75) null,
	street2 VARCHAR(75) null,
	city VARCHAR(75) null,
	state_ VARCHAR(75) null,
	country VARCHAR(75) null,
	zipCode VARCHAR(75) null,
	isMailDiff VARCHAR(75) null,
	mailstreet1 VARCHAR(75) null,
	mailstreet2 VARCHAR(75) null,
	mailcity VARCHAR(75) null,
	mailstate VARCHAR(75) null,
	mailcountry VARCHAR(75) null,
	mailzipCode VARCHAR(75) null
);

create table AW_ActivePhase (
	region VARCHAR(75) not null primary key,
	salePhase INTEGER
);

create table AW_BrokerInfo (
	brokerId LONG not null primary key,
	licenseId VARCHAR(75) null,
	licenseCountry VARCHAR(75) null
);

create table AW_CertDocStore (
	docId LONG not null primary key,
	content BLOB,
	activeFlag VARCHAR(75) null,
	addDate DATE null
);

create table AW_CoinCount (
	phase INTEGER not null primary key,
	totalAmount LONG,
	endFlag VARCHAR(75) null,
	rollToPhase INTEGER,
	rollToAmt LONG,
	limit_ LONG,
	rollOverAmt LONG,
	lastOrderId VARCHAR(75) null
);

create table AW_CoinOrder (
	orderId VARCHAR(75) not null primary key,
	userId VARCHAR(75) null,
	saleRegion VARCHAR(75) null,
	brokerId LONG,
	salesId LONG,
	activeFlag VARCHAR(75) null,
	salePhase INTEGER,
	unitPrice DOUBLE,
	totalPrice DOUBLE,
	currency_ VARCHAR(75) null,
	saleAmount LONG,
	promoAmount LONG,
	totalAmount LONG,
	rsnCd VARCHAR(75) null,
	verifyFlag VARCHAR(75) null,
	docSentFlag VARCHAR(75) null,
	docCarrier VARCHAR(75) null,
	docTrackingNo VARCHAR(75) null,
	docSignFlag VARCHAR(75) null,
	paidFlag VARCHAR(75) null,
	approvalFlag VARCHAR(75) null,
	certSentFlag VARCHAR(75) null,
	certCarrier VARCHAR(75) null,
	certTrackingNo VARCHAR(75) null,
	completeFlag VARCHAR(75) null,
	caseCloser LONG,
	addTime DATE null
);

create table AW_CoinUser (
	userId VARCHAR(75) not null primary key,
	firstName VARCHAR(75) null,
	lastName VARCHAR(75) null,
	activeFlag VARCHAR(75) null,
	birthday VARCHAR(75) null,
	contactId LONG,
	userType VARCHAR(75) null,
	brokerId LONG,
	fundId LONG,
	nationality VARCHAR(75) null,
	idIssueCountry VARCHAR(75) null,
	idType VARCHAR(75) null,
	idNumber VARCHAR(75) null,
	photoImage LONG,
	docId LONG,
	ssn VARCHAR(75) null,
	ein VARCHAR(75) null,
	income VARCHAR(75) null,
	asset VARCHAR(75) null,
	isValidated VARCHAR(75) null,
	isCreddited VARCHAR(75) null,
	addTime DATE null,
	addUser LONG
);

create table AW_FailReason (
	rsnCd VARCHAR(75) not null primary key,
	desc_ VARCHAR(75) null
);

create table AW_FundInfo (
	fundId LONG not null primary key,
	name VARCHAR(75) null,
	contactId LONG
);

create table AW_FutureUser (
	email VARCHAR(75) not null primary key,
	firstName VARCHAR(75) null,
	lastName VARCHAR(75) null,
	residency VARCHAR(75) null,
	phone LONG,
	areaCode INTEGER,
	activeFlag VARCHAR(75) null,
	takenFlag VARCHAR(75) null,
	ownerId LONG,
	addTime DATE null
);

create table AW_ImageStore (
	imageId LONG not null primary key,
	content BLOB,
	activeFlag VARCHAR(75) null,
	addDate DATE null
);

create table AW_UnitPrice (
	currency_ VARCHAR(75) not null,
	salePhase INTEGER not null,
	price DOUBLE,
	activeFlag VARCHAR(75) null,
	addDate DATE null,
	addUser LONG,
	primary key (currency_, salePhase)
);