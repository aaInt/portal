create index IX_68967FAF on AW_AaContact (email);

create index IX_88F82499 on AW_CoinOrder (activeFlag, completeFlag);

create index IX_A9B7B91 on AW_CoinUser (activeFlag);
create index IX_14EC4A11 on AW_CoinUser (addUser);
create index IX_8594CE17 on AW_CoinUser (addUser, activeFlag);
create index IX_C7F8F7CD on AW_CoinUser (firstName, lastName, idNumber);
create index IX_1907B689 on AW_CoinUser (isValidated);
create index IX_4D59468F on AW_CoinUser (isValidated, activeFlag);

create index IX_5200C63B on AW_FutureUser (activeFlag, ownerId);
create index IX_2A15FFE0 on AW_FutureUser (activeFlag, takenFlag);